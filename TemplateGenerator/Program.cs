﻿using Microsoft.Xrm.Client;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TemplateGenerator
{
    class Program
    {
        static void Main(string[] args)
        {
            string myArgs = args[0];
            string templateName = null;

            if (myArgs.Contains("/name:")) 
            {
                templateName = myArgs.Split(':')[1];
            }

            RegenerateTemplate(templateName.Trim());

        }

        public static void RegenerateTemplate(string templateName) 
        {
            CrmConnection conn = new CrmConnection("CRM2016");
            CrmOrganizationServiceContext context = new CrmOrganizationServiceContext(conn);


           
            //IOrganizationService service = context.

            Entity template = (from templates in context.CreateQuery("oems_portalformtemplate")
                               where (string)templates["oems_name"]== templateName
                               select templates).FirstOrDefault();

            Entity toUpd = new Entity(template.LogicalName, template.Id);

            toUpd["oems_regenerate"] = true;

            context.Update(toUpd);
            context.SaveChanges();
           
        }
    }
}

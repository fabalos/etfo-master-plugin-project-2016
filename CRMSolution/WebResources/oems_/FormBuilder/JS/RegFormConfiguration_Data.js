﻿var tempData = [
  {
      "id": "34217291-bed1-e311-ab4d-000c29675537",
      "sourceId": "00000000-0000-0000-0000-000000000000",
      "text": "Test",
      "checked": true,
      "index": 1,
      "expanded": false,
      "spriteCssClass": null,
      "mandatory": false,
      "properties": [
        {
            "pr_Label": "abcedf"
        },
        {
            "pr_PlaceholderText": ""
        },
        {
            "pr_ReadOnly": ""
        },
        {
            "pr_Required": ""
        },
        {
            "pr_MinLength": ""
        },
        {
            "pr_MaxLength": ""
        },
        {
            "pr_PersonalAccommodationList": [
              {
                  "id": "50bdd392-5c63-e311-af5c-22000ab9871c",
                  "sourceId": "00000000-0000-0000-0000-000000000000",
                  "text": "Visual",
                  "checked": false,
                  "expanded": true,
                  "items": [
                    {
                        "id": "52bdd392-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Larger Fonts",
                        "checked": false,
                        "expanded": true,
                        "items": null
                    },
                    {
                        "id": "54bdd392-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Descriptive Video",
                        "checked": false,
                        "expanded": true,
                        "items": null
                    },
                    {
                        "id": "56bdd392-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Alternative Media",
                        "checked": false,
                        "expanded": true,
                        "items": [
                          {
                              "id": "58bdd392-5c63-e311-af5c-22000ab9871c",
                              "sourceId": "00000000-0000-0000-0000-000000000000",
                              "text": "Brail",
                              "checked": false,
                              "expanded": true,
                              "items": null
                          },
                          {
                              "id": "5abdd392-5c63-e311-af5c-22000ab9871c",
                              "sourceId": "00000000-0000-0000-0000-000000000000",
                              "text": "Other",
                              "checked": false,
                              "expanded": true,
                              "items": null
                          }
                        ]
                    }
                  ]
              },
              {
                  "id": "5cbdd392-5c63-e311-af5c-22000ab9871c",
                  "sourceId": "00000000-0000-0000-0000-000000000000",
                  "text": "Audio",
                  "checked": false,
                  "expanded": true,
                  "items": [
                    {
                        "id": "5ebdd392-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Hearing Aid",
                        "checked": false,
                        "expanded": true,
                        "items": null
                    },
                    {
                        "id": "60bdd392-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Headphones",
                        "checked": false,
                        "expanded": true,
                        "items": null
                    },
                    {
                        "id": "62bdd392-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Sign Language",
                        "checked": false,
                        "expanded": true,
                        "items": null
                    },
                    {
                        "id": "64bdd392-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Video/Subtitling",
                        "checked": false,
                        "expanded": true,
                        "items": null
                    },
                    {
                        "id": "66bdd392-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Close Captioning",
                        "checked": false,
                        "expanded": true,
                        "items": null
                    },
                    {
                        "id": "68bdd392-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Add FM System",
                        "checked": false,
                        "expanded": true,
                        "items": null
                    },
                    {
                        "id": "6abdd392-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Other",
                        "checked": false,
                        "expanded": true,
                        "items": null
                    }
                  ]
              },
              {
                  "id": "6cbdd392-5c63-e311-af5c-22000ab9871c",
                  "sourceId": "00000000-0000-0000-0000-000000000000",
                  "text": "Physical",
                  "checked": false,
                  "expanded": true,
                  "items": [
                    {
                        "id": "6ebdd392-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Wheelchair",
                        "checked": false,
                        "expanded": true,
                        "items": null
                    },
                    {
                        "id": "70bdd392-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Motorized Scooter",
                        "checked": false,
                        "expanded": true,
                        "items": null
                    },
                    {
                        "id": "72bdd392-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Cane",
                        "checked": false,
                        "expanded": true,
                        "items": null
                    },
                    {
                        "id": "74bdd392-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Walker",
                        "checked": false,
                        "expanded": true,
                        "items": null
                    },
                    {
                        "id": "76bdd392-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Tiling of the doors",
                        "checked": false,
                        "expanded": true,
                        "items": null
                    },
                    {
                        "id": "78bdd392-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Other",
                        "checked": false,
                        "expanded": true,
                        "items": null
                    }
                  ]
              },
              {
                  "id": "7abdd392-5c63-e311-af5c-22000ab9871c",
                  "sourceId": "00000000-0000-0000-0000-000000000000",
                  "text": "Allergies",
                  "checked": false,
                  "expanded": true,
                  "items": [
                    {
                        "id": "7cbdd392-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Food Allergy",
                        "checked": false,
                        "expanded": true,
                        "items": [
                          {
                              "id": "7ebdd392-5c63-e311-af5c-22000ab9871c",
                              "sourceId": "00000000-0000-0000-0000-000000000000",
                              "text": "Nuts",
                              "checked": false,
                              "expanded": true,
                              "items": null
                          },
                          {
                              "id": "80bdd392-5c63-e311-af5c-22000ab9871c",
                              "sourceId": "00000000-0000-0000-0000-000000000000",
                              "text": "Gluten",
                              "checked": false,
                              "expanded": true,
                              "items": null
                          },
                          {
                              "id": "82bdd392-5c63-e311-af5c-22000ab9871c",
                              "sourceId": "00000000-0000-0000-0000-000000000000",
                              "text": "Lactose",
                              "checked": false,
                              "expanded": true,
                              "items": null
                          },
                          {
                              "id": "84bdd392-5c63-e311-af5c-22000ab9871c",
                              "sourceId": "00000000-0000-0000-0000-000000000000",
                              "text": "Wheat",
                              "checked": false,
                              "expanded": true,
                              "items": null
                          },
                          {
                              "id": "86bdd392-5c63-e311-af5c-22000ab9871c",
                              "sourceId": "00000000-0000-0000-0000-000000000000",
                              "text": "Other",
                              "checked": false,
                              "expanded": true,
                              "items": null
                          }
                        ]
                    },
                    {
                        "id": "88bdd392-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Airborne Allergy",
                        "checked": false,
                        "expanded": true,
                        "items": [
                          {
                              "id": "8abdd392-5c63-e311-af5c-22000ab9871c",
                              "sourceId": "00000000-0000-0000-0000-000000000000",
                              "text": "Nuts",
                              "checked": false,
                              "expanded": true,
                              "items": null
                          },
                          {
                              "id": "8cbdd392-5c63-e311-af5c-22000ab9871c",
                              "sourceId": "00000000-0000-0000-0000-000000000000",
                              "text": "Perfume",
                              "checked": false,
                              "expanded": true,
                              "items": null
                          },
                          {
                              "id": "8ebdd392-5c63-e311-af5c-22000ab9871c",
                              "sourceId": "00000000-0000-0000-0000-000000000000",
                              "text": "Other",
                              "checked": false,
                              "expanded": true,
                              "items": null
                          }
                        ]
                    },
                    {
                        "id": "90bdd392-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Epipen",
                        "checked": false,
                        "expanded": true,
                        "items": null
                    },
                    {
                        "id": "92bdd392-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Inhaler",
                        "checked": false,
                        "expanded": true,
                        "items": null
                    },
                    {
                        "id": "94bdd392-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Other",
                        "checked": false,
                        "expanded": true,
                        "items": null
                    }
                  ]
              },
              {
                  "id": "96bdd392-5c63-e311-af5c-22000ab9871c",
                  "sourceId": "00000000-0000-0000-0000-000000000000",
                  "text": "Assistance",
                  "checked": false,
                  "expanded": true,
                  "items": [
                    {
                        "id": "98bdd392-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Support Person",
                        "checked": false,
                        "expanded": true,
                        "items": [
                          {
                              "id": "9abdd392-5c63-e311-af5c-22000ab9871c",
                              "sourceId": "00000000-0000-0000-0000-000000000000",
                              "text": "Accommodation",
                              "checked": false,
                              "expanded": true,
                              "items": null
                          },
                          {
                              "id": "9cbdd392-5c63-e311-af5c-22000ab9871c",
                              "sourceId": "00000000-0000-0000-0000-000000000000",
                              "text": "Separate Bed",
                              "checked": false,
                              "expanded": true,
                              "items": null
                          },
                          {
                              "id": "9ebdd392-5c63-e311-af5c-22000ab9871c",
                              "sourceId": "00000000-0000-0000-0000-000000000000",
                              "text": "Meals",
                              "checked": false,
                              "expanded": true,
                              "items": null
                          },
                          {
                              "id": "a0bdd392-5c63-e311-af5c-22000ab9871c",
                              "sourceId": "00000000-0000-0000-0000-000000000000",
                              "text": "Other",
                              "checked": false,
                              "expanded": true,
                              "items": null
                          }
                        ]
                    },
                    {
                        "id": "a2bdd392-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Service Animal",
                        "checked": false,
                        "expanded": true,
                        "items": null
                    },
                    {
                        "id": "a4bdd392-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Check-in Assistance",
                        "checked": false,
                        "expanded": true,
                        "items": null
                    },
                    {
                        "id": "a6bdd392-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Emergency Evacuation",
                        "checked": false,
                        "expanded": true,
                        "items": null
                    },
                    {
                        "id": "a8bdd392-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Accessible Room",
                        "checked": false,
                        "expanded": true,
                        "items": [
                          {
                              "id": "aabdd392-5c63-e311-af5c-22000ab9871c",
                              "sourceId": "00000000-0000-0000-0000-000000000000",
                              "text": "Lower Bed",
                              "checked": false,
                              "expanded": true,
                              "items": null
                          },
                          {
                              "id": "acbdd392-5c63-e311-af5c-22000ab9871c",
                              "sourceId": "00000000-0000-0000-0000-000000000000",
                              "text": "Full accessibility (Roll-in Shower)",
                              "checked": false,
                              "expanded": true,
                              "items": null
                          },
                          {
                              "id": "aebdd392-5c63-e311-af5c-22000ab9871c",
                              "sourceId": "00000000-0000-0000-0000-000000000000",
                              "text": "Partial Accessibility (Grab Bars)",
                              "checked": false,
                              "expanded": true,
                              "items": null
                          },
                          {
                              "id": "b0bdd392-5c63-e311-af5c-22000ab9871c",
                              "sourceId": "00000000-0000-0000-0000-000000000000",
                              "text": "Raised Toilet Seat",
                              "checked": false,
                              "expanded": true,
                              "items": null
                          },
                          {
                              "id": "b2bdd392-5c63-e311-af5c-22000ab9871c",
                              "sourceId": "00000000-0000-0000-0000-000000000000",
                              "text": "Other",
                              "checked": false,
                              "expanded": true,
                              "items": null
                          }
                        ]
                    },
                    {
                        "id": "b4bdd392-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Refrigerated Medication",
                        "checked": false,
                        "expanded": true,
                        "items": null
                    },
                    {
                        "id": "b6bdd392-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Parking Spot",
                        "checked": false,
                        "expanded": true,
                        "items": null
                    },
                    {
                        "id": "b8bdd392-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Other",
                        "checked": false,
                        "expanded": true,
                        "items": null
                    }
                  ]
              }
            ]
        },
        {
            "pr_AttendingAsRolesList": [
              {
                  "id": "00000000-0000-0000-0000-000000000001",
                  "sourceId": "00000000-0000-0000-0000-000000000000",
                  "text": "Voting Role",
                  "checked": false,
                  "expanded": true,
                  "items": [
                    {
                        "id": "607570b0-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Delegate",
                        "checked": false,
                        "expanded": true,
                        "items": null
                    },
                    {
                        "id": "627570b0-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Alternate",
                        "checked": false,
                        "expanded": true,
                        "items": null
                    },
                    {
                        "id": "647570b0-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Paid Alternate",
                        "checked": false,
                        "expanded": true,
                        "items": null
                    },
                    {
                        "id": "687570b0-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Executive",
                        "checked": false,
                        "expanded": true,
                        "items": null
                    },
                    {
                        "id": "7c7570b0-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "President",
                        "checked": false,
                        "expanded": true,
                        "items": null
                    },
                    {
                        "id": "7e7570b0-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Additional Local Representative",
                        "checked": false,
                        "expanded": true,
                        "items": null
                    },
                    {
                        "id": "807570b0-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "DECE President",
                        "checked": false,
                        "expanded": true,
                        "items": null
                    }
                  ]
              },
              {
                  "id": "00000000-0000-0000-0000-000000000002",
                  "sourceId": "00000000-0000-0000-0000-000000000000",
                  "text": "Non-Voting Role",
                  "checked": false,
                  "expanded": true,
                  "items": [
                    {
                        "id": "667570b0-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Observer",
                        "checked": false,
                        "expanded": true,
                        "items": null
                    },
                    {
                        "id": "6a7570b0-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Honorary Life Member",
                        "checked": false,
                        "expanded": true,
                        "items": null
                    },
                    {
                        "id": "6c7570b0-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Past President",
                        "checked": false,
                        "expanded": true,
                        "items": null
                    },
                    {
                        "id": "6e7570b0-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Committee Chair",
                        "checked": false,
                        "expanded": true,
                        "items": null
                    },
                    {
                        "id": "707570b0-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Staff",
                        "checked": false,
                        "expanded": true,
                        "items": null
                    },
                    {
                        "id": "727570b0-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Committee Member",
                        "checked": false,
                        "expanded": true,
                        "items": null
                    },
                    {
                        "id": "747570b0-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Award Recipient",
                        "checked": false,
                        "expanded": true,
                        "items": null
                    },
                    {
                        "id": "767570b0-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Affiliate Observer",
                        "checked": false,
                        "expanded": true,
                        "items": null
                    },
                    {
                        "id": "787570b0-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Organization Guest",
                        "checked": false,
                        "expanded": true,
                        "items": null
                    },
                    {
                        "id": "7a7570b0-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Other  <specify>",
                        "checked": false,
                        "expanded": true,
                        "items": null
                    },
                    {
                        "id": "827570b0-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Local Observer",
                        "checked": false,
                        "expanded": true,
                        "items": null
                    },
                    {
                        "id": "847570b0-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Individual Observer",
                        "checked": false,
                        "expanded": true,
                        "items": null
                    },
                    {
                        "id": "867570b0-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Member of  Designated Group",
                        "checked": false,
                        "expanded": true,
                        "items": null
                    }
                  ]
              }
            ]
        }
      ],
      "items": [
        {
            "id": "1ec817c6-04d2-e311-ab4d-000c29675537",
            "sourceId": "34217291-bed1-e311-ab4d-000c29675537",
            "text": "Test2",
            "checked": true,
            "index": 1,
            "expanded": false,
            "spriteCssClass": null,
            "mandatory": false,
            "properties": [
              {
                  "pr_Label": ""
              },
              {
                  "pr_PlaceholderText": ""
              },
              {
                  "pr_ReadOnly": ""
              },
              {
                  "pr_Required": ""
              },
              {
                  "pr_MinLength": ""
              },
              {
                  "pr_MaxLength": ""
              },
              {
                  "pr_PersonalAccommodationList": [
                    {
                        "id": "50bdd392-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Visual",
                        "checked": false,
                        "expanded": true,
                        "items": [
                          {
                              "id": "52bdd392-5c63-e311-af5c-22000ab9871c",
                              "sourceId": "00000000-0000-0000-0000-000000000000",
                              "text": "Larger Fonts",
                              "checked": false,
                              "expanded": true,
                              "items": null
                          },
                          {
                              "id": "54bdd392-5c63-e311-af5c-22000ab9871c",
                              "sourceId": "00000000-0000-0000-0000-000000000000",
                              "text": "Descriptive Video",
                              "checked": false,
                              "expanded": true,
                              "items": null
                          },
                          {
                              "id": "56bdd392-5c63-e311-af5c-22000ab9871c",
                              "sourceId": "00000000-0000-0000-0000-000000000000",
                              "text": "Alternative Media",
                              "checked": false,
                              "expanded": true,
                              "items": [
                                {
                                    "id": "58bdd392-5c63-e311-af5c-22000ab9871c",
                                    "sourceId": "00000000-0000-0000-0000-000000000000",
                                    "text": "Brail",
                                    "checked": false,
                                    "expanded": true,
                                    "items": null
                                },
                                {
                                    "id": "5abdd392-5c63-e311-af5c-22000ab9871c",
                                    "sourceId": "00000000-0000-0000-0000-000000000000",
                                    "text": "Other",
                                    "checked": false,
                                    "expanded": true,
                                    "items": null
                                }
                              ]
                          }
                        ]
                    },
                    {
                        "id": "5cbdd392-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Audio",
                        "checked": false,
                        "expanded": true,
                        "items": [
                          {
                              "id": "5ebdd392-5c63-e311-af5c-22000ab9871c",
                              "sourceId": "00000000-0000-0000-0000-000000000000",
                              "text": "Hearing Aid",
                              "checked": false,
                              "expanded": true,
                              "items": null
                          },
                          {
                              "id": "60bdd392-5c63-e311-af5c-22000ab9871c",
                              "sourceId": "00000000-0000-0000-0000-000000000000",
                              "text": "Headphones",
                              "checked": false,
                              "expanded": true,
                              "items": null
                          },
                          {
                              "id": "62bdd392-5c63-e311-af5c-22000ab9871c",
                              "sourceId": "00000000-0000-0000-0000-000000000000",
                              "text": "Sign Language",
                              "checked": false,
                              "expanded": true,
                              "items": null
                          },
                          {
                              "id": "64bdd392-5c63-e311-af5c-22000ab9871c",
                              "sourceId": "00000000-0000-0000-0000-000000000000",
                              "text": "Video/Subtitling",
                              "checked": false,
                              "expanded": true,
                              "items": null
                          },
                          {
                              "id": "66bdd392-5c63-e311-af5c-22000ab9871c",
                              "sourceId": "00000000-0000-0000-0000-000000000000",
                              "text": "Close Captioning",
                              "checked": false,
                              "expanded": true,
                              "items": null
                          },
                          {
                              "id": "68bdd392-5c63-e311-af5c-22000ab9871c",
                              "sourceId": "00000000-0000-0000-0000-000000000000",
                              "text": "Add FM System",
                              "checked": false,
                              "expanded": true,
                              "items": null
                          },
                          {
                              "id": "6abdd392-5c63-e311-af5c-22000ab9871c",
                              "sourceId": "00000000-0000-0000-0000-000000000000",
                              "text": "Other",
                              "checked": false,
                              "expanded": true,
                              "items": null
                          }
                        ]
                    },
                    {
                        "id": "6cbdd392-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Physical",
                        "checked": false,
                        "expanded": true,
                        "items": [
                          {
                              "id": "6ebdd392-5c63-e311-af5c-22000ab9871c",
                              "sourceId": "00000000-0000-0000-0000-000000000000",
                              "text": "Wheelchair",
                              "checked": false,
                              "expanded": true,
                              "items": null
                          },
                          {
                              "id": "70bdd392-5c63-e311-af5c-22000ab9871c",
                              "sourceId": "00000000-0000-0000-0000-000000000000",
                              "text": "Motorized Scooter",
                              "checked": false,
                              "expanded": true,
                              "items": null
                          },
                          {
                              "id": "72bdd392-5c63-e311-af5c-22000ab9871c",
                              "sourceId": "00000000-0000-0000-0000-000000000000",
                              "text": "Cane",
                              "checked": false,
                              "expanded": true,
                              "items": null
                          },
                          {
                              "id": "74bdd392-5c63-e311-af5c-22000ab9871c",
                              "sourceId": "00000000-0000-0000-0000-000000000000",
                              "text": "Walker",
                              "checked": false,
                              "expanded": true,
                              "items": null
                          },
                          {
                              "id": "76bdd392-5c63-e311-af5c-22000ab9871c",
                              "sourceId": "00000000-0000-0000-0000-000000000000",
                              "text": "Tiling of the doors",
                              "checked": false,
                              "expanded": true,
                              "items": null
                          },
                          {
                              "id": "78bdd392-5c63-e311-af5c-22000ab9871c",
                              "sourceId": "00000000-0000-0000-0000-000000000000",
                              "text": "Other",
                              "checked": false,
                              "expanded": true,
                              "items": null
                          }
                        ]
                    },
                    {
                        "id": "7abdd392-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Allergies",
                        "checked": false,
                        "expanded": true,
                        "items": [
                          {
                              "id": "7cbdd392-5c63-e311-af5c-22000ab9871c",
                              "sourceId": "00000000-0000-0000-0000-000000000000",
                              "text": "Food Allergy",
                              "checked": false,
                              "expanded": true,
                              "items": [
                                {
                                    "id": "7ebdd392-5c63-e311-af5c-22000ab9871c",
                                    "sourceId": "00000000-0000-0000-0000-000000000000",
                                    "text": "Nuts",
                                    "checked": false,
                                    "expanded": true,
                                    "items": null
                                },
                                {
                                    "id": "80bdd392-5c63-e311-af5c-22000ab9871c",
                                    "sourceId": "00000000-0000-0000-0000-000000000000",
                                    "text": "Gluten",
                                    "checked": false,
                                    "expanded": true,
                                    "items": null
                                },
                                {
                                    "id": "82bdd392-5c63-e311-af5c-22000ab9871c",
                                    "sourceId": "00000000-0000-0000-0000-000000000000",
                                    "text": "Lactose",
                                    "checked": false,
                                    "expanded": true,
                                    "items": null
                                },
                                {
                                    "id": "84bdd392-5c63-e311-af5c-22000ab9871c",
                                    "sourceId": "00000000-0000-0000-0000-000000000000",
                                    "text": "Wheat",
                                    "checked": false,
                                    "expanded": true,
                                    "items": null
                                },
                                {
                                    "id": "86bdd392-5c63-e311-af5c-22000ab9871c",
                                    "sourceId": "00000000-0000-0000-0000-000000000000",
                                    "text": "Other",
                                    "checked": false,
                                    "expanded": true,
                                    "items": null
                                }
                              ]
                          },
                          {
                              "id": "88bdd392-5c63-e311-af5c-22000ab9871c",
                              "sourceId": "00000000-0000-0000-0000-000000000000",
                              "text": "Airborne Allergy",
                              "checked": false,
                              "expanded": true,
                              "items": [
                                {
                                    "id": "8abdd392-5c63-e311-af5c-22000ab9871c",
                                    "sourceId": "00000000-0000-0000-0000-000000000000",
                                    "text": "Nuts",
                                    "checked": false,
                                    "expanded": true,
                                    "items": null
                                },
                                {
                                    "id": "8cbdd392-5c63-e311-af5c-22000ab9871c",
                                    "sourceId": "00000000-0000-0000-0000-000000000000",
                                    "text": "Perfume",
                                    "checked": false,
                                    "expanded": true,
                                    "items": null
                                },
                                {
                                    "id": "8ebdd392-5c63-e311-af5c-22000ab9871c",
                                    "sourceId": "00000000-0000-0000-0000-000000000000",
                                    "text": "Other",
                                    "checked": false,
                                    "expanded": true,
                                    "items": null
                                }
                              ]
                          },
                          {
                              "id": "90bdd392-5c63-e311-af5c-22000ab9871c",
                              "sourceId": "00000000-0000-0000-0000-000000000000",
                              "text": "Epipen",
                              "checked": false,
                              "expanded": true,
                              "items": null
                          },
                          {
                              "id": "92bdd392-5c63-e311-af5c-22000ab9871c",
                              "sourceId": "00000000-0000-0000-0000-000000000000",
                              "text": "Inhaler",
                              "checked": false,
                              "expanded": true,
                              "items": null
                          },
                          {
                              "id": "94bdd392-5c63-e311-af5c-22000ab9871c",
                              "sourceId": "00000000-0000-0000-0000-000000000000",
                              "text": "Other",
                              "checked": false,
                              "expanded": true,
                              "items": null
                          }
                        ]
                    },
                    {
                        "id": "96bdd392-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Assistance",
                        "checked": false,
                        "expanded": true,
                        "items": [
                          {
                              "id": "98bdd392-5c63-e311-af5c-22000ab9871c",
                              "sourceId": "00000000-0000-0000-0000-000000000000",
                              "text": "Support Person",
                              "checked": false,
                              "expanded": true,
                              "items": [
                                {
                                    "id": "9abdd392-5c63-e311-af5c-22000ab9871c",
                                    "sourceId": "00000000-0000-0000-0000-000000000000",
                                    "text": "Accommodation",
                                    "checked": false,
                                    "expanded": true,
                                    "items": null
                                },
                                {
                                    "id": "9cbdd392-5c63-e311-af5c-22000ab9871c",
                                    "sourceId": "00000000-0000-0000-0000-000000000000",
                                    "text": "Separate Bed",
                                    "checked": false,
                                    "expanded": true,
                                    "items": null
                                },
                                {
                                    "id": "9ebdd392-5c63-e311-af5c-22000ab9871c",
                                    "sourceId": "00000000-0000-0000-0000-000000000000",
                                    "text": "Meals",
                                    "checked": false,
                                    "expanded": true,
                                    "items": null
                                },
                                {
                                    "id": "a0bdd392-5c63-e311-af5c-22000ab9871c",
                                    "sourceId": "00000000-0000-0000-0000-000000000000",
                                    "text": "Other",
                                    "checked": false,
                                    "expanded": true,
                                    "items": null
                                }
                              ]
                          },
                          {
                              "id": "a2bdd392-5c63-e311-af5c-22000ab9871c",
                              "sourceId": "00000000-0000-0000-0000-000000000000",
                              "text": "Service Animal",
                              "checked": false,
                              "expanded": true,
                              "items": null
                          },
                          {
                              "id": "a4bdd392-5c63-e311-af5c-22000ab9871c",
                              "sourceId": "00000000-0000-0000-0000-000000000000",
                              "text": "Check-in Assistance",
                              "checked": false,
                              "expanded": true,
                              "items": null
                          },
                          {
                              "id": "a6bdd392-5c63-e311-af5c-22000ab9871c",
                              "sourceId": "00000000-0000-0000-0000-000000000000",
                              "text": "Emergency Evacuation",
                              "checked": false,
                              "expanded": true,
                              "items": null
                          },
                          {
                              "id": "a8bdd392-5c63-e311-af5c-22000ab9871c",
                              "sourceId": "00000000-0000-0000-0000-000000000000",
                              "text": "Accessible Room",
                              "checked": false,
                              "expanded": true,
                              "items": [
                                {
                                    "id": "aabdd392-5c63-e311-af5c-22000ab9871c",
                                    "sourceId": "00000000-0000-0000-0000-000000000000",
                                    "text": "Lower Bed",
                                    "checked": false,
                                    "expanded": true,
                                    "items": null
                                },
                                {
                                    "id": "acbdd392-5c63-e311-af5c-22000ab9871c",
                                    "sourceId": "00000000-0000-0000-0000-000000000000",
                                    "text": "Full accessibility (Roll-in Shower)",
                                    "checked": false,
                                    "expanded": true,
                                    "items": null
                                },
                                {
                                    "id": "aebdd392-5c63-e311-af5c-22000ab9871c",
                                    "sourceId": "00000000-0000-0000-0000-000000000000",
                                    "text": "Partial Accessibility (Grab Bars)",
                                    "checked": false,
                                    "expanded": true,
                                    "items": null
                                },
                                {
                                    "id": "b0bdd392-5c63-e311-af5c-22000ab9871c",
                                    "sourceId": "00000000-0000-0000-0000-000000000000",
                                    "text": "Raised Toilet Seat",
                                    "checked": false,
                                    "expanded": true,
                                    "items": null
                                },
                                {
                                    "id": "b2bdd392-5c63-e311-af5c-22000ab9871c",
                                    "sourceId": "00000000-0000-0000-0000-000000000000",
                                    "text": "Other",
                                    "checked": false,
                                    "expanded": true,
                                    "items": null
                                }
                              ]
                          },
                          {
                              "id": "b4bdd392-5c63-e311-af5c-22000ab9871c",
                              "sourceId": "00000000-0000-0000-0000-000000000000",
                              "text": "Refrigerated Medication",
                              "checked": false,
                              "expanded": true,
                              "items": null
                          },
                          {
                              "id": "b6bdd392-5c63-e311-af5c-22000ab9871c",
                              "sourceId": "00000000-0000-0000-0000-000000000000",
                              "text": "Parking Spot",
                              "checked": false,
                              "expanded": true,
                              "items": null
                          },
                          {
                              "id": "b8bdd392-5c63-e311-af5c-22000ab9871c",
                              "sourceId": "00000000-0000-0000-0000-000000000000",
                              "text": "Other",
                              "checked": false,
                              "expanded": true,
                              "items": null
                          }
                        ]
                    }
                  ]
              },
              {
                  "pr_AttendingAsRolesList": [
                    {
                        "id": "00000000-0000-0000-0000-000000000001",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Voting Role",
                        "checked": false,
                        "expanded": true,
                        "items": [
                          {
                              "id": "607570b0-5c63-e311-af5c-22000ab9871c",
                              "sourceId": "00000000-0000-0000-0000-000000000000",
                              "text": "Delegate",
                              "checked": false,
                              "expanded": true,
                              "items": null
                          },
                          {
                              "id": "627570b0-5c63-e311-af5c-22000ab9871c",
                              "sourceId": "00000000-0000-0000-0000-000000000000",
                              "text": "Alternate",
                              "checked": false,
                              "expanded": true,
                              "items": null
                          },
                          {
                              "id": "647570b0-5c63-e311-af5c-22000ab9871c",
                              "sourceId": "00000000-0000-0000-0000-000000000000",
                              "text": "Paid Alternate",
                              "checked": false,
                              "expanded": true,
                              "items": null
                          },
                          {
                              "id": "687570b0-5c63-e311-af5c-22000ab9871c",
                              "sourceId": "00000000-0000-0000-0000-000000000000",
                              "text": "Executive",
                              "checked": false,
                              "expanded": true,
                              "items": null
                          },
                          {
                              "id": "7c7570b0-5c63-e311-af5c-22000ab9871c",
                              "sourceId": "00000000-0000-0000-0000-000000000000",
                              "text": "President",
                              "checked": false,
                              "expanded": true,
                              "items": null
                          },
                          {
                              "id": "7e7570b0-5c63-e311-af5c-22000ab9871c",
                              "sourceId": "00000000-0000-0000-0000-000000000000",
                              "text": "Additional Local Representative",
                              "checked": false,
                              "expanded": true,
                              "items": null
                          },
                          {
                              "id": "807570b0-5c63-e311-af5c-22000ab9871c",
                              "sourceId": "00000000-0000-0000-0000-000000000000",
                              "text": "DECE President",
                              "checked": false,
                              "expanded": true,
                              "items": null
                          }
                        ]
                    },
                    {
                        "id": "00000000-0000-0000-0000-000000000002",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Non-Voting Role",
                        "checked": false,
                        "expanded": true,
                        "items": [
                          {
                              "id": "667570b0-5c63-e311-af5c-22000ab9871c",
                              "sourceId": "00000000-0000-0000-0000-000000000000",
                              "text": "Observer",
                              "checked": false,
                              "expanded": true,
                              "items": null
                          },
                          {
                              "id": "6a7570b0-5c63-e311-af5c-22000ab9871c",
                              "sourceId": "00000000-0000-0000-0000-000000000000",
                              "text": "Honorary Life Member",
                              "checked": false,
                              "expanded": true,
                              "items": null
                          },
                          {
                              "id": "6c7570b0-5c63-e311-af5c-22000ab9871c",
                              "sourceId": "00000000-0000-0000-0000-000000000000",
                              "text": "Past President",
                              "checked": false,
                              "expanded": true,
                              "items": null
                          },
                          {
                              "id": "6e7570b0-5c63-e311-af5c-22000ab9871c",
                              "sourceId": "00000000-0000-0000-0000-000000000000",
                              "text": "Committee Chair",
                              "checked": false,
                              "expanded": true,
                              "items": null
                          },
                          {
                              "id": "707570b0-5c63-e311-af5c-22000ab9871c",
                              "sourceId": "00000000-0000-0000-0000-000000000000",
                              "text": "Staff",
                              "checked": false,
                              "expanded": true,
                              "items": null
                          },
                          {
                              "id": "727570b0-5c63-e311-af5c-22000ab9871c",
                              "sourceId": "00000000-0000-0000-0000-000000000000",
                              "text": "Committee Member",
                              "checked": false,
                              "expanded": true,
                              "items": null
                          },
                          {
                              "id": "747570b0-5c63-e311-af5c-22000ab9871c",
                              "sourceId": "00000000-0000-0000-0000-000000000000",
                              "text": "Award Recipient",
                              "checked": false,
                              "expanded": true,
                              "items": null
                          },
                          {
                              "id": "767570b0-5c63-e311-af5c-22000ab9871c",
                              "sourceId": "00000000-0000-0000-0000-000000000000",
                              "text": "Affiliate Observer",
                              "checked": false,
                              "expanded": true,
                              "items": null
                          },
                          {
                              "id": "787570b0-5c63-e311-af5c-22000ab9871c",
                              "sourceId": "00000000-0000-0000-0000-000000000000",
                              "text": "Organization Guest",
                              "checked": false,
                              "expanded": true,
                              "items": null
                          },
                          {
                              "id": "7a7570b0-5c63-e311-af5c-22000ab9871c",
                              "sourceId": "00000000-0000-0000-0000-000000000000",
                              "text": "Other  <specify>",
                              "checked": false,
                              "expanded": true,
                              "items": null
                          },
                          {
                              "id": "827570b0-5c63-e311-af5c-22000ab9871c",
                              "sourceId": "00000000-0000-0000-0000-000000000000",
                              "text": "Local Observer",
                              "checked": false,
                              "expanded": true,
                              "items": null
                          },
                          {
                              "id": "847570b0-5c63-e311-af5c-22000ab9871c",
                              "sourceId": "00000000-0000-0000-0000-000000000000",
                              "text": "Individual Observer",
                              "checked": false,
                              "expanded": true,
                              "items": null
                          },
                          {
                              "id": "867570b0-5c63-e311-af5c-22000ab9871c",
                              "sourceId": "00000000-0000-0000-0000-000000000000",
                              "text": "Member of  Designated Group",
                              "checked": false,
                              "expanded": true,
                              "items": null
                          }
                        ]
                    }
                  ]
              }
            ],
            "items": [

            ]
        }
      ]
  },
  {
      "id": "47ab9f86-a0d4-e311-a5e6-000c29675537",
      "sourceId": "00000000-0000-0000-0000-000000000000",
      "text": "Test3",
      "checked": true,
      "index": 2,
      "expanded": false,
      "spriteCssClass": null,
      "mandatory": false,
      "properties": [
        {
            "pr_Label": ""
        },
        {
            "pr_PlaceholderText": ""
        },
        {
            "pr_ReadOnly": ""
        },
        {
            "pr_Required": ""
        },
        {
            "pr_MinLength": ""
        },
        {
            "pr_MaxLength": ""
        },
        {
            "pr_PersonalAccommodationList": [
              {
                  "id": "50bdd392-5c63-e311-af5c-22000ab9871c",
                  "sourceId": "00000000-0000-0000-0000-000000000000",
                  "text": "Visual",
                  "checked": false,
                  "expanded": true,
                  "items": [
                    {
                        "id": "52bdd392-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Larger Fonts",
                        "checked": false,
                        "expanded": true,
                        "items": null
                    },
                    {
                        "id": "54bdd392-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Descriptive Video",
                        "checked": false,
                        "expanded": true,
                        "items": null
                    },
                    {
                        "id": "56bdd392-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Alternative Media",
                        "checked": false,
                        "expanded": true,
                        "items": [
                          {
                              "id": "58bdd392-5c63-e311-af5c-22000ab9871c",
                              "sourceId": "00000000-0000-0000-0000-000000000000",
                              "text": "Brail",
                              "checked": false,
                              "expanded": true,
                              "items": null
                          },
                          {
                              "id": "5abdd392-5c63-e311-af5c-22000ab9871c",
                              "sourceId": "00000000-0000-0000-0000-000000000000",
                              "text": "Other",
                              "checked": false,
                              "expanded": true,
                              "items": null
                          }
                        ]
                    }
                  ]
              },
              {
                  "id": "5cbdd392-5c63-e311-af5c-22000ab9871c",
                  "sourceId": "00000000-0000-0000-0000-000000000000",
                  "text": "Audio",
                  "checked": false,
                  "expanded": true,
                  "items": [
                    {
                        "id": "5ebdd392-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Hearing Aid",
                        "checked": false,
                        "expanded": true,
                        "items": null
                    },
                    {
                        "id": "60bdd392-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Headphones",
                        "checked": false,
                        "expanded": true,
                        "items": null
                    },
                    {
                        "id": "62bdd392-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Sign Language",
                        "checked": false,
                        "expanded": true,
                        "items": null
                    },
                    {
                        "id": "64bdd392-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Video/Subtitling",
                        "checked": false,
                        "expanded": true,
                        "items": null
                    },
                    {
                        "id": "66bdd392-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Close Captioning",
                        "checked": false,
                        "expanded": true,
                        "items": null
                    },
                    {
                        "id": "68bdd392-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Add FM System",
                        "checked": false,
                        "expanded": true,
                        "items": null
                    },
                    {
                        "id": "6abdd392-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Other",
                        "checked": false,
                        "expanded": true,
                        "items": null
                    }
                  ]
              },
              {
                  "id": "6cbdd392-5c63-e311-af5c-22000ab9871c",
                  "sourceId": "00000000-0000-0000-0000-000000000000",
                  "text": "Physical",
                  "checked": false,
                  "expanded": true,
                  "items": [
                    {
                        "id": "6ebdd392-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Wheelchair",
                        "checked": false,
                        "expanded": true,
                        "items": null
                    },
                    {
                        "id": "70bdd392-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Motorized Scooter",
                        "checked": false,
                        "expanded": true,
                        "items": null
                    },
                    {
                        "id": "72bdd392-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Cane",
                        "checked": false,
                        "expanded": true,
                        "items": null
                    },
                    {
                        "id": "74bdd392-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Walker",
                        "checked": false,
                        "expanded": true,
                        "items": null
                    },
                    {
                        "id": "76bdd392-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Tiling of the doors",
                        "checked": false,
                        "expanded": true,
                        "items": null
                    },
                    {
                        "id": "78bdd392-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Other",
                        "checked": false,
                        "expanded": true,
                        "items": null
                    }
                  ]
              },
              {
                  "id": "7abdd392-5c63-e311-af5c-22000ab9871c",
                  "sourceId": "00000000-0000-0000-0000-000000000000",
                  "text": "Allergies",
                  "checked": false,
                  "expanded": true,
                  "items": [
                    {
                        "id": "7cbdd392-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Food Allergy",
                        "checked": false,
                        "expanded": true,
                        "items": [
                          {
                              "id": "7ebdd392-5c63-e311-af5c-22000ab9871c",
                              "sourceId": "00000000-0000-0000-0000-000000000000",
                              "text": "Nuts",
                              "checked": false,
                              "expanded": true,
                              "items": null
                          },
                          {
                              "id": "80bdd392-5c63-e311-af5c-22000ab9871c",
                              "sourceId": "00000000-0000-0000-0000-000000000000",
                              "text": "Gluten",
                              "checked": false,
                              "expanded": true,
                              "items": null
                          },
                          {
                              "id": "82bdd392-5c63-e311-af5c-22000ab9871c",
                              "sourceId": "00000000-0000-0000-0000-000000000000",
                              "text": "Lactose",
                              "checked": false,
                              "expanded": true,
                              "items": null
                          },
                          {
                              "id": "84bdd392-5c63-e311-af5c-22000ab9871c",
                              "sourceId": "00000000-0000-0000-0000-000000000000",
                              "text": "Wheat",
                              "checked": false,
                              "expanded": true,
                              "items": null
                          },
                          {
                              "id": "86bdd392-5c63-e311-af5c-22000ab9871c",
                              "sourceId": "00000000-0000-0000-0000-000000000000",
                              "text": "Other",
                              "checked": false,
                              "expanded": true,
                              "items": null
                          }
                        ]
                    },
                    {
                        "id": "88bdd392-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Airborne Allergy",
                        "checked": false,
                        "expanded": true,
                        "items": [
                          {
                              "id": "8abdd392-5c63-e311-af5c-22000ab9871c",
                              "sourceId": "00000000-0000-0000-0000-000000000000",
                              "text": "Nuts",
                              "checked": false,
                              "expanded": true,
                              "items": null
                          },
                          {
                              "id": "8cbdd392-5c63-e311-af5c-22000ab9871c",
                              "sourceId": "00000000-0000-0000-0000-000000000000",
                              "text": "Perfume",
                              "checked": false,
                              "expanded": true,
                              "items": null
                          },
                          {
                              "id": "8ebdd392-5c63-e311-af5c-22000ab9871c",
                              "sourceId": "00000000-0000-0000-0000-000000000000",
                              "text": "Other",
                              "checked": false,
                              "expanded": true,
                              "items": null
                          }
                        ]
                    },
                    {
                        "id": "90bdd392-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Epipen",
                        "checked": false,
                        "expanded": true,
                        "items": null
                    },
                    {
                        "id": "92bdd392-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Inhaler",
                        "checked": false,
                        "expanded": true,
                        "items": null
                    },
                    {
                        "id": "94bdd392-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Other",
                        "checked": false,
                        "expanded": true,
                        "items": null
                    }
                  ]
              },
              {
                  "id": "96bdd392-5c63-e311-af5c-22000ab9871c",
                  "sourceId": "00000000-0000-0000-0000-000000000000",
                  "text": "Assistance",
                  "checked": false,
                  "expanded": true,
                  "items": [
                    {
                        "id": "98bdd392-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Support Person",
                        "checked": false,
                        "expanded": true,
                        "items": [
                          {
                              "id": "9abdd392-5c63-e311-af5c-22000ab9871c",
                              "sourceId": "00000000-0000-0000-0000-000000000000",
                              "text": "Accommodation",
                              "checked": false,
                              "expanded": true,
                              "items": null
                          },
                          {
                              "id": "9cbdd392-5c63-e311-af5c-22000ab9871c",
                              "sourceId": "00000000-0000-0000-0000-000000000000",
                              "text": "Separate Bed",
                              "checked": false,
                              "expanded": true,
                              "items": null
                          },
                          {
                              "id": "9ebdd392-5c63-e311-af5c-22000ab9871c",
                              "sourceId": "00000000-0000-0000-0000-000000000000",
                              "text": "Meals",
                              "checked": false,
                              "expanded": true,
                              "items": null
                          },
                          {
                              "id": "a0bdd392-5c63-e311-af5c-22000ab9871c",
                              "sourceId": "00000000-0000-0000-0000-000000000000",
                              "text": "Other",
                              "checked": false,
                              "expanded": true,
                              "items": null
                          }
                        ]
                    },
                    {
                        "id": "a2bdd392-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Service Animal",
                        "checked": false,
                        "expanded": true,
                        "items": null
                    },
                    {
                        "id": "a4bdd392-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Check-in Assistance",
                        "checked": false,
                        "expanded": true,
                        "items": null
                    },
                    {
                        "id": "a6bdd392-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Emergency Evacuation",
                        "checked": false,
                        "expanded": true,
                        "items": null
                    },
                    {
                        "id": "a8bdd392-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Accessible Room",
                        "checked": false,
                        "expanded": true,
                        "items": [
                          {
                              "id": "aabdd392-5c63-e311-af5c-22000ab9871c",
                              "sourceId": "00000000-0000-0000-0000-000000000000",
                              "text": "Lower Bed",
                              "checked": false,
                              "expanded": true,
                              "items": null
                          },
                          {
                              "id": "acbdd392-5c63-e311-af5c-22000ab9871c",
                              "sourceId": "00000000-0000-0000-0000-000000000000",
                              "text": "Full accessibility (Roll-in Shower)",
                              "checked": false,
                              "expanded": true,
                              "items": null
                          },
                          {
                              "id": "aebdd392-5c63-e311-af5c-22000ab9871c",
                              "sourceId": "00000000-0000-0000-0000-000000000000",
                              "text": "Partial Accessibility (Grab Bars)",
                              "checked": false,
                              "expanded": true,
                              "items": null
                          },
                          {
                              "id": "b0bdd392-5c63-e311-af5c-22000ab9871c",
                              "sourceId": "00000000-0000-0000-0000-000000000000",
                              "text": "Raised Toilet Seat",
                              "checked": false,
                              "expanded": true,
                              "items": null
                          },
                          {
                              "id": "b2bdd392-5c63-e311-af5c-22000ab9871c",
                              "sourceId": "00000000-0000-0000-0000-000000000000",
                              "text": "Other",
                              "checked": false,
                              "expanded": true,
                              "items": null
                          }
                        ]
                    },
                    {
                        "id": "b4bdd392-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Refrigerated Medication",
                        "checked": false,
                        "expanded": true,
                        "items": null
                    },
                    {
                        "id": "b6bdd392-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Parking Spot",
                        "checked": false,
                        "expanded": true,
                        "items": null
                    },
                    {
                        "id": "b8bdd392-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Other",
                        "checked": false,
                        "expanded": true,
                        "items": null
                    }
                  ]
              }
            ]
        },
        {
            "pr_AttendingAsRolesList": [
              {
                  "id": "00000000-0000-0000-0000-000000000001",
                  "sourceId": "00000000-0000-0000-0000-000000000000",
                  "text": "Voting Role",
                  "checked": false,
                  "expanded": true,
                  "items": [
                    {
                        "id": "607570b0-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Delegate",
                        "checked": false,
                        "expanded": true,
                        "items": null
                    },
                    {
                        "id": "627570b0-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Alternate",
                        "checked": false,
                        "expanded": true,
                        "items": null
                    },
                    {
                        "id": "647570b0-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Paid Alternate",
                        "checked": false,
                        "expanded": true,
                        "items": null
                    },
                    {
                        "id": "687570b0-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Executive",
                        "checked": false,
                        "expanded": true,
                        "items": null
                    },
                    {
                        "id": "7c7570b0-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "President",
                        "checked": false,
                        "expanded": true,
                        "items": null
                    },
                    {
                        "id": "7e7570b0-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Additional Local Representative",
                        "checked": false,
                        "expanded": true,
                        "items": null
                    },
                    {
                        "id": "807570b0-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "DECE President",
                        "checked": false,
                        "expanded": true,
                        "items": null
                    }
                  ]
              },
              {
                  "id": "00000000-0000-0000-0000-000000000002",
                  "sourceId": "00000000-0000-0000-0000-000000000000",
                  "text": "Non-Voting Role",
                  "checked": false,
                  "expanded": true,
                  "items": [
                    {
                        "id": "667570b0-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Observer",
                        "checked": false,
                        "expanded": true,
                        "items": null
                    },
                    {
                        "id": "6a7570b0-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Honorary Life Member",
                        "checked": false,
                        "expanded": true,
                        "items": null
                    },
                    {
                        "id": "6c7570b0-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Past President",
                        "checked": false,
                        "expanded": true,
                        "items": null
                    },
                    {
                        "id": "6e7570b0-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Committee Chair",
                        "checked": false,
                        "expanded": true,
                        "items": null
                    },
                    {
                        "id": "707570b0-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Staff",
                        "checked": false,
                        "expanded": true,
                        "items": null
                    },
                    {
                        "id": "727570b0-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Committee Member",
                        "checked": false,
                        "expanded": true,
                        "items": null
                    },
                    {
                        "id": "747570b0-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Award Recipient",
                        "checked": false,
                        "expanded": true,
                        "items": null
                    },
                    {
                        "id": "767570b0-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Affiliate Observer",
                        "checked": false,
                        "expanded": true,
                        "items": null
                    },
                    {
                        "id": "787570b0-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Organization Guest",
                        "checked": false,
                        "expanded": true,
                        "items": null
                    },
                    {
                        "id": "7a7570b0-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Other  <specify>",
                        "checked": false,
                        "expanded": true,
                        "items": null
                    },
                    {
                        "id": "827570b0-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Local Observer",
                        "checked": false,
                        "expanded": true,
                        "items": null
                    },
                    {
                        "id": "847570b0-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Individual Observer",
                        "checked": false,
                        "expanded": true,
                        "items": null
                    },
                    {
                        "id": "867570b0-5c63-e311-af5c-22000ab9871c",
                        "sourceId": "00000000-0000-0000-0000-000000000000",
                        "text": "Member of  Designated Group",
                        "checked": false,
                        "expanded": true,
                        "items": null
                    }
                  ]
              }
            ]
        }
      ],
      "items": [

      ]
  }
];
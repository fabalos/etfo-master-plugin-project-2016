function SetVisibilityNewPresenterButton(selectedControl)
{
	if(selectedControl._element.id == 'Presenters' || selectedControl._element.id == 'PresentersGrid') return true;
	return false;
}

function onSave()
{
    var course = Xrm.Page.getAttribute("oems_course").getValue();
	Xrm.Page.getAttribute("oems_event").setValue(course);
}

function OpenPresenterForm()
{
    var parameters = {};
	parameters['oems_participanttype'] = 347780001;
	
    /*if(Xrm.Page.getAttribute("oems_courseterm") != null){
		parameters["oems_event"] = Xrm.Page.getAttribute("oems_courseterm").getValue()[0].id;
		parameters["oems_eventname"] = Xrm.Page.getAttribute("oems_courseterm").getValue()[0].name;
	}*/
	
	parameters["oems_event"] = Xrm.Page.data.entity.getId();
	parameters["oems_eventname"] = Xrm.Page.getAttribute("oems_eventname").getValue();
	
	Xrm.Utility.openEntityForm('oems_eventattendee', null, parameters);
}

function onLoad()
{
	if(Xrm.Page.ui.getFormType() == 1 && Xrm.Page.getAttribute("oems_participanttype").getValue() == 347780001)
	{
		Xrm.Page.getControl("oems_eventregistration").setDisabled(false);
		Xrm.Page.getControl("oems_account").setDisabled(false);
        //Xrm.Page.getAttribute("oems_copresenter").controls.get(0).setVisible(true);
        Xrm.Page.getAttribute("oems_copresenter").setValue(1);
                
	}else
	{
		Xrm.Page.getControl("oems_account").setDisabled(true);
		Xrm.Page.getControl("oems_eventregistration").setDisabled(true);
        //Xrm.Page.getAttribute("oems_copresenter").controls.get(0).setVisible(false);
	}
}

function onChangeAccount()
{
	var lookupObject = Xrm.Page.getAttribute("oems_account");

	if(lookupObject != null){
		   var idAccount = lookupObject.getValue();
		   Xrm.Page.getAttribute("oems_attendeename").setValue(idAccount[0].name);
	}else{
			Xrm.Page.getAttribute("oems_attendeename").setValue("");
	}
}
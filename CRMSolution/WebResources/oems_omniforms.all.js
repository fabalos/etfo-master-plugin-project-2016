//  THIS FILE IS GENERATED DO NO MODIFY 
//   Thu Oct 13 2016 16:03:21 GMT-0500 (SA Pacific Standard Time)
var omniware = omniware || {};

$(function () {

    /* builds fully formed json object using form data as input
      form data represented as name value pairs  <(fullpath of the lement), value >

     Example:
     input:
     [
     {
     "name": "/panel5/simpleChildcareSection/advChildcareSection#0/drinkersName",
     "value": "row1"
     },
     {
     "name": "/panel5/simpleChildcareSection/advChildcareSection#0/foodPrefences",
     "value": "milk-guid"
     }
     ]
     output:
     {
     "panel5": {
     "simpleChildcareSection": [
     {
     "drinkersName": "row1"
     },
     {
     "foodPrefences": "milk-guid"
     }
     ]
     }
     }
     */
    omniware.formDataToObject = function (data) {
        var result = {};
        // sorting key values by key(path) makes a more sensible top-down build process.
        var sorted = _.sortBy(data, function (pair) {
            return pair.name
        });

        for (var i = 0, length = sorted.length; i < length; i++) {
            var valuePair = sorted[i];
            addTo(result, valuePair.name, valuePair.value)
        }

        return result;
    }


    // add property to the object at given path, create nested object/arrays as needed
    function addTo(object, path, value) {

        console.log("addTo - Adding "+  path + " <- " + JSON.stringify(value) + " \t to object: " + JSON.stringify(object));

        var pathSegments = path.split('/');
        pathSegments.shift(); // split [0] is empty since paths starts with '/'
        var propertyName = pathSegments[0];

        var elementIsContainer = pathSegments.length > 1;
        var elementIsArray = elementIsContainer && pathSegments[1].indexOf('#') > 0; // array has a child:  e.g ../array/child#1

        if (!elementIsContainer) {

            // simply add propertyName object or array if current container object is an array
            if (object instanceof  Array) {
                object.push(value);
            } else {
                object[propertyName] = value;
            }

            return object;

        } else {

            var tailPath = path.substring(1 + propertyName.length);

            if (object instanceof  Array) {

                var idx = propertyName.substring(propertyName.indexOf('#')+1);

                var arrayElement = object[idx] || {};   // use existing or create new element in array - arrays of arrays are not supported
                object[idx] = arrayElement;

                // now continue adding to this array element
                addTo(arrayElement, tailPath, value);
                return object;

            } else {
                // create container if one does not exists yet
                if (object[propertyName] == undefined) {
                    object[propertyName] = elementIsArray ? [] : {}
                }
                // continue adding to this container
                return addTo(object[propertyName], tailPath, value);
            }

        }

    }


})
//  THIS FILE IS GENERATED DO NO MODIFY 
//   Thu Oct 13 2016 16:03:21 GMT-0500 (SA Pacific Standard Time)
$(function () {
/*
* jQuery File Download Plugin v1.4.2 
*
* http://www.johnculviner.com
*
* Copyright (c) 2013 - John Culviner
*
* Licensed under the MIT license:
*   http://www.opensource.org/licenses/mit-license.php
*
* !!!!NOTE!!!!
* You must also write a cookie in conjunction with using this plugin as mentioned in the orignal post:
* http://johnculviner.com/jquery-file-download-plugin-for-ajax-like-feature-rich-file-downloads/
* !!!!NOTE!!!!
*/

(function($, window){
	// i'll just put them here to get evaluated on script load
	var htmlSpecialCharsRegEx = /[<>&\r\n"']/gm;
	var htmlSpecialCharsPlaceHolders = {
				'<': 'lt;',
				'>': 'gt;',
				'&': 'amp;',
				'\r': "#13;",
				'\n': "#10;",
				'"': 'quot;',
				"'": '#39;' /*single quotes just to be safe, IE8 doesn't support &apos;, so use &#39; instead */
	};

$.extend({
    //
    //$.fileDownload('/path/to/url/', options)
    //  see directly below for possible 'options'
    fileDownload: function (fileUrl, options) {

        //provide some reasonable defaults to any unspecified options below
        var settings = $.extend({

            //
            //Requires jQuery UI: provide a message to display to the user when the file download is being prepared before the browser's dialog appears
            //
            preparingMessageHtml: null,

            //
            //Requires jQuery UI: provide a message to display to the user when a file download fails
            //
            failMessageHtml: null,

            //
            //the stock android browser straight up doesn't support file downloads initiated by a non GET: http://code.google.com/p/android/issues/detail?id=1780
            //specify a message here to display if a user tries with an android browser
            //if jQuery UI is installed this will be a dialog, otherwise it will be an alert
            //
            androidPostUnsupportedMessageHtml: "Unfortunately your Android browser doesn't support this type of file download. Please try again with a different browser.",

            //
            //Requires jQuery UI: options to pass into jQuery UI Dialog
            //
            dialogOptions: { modal: true },

            //
            //a function to call while the dowload is being prepared before the browser's dialog appears
            //Args:
            //  url - the original url attempted
            //
            prepareCallback: function (url) { },

            //
            //a function to call after a file download dialog/ribbon has appeared
            //Args:
            //  url - the original url attempted
            //
            successCallback: function (url) { },

            //
            //a function to call after a file download dialog/ribbon has appeared
            //Args:
            //  responseHtml    - the html that came back in response to the file download. this won't necessarily come back depending on the browser.
            //                      in less than IE9 a cross domain error occurs because 500+ errors cause a cross domain issue due to IE subbing out the
            //                      server's error message with a "helpful" IE built in message
            //  url             - the original url attempted
            //
            failCallback: function (responseHtml, url) { },

            //
            // the HTTP method to use. Defaults to "GET".
            //
            httpMethod: "GET",

            //
            // if specified will perform a "httpMethod" request to the specified 'fileUrl' using the specified data.
            // data must be an object (which will be $.param serialized) or already a key=value param string
            //
            data: null,

            //
            //a period in milliseconds to poll to determine if a successful file download has occured or not
            //
            checkInterval: 100,

            //
            //the cookie name to indicate if a file download has occured
            //
            cookieName: "fileDownload",

            //
            //the cookie value for the above name to indicate that a file download has occured
            //
            cookieValue: "true",

            //
            //the cookie path for above name value pair
            //
            cookiePath: "/",

            //
            //if specified it will be used when attempting to clear the above name value pair
            //useful for when downloads are being served on a subdomain (e.g. downloads.example.com)
            //	
            cookieDomain: null,

            //
            //the title for the popup second window as a download is processing in the case of a mobile browser
            //
            popupWindowTitle: "Initiating file download...",

            //
            //Functionality to encode HTML entities for a POST, need this if data is an object with properties whose values contains strings with quotation marks.
            //HTML entity encoding is done by replacing all &,<,>,',",\r,\n characters.
            //Note that some browsers will POST the string htmlentity-encoded whilst others will decode it before POSTing.
            //It is recommended that on the server, htmlentity decoding is done irrespective.
            //
            encodeHTMLEntities: true
            
        }, options);

        var deferred = new $.Deferred();

        //Setup mobile browser detection: Partial credit: http://detectmobilebrowser.com/
        var userAgent = (navigator.userAgent || navigator.vendor || window.opera).toLowerCase();

        var isIos;                  //has full support of features in iOS 4.0+, uses a new window to accomplish this.
        var isAndroid;              //has full support of GET features in 4.0+ by using a new window. Non-GET is completely unsupported by the browser. See above for specifying a message.
        var isOtherMobileBrowser;   //there is no way to reliably guess here so all other mobile devices will GET and POST to the current window.

        if (/ip(ad|hone|od)/.test(userAgent)) {

            isIos = true;

        } else if (userAgent.indexOf('android') !== -1) {

            isAndroid = true;

        } else {

            isOtherMobileBrowser = /avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|playbook|silk|iemobile|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(userAgent) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|e\-|e\/|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(di|rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|xda(\-|2|g)|yas\-|your|zeto|zte\-/i.test(userAgent.substr(0, 4));

        }

        var httpMethodUpper = settings.httpMethod.toUpperCase();

        if (isAndroid && httpMethodUpper !== "GET") {
            //the stock android browser straight up doesn't support file downloads initiated by non GET requests: http://code.google.com/p/android/issues/detail?id=1780

            if ($().dialog) {
                $("<div>").html(settings.androidPostUnsupportedMessageHtml).dialog(settings.dialogOptions);
            } else {
                alert(settings.androidPostUnsupportedMessageHtml);
            }

            return deferred.reject();
        }

        var $preparingDialog = null;

        var internalCallbacks = {

            onPrepare: function (url) {

                //wire up a jquery dialog to display the preparing message if specified
                if (settings.preparingMessageHtml) {

                    $preparingDialog = $("<div>").html(settings.preparingMessageHtml).dialog(settings.dialogOptions);

                } else if (settings.prepareCallback) {

                    settings.prepareCallback(url);

                }

            },

            onSuccess: function (url) {

                //remove the perparing message if it was specified
                if ($preparingDialog) {
                    $preparingDialog.dialog('close');
                };

                settings.successCallback(url);

                deferred.resolve(url);
            },

            onFail: function (responseHtml, url) {

                //remove the perparing message if it was specified
                if ($preparingDialog) {
                    $preparingDialog.dialog('close');
                };

                //wire up a jquery dialog to display the fail message if specified
                if (settings.failMessageHtml) {
                    $("<div>").html(settings.failMessageHtml).dialog(settings.dialogOptions);
                }

                settings.failCallback(responseHtml, url);
                
                deferred.reject(responseHtml, url);
            }
        };

        internalCallbacks.onPrepare(fileUrl);

        //make settings.data a param string if it exists and isn't already
        if (settings.data !== null && typeof settings.data !== "string") {
            settings.data = $.param(settings.data);
        }


        var $iframe,
            downloadWindow,
            formDoc,
            $form;

        if (httpMethodUpper === "GET") {

            if (settings.data !== null) {
                //need to merge any fileUrl params with the data object

                var qsStart = fileUrl.indexOf('?');

                if (qsStart !== -1) {
                    //we have a querystring in the url

                    if (fileUrl.substring(fileUrl.length - 1) !== "&") {
                        fileUrl = fileUrl + "&";
                    }
                } else {

                    fileUrl = fileUrl + "?";
                }

                fileUrl = fileUrl + settings.data;
            }

            if (isIos || isAndroid) {

                downloadWindow = window.open(fileUrl);
                downloadWindow.document.title = settings.popupWindowTitle;
                window.focus();

            } else if (isOtherMobileBrowser) {

                window.location(fileUrl);

            } else {

                //create a temporary iframe that is used to request the fileUrl as a GET request
                $iframe = $("<iframe>")
                    .hide()
                    .prop("src", fileUrl)
                    .appendTo("body");
            }

        } else {

            var formInnerHtml = "";

            if (settings.data !== null) {

                $.each(settings.data.replace(/\+/g, ' ').split("&"), function () {

                    var kvp = this.split("=");

                    var key = settings.encodeHTMLEntities ? htmlSpecialCharsEntityEncode(decodeURIComponent(kvp[0])) : decodeURIComponent(kvp[0]);
                    if (key) {
                        var value = settings.encodeHTMLEntities ? htmlSpecialCharsEntityEncode(decodeURIComponent(kvp[1])) : decodeURIComponent(kvp[1]);
                    formInnerHtml += '<input type="hidden" name="' + key + '" value="' + value + '" />';
                    }
                });
            }

            if (isOtherMobileBrowser) {

                $form = $("<form>").appendTo("body");
                $form.hide()
                    .prop('method', settings.httpMethod)
                    .prop('action', fileUrl)
                    .html(formInnerHtml);

            } else {

                if (isIos) {

                    downloadWindow = window.open("about:blank");
                    downloadWindow.document.title = settings.popupWindowTitle;
                    formDoc = downloadWindow.document;
                    window.focus();

                } else {

                    $iframe = $("<iframe style='display: none' src='about:blank'></iframe>").appendTo("body");
                    formDoc = getiframeDocument($iframe);
                }

                formDoc.write("<html><head></head><body><form method='" + settings.httpMethod + "' action='" + fileUrl + "'>" + formInnerHtml + "</form>" + settings.popupWindowTitle + "</body></html>");
                $form = $(formDoc).find('form');
            }

            $form.submit();
        }


        //check if the file download has completed every checkInterval ms
        setTimeout(checkFileDownloadComplete, settings.checkInterval);


        function checkFileDownloadComplete() {
            //has the cookie been written due to a file download occuring?
            if (document.cookie.indexOf(settings.cookieName + "=" + settings.cookieValue) != -1) {

                //execute specified callback
                internalCallbacks.onSuccess(fileUrl);

                //remove cookie
                var cookieData = settings.cookieName + "=; path=" + settings.cookiePath + "; expires=" + new Date(0).toUTCString() + ";";
                if (settings.cookieDomain) cookieData += " domain=" + settings.cookieDomain + ";";
                document.cookie = cookieData;

                //remove iframe
                cleanUp(false);

                return;
            }

            //has an error occured?
            //if neither containers exist below then the file download is occuring on the current window
            if (downloadWindow || $iframe) {

                //has an error occured?
                try {

                    var formDoc = downloadWindow ? downloadWindow.document : getiframeDocument($iframe);

                    if (formDoc && formDoc.body != null && formDoc.body.innerHTML.length) {

                        var isFailure = true;

                        if ($form && $form.length) {
                            var $contents = $(formDoc.body).contents().first();

                            try {
                                if ($contents.length && $contents[0] === $form[0]) {
                                    isFailure = false;
                                }
                            } catch (e) {
                                if (e && e.number == -2146828218) {
                                    // IE 8-10 throw a permission denied after the form reloads on the "$contents[0] === $form[0]" comparison
                                    isFailure = true;
                                } else {
                                    throw e;
                                }
                            } 
                        }

                        if (isFailure) {
                            // IE 8-10 don't always have the full content available right away, they need a litle bit to finish
                            setTimeout(function () {
                                internalCallbacks.onFail(formDoc.body.innerHTML, fileUrl);
                                cleanUp(true);
                            }, 100);
                            
                            return;
                        }
                    }
                }
                catch (err) {

                    //500 error less than IE9
                    internalCallbacks.onFail('', fileUrl);

                    cleanUp(true);

                    return;
                }
            }


            //keep checking...
            setTimeout(checkFileDownloadComplete, settings.checkInterval);
        }

        //gets an iframes document in a cross browser compatible manner
        function getiframeDocument($iframe) {
            var iframeDoc = $iframe[0].contentWindow || $iframe[0].contentDocument;
            if (iframeDoc.document) {
                iframeDoc = iframeDoc.document;
            }
            return iframeDoc;
        }

        function cleanUp(isFailure) {

            setTimeout(function() {

                if (downloadWindow) {

                    if (isAndroid) {
                        downloadWindow.close();
                    }

                    if (isIos) {
                        if (downloadWindow.focus) {
                            downloadWindow.focus(); //ios safari bug doesn't allow a window to be closed unless it is focused
                            if (isFailure) {
                                downloadWindow.close();
                            }
                        }
                    }
                }
                
                //iframe cleanup appears to randomly cause the download to fail
                //not doing it seems better than failure...
                //if ($iframe) {
                //    $iframe.remove();
                //}

            }, 0);
        }


        function htmlSpecialCharsEntityEncode(str) {
            return str.replace(htmlSpecialCharsRegEx, function(match) {
                return '&' + htmlSpecialCharsPlaceHolders[match];
        	});
        }

        return deferred.promise();
    }
});

})(jQuery, this);
});
//  THIS FILE IS GENERATED DO NO MODIFY 
//   Thu Oct 13 2016 16:03:21 GMT-0500 (SA Pacific Standard Time)
var omniware = omniware || {};

$(function(){

    // Merges object of values into the form defintion
    /* Example:

       data:
       {
                "innerHtml" : "HI THERE"
       }

      formSchema:
       {
           "children": [

               {
                   "name": "innerHtml",
                   "type": "htmlinput",
                   "value": "",
                   "options": {
                       "label": "Html"
                   }
               }
           ]
       }


       result:
       {
          "children": [

              {
                  "name": "innerHtml",
                  "type": "htmlinput",
                  "value": "HI THERE",
                  "options": {
                      "label": "Html"
                  }
              }
          ]
      }



    data:
            {
                      "links": [
                          {
                              "url": "http://staging2.omniwareservice.com/bookclubs.jpg",
                              "captionHtml" :  "<h3>First slide label (Optional)</h3><p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p"
                          },
                          {
                              "url": "http://staging2.omniwareservice.com/andstillwerise2014.jpg",
                              "captionHtml" :  "<h3>2nd slide label (Optional)</h3><p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p"
                          },
                          {
                              "url": "http://staging2.omniwareservice.com/lifepreservers2014.jpg",
                              "captionHtml" :  "<h3>3rd slide label (Optional)</h3><p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p"
                          }
                      ]
                }

    formSchema:
    {
        "children": [

            {
                "name": "links",
                "type": "repeatableSection",

                "options": {
                "newRowTemplate":
                {
                    "name": "childSection",
                    "type": "section",

                    "options": {
                    },

                    "children": [
                        {
                            "name": "url",
                            "type": "urlpicker",

                            "value": "",

                            "options": {
                                "label": "Url",
                                "placeholderText": "Please type here"
                            }
                        },
                        {
                            "name": "captionHtml",
							"type": "htmlinput",

                            "value": "",

                            "options": {
                                "label": "Caption Html"
                            }
                        }
                    ]
                }
               "children": [
                ]
            }
        ]
    }

     */

    omniware.mergeValuesIntoForm = function (formSchema, data) {

        console.log("Merging ")
        console.log("\t data: " + JSON.stringify(data));
        console.log("\t into: "+ JSON.stringify(formSchema) );

        for (var propertyName in data) {
              var value = data[propertyName];

              var childSchema = _.find(formSchema.children, function (item) {
                  if (item.name == propertyName) return item;
              });

            if( isArray(value) ) {

                  if( !childSchema.options || !childSchema.options.newRowTemplate)  throw "Unsupported schema.";

                  // construct array one by one by filling in template
                  childSchema.children = [];
                  for(var i=0, length = value.length; i < length; i++){
                       var template = JSON.parse(JSON.stringify(childSchema.options.newRowTemplate)); // deep clone
                       omniware.mergeValuesIntoForm(template, value[i]);
                       childSchema.children.push( template);
                   }
               //todo ..
			  // else if subschema is section (or other container)
			  // for childProperty in propeties of  value merge values

              } else {
                  childSchema.value = value
              }
        }

        console.log( "\t rslt= " + JSON.stringify(formSchema) );

        return formSchema;
    }

    // objects from different frames do not share prototype so frame instanceof Array would fail
    function isArray(value) {
        return  JSON.parse(JSON.stringify(value)) instanceof Array;
    }


})
//  THIS FILE IS GENERATED DO NO MODIFY 
//   Thu Oct 13 2016 16:03:21 GMT-0500 (SA Pacific Standard Time)
// ------------------------------------
// ominware custom form generation code
// ------------------------------------
$(function () {
    if (typeof (_) == "undefined") alert('include underscore.js in the page: //cdnjs.cloudflare.com/ajax/libs/underscore.js/1.6.0/underscore-min.js');
	if (!String.prototype.format) 	{	  String.prototype.format = function() 	  {		var args = arguments;		return this.replace(/{(\d+)}/g, function(match, number) { 		  return typeof args[number] != 'undefined'			? args[number]			: match		  ;		});	  };	}	
    var schema;
    var formOptions = {};
    var form$ = $('#thisDynamicForm');
    //var bytesInMb = 1048576;
    var bytesInMb = 1024000;
    var maxFileSizeMb = bytesInMb * 5;	var onLoadDataSuccess;
    var formMessages =
    {
        documents:
        {
            noData: 'Please select a document from your computer and enter a description.',
            noDescription: 'Please enter a description for the document.',
            noDocument: 'Please select a document from your computer.',
            maxSize: 'The document you selected exceeds the maximum of 5MB allowed. Please select another one.',			minimumDocuments: 'Minimum {0} doc(s) required'
        },
		attachments:
		{
			downloadError: 'There was an error downloading the document. Please try again',
			requiredField: 'The following fields are required:',
		},
		other:
        {
            fieldRequired: '* At least one item is required',
            caucusOverlapping: '* You cannot have overlapping caucus schedules'
        }
    };
	var eventRegistrationStatus =
	{
		Initial: 347780004,
		Created: 1,
		Cancelled: 347780007,//2,
		Withdrawn: 347780008,//347780000,
		Sumbitted: 347780001,
		Rejected: 347780006,//347780002,
		Approved: 347780003
	};	var callbacks = { }

	var preferredSchoolBoardIds,preferredSchoolIds,preferredLocalIds;	var utl = {}; //container for utility functions which are later added to schema (model)

    //Shows size in MBytes
    ko.bindingHandlers.documentSize =
	{
	    init: function (element, valueAccessor, allBindingsAccessor) {
	        var $el = $(element);
	        var value = ko.utils.unwrapObservable(valueAccessor());

	        if (value != null) {
	            if (value != "") {
	                var size = parseInt(value) / bytesInMb;
	                $el.text(size.toFixed(2));
	            }
	        }
	    }
	};
    
    // Global school / schoolboard / local lists
    // full lists:
    utl.schoolBoardList = ko.observableArray();
    utl.schoolList = ko.observableArray();
    utl.localList = ko.observableArray();
    // Filter list
    utl.preparedBoardSchoolList = ko.observableArray();

    // Personal info
    // dynamically filtered preprocessed lists
    utl.filteredSchoolList = ko.observableArray();
    utl.filteredLocalList = ko.observableArray();
    utl.schoolboard = ko.observable('');
    utl.school = ko.observable('');
    utl.local = ko.observable('');
    utl.previewSchoolName = ko.observable('');    utl.previewSchoolCity = ko.observable('');    utl.previewSchoolPhone = ko.observable('');    utl.previewSchoolFax = ko.observable('');	utl.previewSchoolEmail = ko.observable('');    utl.previewLocalName = ko.observable('');
    // CoInstructor 
    utl.filteredCoSchoolList = ko.observableArray();
    utl.filteredCoLocalList = ko.observableArray();
    utl.coSchoolboard = ko.observable('');
    utl.coSchool = ko.observable('');
    utl.coLocal = ko.observable('');    utl.previewCoSchoolName = ko.observable('');    utl.previewCoSchoolCity = ko.observable('');    utl.previewCoSchoolPhone = ko.observable('');    utl.previewCoSchoolFax = ko.observable('');	utl.previewCoSchoolEmail = ko.observable('');    utl.previewCoLocalName = ko.observable('');	    // Release Time    utl.filteredRtSchoolList = ko.observableArray();    utl.filteredRtLocalList = ko.observableArray();    utl.rtSchoolboard = ko.observable('');    utl.rtSchool = ko.observable('');    utl.rtLocal = ko.observable('');
    utl.previewRtSchoolName = ko.observable('');    utl.previewRtSchoolCity = ko.observable('');    utl.previewRtSchoolPhone = ko.observable('');    utl.previewRtSchoolFax = ko.observable('');	utl.previewRtSchoolEmail = ko.observable('');    utl.previewRtLocalName = ko.observable('');
	// derived display only fields	
    utl.previewDelegationLimit = ko.observable('');
    utl.printMode = ko.observable(true);

    // do the prep work
    console.log('iframe ready');
    //window.omniutl = utl; //debug

    // use string KO template engine
    omniware.configureKoStringTemplateEngine(ko);  // see stringTemplateEngine.js
    omniware.loadKoTemplates();                    // this function is generated based on templates/*.html
	
	// ------------------------------------------------------------------------
    //  --- Form API  - public functions are exposed via global omniware object
	// ------------------------------------------------------------------------	
    // render form
    omniware.renderForm = function (_formsSchema, _formsOptions, _formsMessages, _eventRegistrationsStatus, _callbacks) 	{
        schema = _formsSchema;
        formOptions = _formsOptions || {};
        schema.utl = utl;
        formMessages = _.extend(formMessages, _formsMessages);
		eventRegistrationStatus = _.extend(eventRegistrationStatus, _eventRegistrationsStatus);		callbacks = _.extend(callbacks, _callbacks);
		
        // bind model - kick start templating process - this will template recursively that is all that there is to be done
        ko.applyBindings(schema);
        console.log("Form rendered");
        setupDynamicSections();
        setupDatepicker();
        showGroupLabels();

        //Show loading panel
        if (callbacks.showLoadingPanel)
        {
            callbacks.showLoadingPanel();
        }

        //Asign preferred ids
        preferredSchoolBoardIds = schema.preferredSchoolboardIds || [],        preferredSchoolIds = schema.preferredSchoolIds || [],        preferredLocalIds = schema.preferredLocalIds || [];
        //Setup School board, School and local DropDowns
        $.when(setupSchoolBoardDropdown(), setupSchoolDropdown(), setupLocalDropdown())
        .done(function (sb, b, l)
        {
            //School Board
            utl.schoolboard.subscribe(function () { schoolboardChanged("schoolboard", "filteredSchoolList", "filteredLocalList", "school", "local"); });
            utl.coSchoolboard.subscribe(function () { schoolboardChanged("coSchoolboard", "filteredCoSchoolList", "filteredCoLocalList", "coSchool", "coLocal"); });
            utl.rtSchoolboard.subscribe(function () { schoolboardChanged("rtSchoolboard", "filteredRtSchoolList", "filteredRtLocalList", "rtSchool", "rtLocal"); });
            //Filter School and Local lists            schoolboardChanged("schoolboard", "filteredSchoolList", "filteredLocalList", "school", "local");            schoolboardChanged("coSchoolboard", "filteredCoSchoolList", "filteredCoLocalList", "coSchool", "coLocal");            schoolboardChanged("rtSchoolboard", "filteredRtSchoolList", "filteredRtLocalList", "rtSchool", "rtLocal");

            //School
            utl.school.subscribe(function () { schoolChanged("school", "previewSchoolName", "previewSchoolCity", "previewSchoolPhone", "previewSchoolFax", "previewSchoolEmail"); });            utl.rtSchool.subscribe(function () { schoolChanged("rtSchool", "previewRtSchoolName", "previewRtSchoolCity", "previewRtSchoolPhone", "previewRtSchoolFax", "previewRtSchoolEmail"); });
            //Local
            utl.local.subscribe(function () { localChanged("local", "previewLocalName"); });            utl.rtLocal.subscribe(function () { localChanged("rtLocal", "previewRtLocalName"); });

            //Check if dropdowns load correctly
            if (callbacks.hideLoadingPanel)
            {
                callbacks.hideLoadingPanel();
            }            if (utl.schoolBoardList().length > 0 && utl.filteredSchoolList().length > 0 && utl.filteredLocalList().length > 0)
            {
                if (callbacks.onLoadDataSuccess)
                {
                    callbacks.onLoadDataSuccess();
                }
            }            else
            {
                if (callbacks.onLoadDataError)
                {
                    callbacks.onLoadDataError();
                }
            }
        });

        //setup numeric fields - prevent entering anything else then the number
        form$.on
        (
            "keydown",
            ".numeric",
            function (e)
            {
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                    // Allow: Ctrl+A, Command+A
                    (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                    // Allow: home, end, left, right, down, up
                    (e.keyCode >= 35 && e.keyCode <= 40))
                {
                    // let it happen, don't do anything
                    return;
                }
                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105))
                {
                    e.preventDefault();
                }
            }
        );			
        setupTicketTotalCostCalculator();
        attachValidation();		omniware.setupCKEditor();
        omniware.setupCKFinder();
        omniware.adjustFrameSize();
			
        // auto-resize iframe following expand or collapse
		form$.on('shown.bs.collapse', function ()
		{
            omniware.adjustFrameSize();
        });
		form$.on('hidden.bs.collapse', function ()
		{
            omniware.adjustFrameSize();
        });

        setupUploadDocuments();
        //form$.on("DOMSubtreeModified", setupUploadDocuments);

        setupAccomodationValidations();
		
		setupAttachments();
    };

    // renders version of the form for printing
    omniware.renderPrintableForm = function (_header, _formsSchema, _formsOptions, _eventRegistrationsStatus) {
        schema = _formsSchema;
        formOptions = _formsOptions;		eventRegistrationStatus = _.extend(eventRegistrationStatus, _eventRegistrationsStatus);

        // substitute normal templates with printing templates where available - see stringTemplateEngine.js.
        ko.templateNameSuffix = ".print";

        // bind model - kick start templating process - this will template recursively that is all that there is to be done
        ko.applyBindings(schema);

        // expand all collapsible elements
        //form$.find('.collapse').addClass('in');
        //form$.find('body').css('background', 'none');

        $("input.no_radio[type=radio][data-target]").each(function () {
            var target = $($(this).attr('data-target'));
            if (target.length > 0)
            {
                if ($(this).prop('checked'))
                {
                    target.remove();
                }
            }
        });

        //Hide upload form
        form$.find(".upload-document-container").hide();

        //Prepend event info
        form$.parents('body').find("#printTombstone").prepend(_header);
        //form$.prepend(_header);

        console.log("Form rendered");
    };
    // removes all data attributes from a target element
    // example:  removeDataAttributes('#user-list');
    function removeDataAttributes(target)
    {
        var i;
        //var $target = $(target);
        var attrName;
        var dataAttrsToDelete = [];
        var dataAttrs = form$;//$target.get(0).attributes;
        var dataAttrsLen = dataAttrs.length;

        // loop through attributes and make a list of those
        // that begin with 'data-'
        for (i = 0; i < dataAttrsLen; i++)
        {
            if ('data-' === dataAttrs[i].name.substring(0, 5))
            {
                // Why don't you just delete the attributes here?
                // Deleting an attribute changes the indices of the others wreaking havoc on the loop we are inside
                // b/c dataAttrs is a NamedNodeMap (not an array or obj)
                dataAttrsToDelete.push(dataAttrs[i].name);
            }
        }

        // delete each of the attributes we found above
        // i.e. those that start with "data-"
        $.each(dataAttrsToDelete, function (index, attrName)
        {
            // $target.removeAttr(attrName);
            form$.removeAttr(attrName);
        })
    };	// retrieve form data as JSON    omniware.getSchema = function () 	{        return schema;    };

    // adjust frame size - it is called when needed, should be no need to call it directly
    omniware.adjustFrameSize = function () {
        if (formOptions && formOptions.selector) {
            // resize parent iframe
            var frame = $(formOptions.selector, window.parent.document);
            var height = jQuery("body").height();
            //noinspection JSUnresolvedVariable
            var newHeight = height + formOptions.extraHeight || 15;
            frame.height(newHeight);
            console.log('iframe resized to ' + newHeight);
        }
    };

    // validate form and display errors
    omniware.validateForm = function (isSubmit) {

        var tickets = $("input.ow_tickets[type='number']");
        if (tickets.is(":visible")) 		{
            tickets.attr("required", "required");
        }
        else 		{
            tickets.removeAttr("required");
        }
        //Attachments and supporting documents validator        var valid = true;        if (isSubmit)        {            valid = form$.valid();            var formExtraValid = formExtraValidations();            if (!formExtraValid)
            {
                valid = false;
            }            //Validate attachments
            var checksAttachments = $(".upload-attachments-container").find(".attachment-row:not(.attachment-row-disabled) :checkbox");
            var invalidAttachments = [];
            for (var i = 0; i < checksAttachments.length; i++)
            {
                var required = $(checksAttachments[i]).attr("required");
                var checked = $(checksAttachments[i]).is(":checked");
                if (required && !checked)
                {
                    var attachmentName = $(checksAttachments[i]).parent().text().trim();
                    invalidAttachments.push(attachmentName);
                }
            }
            messageContainer = $("#attachment-list-alert-container");
            messageContainer.removeClass("has-error");            if (invalidAttachments.length > 0)
            {
                valid = false;
                messageContainer.addClass("has-error");
                var invalidMessage = formMessages.attachments.requiredField + "</br>";
                for (var i = 0; i < invalidAttachments.length; i++)
                {
                    invalidMessage = invalidMessage + "</br>" + "<li>" + invalidAttachments[i] + "</li>";
                }

                messageAlert(invalidMessage, 'danger', messageContainer);
            }            var supportingMinDocs = $("#supporting-documents-minimum");            messageContainer = supportingMinDocs.siblings(".checkbox-list-alert-container");            messageContainer.removeClass("has-error");            if (supportingMinDocs.length)
            {
                var minDocs = supportingMinDocs.val();                var newDocs = utl.documents();                var oldDocs = $("#pnl-supporting-documents").children().find("input[type=checkbox]");                var oldDocsChecked = oldDocs.filter(function () { return $(this).is(":checked"); });                var totalDocs = newDocs.length + oldDocsChecked.length;                if (minDocs > totalDocs)
                {
                    valid = false;                    messageContainer.addClass("has-error");                    var message = formMessages.documents.minimumDocuments.format(supportingMinDocs.val());                    messageAlert(message, 'danger', messageContainer)
                }
            }
        }

        omniware.adjustFrameSize();
        console.log('Form valid ?' + valid);
        return valid;
    };

    // retrieve form data as JSON
    omniware.getFormData = function ()     {
        // Find disabled inputs, and remove the "disabled" attribute
        var disabled = form$.find(':input:disabled').removeAttr('disabled');

        // serialize the form
        var formData = form$.serializeArray();

        // re-disabled the set of inputs that you previously enabled
        disabled.attr('disabled', 'disabled');        		formData = _.filter		(			formData, 			function(val)			{ 				return val.name != "rawFile" && val.value != "[radio-null]";			}		);
        return JSON.stringify(formData, undefined, 2);
    };

    // format the form for printing
    omniware.printForm = function () {
        //form$.find('.panel-collapse.collapse').addClass('in'); // expand panels
        // Change some styles
        $('body').css('background', 'none');

    };
		//HTML Editors	omniware.setupCKEditor = function () {        if (!window.CKEDITOR) {            console.log("CKEditor not detected ");            return        }        CKEDITOR.editorConfig = function (config) {            // Define changes to default configuration here. For example:            // config.language = 'fr';            // config.uiColor = '#AADC6E';            config.toolbar = 'Custom';            config.toolbar_Custom = [                { name: 'styles', items: ['Styles', 'Format'] },                { name: 'basicstyles', items: ['Bold', 'Italic', 'Strike', '-', 'RemoveFormat'] },                { name: 'paragraph', items: ['NumberedList', 'BulletedList', '-', 'Blockquote'] },                { name: 'links', items: ['Link', 'Unlink', 'Anchor'] },                { name: 'insert', items: ['Image'] },                { name: 'tools', items: ['Maximize', '-', 'About'] }            ];        };        $('.htmlEditor').not('.ow_ckattached').ckeditor();        $('.htmlEditor').not('.ow_ckattached').addClass('.ow_ckattached');    }    omniware.setupCKFinder = function () {        if (!window.CKFinder) {            console.log("CKFinder not detected ");            return        }        CKFinder.setupCKEditor(null, '/ckfinder/');        var attachUrlPicker = function () {            $('.urlPicker').click(function () {                var picker = this;                CKFinder.popup(                    {                        basePath: '/ckfinder/',                        selectActionFunction: function (url) {                            $(picker).siblings('.urlValue').val(url);                        }                    });            })        }        attachUrlPicker();        //live        form$.on("DOMSubtreeModified", attachUrlPicker);    }		// -------  Documents --------    //Gets the list of documents to upload within the registration form    omniware.getDocuments = function ()    {        return JSON.stringify(utl.documents(), undefined, 2);    };	// ------- Attachments --------	//Gets the list of attachments to upload within the registration form    omniware.getAttachments = function ()    {		return JSON.stringify(utl.attachments, undefined, 2);    };    //Gets the list of attachments to upload within the registration form    omniware.getReturningPresenter = function ()
    {
        return $("input[name='returning-presenter']:checked").val();
    };			// ------------------------------------	
    // --------- private functions --------
	// ------------------------------------	
    // configure and attach jquery validation
    function attachValidation()
    {
        // configure jquery validation for Bootstrap 3.0 compatibility  @see: http://stackoverflow.com/questions/18754020/bootstrap-3-with-jquery-validation-plugin
        // override jquery validate plugin defaults
        $.validator.setDefaults({
            ignore: '.ow_dependentSection:not(.in) *',  // ignore elements ihe disabled dependent sections

            highlight: function (element) {
                $(element).parents('.collapse').addClass('in').height('auto'); // expand panels containing errors, height auto works around the bug
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function (element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }
        });

        // define custom validation for phone, because phoneUS did not work well
        jQuery.validator.addMethod("phone", function (phone_number, element) {
            phone_number = phone_number.replace(/\s+/g, "");
            return this.optional(element) || phone_number.length > 9 &&
                  phone_number.match(/^\(?\([\d\s]{3}\)[\d\s]{3}-[\d\s]{4}$/);
        }, "Invalid phone number");

        // and basic mask functionality for the phone field
        form$.find("input[type=phone]").blur(function () {
            var numbers = this.value.replace(/\D/g, ''),
                char = { 0: '(', 3: ') ', 6: ' - ' };
            this.value = '';
            for (var i = 0; i < numbers.length; i++) {
                this.value += (char[i] || '') + numbers[i];
            }
        });


        // validate and don't submit - calling it now will attach onblur/onchange field validations
        form$.validate({
            submitHandler: function (form) {
                console.log('form validation successful');
                return false; // do not submit
            },
            invalidHandler: function (form) {
                console.log('form validation failed');
            }
        });
    }

    //synchronize state of controlling checkboxes with the expanded stage of controlled sections
    function setupDynamicSections()
    {
        $("input[type=checkbox][data-target], input.yes_radio[type=radio][data-target]").each(function () {
            var target = $($(this).attr('data-target'));
            target.addClass('ow_dependentSection'); // used to suppress validation of disabled sections
            if (target.length > 0)
            {
                if ($(this).prop('checked'))
                {
                    target.addClass('in'); // ensure section is opened see http://getbootstrap.com/javascript/#collapse-usage
                }
                target.addClass('collapse').addClass('ow_collapsable_section'); // add those classes in case collapse attribute was missing in json
            }
            else
            {
                console.log("Checkbox points to non existing section: " + target)
            }
        });				$('input.yes_radio[type=radio][data-target]').on('change', function () {			var target = $($(this).attr('data-target'));			target.addClass("in");			$(this).siblings(".expand-arrow").removeClass("collapsed");			omniware.adjustFrameSize();		});				$('input.no_radio[type=radio][data-target]').on('change', function () {			var target = $($(this).attr('data-target'));			target.removeClass("in");			$(this).siblings(".expand-arrow").addClass("collapsed");			omniware.adjustFrameSize();		});
    };

    // set up schoolboard dropdowns with initial data, preferred values at the top and filtering
    function setupSchoolBoardDropdown()     {
        var d = $.Deferred();
        if (formOptions.schoolboardURI)
        {
            $.ajax			({
			    type: "GET",			    url: formOptions.schoolboardURI,			    success: function (result)
			    {
			        //General
			        utl.schoolBoardList(result);
			        console.log('got schoolboards ' + utl.schoolBoardList().length);
			        prepareSchoolBoardList();

			        $(document).trigger("boards-schools-locals-loaded");

			        // if we set value earlier it would be erased since the select drop down is initially empty
			        // Personal Info
			        var initialValue = $('select[schoolboardselector]').first().attr('initialValue');
			        if (!initialValue && utl.preparedBoardSchoolList().length > 0)
			            initialValue = utl.preparedBoardSchoolList()[0].m_Item1; // pretend 1st entry was truly selected  so preview fields are ok
			        utl.schoolboard(initialValue ? initialValue : '');

			        // Co Instructor
			        var initialValueCo = $('select[coschoolboardselector]').first().attr('initialValue');
			        if (!initialValueCo && utl.preparedBoardSchoolList().length > 0)
			            initialValueCo = utl.preparedBoardSchoolList()[0].m_Item1; // pretend 1st entry was truly selected  so preview fields are ok
			        utl.coSchoolboard(initialValueCo ? initialValueCo : '');

			        // Release Time
			        var initialValueRt = $('select[rtschoolboardselector]').first().attr('initialValue');
			        if (!initialValueRt && utl.preparedBoardSchoolList().length > 0)
			            initialValueRt = utl.preparedBoardSchoolList()[0].m_Item1; // pretend 1st entry was truly selected  so preview fields are ok
			        utl.rtSchoolboard(initialValueRt ? initialValueRt : '');

			        d.resolve();
			    },
			    error: function (result)
			    {
			        d.resolve();
			    }
			});
        }        else
        {
            d.resolve();
        }        return d.promise();    }    // set up school dropdowns with initial data, preferred values at the top and filtering
    function setupSchoolDropdown()     {
        var d = $.Deferred();
        if (formOptions.schoolURI)         {            $.ajax			({			    type: "GET",			    url: formOptions.schoolURI,			    success: function (result) 			    {			        //General			        utl.schoolList(result);			        console.log('got schools ' + utl.schoolList().length);			        // if we set value earlier it would be erased since the select drop down is initially empty			        // Personal Info			        filterSchoolsList("schoolboard", "filteredSchoolList", "school");								        var initialValue = $('select[schoolselector]').first().attr('initialValue');			        var initialValueFax = $('input[previewschoolfaxselector]').first().attr('initialValue');			        var initialValueEmail = $('input[previewschoolemailselector]').first().attr('initialValue');			        if (initialValue)			        {			            utl.school(initialValue);

			            // this could be done with ko.computed but it computed behaved a bit flaky
			            var selectedSchoolObject = _.find(utl.schoolList(), function (school)
			            {
			                return school.id == utl["rtSchool"]()
			            });

			            if (selectedSchoolObject)
			            {
			                utl.previewSchoolName(selectedSchoolObject.name);
			                utl.previewSchoolCity(selectedSchoolObject.city);
			                utl.previewSchoolPhone(selectedSchoolObject.phone);
			                utl.previewSchoolFax(selectedSchoolObject.fax);			                utl.previewSchoolEmail(selectedSchoolObject.email);
			            }
			        }			        if (initialValueFax)			        {			            utl.previewSchoolFax(initialValueFax);			        }			        if (initialValueEmail)			        {			            utl.previewSchoolEmail(initialValueEmail);			        }								        // Co Instructor			        filterSchoolsList("coSchoolboard", "filteredCoSchoolList", "coSchool");			        var coinitialValue = $('select[coschoolselector]').first().attr('initialValue');			        if (coinitialValue)			        {			            utl.coSchool(coinitialValue);
			        }								        // Release Time			        filterSchoolsList("rtSchoolboard", "filteredRtSchoolList", "rtSchool");			        var rtinitialValue = $('select[rtschoolselector]').first().attr('initialValue');			        var rtinitialValueFax = $('input[previewrtschoolfaxselector]').first().attr('initialValue');			        var rtinitialValueEmail = $('input[previewrtschoolemailselector]').first().attr('initialValue');			        if (rtinitialValue)			        {			            utl.rtSchool(rtinitialValue);			            // this could be done with ko.computed but it computed behaved a bit flaky
			            var selectedRtSchoolObject = _.find(utl.schoolList(), function (school) {
			                return school.id == utl["rtSchool"]()
			            });

			            if (selectedRtSchoolObject)
			            {
			                utl.previewRtSchoolName(selectedRtSchoolObject.name);
			                utl.previewRtSchoolCity(selectedRtSchoolObject.city);
			                utl.previewRtSchoolPhone(selectedRtSchoolObject.phone);
			                utl.previewRtSchoolFax(selectedRtSchoolObject.fax);			                utl.previewRtSchoolEmail(selectedRtSchoolObject.email);
			            }			        }			        if (rtinitialValueFax)			        {			            utl.previewRtSchoolFax(rtinitialValueFax);			        }			        if (rtinitialValueEmail)			        {			            utl.previewRtSchoolEmail(rtinitialValueEmail);			        }			        d.resolve();			    },
			    error: function (result)
			    {
			        d.resolve();
			    }			});
        }        else
        {
            d.resolve();
        }        return d.promise();    }    // set up local dropdowns with initial data, preferred values at the top and filtering
    function setupLocalDropdown()     {
        var d = $.Deferred();
        if (formOptions.localURI)         {            $.ajax			({			    type: "GET",			    url: formOptions.localURI,			    success: function (result) 			    {			        //General			        utl.localList(result);			        console.log('got locals ' + utl.localList().length);			        // if we set value earlier it would be erased since the select drop down is initially empty								        // Personal Info 			        filterLocalList("schoolboard", "filteredLocalList", "local");			        var initialValue = $('select[localselector]').first().attr('initialValue');			        utl.local(initialValue ? initialValue : '');			        // Co Instructor			        filterLocalList("coSchoolboard", "filteredCoLocalList", "coLocal");			        var coinitialValue = $('select[colocalselector]').first().attr('initialValue');			        utl.coLocal(coinitialValue ? coinitialValue : '');			        // Release Time			        filterLocalList("rtSchoolboard", "filteredRtLocalList", "rtLocal");			        var rtinitialValue = $('select[rtlocalselector]').first().attr('initialValue');			        utl.rtLocal(rtinitialValue ? rtinitialValue : '');
			        d.resolve();
			    },
			    error: function (result)
			    {
			        d.resolve();
			    }			});
        }        else
        {
            d.resolve();
        }        return d.promise();    }	
	//schoolboard
	function schoolboardChanged(sb, fsl, fll, s, l)
	{
		console.log(sb + " changed");
		filterSchoolsList(sb, fsl, s);
		filterLocalList(sb, fll, l);
	}

	// prepend preferred schoolboard to the beginning of the list .. the
	function prepareSchoolBoardList() {
		var separator = { "m_Item1": "", "m_Item2": "----" };

		var preferredSchoolboards = _.sortBy(_.filter(utl.schoolBoardList(), function (sBoard) {
			return _.contains(preferredSchoolBoardIds, sBoard.m_Item1);
		}), 'm_Item2');

		preferredSchoolboards = _.sortBy(_.sortBy(preferredSchoolboards, function (sBoard) {
			return sBoard.m_Item2
		}), 'm_Item2');

		if (preferredSchoolboards.length > 0) {
			utl.preparedBoardSchoolList(preferredSchoolboards.concat(separator).concat(utl.schoolBoardList()));
		} else {
			utl.preparedBoardSchoolList(utl.schoolBoardList());
		}
	}

	//schoolboard
	//filteredSchoolList
	function filterSchoolsList(sb, fsl, s)
	{
		var schoolboard = utl[sb]();

		utl[fsl]([]);
		var filtered = _.sortBy(_.filter(utl.schoolList(), function (school) {
			return school.parent == schoolboard
		}), 'name');

		//prepend preferred schools if any
		var preferredSchools = _.sortBy(_.filter(filtered, function (school) {
			return _.contains(preferredSchoolIds, school.id);
		}), 'name');

		if (preferredSchools.length > 0) {
			utl[fsl](preferredSchools.concat({ "id": "", "name": "----" }).concat(filtered));
		} else {
			utl[fsl](filtered);
		}				if (!_.contains(utl[fsl](), utl[s]())) 			utl[s](''); // clear invalid value				// pretend 1st entry was truly selected  so preview fields are ok		if (!utl[s]() && utl[fsl]().length > 0) 			utl[s](utl[fsl]()[0].id);
	}

	//schoolboard
	//filteredLocalList
	function filterLocalList(sb, fll, l) {
		console.log('filterLocalList');

		var schoolboard = utl[sb]();
		utl[fll]([]);
		var filtered = _.sortBy(_.filter(utl.localList(), function (local) {
			return local.parent == schoolboard
		}), 'name');

		//prepend preferred locals if any
		var preferredLocals = _.sortBy(_.filter(filtered, function (local) {
			return _.contains(preferredLocalIds, local.id);
		}), 'name');

		if (preferredLocals.length > 0) 		{
			utl[fll](preferredLocals.concat({ "id": "", "name": "----" }).concat(filtered))
		} 		else 		{
			utl[fll](filtered);
		}

		if (!_.contains(utl[fll](), utl[l]())) 			utl[l](''); // clear invalid value

		// if empty - pretend 1st entry was truly selected  so preview fields are ok
		if (!utl[l]() && utl[fll]().length) 		    utl[l](utl[fll]()[0].id);

		console.log("Locals for board " + schoolboard + " -> " + utl[fll]().length);
	}

	function schoolChanged(s, psn, psc, psp, psf, pse) {
		console.log("School changed " + utl.school());

		// this could be done with ko.computed but it computed behaved a bit flaky
		var selectedSchoolObject = _.find(utl.schoolList(), function (school) {
			return school.id == utl[s]()
		});
		console.log("Selected school: " + JSON.stringify(selectedSchoolObject));

		if (selectedSchoolObject) 		{			utl[psn](selectedSchoolObject.name);
			utl[psc](selectedSchoolObject.city);
			utl[psp](selectedSchoolObject.phone);
			utl[psf](selectedSchoolObject.fax);			utl[pse](selectedSchoolObject.email);
		} 		else 		{
			utl[psn]('');
			utl[psc]('');
			utl[psp]('');
			utl[psf]('');			utl[pse]('');
		}
	}

	function localChanged(l, pln)
	{
		// this could be done with ko.computed but it computed behaved a bit flaky
	    console.log("Local changed " + utl.local());
		var selectedLocalObject = _.find(utl.localList(), function (local)
		{
		    return local.id == utl[l]();
		}); 
		console.log("Selected local: " + JSON.stringify(selectedLocalObject));

		if (selectedLocalObject) 		{
		    utl[pln](selectedLocalObject.name);
		    if (l === "local")
		    {
		        utl["previewDelegationLimit"](selectedLocalObject.delegationLimit);
		    }
		} 		else 		{
		    utl[pln]('');
		    if (l === "local")
		    {
			    utl["previewDelegationLimit"]('');
		    }
		}
	}

    // datepicker defaults and hook up
    function setupDatepicker() {
        $.fn.datepicker.defaults.format = "yyyy-mm-dd";
        $.fn.datepicker.defaults.autoclose = true;
        $.fn.datepicker.defaults.todayHighlight = true;
        $.fn.datepicker.defaults.todayBtn = true;
        $('.input-group.date:not(.ow_disabled)').datepicker();
        //make live version too
        form$.on("DOMSubtreeModified", function () {
            $('.input-group.date:not(.ow_disabled)').datepicker();
        });
    };

    // recalculate TicketTotalCost on change .. assumes only one price and total fields per form
    function setupTicketTotalCostCalculator() {

        function recalculateTotalCost() {
            var price = $('input.ow_ticketPrice').val();
            if (!price || isNaN(price)) {
                console.log('Form doesnt contain ticket price ... total wil not be calculated');
                $(".input.ow_totalTicketCost").val('');
                return;
            }
            var totalTickets = 0;
            $("input.ow_tickets").each(function (idx, input) {
                var value = input.value;
                if (value && !isNaN(value)) totalTickets += parseInt(value, 10);
            })
            $(".ow_totalTicketCost").text((parseFloat(price) * totalTickets).toFixed(2))
        }

        recalculateTotalCost();

        //Format price
        var txt = $("input.ow_ticketPrice").val();
        var price = parseFloat(txt ? txt : 0).toFixed(2);
        $("input.ow_ticketPrice").val(price);
        var lbl = $("input.ow_ticketPrice").siblings("label.control-label");
        if (lbl.length) {
            lbl.html(price);
        }
        //$("input.ow_tickets").attr("min", "0");

        $("input.ow_tickets").change(function () {
            recalculateTotalCost();
        });
    }	
    // Alex's custom form field structure 'extendedlistofcheckboxes'
    function showGroupLabels() {
        $(".extendedlistofcheckboxes").each(function (ind, val) {
            var lastLabel = null;
            $(this).find("tr[name='grouplabel']").each(function (ind, val) {

                if (lastLabel == null || $(this).attr('grp') != $(lastLabel).attr('grp')) {
                    $(this).show();
                }

                lastLabel = this;
            });
            lastLabel = null;
            $(this).find("tr[name='groupcolumns']").each(function (ind, val) {

                if (lastLabel == null || $(this).attr('grp') != $(lastLabel).attr('grp')) {
                    $(this).show();
                }

                lastLabel = this;
            });
        });
    };

    // Setups the ability for uploading documents
    function setupAccomodationValidations() {
        var parentCheckBox = $("input#personal-accommodation-required");
        var childCheckBox = $("input#dependent-care-exemption-required");

        if (childCheckBox && parentCheckBox) {
            var parent = parentCheckBox.closest("div.form-group").parent();
            var child = childCheckBox.closest("div.form-group").parent();
            var targetElements = $(childCheckBox.data("target"));

            child.css({ "margin-left": "30px" });
            targetElements.parent().css({ "margin-left": "30px" });

            if (parent.is(':checked')) {
                child.show();
            }
            else {
                child.hide();
            }

            $(parentCheckBox).on("click", function () {
                if (this.checked) {
                    child.show();
                }
                else {
                    childCheckBox.prop("checked", false);
                    targetElements.find("textarea").val("");
                    targetElements.find("input[type='text']").val("");
                    child.hide();
                }

                if (childCheckBox.is(":checked")) {
                    targetElements.addClass("in");
                    childCheckBox.removeClass("collapsed");
                }
                else {
                    targetElements.removeClass("in");
                    childCheckBox.addClass("collapsed");
                }

            });
        }
    }		
    //Generates a new GUID
    function newGuid() {
        try {
            var guid = generateGuidPart() + generateGuidPart() + "-" + generateGuidPart() + "-" + generateGuidPart() + "-" + generateGuidPart() + "-" + generateGuidPart() + generateGuidPart() + generateGuidPart();
            return validateGuid(guid);
        }
        catch (e) {
            var g = "";
            for (var i = 0; i < 32; i++) {
                g += Math.floor(Math.random() * 0xF).toString(0xF) + (i == 8 || i == 12 || i == 16 || i == 20 ? "-" : "");
            }

            return validateGuid(g);
        }
    }

    //Generates a GUID part: 4 chars
    function generateGuidPart() {
        return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    }

    //Validates a GUID
    function validateGuid(guid) {
        var exp = /^(\{){0,1}[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}(\}){0,1}$/;
        if (exp.test(guid)) {
            return guid;
        }
        else {
            return newGuid();
        }
    }

    //Displays an alert using bootstrap alert machanism
    function messageAlert(text, type, container) {
        if (!type) {
            type = 'danger';
        }
        var html = '';
        html += '<div class="alert alert-' + type + ' alert-dismissible fade in" role="alert">';
        html += '   <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>';
        html += '   <span>' + text + '</span>';
        html += '</div>';

        $(container).html(html);
    };

    function formExtraValidations()
    {
        var isValid = true;
        var messageContainer;        //Validate Accommodations        var accommodationRequired = $("#accommodation-required, #accommodation-required_yes").is(":checked");        if (accommodationRequired)
        {
            var accommodationSchedules = $("#pnl-accommodation").find("[data-idName='schedule']").find("input[type=checkbox]");
            var accommodationSchedulesChecked = accommodationSchedules.filter(function () { return $(this).is(":checked"); });

            messageContainer = $("#pnl-accommodation").find("[data-idName='schedule']")[0];
            if (accommodationSchedulesChecked.length === 0)
            {
                isValid = false;
                $(messageContainer).addClass("has-error");
            }
            else
            {
                $(messageContainer).removeClass("has-error");
            }
        }        //Validate Release Time        var releaseTimeRequired = $("#releasetimerequired, #releasetimerequired_yes").is(":checked");        if (releaseTimeRequired)
        {
            var validValues = false;
            var releaseTimeSchedules = $("#pnl-release-time, #pnl-release-time-panel").find("input[type=radio]:checked");
            releaseTimeSchedules.each(function ()
            {
                var rValue = $(this).val();
                if ($.isNumeric(rValue) && rValue != 0)
                {
                    validValues = true;
                }
            });

            var messageLabel = $("#pnl-release-time, #pnl-release-time-panel").find("[data-idName='regularreleasetimesection']")[0];
            messageContainer = $(messageLabel).parent();
            if (!validValues)
            {
                isValid = false;
                $(messageLabel).text(formMessages.other.fieldRequired);
                $(messageContainer).addClass("has-error");
                $(messageContainer).show();
            }
            else
            {
                $(messageLabel).text('');
                $(messageContainer).removeClass("has-error");
                $(messageContainer).hide();
            }
        }        // Validate Caucus Overlapping schedules        var checkCaucusRequired = $("#pnl-caucus");        if (checkCaucusRequired)
        {
            var isOverlapping = false;
            var messageLabel = $("#pnl-caucus").find("[data-idName='event-caucuses']")[0];
            messageContainer = $(messageLabel).parent();

            var caucusSchedules = $("#pnl-caucus").find("input[type=checkbox]");
            var caucusSchedulesChecked = caucusSchedules.filter(function () { return $(this).is(":checked"); });
            var checkedOverlapping = [];

            var caucusStartHour;
            var caucusEndHour;
            var caucusStartDate;
            var caucusEndDate;
            caucusSchedulesChecked.each(function ()
            {
                var caucusReferenceStr = $(this).closest('tbody').children('tr:eq(1)').children('td').attr('ref');
                var caucusReference = JSON.parse(caucusReferenceStr);
                var caucusStartDate = _.where(caucusReference.Attributes, { Key: "oems_startdatetime" })[0];
                var caucusEndDate = _.where(caucusReference.Attributes, { Key: "oems_enddatetime" })[0];

                if (caucusStartDate && caucusEndDate)
                {
                    var caucus =
                    {
                        StartDate: new Date(caucusStartDate.Value),
                        EndDate: new Date(caucusEndDate.Value)
                    }

                    //Check overlapping
                    $.each(checkedOverlapping, function (key, value)
                    {
                        var overlapping = caucus.StartDate < value.EndDate && value.StartDate < caucus.EndDate;
                        if (overlapping) {
                            isOverlapping = overlapping;
                        }
                    });

                    checkedOverlapping.push(caucus);
                }
            });

            if (isOverlapping)
            {
                isValid = false;
                $(messageLabel).text(formMessages.other.caucusOverlapping);
                $(messageContainer).addClass("has-error");
                $(messageContainer).show();
            }
            else
            {
                $(messageLabel).text('');
                $(messageContainer).removeClass("has-error");
                $(messageContainer).hide();
            }
        }        return isValid;
    }
	// ------- Documents -------    //Setups the ability for uploading documents    function setupUploadDocuments() {        $(".upload-document-container").find("input[name='file']").each		(			function () {			    $(this).unbind('change');			    $(this).bind('change', handleUploadDocumentSelect);			}		);    }    //Triggered when a document is selected via input file and using FileReader API    function handleUploadDocumentSelect(evt) {        var files = evt.target.files;        var reader = new FileReader();        var rawFile = $(this).closest(".upload-document-container").find("input[name='rawFile']");        var selectedFile = $(this).closest(".upload-document-container").find(".selected-file");        var messageContainer = $(this).closest(".upload-document-container").find(".alert-container");        reader.onload =		(			function (file) {			    return function (e) {			        if (file.size >= maxFileSizeMb) {			            rawFile.val("");			            rawFile.data("name", "");			            rawFile.data("size", "");			            rawFile.data("type", "");			            selectedFile.hide();			            messageAlert(formMessages.documents.maxSize, 'danger', messageContainer);			            omniware.adjustFrameSize();			        }			        else {			            messageContainer.html("");			            omniware.adjustFrameSize();			            rawFile.val(e.target.result);			            rawFile.data("name", file.name);			            rawFile.data("size", file.size);			            rawFile.data("type", file.type);			            selectedFile.text(file.name);			            selectedFile.show();			        }			    };			}		)(files[0]);        if (files.length > 0) {            reader.readAsDataURL(files[0]);        }    }				// ------- Attachments -------    //Setup the ability for uploading documents    function setupAttachments()    {        $(".upload-attachments-container").find("input[name='file']").each(function ()        {			$(this).unbind('change');			$(this).bind('change', handleUploadAttachmentsSelect);		});        $(".upload-attachments-container").find("input[type='radio']").each(function ()
		{
			$(this).unbind('change');			$(this).bind('change', handleReturningPresenterSelect);
		});    }    //Triggered when a document is selected via input file and using FileReader API    function handleUploadAttachmentsSelect(evt)    {        var files = evt.target.files;        var reader = new FileReader();        var rawFile = $(this).siblings("input[name='rawFile']");		var attachmentType = $(this).siblings("input[name='attachmentType']").val();		var checkBox = $(this).closest(".row").find(":checkbox");        var selectedFile = $(this).parent().siblings(".selected-file");        var messageContainer = $("#attachment-list-alert-container"); 		var upload = $(this).siblings(".fa-cloud-upload");        reader.onload =		(			function (file)			{			    return function (e)			    {					//Quit the current element					utl.attachments = _.without(utl.attachments, _.findWhere(utl.attachments, {attachmentType: attachmentType}));										if (file.size >= maxFileSizeMb)					{						//Raw file			            rawFile.val("");			            rawFile.data("name", "");			            rawFile.data("size", "");			            rawFile.data("type", "");			            selectedFile.hide();			            messageAlert(formMessages.documents.maxSize, 'danger', messageContainer);			            omniware.adjustFrameSize();												//Checkbox						if(!checkBox.val())						{							checkBox.prop('checked', false);						}									        }					else					{			            messageContainer.html("");			            omniware.adjustFrameSize();												//Raw file			            rawFile.val(e.target.result);			            rawFile.data("name", file.name);			            rawFile.data("size", file.size);			            rawFile.data("type", file.type);									            //selectedFile.text(file.name);			            //selectedFile.show();						upload.css("color","orangered");												//Checkbox						checkBox.prop('checked', true);												//Process the attachment						var attachment =						{							id: newGuid(),							attachmentType: attachmentType,							fileName: rawFile.data("name"),							body: $.trim(rawFile.val()).split(',')[1],							mimeType: rawFile.data("type"),							fileSize: rawFile.data("size")						};						utl.attachments.push(attachment);			        }			    };			}		)(files[0]);        if (files.length > 0) {            reader.readAsDataURL(files[0]);        }    }	    //Triggered when a returning presenter radio is changed    function handleReturningPresenterSelect(evt)
    {        var radio = $(this);        $(".attachments-beforeapproval .icon-attachment").each		(			function ()
			{
			    if (radio.val() === "false")
			    {
			        $(this).show();
			    }
                else
			    {
			        $(this).hide();
			    }
			}		);

        $(".attachments-beforeapproval .attachment-row").each		(			function ()
			{
			    if (radio.val() === "false")
			    {
			        $(this).removeClass("attachment-row-disabled");
			    }
			    else
			    {
			        $(this).addClass("attachment-row-disabled");
			    }
			}		);
    }	// ----------------------------------------------------------------------------    // ---- KO view utils - Helpers functions available from templates via viewmodel 	// ----------------------------------------------------------------------------	
    utl.isDisabled = function (val) {
        return utl.isTrue(schema.disabled) || utl.isTrue(val);
    };

    utl.isTrue = function (val) {
        return val != undefined && (val == true || val == "true");
    };		utl.isFalse = function (val) {        return val != undefined && (val == false || val == "false");    };    utl.isComponentVisible = function (id)
    {
        return formOptions.isPortal || (id.toLowerCase() != "attachments" && id.toLowerCase() != "supportingdocuments");
    };		utl.isChildComponentVisible = function (id, parent, parents)    {		var valid = true;		//if(id.toLowerCase() == "status" || id.toLowerCase() == "notes")		//{			var parentsCopy = _.clone(parents);			parentsCopy.shift();			if(parentsCopy[0].id.toLowerCase() == parent && !utl.isApproved())			{				valid = false;
			}
		//}		        return valid;    };	
    utl.toQs = function (val) {
        var qs = "";
        if (val) {
            qs = val.toLowerCase().replace(/ /gi, "-");
        }
        return qs;
    };	
    // add observable version of the 'children' Array to current form element - used by repeatableSection
    utl.fetchObservableChildren = function (data) {
        if (!data.observableChildren) {
            if (!data.children) {
                console.log("element " + JSON.stringify(data) + " has no children");
                return;
            }
            data.observableChildren = ko.observableArray(data.children);

            // also add functions to add/delete element to the children array .. those are hooked up to add/delete buttons
            data.addChild = function () {
                var template = data.options.newRowTemplate; // this is template for 'blank' array element
                if (!template) {
                    console.log("no newRowTemplate cant add row");
                    return;
                }
                var deepClone = JSON.parse(JSON.stringify(template)); // _.clone is shallow: http://underscorejs.org/#clone
                data.observableChildren.push(deepClone);
                omniware.adjustFrameSize();
                setupCKEditor();
            };

            data.removeChild = function () {
                data.observableChildren.remove(this);
                omniware.adjustFrameSize();
            };

            // no need to copy back self.observableChildren.subscribe()
        }
        return data.observableChildren;
    };

    // repeat template few times - used by printable version of the form
    utl.repeatTemplate = function (data) 	{		var repetitions = 0;		var results = [];		if(data && data.children)		{			repetitions = data.children.length;			$.each(data.children, function(index, obj) 			{				results.push(obj);			}		)};				return results;
        //return _(repetitions).times(function () { return data.options.newRowTemplate });
    };

    // builds unique name (path) of the element within the form
    utl.name = function (parents) {
        if (formOptions.fullPaths) {

            var path = "";
            _.uniq(parents).reverse().forEach(
                function (node) {
                    if (typeof node.name != "undefined") {
                        path = path + '/' + node.name;
                    }
                    if (typeof node.index != "undefined") {
                        path = path + '#' + node.index;
                    }
                }
            );
            return path;

        } else {
            return parents[0].name; // short name
        }
    };

    // builds unique name starting from the parent - required for names of nested options inside input element
    utl.parentsName = function (parents) {
        var safeCopy = _.clone(parents);
        safeCopy.shift(); // skip current node -> move up to parent
        return utl.name(safeCopy)
    };
	//Check if approved    utl.isApproved = function () {		var isApproved = formOptions && formOptions.currentStatus && formOptions.currentStatus == eventRegistrationStatus.Approved;        return isApproved;    }; 
    utl.grandParentsName = function (parents) {
        var safeCopy = _.clone(parents);
        safeCopy.shift(); // skip current node -> move up to parent
        safeCopy.shift(); // skip current node -> move up to parent
        return utl.name(safeCopy)
    };

    // add index property to current context
    utl.addIndex = function (data, index) {
        data.index = index;
        return data;
    };

    // support chaining
    utl.set = function (observable, value)
    {
        observable(value);
        return observable;
    };
    //set json as string    utl.objectToString = function (data)    {
        var objStr = JSON.stringify(data);
        return objStr;
    }    utl.count = function (data) {
        return data.length;
    }	// ------- Printable functions -------		//School boards, school and locals    utl.schoolBoardName = function (id) 	{        var name = "";		var schoolboard = _.find(utl.schoolBoardList(), function (sb) { return sb.m_Item1 === id }) ;		if(schoolboard)			name = schoolboard.m_Item2;				return name;    };    utl.schoolName = function (id) 	{        var name = "";		var school = _.find(utl.schoolList(), function (s) { return s.id === id }) ;		if(school)			name = school.name;				return name;    };	    utl.localName = function (id) 	{        var name = "";		var local = _.find(utl.localList(), function (l) { return l.id === id }) ;		if(local)			name = local.name;				return name;    };		utl.radioValueName = function (items, value) 	{		if(!value)		{			return "";		}				var name = "";        var selected = _.find(items, function (item)        {
            return item.idValue === value
        });		if(selected)			name = selected.label;				return name;    };		utl.selectBoxValueName = function (items, value) 	{        var name = "";        var selected = _.find(items, function (item)        {
            return item.value === value
        });		if(selected)			name = selected.label;				return name;    };	    // -------  Documents --------
    utl.documents = ko.observableArray([]);
	
    //Deletes a document
    utl.deleteDocument = function (document) {
        utl.documents.remove(function (d) {
            return d.id === document.id;
        });
        omniware.adjustFrameSize();
    };

    //Returns true if there are documents to be uploaded
    utl.haveDocuments = ko.computed
	(
		function () {
		    return utl.documents().length > 0;
		}
	);

    //Uploads a document using information from file reader
    utl.uploadDocument = function (model, event) {
        var tg = $(event.target);
        var description = tg.closest(".upload-document-container").find("input[name='description']");
        var file = tg.closest(".upload-document-container").find("input[name='file']");
        var selectedFile = tg.closest(".upload-document-container").find(".selected-file");
        var rawFile = tg.closest(".upload-document-container").find("input[name='rawFile']");

        //Validation
        if ($.trim(description.val()) && $.trim(rawFile.val()) && rawFile.data("size") < maxFileSizeMb) {
            //Process the document
            var document =
			{
			    id: newGuid(),
			    subject: $.trim(description.val()),
			    fileName: rawFile.data("name"),
			    body: $.trim(rawFile.val()).split(',')[1],
			    mimeType: rawFile.data("type"),
			    fileSize: rawFile.data("size")
			};
            utl.documents.push(document);

            selectedFile.hide();
            selectedFile.text('');
            rawFile.val('');
            rawFile.data("name", "");
            rawFile.data("size", "");
            rawFile.data("type", "");
            description.val('');
            tg.closest(".upload-document-container").find(".alert-container").html("");
            omniware.adjustFrameSize();
        }
        else 
		{
            var message = '';
            if (!$.trim(description.val()) && !$.trim(rawFile.val())) {
                message = formMessages.documents.noData;
            }
            else if (!$.trim(description.val())) {
                message = formMessages.documents.noDescription;
            }
            else if (!$.trim(rawFile.val())) {
                message = formMessages.documents.noDocument;
            }
            else if (rawFile.data("size") >= maxFileSizeMb) {
                message = formMessages.documents.maxSize;
            }

            messageAlert(message, 'danger', tg.closest(".upload-document-container").find(".alert-container"));
            omniware.adjustFrameSize();
        }
    };	
	
	// -------  Attachments --------	utl.attachments = ko.observableArray([]);	

	//Download link
	utl.downloadattachment = function (item)
	{
		var type = item.type;
		var id = item.value;
		$.fileDownload
		(
			formOptions.attachmentDownloadUrl + id,
			{
				httpMethod: 'POST',
				cookieName: "OMWFileDownload",
				successCallback: function (url)
				{
					$("#documents-list-alert-container").html('');
				},
				failCallback: function (html, url)
				{
					messageAlert(formMessages.attachments.downloadError, 'danger', $("#documents-list-alert-container"));
				}
			}
		);
	};
	
});



//  THIS FILE IS GENERATED DO NO MODIFY 
//   Thu Oct 13 2016 16:03:21 GMT-0500 (SA Pacific Standard Time)
// ko template engine using string templates
// based on  https://github.com/rniemeyer/SamplePresentation/blob/master/js/stringTemplateEngine.js
// see http://www.knockmeout.net/2011/10/ko-13-preview-part-3-template-sources.html
// modified to not use require.js
var omniware = omniware || {};

omniware.configureKoStringTemplateEngine = function(ko) {
    //define a template source that tries to key into an object first to find a template string
    var templates = {},
        data = {},
        engine = new ko.nativeTemplateEngine();

    ko.templateSources.stringTemplate = function(template) {
        this.templateName = template;
    };

    ko.utils.extend(ko.templateSources.stringTemplate.prototype, {
        data: function(key, value) {
            data[this.templateName] = data[this.templateName] || {};

            if (arguments.length === 1) {
                return data[this.templateName][key];
            }

            data[this.templateName][key] = value;
        },
        text: function(value) {
            if (arguments.length === 0) {
                // return '.print' version of template if instructed to do so
                if( ko.templateNameSuffix && templates[this.templateName+ko.templateNameSuffix] ) {
                    return templates[this.templateName+ko.templateNameSuffix];
                } else {
                    return templates[this.templateName];
                }
            }

//            if(ko.templateNameSuffix && templates[this.templateName+ko.templateNameSuffix]) {
//                templates[this.templateName+ko.templateNameSuffix] = value;
//            }
            templates[this.templateName] = value;
        }
    });

    engine.makeTemplateSource = function(template, doc) {
        var elem;
        if (typeof template === "string") {
            elem = (doc || document).getElementById(template);

            if (elem) {
                return new ko.templateSources.domElement(elem);
            }

            return new ko.templateSources.stringTemplate(template);
        }
        else if (template && (template.nodeType == 1) || (template.nodeType == 8)) {
            return new ko.templateSources.anonymousTemplate(template);
        }
    };

    //make the templates accessible
    ko.templates = templates;

    //make this new template engine our default engine
    ko.setTemplateEngine(engine);
};
//  THIS FILE IS GENERATED DO NO MODIFY 
//   Thu Oct 13 2016 16:03:21 GMT-0500 (SA Pacific Standard Time)
// stringified KO templates
 var omniware = omniware || {};
omniware.loadKoTemplates = function () {
	ko.templates["attachmentsalert"] = "<div id=\x22attachment-list-alert-container\x22 class=\x22col-xs-12 alert-container\x22></div>";
	ko.templates["attachmentsapproved"] = "<!-- Document List Approved --><div class=\x22col-xs-12\x22>	<div class=\x22form-group\x22 data-bind=\x22visible: checkboxValues && checkboxValues.length > 0\x22>		<fieldset class=\x22attachments\x22>            <legend data-bind=\x22html: value\x22></legend>            <div class=\x22col-xs-6\x22>		        <div data-bind=\x22foreach: checkboxValues\x22>			        <div class=\x22row attachment-row\x22>				        <div class=\x22col-xs-8\x22>					        <label class=\x22checkbox inline document-list\x22>						        <!-- ko if: $root.utl.isTrue($data.required) -->							        <span class=\x22required\x22>*</span>						        <!-- /ko -->							        <!-- ko ifnot: $root.utl.isTrue($data.required) -->							        <span class=\x22optional\x22></span>						        <!-- /ko -->												        <input type=\x22checkbox\x22 disabled data-bind=\x22value: value, checked: $root.utl.isTrue($data.checked), attr: { name: $root.utl.parentsName($parents), required: $data.required && $root.utl.isTrue($data.required) }\x22>						        <!-- ko  text: label -->							        <!-- knockout does not seem to like text binding on input label element -->						        <!-- /ko -->					        </label>				        </div>				        <div class=\x22col-xs-4\x22>					        <span class=\x22btn btn-attachment-download fileinput-button pull-left\x22> 						        <i class=\x22fa fa-cloud-upload\x22></i>						        <input type=\x22file\x22 name=\x22file\x22 />						        <input type=\x22hidden\x22 name=\x22rawFile\x22 />						        <input type=\x22hidden\x22 name=\x22attachmentType\x22 data-bind=\x22value: type\x22/>					        </span>					        <div class=\x22selected-file attachment-name\x22 style=\x22display: none;\x22></div>										        <!-- ko if: value -->					        <a class=\x22download-form-document btn\x22 href=\x22javascript:void(0);\x22 data-bind=\x22click: $root.utl.downloadattachment\x22>						        <i class=\x22fa fa-cloud-download\x22></i>					        </a>					        <!-- /ko -->				        </div>			        </div>		        </div>            </div>        </fieldset>	</div></div>";
	ko.templates["attachmentsapprovedcontainer"] = "<!-- Documents List Approved Container --><div class=\x22form-group row upload-attachments-container\x22 data-bind=\x22visible: $root.utl.isApproved()\x22>	<div class=\x22ow_legend\x22 data-bind=\x22html: value\x22></div>	<div data-bind=\x22template: { name: 'renderAllChildren', data: $data }\x22></div></div>";
	ko.templates["attachmentsapprovedempty"] = "<!-- Document List Approved Empty --><div class=\x22col-xs-12\x22>	<div class=\x22form-group\x22 data-bind=\x22visible: checkboxValues && checkboxValues.length > 0\x22>		<fieldset class=\x22attachments\x22>            <legend data-bind=\x22html: value\x22></legend>            <div class=\x22col-xs-6\x22>			    <div data-bind=\x22foreach: checkboxValues\x22>				    <div class=\x22row\x22>					    <div class=\x22col-xs-8\x22>						    <label class=\x22checkbox inline document-list\x22>							    <span data-bind=\x22html: label\x22></span>						    </label>					    </div>					    <div class=\x22col-xs-4\x22>									    <!-- ko if: value -->						    <a class=\x22download-form-document  btn\x22 href=\x22javascript:void(0);\x22 data-bind=\x22click: $root.utl.downloadattachment\x22>							    <i class=\x22fa fa-cloud-download\x22></i>						    </a>						    <!-- /ko -->					    </div>				    </div>			    </div>            </div>        </fieldset>	</div></div>";
	ko.templates["attachmentsbeforeapproval"] = "<!-- Document List Before Approval --><div class=\x22col-xs-12\x22>    <div class=\x22col-xs-6\x22>	    <div class=\x22form-group attachments-beforeapproval\x22 data-bind=\x22visible: checkboxValues && checkboxValues.length > 0\x22>		    <div data-bind=\x22foreach: checkboxValues\x22>			    <div data-bind=\x22css: $root.utl.isTrue($data.group) ? 'row attachment-row attachment-row-disabled' : 'row attachment-row'\x22>				    <div class=\x22col-xs-8 chk-container\x22>					    <label class=\x22checkbox inline document-list\x22>						    <!-- ko if: $root.utl.isTrue($data.required) -->							    <span class=\x22required\x22>*</span>						    <!-- /ko -->							    <!-- ko ifnot: $root.utl.isTrue($data.required) -->							    <span class=\x22optional\x22></span>						    <!-- /ko -->													    <input type=\x22checkbox\x22 disabled data-bind='value: value,                                 attr: {                                    name: $root.utl.parentsName($parents),                                    checked: $root.utl.isTrue($data.checked),                                    required: $data.required && $root.utl.isTrue($data.required) && !$root.utl.isApproved()                                }'>						    <!-- ko  text: label -->							    <!-- knockout does not seem to like text binding on input label element -->						    <!-- /ko -->					    </label>				    </div>				    <div class=\x22col-xs-4\x22>					    <!-- ko ifnot: $root.utl.isApproved() -->						    <span class=\x22btn btn-attachment-download fileinput-button pull-left upload-attachment icon-attachment\x22 data-bind=\x22attr: { 'data-type': type }, style: { display: $root.utl.isTrue($data.group) ? 'none' : '' }\x22> 							    <i class=\x22fa fa-cloud-upload\x22></i>							    <input type=\x22file\x22 name=\x22file\x22 />							    <input type=\x22hidden\x22 name=\x22rawFile\x22 />							    <input type=\x22hidden\x22 name=\x22attachmentType\x22 data-bind=\x22value: type\x22/>						    </span>						    <div class=\x22selected-file attachment-name\x22 style=\x22display: none;\x22>Test</div>					    <!-- /ko -->										    <!-- ko if: value -->						    <a class=\x22download-form-attachment btn icon-attachment\x22 href=\x22javascript:void(0);\x22 data-bind=\x22attr: { 'data-type': type }, style: { display: $root.utl.isTrue($data.group) ? 'none' : '' }, click: $root.utl.downloadattachment\x22>							    <i class=\x22fa fa-cloud-download\x22></i>						    </a>										    <!-- /ko -->				    </div>			    </div>		    </div>	    </div>    </div></div><!-- 1 -->";
	ko.templates["attachmentsbeforeapprovalcontainer"] = "<!-- Document List Before Approval --><div class=\x22form-group upload-attachments-container attachments-before-approval-container\x22>    <div data-bind=\x22template: { name: 'renderAllChildren', data: $data }\x22></div></div>";
	ko.templates["attachmentsbeforeapprovalrequired"] = "<!-- Document List Before Approval --><div class=\x22col-xs-12\x22>    <div class=\x22col-xs-6\x22>	    <div class=\x22form-group attachments-beforeapproval-required\x22 data-bind=\x22visible: checkboxValues && checkboxValues.length > 0\x22>		    <div data-bind=\x22foreach: checkboxValues\x22>			    <div class=\x22row\x22>				    <div class=\x22col-xs-8 chk-container\x22>					    <label class=\x22checkbox inline document-list\x22>						    <!-- ko if: $root.utl.isTrue($data.required) -->							    <span class=\x22required\x22>*</span>						    <!-- /ko -->							    <!-- ko ifnot: $root.utl.isTrue($data.required) -->							    <span class=\x22optional\x22></span>						    <!-- /ko -->													    <input type=\x22checkbox\x22 disabled data-bind=\x22value: value,    attr: { name: $root.utl.parentsName($parents), checked: $root.utl.isTrue($data.checked), required: $data.required && $root.utl.isTrue($data.required) && !$root.utl.isApproved() }\x22>						    <!-- ko  text: label -->							    <!-- knockout does not seem to like text binding on input label element -->						    <!-- /ko -->					    </label>				    </div>				    <div class=\x22col-xs-4\x22>					    <!-- ko ifnot: $root.utl.isApproved() -->						    <span class=\x22btn btn-attachment-download fileinput-button pull-left upload-attachment\x22 data-bind=\x22attr: { 'data-type': type }, style: { display: $root.utl.isTrue($data.group) ? 'none' : '' }\x22> 							    <i class=\x22fa fa-cloud-upload\x22></i>							    <input type=\x22file\x22 name=\x22file\x22 />							    <input type=\x22hidden\x22 name=\x22rawFile\x22 />							    <input type=\x22hidden\x22 name=\x22attachmentType\x22 data-bind=\x22value: type\x22/>						    </span>						    <div class=\x22selected-file attachment-name\x22 style=\x22display: none;\x22>Test</div>					    <!-- /ko -->										    <!-- ko if: value -->						    <a class=\x22download-form-attachment btn\x22 href=\x22javascript:void(0);\x22 data-bind=\x22attr: { 'data-type': type }, click: $root.utl.downloadattachment\x22>							    <i class=\x22fa fa-cloud-download\x22></i>						    </a>										    <!-- /ko -->				    </div>			    </div>		    </div>	    </div>    </div></div><!-- 1 -->";
	ko.templates["attachmentsyesnoradios"] = "<!-- YesNo Radios --><div class=\x22col-xs-12\x22>	<!-- ko if: $root.utl.isTrue(options.required) -->	<span class=\x22required required-yes-no\x22>*</span>	<!-- /ko -->		<!-- ko ifnot: $root.utl.isTrue(options.required) -->	<span class=\x22optional\x22></span>	<!-- /ko -->    <!-- ko if: options.label -->    <label class=\x22control-label returning-presenter\x22 data-bind=\x22attr: { for: name }, html: options.label \x22> </label>    <!-- /ko -->    <input type=\x22radio\x22 class=\x22yes_radio\x22 value=\x22true\x22 data-bind=\x22attr: { name: $data.name }, checkedValue: true, checked: $root.utl.isTrue(value), disable: $root.utl.isApproved()\x22 /> Yes    <input type=\x22radio\x22 class=\x22no_radio\x22 value=\x22false\x22 data-bind=\x22attr: { name: $data.name }, checkedValue: false, checked: $root.utl.isTrue(value), disable: $root.utl.isApproved()\x22 /> No</div>";
	ko.templates["checkbox"] = "<!-- Checkbox input--><div class=\x22form-group\x22 data-bind=\x22attr: { class: options.width ? 'form-group col-xs-'+options.width : 'form-group' }\x22>    <!-- ko if: options.leftLabel -->    <label class=\x22control-label col-xs-2\x22  data-bind=\x22html: options.leftLabel\x22></label>    <!-- /ko -->    <div class=\x22col-xs-10\x22>				<label class=\x22checkbox inline \x22>					<!-- ko ifnot: options.displayAsRadios -->				<!-- ko ifnot: options.enablesSection -->				<input type=\x22checkbox\x22 data-bind=\x22value: value, attr: { name: $root.utl.name($parents), id: $root.utl.toQs(id) }, checked: options.checked, disable: $root.utl.isDisabled(options.disabled)\x22>				<!-- /ko -->				<!-- todo this requires extra js logic to expand the section when checkbox is already checked on incoming data -->				<!-- ko if: options.enablesSection --> <!-- this version of the checkbox colapses/hides a section-->                <input type=\x22checkbox\x22 class=\x22ow_enablesSection\x22 data-bind=\x22value: value, css:{collapsed: !$root.utl.isTrue(options.checked)}, attr: { name: $root.utl.name($parents), id: $root.utl.toQs(id), 'data-target': '#' + options.enablesSection }, checked: options.checked, disable: $root.utl.isDisabled(options.disabled)\x22 data-toggle=\x22collapse\x22>				<!-- /ko -->				<!-- ko  text: options.label-->				<!-- knockout does not seem to like text binding on input label element -->				<!-- /ko -->			<!-- /ko -->						<!-- ko if: options.displayAsRadios --> 							<div class=\x22expand-arrow ow_enablesSection\x22 data-bind=\x22css:{collapsed: !$root.utl.isTrue(options.checked)}\x22/>							<!-- ko  text: options.label -->				<!-- knockout does not seem to like text binding on input label element -->				<!-- /ko -->				                                <!-- ko ifnot: options.enablesSection -->                (                Yes <input type=\x22radio\x22 class=\x22yes_radio\x22 data-bind=\x22value: value, attr: { name: $root.utl.name($parents), id: $root.utl.toQs(id + '_yes') }, checkedValue: true, checked: options.checked, disable: $root.utl.isDisabled(options.disabled)\x22 required=\x22required\x22 />                /                No <input type=\x22radio\x22 class=\x22no_radio\x22 value=\x22[radio-null]\x22 data-bind=\x22attr: { name: $root.utl.name($parents), id: $root.utl.toQs(id + '_no') }, checkedValue: false, checked: options.checked, disable: $root.utl.isDisabled(options.disabled)\x22 required=\x22required\x22 />                )                <!-- /ko -->                <!-- todo this requires extra js logic to expand the section when checkbox is already checked on incoming data -->                <!-- ko if: options.enablesSection --> <!-- this version of the checkbox colapses/hides a section-->                (                Yes <input type=\x22radio\x22 class=\x22ow_enablesSection yes_radio\x22 data-bind=\x22value: value, attr: { name: $root.utl.name($parents), id: $root.utl.toQs(id + '_yes'), 'data-target': '#' + options.enablesSection }, checkedValue: true, checked: options.checked, disable: $root.utl.isDisabled(options.disabled)\x22 required=\x22required\x22 />                /                No <input type=\x22radio\x22 class=\x22ow_enablesSection no_radio\x22 value=\x22[radio-null]\x22 data-bind=\x22attr: { name: $root.utl.name($parents), id: $root.utl.toQs(id + '_no'), 'data-target': '#' + options.enablesSection }, checkedValue: false, checked: options.checked, disable: $root.utl.isDisabled(options.disabled)\x22 required=\x22required\x22 />                )                <span class=\x22help-block\x22 style=\x22display: none;\x22 data-bind=\x22attr: { for: $root.utl.name($parents) }\x22></span>                <!-- /ko -->            <!-- /ko -->        </label>    </div></div>";
	ko.templates["colocal"] = "<div class=\x22form-group\x22>    <!-- ko if: options.label -->    <label class=\x22control-label col-xs-2\x22 data-bind=\x22attr: {for: name}, text: options.label, attr: {required: $root.utl.isTrue(options.required)}\x22></label>    <!-- /ko -->    <div class=\x22col-xs-10\x22>        <select data-bind=\x22attr: {name: $root.utl.name($parents), colocalselector: true, initialValue: value},                            value: $root.utl.coLocal,                            disable: $root.utl.isDisabled(options.disabled),                            foreach: $root.utl.filteredCoLocalList\x22>            <option data-bind=\x22value: id, text:name, attr:{ selected: id == $parent.value}\x22></option>        </select>        <!-- ko if: !$root.utl.filteredLocalList().length  -->        <i class=\x22fa fa-refresh fa-spin\x22></i>        <!-- /ko -->    </div></div>";
	ko.templates["colocal.print"] = "<div class=\x22form-group\x22>    <!-- ko if: options.label -->    <label class=\x22control-label col-xs-2\x22 data-bind=\x22attr: {for: name}, text: options.label, attr: {required: $root.utl.isTrue(options.required)}\x22></label>    <!-- /ko -->    <div class=\x22col-xs-10\x22>        <input type=\x22text\x22 data-bind=\x22value: $root.utl.localName($root.utl.coLocal())\x22 class=\x22form-control\x22>    </div></div>";
	ko.templates["coschool"] = "<div class=\x22form-group\x22>    <!-- ko if: options.label -->    <label class=\x22control-label col-xs-2\x22  data-bind=\x22text: options.label, attr: {required: $root.utl.isTrue(options.required)}\x22></label>    <!-- /ko -->    <div class=\x22col-xs-10\x22>        <select data-bind=\x22attr: { coschoolselector: true, initialValue: value, name: $root.utl.name($parents)},                         value: $root.utl.coSchool,                         disable: $root.utl.isDisabled(options.disabled),                         foreach: $root.utl.filteredCoSchoolList\x22>            <option data-bind=\x22value: id, text:name, attr:{ selected: id == $parent.value}\x22></option>        </select>        <!-- ko if: !$root.utl.filteredSchoolList().length -->        <i class=\x22fa fa-refresh fa-spin\x22></i>        <!--<div>loading</div>-->        <!-- /ko -->    </div></div>";
	ko.templates["coschool.print"] = "<div class=\x22form-group\x22>    <!-- ko if: options.label -->    <label class=\x22control-label col-xs-2\x22 data-bind=\x22attr: {for: name}, text: options.label, attr: {required: $root.utl.isTrue(options.required)}\x22></label>    <!-- /ko -->    <div class=\x22col-xs-10\x22>        <input type=\x22text\x22 data-bind=\x22value: $root.utl.schoolName($root.utl.coSchool())\x22 class=\x22form-control\x22>    </div></div>";
	ko.templates["coschoolboard"] = "<div class=\x22form-group\x22>    <!-- ko if: options.label -->    <label class=\x22control-label col-xs-2\x22 data-bind=\x22text: options.label, attr: {required: $root.utl.isTrue(options.required)}\x22></label>    <!-- /ko -->    <div class=\x22col-xs-4\x22>        <select data-bind=\x22attr: { coschoolboardselector: true, name: $root.utl.name($parents), initialValue: value },							value: $root.utl.coSchoolboard,							disable: $root.utl.isDisabled(options.disabled), 							foreach: $root.utl.preparedBoardSchoolList\x22>            <option data-bind=\x22value: m_Item1, text:m_Item2, attr:{ selected: m_Item1 == $parent.value}\x22></option>        </select>  </div></div>";
	ko.templates["coschoolboard.print"] = "<div class=\x22form-group\x22>    <!-- ko if: options.label -->    <label class=\x22col-xs-2 control-label\x22 data-bind=\x22attr: {for: name}, text: options.label, attr: {required: $root.utl.isTrue(options.required)}\x22></label>    <!-- /ko -->    <div class=\x22col-xs-10\x22>        <input type=\x22text\x22 data-bind=\x22value: $root.utl.schoolBoardName($root.utl.coSchoolboard())\x22 class=\x22form-control\x22>    </div></div>";
	ko.templates["datepicker"] = "<!-- Text input--><div class=\x22form-group ow_datepicker-form-group\x22>    <!-- ko if: options.label -->    <label class=\x22control-label col-xs-2\x22  data-bind=\x22text: options.label, attr: {required: $root.utl.isTrue(options.required)}\x22></label>    <!-- /ko -->    <div class=\x22col-xs-3\x22>        <div class=\x22input-group date\x22 data-bind=\x22css: {  ow_disabled: $root.utl.isTrue(options.disabled) }\x22>            <input data-provide=\x22datepicker\x22                    data-bind=\x22attr: {id:name, name: $root.utl.name($parents), placeholder: options.placeholderText,                    required: $root.utl.isTrue(options.required), minlength: options.minlength, maxlength: options.maxlength },                     value: value, disable: $root.utl.isDisabled(options.disabled)\x22                    class=\x22form-control datepicker\x22>            <span class=\x22input-group-addon\x22><i class=\x22glyphicon glyphicon-th\x22></i></span>            <!-- not a requirement <p class=\x22help-block\x22 data-bind=\x22text: options.help\x22></p>-->        </div>    </div></div>";
	ko.templates["documentlistofcheckboxes"] = "<!-- Multiple Checkboxes (inline) --><div class=\x22form-group\x22 data-bind=\x22visible: checkboxValues && checkboxValues.length > 0\x22>    <!-- ko if: options.label -->    <label class=\x22control-label col-xs-2\x22  data-bind=\x22html: options.label, attr: {required: $root.utl.isTrue(options.required)}\x22> </label>     <!-- /ko -->    <div class=\x22col-xs-10\x22 data-bind=\x22foreach: checkboxValues\x22>        <label class=\x22checkbox inline \x22 data-bind=\x22css: { 'checkbox-inline': $parent.options.inline }, visible: !($data.options && $root.utl.isDisabled($data.options.disabled))\x22>            <input type=\x22checkbox\x22 data-bind=\x22value: value, attr: { name: $root.utl.parentsName($parents) }, checked: $root.utl.isTrue($data.checked), disable: $data.options && $root.utl.isDisabled($data.options.disabled)\x22>            <!-- ko  text: label-->            <!-- knockout does not seem to like text binding on input label element -->            <!-- /ko -->        </label>    </div></div>";
	ko.templates["emailinput"] = "<!-- Text input--><div class=\x22form-group\x22>    <!-- ko if: options.label -->    <label class=\x22control-label col-xs-2\x22  data-bind=\x22text: options.label, attr: {required: $root.utl.isTrue(options.required)}\x22></label>    <!-- /ko -->    <div class=\x22col-xs-4\x22>        <input  type=\x22email\x22                data-bind=\x22attr: {id:name, name: $root.utl.name($parents), placeholder: options.placeholderText,                required: $root.utl.isTrue(options.required), minlength: options.minlength, maxlength: options.maxlength },                 value: value, disable: $root.utl.isDisabled(options.disabled)\x22                class=\x22form-control email\x22>        <!-- not a requirement <p class=\x22help-block\x22 data-bind=\x22text: options.help\x22></p>-->    </div></div>";
	ko.templates["extendedlistofcheckboxes"] = "<!-- Multiple Checkboxes (inline) --><!-- Same Id name as the label --><!--<div class=\x22form-group\x22 data-bind=\x22attr: { 'data-idName': $root.utl.toQs(id) }\x22>--> <div class=\x22form-group\x22>    <div class=\x22col-xs-12 required-message-container\x22 style=\x22display:none;\x22>        <label class=\x22control-label\x22 data-bind=\x22attr: { 'data-idName': $root.utl.toQs(id) }\x22></label>    </div>    <label class=\x22control-label \x22 data-bind=\x22text: options.label, attr: {required: $root.utl.isTrue(options.required)}\x22></label>    <div>        <table class=\x22extendedlistofcheckboxes\x22 width=\x22100%\x22 data-bind=\x22foreach: checkboxValues\x22>            <tr style='display:none' name=\x22grouplabel\x22 data-bind=\x22attr: {grp : group}\x22>                <th class=\x22group-checkboxes-header\x22 style='padding-top:10px' data-bind=\x22text: group, attr: { colspan: $root.utl.count(labelColumns) }\x22></th>            </tr>            <tr style='display:none' name=\x22reference\x22>                <td data-bind=\x22attr: { colspan: $root.utl.count(labelColumns), ref: $root.utl.objectToString(reference) }\x22></td>            </tr>            <tr style='display:none' name=\x22groupcolumns\x22 data-bind=\x22attr: {grp : group}, foreach: $parent.options.labelColumns\x22>                <td style='font-weight:bold' data-bind=\x22text:label\x22></td>            </tr>            <tr data-bind=\x22foreach: labelColumns\x22>                <!-- ko if: label!=\x22checkbox\x22 -->                <td data-bind=\x22text:label\x22></td>                <!-- /ko -->                <!-- ko if: label==\x22checkbox\x22 -->                <td class=\x22group-checkboxes-check\x22>                    <input type=\x22checkbox\x22 data-bind=\x22value: $parent.value, attr: {name: $root.utl.grandParentsName($parents)}, checked: $parent.checked, disable: $root.utl.isDisabled($parents[2].options.disabled)\x22 />                </td>                <!-- /ko -->            </tr>        </table>    </div></div>";
	ko.templates["extendedlistofcheckboxes.print"] = "<!-- Multiple Checkboxes (inline) --><div class=\x22form-group\x22>    <label class=\x22control-label \x22 data-bind=\x22text: options.label, attr: {required: $root.utl.isTrue(options.required)}\x22> </label>    <div>        <table class=\x22extendedlistofcheckboxes\x22 width=\x22100%\x22 data-bind=\x22foreach: checkboxValues\x22>            <tr>                <td style='padding-top:10px' data-bind=\x22text:group\x22></td>            </tr>            <tr data-bind=\x22foreach: $parent.options.labelColumns\x22>                <td style='font-weight:bold' data-bind=\x22text:label\x22></td>            </tr>            <tr data-bind=\x22foreach: labelColumns\x22>                <!-- ko if: label!=\x22checkbox\x22 -->                <td data-bind=\x22text:label\x22></td>                <!-- /ko -->                <!-- ko if: label==\x22checkbox\x22 -->                <td>                    <input type=\x22checkbox\x22 data-bind=\x22value: $parent.value, checked: $parent.checked, disable: $root.utl.isDisabled($parents[2].options.disabled)\x22 />                </td>                <!-- /ko -->            </tr>        </table>    </div></div>";
	ko.templates["fileupload"] = "<div class=\x22form-group\x22>    <!-- ko if: options.label -->  <label class=\x22control-label col-xs-2\x22 data-bind=\x22attr:{for: name}, text: options.label, attr: {required: $root.utl.isTrue(options.required)}\x22></label>    <!-- /ko -->    <div class=\x22col-xs-4\x22>        <input type=\x22file\x22 data-bind=\x22attr:{id: name, name: $root.utl.name($parents)}, disable: $root.utl.isDisabled(options.disabled)\x22>        <!-- not a requirement <p class=\x22help-block\x22>Example block-level help text here.</p> -->    </div></div>";
	ko.templates["fileuploaddocument"] = "<div class=\x22form-group upload-document-container\x22>    <!-- ko if: options.label -->	<label class=\x22control-label col-xs-2\x22 data-bind=\x22attr:{for: name}, text: options.label, attr: {required: $root.utl.isTrue(options.required)}\x22></label>    <!-- /ko -->    <div class=\x22col-xs-10\x22>		<label class=\x22inline\x22>			<span class=\x22btn btn-success fileinput-button pull-left\x22>				<i class=\x22fa fa-plus\x22></i>				<span>Select file</span>				<input type=\x22file\x22 name=\x22file\x22 style=\x22display: none;\x22/>				<input type=\x22hidden\x22 name=\x22rawFile\x22 />			</span>			<div class=\x22selected-file\x22 style=\x22display: none;\x22>Test</div>		</label>		<div>			<input type=\x22text\x22 name=\x22description\x22 class=\x22form-control\x22 placeholder=\x22Document description\x22>		</div>		<p class=\x22help-block\x22>Select a document from your computer and type in a description.</p>		<button type=\x22button\x22 id=\x22upload-tg\x22 class=\x22btn btn-primary pull-left\x22 data-bind=\x22click: $root.utl.uploadDocument\x22>Attach</button>        <p class=\x22clearfix\x22></p>        <div class=\x22alert-container\x22></div>    </div></div><div class=\x22form-group\x22 data-bind=\x22visible: $root.utl.haveDocuments()\x22>    <label class=\x22control-label col-xs-2\x22>Documents to upload</label>    <div class=\x22col-xs-10\x22>		<table class=\x22table table-striped table-bordered table-hover\x22>			<thead>				<tr>					<td>File Name</td>					<td>Description</td>					<td>Type</td>					<td>Size (MB)</td>					<td>Actions</td>				</tr>			</thead>			<tbody>				<!-- ko foreach: $root.utl.documents -->				<tr>					<td data-bind=\x22text: fileName\x22></td>					<td data-bind=\x22text: subject\x22></td>					<td data-bind=\x22text: mimeType\x22></td>					<td data-bind=\x22documentSize: fileSize\x22></td>					<td>						<a href=\x22javascript:void(0);\x22 data-bind=\x22click: $root.utl.deleteDocument\x22>							<i class=\x22fa fa-times\x22></i>						</a>					</td>				</tr>				<!-- /ko -->			</tbody>		</table>    </ul></div>";
	ko.templates["hidden"] = "<!--hidden field--><input type=\x22hidden\x22           data-bind=\x22attr: {id:name, name: $root.utl.name($parents)},                     value: value\x22           class=\x22ow_hidden\x22>";
	ko.templates["htmlinput"] = "<!-- Text area--><div class=\x22form-group\x22>    <!-- ko if: options.label -->    <label class=\x22control-label col-xs-2\x22  data-bind=\x22text: options.label, attr: {required: $root.utl.isTrue(options.required)}\x22></label>    <!-- /ko -->    <div class=\x22col-xs-10\x22 style=\x22height: 337px\x22>        <textarea data-bind=\x22attr: {id:name, name: $root.utl.name($parents), placeholder: options.placeholderText,                            required: $root.utl.isTrue(options.required), minlength: options.minlength, maxlength: options.maxlength},                            text: value, disable: $root.utl.isDisabled(options.disabled)  \x22                  class=\x22form-control htmlEditor\x22 rows=\x224\x22></textarea>        <!-- not a requirement <p class=\x22help-block\x22 data-bind=\x22text: options.help\x22></p>-->    </div></div>";
	ko.templates["legend"] = "<!-- Arbitrary html text--><div class=\x22form-group ow_legend\x22>    <div data-bind=\x22html: value\x22></div></div>";
	ko.templates["listofcheckboxes"] = "<!-- Multiple Checkboxes (inline) --><!-- ko if: options.minDocs --> <div class=\x22col-xs-12 checkbox-list-alert-container\x22></div><input type=\x22hidden\x22 id=\x22supporting-documents-minimum\x22 data-bind=\x22value: options.minDocs\x22 /><!-- /ko --><div class=\x22form-group\x22 data-bind=\x22attr: { 'data-idName': $root.utl.toQs(id) }, visible: checkboxValues && checkboxValues.length > 0\x22>	<!-- ko if: options.label -->	<label class=\x22control-label col-xs-2\x22 data-bind=\x22html: options.label, attr: {required: $root.utl.isTrue(options.required)}\x22></label>	<!-- /ko --> 	<div class=\x22col-xs-10\x22 data-bind=\x22foreach: checkboxValues\x22>        <label class=\x22checkbox inline \x22 data-bind=\x22css: {'checkbox-inline': $parent.options.inline}\x22>            <input type=\x22checkbox\x22 data-bind=\x22value: value, attr: {name: $root.utl.parentsName($parents)}, checked: $data.checked, disable: $root.utl.isDisabled($parent.options.disabled)\x22>            <!-- ko  text: label-->            <!-- knockout does not seem to like text binding on input label element -->            <!-- /ko -->        </label>    </div></div>";
	ko.templates["listofradios"] = "<!-- Multiple Radios --><div class=\x22form-group\x22>    <!-- ko if: options.label -->    <label class=\x22col-xs-2 control-label\x22 data-bind=\x22attr: {for: name}, html: options.label, attr: {required: $root.utl.isTrue(options.required)}\x22> </label>    <!-- /ko -->    <div class=\x22col-xs-10\x22 data-bind=\x22foreach: radioValues\x22>        <label class=\x22radio \x22 data-bind=\x22css: {'radio-inline': $parent.options.inline}\x22>            <input type=\x22radio\x22 data-bind=\x22attr: {name: $root.utl.parentsName($parents), value: value, required: $root.utl.isTrue($parent.options.required)}, checked: $parent.value, disable: $root.utl.isDisabled($parent.options.disabled)\x22>            <!-- ko  text: label-->            <!--knockout does not seem to like text binding on input label element -->            <!-- /ko -->        </label>    </div></div>";
	ko.templates["local"] = "<div class=\x22form-group\x22>    <!-- ko if: options.label -->    <label class=\x22control-label col-xs-2\x22 data-bind=\x22attr: {for: name}, text: options.label, attr: {required: $root.utl.isTrue(options.required)}\x22></label>    <!-- /ko -->    <div class=\x22col-xs-10\x22>        <select data-bind=\x22value: $root.utl.local, attr: {name: $root.utl.name($parents), localselector: true, initialValue: value}, disable: $root.utl.isDisabled(options.disabled), foreach: $root.utl.filteredLocalList\x22>            <option data-bind=\x22value: id, text:name, attr:{ selected: id == $parent.value}\x22></option>        </select>        <!-- ko if: !$root.utl.filteredLocalList().length  -->        <i class=\x22fa fa-refresh fa-spin\x22></i>        <!-- /ko -->    </div></div>";
	ko.templates["local.print"] = "<div class=\x22form-group\x22>    <!-- ko if: options.label -->    <label class=\x22control-label col-xs-2\x22 data-bind=\x22attr: {for: name}, text: options.label, attr: {required: $root.utl.isTrue(options.required)}\x22></label>    <!-- /ko -->    <div class=\x22col-xs-10\x22>        <input type=\x22text\x22 data-bind=\x22value: $root.utl.localName($root.utl.local())\x22 class=\x22form-control\x22>    </div></div>";
	ko.templates["numberinput"] = "<!-- Text input--><div class=\x22form-group\x22>    <!-- ko if: options.label -->    <label class=\x22control-label col-xs-2\x22  data-bind=\x22text: options.label, attr: {required: $root.utl.isTrue(options.required)}\x22></label>    <!-- /ko -->    <div class=\x22col-xs-2\x22>        <input  type=\x22number\x22                data-bind=\x22attr: {id:name, name: $root.utl.name($parents), placeholder: options.placeholderText,                required: $root.utl.isTrue(options.required), min: options.min, max: options.max, step: options.step },                 value: value, disable: $root.utl.isDisabled(options.disabled)\x22                class=\x22form-control numeric\x22>        <!-- not a requirement <p class=\x22help-block\x22 data-bind=\x22text: options.help\x22></p>-->    </div></div>";
	ko.templates["panel"] = "<!-- ko if: $root.utl.isComponentVisible(id) --><div class=\x22panel panel-default\x22 data-bind=\x22attr: { id: 'pnl-' + $root.utl.toQs(id) }\x22>    <div class=\x22panel-heading\x22 data-bind=\x22attr:{style: 'background-color:'+options.backgroundColor}\x22>        <h4 class=\x22panel-title\x22>            <a data-toggle=\x22collapse\x22               data-bind=\x22text: options.title, attr:{href:'#'+name}, css: {collapsed: !options.open}\x22>            </a>        </h4>    </div>    <div class=\x22panel-collapse collapse\x22 data-bind=\x22attr:{id: name}, css: {in: options.open}\x22>        <div class=\x22panel-body\x22>            <!--items contained in panel are rendered here-->            <div data-bind=\x22template: { name: 'renderAllChildren', data: $data }\x22></div>        </div>    </div></div><!-- /ko -->";
	ko.templates["phoneinput"] = "<!-- Text input--><div class=\x22form-group\x22>    <!-- ko if: options.label -->    <label class=\x22control-label col-xs-2\x22  data-bind=\x22text: options.label, attr: {required: $root.utl.isTrue(options.required)}\x22></label>    <!-- /ko -->    <div class=\x22col-xs-4\x22>        <input  type=\x22phone\x22                data-bind=\x22attr: {id:name, name: $root.utl.name($parents), placeholder: options.placeholderText,                required: $root.utl.isTrue(options.required), minlength: options.minlength, maxlength: options.maxlength },                 value: value, disable: $root.utl.isDisabled(options.disabled)\x22                class=\x22form-control phone\x22>        <!-- not a requirement <p class=\x22help-block\x22 data-bind=\x22text: options.help\x22></p>-->    </div></div>";
	ko.templates["previewDelegationLimit"] = "<!-- Text input--><div class=\x22form-group\x22>    <!-- ko if: options.label -->    <label class=\x22control-label col-xs-2\x22  data-bind=\x22text: options.label, attr: {required: $root.utl.isTrue(options.required)}\x22></label>    <!-- /ko -->    <div class=\x22col-xs-10\x22>        <input type=\x22text\x22 data-bind=\x22attr: {name: $root.utl.name($parents), placeholder: options.placeholderText}, value: $root.utl.previewDelegationLimit, disable: true\x22 class=\x22form-control\x22 />        <!-- not a requirement <p class=\x22help-block\x22 data-bind=\x22text: options.help\x22></p>-->    </div></div>";
	ko.templates["previewDelegationLimit.print"] = "<div class=\x22form-group\x22>    <!-- ko if: options.label -->    <label class=\x22control-label col-xs-2\x22 data-bind=\x22attr: {for: name}, text: options.label, attr: {required: $root.utl.isTrue(options.required)}\x22></label>    <!-- /ko -->    <div class=\x22col-xs-10\x22>        <input  type=\x22text\x22 data-bind=\x22value: $root.utl.previewDelegationLimit()\x22 class=\x22form-control\x22>    </div></div>";
	ko.templates["previewLocalName"] = "<!-- Text input--><div class=\x22form-group\x22>    <!-- ko if: options.label -->    <label class=\x22control-label col-xs-2\x22  data-bind=\x22text: options.label, attr: {required: $root.utl.isTrue(options.required)}\x22></label>    <!-- /ko -->    <div class=\x22col-xs-10\x22>        <input type=\x22text\x22 data-bind=\x22attr: {name: $root.utl.name($parents), placeholder: options.placeholderText}, value: $root.utl.previewLocalName, disable: true\x22 class=\x22form-control\x22 />        <!-- not a requirement <p class=\x22help-block\x22 data-bind=\x22text: options.help\x22></p>-->    </div></div>";
	ko.templates["previewLocalName.print"] = "<div class=\x22form-group\x22>    <!-- ko if: options.label -->    <label class=\x22control-label col-xs-2\x22 data-bind=\x22attr: {for: name}, text: options.label, attr: {required: $root.utl.isTrue(options.required)}\x22></label>    <!-- /ko -->    <div class=\x22col-xs-10\x22>        <input  type=\x22text\x22 data-bind=\x22value: $root.utl.previewLocalName()\x22 class=\x22form-control\x22>    </div></div>";
	ko.templates["previewRtLocalName"] = "<!-- Text input--><div class=\x22form-group\x22>    <!-- ko if: options.label -->    <label class=\x22control-label col-xs-2\x22  data-bind=\x22text: options.label, attr: {required: $root.utl.isTrue(options.required)}\x22></label>    <!-- /ko -->    <div class=\x22col-xs-10\x22>        <input type=\x22text\x22 data-bind=\x22attr: {name: $root.utl.name($parents), placeholder: options.placeholderText}, value: $root.utl.previewRtLocalName, disable: true\x22 class=\x22form-control\x22 />        <!-- not a requirement <p class=\x22help-block\x22 data-bind=\x22text: options.help\x22></p>-->    </div></div>";
	ko.templates["previewRtLocalName.print"] = "<div class=\x22form-group\x22>    <!-- ko if: options.label -->    <label class=\x22control-label col-xs-2\x22 data-bind=\x22attr: {for: name}, text: options.label, attr: {required: $root.utl.isTrue(options.required)}\x22></label>    <!-- /ko -->    <div class=\x22col-xs-10\x22>        <input  type=\x22text\x22 data-bind=\x22value: $root.utl.previewRtLocalName()\x22 class=\x22form-control\x22>    </div></div>";
	ko.templates["previewRtSchoolCity"] = "<!-- Text input--><div class=\x22form-group\x22>    <!-- ko if: options.label -->    <label class=\x22control-label col-xs-2\x22  data-bind=\x22text: options.label, attr: {required: $root.utl.isTrue(options.required)}\x22></label>    <!-- /ko -->    <div class=\x22col-xs-10\x22>        <input  type=\x22text\x22                data-bind=\x22attr: { name: $root.utl.name($parents), placeholder: options.placeholderText},                 value: $root.utl.previewRtSchoolCity, disable: true\x22                class=\x22form-control\x22>        <!-- not a requirement <p class=\x22help-block\x22 data-bind=\x22text: options.help\x22></p>-->    </div></div>";
	ko.templates["previewRtSchoolCity.print"] = "<div class=\x22form-group\x22>    <!-- ko if: options.label -->    <label class=\x22control-label col-xs-2\x22 data-bind=\x22attr: {for: name}, text: options.label, attr: {required: $root.utl.isTrue(options.required)}\x22></label>    <!-- /ko -->    <div class=\x22col-xs-10\x22>        <input  type=\x22text\x22 data-bind=\x22value: $root.utl.previewRtSchoolCity()\x22 class=\x22form-control\x22>    </div></div>";
	ko.templates["previewRtSchoolEmail"] = "<!-- Text input--><div class=\x22form-group\x22>    <!-- ko if: options.label -->    <label class=\x22control-label col-xs-2\x22  data-bind=\x22text: options.label, attr: {required: $root.utl.isTrue(options.required)}\x22></label>    <!-- /ko -->    <div class=\x22col-xs-10\x22>        <input  type=\x22text\x22                data-bind=\x22attr: { previewrtschoolemailselector: true, name: $root.utl.name($parents), placeholder: options.placeholderText, initialValue: value},                 value: $root.utl.previewRtSchoolEmail, disable: $root.utl.isDisabled(options.disabled)\x22                class=\x22form-control\x22>        <!-- not a requirement <p class=\x22help-block\x22 data-bind=\x22text: options.help\x22></p>-->    </div></div>";
	ko.templates["previewRtSchoolEmail.print"] = "<div class=\x22form-group\x22>    <!-- ko if: options.label -->    <label class=\x22control-label col-xs-2\x22 data-bind=\x22attr: {for: name}, text: options.label, attr: {required: $root.utl.isTrue(options.required)}\x22></label>    <!-- /ko -->    <div class=\x22col-xs-10\x22>        <input  type=\x22text\x22 data-bind=\x22value: $root.utl.previewRtSchoolEmail()\x22 class=\x22form-control\x22>    </div></div>";
	ko.templates["previewRtSchoolFax"] = "<!-- Text input--><div class=\x22form-group\x22>    <!-- ko if: options.label -->    <label class=\x22control-label col-xs-2\x22  data-bind=\x22text: options.label, attr: {required: $root.utl.isTrue(options.required)}\x22></label>    <!-- /ko -->    <div class=\x22col-xs-10\x22>        <input  type=\x22text\x22                data-bind=\x22attr: { previewrtschoolfaxselector: true, name: $root.utl.name($parents), placeholder: options.placeholderText, initialValue: value},                 value: $root.utl.previewRtSchoolFax, disable: $root.utl.isDisabled(options.disabled)\x22                class=\x22form-control\x22>        <!-- not a requirement <p class=\x22help-block\x22 data-bind=\x22text: options.help\x22></p>-->    </div></div>";
	ko.templates["previewRtSchoolFax.print"] = "<div class=\x22form-group\x22>    <!-- ko if: options.label -->    <label class=\x22control-label col-xs-2\x22 data-bind=\x22attr: {for: name}, text: options.label, attr: {required: $root.utl.isTrue(options.required)}\x22></label>    <!-- /ko -->    <div class=\x22col-xs-10\x22>        <input  type=\x22text\x22 data-bind=\x22value: $root.utl.previewRtSchoolFax()\x22 class=\x22form-control\x22>    </div></div>";
	ko.templates["previewRtSchoolName"] = "<!-- Text input--><div class=\x22form-group\x22>    <!-- ko if: options.label -->    <label class=\x22control-label col-xs-2\x22  data-bind=\x22text: options.label, attr: {required: $root.utl.isTrue(options.required)}\x22></label>    <!-- /ko -->    <div class=\x22col-xs-10\x22>        <input  type=\x22text\x22                data-bind=\x22attr: {name: $root.utl.name($parents), placeholder: options.placeholderText},                 value: $root.utl.previewRtSchoolName, disable: true\x22                class=\x22form-control\x22>        <!-- not a requirement <p class=\x22help-block\x22 data-bind=\x22text: options.help\x22></p>-->    </div></div>";
	ko.templates["previewRtSchoolName.print"] = "<div class=\x22form-group\x22>    <!-- ko if: options.label -->    <label class=\x22control-label col-xs-2\x22 data-bind=\x22attr: {for: name}, text: options.label, attr: {required: $root.utl.isTrue(options.required)}\x22></label>    <!-- /ko -->    <div class=\x22col-xs-10\x22>        <input  type=\x22text\x22 data-bind=\x22value: $root.utl.previewRtSchoolName()\x22 class=\x22form-control\x22>    </div></div>";
	ko.templates["previewRtSchoolPhone"] = " <!-- Text input--><div class=\x22form-group\x22>    <!-- ko if: options.label -->    <label class=\x22control-label col-xs-2\x22  data-bind=\x22text: options.label, attr: {required: $root.utl.isTrue(options.required)}\x22></label>    <!-- /ko -->    <div class=\x22col-xs-10\x22>        <input  type=\x22text\x22                data-bind=\x22attr: { name: $root.utl.name($parents), placeholder: options.placeholderText},                 value: $root.utl.previewRtSchoolPhone, disable: true\x22                class=\x22form-control\x22>        <!-- not a requirement <p class=\x22help-block\x22 data-bind=\x22text: options.help\x22></p>-->    </div></div>";
	ko.templates["previewRtSchoolPhone.print"] = "<div class=\x22form-group\x22>    <!-- ko if: options.label -->    <label class=\x22control-label col-xs-2\x22 data-bind=\x22attr: {for: name}, text: options.label, attr: {required: $root.utl.isTrue(options.required)}\x22></label>    <!-- /ko -->    <div class=\x22col-xs-10\x22>        <input  type=\x22text\x22 data-bind=\x22value: $root.utl.previewRtSchoolPhone()\x22 class=\x22form-control\x22>    </div></div>";
	ko.templates["previewSchoolCity"] = "<!-- Text input--><div class=\x22form-group\x22>    <!-- ko if: options.label -->    <label class=\x22control-label col-xs-2\x22  data-bind=\x22text: options.label, attr: {required: $root.utl.isTrue(options.required)}\x22></label>    <!-- /ko -->    <div class=\x22col-xs-10\x22>        <input  type=\x22text\x22                data-bind=\x22attr: { name: $root.utl.name($parents), placeholder: options.placeholderText},                 value: $root.utl.previewSchoolCity, disable: true\x22                class=\x22form-control\x22>        <!-- not a requirement <p class=\x22help-block\x22 data-bind=\x22text: options.help\x22></p>-->    </div></div>";
	ko.templates["previewSchoolCity.print"] = "<div class=\x22form-group\x22>    <!-- ko if: options.label -->    <label class=\x22control-label col-xs-2\x22 data-bind=\x22attr: {for: name}, text: options.label, attr: {required: $root.utl.isTrue(options.required)}\x22></label>    <!-- /ko -->    <div class=\x22col-xs-10\x22>        <input  type=\x22text\x22 data-bind=\x22value: $root.utl.previewSchoolCity()\x22 class=\x22form-control\x22>    </div></div>";
	ko.templates["previewSchoolEmail"] = "<!-- Text input--><div class=\x22form-group\x22>    <!-- ko if: options.label -->    <label class=\x22control-label col-xs-2\x22  data-bind=\x22text: options.label, attr: {required: $root.utl.isTrue(options.required)}\x22></label>    <!-- /ko -->    <div class=\x22col-xs-10\x22>        <input  type=\x22text\x22                data-bind=\x22attr: { previewschoolemailselector: true, name: $root.utl.name($parents), placeholder: options.placeholderText, initialValue: value},                 value: $root.utl.previewSchoolEmail, disable: $root.utl.isDisabled(options.disabled)\x22                class=\x22form-control\x22>        <!-- not a requirement <p class=\x22help-block\x22 data-bind=\x22text: options.help\x22></p>-->    </div></div>";
	ko.templates["previewSchoolEmail.print"] = "<div class=\x22form-group\x22>    <!-- ko if: options.label -->    <label class=\x22control-label col-xs-2\x22 data-bind=\x22attr: {for: name}, text: options.label, attr: {required: $root.utl.isTrue(options.required)}\x22></label>    <!-- /ko -->    <div class=\x22col-xs-10\x22>        <input  type=\x22text\x22 data-bind=\x22value: $root.utl.previewSchoolEmail()\x22 class=\x22form-control\x22>    </div></div>";
	ko.templates["previewSchoolFax"] = "<!-- Text input--><div class=\x22form-group\x22>    <!-- ko if: options.label -->    <label class=\x22control-label col-xs-2\x22  data-bind=\x22text: options.label, attr: {required: $root.utl.isTrue(options.required)}\x22></label>    <!-- /ko -->    <div class=\x22col-xs-10\x22>        <input  type=\x22text\x22                data-bind=\x22attr: { previewschoolfaxselector: true, name: $root.utl.name($parents), placeholder: options.placeholderText, initialValue: value},                 value: $root.utl.previewSchoolFax, disable: $root.utl.isDisabled(options.disabled)\x22                class=\x22form-control\x22>        <!-- not a requirement <p class=\x22help-block\x22 data-bind=\x22text: options.help\x22></p>-->    </div></div>";
	ko.templates["previewSchoolFax.print"] = "<div class=\x22form-group\x22>    <!-- ko if: options.label -->    <label class=\x22control-label col-xs-2\x22 data-bind=\x22attr: {for: name}, text: options.label, attr: {required: $root.utl.isTrue(options.required)}\x22></label>    <!-- /ko -->    <div class=\x22col-xs-10\x22>        <input  type=\x22text\x22 data-bind=\x22value: $root.utl.previewSchoolFax()\x22 class=\x22form-control\x22>    </div></div>";
	ko.templates["previewSchoolName"] = "<!-- Text input--><div class=\x22form-group\x22>    <!-- ko if: options.label -->    <label class=\x22control-label col-xs-2\x22  data-bind=\x22text: options.label, attr: {required: $root.utl.isTrue(options.required)}\x22></label>    <!-- /ko -->    <div class=\x22col-xs-10\x22>        <input  type=\x22text\x22                data-bind=\x22attr: {name: $root.utl.name($parents), placeholder: options.placeholderText},                 value: $root.utl.previewSchoolName, disable: true\x22                class=\x22form-control\x22>        <!-- not a requirement <p class=\x22help-block\x22 data-bind=\x22text: options.help\x22></p>-->    </div></div>";
	ko.templates["previewSchoolName.print"] = "<div class=\x22form-group\x22>    <!-- ko if: options.label -->    <label class=\x22control-label col-xs-2\x22 data-bind=\x22attr: {for: name}, text: options.label, attr: {required: $root.utl.isTrue(options.required)}\x22></label>    <!-- /ko -->    <div class=\x22col-xs-10\x22>        <input  type=\x22text\x22 data-bind=\x22value: $root.utl.previewSchoolName()\x22 class=\x22form-control\x22>    </div></div>";
	ko.templates["previewSchoolPhone"] = " <!-- Text input--><div class=\x22form-group\x22>    <!-- ko if: options.label -->    <label class=\x22control-label col-xs-2\x22  data-bind=\x22text: options.label, attr: {required: $root.utl.isTrue(options.required)}\x22></label>    <!-- /ko -->    <div class=\x22col-xs-10\x22>        <input  type=\x22text\x22                data-bind=\x22attr: { name: $root.utl.name($parents), placeholder: options.placeholderText},                 value: $root.utl.previewSchoolPhone, disable: true\x22                class=\x22form-control\x22>        <!-- not a requirement <p class=\x22help-block\x22 data-bind=\x22text: options.help\x22></p>-->    </div></div>";
	ko.templates["previewSchoolPhone.print"] = "<div class=\x22form-group\x22>    <!-- ko if: options.label -->    <label class=\x22control-label col-xs-2\x22 data-bind=\x22attr: {for: name}, text: options.label, attr: {required: $root.utl.isTrue(options.required)}\x22></label>    <!-- /ko -->    <div class=\x22col-xs-10\x22>        <input  type=\x22text\x22 data-bind=\x22value: $root.utl.previewSchoolPhone()\x22 class=\x22form-control\x22>    </div></div>";
	ko.templates["previewTicketPrice"] = "<!-- Text input--><div class=\x22form-group\x22>    <!-- ko if: options.label -->    <label class=\x22control-label col-xs-2\x22  data-bind=\x22text: options.label, attr: {required: $root.utl.isTrue(options.required)}\x22></label>    <!-- /ko -->    <div class=\x22col-xs-10\x22>        <input  type=\x22hidden\x22                data-bind=\x22attr: {id:name, name: $root.utl.name($parents)},                           value: value, disable: true\x22                class=\x22hidden ow_ticketPrice\x22>        $<label class=\x22control-label\x22 data-bind=\x22text: value\x22></label>        <!-- not a requirement <p class=\x22help-block\x22 data-bind=\x22text: options.help\x22></p>-->    </div></div>";
	ko.templates["previewTotalTicketCost"] = "<!-- Text input--><div class=\x22form-group\x22>    <!-- ko if: options.label -->    <label class=\x22control-label col-xs-2\x22  data-bind=\x22text: options.label, attr: {required: $root.utl.isTrue(options.required)}\x22></label>    <!-- /ko -->    <div class=\x22col-xs-10\x22>        $<label class=\x22control-label ow_totalTicketCost\x22></label>        <!-- not a requirement <p class=\x22help-block\x22 data-bind=\x22text: options.help\x22></p>-->    </div></div>";
	ko.templates["renderAllChildren"] = "<div data-bind=\x22foreach: children\x22 class=\x22children-list\x22>    <!-- ko if: $root.enableDebugOutput -->    <!--this is just for debugging-->    <h6 style=\x22background-color: #eeeeee\x22        data-bind=\x22html: 'DEBUG: Rendering input of type <i>' +type+ '</i> using data: <code>' + JSON.stringify($data) +'</code>'\x22></h6>    <br>    <!-- /ko -->    <!--this renders a child element-->    <div data-bind=\x22template: { name: type, data: $data }\x22></div></div>";
	ko.templates["repeatableSection"] = "<!--indented section container for form element, can be collapsed/open by a checkbox using enablesSection option on checkbox--><div data-bind=\x22attr:{id: name},  css: {ow_repeatableSection:true, collapse: options.collapse}\x22>    <!--items contained in repeatable section are rendered here-->    <div data-bind=\x22foreach: $root.utl.fetchObservableChildren($data)\x22>        <div class=\x22form-group ow_repeatableSection_row\x22>            <!-- ko if: $root.enableDebugOutput -->            <!--this is just for debugging-->            <h6 style=\x22background-color: #eeeeee\x22                data-bind=\x22html: 'DEBUG: Rendering input of type <i>' +type+ '</i> using data: <code>' + JSON.stringify($data) +'</code>'\x22></h6>            <br>            <!-- /ko -->            <!--this renders a child element-->            <div data-bind=\x22attr:{id: $parent.name+'#'+$index()},                            template: { name: type, data: $root.utl.addIndex($data,$index() ) }\x22></div>        </div>        <div class=\x22form-group ow_add\x22 data-bind=\x22if: !$root.utl.isDisabled()\x22>            <button type=\x22button\x22 class=\x22ow_delete btn btn-danger btn-sm pull-right\x22                    data-bind=\x22click: $parent.removeChild\x22>                <span class=\x22glyphicon glyphicon-trash\x22></span> Delete            </button>        </div>        <hr>    </div>    <div class=\x22form-group ow_add\x22 data-bind=\x22if: !$root.utl.isDisabled()\x22>        <button type=\x22button\x22 class=\x22ow_add btn btn-success btn-sm pull-left\x22                data-bind=\x22click: addChild\x22>            <span class=\x22glyphicon glyphicon-plus\x22></span> Add        </button>    </div></div>";
	ko.templates["repeatableSection.print"] = "<!--indented section container for form element, can be collapsed/open by a checkbox using enablesSection option on checkbox--><div data-bind=\x22attr:{id: name},  css: {ow_repeatableSection:true, collapse: options.collapse}\x22>    <!--items contained in repeatable section are rendered here-->    <div data-bind=\x22foreach: $root.utl.repeatTemplate($data)\x22>        <div class=\x22form-group ow_repeatableSection_row\x22>            <!-- ko if: $root.enableDebugOutput -->            <!--this is just for debugging-->            <h6 style=\x22background-color: #eeeeee\x22                data-bind=\x22html: 'DEBUG: Rendering input of type <i>' +type+ '</i> using data: <code>' + JSON.stringify($data) +'</code>'\x22></h6>            <br>            <!-- /ko -->            <!--this renders a child element-->			<div data-bind=\x22attr:{id: $parent.name+'#'+$index()},							template: { name: type, data: $root.utl.addIndex($data,$index() ) }\x22></div>        </div>        <hr>    </div></div>";
	ko.templates["rtlocal"] = "<div class=\x22form-group\x22>    <!-- ko if: options.label -->    <label class=\x22control-label col-xs-2\x22 data-bind=\x22attr: {for: name}, text: options.label, attr: {required: $root.utl.isTrue(options.required)}\x22></label>    <!-- /ko -->    <div class=\x22col-xs-10\x22>        <select data-bind=\x22attr: {name: $root.utl.name($parents), rtlocalselector: true, initialValue: value},                            value: $root.utl.rtLocal,                            disable: $root.utl.isDisabled(options.disabled),                            foreach: $root.utl.filteredRtLocalList\x22>            <option data-bind=\x22value: id, text:name, attr:{ selected: id == $parent.value}\x22></option>        </select>        <!-- ko if: !$root.utl.filteredLocalList().length  -->        <i class=\x22fa fa-refresh fa-spin\x22></i>        <!-- /ko -->    </div></div>";
	ko.templates["rtlocal.print"] = "<div class=\x22form-group\x22>    <!-- ko if: options.label -->    <label class=\x22control-label col-xs-2\x22 data-bind=\x22attr: {for: name}, text: options.label, attr: {required: $root.utl.isTrue(options.required)}\x22></label>    <!-- /ko -->    <div class=\x22col-xs-10\x22>        <input type=\x22text\x22 data-bind=\x22value: $root.utl.localName($root.utl.rtLocal())\x22 class=\x22form-control\x22>    </div></div>";
	ko.templates["rtschool"] = "<div class=\x22form-group\x22>    <!-- ko if: options.label -->    <label class=\x22control-label col-xs-2\x22  data-bind=\x22text: options.label, attr: {required: $root.utl.isTrue(options.required)}\x22></label>    <!-- /ko -->    <div class=\x22col-xs-10\x22>        <select data-bind=\x22attr: { rtschoolselector: true, initialValue: value, name: $root.utl.name($parents)},                         value: $root.utl.rtSchool,                         disable: $root.utl.isDisabled(options.disabled),                         foreach: $root.utl.filteredRtSchoolList\x22>            <option data-bind=\x22value: id, text:name, attr:{ selected: id == $parent.value}\x22></option>        </select>        <!-- ko if: !$root.utl.filteredSchoolList().length -->        <i class=\x22fa fa-refresh fa-spin\x22></i>        <!--<div>loading</div>-->        <!-- /ko -->    </div></div>";
	ko.templates["rtschool.print"] = "<div class=\x22form-group\x22>    <!-- ko if: options.label -->    <label class=\x22control-label col-xs-2\x22 data-bind=\x22attr: {for: name}, text: options.label, attr: {required: $root.utl.isTrue(options.required)}\x22></label>    <!-- /ko -->    <div class=\x22col-xs-10\x22>        <input type=\x22text\x22 data-bind=\x22value: $root.utl.schoolName($root.utl.rtSchool())\x22 class=\x22form-control\x22>    </div></div>";
	ko.templates["rtschoolboard"] = "<div class=\x22form-group\x22>    <!-- ko if: options.label -->    <label class=\x22control-label col-xs-2\x22 data-bind=\x22text: options.label, attr: {required: $root.utl.isTrue(options.required)}\x22></label>    <!-- /ko -->    <div class=\x22col-xs-4\x22>        <select data-bind=\x22attr: { rtschoolboardselector: true, name: $root.utl.name($parents), initialValue: value },							value: $root.utl.rtSchoolboard,							disable: $root.utl.isDisabled(options.disabled), 							foreach: $root.utl.preparedBoardSchoolList\x22>            <option data-bind=\x22value: m_Item1, text:m_Item2, attr:{ selected: m_Item1 == $parent.value}\x22></option>        </select>  </div></div>";
	ko.templates["rtschoolboard.print"] = "<div class=\x22form-group\x22>    <!-- ko if: options.label -->    <label class=\x22col-xs-2 control-label\x22 data-bind=\x22attr: {for: name}, text: options.label, attr: {required: $root.utl.isTrue(options.required)}\x22></label>    <!-- /ko -->    <div class=\x22col-xs-10\x22>        <input type=\x22text\x22 data-bind=\x22value: $root.utl.schoolBoardName($root.utl.rtSchoolboard())\x22 class=\x22form-control\x22>    </div></div>";
	ko.templates["school"] = "<div class=\x22form-group\x22>    <!-- ko if: options.label -->    <label class=\x22control-label col-xs-2\x22  data-bind=\x22text: options.label, attr: {required: $root.utl.isTrue(options.required)}\x22></label>    <!-- /ko -->    <div class=\x22col-xs-10\x22>        <select data-bind=\x22attr: {schoolselector: true, initialValue: value, name: $root.utl.name($parents)},                         value: $root.utl.school,                         disable: $root.utl.isDisabled(options.disabled),                         foreach: $root.utl.filteredSchoolList\x22>            <option data-bind=\x22value: id, text:name, attr:{ selected: id == $parent.value}\x22></option>        </select>        <!-- ko if: !$root.utl.filteredSchoolList().length -->        <i class=\x22fa fa-refresh fa-spin\x22></i>        <!--<div>loading</div>-->        <!-- /ko -->    </div></div>";
	ko.templates["school.print"] = "<div class=\x22form-group\x22>    <!-- ko if: options.label -->    <label class=\x22control-label col-xs-2\x22 data-bind=\x22attr: {for: name}, text: options.label, attr: {required: $root.utl.isTrue(options.required)}\x22></label>    <!-- /ko -->    <div class=\x22col-xs-10\x22>        <input type=\x22text\x22 data-bind=\x22value: $root.utl.schoolName($root.utl.school())\x22 class=\x22form-control\x22>    </div></div>";
	ko.templates["schoolboard"] = "<div class=\x22form-group\x22>    <!-- ko if: options.label -->    <label class=\x22control-label col-xs-2\x22 data-bind=\x22text: options.label, attr: {required: $root.utl.isTrue(options.required)}\x22></label>    <!-- /ko -->    <div class=\x22col-xs-4\x22>		<select data-bind=\x22attr: { schoolboardselector: true, name: $root.utl.name($parents), initialValue: value },							value: $root.utl.schoolboard,							disable: $root.utl.isDisabled(options.disabled), 							foreach: $root.utl.preparedBoardSchoolList\x22>			<option data-bind=\x22value: m_Item1, text:m_Item2, attr:{ selected: m_Item1 == $parent.value}\x22></option>		</select>  </div></div>";
	ko.templates["schoolboard.print"] = "<div class=\x22form-group\x22>    <!-- ko if: options.label -->    <label class=\x22col-xs-2 control-label\x22 data-bind=\x22attr: {for: name}, text: options.label, attr: {required: $root.utl.isTrue(options.required)}\x22></label>    <!-- /ko -->    <div class=\x22col-xs-10\x22>        <input type=\x22text\x22 data-bind=\x22value: $root.utl.schoolBoardName($root.utl.schoolboard())\x22 class=\x22form-control\x22>    </div></div>";
	ko.templates["section"] = "<!--indented section container for form element, can be collapsed/open by a checkbox using enablesSection option on checkbox--><div data-bind=\x22attr:{id: name}, css: {collapse: options.collapse, ow_section: true, ow_collapsable_section:options.collapse} \x22>    <div class=\x22form-group\x22>        <div class=\x22col-xs-12 section-message-container\x22 style=\x22display:none;\x22>            <label class=\x22control-label\x22 data-bind=\x22attr: { 'data-idName': $root.utl.toQs(id) }\x22></label>        </div>        <!--items contained in panel are rendered here-->        <div data-bind=\x22template: { name: 'renderAllChildren', data: $data }\x22></div>    </div></div>";
	ko.templates["section.print"] = "<!--indented section container for form element, can be collapsed/open by a checkbox using enablesSection option on checkbox--><div data-bind=\x22attr: {id: name}\x22>    <div data-bind=\x22template: { name: 'renderAllChildren', data: $data }\x22></div></div>";
	ko.templates["selectbox"] = "<div class=\x22form-group\x22>    <!-- ko if: options.label -->    <label class=\x22control-label col-xs-2\x22 data-bind=\x22text: options.label, attr: {required: $root.utl.isTrue(options.required)}\x22></label>    <!-- /ko -->    <div class=\x22col-xs-10\x22>    <select data-bind=\x22value: value, attr: { id: name, name: $root.utl.name($parents), required: $root.utl.isTrue(options.required) }, disable: $root.utl.isDisabled(options.disabled), foreach: selectValues\x22>      <option data-bind=\x22value: value, text:label, attr:{ selected: value == $parent.value}\x22></option>    </select>  </div></div>";
	ko.templates["selectbox.print"] = "<div class=\x22form-group\x22>    <!-- ko if: options.label -->    <label class=\x22control-label col-xs-2\x22 data-bind=\x22text: options.label\x22></label>    <!-- /ko -->    <div class=\x22col-xs-10\x22>        <input type=\x22text\x22 data-bind=\x22value: $root.utl.selectBoxValueName(selectValues, value)\x22 class=\x22form-control\x22>    </div></div>";
	ko.templates["textarea"] = "<!-- Text area--><div class=\x22form-group\x22>	<!-- ko if: options.label -->	<label class=\x22control-label col-xs-2\x22  data-bind=\x22text: options.label, attr: {required: $root.utl.isTrue(options.required)}\x22></label>	<!-- /ko -->	<div class=\x22col-xs-10\x22 >        <textarea data-bind=\x22attr: {id:name, name: $root.utl.name($parents), placeholder: options.placeholderText,							required: $root.utl.isTrue(options.required), minlength: options.minlength, maxlength: options.maxlength},							text: value, value: value, disable: $root.utl.isDisabled(options.disabled)  \x22                  class=\x22form-control\x22 rows=\x224\x22></textarea>		<!-- not a requirement <p class=\x22help-block\x22 data-bind=\x22text: options.help\x22></p>-->	</div></div>";
	ko.templates["textinput"] = "<!-- Text input--><div class=\x22form-group\x22>	<!-- ko if: options.label -->	<label class=\x22control-label col-xs-2\x22  data-bind=\x22text: options.label, attr: {required: $root.utl.isTrue(options.required)}\x22></label>	<!-- /ko -->	<div class=\x22col-xs-10\x22>		<input  type=\x22text\x22				data-bind=\x22attr: {id:name, name: $root.utl.name($parents), placeholder: options.placeholderText,				    required: $root.utl.isTrue(options.required), minlength: options.minlength, maxlength: options.maxlength },				    value: value, disable: $root.utl.isDisabled(options.disabled)\x22				class=\x22form-control\x22>		<!-- not a requirement <p class=\x22help-block\x22 data-bind=\x22text: options.help\x22></p>-->	</div></div>";
	ko.templates["ticketsinput"] = "<!-- Text input--><div class=\x22form-group\x22>    <!-- ko if: options.label -->    <label class=\x22control-label col-xs-2\x22  data-bind=\x22text: options.label, attr: {required: $root.utl.isTrue(options.required)}\x22></label>    <!-- /ko -->    <div class=\x22col-xs-2\x22>        <input  type=\x22number\x22                data-bind=\x22attr: {id:name, name: $root.utl.name($parents), placeholder: options.placeholderText,                required: $root.utl.isTrue(options.required), min: options.min, max: options.max, step: options.step },                 value: value, disable: $root.utl.isDisabled(options.disabled)\x22                class=\x22form-control numeric ow_tickets\x22>        <!-- not a requirement <p class=\x22help-block\x22 data-bind=\x22text: options.help\x22></p>-->    </div></div>";
	ko.templates["urlpicker"] = "<!-- Text input--><div class=\x22form-group ow_datepicker-form-group\x22>    <!-- ko if: options.label -->    <label class=\x22control-label col-xs-2\x22  data-bind=\x22text: options.label, attr: {required: $root.utl.isTrue(options.required)}\x22></label>    <!-- /ko -->    <div class=\x22col-xs-10\x22>        <div class=\x22input-group\x22 data-bind=\x22css: {  ow_disabled: $root.utl.isTrue(options.disabled) }\x22>            <input  data-bind=\x22attr: {id:name, name: $root.utl.name($parents), placeholder: options.placeholderText,                    required: $root.utl.isTrue(options.required), minlength: options.minlength, maxlength: options.maxlength },                     value: value, disable: $root.utl.isDisabled(options.disabled)\x22                    class=\x22form-control urlValue\x22>            <span class=\x22input-group-addon urlPicker\x22><i class=\x22glyphicon glyphicon-link\x22></i></span>            <!-- not a requirement <p class=\x22help-block\x22 data-bind=\x22text: options.help\x22></p>-->        </div>    </div></div>";
}
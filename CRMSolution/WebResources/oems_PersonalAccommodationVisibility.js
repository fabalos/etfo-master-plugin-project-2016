function setVisibility() {
   var childrenSection = Xrm.Page.ui.tabs.get(0).sections.get('children');
   var optionType = Xrm.Page.data.entity.attributes.get('oems_optiontype');

   if (optionType != null && optionType.getValue() == 1)
      childrenSection.setVisible(true);
   else
      childrenSection.setVisible(false);
}
function RefundMultipleBR(selectedItems){
	for(var x = 0; x < selectedItems.length; x++){
		//Update Entity
		var updateEntity = new XrmServiceToolkit.Soap.BusinessEntity("oems_billingrequest", selectedItems[x]);
		//OptionSet
		updateEntity.attributes["oems_refundmanuallyflag"] = { value : true, type : 'boolean' };
		var updateResponse = XrmServiceToolkit.Soap.Update(updateEntity );
	}
        
        var control = Xrm.Page.ui.controls.get("BillingRequests");
        control.refresh();
}

function RefundSingleBR(){
	Xrm.Page.getAttribute("oems_refundmanuallyflag").setValue(1);
	Xrm.Page.data.entity.save();
}

function SetVisibilityRefundForm(){
	var status = Xrm.Page.getAttribute("oems_paymentstatus").getValue();
	
	//Refund Failed
	if(status == 347780010) return true;
	return false;
}

function SetVisibilityRefundGrid(){
	if(typeof $ != 'undefined'){
		if($(BillingRequests_SavedNewQuerySelector) != null){
			if($(BillingRequests_SavedNewQuerySelector)[0] != null){
				if($(BillingRequests_SavedNewQuerySelector)[0].childNodes[0] != null){
					if($(BillingRequests_SavedNewQuerySelector)[0].childNodes[0].childNodes[0] != null){
						if($(BillingRequests_SavedNewQuerySelector)[0].childNodes[0].childNodes[0].textContent != null){
							if($(BillingRequests_SavedNewQuerySelector)[0].childNodes[0].childNodes[0].textContent == "Failed Refunds") return true;
						}
					}
				}
			}
		}
	}
	return false;
}
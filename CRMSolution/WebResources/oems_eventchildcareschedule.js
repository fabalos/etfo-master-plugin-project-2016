function onSave()
{

    var name =   Xrm.Page.getAttribute('oems_childcareschedulename').getValue();
   
    if(name == null  )
    {
        var startdate = Xrm.Page.getAttribute("oems_startdatetime").getValue().format("MMMM dd, yyyy h:mm tt");
        var enddate = Xrm.Page.getAttribute("oems_enddatetime").getValue().format("h:mm tt");

        Xrm.Page.getAttribute("oems_childcareschedulename").setValue(startdate + ' - ' + enddate);
    }
}

function OnChangeDateTime()
{
	var control = Xrm.Page.getControl("oems_enddatetime");
        var myElement = control._control._element.getElementsByClassName('ms-crm-Input')[0];
        myElement.disabled = true;
        var myButton = control._control._element.getElementsByClassName("ms-crm-DateTime ms-crm-ImageStrip-btn_off_cal")[0];
        myButton.disabled = true;
        myButton.style.visibility = "hidden";


}


function OnChangeDate()
{
       var startDate = Xrm.Page.getAttribute('oems_startdatetime').getValue(); 
       Xrm.Page.getAttribute('oems_enddatetime').setValue(startDate);
}
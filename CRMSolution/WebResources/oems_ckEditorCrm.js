﻿var omniware = omniware || {};
omniware.ckEditorCrm = {
    
    // this script works together with ckEditorCrm.html resource file
    
    //webresource has to be named WebResource_ckEditor_fieldName
    FRAME_PREFIX: 'WebResource_ckEditor_',

    editorFieldNames: [], // keep track all the editor fields on this form

    // map fieldname to iframe id by convention
    iframeIdForFieldName: function(fieldName) {
        return omniware.ckEditorCrm.FRAME_PREFIX + fieldName;
    },

    // map  iframe id to fieldname by convention
    fieldNameForIframeId: function (iframeId) {
        if (!iframeId.toLowerCase().startsWith(omniware.ckEditorCrm.FRAME_PREFIX.toLowerCase())) {
			alert('Ckeditor webresource must start with ' + omniware.ckEditorCrm.FRAME_PREFIX);
		}
        return iframeId.substr(omniware.ckEditorCrm.FRAME_PREFIX.length);
    },
    

    // Call onSave - copies all values back from ckEditor to form fields
    // Simply attach omniware.ckEditorCrm.getAllContents on Form Save
    getAllContents: function() {
        omniware.ckEditorCrm.editorFieldNames.forEach(function(fieldName) {
            var iframeName = omniware.ckEditorCrm.iframeIdForFieldName(fieldName);
            var content = $get(iframeName).contentWindow.CKEDITOR.instances.editor1.getData();
            Xrm.Page.getAttribute(fieldName).setValue(content);
        });
    },    
};
function ValidateTimeValues()
{ 
   var startTime = $('input[id$="oems_starttime"]')[0].value;
   var endTime = $('input[id$="oems_endtime"]')[0].value;
   var times = '';
   
   if(!ValidateTime(startTime)){
		times += ' \n' +Xrm.Page.getControl("oems_starttime").getLabel() +" - " + startTime ;
   }
   
   if(!ValidateTime(endTime )){
		times += ' \n' +Xrm.Page.getControl("oems_endtime").getLabel() +" - " + endTime;
   }
   
   if(times != '')
   {
		var notification = 'Incorrect time values:';
		alert(notification + times);
		Xrm.Page.context.getEventArgs().preventDefault();
   }else{
		Xrm.Page.getAttribute("oems_starttime").setValue(startTime);
		Xrm.Page.getAttribute("oems_endtime").setValue(endTime);
   }
   
}

function ValidateTime(timeStr)
{
   var valid = (timeStr.search(/(0-1[012]|[1-9]):[0-5][0-9]\s?(am|pm)$/i) != -1) &&
				(timeStr.substr(0,1) >= 0 && timeStr.substr(0,1) <= 12) &&
				(timeStr.substr(3,2) >= 0 && timeStr.substr(3,2) <= 59);
	
   if(!valid) return false;
   return true;
}

function OnLoad(){
     $('input[id$="oems_starttime"]').inputmask(
        "hh:mm", 
        {
          placeholder: "HH:MM xm", 
          insertMode: false, 
          showMaskOnHover: false,
          hourFormat: 12,
          mask: "h:s t\\m",
        }
      );
	  
	 $('input[id$="oems_endtime"]').inputmask(
        "hh:mm", 
        {
          placeholder: "HH:MM xm", 
          insertMode: false, 
          showMaskOnHover: false,
          hourFormat: 12,
          mask: "h:s t\\m",
        }
      );

      	if(Xrm.Page.getAttribute("oems_starttime").getValue() == null){
		Xrm.Page.getAttribute("oems_starttime").setValue("HH:MM xm");
	}
     
	if(Xrm.Page.getAttribute("oems_endtime").getValue() == null){
		Xrm.Page.getAttribute("oems_endtime").setValue("HH:MM xm");
	}

}


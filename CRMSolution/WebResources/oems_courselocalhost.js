function replaceAddExistingButtonView(params) {
    var relName = params.gridControl.GetParameter("relName"),
        roleOrd = params.gridControl.GetParameter("roleOrd"),
        viewId = "{1DFB2B35-B02C-44D1-868D-258DEEAB88E1}"; //Random GUID
     
    var customView = {
        fetchXml: params.fetchXml,
        id: viewId, 
        layoutXml: params.layoutXml,
        name: params.name,
        recordType: params.gridTypeCode,
        Type: 0
    };
 
    var lookupItems = LookupObjects(null, "multi", params.gridTypeCode, 0, null, "", null, null, null, null, null, null, viewId, [customView]);
    if (lookupItems && lookupItems.items.length > 0) {
        AssociateObjects(params.primaryEntityTypeCode, params.primaryItemId, params.gridTypeCode, lookupItems, IsNull(roleOrd) || roleOrd == 2, null, relName);
    }
}

function replaceAddExistingButtonViewForCourseLocalHost(gridTypeCode, gridControl, primaryEntityName, primaryEntityTypeCode, firstPrimaryItemId) {
    if (primaryEntityName == "oems_event") {

		var filter = getExistingCourseLocalHosts(gridControl);
	
        replaceAddExistingButtonView({
            gridTypeCode: gridTypeCode,
            gridControl: gridControl,
            primaryEntityTypeCode: primaryEntityTypeCode,
            primaryItemId: firstPrimaryItemId,
            fetchXml: '<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">'+
						  '<entity name="oems_courselocalhost">'+
							'<attribute name="oems_courselocalhostid" />'+
							'<attribute name="oems_name" />'+
							'<attribute name="createdon" />'+
							'<order attribute="oems_name" descending="false" />'+
							'<filter type="and">'+
							    filter +
							  '<condition attribute="oems_masterflag" operator="eq" value="1" />'+
							'</filter>'+
						  '</entity>'+
						'</fetch>',
            layoutXml: "<grid name='resultset' object='1' jump='name' select='1' icon='1' preview='1'>" +
                          "<row name='result' id='oems_courselocalhostid'>" +
                            "<cell name='oems_name' width='300' />" +
                          "</row>" +
                        "</grid>",
            name: "Course Local Hosts"
        });
    }
    else {
        Mscrm.GridRibbonActions.addExistingFromSubGridStandard(gridTypeCode, gridControl);
    }
}

function getExistingCourseLocalHosts(gridControl)
{
	var field = "";
	if(gridControl._element.id == "CourseLocalHost"){
		field = "oems_courseterm";
	}
	
	var courseTermId = Xrm.Page.data.entity.getId();
	var fetchXml = '<fetch version="1.0" output-format="xml-platform" mapping="logical" distinct="false">'+
					  '<entity name="oems_courselocalhost">'+
						'<attribute name="oems_courselocalhostid" />'+
						'<attribute name="oems_name" />'+
						'<attribute name="createdon" />'+
						'<order attribute="oems_name" descending="false" />'+
						'<filter type="and">'+
						  '<condition attribute="' + field + '" operator="eq" uitype="oems_event" value="' + courseTermId + '" />'+
					   '</filter>'+
					  '</entity>'+
					'</fetch>';
						
	var retrievedEvent = XrmServiceToolkit.Soap.Fetch(fetchXml);
	
	if(retrievedEvent.length > 0){
		var filter = '<filter type="and">';
		for(x=0; x < retrievedEvent.length; x++)
		{
			filter += '<condition attribute="oems_name" operator="ne" value="' + retrievedEvent[x].attributes.oems_name.value + '" />';
		}
		
		filter += '</filter>';
		return filter;
		
	}else{
		return '';
	}

}
rem deploy previously build solution to ETFOTEST - run by build server


setlocal enableextensions enabledelayedexpansion

set connection="ServiceUri=http://10.56.52.32/ETFO2016Portal; Domain=devupg16; Username=Administrator; Password=Omniware2; Timeout=24"
set solutionname="OEMSAdministration"

set solutionDir=.\SolutionFiles
set file="%solutionDir%\OEMSAdministration.zip"
set file_managed="%solutionDir%\OEMSAdministration_managed.zip"
set tools=".\tools"
set log=".\deploy2016.log"
set solutionimportlog=".\solutionImport.log"


echo on

%tools%\Solution.Cmd.Helper.exe /operation:import  /overwriteunmanagedcustomizations /solutionname:%solutionname% /crmconnectionstring:%connection% /solutionfilepath:%file_managed% /publishworkflows /logpath:%log% /logfilepath:%solutionimportlog% || ( Echo IMPORT Error & EXIT /B 1)
%tools%\Solution.Cmd.Helper.exe /operation:publish /solutionname:%solutionname% /crmconnectionstring:%connection%  /logpath:%log% || ( Echo PUBLISH Error & EXIT /B 1)

endlocal
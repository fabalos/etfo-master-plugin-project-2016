rem Export managed and unmanaged solution from CI


setlocal enableextensions enabledelayedexpansion

set connection="ServiceUri=http://10.56.52.32/ETFO2016Portal; Domain=devupg16; Username=Administrator; Password=Omniware2; Timeout=24"
set solutionname="OEMSAdministration"

set solutionDir=.\SolutionFiles
set file="%solutionDir%\OEMSAdministration.zip"
set file_managed="%solutionDir%\OEMSAdministration_managed.zip"
set tools=".\tools"
set log=".\tmp\export2016CI.log"

mkdir %solutionDir%
mkdir .\tmp
echo on

rem echo only managed solution exported for now
%tools%\Solution.Cmd.Helper.exe /operation:export /solutionname:%solutionname% /crmconnectionstring:%connection% /solutionfilepath:%file% /logpath:%log% || ( Echo EXPORT Error & EXIT /B 1)
%tools%\Solution.Cmd.Helper.exe /managed /operation:export /solutionname:%solutionname% /crmconnectionstring:%connection% /solutionfilepath:%file_managed% /logpath:%log% || ( Echo EXPORT Managed Error & EXIT /B 1)

endlocal
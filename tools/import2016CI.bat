rem Build and import package to CI

setlocal enableextensions enabledelayedexpansion

set connection="ServiceUri=http://10.56.52.32/ETFO2016Portal; Domain=devupg16; Username=Administrator; Password=Omniware2; Timeout=24"
set solutionname="OEMSAdministration"

set solutionDir=.\CRMSolution
set solutionFile=%solutionDir%\Other\Solution.xml
set file=".\tmp\OEMSAdministrationCI.zip"
set tools=".\tools"
set log=".\tmp\import.log"
set solutionimportlog=".\tmp\import2016CI.xml"

mkdir .\tmp

echo on

del %file%
%tools%\replaceXpath.exe %solutionFile% "ImportExportXml/SolutionManifest/Descriptions/Description/@description"  "Build %BUILD_NUMBER%"
%tools%\solutionPackager /action:pack /zipfile:%file% /folder:%solutionDir% /packagetype:Unmanaged /map:%tools%\solutionPackagerMap.xml /allowDelete:Yes  /clobber  || ( Echo PACK Error & EXIT /B 1)
%tools%\Solution.Cmd.Helper.exe /operation:import /overwriteunmanagedcustomizations /solutionname:%solutionname% /crmconnectionstring:%connection% /solutionfilepath:%file% /publishworkflows /logpath:%log% /logfilepath:%solutionimportlog% || ( Echo IMPORT Error & EXIT /B 1)
%tools%\Solution.Cmd.Helper.exe /operation:publish /solutionname:%solutionname% /crmconnectionstring:%connection%  /logpath:%log% || ( Echo PUBLISH Error & EXIT /B 1)

endlocal
﻿using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;
using Omniware.ETFO.Plugin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xrm;
using Omniware.ETFO.Plugin.Shared;
using log4net;
using Omniware.ETFO.Plugin.Billing;
using System.Diagnostics;

namespace Omniware.WaitingListMonitoring
{
    public class Crm
    {
        private static ILog _log = LogManager.GetLogger(typeof(Crm));

        // Delete unused event registrations 
        public static void DeleteUnusedRegistrations(OmniContext localContext)
        {
            var sw = new Stopwatch();
            sw.Start();

            var config = (from configuration in localContext.CreateQuery("oems_configuration") select configuration).First();

            var regpendingexpirationhours = config.GetAttributeValue<decimal?>("oems_regpendingexpirationhours") ?? 24;
            var expirationDateTime = DateTime.UtcNow.AddHours(-(double)regpendingexpirationhours);

            _log.InfoFormat("Starting Unused Registrations,  Registration Expiration Hours: {0}, expiration Date Time: {1} ---", regpendingexpirationhours, expirationDateTime);

            var registrations = (from reg in localContext.CreateQuery("oems_eventregistration")
                                 where (int)reg["statuscode"] == (int)EventRegistrationStatusReason.Initial 
                                    && reg.GetAttributeValue<DateTime?>("modifiedon") < expirationDateTime
                                 select reg).ToList();

            var requestWithResults = new ExecuteMultipleRequest()
            {
                Settings = new ExecuteMultipleSettings()
                {
                    ContinueOnError = true,
                    ReturnResponses = true
                },
                Requests = new OrganizationRequestCollection()
            };

            foreach (var reg in registrations)
            {
                localContext.DeleteObject(reg);
                _log.DebugFormat("Delete Unused Registration {0}", reg.GetAttributeValue<Guid>("oems_eventregistrationid"));

                var delReq1 = new DeleteRequest () { Target = reg.ToEntityReference() };
                requestWithResults.Requests.Add(delReq1);
            }

            var responseWithResults = (ExecuteMultipleResponse)localContext.Execute(requestWithResults);
            sw.Stop();

            if (responseWithResults.IsFaulted)
            {
                var faults = responseWithResults.Responses.Where(r => r.Fault != null).ToList();
                _log.ErrorFormat("Failed DeleteUnusedRegistrations: {0} of {1}, elapsed: {2} ms", faults.Count, registrations.Count, sw.ElapsedMilliseconds);

                foreach (var fault in faults)
                {
                    //todo review how useful it is .. maybe needs logging of inner fault 
                    _log.WarnFormat("Failed request : {0} fault:{1}", fault.RequestIndex, fault.Fault);
                }
            }
            else
            {
                _log.InfoFormat("Completed DeleteUnusedRegistrations requests: {0}, elapsed: {1} ms", registrations.Count, sw.ElapsedMilliseconds);
            }
        }

        // Expire unpaid CCPending Billing requests
        public static void ExpireCcPendingBillingRequests(OmniContext localContext)
        {
            var sw = new Stopwatch();
            sw.Start();

            var config = (from configuration in localContext.CreateQuery("oems_configuration")
                          select configuration).First();

            var ccpendingexpirationhours = config.GetAttributeValue<decimal?>("oems_ccpendingexpirationhours") ?? 24;
            var expirationDateTime = DateTime.UtcNow.AddHours(-(double)ccpendingexpirationhours);

            _log.InfoFormat("Starting UpdateBillingRequests,  Credit Card Expiration Hours: {0}, expiration Date Time: {1} ---", ccpendingexpirationhours, expirationDateTime);

            var billRequests = (from bill in localContext.CreateQuery("oems_billingrequest")
                                join evt in localContext.CreateQuery("oems_event") on ((EntityReference)bill["oems_event"]).Id equals (Guid)evt["oems_eventid"]
                                where ((int)evt["statuscode"] == (int)EventStatusReason.OPEN_REGISTRATION || (int)evt["statuscode"] == (int)EventStatusReason.CLOSE_REGISTRATION) &&
                                    (bool)evt["oems_waitinglistcomponentflag"] == true
                                where (int)bill["oems_paymentstatus"] == PaymentStatus.CreditCardPending && bill.GetAttributeValue<DateTime?>("modifiedon") < expirationDateTime
                                orderby bill["oems_event"]
                                select bill).ToList();

            var requestWithResults = new ExecuteMultipleRequest()
            {
                Settings = new ExecuteMultipleSettings()
                {
                    ContinueOnError = true,
                    ReturnResponses = true
                },
                Requests = new OrganizationRequestCollection()
            };

            foreach (var billReq in billRequests)
            {
                var account = BillingUtil.ResetBillingRequestValues(localContext, billReq);
                if (account != null)
                {
                    localContext.UpdateObject(account);
                    var updReq = new UpdateRequest() { Target = account };
                    requestWithResults.Requests.Add(updReq);
                }

                _log.DebugFormat("Expiring Unpaid Billing Requests {0}", billReq.GetAttributeValue<Guid>("oems_billingrequestid"));

                localContext.UpdateObject(billReq);
                var updReq1 = new UpdateRequest() { Target = billReq };
                requestWithResults.Requests.Add(updReq1);
            }

            var responseWithResults = (ExecuteMultipleResponse)localContext.Execute(requestWithResults);
            sw.Stop();

            if (responseWithResults.IsFaulted)
            {
                var faults = responseWithResults.Responses.Where(r => r.Fault != null).ToList();
                _log.ErrorFormat("Failed UpdateBillingRequests requests: {0} of {1}, elapsed: {2} ms", faults.Count, billRequests.Count, sw.ElapsedMilliseconds);

                foreach (var fault in faults)
                {
                    //todo review how useful it is .. maybe needs logging of inner fault 
                    _log.WarnFormat("Failed request : {0} fault:{1}", fault.RequestIndex, fault.Fault);
                }
            }
            else
            {
                _log.InfoFormat("Completed UpdateBillingRequests requests: {0}, elapsed: {1} ms", billRequests.Count, sw.ElapsedMilliseconds);
            }

        }

        public static void ExpireWaitingListOffers(OmniContext localContext)
        {
            var sw = new Stopwatch();
            sw.Start();
            _log.InfoFormat("Starting ExpireWaitingListOffers ---");

            var waitingRequestsFull =
                           (from wait in localContext.CreateQuery("oems_waitinglistrequest")
                            join evt in localContext.CreateQuery("oems_event") on ((EntityReference)wait["oems_event"]).Id equals (Guid)evt["oems_eventid"]
                            where ((int)evt["statuscode"] == (int)EventStatusReason.OPEN_REGISTRATION || (int)evt["statuscode"] == (int)EventStatusReason.CLOSE_REGISTRATION) &&
                                (bool)evt["oems_waitinglistcomponentflag"] == true
                            where (int)wait["statuscode"] == WaitingListStatus.Offered && ((DateTime?)wait["oems_offerdate"]) != null
                            orderby wait["oems_event"]
                            select wait).ToList();

            var requestWithResults = new ExecuteMultipleRequest()
            {
                Settings = new ExecuteMultipleSettings()
                {
                    ContinueOnError = true,
                    ReturnResponses = true
                },
                Requests = new OrganizationRequestCollection()
            };

            foreach (var req in waitingRequestsFull)
            {
                var offeredDate = req.GetAttributeValue<DateTime?>("oems_offerdate");
                if (offeredDate != null && DateTime.UtcNow.Subtract(offeredDate.Value).TotalHours > req.GetAttributeValue<int>("oems_targetexpirydate"))
                {
                    SetStateRequest state = new SetStateRequest();
                    state.State = new OptionSetValue((int)CommonStatus.ACTIVE);
                    state.Status = new OptionSetValue(WaitingListStatus.Expired);
                    state.EntityMoniker = req.ToEntityReference();
                    requestWithResults.Requests.Add(state);
                    _log.DebugFormat("Expiring Waiting List Request {0} after {1} hours", req.GetAttributeValue<Guid>("oems_waitinglistrequestid").ToString(), req.GetAttributeValue<int>("oems_targetexpirydate"));
                }
            }

            var responseWithResults = (ExecuteMultipleResponse)localContext.Execute(requestWithResults);
            
            sw.Stop();
            if (responseWithResults.IsFaulted)
            {
                var faults = responseWithResults.Responses.Where(r => r.Fault != null).ToList();
                _log.ErrorFormat("Failed ExpireWaitingListOffers requests: {0} of {1}, elapsed: {2} ms", faults.Count, waitingRequestsFull.Count, sw.ElapsedMilliseconds);

                foreach (var fault in faults)
                {
                    //todo review how useful it is .. maybe needs logging of inner fault 
                    _log.WarnFormat("Failed request : {0} fault:{1}", fault.RequestIndex, fault.Fault);
                }
            }
            else
            {
                _log.InfoFormat("Completed ExpireWaitingListOffers requests: {0}, elapsed: {1} ms", waitingRequestsFull.Count, sw.ElapsedMilliseconds);
            }

        }

        public static int GetEventCapacity(OmniContext localContext, Entity evt)
        {
            var sw = new Stopwatch();
            sw.Start();
            _log.DebugFormat("GetEventCapacity for '{0}'", evt.GetAttributeValue<string>("oems_eventname"));

            var eventId = evt.Id;
            var registrationMaximum = evt.GetAttributeValue<int>("oems_registrationmaximum");

            //Get the number of registrations with status SUBMITTED or APPROVED or CREATED with CC PENDING
            // + SUBMITTED or APPROVED
            var fetch =
            @" 
                <fetch distinct='false' mapping='logical' aggregate='true'> 
                    <entity name='oems_eventregistration'> 
                        <attribute name='oems_eventregistrationid' alias='count' aggregate='count'/>
                        <filter type='and'>
                            <condition attribute='oems_event' operator='eq' value='{0}' />
                            <filter type='or'>
                                <condition attribute='statuscode' operator='eq' value='{1}' />
                                <condition attribute='statuscode' operator='eq' value='{2}' />
                            </filter>
                        </filter>
                    </entity>
                </fetch>
            ";
            fetch = String.Format(fetch, eventId, EventRegistrationStatusReason.Submitted, EventRegistrationStatusReason.Approved);
            var result = localContext.RetrieveMultiple(new FetchExpression(fetch));
            var totalSubmittedOrApproved = result.Entities.Any() ? (int)((AliasedValue)result.Entities.Single()["count"]).Value : 0;

            // + CREATED with CC PENDING
            fetch =
            @" 
                <fetch distinct='false' mapping='logical' aggregate='true'> 
                    <entity name='oems_eventregistration'> 
                        <attribute name='oems_eventregistrationid' alias='count' aggregate='count'/> 
                        <filter type='and'>
                            <condition attribute='oems_event' operator='eq' value='{0}' />
                            <condition attribute='statuscode' operator='eq' value='{1}' />
                        </filter>
                        <link-entity name='oems_billingrequest' from='oems_eventregistration' to='oems_eventregistrationid'>
			                <filter type='and'>
					            <condition attribute='oems_paymentstatus' operator='eq' value='{2}' />
					            <condition attribute='oems_paymentmethod' operator='eq' value='{3}' />
				            </filter>
		                </link-entity>
                    </entity>
                </fetch>
            ";
            fetch = String.Format(fetch, eventId, EventRegistrationStatusReason.Created, PaymentStatus.CreditCardPending, PaymentMethod.CreditCard);
            result = localContext.RetrieveMultiple(new FetchExpression(fetch));
            var totalCreditCardPending = result.Entities.Any() ? (int)((AliasedValue)result.Entities.Single()["count"]).Value : 0;

            var spots = (registrationMaximum - (totalSubmittedOrApproved + totalCreditCardPending));

            sw.Stop();

            _log.InfoFormat("GetEventCapacity({0}): {1} = ({2} - ({3} + {4})), elapsed {5} ms",
                evt.GetAttributeValue<string>("oems_eventname"), spots, registrationMaximum, totalSubmittedOrApproved, totalCreditCardPending, sw.ElapsedMilliseconds);

            return spots;
        }

        public static void OfferSpots(OmniContext localContext)
        {
            var sw = new Stopwatch();
            sw.Start();
            _log.InfoFormat("Starting OfferSpots --- ");

            var events = 
            (
                from evt in localContext.CreateQuery("oems_event")
                where 
                    ((int)evt["statuscode"] == (int)EventStatusReason.OPEN_REGISTRATION || (int)evt["statuscode"] == (int)EventStatusReason.CLOSE_REGISTRATION) && 
                    (bool)evt["oems_waitinglistcomponentflag"] == true && 
                    (bool)evt["oems_waitinglistmanualflag"] == false && 
                    (bool)evt["oems_eventfullflag"] == false
                select evt
            ).ToList();

            _log.DebugFormat("Retrieved {0} events with waiting list active.", events.Count);

            foreach (var evt in events)
            {
                var eventName = evt.GetAttributeValue<string>("oems_eventname");
                _log.DebugFormat("Processing event '{0}'", eventName);

                int capacity = GetEventCapacity(localContext, evt);

                if (capacity > 0)
                {
                    var waitingListRequests = (from wait in localContext.CreateQuery("oems_waitinglistrequest")
                                               where ((EntityReference)wait["oems_event"]).Id == evt.Id && (int)wait["statuscode"] == WaitingListStatus.Waiting
                                               orderby wait["oems_manualorder"], wait["createdon"]
                                               select wait).Take(capacity).ToList();

                    var requestWithResults = new ExecuteMultipleRequest()
                    {
                        Settings = new ExecuteMultipleSettings()
                        {
                            ContinueOnError = true,
                            ReturnResponses = true
                        },
                        Requests = new OrganizationRequestCollection()
                    };

                    foreach (var wait in waitingListRequests)
                    {
                        var state = new SetStateRequest
                        {
                            State = new OptionSetValue((int)CommonStatus.ACTIVE),
                            Status = new OptionSetValue(WaitingListStatus.Offered),
                            EntityMoniker = wait.ToEntityReference()
                        };
                        requestWithResults.Requests.Add(state);
                        _log.DebugFormat("Offering spot on event '{0}' to Waiting List Request {1} ", eventName, wait.GetAttributeValue<Guid>("oems_waitinglistrequestid"));
                    }

                    var responseWithResults = (ExecuteMultipleResponse)localContext.Execute(requestWithResults);

                    if (responseWithResults.IsFaulted)
                    {
                        var faults = responseWithResults.Responses.Where(r => r.Fault != null).ToList();
                        _log.ErrorFormat("Failed OfferSpots event: {0} requests: {1} of {2}", eventName,
                            faults.Count, waitingListRequests.Count());

                        foreach (var fault in faults)
                        {
                            //todo review how useful it is .. maybe needs logging of inner fault 
                            _log.WarnFormat("Failed request : {0} fault:{1}", fault.RequestIndex, fault.Fault);
                        }
                    }
                    else
                    {
                        _log.InfoFormat("Completed OfferSpots event: {0} requests: {1}", eventName, waitingListRequests.Count());
                    }
                }
            }
            sw.Stop();
            _log.InfoFormat("Completed OfferSpots step in: {0} ms", sw.ElapsedMilliseconds);
        }
    }
}

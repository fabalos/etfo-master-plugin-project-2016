﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.ServiceModel;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Client.Services;
using Microsoft.Xrm.Sdk.Client;
using Xrm;
using log4net;


namespace Omniware.WaitingListMonitoring
{
    class Program
    {
        // logger is configured via Assembly.cs attribute, log file location can be changed in App.config
        private static ILog _log = LogManager.GetLogger(typeof(Program));

        static void Main(string[] args)
        {
            try
            {
                var connection = ConfigurationManager.ConnectionStrings["Xrm"].ConnectionString;
                _log.InfoFormat("   =============================   ");
                _log.InfoFormat("Starting Waiting List Monitoring, connecting to {0} ", connection);
                CrmConnection conn = CrmConnection.Parse(connection);
                IOrganizationService service = new OrganizationService(conn);

                var context = new OmniContext(service);

                //Delete unused registrations
                try
                {
                    Crm.DeleteUnusedRegistrations(context);
                }
                catch (Exception e)
                {
                    _log.Fatal("Failed DeleteUnusedRegisatrtions Step ", e);                    
                }

                //allow continue after one steps fails
                try
                {
                    Crm.ExpireCcPendingBillingRequests(context);
                }
                catch (Exception e)
                {
                    _log.Fatal("Failed ExpireCCPendingBillingRequests Step ", e);                    
                }
                try
                {
                    Crm.ExpireWaitingListOffers(context);
                }
                catch (Exception e)
                {
                    _log.Fatal("Failed ExpireWaitingListOffers Step", e);
                }
                try
                {
                    Crm.OfferSpots(context);
                }
                catch (Exception e)
                {
                    _log.Fatal("Failed OfferSpots Step", e);
                }
            }
            catch (Exception e)
            {
                _log.Fatal("WaitingListing Monitoring job failed " + e.Message);
                throw e;
            }
            _log.InfoFormat("Exit Waiting List Monitoring ");

        }
    }
}

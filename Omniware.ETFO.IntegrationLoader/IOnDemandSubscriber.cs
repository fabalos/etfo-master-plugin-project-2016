﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Omniware.ETFO.IntegrationLoader
{
    public interface IOnDemandSubscriber
    {
        bool LoadData(Worker worker,ILog log );
    }
}
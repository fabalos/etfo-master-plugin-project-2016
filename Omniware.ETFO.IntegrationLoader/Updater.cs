﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using log4net;

namespace Omniware.ETFO.IntegrationLoader
{
    public class Updater
    {
        static ILog Log = LogManager.GetLogger(typeof(Updater));

        private static Worker worker = null;
        public static void ProcessUpdates(Worker worker, string updateVersion)
        {
            Updater.worker = worker;
            if (updateVersion.CompareTo("1") < 1)
            {
                ApplyUpdate1();
            }
            if (updateVersion.CompareTo("2") < 1)
            {
                ApplyUpdate2();
            }
        }

        private static void ApplyUpdateInnerRunEMS1(int? active, StringBuilder idList)
        {
            if (idList.Length == 0) return;
            Log.Debug("Running EMS query");
            string sql = "UPDATE accountExtensionBase SET oems_etfoactive = " + (active != null ? ((int)active).ToString() : "null") + " WHERE oems_etfomemberid in (" + idList.ToString() + ")";
            Log.Debug(sql);
            SqlCommand cmd = new SqlCommand(sql, worker.EMSEnvironment.SQLCon);
            cmd.CommandTimeout = 0;
            cmd.ExecuteNonQuery();
        }
        private static void ApplyUpdateInner1(int? active)
        {
            string queryRegActive = "SELECT reg_etfoid, reg_active FROM Contact WHERE reg_etfoid IS NOT NULL AND reg_active";
            if (active == 1) queryRegActive += "=1";
            else if (active == 0) queryRegActive += "=0";
            else if (active == null) queryRegActive += " is null";

            SqlCommand cmd = null;
            cmd = new SqlCommand(queryRegActive, worker.MRSEnvironment.SQLCon);
            cmd.CommandTimeout = 0;
            SqlDataReader reader = cmd.ExecuteReader();
            //List<string> idList = new List<string>();
            Log.Info("Loading MRS accounts");
            StringBuilder idList = new StringBuilder();
            bool first = true;
            int counter = 0;
            while (reader.Read())
            {
                counter++;
                if (!(reader["reg_etfoid"] is DBNull))
                {
                    if(!first)idList.Append(",");
                    first = false;
                    idList.Append("'"+(string)reader["reg_etfoid"]+"'");
                }
                if (counter % 499 == 0)
                {
                    ApplyUpdateInnerRunEMS1(active, idList);
                    idList.Clear();
                    first = true;
                }
            }
            ApplyUpdateInnerRunEMS1(active, idList);
            reader.Close();
            
        }

        public static void ApplyUpdate1()
        {
            Log.Debug("Applying update 1");

            ApplyUpdateInner1(1);
            ApplyUpdateInner1(0);
            ApplyUpdateInner1(null);

            Log.Info("Update 1 applied");
            

        }

        public static void ApplyUpdate2()
        {
        }
    }
}

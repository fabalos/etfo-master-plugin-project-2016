﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xrm.Sdk;

using Microsoft.Xrm.Sdk.Linq;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Client.Services;
using Microsoft.Xrm.Sdk.Query;

using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Messages;
using System.Data.SqlClient;
using System.ServiceModel.Description;
using log4net;
using Omniware.ETFO.Integration;



namespace Omniware.ETFO.IntegrationLoader
{


    
    public class EntityContainer
    {
       public Entity Entity {get;set;}
       public string LookupField {get;set;}
       public bool Create{get;set;}
    }

    public enum CRMEnvironmentType
    {
        MRS = 0,
        EMS = 1,
    }

    public enum ActionType 
    {
        Sync = 0,
        Date=1
    }

    public enum CRMFieldType
    {
        STRING_FIELD,
        MONEY_FIELD,
        INTEGER_FIELD,
        DATETIME_FIELD,
        DECIMAL_FIELD,
        DOUBLE_FIELD,
        OPTIONSET_FIELD,
        LOOKUP_FIELD,
        BOOL_FIELD
    }

    public class FieldDefinition
    {
       public CRMFieldType FieldType {get;set;}
       public string FieldName {get;set;}
       public FieldDefinition(string fieldName, CRMFieldType fieldType)
       {
          this.FieldType = fieldType;
          this.FieldName = fieldName;
       }
    }

    public class CRMEnvironment
    {
        static ILog Log = LogManager.GetLogger(typeof(CRMEnvironment));

        public static int MAX_BATCH = 500;

        private string connectionString = null;


        public CRMEnvironmentType EnvironmentType {get;set;} 
        public IOrganizationService OrgService {get;set;}
        public SqlConnection SQLCon {get;set;}
        public OrganizationServiceContext OrgContext {get;set;}

        // unused
        //        public EntityReference UpdateRequestUser {get;set;}

        public EntityReference UpdateRequestTeam { get; set; }

        public string MBRreferenceUpdateRequestTeam { get; set; }

        //  unused
        //        public EntityReference ErrorNotificationUser { get; set; }

        //  unused
        //        public string MBRreferenceErrorNotificatorUser { get; set; }
        //
        //        public string MBRreferenceUpdateRequestUser { get; set; }
        
        public ExecuteMultipleRequest CurrentBatch {get;set;}
        public ExecuteMultipleRequest ChildrenBatch { get; set; }

        public int BatchSize { get; set; }


        

        public void OtherConnect(string connectionString)
        {
                string userName = "ETFODOM\\Omniaccess";
                string password = "Omn3ware";
                string extraParams = "Password=" + password + ";";
                if (!userName.Contains("\\"))
                {
                    extraParams += "Username=" + userName + ";";
                }
                else
                {
                    string[] pair = userName.Split('\\');
                    extraParams += "Username=" + pair[1] + "; Domain=" + pair[0] + ";";
                }


                Uri oUri = new Uri("http://10.50.86.19/ETFO/XRMServices/2011/Organization.svc");
                //** Your client credentials   
                ClientCredentials clientCredentials = new ClientCredentials();

                clientCredentials.UserName.UserName = userName;
                clientCredentials.UserName.Password = password;

                //Create your Organization Service Proxy  
                OrganizationServiceProxy _serviceProxy = new OrganizationServiceProxy(
                    oUri,
                    null,
                    clientCredentials,
                    null);

                OrgService = _serviceProxy;
                /*

                extraParams += "DeviceID=dts-ba9f6b7b2e6d; DevicePassword=dts123_;";
                connectionString += extraParams;

                connection = Microsoft.Xrm.Client.CrmConnection.Parse(connectionString);
                OrgService = new OrganizationService(connection);
                 */
                OrgContext = new OrganizationServiceContext(OrgService);
            
        }

        public void MainConnect(string connectionString)
        {
            Microsoft.Xrm.Client.CrmConnection crmconnection = Microsoft.Xrm.Client.CrmConnection.Parse(connectionString);
            crmconnection.Timeout = new TimeSpan(0, 3, 0);
            OrgService = new OrganizationService(crmconnection);
            OrgContext = new OrganizationServiceContext(OrgService);
        }

        public void GetConfiguration()
        {
            if (this.EnvironmentType == CRMEnvironmentType.EMS)
            {
                var configuration = (from c in OrgContext.CreateQuery("oems_integrationconfiguration")
                                     where (int)c["statecode"] == 0
                                     select c).FirstOrDefault();
                    /* unused
                                        if (configuration.Contains("oems_integrationerrornotificationuser"))
                                        {
                                            ErrorNotificationUser = (EntityReference)configuration["oems_integrationerrornotificationuser"];
                                        }
                                        if (configuration.Contains("oems_integrationtaskuser"))
                                        {
                                            UpdateRequestUser = (EntityReference)configuration["oems_integrationtaskuser"];
                                        }
                    */

                if (configuration.Contains("oems_mbrteam"))
                {
                    MBRreferenceUpdateRequestTeam = (string) configuration["oems_mbrteam"];
                }
                else
                {
                    Log.Fatal("MBR Team Name must be defined in the Integration Configuration ");
                    throw new Exception("MBR Team Name must be defined in the Integration Configuration ");
                }
            
                /* unused
                if (configuration.Contains("oems_mbrintegrationerrornotificationuser"))
                {
                    MBRreferenceErrorNotificatorUser = (string)configuration["oems_mbrintegrationerrornotificationuser"];
                }
            
                if (configuration.Contains("oems_mbrintegrationtaskuser"))
                {
                    MBRreferenceUpdateRequestUser = (string)configuration["oems_mbrintegrationtaskuser"];
                }      
                */
            }
            else
            {
                //Get mbr configurations

                /* unused
                    if (!string.IsNullOrEmpty(MBRreferenceErrorNotificatorUser))
                    {
                        var user  = (from c in OrgContext.CreateQuery("systemuser")
                                     where (string)c["domainname"] == MBRreferenceErrorNotificatorUser
                                 select c).FirstOrDefault();
                        ErrorNotificationUser = user.ToEntityReference();
                    }
                    if (!string.IsNullOrEmpty(MBRreferenceUpdateRequestUser))
                    {
                        var user = (from c in OrgContext.CreateQuery("systemuser")
                                    where (string)c["domainname"] == MBRreferenceUpdateRequestUser
                                    select c).FirstOrDefault();
                        UpdateRequestUser = user.ToEntityReference();
                    }
                */

                if (!string.IsNullOrEmpty(MBRreferenceUpdateRequestTeam))
                {
                    var team = (from c in OrgContext.CreateQuery("team")
                                where (string)c["name"] == MBRreferenceUpdateRequestTeam
                                select c).FirstOrDefault();
                    if (team == null)
                    {
                        Log.FatalFormat("Could not find the team named '{0}' check Integration Configuration in EMS ", MBRreferenceUpdateRequestTeam);
                        throw new Exception("Could not find the team named " + MBRreferenceUpdateRequestTeam +
                                            " check Integration Configuration in EMS ");
                    }
                    UpdateRequestTeam = team.ToEntityReference();
                }
            }
            
        }

        public CRMEnvironment(string connectionString, CRMEnvironmentType environmentType,int batchSize)
        {
            this.connectionString = connectionString;
            this.EnvironmentType = environmentType;
            this.BatchSize = batchSize;

            MainConnect(connectionString);

            GetConfiguration();
        }

        public CRMEnvironment(string connectionString, CRMEnvironmentType environmentType, int batchSize, string referenceUpdateRequestTeam)
        {
            this.connectionString = connectionString;
            this.EnvironmentType = environmentType;
            this.BatchSize = batchSize;
            // unused
            //            this.MBRreferenceErrorNotificatorUser = referenceErrorNotificationUser;
            //            this.MBRreferenceUpdateRequestUser = referenceUpdateUserRequest;
            this.MBRreferenceUpdateRequestTeam = referenceUpdateRequestTeam;

            MainConnect(connectionString);

            GetConfiguration();
        }

        public void SetEntityFields(SqlDataReader reader, Entity entity, List<FieldDefinition> fields)
        {
            foreach (var f in fields)
            {
                if (!(reader[f.FieldName] is DBNull))
                {
                    switch (f.FieldType)
                    {
                        case CRMFieldType.DATETIME_FIELD:
                            entity[f.FieldName] = (DateTime)reader[f.FieldName];
                            break;
                        case CRMFieldType.DECIMAL_FIELD:
                            entity[f.FieldName] = (decimal)reader[f.FieldName];
                            break;
                        case CRMFieldType.DOUBLE_FIELD:
                            entity[f.FieldName] = (double)reader[f.FieldName];
                            break;
                        case CRMFieldType.INTEGER_FIELD:
                            entity[f.FieldName] = (int)reader[f.FieldName];
                            break;
                        case CRMFieldType.LOOKUP_FIELD:
                            entity[f.FieldName] = new EntityReference("UnknownEntity", (Guid)reader[f.FieldName]);
                            break;
                        case CRMFieldType.MONEY_FIELD:
                            entity[f.FieldName] = new Money((Decimal)reader[f.FieldName]);
                            break;
                        case CRMFieldType.OPTIONSET_FIELD:
                            entity[f.FieldName] = new OptionSetValue((int)reader[f.FieldName]);
                            break;
                        case CRMFieldType.STRING_FIELD:
                            entity[f.FieldName] = (string)reader[f.FieldName];
                            break;
                        case CRMFieldType.BOOL_FIELD:
                            entity[f.FieldName] = (bool)reader[f.FieldName];
                            break;
                    }
                }
            }
        }

        public Entity SetEntityFromSql(SqlDataReader reader, string logicalName, List<FieldDefinition> fields, bool doRead = true)
        {
            Entity result = null;
            if (!doRead || reader.Read())
            {
                result = new Entity(logicalName);
                result.Id = (Guid)reader[logicalName+"id"];
                SetEntityFields(reader, result, fields);
            }
            return result;
        }

        

        public Entity RetrieveEntityFromSql(string logicalName, string id, List<FieldDefinition> fields)
        {
            StringBuilder sb = new StringBuilder("select ");
            
            for(int i = 0; i < fields.Count; i++)
            {
                 if(i > 0) sb.Append(",");
                 sb.Append(fields[i].FieldName);
            }
            sb.Append(" from Filtered").Append(logicalName).Append(" where ").Append(logicalName).Append("id='").Append(id).Append("'");
            
           SqlCommand cmd = new SqlCommand(sb.ToString(), SQLCon);
           cmd.CommandTimeout = 0; 
           var reader = cmd.ExecuteReader();


           Entity result =  SetEntityFromSql(reader, logicalName, fields);
           reader.Close();
           return result;
           
        }

        public Entity RetrieveEntityFromSqlByName(string logicalName, string field, string fieldValue, List<FieldDefinition> fields)
        {
            StringBuilder sb = new StringBuilder("select ");

            for (int i = 0; i < fields.Count; i++)
            {
                if (i > 0) sb.Append(",");
                sb.Append(fields[i].FieldName);
            }

            if (fields.Count <= 0) sb.Append(logicalName).Append("id");

            sb.Append(" from ").Append(logicalName).Append("base").Append(" where ").Append(field).Append("='").Append(fieldValue).Append("'");

            SqlCommand cmd = new SqlCommand(sb.ToString(), SQLCon);
            cmd.CommandTimeout = 0;
            var reader = cmd.ExecuteReader();

            Entity result = SetEntityFromSql(reader, logicalName, fields);
            reader.Close();
            return result;
            
        }

        public List<Entity> RetrieveMultipleFromSql(string selectCmd, string logicalName, List<FieldDefinition> fields)
        {
            SqlCommand cmd = new SqlCommand(selectCmd, SQLCon);
            cmd.CommandTimeout = 0; 
            var reader = cmd.ExecuteReader();
            List<Entity> result = new List<Entity>();
            Entity currentEntity = null;
            while( (currentEntity = SetEntityFromSql(reader, logicalName, fields)) != null)
            {
               result.Add(currentEntity);
            }
            reader.Close();
            return result;

        }


        
        public Entity RetrieveCreateRecord(string entityName, string lookupField, string id, bool create = true)
        {
         
            string cmdString = "select " + entityName + "id from " + entityName + " where "+lookupField + "=@lookupField";

            SqlCommand cmd = new SqlCommand(cmdString, SQLCon);
            cmd.CommandTimeout = 0; 
            cmd.Parameters.Add(new SqlParameter("@lookupField", ""));
            
            cmd.Parameters[0].Value = id;
            

            var entityId = cmd.ExecuteScalar();
            Entity ent = new Entity(entityName);
            if (entityId is DBNull || entityId == null)
            {
                if(!create) return null;

                CreateRequest cr = new CreateRequest();
                cr.Target= ent;
                //ent.Id = Guid.NewGuid();
                ent[lookupField] = id;
                CurrentBatch.Requests.Add(cr);
             
            }
            else
            {
                string existingId = entityId.ToString();
                ent.Id = Guid.Parse(existingId);
                UpdateRequest ur = new UpdateRequest();
                ur.Target = ent;
                CurrentBatch.Requests.Add(ur);
            }
            return ent;

            /*
            QueryByAttribute querybyattribute = new QueryByAttribute(entityName);
            querybyattribute.ColumnSet = new ColumnSet(true);
            querybyattribute.Attributes.AddRange(lookupField);
            querybyattribute.Values.AddRange(id);
            
            var existingCollection = OrgService.RetrieveMultiple(querybyattribute);
            Entity existingItem = null;
            if (existingCollection.Entities.Count == 0)
            {
               if (!create)
                    return null;

               existingItem = new Entity(entityName);
               existingItem[lookupField] = id;
               existingItem.Id = OrgService.Create(existingItem);
               if(entityName.ToLower() == "account")
               {
                   
                   SetStateRequest setState = new SetStateRequest();
                   setState.EntityMoniker = existingItem.ToEntityReference();
                   setState.State = new OptionSetValue();
                   setState.State.Value = 1;
                   setState.Status = new OptionSetValue();
                   setState.Status.Value = 2;
                   SetStateResponse setStateResponse = (SetStateResponse)OrgService.Execute(setState);
               }

            }
            else
            {
               existingItem = existingCollection.Entities[0];
            }
             
            return existingItem;
             */
        }

        public void SetReccordToDelete(EntityContainer ec) 
        {
            DeleteRequest dr = new DeleteRequest();
            dr.Target = ec.Entity.ToEntityReference();
            CurrentBatch.Requests.Add(dr);

        }

        public Entity RetrieveCreateRecord(EntityContainer ec, bool create = true,bool sync = false)
        {

            
            if (ec.Create)
            {
                if (!create) return null;

                CreateRequest cr = new CreateRequest();
                cr.Target = ec.Entity;
                if(ec.Entity.Id == Guid.Empty )
                {
                    ec.Entity.Id = Guid.NewGuid();
                }
                
                CurrentBatch.Requests.Add(cr);
             
            }
            else
            {
                UpdateRequest ur = new UpdateRequest();
                ur.Target = ec.Entity;
                CurrentBatch.Requests.Add(ur);
            }
            return ec.Entity;
        }

        public Entity RetrieveCreateRecord(EntityContainer ec, out OrganizationRequest or, bool create = true,bool sync = false)
        {
            or = null;

            if (ec.Create)
            {
                if (!create) return null;

                CreateRequest cr = new CreateRequest();
                cr.Target = ec.Entity;
                if (ec.Entity.Id == Guid.Empty && !sync)
                {
                    ec.Entity.Id = Guid.NewGuid();
                }
                or = cr;

                CurrentBatch.Requests.Add(cr);
                
            }
            else
            {
                UpdateRequest ur = new UpdateRequest();
                ur.Target = ec.Entity;
                CurrentBatch.Requests.Add(ur);
                or = ur;
            }
            return ec.Entity;
        }

        public Entity RetrieveCreateRecordWithoutAddingRequest(EntityContainer ec, bool create = true)
        {
            if (ec.Create)
            {
                if (!create) return null;

                CreateRequest cr = new CreateRequest();
                cr.Target = ec.Entity;
                if (ec.Entity.Id == Guid.Empty)
                {
                    ec.Entity.Id = Guid.NewGuid();
                }
            }
            else
            {
                UpdateRequest ur = new UpdateRequest();
                ur.Target = ec.Entity;
            }
            return ec.Entity;
        }

        public List<EntityContainer> RetrieveCreateRecordList(string entityName, string lookupField, List<string> idList, bool create = true, List<FieldDefinition> additionalFields = null)
        {
            int BATCH_SIZE = 400;
            List<EntityContainer> result = new List<EntityContainer>();
            
            
            List<string> createList = new List<string>();
            

            StringBuilder condition =  new StringBuilder();
            bool firstStep = true;
            for(int i = 0; i < idList.Count; i++)
            {
                if(!firstStep) condition.Append(",");
                else firstStep = false;
                idList[i] = idList[i].Replace("'", "''");
                condition.Append("'" + idList[i] + "'");
                createList.Add(idList[i]);
                if (i % BATCH_SIZE == 0 && i > 0 || i == idList.Count - 1)
                {
                    string cmdString = "select " + entityName + "id, "+ lookupField;
                    if(additionalFields != null)
                    {
                       foreach(var f in additionalFields)
                       {
                          cmdString += "," + f.FieldName;
                       }
                       cmdString += " ";
                    }
                    cmdString += " from " + entityName + " where " + lookupField + " in ("+condition.ToString()+")";

                    Log.InfoFormat("Processing RetrieveCreateRecordList fucntion for query: {0} ", cmdString);

                    SqlCommand cmd = new SqlCommand(cmdString, SQLCon);
                    cmd.CommandTimeout = 0; 
                    SqlDataReader reader = cmd.ExecuteReader();
                    while(reader.Read())
                    {
                       string lookupId = reader[lookupField].ToString();
                       createList.Remove(lookupId);
                       

                       Entity ent = new Entity(entityName);
                       ent.Id = (Guid)reader[entityName+"id"];
                       if (additionalFields != null)
                       {
                           SetEntityFields(reader, ent, additionalFields);
                       }

                       EntityContainer ec = new EntityContainer();
                       ec.Entity = ent;
                       ec.LookupField = lookupId;
                       ec.Create = false;
                       result.Add(ec);
                       


                    }
                    reader.Close();
                    condition = new StringBuilder();
                    
                    firstStep = true;
                }
            }
            if(create)
            {
                foreach(var s in createList)
                {
                    Entity ent = new Entity(entityName);
                    ent[lookupField] = s;
                    if(lookupField == entityName+"id")
                    {
                       ent.Id = Guid.Parse(s);
                       ent.Attributes.Remove(lookupField);
                       //ent[lookupField] = ent.Id;
                    }
                    EntityContainer ec = new EntityContainer();
                    ec.Entity = ent;
                    ec.LookupField = s;
                    ec.Create = true;
                    result.Add(ec);
                }
            }
            return result;

           
        }


        public ExecuteMultipleRequest CreateBatchRequest()
        {    
            ExecuteMultipleRequest request = new ExecuteMultipleRequest()
            {
                Settings = new ExecuteMultipleSettings()
                {
                    ContinueOnError = false,
                    ReturnResponses = true
                    
                },
                
                Requests = new OrganizationRequestCollection()
            };
            return request;
        }

        

        //public ExecuteMultipleResponse ExecuteBatch(ExecuteMultipleRequest request)
        //{
        //  if(request.Requests.Count == 0) return null;

        //  ExecuteMultipleResponse response = (ExecuteMultipleResponse)OrgService.Execute(request);
        //  if (response != null)
        //  {
        //      foreach (var r in response.Responses)
        //      {
                  
        //          if (r.Fault != null)
        //          {
        //              request.Requests.RemoveAt(0);
        //              throw new Exception("Error in batch: " + r.Fault.Message);
        //          }
        //          request.Requests.RemoveAt(0);
        //      }
        //  }
        //  return response;
        //}

        public ExecuteMultipleResponse ExecuteBatch(ExecuteMultipleRequest request, int numberOfBatch,string direction)
        {

            var taskcache = TaskCache.GetInstance();
            if (request.Requests.Count == 0) return null;

            ExecuteMultipleResponse response = (ExecuteMultipleResponse)OrgService.Execute(request);

            taskcache.Flush();

            if (response != null)
            {
                foreach (var r in response.Responses)
                {

                    if (r.Fault != null)
                    {
                        string error = ProcessError(r, request);

                        request.Requests.RemoveAt(r.RequestIndex);
                        Exception ex = new Exception("Error in batch: " + error);
                        ex.Data.Add("RequestIndex", numberOfBatch + r.RequestIndex);
                        ex.Data.Add("Direction", direction);
                        throw (ex);
                    }
                    else
                    {
                        //request.Requests[r.Response.Results.]
                        try
                        {
                            if (request.Requests[r.RequestIndex].RequestName == "Create")
                            {
                                if (request.Requests[r.RequestIndex].Parameters.Contains("Target"))
                                {

                                    Entity executedReq = (Entity)request.Requests[r.RequestIndex].Parameters["Target"];
                                    if (executedReq.LogicalName == "account")
                                    {
                                        EntityReference account = new EntityReference("account", (Guid)r.Response.Results["id"]);
                                        SetStateRequest setState = new SetStateRequest();
                                        setState.EntityMoniker = account;
                                        setState.State = new OptionSetValue();
                                        setState.State.Value = 1;
                                        setState.Status = new OptionSetValue();
                                        setState.Status.Value = 2;
                                        OrgService.Execute(setState);
                                    }
                                }
                            }
                        }
                        catch (Exception ewx) 
                        {
                            Exception ChangeStatus = new Exception("Exception when trying to change the status of an account.", ewx);
                            throw (ChangeStatus);
                        }
                    }
                    //request.Requests.RemoveAt(0);
                }
            }

            
            return response;
        }

        private string ProcessError(ExecuteMultipleResponseItem response, ExecuteMultipleRequest request) 
        {

            string resultMessage = null;
            switch (request.Requests[response.RequestIndex].RequestName) 
            {
                case "Create":
                    CreateRequest creq = (CreateRequest)request.Requests[response.RequestIndex];
                    resultMessage = string.Concat("- SdkMessage: ",creq.RequestName," ,Entity Type: ", creq.Target.LogicalName, " -Id: ", creq.Target.Id);
                break;
                case "Update":
                    UpdateRequest ureq = (UpdateRequest)request.Requests[response.RequestIndex];
                    resultMessage = string.Concat("- SdkMessage: ", ureq.RequestName, " ,Entity Type: ", ureq.Target.LogicalName, " -Id: ", ureq.Target.Id);
                break;
                case "Delete":
                DeleteRequest delReq = (DeleteRequest)request.Requests[response.RequestIndex];
                resultMessage = string.Concat("- SdkMessage: ", delReq.RequestName, " ,Entity Type: ", delReq.Target.LogicalName, " -Id: ", delReq.Target.Id);
                break;
                case "SetState":
                    SetStateRequest setReq = (SetStateRequest)request.Requests[response.RequestIndex];
                    resultMessage = string.Concat("- SdkMessage: ", setReq.RequestName, " ,Entity Type: ", setReq.EntityMoniker.LogicalName, " -Id: ", setReq.EntityMoniker.Id);
                break;
                default:
                     Microsoft.Xrm.Sdk.OrganizationRequest oreq = (OrganizationRequest)request.Requests[response.RequestIndex];
                     resultMessage = string.Concat("- SdkMessage: ", oreq.RequestName, " ,No more information associated");
                     break;

            }

            resultMessage = string.Concat(resultMessage, " ,Error: ", response.Fault.Message);
            InformMissingReccordError(resultMessage);
            return resultMessage;
        }

        private void InformMissingReccordError(string message) 
        {
            ILog LogMissingReccords = log4net.LogManager.GetLogger("MissingReccords");
            LogMissingReccords.Error(message);
        }


        public List<ExecuteMultipleResponse> ExecuteBatchWithLimit(ExecuteMultipleRequest request,string direction)
        {
            if (request.Requests.Count == 0) return null;
            //int batchSize = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["batchSize"]);
            var limitedBatch = CreateBatchRequest();
            List<ExecuteMultipleResponse> result = new List<ExecuteMultipleResponse>();
            int numberOfBatches =0;

            for(var i = 0; i < request.Requests.Count; i++)
            {
               limitedBatch.Requests.Add(request.Requests[i]);
               if (limitedBatch.Requests.Count > 0 && limitedBatch.Requests.Count % this.BatchSize == 0 ||
                  i == request.Requests.Count - 1)
               {
                   
                  var limitedResponse = ExecuteBatch(limitedBatch,numberOfBatches,direction);
                  result.Add(limitedResponse);
                  limitedBatch.Requests.Clear();

                  numberOfBatches++;
               }
            }
            request.Requests.Clear();
            return result;
        }

        public void AddBatchRequest(ExecuteMultipleRequest request, OrganizationRequest item)
        {
            request.Requests.Add(item);
        }
    }

}

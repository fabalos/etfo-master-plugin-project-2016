﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xrm.Sdk;

namespace Omniware.ETFO.IntegrationLoader
{

    

    public class DataQueueItem
    {
        public Guid Id { get; set; }
        public string Fields { get; set; }
        public string EntityName { get; set; }
        public string RecordId { get; set; }
        public string RelatedEntityName { get; set; }
        public string RelatedRecordId { get; set; }
        public string Operation { get; set; }
        public string ErrorMessage { get; set; }

        public Entity Entity {get;set;}
        public CRMEnvironment CRMEnvironment { get; set; }
    }
}

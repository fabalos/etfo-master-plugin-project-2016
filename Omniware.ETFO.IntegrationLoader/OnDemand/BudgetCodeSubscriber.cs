﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using log4net;

namespace Omniware.ETFO.IntegrationLoader.OnDemand
{
    public class BudgetCodeSubscriber : IOnDemandSubscriber
    {
        public bool LoadData(Worker worker,ILog log )
        {
            RetrieveAttributeRequest rar = new RetrieveAttributeRequest();
            rar.EntityLogicalName = "reg_releasetimetracking";
            rar.LogicalName = "reg_budgetline";
            RetrieveAttributeResponse response = (RetrieveAttributeResponse)worker.MRSEnvironment.OrgService.Execute(rar);
            Microsoft.Xrm.Sdk.Metadata.PicklistAttributeMetadata pam =  (Microsoft.Xrm.Sdk.Metadata.PicklistAttributeMetadata)response.AttributeMetadata;
            try
            {
                foreach (var p in pam.OptionSet.Options)
                {
                    var existingCode = (from c in worker.EMSEnvironment.OrgContext.CreateQuery("oems_budgetcode")
                                        where (string)c["oems_etfoid"] == p.Value.ToString()
                                        select c).FirstOrDefault();
                    if (existingCode == null)
                    {
                        existingCode = new Entity("oems_budgetcode");
                        existingCode["oems_etfoid"] = p.Value.ToString();
                        existingCode.Id = worker.EMSEnvironment.OrgService.Create(existingCode);
                    }

                    existingCode = worker.EMSEnvironment.OrgService.Retrieve("oems_budgetcode", existingCode.Id, new Microsoft.Xrm.Sdk.Query.ColumnSet());
                    existingCode["oems_budgetnumber"] = p.Label.UserLocalizedLabel.Label;

                    worker.EMSEnvironment.OrgService.Update(existingCode);
                    log.Debug("Updated existing code entity " + existingCode.Id);
                }


                return true;
            }
            catch (Exception e) 
            {
                log.Error("Error on BC subscriber when trying to update or create a new entry. ", e);
                throw (e);
            }
        }
    }
}

﻿using System;
using System.Linq;
using log4net;
using log4net.Config;
using System.Reflection;
using System.Diagnostics;

namespace Omniware.ETFO.IntegrationLoader
{


    class Program
    {
        // logger is configured via Assembly.cs attribute, log file location can be changed in App.config
        static ILog Log = LogManager.GetLogger(typeof(Program));


        static void Main(string[] args)
        {

            Log.Warn("\n\n");
            Log.Warn("Process STARTED\t==============================================================");
            
            if (args.Length > 0 && args[0] == "/help")
            {
                Log.Info("Displaying help");
                Console.WriteLine(""); Console.WriteLine(""); Console.WriteLine("");
                Console.WriteLine("Omniware.ETFO.Integration.DataShaman.exe <switch> <parameter> ");
                Console.WriteLine("");
                Console.WriteLine("No switches, no parameters - standard mode, processing changes");
                Console.WriteLine("");
                Console.WriteLine("Switches:");
                Console.WriteLine("");
                Console.WriteLine("/budgetCodesOnly                    - pre-load EMS with budget codes");
// not fully tested, not officially supported
//                Console.WriteLine("");
//                Console.WriteLine("/update                      - quick update for newly added fields etc. Specify update version in place of <parameter>. All updates with greater version number will be applied.");
                Console.WriteLine("");
                Console.WriteLine("/toEMS                       - Uses latest information from Member Of Records system to Update EMS, this  includes Accounts, Contacts, Schools, Organization Contacts and Release Time tracking");
                Console.WriteLine("");
                Console.WriteLine("/fromEMS                     - Carries information form EMS Member of Records Update Requests, Release Time Request to Member of Records system.");
                Console.WriteLine("");
                Console.WriteLine("/releasetimeOnly             - Will transfer only the Release Time related records. If no direction is specified transfer happens  in both directions.");
                Console.WriteLine("");
                Console.WriteLine("/releasetimeonly /fromEMS    - Will transfer only Release Time Request information from EMS to Member of Records System.");
                Console.WriteLine("");
                Console.WriteLine("/nonReleasetimeOnly /fromEMS - Will transfer only Account and Contact updates (Member of Records Update Requests) from EMS to Member of Records System.");
                Console.WriteLine("");
                Console.WriteLine("/nonReleasetimeOnly /toEMS   - Will take non Release Time information form Member of Records system and update EMS Accounts, Contacts, Schools, Organization Contacts.");
                Console.WriteLine("/PresidentsOnly              - Will sync only the president's role on EMS");
                Console.WriteLine("/SyncTasks                  - Will clean up all the pending tasks that are no longer needed");
                Console.WriteLine(""); Console.WriteLine(""); Console.WriteLine("");


                Environment.Exit(0);

            }


            // replaced with command line option Log.Info("Load Direction from config - " + Enum.GetName(typeof(LOAD_DIRECTION), Config.LoadDirection));

            try
            {
                bool budgetCodesOnlyArg =
                    args.Any(arg => "/budgetCodesOnly".Equals(arg, StringComparison.InvariantCultureIgnoreCase));
                bool toEmsArg = args.Any(arg => "/toEMS".Equals(arg, StringComparison.InvariantCultureIgnoreCase));
                bool fromEmsArg = args.Any(arg => "/fromEMS".Equals(arg, StringComparison.InvariantCultureIgnoreCase));
                bool releasetimeOnlyArg =
                    args.Any(arg => "/releasetimeOnly".Equals(arg, StringComparison.InvariantCultureIgnoreCase));
                bool nonReleasetimeOnlyArg =
                    args.Any(arg => "/nonReleasetimeOnly".Equals(arg, StringComparison.InvariantCultureIgnoreCase));

                bool synchronization = args.Any(arg => "/Sync".Equals(arg, StringComparison.InvariantCulture));

                bool presidentsOnly = args.Any(arg => "/PresidentsOnly".Equals(arg, StringComparison.InvariantCulture));

                bool syncTasks = args.Any(arg => "/SyncTasks".Equals(arg, StringComparison.InvariantCulture));

                Log.InfoFormat(
                    "Parameters budgetCodesOnlyArg={0} toEmsArg={1} fromEMS={2} releasetimeOnly={3} nonReleasetimeOnly={3}",
                    budgetCodesOnlyArg, toEmsArg, fromEmsArg, releasetimeOnlyArg, nonReleasetimeOnlyArg);

                if (toEmsArg && fromEmsArg)
                {
                    Log.Fatal("Conficting /toEms and /fromEMS option");
                    Environment.Exit(-1);
                }
                if (releasetimeOnlyArg && nonReleasetimeOnlyArg)
                {
                    Log.Fatal("Conficting /releasetimeOnly and /nonReleasetimeOnly option");
                    Environment.Exit(-1);
                }


                if (budgetCodesOnlyArg)
                {
                    Log.Info("budgetCodesOnly mode");
                    new Worker().BudgetCodesOnly(Log);
                }
// not fully tested, not officially supported
//                    else if (args.Length > 0 && args[0] == "/update")
//                    {
//                        //Logger.LogMessage("Update mode", LOG_LEVEL.PROGRESS);
//                        w = new Worker();
//                        Log.Info( "Update mode");
//                        w.Update(args[1].Trim());
//                    }
                else
                {
                    bool? directionToEms = null;
                    if (toEmsArg) directionToEms = true;
                    if (fromEmsArg) directionToEms = false;
                    //else null => both directions

                    bool? relasetimeSubset = null;
                    if (releasetimeOnlyArg) relasetimeSubset = true;
                    if (nonReleasetimeOnlyArg) relasetimeSubset = false;
                    //else null => everything

                    bool? sync = null;
                    if (synchronization) sync = true;
                    

                    Log.InfoFormat("Standard mode directionToEms={0} relasetimeOnlySubset={1}", directionToEms,
                                   relasetimeSubset);
                    new Worker(directionToEms,sync, relasetimeSubset,presidentsOnly,syncTasks).ProcessChanges();
                }
            }
            catch (Exception ex)
            {

                int lineError = GetErrorLine(ex);
                Log.Error(ex.Message + "; On line:" + lineError.ToString() , ex);
                if (ex.InnerException != null) Log.Error(ex.InnerException.Message, ex);
                throw; // exit with exception
            }
            finally
            {
                LoadProgress.GetLoadProgress().SaveProgress();
                Log.Warn("Process COMPLETED\t==============================================================\n\n");
            }
        }

        static int GetErrorLine(Exception ex) 
        {
            var st = new StackTrace(ex, true);
            // Get the top stack frame
            var frame = FindFrameWithError(st);
            // Get the line number from the stack frame
            return frame.GetFileLineNumber();
        }

        static StackFrame FindFrameWithError(StackTrace st) 
        {
            foreach (StackFrame frame in st.GetFrames()) 
            {
                if (frame.GetFileLineNumber() > 0) 
                {
                    return frame;
                }
            }
            return null;
        }
    }
}
 
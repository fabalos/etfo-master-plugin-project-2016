﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Omniware.ETFO.IntegrationLoader;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Omniware.ETFO.IntegrationLoader.MBREMSSync
{
    public class PresidentsSyncSubscriber: IDataSyncSubscriber
    {
        const int PRESIDENTROLE = 347780000;
        public void Sync(Worker worker)
        {
            ResetAllPresidents(worker);
            SetPresidentsFromMBR(worker);
        }


      

        void ResetAllPresidents(Worker worker)
        {
            //Get all the accounts in which the role is the president
            var presidents = (from p in worker.EMSEnvironment.OrgContext.CreateQuery("account") where (int)p["oems_role"] == PRESIDENTROLE select p).ToList<Entity>();


            if (worker.EMSEnvironment.CurrentBatch == null)
                worker.EMSEnvironment.CurrentBatch = worker.EMSEnvironment.CreateBatchRequest();
            //reset all the presidents
            foreach (Entity president in presidents)
            {
                Entity toUpdate = new Entity(president.LogicalName);
                toUpdate.Id = president.Id;
                toUpdate.Attributes["oems_role"] = null;
                UpdateRequest ur = new UpdateRequest();
                ur.Target = toUpdate;

                worker.EMSEnvironment.CurrentBatch.Requests.Add(ur);
            }

        }

        void SetPresidentsFromMBR(Worker worker)
        {
            var currentPresidents = (from p in worker.MRSEnvironment.OrgContext.CreateQuery("reg_organizationcontacts") where (bool)p["reg_currentpresident"] == true select p).ToList<Entity>();

            List<string> contactIdlist = new List<string>();

            foreach (Entity president in currentPresidents)
            {
                EntityReference eref = (EntityReference)president["reg_contactid"];
                Entity contact = worker.MRSEnvironment.OrgService.Retrieve(eref.LogicalName, eref.Id, new Microsoft.Xrm.Sdk.Query.ColumnSet("reg_etfoid"));

                contactIdlist.Add(contact["reg_etfoid"].ToString());
            }

            var accountList = worker.EMSEnvironment.RetrieveCreateRecordList("account", "oems_etfomemberid", contactIdlist, false);


            foreach (EntityContainer presToUpdate in accountList)
            {
                Entity accupd = new Entity(presToUpdate.Entity.LogicalName);
                accupd.Id = presToUpdate.Entity.Id;
                accupd["oems_role"] = new OptionSetValue(PRESIDENTROLE);

                UpdateRequest ur = new UpdateRequest();
                ur.Target = accupd;
                worker.EMSEnvironment.CurrentBatch.Requests.Add(ur);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

using Microsoft.Xrm.Sdk;

using Microsoft.Xrm.Sdk.Linq;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Client.Services;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Messages;
using log4net;

namespace Omniware.ETFO.IntegrationLoader.MBREMSSync
{


    public class ContactSubscriber : IDataQueueSubscriber
    {
        static ILog Log = LogManager.GetLogger(typeof(ContactSubscriber));

        public override string EntityName
        {
            get
            {
                return "contact";
            }
        }

        public static Entity ProcessRecord(Worker worker, Entity source, Entity target)
        {
            Log.DebugFormat("Processing {0}#{1} ", source.LogicalName, source.Id);


            if (source.Contains("fullname")) target["name"] = source["fullname"];
            else target["name"] = "";
            
             target["oems_memberflag"] = true;

             if (source.Contains("reg_onleave")) target["oems_memberonleaveflag"] = source["reg_onleave"];
             else target["oems_memberonleaveflag"] = null;

             if (source.Contains("reg_holdreasonName")) target["oems_memberingoodstandingflag"] = ((string)source["reg_holdreasonName"]).ToUpper().IndexOf("DISCIPL") == -1;
             else target["oems_memberingoodstandingflag"] = true;

             if (source.Contains("firstname")) target["oems_firstname"] = source["firstname"];
             else target["oems_firstname"] = null;

             if (source.Contains("lastname")) target["oems_lastname"] = source["lastname"];
             else target["oems_lastname"] = null;

             if (source.Contains("address1_line1")) target["address1_line1"] = source["address1_line1"];
             else target["address1_line1"] = null;
            
            if (source.Contains("address1_line2")) target["address1_line2"] = source["address1_line2"];
            else target["address1_line2"] = null;
            
            if (source.Contains("address1_city")) target["address1_city"] = source["address1_city"];
            else target["address1_city"] = null;

            if (source.Contains("address1_country")) target["address1_country"] = source["address1_country"];
            else target["address1_country"] = null;

            if (source.Contains("address1_stateorprovince")) target["address1_stateorprovince"] = source["address1_stateorprovince"];
            else target["address1_stateorprovince"] = null;

            if (source.Contains("address1_postalcode")) target["address1_postalcode"] = source["address1_postalcode"];
            else target["address1_postalcode"] = null;

            if (source.Contains("telephone2")) target["telephone2"] = source["telephone2"];
            else target["telephone2"] = null;

            if (source.Contains("mobilephone")) target["telephone3"] = source["mobilephone"];
            else target["telephone3"] = null;

            if (source.Contains("emailaddress1")) target["emailaddress2"] = source["emailaddress1"];
            else target["emailaddress2"] = null;
            
            if (source.Contains("reg_aboriginal")) target["oems_aboriginalflag"] = source["reg_aboriginal"];
            else target["oems_aboriginalflag"] = null;

            if (source.Contains("reg_disabled")) target["oems_disabledflag"] = source["reg_disabled"];
            else target["oems_disabledflag"] = null;

            if (source.Contains("reg_gaylesbianbisexualtransgender")) target["oems_lesbiangaybisexualtransgenderflag"] = source["reg_gaylesbianbisexualtransgender"];
            else target["oems_lesbiangaybisexualtransgenderflag"] = null;

            if (source.Contains("reg_active")) target["oems_etfoactive"] = source["reg_active"];
            else target["oems_etfoactive"] = null;

            if (source.Contains("reg_racialminority")) target["oems_racializedgroupflag"] = source["reg_racialminority"];
            else target["oems_racializedgroupflag"] = null;


            string sinCheck = null;
            if (source.Contains("reg_sin") && source["reg_sin"] != null)
            {
                sinCheck = (string)source["reg_sin"];
                sinCheck = sinCheck.Replace(" ", "");
                if (sinCheck.Length >= 4)
                {
                    sinCheck = sinCheck.Substring(3, sinCheck.Length > 6 ? 3 : sinCheck.Length - 3);
                }
                else
                {
                    sinCheck = null;
                }
            }

            target["oems_sincheck"] = sinCheck;


            target["oems_skipupdatequeue"] = true;

            return target;
        }


        public override void ProcessList(Worker worker, List<Entity> sourceEntityList)
        {
            //Entity emsEntity = worker.EMSEnvironment.RetrieveCreateRecord("account", "oems_etfomemberid", (string)contact["reg_etfoid"]);

            List<string> IdList = new List<string>();
            foreach (var e in sourceEntityList)
            {
                if(e.Contains("reg_etfoid"))
                {
                    IdList.Add((string)e["reg_etfoid"]);
                }
            }

            var entityList = worker.EMSEnvironment.RetrieveCreateRecordList("account", "oems_etfomemberid", IdList);


            if (entityList.Count > 0)
            {

                foreach (var e in sourceEntityList)
                {
                    if (e.Contains("reg_etfoid"))
                    {
                        EntityContainer ec = entityList.Find(x => x.LookupField == (string)e["reg_etfoid"]);
                        if (ec != null)
                        {
                            var target = worker.EMSEnvironment.RetrieveCreateRecord(ec,true,true);
                            ProcessRecord(worker, e, target);
                            Log.Debug("Processed record: " + e.Id);
                        }
                    }
                }
            }
            else 
            {
                foreach (var e in sourceEntityList) 
                {
                    EntityContainer ec = new EntityContainer() { Entity = new Entity("account") };
                    ec.Create = true;
                    var target = worker.EMSEnvironment.RetrieveCreateRecord(ec, true,true);
                    ProcessRecord(worker, e, target);
                    Log.Debug("Processed record");

                }
            }

        }
        

        private List<string> queries = null;
        public override List<string> GetQueries()
        {
            if (queries == null)
            {
                queries = new List<string>();
//                queries.Add(
//                     @"SELECT A.*,
//                           (SELECT TOP 1 reg_aboriginal FROM Filteredreg_selfid B WITH (NOLOCK) WHERE B.reg_memberid = A.ContactId) AS reg_aboriginal,
//	                       (SELECT TOP 1 reg_disabled FROM Filteredreg_selfid B WITH (NOLOCK) WHERE B.reg_memberid = A.ContactId) AS reg_disabled,
//	                       (SELECT TOP 1 reg_gaylesbianbisexualtransgender FROM Filteredreg_selfid B WITH (NOLOCK) WHERE B.reg_memberid = A.ContactId) AS reg_gaylesbianbisexualtransgender,
//	                       (SELECT TOP 1 reg_racialminority FROM Filteredreg_selfid B WITH (NOLOCK) WHERE  B.reg_memberid = A.ContactId) AS reg_racialminority,
//                           (SELECT TOP 1 reg_name FROM reg_sin B WITH (NOLOCK) WHERE B.reg_memberid = A.ContactId) AS reg_sin
//                      FROM FilteredContact A WITH (NOLOCK)
//					       
//                      WHERE A.modifiedOn > @modifiedon 
//					        OR EXISTS (SELECT TOP 1 * FROM Filteredreg_selfid B WITH (NOLOCK) WHERE B.reg_memberid = A.ContactId AND B.modifiedOn > @modifiedon)
//                            OR EXISTS (SELECT TOP 1 * FROM reg_sin B WITH (NOLOCK) WHERE B.reg_memberid = A.ContactId AND B.modifiedOn > @modifiedon)
//
//					  ");

                queries.Add(
                    @"IF OBJECT_ID('tempdb..#StringMap1') IS NOT NULL
                    /*Then it exists*/
                       DROP TABLE #StringMap1
                    
                    
                    SELECT t.ObjectTypeCode, e.Name, t.AttributeName, t.AttributeValue, t.Value
                    INTO #StringMap1
                    FROM [ETFO-CRMSQL\ETFOCRMSQL].ETFO_MSCRM.dbo.StringMap t INNER JOIN [ETFO-CRMSQL\ETFOCRMSQL].ETFO_MSCRM.MetadataSchema.Entity e ON
                        t.ObjectTypeCode = e.ObjectTypeCode
                    ORDER BY t.ObjectTypeCode, t.AttributeName, t.AttributeValue;
                    
                    SELECT A.fullname
                    ,A.reg_onleave
                    ,#StringMap1.Value as reg_holdreasonName
                    ,A.firstname
                    ,A.lastname
                    ,A.address1_line1
                    ,A.address1_line2
                    ,A.address1_city
                    ,A.address1_country
                    ,A.address1_stateorprovince
                    ,A.address1_postalcode
                    ,A.telephone2
                    ,A.mobilephone
                    ,A.ContactId
                    ,A.emailaddress1
                    ,A.reg_etfoid
                    ,A.reg_active
                                                
                     ,(SELECT TOP 1 reg_aboriginal FROM [ETFO-CRMSQL\ETFOCRMSQL].ETFO_MSCRM.dbo.reg_selfid B WITH (NOLOCK) WHERE B.reg_memberid = A.ContactId) AS reg_aboriginal,
                     (SELECT TOP 1 reg_disabled FROM [ETFO-CRMSQL\ETFOCRMSQL].ETFO_MSCRM.dbo.reg_selfid B WITH (NOLOCK) WHERE B.reg_memberid = A.ContactId) AS reg_disabled,
                     (SELECT TOP 1 reg_gaylesbianbisexualtransgender FROM [ETFO-CRMSQL\ETFOCRMSQL].ETFO_MSCRM.dbo.reg_selfid B WITH (NOLOCK) WHERE B.reg_memberid = A.ContactId) AS reg_gaylesbianbisexualtransgender,
                     (SELECT TOP 1 reg_racialminority FROM [ETFO-CRMSQL\ETFOCRMSQL].ETFO_MSCRM.dbo.reg_selfid B WITH (NOLOCK) WHERE  B.reg_memberid = A.ContactId) AS reg_racialminority,
                     (SELECT TOP 1 reg_name FROM [ETFO-CRMSQL\ETFOCRMSQL].ETFO_MSCRM.dbo.reg_sin B WITH (NOLOCK) WHERE B.reg_memberid = A.ContactId
                     and B.statuscode=1
                     and B.statecode =0) AS reg_sin
                     FROM [ETFO-CRMSQL\ETFOCRMSQL].ETFO_MSCRM.dbo.Contact A WITH (NOLOCK) left outer join #StringMap1  on 
                             #StringMap1.AttributeName = 'reg_holdreason'
                             and #StringMap1.ObjectTypeCode = 2
                             and #StringMap1.AttributeValue = A.Reg_HoldReason
                    	left join Account on A.reg_etfoid = Account.oems_ETFOMemberId
                    	where Account.oems_ETFOMemberId is null");

                
            }

            return queries;
        }

        public override List<FieldDefinition> FieldList
        {
            get
            {
                return SQLSupport.MRSContactFieldList;
            }
        }

        public override CRMEnvironmentType SubscriberType
        {
            get
            {
                return CRMEnvironmentType.MRS;
            }
        }

        public override ActionType SubscriberActionType
        {
            get
            {
                return ActionType.Sync;
            }
        }
    }
}
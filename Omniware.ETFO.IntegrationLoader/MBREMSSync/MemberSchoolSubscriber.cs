﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xrm.Sdk;

using Microsoft.Xrm.Sdk.Linq;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Client.Services;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Messages;
using log4net;
using Omniware.ETFO.IntegrationLoader;
using Omniware.ETFO.IntegrationLoader.MRSPostOperationSubscribers;


namespace Omniware.ETFO.IntegrationLoader.MBREMSSync
{
    public class MemberSchoolSubscriber : IDataQueueSubscriber
    {
         static ILog Log = LogManager.GetLogger(typeof(MemberSchoolSubscriber));

    /*
    - Board and school fields will be set using member school fields [YES]
- Teacher type will be set using member school "member type" field  [YES]
- Local can be "implied" - will need to look up a local in the member records that is linked to the same school board and has member's teacher type in the local name  [YES. EXACTLY.  UNFORTUNATELY, YOU WILL HAVE TO PARSE THE LOCAL NAME IF WE DON’T ADD THE MEMBER TYPE FIELD NOW WHICH IS FINE IF YOU ARE GOOD WITH THE PARSING]
*/


        public override string EntityName
        {
            get
            {
                return "reg_MemberSchool";
            }
        }

        

        //public static void ProcessMemberSchool(DataQueueItem item, Worker worker, Entity memberSchool)

        public Entity ProcessRecord(Worker worker, Entity source, Entity target, EntityContainer account, EntityContainer board, EntityContainer school, List<Entity> emsLocalList, List<Entity> mrsLocalList, List<Entity> teacherTypes)
        {

            Log.DebugFormat("Processing {0}#{1} ", source.LogicalName, source.Id);


             Entity emsLocal = null;
             Entity emsTeacherType = null;
             int stage = 1;
            
            //try
            //{

                if(source.Contains("reg_membertypeName") && source["reg_membertypeName"] != null)
                {
                    stage = 2;
                    emsTeacherType =  teacherTypes.Find(x => (string)x["oems_teachertype"]  == (string)source["reg_membertypeName"]);
                    string strTeacherType = (string)source["reg_membertypeName"];
                    string localKeyword = Config.GetLocalKeyword(strTeacherType);
                    string[] localKeywordWrongMatch = Config.GetLocalKeywordWrongMatch(strTeacherType).ToLower().Split('/');
                    if(source.Contains("reg_boardid") && source["reg_boardid"] != null)
                    {
                        stage = 3;
                        var locals = from x in mrsLocalList
                                     where ((EntityReference)x["parentaccountid"]).Id == ((EntityReference)source["reg_boardid"]).Id
                                     select x;
                    
                        /*from acc in worker.MRSEnvironment.OrgContext.CreateQuery("account")
                                     where (Guid)acc["parentaccountid"] == boardId && (int)acc["customertypecode"] == 4
                                     select acc;*/
                        foreach(var l in locals)
                        {
                            stage = 4;
                            string localName = ((string)l["name"]).ToLower();
                            if (localName.Contains(localKeyword.ToLower()))
                            {

                                bool wrongMatch = localKeywordWrongMatch.Any(s => localName.Contains(s));

                                if(!wrongMatch)
                                  {
                                      //AccountSubscriber.ProcessAccount(item, worker, l);
                                      stage = 5;
                                      emsLocal = emsLocalList.Find(x => ((string)x["oems_localname"]).ToLower() == localName);
                                      break;
                                  }
                            }
                        }
                    }
                   // if (emsLocal == null)
                      //  return null;
                }
                stage = 6;
                string name = "";

                if (board != null) name = (string)board.Entity["oems_schoolboardname"];
                stage = 7;
                if (school != null) name += " / " + (string)school.Entity["oems_schoolname"];
                //else name += " / ";
                stage = 8;
                if (emsLocal != null) name += " / " + (string)emsLocal["oems_localname"];
                target["oems_name"] = name;
                stage = 9;

             
                target["oems_teachertype"] = emsTeacherType != null ? emsTeacherType.ToEntityReference() : null;
                target["oems_schoolboard"] = board != null ? board.Entity.ToEntityReference() : null;
                target["oems_school"] = school != null ? school.Entity.ToEntityReference() : null;
                target["oems_local"] = emsLocal != null ? emsLocal.ToEntityReference() : null;
                target["oems_account"] = account != null ? account.Entity.ToEntityReference() : null;
                if (source.Contains("reg_active")) target["oems_defaultflag"] = source["reg_active"];

                target["oems_skipupdatequeue"] = true;
            //}
            //catch (Exception ex)
            //{
            //    throw;
            //}
            return target;
            //worker.EMSEnvironment.OrgService.Update(emsEntity);
        }


        public Entity ProcessRecord(Worker worker, Entity source, OrganizationRequest or, EntityContainer account, EntityContainer board, EntityContainer school, List<Entity> emsLocalList, List<Entity> teacherTypes) 
        {
            
            //The purpose of this function is to synchronize the member school

            Log.Info("Processing MemberSchool");
            bool error = false;
            Entity emsLocal = null;
            Entity emsTeacherType = null;
            Entity target = null;

            if (or.GetType() == typeof(CreateRequest))
            {
                target = ((CreateRequest)or).Target;
            }
            if (or.GetType() == typeof(UpdateRequest)) 
            {
                target = ((UpdateRequest)or).Target;
            }
            //Chech the status of the target
            
             

            //Set Initial Values
            target["oems_schoolboard"] = board != null ? board.Entity.ToEntityReference() : null;
            target["oems_school"] = school != null ? school.Entity.ToEntityReference() : null;
            target["oems_account"] = account != null ? account.Entity.ToEntityReference() : null;

            this.PostSubscriber.Entities.Add(account.Entity);

            if (source.Contains("reg_membertypeName"))
            {

                emsTeacherType = teacherTypes.Find(x => (string)x["oems_teachertype"] == (string)source["reg_membertypeName"]);

                //Get the local
                if (emsTeacherType != null)
                {
                    target["oems_teachertype"] = emsTeacherType.ToEntityReference();
                    string localtofind = board.Entity.Attributes["oems_schoolboardname"].ToString();


                    //This is the exception that breaks the rule
                    if (localtofind == "Toronto DSB")
                    {
                        string originalName = null;
                        try
                        {

                            localtofind = "Elementary Teachers of Toronto Local";
                            originalName = localtofind;

                            localtofind = localtofind.Replace(" ", "").Replace("(", "").Replace(")", "").Replace("-", "").Trim().ToUpper();
                            emsLocal = emsLocalList.Find(x => x["oems_localname"].ToString().Replace(" ", "").Trim().ToUpper() == localtofind);

                            target["oems_local"] = emsLocal.ToEntityReference();
                        }
                        catch (Exception exLocal)
                        {
                            Log.Error("The local with name: " + originalName + " does not exists.", exLocal);
                            throw (exLocal);
                        }

                    }
                    else
                    {
                        string originalName = localtofind;
                        localtofind = localtofind.Replace("DSB", "").Trim().ToUpper();
                        string techeartypename = emsTeacherType.Attributes["oems_teachertype"].ToString();
                        if (Config.GetKeypairValueTeacherTypes.ContainsKey(techeartypename))
                        {
                            try
                            {
                                string originalLocal = localtofind;
                                string localToLog = string.Concat(localtofind, " ", Config.GetKeypairValueTeacherTypes[techeartypename]);
                                localtofind = string.Concat(localtofind, Config.GetKeypairValueTeacherTypes[techeartypename]).Replace(" ", "").Replace("-","").Trim().ToUpper();
                                emsLocal = emsLocalList.Find(x => x["oems_localname"].ToString().Replace(" ", "").Replace("-","").Trim().ToUpper() == localtofind);
                                if (emsLocal == null)
                                {
                                    string searchLocal = string.Concat(originalLocal," ", Config.GetKeypairValueTeacherTypes[techeartypename]);
                                    string expLocal = GetException(searchLocal);
                                    if (expLocal != null)
                                    {
                                        emsLocal = emsLocalList.Find(x => x["oems_localname"].ToString().Replace(" ", "").ToUpper() == expLocal.Replace(" ", "").ToUpper());

                                        if (emsLocal != null)
                                            target["oems_local"] = emsLocal.ToEntityReference();
                                        else
                                            Log.Error("The local with name: " + expLocal + " does not exists and it should exist, contact the IT administrator.");
                                    }
                                    else
                                    {
                                        Log.Error("The local with name: " + localToLog + " does not exists.");
                                        error = true;
                                    }

                                }
                                else
                                {
                                    target["oems_local"] = emsLocal.ToEntityReference();
                                }
                            }
                            catch (Exception exLocal)
                            {
                                Log.Error("The local with name: " + originalName + " does not exists.", exLocal);
                                throw (exLocal);
                            }
                        }
                    }


                }
            }
            else
                error = true;

            //Set the name of the Account SchoolBoardLocal
            if (!error)
            {
                string name = "";

                if (board != null) name = (string)board.Entity["oems_schoolboardname"];

                if (school != null) name += " / " + (string)school.Entity["oems_schoolname"];

                if (emsLocal != null) name += " / " + (string)emsLocal["oems_localname"];

                target["oems_name"] = name;

                target["oems_activeflag"] = (bool)source.Attributes["reg_active"];

                if (!(bool)source.Attributes["reg_active"])
                {
                    target["oems_defaultflag"] = false;
                }

                return target;
            }
            return null;
        }

        private string GetException(string originalLocal)
        {
            Dictionary<string, string> keyValues = Config.LoadKeyPairTValues("LocalExceptions");

            if (keyValues.ContainsKey(originalLocal.ToUpper())) 
            {
                return keyValues[originalLocal.ToUpper()];
            }
            return null;
        }


        public override void ProcessList(Worker worker, List<Entity> sourceEntityList)
        {
            List<string> IdList = new List<string>();
            List<string> boardIdList = new List<string>();
            List<string> schoolIdList = new List<string>();
            List<string> memberIdList = new List<string>();

            foreach (var e in sourceEntityList)
            {
                IdList.Add(e.Id.ToString());
                if (e.Contains("reg_memberidetfoid"))
                {
                    memberIdList.Add(e["reg_memberidetfoid"].ToString());
                }
                if (e.Contains("reg_boardid"))
                {
                    var board =(EntityReference)e["reg_boardid"];
                    boardIdList.Add(board.Id.ToString());
                }
                if (e.Contains("reg_schoolid"))
                {
                    var school = (EntityReference)e["reg_schoolid"];
                    schoolIdList.Add(school.Id.ToString());
                }

            }
            var schoolBoardLocalList = worker.EMSEnvironment.RetrieveCreateRecordList("oems_accountschoolboardlocal", "oems_etfoid", IdList);

            List<FieldDefinition> fieldList = new List<FieldDefinition>();
            fieldList.Add(new FieldDefinition("oems_schoolname", CRMFieldType.STRING_FIELD));
            fieldList.Add(new FieldDefinition("oems_schoolboard", CRMFieldType.LOOKUP_FIELD));
            var schoolList = worker.EMSEnvironment.RetrieveCreateRecordList("oems_school", "oems_etfoid", schoolIdList, false, fieldList);
            fieldList.Clear();
            
            fieldList.Add(new FieldDefinition("oems_schoolboardname", CRMFieldType.STRING_FIELD));
            var boardList = worker.EMSEnvironment.RetrieveCreateRecordList("oems_schoolboard", "oems_etfoid", boardIdList, false, fieldList);
            fieldList.Clear();
            
            fieldList.Add(new FieldDefinition("name", CRMFieldType.STRING_FIELD));
            var memberList = worker.EMSEnvironment.RetrieveCreateRecordList("account", "oems_etfomemberid", memberIdList, false, fieldList);

            var emsLocalList = worker.EMSEnvironment.RetrieveMultipleFromSql("select * from oems_local", "oems_local", SQLSupport.EMSLocalFieldList);
            //var mrsLocalList = worker.MRSEnvironment.RetrieveMultipleFromSql("select * from account where customertypecode=4", "account", SQLSupport.MRSAccountFieldList);
            var teacherTypes = worker.EMSEnvironment.RetrieveMultipleFromSql("select * from oems_teachertype", "oems_teachertype", SQLSupport.EMSTeacherTypeFieldList);


            foreach (var e in sourceEntityList)
            {
                EntityContainer ec = schoolBoardLocalList.Find(x => x.LookupField == e.Id.ToString());
                if (ec != null) 
                {
                    OrganizationRequest or = null;
                    var target = worker.EMSEnvironment.RetrieveCreateRecord(ec,out or,true,true);
                    EntityContainer school = null;
                    EntityContainer board = null;
                    EntityContainer account = null;
                    if (e.Contains("reg_memberidetfoid"))
                    {
                        account = memberList.Find(x => x.LookupField == e["reg_memberidetfoid"].ToString());
                        if (account == null || account.Create)
                        {
                           account = null;
                        }
                    }

                    if (e.Contains("reg_boardid"))
                    {
                        board = boardList.Find(x => Guid.Parse(x.LookupField) == ((EntityReference)e["reg_boardid"]).Id);

                        if (board == null) 
                        {
                            if (e.Contains("reg_boardidname"))
                            {
                                Log.Error("Unable to find Board: " + e["reg_boardidname"].ToString());
                            }
                            else 
                            {
                                Log.Error("Unable to find Board: " + e.Id.ToString());
                            }
                        }
                    }
                    
                    if(e.Contains("reg_schoolid"))
                    {
                        school = schoolList.Find(x => Guid.Parse(x.LookupField) == ((EntityReference)e["reg_schoolid"]).Id && ((EntityReference)x.Entity["oems_schoolboard"]).Id == board.Entity.Id);

                        if (school == null) 
                        {
                            if (e.Contains("reg_schoolidname"))
                            {
                                Log.Error("Unable to find School: " + e["reg_schoolidname"].ToString());
                            }
                            else 
                            {
                                Log.Error("Unable to find School: " + e.Id.ToString());
                            }
                        }
                    }

                    if (account != null && board != null && school != null && ProcessRecord(worker, e, or, account, board, school, emsLocalList, teacherTypes)!=null)
                    {
                        //ProcessRecord(worker, e, or, account, board, school, emsLocalList, teacherTypes);
                        Log.Debug("Processed record: ");
                    }
                    else 
                    {
                        worker.EMSEnvironment.CurrentBatch.Requests.Remove(or);
                        bool accountTrue = account!=null?true:false;
                        bool boardTrue = board !=null?true:false;
                        bool schoolTrue = school !=null?true:false;
                        string reason = string.Concat("Removing request from batch Reason = Account: ",accountTrue.ToString(),
                            ",Board: "+ boardTrue + ", School: " + schoolTrue);
                        Log.Info(reason);

                    }
                }
            }
            
        }

        private List<string> queries = null;
        public override List<string> GetQueries()
        {
            List<string> result = new List<string>();

            if (queries == null)
            {
                queries = new List<string>();
//                queries.Add(
//                     @"SELECT A.*, B.Reg_ETFOID AS reg_memberidetfoid
//                      FROM reg_MemberSchool A WITH (NOLOCK), Contact B WITH (NOLOCK)
//					  WHERE A.reg_memberid = B.ContactId AND A.modifiedOn > @modifiedon");

                queries.Add(
                        @"IF OBJECT_ID('tempdb..#StringMap1') IS NOT NULL
                        /*Then it exists*/
                       DROP TABLE #StringMap1
                        SELECT t.ObjectTypeCode, e.Name, t.AttributeName, t.AttributeValue, t.Value
                        INTO #StringMap1
                        FROM [ETFO-CRMSQL\ETFOCRMSQL].ETFO_MSCRM.dbo.StringMap t INNER JOIN [ETFO-CRMSQL\ETFOCRMSQL].ETFO_MSCRM.MetadataSchema.Entity e ON
                            t.ObjectTypeCode = e.ObjectTypeCode
                        ORDER BY t.ObjectTypeCode, t.AttributeName, t.AttributeValue;

                      SELECT A.*, B.Reg_ETFOID AS reg_memberidetfoid, #StringMap1.Value as reg_membertypeName,A.reg_active
                      FROM [ETFO-CRMSQL\ETFOCRMSQL].ETFO_MSCRM.dbo.reg_MemberSchool A WITH (NOLOCK) 
                      left outer join #StringMap1  on 
                        (#StringMap1.AttributeName = 'reg_membertype'
                        and #StringMap1.ObjectTypeCode = 10011
                        and #StringMap1.AttributeValue = A.Reg_MemberType)
                       
                       INNER JOIN [ETFO-CRMSQL\ETFOCRMSQL].ETFO_MSCRM.dbo.Contact B WITH (NOLOCK)
                       ON  A.reg_memberid = B.ContactId
                       left  join oems_AccountSchoolBoardLocal oasbl
                       on A.reg_memberschoolid = oasbl.oems_etfoid
                       where oasbl.oems_AccountSchoolBoardLocalId is null");

            }

            return queries;

        }
        public override List<FieldDefinition> FieldList
        {
            get
            {
                return SQLSupport.MRSMemberSchoolFieldList;
            }
        }

        public override CRMEnvironmentType SubscriberType
        {
            get
            {
                return CRMEnvironmentType.MRS;
            }
        }

        public override ActionType SubscriberActionType
        {
            get
            {
                return ActionType.Sync;
            }
        }

    }
}


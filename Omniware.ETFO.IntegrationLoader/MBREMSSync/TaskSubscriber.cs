﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Omniware.ETFO.IntegrationLoader;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Omniware.ETFO.IntegrationLoader.MBREMSSync
{
    public class TaskSubscriber : IDataQueueSubscriber
    {
        public override List<string> GetQueries()
        {
            List<string> result = new List<string>();

            result.Add(@"Select MR.reg_etfoid,EMS.oems_MemberRecordsUpdateRequestId,MR.TaskId
                        FROM
                        (select MRContact.reg_etfoid,MRTask.statecode,MRTask.ActivityId as 'TaskId'   from 
                        [ETFO-CRMSQL\ETFOCRMSQL].ETFO_MSCRM.dbo.Task MRTask
                        inner join 
                        [ETFO-CRMSQL\ETFOCRMSQL].ETFO_MSCRM.dbo.Contact MRContact
                        on
                        MRTask.RegardingObjectId = MRContact.ContactId
                        where MRTask.statecode = 1) as MR
                        inner join
                        (select acc.oems_ETFOMemberId,MRUR.oems_MemberRecordsUpdateRequestId from oems_MemberRecordsUpdateRequest MRUR
                        inner join Account acc 
                        on MRUR.oems_Account = acc.AccountId) as EMS
                        on MR.reg_etfoid = EMS.oems_ETFOMemberId");

            return result;
        }

        public override List<FieldDefinition> FieldList
        {
            get
            {
                return SQLSupport.EMSTaskPurgeFieldList;
            }
        }

        public override CRMEnvironmentType SubscriberType
        {
            get { return CRMEnvironmentType.MRS; }
        }

        public override ActionType SubscriberActionType
        {
            get { return ActionType.Sync; }
        }

        public override string EntityName
        {
            get { return "Task"; }
        }

        public override void ProcessList(Worker worker, List<Microsoft.Xrm.Sdk.Entity> sourceEntityList)
        {
            foreach(Entity entityProxy in sourceEntityList)
            {
                EntityReference emr = (EntityReference)entityProxy["oems_MemberRecordsUpdateRequestId"];
                Entity entityToRemove = worker.EMSEnvironment.OrgService.Retrieve("oems_memberrecordsupdaterequest", emr.Id, new Microsoft.Xrm.Sdk.Query.ColumnSet(true));

                DeleteRequest dr = new DeleteRequest();
                dr.Target = entityToRemove.ToEntityReference();

                if (worker.EMSEnvironment.CurrentBatch == null)
                    worker.EMSEnvironment.CurrentBatch = new ExecuteMultipleRequest();

                worker.EMSEnvironment.CurrentBatch.Requests.Add(dr);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

using Microsoft.Xrm.Sdk;

using Microsoft.Xrm.Sdk.Linq;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Client.Services;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Messages;
using log4net;

namespace Omniware.ETFO.IntegrationLoader.MRSQueueSubscribers
{

    public enum OrgType
    {
       Board = 1,
       SchoolAuthority = 2,
       School = 3,
       Local = 4,
       Other = 5
    }

    public class AccountSubscriber : IDataQueueSubscriber
    {
        static ILog Log = LogManager.GetLogger(typeof(AccountSubscriber));

        public override string EntityName 
        {
          get
          {
             return "account";
          }
        }



        public static Entity ProcessRecord(Worker worker, Entity source, Entity target, EntityContainer board)
        {
            Log.DebugFormat("Processing {0}#{1} ", source.LogicalName, source.Id);

            
            if (source.Contains("customertypecode") && source["customertypecode"] != null)
            {
                switch (((OptionSetValue)source["customertypecode"]).Value)
                {
                    case (int)OrgType.Board:
                        target["oems_schoolboardname"] = source["name"];
                        break;
                    case (int)OrgType.School:
                        target["oems_schoolname"] = source["name"];
                        target["oems_schoolboard"] = board != null ? board.Entity.ToEntityReference() : null;
                        target["oems_phone_main"] = source.Contains("telephone1") ? source["telephone1"] : null;
                        target["oems_phone_fax"] = source.Contains("fax") ? source["fax"] : null;
                        target["oems_address_city"] = source.Contains("address1_city") ? source["address1_city"] : null;
                        target["oems_email"] = source.Contains("emailaddress1") ? source["emailaddress1"] : null;
                        
                        break;
                    case (int)OrgType.Local:
                        //emsEntity = worker.EMSEnvironment.RetrieveCreateRecord("oems_local", "oems_etfoid", account.Id.ToString());
                        target["oems_localname"] = source["name"];
                        target["oems_schoolboard"] = board != null ? board.Entity.ToEntityReference() : null;

                        string[] teacherTypes = Config.TeacherTypes;
                        string[] localKeywords = Config.LocalKeywords;


                        Entity emsTeacherType = null;

                        for (int i = 0; i < localKeywords.Length; i++)
                        {
                            string localName = (string)source["name"];
                            if (localName.ToLower().Contains(localKeywords[i].ToLower()))
                            {
                                string[] localKeywordWrongMatch = Config.GetLocalKeywordWrongMatch(teacherTypes[i]).ToLower().Split('/');

                                bool wrongMatch = localKeywordWrongMatch.Any(s => localName.ToLower().Contains(s));

                                if (!wrongMatch)
                                {
                                    emsTeacherType = worker.EMSEnvironment.RetrieveCreateRecord("oems_teachertype", "oems_teachertype", teacherTypes[i]);
                                    break;
                                }
                            }
                        }
                        target["oems_teachertype"] = emsTeacherType.ToEntityReference();

                        break;
                    default: break;
                }
            }
            target["oems_skipupdatequeue"] = true;
            return target;
        }

        public override void ProcessList(Worker worker, List<Entity> sourceEntityList)
        {
            List<string> IdList = new List<string>();
            IdList = sourceEntityList.Where(x => ((OptionSetValue)x["customertypecode"]).Value == (int)OrgType.Board).Select(a => a.Id.ToString()).ToList();

            //foreach(var e in sourceEntityList)
            //{
            //    if(((OptionSetValue)e["customertypecode"]).Value == (int)OrgType.Board)
            //    {
            //        IdList.Add(e.Id.ToString());
            //    }
            //}
            var schoolBoardList = worker.EMSEnvironment.RetrieveCreateRecordList("oems_schoolboard", "oems_etfoid", IdList);

            IdList = new List<string>();
            IdList = sourceEntityList.Where(x => ((OptionSetValue)x["customertypecode"]).Value == (int)OrgType.School).Select(a => a.Id.ToString()).ToList();

            //foreach (var e in sourceEntityList)
            //{
            //    if (((OptionSetValue)e["customertypecode"]).Value == (int)OrgType.School)
            //    {
            //        IdList.Add(e.Id.ToString());
            //    }
            //}
            var schoolList = worker.EMSEnvironment.RetrieveCreateRecordList("oems_school", "oems_etfoid", IdList);

            IdList = new List<string>();
            IdList = sourceEntityList.Where(x => ((OptionSetValue)x["customertypecode"]).Value == (int)OrgType.Local).Select(a => a.Id.ToString()).ToList();
                     
            //foreach (var e in sourceEntityList)
            //{
            //    if (((OptionSetValue)e["customertypecode"]).Value == (int)OrgType.Local)
            //    {
            //        IdList.Add(e.Id.ToString());
            //    }
            //}
            var localList = worker.EMSEnvironment.RetrieveCreateRecordList("oems_local", "oems_etfoid", IdList);

            //List<EntityContainer> entityList = new List<EntityContainer>();
            //entityList.AddRange(schoolBoardList);
            //entityList.AddRange(schoolList);
            //entityList.AddRange(localList);

            var boards = from a in sourceEntityList
                         join b in schoolBoardList on a.Id.ToString() equals b.LookupField
                         select new { e = a, ec = b };

            foreach (var board in boards)
            {
                var target = worker.EMSEnvironment.RetrieveCreateRecord(board.ec);
                ProcessRecord(worker, board.e, target, null);
            }

            // get school board list
            List<String> boardIdList = sourceEntityList.Where(a => a.Contains("parentaccountid")).Select(b => ((EntityReference)b["parentaccountid"]).Id.ToString()).ToList();
            var boardList = worker.EMSEnvironment.RetrieveCreateRecordList("oems_schoolboard", "oems_etfoid", boardIdList, false);

            var schools = from a in sourceEntityList
                          join b in schoolList on a.Id.ToString() equals b.LookupField
                          select new { e = a, ec = b };

            foreach (var school in schools)
            {
                var target = worker.EMSEnvironment.RetrieveCreateRecord(school.ec);
                EntityContainer board = null;
                board = boardList.Find(x => x.LookupField == ((EntityReference)school.e["parentaccountid"]).Id.ToString()); 
                ProcessRecord(worker, school.e, target, board);
            }

            var locals = from a in sourceEntityList
                         join b in localList on a.Id.ToString() equals b.LookupField
                         select new { e = a, ec = b };

            foreach (var local in locals)
            {
                var target = worker.EMSEnvironment.RetrieveCreateRecord(local.ec);
                EntityContainer board = null;
                board = boardList.Find(x => x.LookupField == ((EntityReference)local.e["parentaccountid"]).Id.ToString()); 
                ProcessRecord(worker, local.e, target, board);
            }

            //foreach(var e in sourceEntityList.Where(x => ((OptionSetValue)x["customertypecode"]).Value == (int)OrgType.Board).ToList())
            //{
            //    EntityContainer ec = entityList.Find(x => x.LookupField == e.Id.ToString());
            //    if(ec != null) //other "customer type codes"
            //    {
            //        var target = worker.EMSEnvironment.RetrieveCreateRecord(ec);

            //        // get board if processing local or school
            //        ProcessRecord(worker, e, target);
            //    }
            //}
        }

        public override List<string> GetQueries()
        {
            List<string> queries = new List<string>();
               queries = new List<string>();
               queries.Add(
                    @"SELECT A.*
                      FROM Account A WITH (NOLOCK)
                      WHERE ParentAccountId IS NULL AND modifiedOn > @modifiedon ");
           
               queries.Add(
                    @"SELECT A.*
                      FROM Account A WITH (NOLOCK)
                      WHERE ParentAccountId IS NOT NULL AND modifiedOn > @modifiedon ");

            return queries;
        }

        public override List<FieldDefinition> FieldList 
        { 
          get
          {
             return SQLSupport.MRSAccountFieldList;
          }
        }

        public override CRMEnvironmentType SubscriberType
        {
            get
            {
                return CRMEnvironmentType.MRS;
            }
        }

        public override ActionType SubscriberActionType
        {
            get
            {
                return ActionType.Date;
            }
        }

    }
}

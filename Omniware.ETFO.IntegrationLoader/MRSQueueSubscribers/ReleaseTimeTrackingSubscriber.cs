﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xrm.Sdk;

using Microsoft.Xrm.Sdk.Linq;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Client.Services;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Messages;
using log4net;

namespace Omniware.ETFO.IntegrationLoader.MRSQueueSubscribers
{

    

    public class ReleaseTimeTrackingSubscriber : IDataQueueSubscriber
    {
        static ILog Log = LogManager.GetLogger(typeof(ReleaseTimeTrackingSubscriber));



        public override string EntityName
        {
            get
            {
                return "reg_releasetimetracking";
            }
        }

        public override void ProcessList(Worker worker, List<Entity> sourceEntityList)
        {
            try
            {
                List<string> accountsIdList = new List<string>();
                foreach (var e in sourceEntityList)
                {
                    Log.DebugFormat("Processing {0}#{1} ", e.LogicalName, e.Id);


                    if (e.Contains("reg_memberetfoid"))
                    {
                        accountsIdList.Add((string)e["reg_memberetfoid"]);
                    }
                }

                var etfoMemberList = worker.EMSEnvironment.RetrieveCreateRecordList("account", "oems_etfomemberid", accountsIdList, false);

                foreach (Entity source in sourceEntityList)
                {
                    if (source.Contains("reg_memberetfoid"))
                    {
                        var etfoMember = etfoMemberList.Find(x => x.LookupField == (string)source["reg_memberetfoid"]);

                        if (etfoMember != null)
                        {
                            DataQueueItem item = new DataQueueItem();
                            item.Entity = source;
                            item.EntityName = source.LogicalName;
                            item.Id = source.Id;
                            item.RecordId = source["reg_memberetfoid"].ToString();
                            item.Fields = "reg_currentstatus";
                            item.CRMEnvironment = worker.MRSEnvironment;
                            ProcessQueueItem(item, worker);
                            Log.Debug("Processed record: " + item.Entity.Id);
                        }
                    }
                }
            }
            catch (Exception ex) 
            {
                throw (ex);
            }
        }

        public bool ProcessQueueItem(DataQueueItem item, Worker worker)
        {
            bool result = true;

            if (item.CRMEnvironment.EnvironmentType == CRMEnvironmentType.MRS && item.EntityName == "reg_releasetimetracking" && item.Fields.Contains("reg_currentstatus"))
            {
                Guid recordId = item.Id;
                try
                { 
                   
                    Entity rtt = null;
                    
                    try
                    {
                       rtt = worker.MRSEnvironment.OrgService.Retrieve(item.EntityName, recordId, new Microsoft.Xrm.Sdk.Query.ColumnSet(true));
                    }
                    catch(Exception ex)
                    {
                        throw (ex);
                    }

                    if (rtt != null && rtt.Contains("reg_currentstatus"))
                    {
                         
                        Entity emsRequest = (from r in worker.EMSEnvironment.OrgContext.CreateQuery("oems_releasetimerequest")
                                            where (string)r["oems_etfoid"] == item.Id.ToString()
                                            select r).FirstOrDefault();
                        if(emsRequest != null && ((OptionSetValue)emsRequest["statecode"]).Value == 0)
                        {
                           int mrsStatus = ((OptionSetValue)rtt["reg_currentstatus"]).Value;
                           int emsStatus = ((OptionSetValue)emsRequest["statuscode"]).Value;

                           int newState = -1;
                           int newStatusReason = -1;

                           if(mrsStatus == EMSQueueSubscribers.ReleaseTimeRequestSubscriber.MRS_STATUS_CANCELLED &&
                              emsStatus != EMSQueueSubscribers.ReleaseTimeRequestSubscriber.STATUS_CANCELLED)
                           {
                               newState = 1;
                              newStatusReason = EMSQueueSubscribers.ReleaseTimeRequestSubscriber.STATUS_CANCELLED;
                           }
                           else if (mrsStatus == EMSQueueSubscribers.ReleaseTimeRequestSubscriber.MRS_STATUS_INPROGRESS &&
                              emsStatus != EMSQueueSubscribers.ReleaseTimeRequestSubscriber.STATUS_APPROVED)
                           {
                               newState = 0;
                               newStatusReason = EMSQueueSubscribers.ReleaseTimeRequestSubscriber.STATUS_APPROVED;
                           }
                           else if (mrsStatus == EMSQueueSubscribers.ReleaseTimeRequestSubscriber.MRS_STATUS_REJECTED &&
                              emsStatus != EMSQueueSubscribers.ReleaseTimeRequestSubscriber.STATUS_REJECTED)
                           {
                               newState = 1;
                               newStatusReason = EMSQueueSubscribers.ReleaseTimeRequestSubscriber.STATUS_REJECTED;
                           }

                           if (newState != -1)
                           {
                               SetStateRequest setState = new SetStateRequest();
                               setState.EntityMoniker = emsRequest.ToEntityReference();
                               setState.State = new OptionSetValue(0);
                               setState.Status = new OptionSetValue(newStatusReason);
                               worker.EMSEnvironment.CurrentBatch.Requests.Add(setState);
                           }
                        }

                    }
                }
                catch (Exception ex)
                {
                    item.ErrorMessage = ex.Message;
                    result = false;
                }
            }
            return result;
        }

        public override List<string> GetQueries()
        {
            List<string> result = new List<string>();

            result.Add(@"
                    SELECT  B.Reg_releasetimetrackingid,reg_currentstatus,reg_memberetfoid
                    FROM Reg_releasetimetrackingExtensionBase A WITH (NOLOCK)
                    inner join Reg_releasetimetrackingBase B WITH (NOLOCK)
                    on A.Reg_releasetimetrackingid = B.Reg_releasetimetrackingid
                    WHERE b.modifiedOn > @modifiedon");
            

            return result;
        }
        public override List<FieldDefinition> FieldList
        {
            get
            {
                return SQLSupport.MRSReleaseTimeTrackingFieldList;
            }
        }

        public override CRMEnvironmentType SubscriberType
        {
            get
            {
                return CRMEnvironmentType.MRS;
            }
        }

        public override ActionType SubscriberActionType
        {
            get
            {
                return ActionType.Date;
            }
        }

        public override bool ReleaseTimeRelated { get { return true; } }

    }
}

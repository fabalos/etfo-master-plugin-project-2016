﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xrm.Sdk;

using Microsoft.Xrm.Sdk.Linq;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Client.Services;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Messages;
using log4net;

namespace Omniware.ETFO.IntegrationLoader.MRSQueueSubscribers
{

    
    public class OrganizationContactSubscriber : IDataQueueSubscriber
    {

        static ILog Log = LogManager.GetLogger(typeof(OrganizationContactSubscriber));


        public override string EntityName
        {
            get
            {
                return "reg_organizationcontacts";
            }
        }




        public override void ProcessList(Worker worker, List<Entity> sourceEntityList)
        {
            //List<string> IdList = new List<string>();
            List<string> contactIdList = new List<string>();
            List<string> localIdList = new List<string>();
            

            foreach (var e in sourceEntityList)
            {
                Log.DebugFormat("Processing {0}#{1} ", e.LogicalName, e.Id);


               // IdList.Add(e.Id.ToString());
               
                if (e.Contains("contact_etfoid"))
                {
                    contactIdList.Add(e["contact_etfoid"].ToString());
                }

                if (e.Contains("presidentLocalId"))
                {
                    localIdList.Add(((EntityReference)e["presidentLocalId"]).Id.ToString());
                }
                
            }

            var accountList = worker.EMSEnvironment.RetrieveCreateRecordList("account", "oems_etfomemberid", contactIdList, false);
            var localList = worker.EMSEnvironment.RetrieveCreateRecordList("oems_local", "oems_etfoid", localIdList,false);

            var etfoProvincialOfficeName = System.Configuration.ConfigurationManager.AppSettings["etfoProvincialOfficeName"]; // "ETFO  -  Provincial Office";
            //var etfoProvincialOfficeAccount = accountList.Find(a => a.Entity.GetAttributeValue<string>("name") == etfoProvincialOfficeName);
            
            foreach (var e in sourceEntityList)
            {
                

                    EntityContainer account = null;

                    if (e.Contains("contact_etfoid"))
                    {
                        account = accountList.Find(x => x.LookupField == (e["contact_etfoid"].ToString()));
                        Log.Debug("Processed record: " + e.Id);
                    }
                    
                    if (account != null)
                    {


                        var target = worker.EMSEnvironment.RetrieveCreateRecord(account);

// ETFO-45
                        EntityReference erefAccount =   (EntityReference)e.Attributes["reg_organizationid"];
                        var etfoProvincialOfficeAccount = worker.MRSEnvironment.OrgService.Retrieve("account", erefAccount.Id, new Microsoft.Xrm.Sdk.Query.ColumnSet(new string[] { "name" }));
                        if ((etfoProvincialOfficeAccount.Attributes["name"].ToString() == etfoProvincialOfficeName) && ((Convert.ToInt32(e.GetAttributeValue<string>("reg_yeartermend")) == DateTime.Now.Year))) 
                        {
                            target["oems_provincialexecutiveflag"] = true;
                            target["oems_yeartermstart"] = Convert.ToInt32(e.GetAttributeValue<string>("reg_yeartermstart"));
                            target["oems_yeartermend"] = Convert.ToInt32(e.GetAttributeValue<string>("reg_yeartermend"));

                        }

                        if (e.Contains("presidentLocalId"))
                        {
                            EntityContainer local = localList.Find(x => x.LookupField == ((EntityReference)e["presidentLocalId"]).Id.ToString());

                            //target["oems_role"] = new OptionSetValue(347780000);
                            target["oems_local"] = local.Entity.ToEntityReference();
                        }
                        else
                        {
                            target["oems_role"] = null;
                            target["oems_local"] = null;
                        }

                    }
            }

        }



        private List<string> queries = null;
        public override List<string> GetQueries()
        {
            if (queries == null)
            {
                queries = new List<string>();
                queries.Add(
                     @"SELECT A.* , B.reg_etfoid as contact_etfoid, 
                         (SELECT TOP 1 reg_organizationid FROM reg_organizationcontacts C WITH (NOLOCK) 
			              WHERE C.reg_contactid = A.reg_contactid AND C.reg_role = 3 AND C.statecode = 0) AS presidentLocalId
                        FROM reg_organizationcontacts A WITH (NOLOCK), Contact B WITH (NOLOCK)
                        WHERE A.reg_contactid = B.ContactId AND A.modifiedOn > @modifiedon
                        ORDER BY A.reg_contactidName ");      

            }

            return queries;
        }

        public override List<FieldDefinition> FieldList
        {
            get
            {
                return SQLSupport.MRSOrganizationContactFieldList;
            }
        }

        public override CRMEnvironmentType SubscriberType
        {
            get
            {
                return CRMEnvironmentType.MRS;
            }
        }

        public override ActionType SubscriberActionType
        {
            get
            {
                return ActionType.Date;
            }
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xrm.Sdk;

using Microsoft.Xrm.Sdk.Linq;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Client.Services;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Messages;
using log4net;

namespace Omniware.ETFO.IntegrationLoader.MRSQueueSubscribers
{
    public class TaskSubscriber: IDataQueueSubscriber
    {
        static ILog Log = LogManager.GetLogger(typeof(TaskSubscriber));

        public override string EntityName
        {
            get
            {
                return "task";
            }
        }


        public static void ProcessRecord(Worker worker, Entity source, Entity updRequest)
        {
      
            if(updRequest != null)
            {
                if (updRequest.Contains("oems_phone_work")) //this field does not exist in MRS. Let's update EMS account
                {
                    Entity account = new Entity("account");
                    account.Id = ((EntityReference)updRequest["oems_account"]).Id;
                    account["telephone1"] = (string)updRequest["oems_phone_work"];
                    account["oems_skipupdatequeue"] = true;

                    UpdateRequest ur = new UpdateRequest();
                    ur.Target = account;
                    worker.EMSEnvironment.CurrentBatch.Requests.Add(ur);
                }
                DeleteRequest dr = new DeleteRequest();
                dr.Target = updRequest.ToEntityReference();
                worker.EMSEnvironment.CurrentBatch.Requests.Add(dr);
            }
        }

        public override void ProcessList(Worker worker, List<Entity> sourceEntityList)
        {
            //Entity emsEntity = worker.EMSEnvironment.RetrieveCreateRecord("account", "oems_etfomemberid", (string)contact["reg_etfoid"]);

            List<string> IdList = new List<string>();
            foreach (var e in sourceEntityList)
            {

                Log.DebugFormat("Processing {0}#{1} ", e.LogicalName, e.Id);

                IdList.Add(e.Id.ToString());
            }

            List<FieldDefinition> fieldList = new List<FieldDefinition>();
            fieldList.Add(new FieldDefinition("oems_phone_work", CRMFieldType.STRING_FIELD));
            fieldList.Add(new FieldDefinition("oems_account", CRMFieldType.LOOKUP_FIELD));
            

            var updRequestList = worker.EMSEnvironment.RetrieveCreateRecordList("oems_memberrecordsupdaterequest", "oems_trackingid", IdList, false, fieldList);

            foreach (var e in sourceEntityList)
            {
                EntityContainer ec = updRequestList.Find(x => x.LookupField == e.Id.ToString());
                if (ec != null)
                {
                    
                    ProcessRecord(worker, e, ec.Entity);
                }
                
            }
        }

        private List<string> queries = null;
        public override List<string> GetQueries()
        {
            if (queries == null)
            {
                queries = new List<string>();
                queries.Add(
                     @"SELECT 
                             activityid as taskid,
	                         statecode
                        FROM Task A WITH (NOLOCK)
                        WHERE statecode = 1 AND A.modifiedOn > @modifiedon 
					  ");


            }

            return queries;
        }

        public override List<FieldDefinition> FieldList
        {
            get
            {
                return SQLSupport.MRSTaskFieldList;
            }
        }

        public override CRMEnvironmentType SubscriberType
        {
            get
            {
                return CRMEnvironmentType.MRS;
            }
        }

        public override ActionType SubscriberActionType
        {
            get
            {
                return ActionType.Date;
            }
        }
    }
}

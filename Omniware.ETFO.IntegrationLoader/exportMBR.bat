@echo off
echo.
echo Update solution directory with the content of Local
echo.

setlocal enableextensions enabledelayedexpansion



set connection="ServiceUri=http://10.50.86.19/ETFO; Domain=ETFODOM; Username=omniaccess; Password=Omn3ware;  Timeout=24"
set solutionname="IntegrationLoaderSolution"

set solutionDir="..\MBRSolution" 
set file="..\tmp\IntegrationLoaderLocal.zip"
set tools="..\tools"
rem /logpath:%log% seems to cause the export to hang 
rem set log="..\tmp\export.log"

mkdir ..\tmp
echo on

del %file%
%tools%\Solution.Cmd.Helper.exe /operation:export /solutionname:%solutionname% /crmconnectionstring:%connection% /solutionfilepath:%file% || ( Echo EXPORT Error & EXIT /B 1)

rem Packager sometimes will not update webresources until they are deleted first 
mkdir %solutionDir%
rmdir %solutionDir% /s /q || ( Echo EXTRACT Error Files may be in use & EXIT /B 1)
%tools%\solutionPackager 	   /action:extract    /folder:%solutionDir%  /zipfile:%file% /packagetype:Unmanaged /allowWrite:yes /allowDelete:Yes /clobber || ( Echo PACKAGE Error & EXIT /B 1)

@echo off

endlocal
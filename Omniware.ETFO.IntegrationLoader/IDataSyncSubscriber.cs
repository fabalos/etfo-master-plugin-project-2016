﻿using Omniware.ETFO.IntegrationLoader;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Omniware.ETFO.IntegrationLoader
{
    interface IDataSyncSubscriber
    {
        void Sync(Worker worker);
    }
}

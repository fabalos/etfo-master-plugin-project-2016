﻿using log4net;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Omniware.ETFO.IntegrationLoader;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Omniware.ETFO.IntegrationLoader.MRSPostOperationSubscribers
{
    public class MemberSchoolPostSubscriber : IPostSubscriber
    {
        private static ILog Log = LogManager.GetLogger(typeof(MemberSchoolPostSubscriber));
        public MemberSchoolPostSubscriber() 
        {
            this.Entities = new List<Entity>();
        }
        public void Operate(Worker worker) 
        {
            foreach (Entity account in this.Entities)
            {
                object hasADefault = false;

                var listofasbl = from acc in worker.EMSEnvironment.OrgContext.CreateQuery("oems_accountschoolboardlocal")
                                 where acc["oems_account"] == account.ToEntityReference()
                                 orderby acc["oems_name"] ascending
                                 select acc ;

                foreach (var asbl in listofasbl)
                {
                    asbl.Attributes.TryGetValue("oems_defaultflag", out hasADefault);

                    if ((bool)hasADefault)
                        break;
                }

                if (!(bool)hasADefault)
                {
                    foreach (var target in listofasbl) 
                    {
                       if ((bool)target.Attributes["oems_activeflag"])
                       {
                           UpdateRequest ur = new UpdateRequest();
                           Entity toUpdate = new Entity(target.LogicalName);
                           toUpdate.Id = target.Id;

                           toUpdate.Attributes["oems_defaultflag"] = true;
                           ur.Target = toUpdate;
                           worker.EMSEnvironment.CurrentBatch.Requests.Add(ur);
                           break;
                       } 
                    }
                }
            }
        }

        public void ExecuteSubscriber(Worker worker) 
        {
            Operate(worker);

            try
            {
                Log.Info("Executing batches");
                worker.EMSEnvironment.ExecuteBatchWithLimit(worker.EMSEnvironment.CurrentBatch, "target");
            }
            catch (Exception ex) 
            {
                Log.Error("Error when executing PostSubscriber MemberSchoolPostSubscriber", ex);
            }

        }



        public List<Entity> Entities
        {
            get;
            set;
        }
    }
}

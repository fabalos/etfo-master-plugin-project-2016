﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Omniware.ETFO.Integration
{
    public abstract class RequestCache
    {
        private Dictionary<string, object> storeBag;

        protected RequestCache() 
        {
            this.storeBag = new Dictionary<string, object>();
        }


        public  void Insert(string key,object value) 
        {
            storeBag.Add(key, value);
        }

        public  object Get(string key) 
        {
            object result = null;

            storeBag.TryGetValue(key, out result);

            return result;
        }

        public  void Flush() 
        {
            storeBag.Clear();
        }
    }

    public class TaskCache: RequestCache
    {
        static TaskCache cache;

        private TaskCache()
        {}

        public static TaskCache GetInstance() 
        {
            if (cache == null) 
            {
                cache = new TaskCache();
            }
            return cache;
        }

        public void InsertTask(string type, string etfoId, int value) 
        {
            string temp = string.Concat(type, ":", etfoId);
            base.Insert(temp, value);
        }

        public int? GetTaskIndex(string type, string etfoId) 
        {
            string temp = string.Concat(type, ":", etfoId);

            return (int?)base.Get(temp);
        }

        public void Flush() 
        {
            base.Flush();
        }
    }

}

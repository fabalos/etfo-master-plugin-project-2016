﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Omniware.ETFO.IntegrationLoader
{
    public class SQLSupport
    {
        public static List<FieldDefinition> MRSContactFieldList = null;
        public static List<FieldDefinition> MRSAccountFieldList = null;
        public static List<FieldDefinition> MRSMemberSchoolFieldList = null;
        public static List<FieldDefinition> MRSOrganizationContactFieldList = null;
        public static List<FieldDefinition> MRSReleaseTimeTrackingFieldList = null;
        public static List<FieldDefinition> MRSTaskFieldList = null;

        

        public static List<FieldDefinition> EMSLocalFieldList = null;
        public static List<FieldDefinition> EMSTeacherTypeFieldList = null;
        public static List<FieldDefinition> EMSReleaseTimeRequestFieldList = null;
        public static List<FieldDefinition> EMSMemberRecordsUpdateRequestFieldList = null;
        public static List<FieldDefinition> EMSTaskPurgeFieldList = null;
        
        

        static SQLSupport()
        {

            



            MRSContactFieldList = new List<FieldDefinition>();
            MRSContactFieldList.Add(new FieldDefinition("contactid", CRMFieldType.LOOKUP_FIELD));
            MRSContactFieldList.Add(new FieldDefinition("fullname", CRMFieldType.STRING_FIELD));
            MRSContactFieldList.Add(new FieldDefinition("reg_onleave", CRMFieldType.BOOL_FIELD));
            MRSContactFieldList.Add(new FieldDefinition("reg_holdreasonName", CRMFieldType.STRING_FIELD));
            MRSContactFieldList.Add(new FieldDefinition("firstname", CRMFieldType.STRING_FIELD));
            MRSContactFieldList.Add(new FieldDefinition("lastname", CRMFieldType.STRING_FIELD));
            MRSContactFieldList.Add(new FieldDefinition("address1_line1", CRMFieldType.STRING_FIELD));
            MRSContactFieldList.Add(new FieldDefinition("address1_line2", CRMFieldType.STRING_FIELD));
            MRSContactFieldList.Add(new FieldDefinition("address1_city", CRMFieldType.STRING_FIELD));
            MRSContactFieldList.Add(new FieldDefinition("address1_country", CRMFieldType.STRING_FIELD));
            MRSContactFieldList.Add(new FieldDefinition("address1_stateorprovince", CRMFieldType.STRING_FIELD));
            MRSContactFieldList.Add(new FieldDefinition("address1_postalcode", CRMFieldType.STRING_FIELD));
            MRSContactFieldList.Add(new FieldDefinition("telephone2", CRMFieldType.STRING_FIELD));
            MRSContactFieldList.Add(new FieldDefinition("mobilephone", CRMFieldType.STRING_FIELD));
            MRSContactFieldList.Add(new FieldDefinition("emailaddress1", CRMFieldType.STRING_FIELD));
            MRSContactFieldList.Add(new FieldDefinition("reg_etfoid", CRMFieldType.STRING_FIELD));
            MRSContactFieldList.Add(new FieldDefinition("reg_aboriginal", CRMFieldType.BOOL_FIELD));
            MRSContactFieldList.Add(new FieldDefinition("reg_disabled", CRMFieldType.BOOL_FIELD));
            MRSContactFieldList.Add(new FieldDefinition("reg_gaylesbianbisexualtransgender", CRMFieldType.BOOL_FIELD));
            MRSContactFieldList.Add(new FieldDefinition("reg_racialminority", CRMFieldType.BOOL_FIELD));
            MRSContactFieldList.Add(new FieldDefinition("reg_active", CRMFieldType.BOOL_FIELD));
            MRSContactFieldList.Add(new FieldDefinition("reg_sin", CRMFieldType.STRING_FIELD));
            //MRSContactFieldList.Add(new FieldDefinition("reg_relatedselfidid", CRMFieldType.LOOKUP_FIELD));

            

            

            MRSAccountFieldList = new List<FieldDefinition>();
            MRSAccountFieldList.Add(new FieldDefinition("accountid", CRMFieldType.LOOKUP_FIELD));
            MRSAccountFieldList.Add(new FieldDefinition("parentaccountid", CRMFieldType.LOOKUP_FIELD));
            MRSAccountFieldList.Add(new FieldDefinition("customertypecode", CRMFieldType.OPTIONSET_FIELD));
            MRSAccountFieldList.Add(new FieldDefinition("name", CRMFieldType.STRING_FIELD));
            MRSAccountFieldList.Add(new FieldDefinition("parentaccountid", CRMFieldType.LOOKUP_FIELD));
            MRSAccountFieldList.Add(new FieldDefinition("telephone1", CRMFieldType.STRING_FIELD));
            MRSAccountFieldList.Add(new FieldDefinition("fax", CRMFieldType.STRING_FIELD));
            MRSAccountFieldList.Add(new FieldDefinition("address1_city", CRMFieldType.STRING_FIELD));

            MRSMemberSchoolFieldList = new List<FieldDefinition>();
            MRSMemberSchoolFieldList.Add(new FieldDefinition("reg_memberschoolid", CRMFieldType.LOOKUP_FIELD));
            MRSMemberSchoolFieldList.Add(new FieldDefinition("reg_memberid", CRMFieldType.LOOKUP_FIELD));
            MRSMemberSchoolFieldList.Add(new FieldDefinition("reg_boardid", CRMFieldType.LOOKUP_FIELD));
            MRSMemberSchoolFieldList.Add(new FieldDefinition("reg_schoolid", CRMFieldType.LOOKUP_FIELD));
            MRSMemberSchoolFieldList.Add(new FieldDefinition("reg_membertype", CRMFieldType.OPTIONSET_FIELD));
            MRSMemberSchoolFieldList.Add(new FieldDefinition("reg_membertypeName", CRMFieldType.STRING_FIELD));
            MRSMemberSchoolFieldList.Add(new FieldDefinition("reg_memberidetfoid", CRMFieldType.STRING_FIELD));
            MRSMemberSchoolFieldList.Add(new FieldDefinition("reg_active", CRMFieldType.BOOL_FIELD));
            MRSMemberSchoolFieldList.Add(new FieldDefinition("reg_schoolidname", CRMFieldType.STRING_FIELD));
            MRSMemberSchoolFieldList.Add(new FieldDefinition("reg_boardidname", CRMFieldType.STRING_FIELD));



            MRSOrganizationContactFieldList = new List<FieldDefinition>();
            MRSOrganizationContactFieldList.Add(new FieldDefinition("presidentLocalId", CRMFieldType.LOOKUP_FIELD));
            MRSOrganizationContactFieldList.Add(new FieldDefinition("contact_etfoid", CRMFieldType.STRING_FIELD));
            MRSOrganizationContactFieldList.Add(new FieldDefinition("reg_yeartermstart", CRMFieldType.STRING_FIELD));
            MRSOrganizationContactFieldList.Add(new FieldDefinition("reg_yeartermend", CRMFieldType.STRING_FIELD));
            MRSOrganizationContactFieldList.Add(new FieldDefinition("reg_organizationid", CRMFieldType.LOOKUP_FIELD));


            EMSLocalFieldList = new List<FieldDefinition>();
            EMSLocalFieldList.Add(new FieldDefinition("oems_localid", CRMFieldType.LOOKUP_FIELD));
            EMSLocalFieldList.Add(new FieldDefinition("oems_localname", CRMFieldType.STRING_FIELD));

            EMSTeacherTypeFieldList = new List<FieldDefinition>();
            EMSTeacherTypeFieldList.Add(new FieldDefinition("oems_teachertypeid", CRMFieldType.LOOKUP_FIELD));
            EMSTeacherTypeFieldList.Add(new FieldDefinition("oems_teachertype", CRMFieldType.STRING_FIELD));


            EMSReleaseTimeRequestFieldList = new List<FieldDefinition>();
            EMSReleaseTimeRequestFieldList.Add(new FieldDefinition("oems_pushtomemberrecords", CRMFieldType.BOOL_FIELD));
            EMSReleaseTimeRequestFieldList.Add(new FieldDefinition("statuscode", CRMFieldType.OPTIONSET_FIELD));
            EMSReleaseTimeRequestFieldList.Add(new FieldDefinition("oems_event", CRMFieldType.LOOKUP_FIELD));
            EMSReleaseTimeRequestFieldList.Add(new FieldDefinition("oems_schoolboard", CRMFieldType.LOOKUP_FIELD));
            EMSReleaseTimeRequestFieldList.Add(new FieldDefinition("oems_local", CRMFieldType.LOOKUP_FIELD));
            EMSReleaseTimeRequestFieldList.Add(new FieldDefinition("local_etfoid", CRMFieldType.STRING_FIELD));
            EMSReleaseTimeRequestFieldList.Add(new FieldDefinition("schoolboard_etfoid", CRMFieldType.STRING_FIELD));
            EMSReleaseTimeRequestFieldList.Add(new FieldDefinition("releasetimerequest_etfoid", CRMFieldType.STRING_FIELD));
            EMSReleaseTimeRequestFieldList.Add(new FieldDefinition("oems_requestid", CRMFieldType.STRING_FIELD));

            EMSReleaseTimeRequestFieldList.Add(new FieldDefinition("oems_budgetnumber", CRMFieldType.LOOKUP_FIELD));
            EMSReleaseTimeRequestFieldList.Add(new FieldDefinition("budgetCode_etfoid", CRMFieldType.STRING_FIELD));
            EMSReleaseTimeRequestFieldList.Add(new FieldDefinition("account_etfomemberid", CRMFieldType.STRING_FIELD));
            EMSReleaseTimeRequestFieldList.Add(new FieldDefinition("oems_eventname", CRMFieldType.STRING_FIELD));

            EMSReleaseTimeRequestFieldList.Add(new FieldDefinition("main_days", CRMFieldType.DECIMAL_FIELD));
            EMSReleaseTimeRequestFieldList.Add(new FieldDefinition("extra_days", CRMFieldType.DECIMAL_FIELD));
            EMSReleaseTimeRequestFieldList.Add(new FieldDefinition("date_of_release", CRMFieldType.DATETIME_FIELD));
            EMSReleaseTimeRequestFieldList.Add(new FieldDefinition("period", CRMFieldType.INTEGER_FIELD));

            

            MRSReleaseTimeTrackingFieldList = new List<FieldDefinition>();

            MRSReleaseTimeTrackingFieldList.Add(new FieldDefinition("reg_currentstatus", CRMFieldType.OPTIONSET_FIELD));
            MRSReleaseTimeTrackingFieldList.Add(new FieldDefinition("reg_memberetfoid", CRMFieldType.STRING_FIELD));

            MRSTaskFieldList = new List<FieldDefinition>();
            MRSTaskFieldList.Add(new FieldDefinition("statecode", CRMFieldType.OPTIONSET_FIELD));
            
            EMSMemberRecordsUpdateRequestFieldList = new List<FieldDefinition>();
            EMSMemberRecordsUpdateRequestFieldList.Add(new FieldDefinition("oems_requesttype", CRMFieldType.OPTIONSET_FIELD));
            EMSMemberRecordsUpdateRequestFieldList.Add(new FieldDefinition("oems_selfid_aboriginalflag", CRMFieldType.BOOL_FIELD));
            EMSMemberRecordsUpdateRequestFieldList.Add(new FieldDefinition("oems_selfid_disabledflag", CRMFieldType.BOOL_FIELD));
            EMSMemberRecordsUpdateRequestFieldList.Add(new FieldDefinition("oems_selfid_lesbiangaytransgenderflag", CRMFieldType.BOOL_FIELD));
            EMSMemberRecordsUpdateRequestFieldList.Add(new FieldDefinition("oems_selfid_racialminorityflag", CRMFieldType.BOOL_FIELD));
            EMSMemberRecordsUpdateRequestFieldList.Add(new FieldDefinition("oems_address_line1", CRMFieldType.STRING_FIELD));
            EMSMemberRecordsUpdateRequestFieldList.Add(new FieldDefinition("oems_address_line2", CRMFieldType.STRING_FIELD));
            EMSMemberRecordsUpdateRequestFieldList.Add(new FieldDefinition("oems_address_city", CRMFieldType.STRING_FIELD));
            EMSMemberRecordsUpdateRequestFieldList.Add(new FieldDefinition("oems_address_stateorprovince", CRMFieldType.STRING_FIELD));
            EMSMemberRecordsUpdateRequestFieldList.Add(new FieldDefinition("oems_address_country", CRMFieldType.STRING_FIELD));
            EMSMemberRecordsUpdateRequestFieldList.Add(new FieldDefinition("oems_address_postalcode", CRMFieldType.STRING_FIELD));
            EMSMemberRecordsUpdateRequestFieldList.Add(new FieldDefinition("oems_phone_cell", CRMFieldType.STRING_FIELD));
            EMSMemberRecordsUpdateRequestFieldList.Add(new FieldDefinition("oems_phone_home", CRMFieldType.STRING_FIELD));
            EMSMemberRecordsUpdateRequestFieldList.Add(new FieldDefinition("oems_phone_fax", CRMFieldType.STRING_FIELD));
            EMSMemberRecordsUpdateRequestFieldList.Add(new FieldDefinition("oems_etfomemberid", CRMFieldType.STRING_FIELD));
            EMSMemberRecordsUpdateRequestFieldList.Add(new FieldDefinition("oems_firstname", CRMFieldType.STRING_FIELD));
            EMSMemberRecordsUpdateRequestFieldList.Add(new FieldDefinition("oems_lastname", CRMFieldType.STRING_FIELD));
            EMSMemberRecordsUpdateRequestFieldList.Add(new FieldDefinition("oems_email", CRMFieldType.STRING_FIELD));


            EMSTaskPurgeFieldList = new List<FieldDefinition>();
            EMSTaskPurgeFieldList.Add(new FieldDefinition("reg_etfoid", CRMFieldType.STRING_FIELD));
            EMSTaskPurgeFieldList.Add(new FieldDefinition("oems_MemberRecordsUpdateRequestId", CRMFieldType.LOOKUP_FIELD));
            
            
        }



    }
}

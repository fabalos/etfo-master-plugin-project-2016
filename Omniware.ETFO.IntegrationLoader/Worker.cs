﻿using System;
using System.Collections.Generic;

using Microsoft.Xrm.Sdk;

using Microsoft.Crm.Sdk.Messages;
using System.Data.SqlClient;
using log4net;
using Omniware.ETFO.IntegrationLoader.MBREMSSync;

namespace Omniware.ETFO.IntegrationLoader
{

/* supperseded by commandline option
    public enum LOAD_DIRECTION
    {
        NONE = -1,
        TOEMS = 0,
        TOMRS = 1,
        BOTH  = 2
    }
*/

    public class Worker
    {

        private List<IDataQueueSubscriber> subscribers = new List<IDataQueueSubscriber>();

        private static ILog Log = LogManager.GetLogger(typeof (Worker));

        public CRMEnvironment EMSEnvironment = null;
        public SqlConnection EMSSqlConnection = null;
        public CRMEnvironment MRSEnvironment = null;
        public SqlConnection MRSSqlConnection = null;
        public Boolean stopOnError = false;
        private LoadProgress progress = null;

        private int numberOfTries;
        private int batchSize;


        readonly  List<IDataQueueSubscriber> allSubscribers = new List<IDataQueueSubscriber>() {

            // Non release time ---------------
            //toEMS
            new MRSQueueSubscribers.AccountSubscriber(),
            new MRSQueueSubscribers.ContactSubscriber(),
            new MRSQueueSubscribers.MemberSchoolSubscriber(){ PostSubscriber= new MRSPostOperationSubscribers.MemberSchoolPostSubscriber()},
            new MRSQueueSubscribers.OrganizationContactSubscriber(),
            new MRSQueueSubscribers.TaskSubscriber(),

            //fromEMS to MBR
            new EMSQueueSubscribers.MemberRecordsUpdateRequestSubscriber(),

            // Release time ---------------
            //to EMS
            new MRSQueueSubscribers.ReleaseTimeTrackingSubscriber(),

            // from EMS
            new EMSQueueSubscribers.ReleaseTimeRequestSubscriber(),

            // Sync
            new MBREMSSync.AccountSubscriber(),
            new MBREMSSync.ContactSubscriber(),
            new MBREMSSync.MemberSchoolSubscriber(){PostSubscriber = new MRSPostOperationSubscribers.MemberSchoolPostSubscriber()}


        };

        //readonly List<IDataSyncSubscriber> syncSubscribers = new List<IDataSyncSubscriber>()
        //{
        //    new PresidentsSyncSubscriber()
        //};

        readonly List<IDataSyncSubscriber> syncSubscribers = null;


        // Process all subscribers optionally filtering by direction and releaseTime use 
        // directionToEms: true == toEMS, false == fromEMS, null both
        // releaseTimeOnlySubset: true=onlyReleaseTime, false = nonReleaseTime, null both
        public Worker(bool? directionToEms = null,bool? sync = null, bool? releaseTimeSubset = null,bool presidentSubscriberOnly = false,bool tasksSync = false)
        {
            this.numberOfTries = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["numberOfTries"]);
            this.batchSize = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["batchSize"]);

            if (!presidentSubscriberOnly)
            {

                if (sync.HasValue && sync.Value == true)
                {
                    if (tasksSync)
                    {
                        if(subscribers==null)
                            subscribers= new List<IDataQueueSubscriber>();

                        subscribers.Add( new Omniware.ETFO.IntegrationLoader.MBREMSSync.TaskSubscriber());
                    }
                    else
                    {//Using the sync pattern
                        subscribers = allSubscribers.FindAll(sub => sub.SubscriberActionType == ActionType.Sync);
                    }
                }
                else
                {
                    //find all subscribers matching direction and releaseTime option
                    subscribers = allSubscribers.FindAll(sub => !directionToEms.HasValue || sub.DirectionToEms == directionToEms)
                                                .FindAll(sub => !releaseTimeSubset.HasValue || sub.ReleaseTimeRelated == releaseTimeSubset).FindAll(sub => sub.SubscriberActionType != ActionType.Sync);
                }



                foreach (var dataQueueSubscriber in subscribers) Log.DebugFormat(" Activated subscriber {0} ", dataQueueSubscriber);
            }
            else 
            {
                syncSubscribers = new List<IDataSyncSubscriber>()
                {
                    new PresidentsSyncSubscriber()
                };
            }
        }


        
        private void ConnectToCrm()
        {
            if(EMSEnvironment != null) return;

            Log.Info("Acquiring connections");

            string MRSSqlConnectionString = System.Configuration.ConfigurationManager.AppSettings["MRSSqlConnection"];
            string EMSSqlConnectionString = System.Configuration.ConfigurationManager.AppSettings["EMSSqlConnection"];


            

            string MRSConnectionString = System.Configuration.ConfigurationManager.AppSettings["MRSConnection"];
            string EMSConnectionString = System.Configuration.ConfigurationManager.AppSettings["EMSConnection"];

            
            EMSEnvironment = new CRMEnvironment(EMSConnectionString, CRMEnvironmentType.EMS,this.batchSize);
            MRSEnvironment = new CRMEnvironment(MRSConnectionString, CRMEnvironmentType.MRS,this.batchSize, EMSEnvironment.MBRreferenceUpdateRequestTeam);

            EMSEnvironment.SQLCon = new SqlConnection(EMSSqlConnectionString);
            EMSEnvironment.SQLCon.Open();

            MRSEnvironment.SQLCon = new SqlConnection(MRSSqlConnectionString);
            MRSEnvironment.SQLCon.Open();



            Log.Debug("Connections ready");

        }


/* unused
        public void CreateErrorTask(CRMEnvironment crmEnvironment, string message)
        {
           Entity task = new Entity("task");
           task["subject"] = "Please review data integration errors";
           task["description"] = "There was an error while processing data:" + System.Environment.NewLine + System.Environment.NewLine + message;
           task["scheduledend"] = DateTime.Now.AddHours(8);
           task.Id = crmEnvironment.OrgService.Create(task);
           AssignRequest assign = new AssignRequest
           {
               Assignee = crmEnvironment.ErrorNotificationUser,
               Target = task.ToEntityReference()
           };
           crmEnvironment.OrgService.Execute(assign);
        }
*/

        

        public void ProcessChanges()
        {
            progress = LoadProgress.GetLoadProgress();
            string startFrom = null;

            if (!string.IsNullOrEmpty(progress.SubscriberTypeName))
            {
                startFrom = progress.SubscriberTypeName;
            }

            DateTime startTime = DateTime.Now;
            DateTime modifiedOn = new DateTime(2001,1,1);

            Log.Info("Getting lastrun timestamp");


            if (System.IO.File.Exists("lastrun.txt"))
            {
                using (System.IO.StreamReader sr = new System.IO.StreamReader("lastrun.txt"))
               {
                  string lastRun = sr.ReadLine();
                  DateTime.TryParse(lastRun, out modifiedOn);
                  sr.Close();
                  Log.InfoFormat("Read lastrun.txt - parsed date {0} ", modifiedOn);
               }
            }
            else
            {
                Log.Warn("File lastrun.txt was not located, will (re)process ALL records.");
            }
            
            int debugCounter = 0;
            bool stopProcessing = false;

            ConnectToCrm();
            
            DateTime dtStart = DateTime.Now;
            TimeSpan delta;
            double seconds;


            Log.Info( "Entering subscribers loop");
            foreach (var s in subscribers)
            {
                CRMEnvironment targetEnvironment = null;
                CRMEnvironment sourceEnvironment = null;
                if(s.SubscriberType == CRMEnvironmentType.MRS)
                {
                   sourceEnvironment = MRSEnvironment;
                   targetEnvironment = EMSEnvironment;
                }
                else
                {
                    targetEnvironment = MRSEnvironment;
                    sourceEnvironment = EMSEnvironment;
                }


                if (startFrom != null && startFrom != s.GetType().ToString())
                {
                    Log.WarnFormat("Skipping processing of subscriber {0} based on progress file.", s.GetType());
                    continue;
                }

                progress.SubscriberTypeName = s.GetType().ToString();
                startFrom = null;


                Log.Info( "Processing subscriber for " + s.EntityName + " in " + Enum.GetName(typeof(CRMEnvironmentType), s.SubscriberType));

                debugCounter++;
                targetEnvironment.CurrentBatch = targetEnvironment.CreateBatchRequest();
                sourceEnvironment.CurrentBatch = sourceEnvironment.CreateBatchRequest();
                foreach (var q in s.GetQueries())
                {

                    Log.InfoFormat( "Processing query: {0} ", q);

                    SqlCommand cmd = null;
                    if (s.SubscriberActionType == ActionType.Sync)
                    {
                        Log.InfoFormat("ConnectionString: {0} ",targetEnvironment.SQLCon.ConnectionString);
                        cmd = new SqlCommand(q, targetEnvironment.SQLCon);

                    }
                    else 
                    {
                        cmd = new SqlCommand(q, sourceEnvironment.SQLCon);
                        cmd.Parameters.AddWithValue("@modifiedon", modifiedOn);
                    }
                    cmd.CommandTimeout = 0; 
                    


                    SqlDataReader reader = cmd.ExecuteReader();
                    List<Entity> sourceEntityList = new List<Entity>();
                    
                    while (!stopProcessing && reader.Read()) 
                    {
                        var e = sourceEnvironment.SetEntityFromSql(reader, s.EntityName, s.FieldList, false);
                        if (e != null && progress.processedEntityIdList.IndexOf(e.Id) == -1)
                        {
                            sourceEntityList.Add(e);
                        }
                        else
                        {
                            Log.DebugFormat("Skipping processing of enitity {0} #{1} based on progress file.", e.LogicalName, e.Id);
                        }
                    }
                    reader.Close();

                    Log.Info("Entering source processing loop"); 
                    bool success = false;

                    List<Entity> shortEntityList = new List<Entity>();

                    for(int i = 0; i < sourceEntityList.Count; i++)
                    {    
                        shortEntityList.Add(sourceEntityList[i]);

                        if((i+1) % this.batchSize == 0 || i == sourceEntityList.Count - 1)
                        {
                            int errorCount = 1;
                            success = false;
                            bool retrying = false;
                            while (!success)
                            {
                                try
                                {
                                    if(!retrying)  s.ProcessList(this, shortEntityList);

                                    Log.Info("Executing batches"); 
                                    targetEnvironment.ExecuteBatchWithLimit(targetEnvironment.CurrentBatch,"target");
                                    sourceEnvironment.ExecuteBatchWithLimit(sourceEnvironment.CurrentBatch,"source");

                                    foreach (var processedEntity in shortEntityList)
                                    {
                                        progress.processedEntityIdList.Add(processedEntity.Id);
                                    }
                                    shortEntityList.Clear();
                                    delta = DateTime.Now - dtStart;
                                    seconds = delta.TotalSeconds;
                                    success = true;
                                    retrying = false;
                                }
                                catch (Exception ex)//SQL Timeout etc - try again
                                {
                                    if (errorCount == this.numberOfTries)
                                    {
                                        Log.Fatal("Stopping the process",ex);
                                        throw;
                                    }

                                    if (ex.Data.Contains("RequestIndex")) 
                                    {
                                        //classify = (input > 0) ? "positive" : "negative";
                                        string direction = ex.Data.Contains("Direction") ? ex.Data["Direction"].ToString():null;

                                        if (direction!=null) 
                                        {
                                            var index = (int) ex.Data["RequestIndex"];
                                            if (direction == "target")
                                                targetEnvironment.CurrentBatch.Requests.RemoveAt(index);
                                            else
                                                sourceEnvironment.CurrentBatch.Requests.RemoveAt(index);

                                            var entity = shortEntityList[index];
                                            Log.WarnFormat("Removed request at index {0} entity {1} #{2} ", index, entity.LogicalName, entity.Id );
                                        }
                                    }

                                    Log.Error( ex.Message,ex);
                                    Log.Error( "Re-trying");
                                    retrying = true;

                                    //targetEnvironment.CurrentBatch.Requests.Clear();
                                    //sourceEnvironment.CurrentBatch.Requests.Clear();
                                    errorCount++;
                                }

                            }
                        }
                    }

                }

                if (s.PostSubscriber != null) 
                {
                    Log.Info("Processing Post Subscribers");
                    s.PostSubscriber.ExecuteSubscriber(this);

                }

                Log.Info( "End of processing for " + s.EntityName + " in " + Enum.GetName(typeof(CRMEnvironmentType), s.SubscriberType));
                progress.processedEntityIdList.Clear();
                progress.SubscriberTypeName = null;
               
            }

            //Start the syncSubscribers
            try
            {
                if (syncSubscribers != null)
                {
                    Log.Debug("Synchronizing Presidents");
                    foreach (IDataSyncSubscriber s in syncSubscribers)
                    {
                        s.Sync(this);
                        CRMEnvironment env = this.EMSEnvironment;
                        env.ExecuteBatchWithLimit(this.EMSEnvironment.CurrentBatch, "target");
                    }
                }
            }
            catch (Exception error) 
            {
                Log.Error("Error while Synchronizing presidents: " + error.Message);
            }

            Log.InfoFormat("Writing date '{0}' to lastrun.txt.", startTime);
            using (System.IO.StreamWriter sw = new System.IO.StreamWriter("lastrun.txt"))
            {
                sw.WriteLine(startTime);
            }
            
        }



        public void BudgetCodesOnly(ILog log)
        {
            ConnectToCrm();
            Omniware.ETFO.IntegrationLoader.OnDemand.BudgetCodeSubscriber bcs = new OnDemand.BudgetCodeSubscriber();
            bcs.LoadData(this,log );
        }

        public void Update(string updateVersion)
        {
            ConnectToCrm();
            Updater.ProcessUpdates(this, updateVersion);
        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xrm.Sdk;

using Microsoft.Xrm.Sdk.Linq;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Client.Services;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Messages;
using log4net;
using Omniware.ETFO.Integration;

namespace Omniware.ETFO.IntegrationLoader.EMSQueueSubscribers
{
    public class MemberRecordsUpdateRequestSubscriber : IDataQueueSubscriber
    {
        static ILog Log = LogManager.GetLogger(typeof(MemberRecordsUpdateRequestSubscriber));


        public override string EntityName
        {
            get
            {
                return "oems_memberrecordsupdaterequest";
            }
        }



        public static void ProcessRecord(Worker worker, Entity source, Entity mrsContact)
        {
            SetStateRequest taskUpdateReq = null;
                    

            Log.DebugFormat("Processing {0}#{1} ", source.LogicalName, source.Id);

            int requestTypeOption = -1;
            
            if(source.Contains("oems_requesttype"))
                requestTypeOption =  ((OptionSetValue)source["oems_requesttype"]).Value;

            string etfoId = (string)mrsContact["reg_etfoid"];
            if (requestTypeOption == 2) //self id update
            {
                Entity mrsSelfId = null;

                if (!mrsContact.Contains("reg_relatedselfidid"))
                {
                    mrsSelfId = new Entity("reg_selfid");
                    mrsSelfId["reg_memberid"] = mrsContact.ToEntityReference();
                    mrsSelfId["reg_etfoid"] = etfoId;
                    mrsSelfId.Id = Guid.NewGuid();
                    CreateRequest cr = new CreateRequest();
                    cr.Target = mrsSelfId;
                    worker.MRSEnvironment.CurrentBatch.Requests.Add(cr);

                    Entity updContact = new Entity(mrsContact.LogicalName);
                    updContact.Id = mrsContact.Id;
                    updContact["reg_relatedselfidid"] = mrsSelfId.ToEntityReference();
                    UpdateRequest ur = new UpdateRequest();
                    ur.Target = updContact;
                    worker.MRSEnvironment.CurrentBatch.Requests.Add(ur);

                }
                else
                {
                    mrsSelfId = new Entity("reg_selfid");
                    mrsSelfId.Id = ((EntityReference)mrsContact["reg_relatedselfidid"]).Id;
                    mrsSelfId["reg_etfoid"] = etfoId;
                    UpdateRequest ur = new UpdateRequest();
                    ur.Target = mrsSelfId;
                    worker.MRSEnvironment.CurrentBatch.Requests.Add(ur);
                }


                if (source.Contains("oems_selfid_aboriginalflag")) mrsSelfId["reg_aboriginal"] = source["oems_selfid_aboriginalflag"];
                else mrsSelfId["reg_aboriginal"] = null;

                if (source.Contains("oems_selfid_disabledflag")) mrsSelfId["reg_disabled"] = source["oems_selfid_disabledflag"];
                else mrsSelfId["reg_disabled"] = null;

                if (source.Contains("oems_selfid_lesbiangaytransgenderflag")) mrsSelfId["reg_gaylesbianbisexualtransgender"] = source["oems_selfid_lesbiangaytransgenderflag"];
                else mrsSelfId["reg_gaylesbianbisexualtransgender"] = null;

                if (source.Contains("oems_selfid_racialminorityflag")) mrsSelfId["reg_racialminority"] = source["oems_selfid_racialminorityflag"];
                else mrsSelfId["reg_racialminority"] = null;

                DeleteRequest dr = new DeleteRequest();
                dr.Target = source.ToEntityReference();
                worker.EMSEnvironment.CurrentBatch.Requests.Add(dr);

            }
            else if (requestTypeOption ==1 || requestTypeOption ==3) //address update
            {
                bool mustUpdate = true;

                List<string> requestType = new List<string>();

                String taskDescription = null;

                if (requestTypeOption == 1)
                {
                    taskDescription = "ETFO ID: " + etfoId + System.Environment.NewLine + System.Environment.NewLine;
                    if (source.Contains("oems_address_line1"))
                    {
                        taskDescription += "Street1: " + (string)source["oems_address_line1"] + System.Environment.NewLine;
                        requestType.Add("oems_address_line1");
                    }
                    if (source.Contains("oems_address_line2"))
                    {
                        taskDescription += "Street 2: " + (string)source["oems_address_line2"] + System.Environment.NewLine;
                        requestType.Add("oems_address_line2");
                    }
                    if (source.Contains("oems_address_city"))
                    {
                        taskDescription += "City: " + (string)source["oems_address_city"] + System.Environment.NewLine;
                        requestType.Add("oems_address_city");
                    }
                    if (source.Contains("oems_address_stateorprovince"))
                    {
                        taskDescription += "Province: " + (string)source["oems_address_stateorprovince"] + System.Environment.NewLine;
                        requestType.Add("oems_address_stateorprovince");
                    }
                    if (source.Contains("oems_address_country"))
                    {
                        taskDescription += "Country: " + (string)source["oems_address_country"] + System.Environment.NewLine;
                        requestType.Add("oems_address_country");
                    }
                    if (source.Contains("oems_address_postalcode"))
                    {
                        taskDescription += "Postal Code: " + (string)source["oems_address_postalcode"] + System.Environment.NewLine;
                        requestType.Add("oems_address_postalcode");
                    }
                    if (source.Contains("oems_phone_cell"))
                    {
                        taskDescription += "Cell#: " + (string)source["oems_phone_cell"] + System.Environment.NewLine;
                        requestType.Add("oems_phone_cell");
                    }
                    if (source.Contains("oems_phone_home"))
                    {
                        taskDescription += "Home#: " + (string)source["oems_phone_home"] + System.Environment.NewLine;
                        requestType.Add("oems_phone_home");
                    }
                    //taskDescription += "Work#: " + (string)account["oems_address_work"] + System.Environment.NewLine;

                    if (source.Contains("oems_phone_fax"))
                    {
                        taskDescription += "Fax#: " + (string)source["oems_phone_fax"] + System.Environment.NewLine;
                        requestType.Add("oems_phone_fax");
                    }
                }
                else if (requestTypeOption == 3) 
                {
                    taskDescription = "ETFO ID: " + etfoId + System.Environment.NewLine + System.Environment.NewLine;
                    if (source.Contains("oems_firstname"))
                    {
                        taskDescription += "First Name: " + (string)source["oems_firstname"] + System.Environment.NewLine;
                        requestType.Add("oems_firstname");
                    }
                    if (source.Contains("oems_lastname"))
                    {
                        taskDescription += "Last Name: " + (string)source["oems_lastname"] + System.Environment.NewLine;
                        requestType.Add("oems_lastname");
                    }
                    if (source.Contains("oems_email"))
                    {
                        taskDescription += "E-mail: " + (string)source["oems_email"] + System.Environment.NewLine;
                        requestType.Add("oems_email");
                    }
                }

                requestType = requestType.OrderBy(q => q).ToList();
                string type = string.Join(",", requestType.ToArray());
                int taskIndex = 0;

                Entity task = GetExistingTask(worker, etfoId, type, out taskIndex);

                //Checking if we can reuse the task
                if (task != null)
                {
                    
                    task = new Entity() { Id = task.Id, LogicalName = task.LogicalName };
                    
                    taskUpdateReq = new SetStateRequest();
                    taskUpdateReq.EntityMoniker = new EntityReference("task", task.Id);
                    taskUpdateReq.State = new OptionSetValue(0);
                    taskUpdateReq.Status = new OptionSetValue(2);

                    worker.MRSEnvironment.CurrentBatch.Requests.Add(taskUpdateReq);
                    
                }
                else
                {
                    task = new Entity("task");
                    mustUpdate = false;
                }



                if (mrsContact != null)
                {
                    task["regardingobjectid"] = mrsContact.ToEntityReference();
                }


                task["subject"] = "New Member Update Request From Event Management";
                task["description"] = taskDescription;
                task["scheduledend"] = DateTime.Now.AddHours(8);


                // task["ownerid"] = worker.MRSEnvironment.UpdateRequestUser;

                var cr = (OrganizationRequest)null;

                if (mustUpdate)
                {
                    if ((taskIndex > -1) && (task == null))
                    {
                        if (worker.MRSEnvironment.CurrentBatch.Requests.ElementAt(taskIndex).GetType() == typeof(CreateRequest))
                        {
                            ((CreateRequest)worker.MRSEnvironment.CurrentBatch.Requests.ElementAt(taskIndex)).Target = task;
                        }

                    }
                    else
                    {
                        if (taskIndex > -1)
                        {
                            ((UpdateRequest)worker.MRSEnvironment.CurrentBatch.Requests.ElementAt(taskIndex)).Target = task;
                        }
                        else
                        {
                            cr = new UpdateRequest();
                            ((UpdateRequest)cr).Target = task;
                        }
                    }

                }
                else
                {

                    task.Id = Guid.NewGuid();
                    cr = new CreateRequest();
                    ((CreateRequest)cr).Target = task;
                    task["oems_etfoid"] = etfoId;
                    task["oems_updaterequesttype"] = type;

                }

                if (cr != null)
                {
                    worker.MRSEnvironment.CurrentBatch.Requests.Add(cr);

                    var taskcache = TaskCache.GetInstance();
                    taskcache.InsertTask(type, etfoId, worker.MRSEnvironment.CurrentBatch.Requests.Count - 1);
                }

                AssignRequest ar = new AssignRequest
                {
                    Assignee = worker.MRSEnvironment.UpdateRequestTeam,
                    Target = task.ToEntityReference()
                };
                worker.MRSEnvironment.CurrentBatch.Requests.Add(ar);

                Entity updUpdateRequest = new Entity(source.LogicalName);
                updUpdateRequest.Id = source.Id;
                updUpdateRequest["oems_trackingid"] = task.Id.ToString();
                UpdateRequest ur = new UpdateRequest();
                ur.Target = updUpdateRequest;
                worker.EMSEnvironment.CurrentBatch.Requests.Add(ur);

                /*
 Entity requestTracker = new Entity("oems_requeststatustracker");
 requestTracker["oems_task"] = task.Id.ToString();//ToEntityReference();
 requestTracker["oems_requestid"] = source.Id.ToString();
 cr = new CreateRequest();
 cr.Target = requestTracker;
 worker.EMSEnvironment.CurrentBatch.Requests.Add(cr);
                     * */




            }

        }

        public override void ProcessList(Worker worker, List<Entity> sourceEntityList)
        {
            List<string> contactIDList = new List<string>();
            foreach (var e in sourceEntityList)
            {

                if (e.Contains("oems_etfomemberid"))
                {
                    contactIDList.Add((string)e["oems_etfomemberid"]);
                }
            }

            List<FieldDefinition> fieldList = new List<FieldDefinition>();
            fieldList.Add(new FieldDefinition("reg_relatedselfidid", CRMFieldType.LOOKUP_FIELD));
            fieldList.Add(new FieldDefinition("reg_etfoid", CRMFieldType.STRING_FIELD));

            var contactList = worker.MRSEnvironment.RetrieveCreateRecordList("contact", "reg_etfoid", contactIDList, false, fieldList);

            foreach (var e in sourceEntityList)
            {
                EntityContainer ec = contactList.Find(x => x.LookupField == e.GetAttributeValue<string>("oems_etfomemberid"));
                if (ec != null) //other "customer type codes"
                {
                    ProcessRecord(worker, e, ec.Entity);
                    Log.Debug("Processed record: " + ec.Entity.Id);
                }
            }
        }

        private static Entity GetExistingTask(Worker worker, string etfoId, string type, out int taskIndex)
        {

            //taskIndex = -1;
            var taskcache = TaskCache.GetInstance();
            Entity task = null;

            task = (from c in worker.MRSEnvironment.OrgContext.CreateQuery("task")
                    orderby c["createdon"] descending
                    where (string)c["oems_etfoid"] == etfoId
                    && (string)c["oems_updaterequesttype"] == type
                    && (int)c["statecode"] == 0
                    && (int)c["statuscode"] == 2
                    select c).FirstOrDefault();

            int? temp = taskcache.GetTaskIndex(type, etfoId);

            taskIndex = temp.HasValue ? temp.Value : -1;


            return task;
        }

        


        public override List<string> GetQueries()
        {

                List<string> queries = new List<string>();
                queries.Add(
                     @"SELECT 
                           A.oems_MemberRecordsUpdateRequestid,
                           A.oems_requesttype,
                           A.oems_selfid_aboriginalflag,
	                       A.oems_selfid_disabledflag,
	                       A.oems_selfid_lesbiangaytransgenderflag,
	                       A.oems_selfid_racialminorityflag,
	                       A.oems_address_line1,
	                       A.oems_address_line2,
	                       A.oems_address_city,
	                       A.oems_address_stateorprovince,
	                       A.oems_address_country,
	                       A.oems_address_postalcode,
	                       A.oems_phone_cell,
	                       A.oems_phone_home,
	                       A.oems_phone_fax,
                           A.oems_firstname,
                           A.oems_lastname,
                           A.oems_email,
                           B.oems_etfomemberid
                    FROM oems_memberrecordsupdaterequest A WITH (NOLOCK)
                         JOIN account B WITH (NOLOCK) ON B.AccountId = A.oems_account 
                    WHERE A.oems_trackingid is null AND A.createdon > @modifiedon
                    ORDER BY A.createdon ASC");//only new ones, so using createdon field

            return queries;

        }


        public override List<FieldDefinition> FieldList
        {
            get
            {
                return SQLSupport.EMSMemberRecordsUpdateRequestFieldList;
            }
        }

        public override CRMEnvironmentType SubscriberType
        {
            get
            {
                return CRMEnvironmentType.EMS;
            }
        }

        public override ActionType SubscriberActionType
        {
            get
            {
                return ActionType.Date;
            }
        }
    }
}

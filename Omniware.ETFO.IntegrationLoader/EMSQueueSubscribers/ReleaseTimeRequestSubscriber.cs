﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xrm.Sdk;

using Microsoft.Xrm.Sdk.Linq;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Client.Services;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Messages;
using log4net;

namespace Omniware.ETFO.IntegrationLoader.EMSQueueSubscribers
{
    public class ReleaseTimeRequestSubscriber : IDataQueueSubscriber
    {
        static ILog Log = LogManager.GetLogger(typeof(ReleaseTimeRequestSubscriber));


        public static int STATUS_SUBMITTED = 1;
        public static int STATUS_APPROVED = 347780000;
        public static int STATUS_ARPPROVAL_PENDING = 347780002;
        public static int STATUS_REJECTED = 347780005;
        public static int STATUS_CANCELLED = 347780004;
        public static int STATUS_WITHDRAWN = 347780006;


        public static int MRS_STATUS_INPROGRESS = 1;
        public static int MRS_STATUS_CLOSED = 2;
        public static int MRS_STATUS_CANCELLED = 3;
        public static int MRS_STATUS_REQUESTED = 100000000;
        public static int MRS_STATUS_REJECTED = 100000001;
        public static int MRS_STATUS_WRITTENOFF = 100000002;

        public const int EMS_PERIOD_FULLDAY = 1;
        public const int EMS_PERIOD_HALFDAY_AM = 2;
        public const int EMS_PERIOD_HALFDAY_PM = 3;

        public override string EntityName
        {
            get
            {
                return "oems_releasetimerequest";
            }
        }



        public bool ProcessRecord(Worker worker, Entity source, Entity target, Entity local, Entity schoolBoard, Entity etfoMember, out bool? update)
        {
            Log.DebugFormat("Processing {0}#{1} ", source.LogicalName, source.Id);

            update = null;

            target["reg_memberetfoid"] = source["account_etfomemberid"];
            target["reg_rttevent"] = source["oems_eventname"];
            target["reg_memberid"] = etfoMember.ToEntityReference();
                

            bool result = true;
            if (((OptionSetValue)source["statuscode"]).Value == STATUS_WITHDRAWN || ((OptionSetValue)source["statuscode"]).Value == STATUS_CANCELLED)
            {
                if (target.Contains("reg_currentstatus") && ((OptionSetValue)target["reg_currentstatus"]).Value != MRS_STATUS_CANCELLED)
                {
                    target["reg_currentstatus"] = new OptionSetValue(MRS_STATUS_CANCELLED);
                }

                update = AllowUpdate(worker, target, MRS_STATUS_CANCELLED);
            }
            else
            {
                OptionSetValue mrsBudgetCode = null;
                if (source.Contains("budgetCode_etfoid") && source["budgetCode_etfoid"] != null)
                {
                    mrsBudgetCode = new OptionSetValue(int.Parse((string)source["budgetCode_etfoid"]));
                }
                //target["reg_memberetfoid"] = source["account_etfomemberid"];
                //target["reg_rttevent"] = source["oems_eventname"];
                //target["reg_memberid"] = etfoMember.ToEntityReference();
                target["reg_currentstatus"] = new OptionSetValue(MRS_STATUS_REQUESTED);
                if (schoolBoard != null) target["reg_boardschoolauthorityid"] = schoolBoard.ToEntityReference();
                if (local != null) target["reg_localid"] = local.ToEntityReference();

                double targetDays = 0;
                if (source.Contains("main_days") && source["main_days"] != null)
                {
                    targetDays += Convert.ToDouble((decimal)source["main_days"]);
                }
                if (source.Contains("extra_days") && source["extra_days"] != null)
                {
                    targetDays += Convert.ToDouble((decimal)source["extra_days"]);
                    target["reg_extradays"] = (decimal)source["extra_days"];
                }

                Entity currency = worker.MRSEnvironment.RetrieveEntityFromSqlByName("transactioncurrency", "currencyname", "Canadian Dollar", new List<FieldDefinition>());

                target["transactioncurrencyid"] = currency.ToEntityReference();
                

                if (source.Contains("date_of_release") && source["date_of_release"] != null)
                {
                    target["reg_dateofrelease"] = (DateTime) source["date_of_release"];
                }

                var allSchedules = GetSchedules(source.Id.ToString(), worker);

                if (allSchedules.Count > 0) 
                {
                    string notes = null;
                    foreach (EntityContainer ec in allSchedules) 
                    {
                        EntityReference eref = (EntityReference)ec.Entity["oems_releasetimeschedule"];
                        Entity releaseDate = worker.EMSEnvironment.OrgService.Retrieve("oems_eventreleasetimeschedule", eref.Id, new Microsoft.Xrm.Sdk.Query.ColumnSet("oems_releasedate"));

                        switch ((int)ec.Entity["oems_releasetimeperiod"]) 
                        {
                            case EMS_PERIOD_FULLDAY:
                                notes = string.Concat(notes,((DateTime)releaseDate["oems_releasedate"]).ToString("MMM dd") + " - Full Day");
                                break;
                            case EMS_PERIOD_HALFDAY_AM:
                                notes = string.Concat(notes,((DateTime)releaseDate["oems_releasedate"]).ToString("MMM dd") + " - Half Day AM");
                                break;
                            case EMS_PERIOD_HALFDAY_PM:
                                notes = string.Concat(notes,((DateTime)releaseDate["oems_releasedate"]).ToString("MMM dd") + " - Half Day PM");
                                break;
                        }

                        notes = string.Concat(notes, Environment.NewLine);
                    }
                    target["reg_notes"] = notes;
                }


                target["reg_daysrequestedfloat"] = targetDays;
                target["reg_budgetline"] = mrsBudgetCode;

               
                string reasons = null;

                var extrareleasetimeScheduleRequest = GetExtraReleaseTimeSchedules(source.Id.ToString(), worker);

                foreach (EntityContainer ec in extrareleasetimeScheduleRequest)
                {
                    string period = null;

                    if (ec.Entity.Contains("oems_releasetimeperiod") && (ec.Entity["oems_releasetimeperiod"] != null))
                    {
                        switch ((int)ec.Entity["oems_releasetimeperiod"])
                        {
                            case EMS_PERIOD_FULLDAY:
                                period = " - Full Day";
                                break;
                            case EMS_PERIOD_HALFDAY_AM:
                                period = " - Half Day AM";
                                break;
                            case EMS_PERIOD_HALFDAY_PM:
                                period = " - Half Day PM";
                                break;
                        }
                    }
                    else
                    {
                        period = " - Full Day";
                    }

                    string reason = (ec.Entity.Contains("oems_reason") && (ec.Entity["oems_reason"] != null)) ? ec.Entity["oems_reason"].ToString() : "";
                    reasons = string.Concat(reasons, ((DateTime)ec.Entity["oems_extrareleasedate"]).ToString("MMM dd"),period," - ",reason, Environment.NewLine);
                }

                target["reg_extradaysreason"] = reasons;


                update = AllowUpdate(worker, target, MRS_STATUS_REQUESTED);

            }
            return result;
        }

        List<EntityContainer> GetExtraReleaseTimeSchedules(string releasetimeId, Worker worker) 
        {
            List<string> ids = new List<string>();
            ids.Add(releasetimeId);

            List<FieldDefinition> requestedFields = new List<FieldDefinition>();
            FieldDefinition oemsReasonField = new FieldDefinition("oems_reason", CRMFieldType.STRING_FIELD);
            FieldDefinition oemsExtrareleasedateField = new FieldDefinition("oems_extrareleasedate", CRMFieldType.DATETIME_FIELD);
            FieldDefinition oemsExtrareleasedatePeriodField = new FieldDefinition("oems_releasetimeperiod", CRMFieldType.INTEGER_FIELD);
            requestedFields.Add(oemsReasonField);
            requestedFields.Add(oemsExtrareleasedateField);
            requestedFields.Add(oemsExtrareleasedatePeriodField);

            var extrareleasetimeScheduleRequest = worker.EMSEnvironment.RetrieveCreateRecordList("oems_extrareleasetimerequestschedule", "oems_releasetimerequest", ids, false, requestedFields);
            return extrareleasetimeScheduleRequest;
        }


        List<EntityContainer> GetSchedules(string releaseTimeRequestId,Worker worker) 
        {
            List<string> ids = new List<string>();
            ids.Add(releaseTimeRequestId);

            List<FieldDefinition> requestedFields = new List<FieldDefinition>();
            FieldDefinition oemsReleasetimePeriod = new FieldDefinition("oems_releasetimeperiod", CRMFieldType.INTEGER_FIELD);
            FieldDefinition oemsReleasetimeSchedule = new FieldDefinition("oems_releasetimeschedule", CRMFieldType.LOOKUP_FIELD);
            requestedFields.Add(oemsReleasetimePeriod);
            requestedFields.Add(oemsReleasetimeSchedule);

            var allSchedules = worker.EMSEnvironment.RetrieveCreateRecordList("oems_releasetimerequestschedule", "oems_releasetimerequest", ids, false, requestedFields);

            return allSchedules;
        }
                    /// <summary>
                    /// Compares the Current request against the existing request in MBR
                    /// </summary>
                    /// <param name="worker"></param>
                    /// <param name="targetRequest"></param>
                    /// <param name="status"></param>
                    /// <returns></returns>
        public bool? AllowUpdate(Worker worker, Entity targetRequest, int status)
        {
            //Null: do not add request
            //False: Add create request
            //True: AllowUpdate
            bool? allowUpdate = null;

            var mbrTarget = (from c in worker.MRSEnvironment.OrgContext.CreateQuery("reg_releasetimetracking")
                             where (string)c["reg_memberetfoid"] == targetRequest["reg_memberetfoid"].ToString()
                             &&
                             c["reg_memberid"] == targetRequest["reg_memberid"]
                             &&
                             c["reg_rttevent"] == targetRequest["reg_rttevent"]
                             &&
                             c["reg_memberetfoid"] == targetRequest["reg_memberetfoid"]
                             select c).FirstOrDefault();

            if (status == MRS_STATUS_CANCELLED && (mbrTarget !=null))
            {   
                //Retrieving the existing request in MBR if it exists
                if (((OptionSetValue)mbrTarget["reg_currentstatus"]).Value == MRS_STATUS_REQUESTED)
                    allowUpdate = true;
            
            }
            if (status == MRS_STATUS_REQUESTED)
            {
                //Retrieving the existing request in MBR if it exists
                if (mbrTarget != null)
                {
                    //Checking if something has changed on the existing request
                    if (((OptionSetValue)mbrTarget["reg_currentstatus"]).Value == MRS_STATUS_REQUESTED)
                    {
                        decimal Target_reg_extradays = decimal.MinusOne;
                        decimal MBR_reg_extradays = decimal.MinusOne;

                        double Target_reg_daysrequestedfloat = -1;
                        double MBR_reg_daysrequestedfloat = -1;

                        string Target_extradaysreason = null;
                        string Mbr_extradaysreason = null;

                        string Target_reg_notes=null;
                        string Mbr_reg_notes =null;

                        if (targetRequest.Contains("reg_extradays") && (targetRequest["reg_extradays"] !=null))
                            decimal.TryParse(targetRequest["reg_extradays"].ToString(), out Target_reg_extradays);

                        if (mbrTarget.Contains("reg_extradays") && (mbrTarget["reg_extradays"] !=null))
                            decimal.TryParse(mbrTarget["reg_extradays"].ToString(), out MBR_reg_extradays);

                        if (targetRequest.Contains("reg_daysrequestedfloat") && (targetRequest["reg_daysrequestedfloat"]!=null))
                            double.TryParse(targetRequest["reg_daysrequestedfloat"].ToString(), out Target_reg_daysrequestedfloat);


                        if (mbrTarget.Contains("reg_daysrequestedfloat") && (mbrTarget["reg_daysrequestedfloat"] !=null))
                            double.TryParse(mbrTarget["reg_daysrequestedfloat"].ToString(), out MBR_reg_daysrequestedfloat);


                        if (targetRequest.Contains("reg_extradaysreason") && (targetRequest["reg_extradaysreason"] !=null))
                            Target_extradaysreason = targetRequest["reg_extradaysreason"].ToString();

                        if (mbrTarget.Contains("reg_extradaysreason") && (mbrTarget["reg_extradaysreason"]!=null))
                            Mbr_extradaysreason = mbrTarget["reg_extradaysreason"].ToString();

                        if (targetRequest.Contains("reg_notes") && (targetRequest["reg_notes"] != null))
                            Target_reg_notes = targetRequest["reg_notes"].ToString();

                        if (mbrTarget.Contains("reg_notes") && (mbrTarget["reg_notes"] != null))
                            Mbr_reg_notes = mbrTarget["reg_notes"].ToString();


                        if (Math.Round(Target_reg_daysrequestedfloat, 2) != Math.Round(MBR_reg_daysrequestedfloat, 2))
                        {
                            allowUpdate = true;
                            targetRequest.Id = mbrTarget.Id;
                        }
                        if (Target_reg_extradays != MBR_reg_extradays) 
                        {
                            allowUpdate = true;
                            targetRequest.Id = mbrTarget.Id;
                        }
                        if (Target_extradaysreason != Mbr_extradaysreason)
                        {
                            allowUpdate = true;
                            targetRequest.Id = mbrTarget.Id;
                        }
                        if (Target_reg_notes != Mbr_reg_notes) 
                        {
                            allowUpdate = true;
                            targetRequest.Id = mbrTarget.Id;
                        }

                    }
                    else
                    {
                        //if allowUpdate is null we are not going to create a batch request
                        allowUpdate = null;
                    }
                }
                else
                {
                    allowUpdate = false;
                }
            }


            return allowUpdate;
        }


        public void CreateChildRequest(Worker worker, OrganizationRequest data)
        {
            worker.MRSEnvironment.ChildrenBatch.Requests.Add(data);
        }

        public void CreateStatusRequest(Entity entity, int statusvalue, Worker worker)
        {
            Microsoft.Xrm.Sdk.EntityReference moniker = new EntityReference();
            moniker.LogicalName = entity.LogicalName;
            moniker.Id = entity.Id;

            Microsoft.Xrm.Sdk.OrganizationRequest request
              = new Microsoft.Xrm.Sdk.OrganizationRequest() { RequestName = "SetState" };
            request["EntityMoniker"] = moniker;
            OptionSetValue state = new OptionSetValue(0);
            OptionSetValue status = new OptionSetValue(statusvalue);
            request["State"] = state;
            request["Status"] = status;

            worker.EMSEnvironment.CurrentBatch.Requests.Add(request);

        }

        public override void ProcessList(Worker worker, List<Entity> sourceEntityList)
        {
            List<string> IdList = new List<string>();
            List<string> schoolBoardIDList = new List<string>();
            List<string> localIDList = new List<string>();
            List<string> etfoMemberIDList = new List<string>();



            foreach (var e in sourceEntityList)
            {
                Entity updatedEms = new Entity("oems_releasetimerequest");
                updatedEms.Id = e.Id;
                updatedEms["oems_pushtomemberrecords"] = false;
                UpdateRequest ur = new UpdateRequest();
                ur.Target = updatedEms;
                worker.EMSEnvironment.CurrentBatch.Requests.Add(ur);
                if (!e.Contains("releasetimerequest_etfoid") || e["releasetimerequest_etfoid"] == null)
                {
                    Guid Id = Guid.NewGuid();
                    updatedEms["oems_etfoid"] = Id.ToString();
                    e["releasetimerequest_etfoid"] = Id.ToString();

                }
                if (e.Contains("local_etfoid")) localIDList.Add((string)e["local_etfoid"]);
                if (e.Contains("schoolboard_etfoid")) schoolBoardIDList.Add((string)e["schoolboard_etfoid"]);

                if (e.Contains("account_etfomemberid") && (e["account_etfomemberid"] != null))
                    etfoMemberIDList.Add((string)e["account_etfomemberid"]);
                else 
                {
                    Exception ex =new Exception("ETFO MEMBER ID is null on Release Time Request ID: " + e["oems_requestid"].ToString());
                    throw (ex);
                }

                IdList.Add((string)e["releasetimerequest_etfoid"]);
            }
            var releaseTimeTrackingList = worker.MRSEnvironment.RetrieveCreateRecordList("reg_releasetimetracking", "reg_releasetimetrackingid", IdList, true, SQLSupport.MRSReleaseTimeTrackingFieldList);

            var schoolBoardList = worker.MRSEnvironment.RetrieveCreateRecordList("account", "accountid", schoolBoardIDList, false);
            var localList = worker.MRSEnvironment.RetrieveCreateRecordList("account", "accountid", localIDList, false);
            var etfoMemberList = worker.MRSEnvironment.RetrieveCreateRecordList("contact", "reg_etfoid", etfoMemberIDList, false);

            foreach (var e in sourceEntityList)
            {
                EntityContainer targetContainer = releaseTimeTrackingList.Find(x => x.Entity.Id.ToString() == (string)e["releasetimerequest_etfoid"]);

                if (targetContainer != null)
                {
                    Entity target = worker.MRSEnvironment.RetrieveCreateRecordWithoutAddingRequest(targetContainer);
                    bool? shouldUpdate = null;
                    var local = localList.Find(x => x.LookupField == (string)e["local_etfoid"]);
                    var schoolBoard = schoolBoardList.Find(x => x.LookupField == (string)e["schoolboard_etfoid"]);
                    var etfoMember = etfoMemberList.Find(x => x.LookupField == (string)e["account_etfomemberid"]);

                    //shouldUpdate = AllowUpdate(worker, target, 0);

                    if (etfoMember != null)
                    {
                        ProcessRecord(worker, e, target, local != null ? local.Entity : null, schoolBoard != null ? schoolBoard.Entity : null, etfoMember.Entity, out shouldUpdate);
                        //Log.Debug("Processed record: " + targetContainer.Entity.Id);
                        Log.Debug("(MBR) reg_releasetimetrackingid Processed: " + e.Id.ToString());
                    }
                   

                    if (shouldUpdate.HasValue && shouldUpdate.Value == true)
                    {
                        UpdateRequest ur = new UpdateRequest();
                        ur.Target = target;
                        worker.MRSEnvironment.CurrentBatch.Requests.Add(ur);
                    }
                    else
                    {
                        if (shouldUpdate.HasValue && shouldUpdate.Value == false)
                        {
                            CreateRequest cr = new CreateRequest();
                            cr.Target = target;
                            worker.MRSEnvironment.CurrentBatch.Requests.Add(cr);
                        }
                    }


                }
            }



        }

        
        public override List<string> GetQueries()
        {
            List<string> queries = new List<string>();

            queries.Add(
                    @"SELECT 
                        A.oems_releasetimerequestid,
                        A.oems_requestid,
                        A.oems_pushtomemberrecords,
                        A.statuscode,
                        A.oems_event,
                        A.oems_schoolboard,
                        A.oems_local,
                        L.oems_etfoid AS local_etfoid,
                        SB.oems_etfoid AS schoolboard_etfoid,

                        A.oems_etfoid AS releasetimerequest_etfoid,
                        B.oems_budgetnumber,
                        C.oems_etfoid AS budgetCode_etfoid,
                        F.oems_etfomemberid AS account_etfomemberid,
                        B.oems_eventname,
						(SELECT SUM (
						                    CASE WHEN RTS.oems_releasetimeperiod = 1 THEN 1
												    WHEN RTS.oems_releasetimeperiod <> 1 THEN 0.5
											END
						            ) 
						FROM   oems_releasetimerequestschedule RTS WITH (NOLOCK)
						WHERE  rts.statecode = 0 AND rts.oems_releasetimerequest = A.oems_releasetimerequestid) AS main_days,

						(SELECT SUM (
						                    CASE WHEN ETS.oems_releasetimeperiod = 1 THEN 1
												    WHEN ETS.oems_releasetimeperiod <> 1 THEN 0.5
											END
						            ) 
						FROM   oems_extrareleasetimerequestschedule ETS WITH (NOLOCK)
						WHERE  ETS.statecode = 0 AND ETS.oems_releasetimerequest = A.oems_releasetimerequestid) AS extra_days,

						(SELECT TOP 1 ERTS.oems_ReleaseDate
						FROM   oems_EventReleaseTimeSchedule ERTS WITH (NOLOCK)
						LEFT JOIN oems_releasetimerequestschedule RTS
						ON ERTS.oems_EventReleaseTimeScheduleId = RTS.oems_ReleaseTimeSchedule
						WHERE  rts.statecode = 0 AND rts.oems_releasetimerequest = A.oems_releasetimerequestid) AS date_of_release,

                        (Select p1.oems_ReleaseTimePeriod
						from 
						(SELECT TOP 1 ERTS.oems_ReleaseDate,RTS.oems_ReleaseTimePeriod
						FROM   oems_EventReleaseTimeSchedule ERTS WITH (NOLOCK)
						LEFT JOIN oems_releasetimerequestschedule RTS
						ON ERTS.oems_EventReleaseTimeScheduleId = RTS.oems_ReleaseTimeSchedule
						WHERE  rts.statecode = 0 AND rts.oems_releasetimerequest = A.oems_releasetimerequestid) p1) as period


                    FROM oems_releasetimerequest A WITH (NOLOCK)
                        JOIN oems_event B WITH (NOLOCK) ON B.oems_eventid = A.oems_event
                        JOIN oems_eventregistration D WITH (NOLOCK) ON D.oems_eventregistrationid = A.oems_eventregistration
                        JOIN account F WITH (NOLOCK) ON F.accountid = D.oems_account 
                        LEFT OUTER JOIN oems_local L WITH (NOLOCK) ON L.oems_localid = A.oems_local
                        LEFT OUTER JOIN oems_schoolboard SB WITH (NOLOCK) ON SB.oems_schoolboardid = A.oems_schoolboard
                        LEFT OUTER JOIN oems_budgetcode C WITH (NOLOCK) ON C.oems_budgetcodeid = B.oems_budgetnumber
                         
                    WHERE A.modifiedOn > @modifiedon AND A.oems_pushtomemberrecords = 1 AND (A.statuscode = 347780002 OR A.statuscode = 347780004)");

            //JOIN account F WITH (NOLOCK) ON F.accountid = D.oems_account AND F.oems_etfomemberid IS NOT NULL

            return queries;
        }

        public override List<FieldDefinition> FieldList
        {
            get
            {
                return SQLSupport.EMSReleaseTimeRequestFieldList;
            }
        }

        public override CRMEnvironmentType SubscriberType
        {
            get
            {
                return CRMEnvironmentType.EMS;
            }
        }

        public override ActionType SubscriberActionType
        {
            get
            {
                return ActionType.Date;
            }
        }

        public override bool ReleaseTimeRelated { get { return true; } }

    }
}

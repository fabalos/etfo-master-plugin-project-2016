﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Omniware.ETFO.IntegrationLoader
{
    

    public class Config
    {

        private static string queueEntityName = null;
        private static Dictionary<string, string> KeypairValueTeacherTypes;
        public static string QueueEntityName
        {
            get {
                return queueEntityName ??
                       (queueEntityName = System.Configuration.ConfigurationManager.AppSettings["queueEntityName"]);
            }
        }

       private static string[] teacherTypes = null;
       public static string[] TeacherTypes
       {
           get {
               return teacherTypes ??
                      (teacherTypes = System.Configuration.ConfigurationManager.AppSettings["teacherTypes"].Split(','));
           }
       }

       public static Dictionary<string, string> GetKeypairValueTeacherTypes 
       {
           get
           {
               if(KeypairValueTeacherTypes==null)
                   LoadKeyPairTeacherTypes();
               return KeypairValueTeacherTypes;
           }
       }

       static void LoadKeyPairTeacherTypes() 
       {
           
           string fulltext = System.Configuration.ConfigurationManager.AppSettings["TeacherTypeLocalRelationship"];
           string[] keypairvalues = fulltext.Split(';');

           if (keypairvalues.Length > 0)
           {
               KeypairValueTeacherTypes = new Dictionary<string, string>();
               foreach (string keypair in keypairvalues)
               {
                   // KeypairValueTeacherTypes
                   string[] keyandpair = keypair.Split(',');
                   KeypairValueTeacherTypes.Add(keyandpair[0], keyandpair[1]);
               }
           }
       }
       public static Dictionary<string, string> LoadKeyPairTValues(string configSetting)
       {

           string fulltext = System.Configuration.ConfigurationManager.AppSettings[configSetting];
           string[] keypairvalues = fulltext.Split(';');
           Dictionary<string, string> keypairValues = null;

           if (keypairvalues.Length > 0)
           {
               keypairValues = new Dictionary<string, string>();
               foreach (string keypair in keypairvalues)
               {
                   // KeypairValueTeacherTypes
                   string[] keyandpair = keypair.Split(',');
                   keypairValues.Add(keyandpair[0], keyandpair[1]);
               }
           }

           return keypairValues;
       }





       private static string[] localKeywords = null;
       public static string[] LocalKeywords
       {
           get {
               return localKeywords ??
                      (localKeywords = System.Configuration.ConfigurationManager.AppSettings["localKeyword"].Split(','));
           }
       }

       private static string[] localKeywordsWrongMatch = null;
       public static string[] LocalKeywordsWrongMatch
       {
           get {
               return localKeywordsWrongMatch ??
                      (localKeywordsWrongMatch =
                       System.Configuration.ConfigurationManager.AppSettings["wrongMatch"].Split(','));
           }
       }


       public static string GetLocalKeyword(string teacherType)
       {
          

          for(int i = 0; i < TeacherTypes.Length; i++)
          {
              if (teacherType.ToLower() == TeacherTypes[i].ToLower()) return LocalKeywords[i];
          }

          return teacherType;
       }

       public static string GetLocalKeywordWrongMatch(string teacherType)
       {


           for (int i = 0; i < TeacherTypes.Length; i++)
           {
               if (teacherType.ToLower() == TeacherTypes[i].ToLower()) return LocalKeywordsWrongMatch[i];
           }

           return teacherType;
       }

       private static string logFile = null;
       public static string LogFile
       {
           get { return logFile ?? (logFile = System.Configuration.ConfigurationManager.AppSettings["logFile"]); }
       }

       private static string progressFile = null;
       public static string ProgressFile
       {
           get {
               return progressFile ??
                      (progressFile = System.Configuration.ConfigurationManager.AppSettings["progressFile"]);
           }
       }


       
/* superseeded by command line option
       public static LOAD_DIRECTION LoadDirection
       {
           get
           {
               LOAD_DIRECTION loadDirection = LOAD_DIRECTION.NONE;
               string direction = System.Configuration.ConfigurationManager.AppSettings["loadDirection"];
               int intDirection;
               if (int.TryParse(direction, out intDirection))
               {
                   loadDirection = (LOAD_DIRECTION)intDirection;
               }
               return loadDirection;
           }
       }
*/
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using log4net;

namespace Omniware.ETFO.IntegrationLoader
{
    public abstract class IDataQueueSubscriber
    {

        abstract public List<string> GetQueries();
        abstract public List<FieldDefinition> FieldList { get; }
        abstract public CRMEnvironmentType SubscriberType { get; }

        abstract public ActionType SubscriberActionType { get; }
        abstract public string EntityName { get; }
        abstract public void ProcessList(Worker worker, List<Entity> sourceEntityList);


        // release timer related ( used to filter release timer only), false by default
        public virtual bool ReleaseTimeRelated { get { return false; } }

        // true if subscriber transfers data from MBR to EMS, false if transfers in opposite direction 
        public bool DirectionToEms
        {
            get { return SubscriberType == CRMEnvironmentType.MRS; }
        }

        //void DoWork(Worker worker);
        public override string ToString()
        {
            return string.Format("{0} EntityName {1}, : DirectionToEms: {2}, ReleaseTimeRelated: {3}", GetType(), DirectionToEms, EntityName, ReleaseTimeRelated);
        }

        public IPostSubscriber PostSubscriber { get; set; }
    }
}

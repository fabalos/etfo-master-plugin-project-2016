﻿using Microsoft.Xrm.Sdk;
using Omniware.ETFO.IntegrationLoader;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Omniware.ETFO.IntegrationLoader
{
    public interface IPostSubscriber
    {
        void ExecuteSubscriber(Worker worker);

        List<Entity> Entities { get; set; }
    }
}

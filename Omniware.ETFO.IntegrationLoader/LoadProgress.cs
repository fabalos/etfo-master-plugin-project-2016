﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;

namespace Omniware.ETFO.IntegrationLoader
{
    public class LoadProgress
    {
        static ILog Log = LogManager.GetLogger(typeof(LoadProgress));

        public string SubscriberTypeName { get; set; }
        public List<Guid> processedEntityIdList { get; set; }

        private static LoadProgress currentProgress = null;

        public LoadProgress()
        {
            processedEntityIdList = new List<Guid>();
            SubscriberTypeName = null;
        }
        
        public static LoadProgress GetLoadProgress()
        {
            if (currentProgress != null) return currentProgress;

            Log.Debug("Loading progress");
            

            if(System.IO.File.Exists(Config.ProgressFile))
            {
                System.IO.StreamReader sr = new System.IO.StreamReader(Config.ProgressFile);
                System.Xml.Serialization.XmlSerializer xs = new System.Xml.Serialization.XmlSerializer(typeof(LoadProgress));
                currentProgress = (LoadProgress)xs.Deserialize(sr);
                sr.Close();
            }
            else currentProgress = new LoadProgress();

            return currentProgress;
        }

        public void SaveProgress()
        {
            Log.Info("Saving progress");
            System.IO.StreamWriter sw = new System.IO.StreamWriter(Config.ProgressFile);
            System.Xml.Serialization.XmlSerializer xs = new System.Xml.Serialization.XmlSerializer(typeof(LoadProgress));
            xs.Serialize(sw, this);
            sw.Close();
        }



    }
}

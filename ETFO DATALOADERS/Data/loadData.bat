@echo off
echo.
echo.
echo =================== LOADING MASTER TEMPLATE (WOW) ===================
echo.
echo 0.01 Loading Form Properties
java -jar ../DataLoader/omniloader.jar ./omni_FormElementProperty.csv ../LoaderDefinitions/omni_FormElementProperty.xml
if %errorlevel% neq 0 exit /b %errorlevel%
echo Success.
echo 0.02 Loading Form Types
java -jar ../DataLoader/omniloader.jar ./omni_FormElementType.csv ../LoaderDefinitions/omni_FormElementType.xml
if %errorlevel% neq 0 exit /b %errorlevel%
echo Success.
echo 0.03 Loading Master Template
java -jar ../DataLoader/omniloader.jar ./omni_MasterTemplate.csv ../LoaderDefinitions/omni_FormTemplate.xml
if %errorlevel% neq 0 exit /b %errorlevel%
echo Success.
echo 0.04 Loading Bindings
java -jar ../DataLoader/omniloader.jar ./omni_FormElementBinding.csv ../LoaderDefinitions/omni_FormElementBinding.xml
if %errorlevel% neq 0 exit /b %errorlevel%
echo Success.

echo 0.05 Loading Form Elements
java -jar ../DataLoader/omniloader.jar ./MasterElementsOrdered.csv ../LoaderDefinitions/omni_FormElement.xml
if %errorlevel% neq 0 exit /b %errorlevel%
echo Success.
echo..DONE


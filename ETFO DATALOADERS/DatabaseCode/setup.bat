@echo off
echo Create DB Objects
echo.
echo Creating OMNIWARE Schema and Loading SQLSERVER Objects ...
echo USE [%organizationName%_MSCRM] > .\createOmniwareSchema.sql
echo GO >> .\createOmniwareSchema.sql
echo CREATE SCHEMA [OMNIWARE] >> .\createOmniwareSchema.sql
echo GO >> .\createOmniwareSchema.sql
echo CREATE SEQUENCE OMNIWARE.OMNI_EVENT_SEQ START WITH 1 INCREMENT BY 1 >> .\createOmniwareSchema.sql
echo GO >> .\createOmniwareSchema.sql
echo.
echo -------------------------------------------
echo Creating OMNIWARE Schema...
echo -------------------------------------------
if "%sqlDatabaseName%"=="" (
	"C:\Program Files\Microsoft SQL Server\110\Tools\Binn\sqlcmd.exe" -S %sqlServerName% -I -i .\createOmniwareSchema.sql -d %organizationName%_MSCRM
) else (
	"C:\Program Files\Microsoft SQL Server\110\Tools\Binn\sqlcmd.exe" -S %sqlServerName%\%sqlDatabaseName% -I -i .\createOmniwareSchema.sql -d %organizationName%_MSCRM
)
if %errorlevel% neq 0 exit /b %errorlevel%
echo.
echo -------------------------------------------
echo Installing Core Functions...
echo -------------------------------------------
if "%sqlDatabaseName%"=="" (
	"C:\Program Files\Microsoft SQL Server\110\Tools\Binn\sqlcmd.exe" -S %sqlServerName% -I -i .\fn_omni_GetArea.sql -d %organizationName%_MSCRM
) else (
	"C:\Program Files\Microsoft SQL Server\110\Tools\Binn\sqlcmd.exe" -S %sqlServerName%\%sqlDatabaseName% -I -i .\fn_omni_GetArea.sql -d %organizationName%_MSCRM
)
if %errorlevel% neq 0 exit /b %errorlevel%
echo.
echo.
echo -------------------------------------------
echo DONE
echo -------------------------------------------
echo.
echo.

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jasbir Kular
-- Create date: Sept 15, 2015
-- Description:	Return one row of area hierarchy
-- =============================================
CREATE FUNCTION [OMNIWARE].[fn_omni_GetArea]
(	
	@areaId uniqueidentifier,
	@abbreviationFlag int
)
RETURNS TABLE 
AS
RETURN 
(
	WITH AreaCTE AS (
	  SELECT omni_AreaId, omni_AreaName, omni_AreaAbbreviation, omni_AreaType, omni_ParentAreaRef,0 AS steps
	  FROM omni_Area
	  WHERE omni_AreaId = @areaId
	  UNION ALL
	  SELECT Area.omni_AreaId, Area.omni_AreaName, Area.omni_AreaAbbreviation, Area.omni_AreaType, Area.omni_ParentAreaRef, AreaCTE.steps +1 AS steps
	  FROM AreaCTE INNER JOIN omni_Area AS Area
		  ON AreaCTE.omni_ParentAreaRef = Area.omni_AreaId
	)
	select *
	from
	(
	  select CASE @abbreviationFlag
			 WHEN 0 THEN omni_AreaName
			 WHEN 1 THEN ISNULL(omni_AreaAbbreviation, omni_AreaName)
			 END areaName,
			 CASE omni_AreaType
			 WHEN 666400002 THEN 'CITY'
			 WHEN 666400001 THEN 'STATE'
			 WHEN 666400000 THEN 'COUNTRY'
			 END columnname
	  from AreaCTE
	) d
	pivot
	(
	  max(areaName)
	  for columnname in (CITY, STATE, COUNTRY)
	) piv
)
GO

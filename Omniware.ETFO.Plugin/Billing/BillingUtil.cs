using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Omniware.ETFO.Plugin.DataWork;

namespace Omniware.ETFO.Plugin.Billing
{
    public static class PaymentStatus
    {
        public static int Unpaid = 347780000;
        public static int ChequePending = 347780001;
        public static int CreditCardPending = 347780002;
        public static int Paid = 347780003;
        public static int Refunded = 347780004;
        public static int Partial = 347780005;
        public static int ChequeRefundPending = 347780006;
        public static int PayLater = 347780007;
        public static int Cancelled = 347780008;
        public static int Failed = 347780009;
        public static int RefundFailed = 347780010;
    } 

    public static class BillingStatus
    {
        public static int None = 347780000;
        public static int RefundFailed = 347780001;
    }

    public static class PaymentMethod
    {
        public static int Cheque = 347780000;
        public static int CreditCard = 347780001;
    }

    public static class BillingUtil
    {
        //retrieve pricing rules for event or room
        public static List<PricingRule> GetPricingRules(CrmOrganizationServiceContext context, Guid eventId, string pricingEntityName)
        {
            return (from pricingRuleEntity in context.CreateQuery(pricingEntityName)
                    where (Guid)pricingRuleEntity["oems_event"] == eventId
                    select PricingRule.FromEntity(pricingRuleEntity)
                   ).ToList();
        }

        public static decimal GetHstRate(CrmOrganizationServiceContext context)
        {
            const decimal defaultHstRate = 0.13m;
            var configuration =
            (
                from c in context.CreateQuery("oems_configuration")
                select c
            ).FirstOrDefault();

            if (configuration == null)
            {
                return defaultHstRate;
            }

            var hstRate = configuration.GetAttributeValue<decimal?>("oems_eventpricinghstrate");

            return hstRate / (decimal)100.0 ?? defaultHstRate;
        }

        public static bool IsEventCostApplyHst(CrmOrganizationServiceContext context, Guid eventPriceId)
        {
            var entity = context.Retrieve("oems_eventpricing", eventPriceId, new ColumnSet("oems_applyhst"));

            if (entity == null)
                return true;

            var applyHst = entity.GetAttributeValue<bool>("oems_applyhst");
            return applyHst;
        }

        public static Entity ResetBillingRequestValues(CrmOrganizationServiceContext context, Entity billingRequest)
        {
            Entity account = null;

            // Calculate deferral
            var deferral = billingRequest.GetAttributeValue<decimal>("oems_deferralamount");
            if (deferral > 0)
            {
                // Event registration
                var registrationRef = billingRequest.GetAttributeValue<EntityReference>("oems_eventregistration");
                var registration = context.Retrieve(registrationRef.LogicalName, registrationRef.Id, new ColumnSet("oems_account")).ToEntity<Xrm.oems_EventRegistration>();

                // User account
                var accountRef = registration.GetAttributeValue<EntityReference>("oems_account");
                account = GetAccount(context, accountRef.Id);
                var adeferral = account.GetAttributeValue<decimal>("oems_deferralcreditamount");
                adeferral += deferral;
                account["oems_deferralcreditamount"] = adeferral;
            }

            billingRequest["oems_paymenttransaction"] = null;
            billingRequest["oems_paymentstatus"] = new OptionSetValue(PaymentStatus.Unpaid);
            billingRequest["oems_deferralamount"] = null;
            billingRequest["oems_paymentdate"] = null;
            billingRequest["oems_paymentamount"] = null;
            return account;
        }

        // retrieve one pricing rule
        public static PricingRule GetPricingRule(CrmOrganizationServiceContext context, Guid pricingRuleId, string pricingEntityName)
        {
            return PricingRule.FromEntity(context.Retrieve(pricingEntityName, pricingRuleId, new ColumnSet(true)));
        }

        // get accomodation or billing request  
        // todo add regform wide cache to only retrieve each request type  once at the beging
        public static Entity GetRequest(CrmOrganizationServiceContext context, Guid registrationId, string requestEntityName)
        {
            return (
                       from requests in context.CreateQuery(requestEntityName)
                       where (Guid)requests["oems_eventregistration"] == registrationId
                       select requests
                   ).FirstOrDefault();
        }

        public static Entity GetAccount(CrmOrganizationServiceContext context, Guid accountId)
        {
            return (
                       from account in context.CreateQuery(Xrm.Account.EntityLogicalName)
                       where (Guid)account["accountid"] == accountId
                       select account
                   ).FirstOrDefault();
        }

        public static Entity GetContact(CrmOrganizationServiceContext context, Guid contactId)
        {
            return (
                       from contact in context.CreateQuery(Xrm.Contact.EntityLogicalName)
                       where (Guid)contact["contactid"] == contactId
                       select contact
                   ).FirstOrDefault();
        }

        public static int GetTotalWorkshopSessions(CrmOrganizationServiceContext context, Guid registrationId)
        {
            var rs =
            (
                from request in context.CreateQuery("oems_eventworkshopsessionrequest")
                where (Guid)request["oems_eventapplication"] == registrationId
                select request
            ).ToList();

            return rs.Count;
        }

        public static decimal GetWorkshopSessionsPercentageDiscount(CrmOrganizationServiceContext context, Guid eventId, int totalWorkshops)
        {
            var discounts =
            (
                from request in context.CreateQuery("oems_eventworkshopdiscount")
                where ((EntityReference)request["oems_event"]).Id == eventId
                select request
            ).ToList();

            var discount = (decimal)0;
            foreach (var dw in discounts)
            {
                if ((decimal)dw["oems_numberofworkshops"] <= (decimal)totalWorkshops && (decimal)dw["oems_discountpercentage"] >= discount)
                {
                    discount = (decimal)dw["oems_discountpercentage"];
                }
            }

            return discount;
        }

        public static bool IsRefundRequired(CrmOrganizationServiceContext context, Entity evt, Guid registrationId, out bool substractRegistrationFee, out decimal registrationFee, out Entity billingRequest)
        {
            substractRegistrationFee = false;
            registrationFee = 0;
            billingRequest = null;

            if (evt == null)
            {
                throw new OEMSPluginException("Event does not exist");
            }

            var doRefund = !evt.GetAttributeValue<bool>("oems_donotautomaticallyrefundoncancellation");
            if (!doRefund)
            {
                return false;
            }

            substractRegistrationFee = evt.GetAttributeValue<bool>("oems_refundlessregistrationfee");
            registrationFee = evt.GetAttributeValue<decimal>("oems_registrationfeeamount");

            billingRequest = BillingUtil.GetRequest(context, registrationId, "oems_billingrequest");
            if (billingRequest == null)
            {
                return false;
            }

            //Pending means cheque pending
            var paymentStatus = billingRequest.Contains("oems_paymentstatus") ? ((OptionSetValue)billingRequest["oems_paymentstatus"]).Value : -1;
            if (paymentStatus != PaymentStatus.Paid)
            {
                doRefund = false;
            }

            return doRefund;
        }

        public static decimal CalculateRefundValue(CrmOrganizationServiceContext context, Guid eventId, bool substractRegistrationFee, decimal registrationFee, Entity billingRequest, bool isWithdrawn, out decimal baseAmount, out decimal percentage)
        {
            baseAmount = 0;
            percentage = 100;
            var refundAmount = (decimal)0;
            if (billingRequest != null)
            {
                var accommodationCost = billingRequest.GetAttributeValue<decimal>("oems_accommodationcost");
                var accommodationTax = billingRequest.GetAttributeValue<decimal>("oems_accommodationtax");
                var ticketsCost = billingRequest.GetAttributeValue<decimal>("oems_ticketcost");
                var ticketsTax = billingRequest.GetAttributeValue<decimal>("oems_tickettax");

                //Deferral was something the user used to pay
                //Accommodation and tickets are refundable now
                baseAmount = billingRequest.GetAttributeValue<decimal>("oems_totalamountdue") - billingRequest.GetAttributeValue<decimal>("oems_deferralamount") /*- accommodationCost - accommodationTax - ticketsCost - ticketsTax*/;

                if (substractRegistrationFee)
                {
                    var hstRate = GetHstRate(context);
                    baseAmount -= registrationFee + (registrationFee * hstRate);
                }

                if (isWithdrawn)
                {
                    percentage = 0;
                    var now = DateTime.Now;
                    var policies =
                    (
                        from request in context.CreateQuery("oems_eventrefundpolicy")
                        where ((EntityReference)request["oems_event"]).Id == eventId
                        select request
                    ).ToList();

                    foreach (var p in policies)
                    {
                        var cancellationDate = p.GetAttributeValue<DateTime>("oems_cancellationdate");

                        //Go to the end of the day
                        cancellationDate = cancellationDate.AddHours(23).AddMinutes(59).AddSeconds(59).AddMilliseconds(999);
                        if (cancellationDate.Subtract(now).TotalMilliseconds >= 0 && p.GetAttributeValue<decimal>("oems_refundpercentage") >= percentage)
                        {
                            percentage = p.GetAttributeValue<decimal>("oems_refundpercentage");
                        }
                    }
                }
                refundAmount = baseAmount * percentage / 100;
            }

            return refundAmount;
        }
    }
}
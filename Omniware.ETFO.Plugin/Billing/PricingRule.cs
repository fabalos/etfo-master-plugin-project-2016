﻿using System;
using Microsoft.Xrm.Sdk;
using Omniware.ETFO.Plugin.Shared;
using Xrm;

namespace Omniware.ETFO.Plugin.Billing
{
    public class PricingRule
    {
        public Guid Id { get; set; }
        public OptionSetValue ParticipantType { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        // precalculated tax amount ( if HST is not used)
        // unused public decimal TaxAmount { get; set; }
        public bool ApplyHst { get; set; }

        public decimal Tax(decimal hstRate, int units = 1)
        {
            return ApplyHst ? CalculateTax(hstRate, units * Price) : 0;
        }

        public static decimal CalculateTax(decimal amount, decimal hstRate)
        {
            return Math.Round(amount * hstRate, 2);
        }

        // it is easier to debug and reuse if it is not backed into query
        public static PricingRule FromEntity(Entity pricingRule)
        {
            return new PricingRule
                {
                    Id = pricingRule.Id,
                    ParticipantType = pricingRule.GetAttributeValue<OptionSetValue>("oems_participanttype"),
                    Name = pricingRule.GetAttributeValue<string>("oems_name"),
                    Price = pricingRule.GetAttributeValue<decimal>("oems_price"),
                    //unused TaxAmount = pricingRule.GetAttributeValue<decimal>("oems_taxamount"),
                    ApplyHst = pricingRule.GetAttributeValue<bool>("oems_applyhst")
                };
        }


        public bool AppliesToAccount(Account account, oems_Event theEvent)
        {

            var participantType = ParticipantType.Value;

            if (participantType == BillingParticipantType.MEMBER
                && account.IsMember()) return true;


            if (participantType == BillingParticipantType.NON_MEMBER_EDUCATOR_OR_EXTERNAL 
                &&  account.IsNonMemberEducatorOrExternal()
               ) return true;


            if (participantType == BillingParticipantType.STAFF  && account.IsNonMemberStaff())
                return true;


            // this test is slow to execute .. run as late as possible store result
            var isGuestOfEvent = account.IsGuestOfEvent(theEvent);

            if (participantType == BillingParticipantType.INVITED_GUEST && isGuestOfEvent)
                return true;

            return false; 

        }




    }
}
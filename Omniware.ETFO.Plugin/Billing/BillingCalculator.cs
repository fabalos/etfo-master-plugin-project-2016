﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Omniware.ETFO.Plugin.DataWork;
using Omniware.ETFO.Plugin.FormComponents;
using Omniware.ETFO.Plugin.Shared;
using Xrm;
using log4net;

namespace Omniware.ETFO.Plugin.Billing
{
    // tracks and calculated pricing information from Room pricing, Ticket and Event pricing 
    class BillingCalculator
    {
        private ILog log = LogHelper.GetLogger(typeof(BillingCalculator));

        private CrmOrganizationServiceContext context;
        private Entity eventRegistration;
        private Entity theEvent;

        private decimal ticketPrice;
        private bool ticketApplyHst;
        private int additionalTickets;
        private bool hasTicketSection;

        private int accomodationDays;
        private decimal accomodationCost, accomodationTax;
        private decimal eventCost, eventTax;
        //private decimal

        private decimal ticketCost, ticketTax;

        private PricingRule roomPricing;
        private PricingRule eventPricing;

        private bool hasWorkshopSession = false;
        private decimal numberOfWorkshopsDiscountAmount = 0;

        public BillingCalculator(CrmOrganizationServiceContext context, Entity eventRegistration, Entity theEvent = null)
        {
            this.context = context;
            this.eventRegistration = eventRegistration;
            this.theEvent = theEvent ?? context.CreateQuery("oems_event").Single(ev => (Guid)ev["oems_eventid"] == eventRegistration.GetAttributeValue<EntityReference>("oems_event").Id);
        }

        // calculation ---------

        // track  billable elements as they are being saved
        public void CollectBillableFormElement(FormElement element, List<NameValuePair> data)
        {
            if (element.id == "Schedule")
            {
                var nameValuePairs = data.FindAll(nv => nv.name.EndsWith(element.name));
                accomodationDays = nameValuePairs.Count;
            }
            else if (element.id == RoomPricingFormElement.ROOM_PRICE)
            {
                // read saved value from form 
                // todo in the future the requests should be cached .. all retrieved up front and saved in the end
                var accommodRequest = BillingUtil.GetRequest(context, eventRegistration.Id, "oems_accommodationrequest");
                var pricing = accommodRequest != null ? accommodRequest.GetAttributeValue<EntityReference>("oems_accommodationpricing") : null;

                if (pricing == null)
                {
                    log.Debug("No room price selection found on evt registration " + eventRegistration.Id);
                    return;
                }
                roomPricing = BillingUtil.GetPricingRule(context, pricing.Id, "oems_accommodationpricing");

            }
            else if (element.id == EventPricingFormElement.EVENT_PRICE)
            {
                // read saved value from form 
                // todo in the future the requests should be cached .. all retrieved up front and saved in the end
                var billingRequest = BillingUtil.GetRequest(context, eventRegistration.Id, "oems_billingrequest");
                var pricing = billingRequest != null ? billingRequest.GetAttributeValue<EntityReference>("oems_eventpricing") : null;

                if (pricing == null)
                {
                    if ((eventRegistration["statuscode"] as OptionSetValue).Value != EventRegistrationStatusReason.Initial)
                    {
                        // do not allow user to spoof the value of empty event price if event price is configured on the template
                        throw new InvalidPluginExecutionException("Event price selection is required.");
                    }
                    return;
                }
                eventPricing = BillingUtil.GetPricingRule(context, pricing.Id, "oems_eventpricing");

            }
            else if (element.id == "Additional Tickets")
            {
                log.Debug("This form has Additional Tickets element");
                var nameValuePair = data.FirstOrDefault(nv => nv.name.EndsWith(element.name));
                if (nameValuePair != null)
                {
                    int.TryParse(nameValuePair.value, out additionalTickets);
                    if (additionalTickets < 0)
                    {
                        // this would create a "credit" to the total amount
                        throw new InvalidPluginExecutionException("Negative amount of additional tickets");
                    }
                    else if (additionalTickets > 0)
                    {
                        hasTicketSection = true;
                    }
                }

                //get ticket price from db oems_event.oems_ticketprice

                ticketPrice = theEvent.GetAttributeValue<decimal>("oems_ticketprice");
                ticketApplyHst = theEvent.GetAttributeValue<bool>("oems_ticketapplyhst");

            }
            else if (element.id == "Workshop Session Component")
            {
                hasWorkshopSession = true;
            }
        }

        // after all elements are processed total up the cost and save Billing request
        public void SaveBillingRequest()
        {
            //todo do not modfiy pricing if paid ?
            var billingRequest = BillingUtil.GetRequest(context, eventRegistration.Id, "oems_billingrequest");

            if (billingRequest == null)
            {
                return;
            }

            var billingRequestAux = new Entity(billingRequest.LogicalName);
            billingRequestAux.Id = billingRequest.Id;

            //Do not allow to modify an unpaid billing request
            var paymentStatusOp = billingRequest.GetAttributeValue<OptionSetValue>("oems_paymentstatus");
            if (paymentStatusOp != null)
            {
                var paymentStatus = paymentStatusOp.Value;
                if (paymentStatus == PaymentStatus.ChequePending || paymentStatus == PaymentStatus.CreditCardPending || paymentStatus == PaymentStatus.Paid || paymentStatus == PaymentStatus.Partial || paymentStatus == PaymentStatus.Refunded)
                {
                    return;
                }
            }

            //HST
            var hstRate = BillingUtil.GetHstRate(context);

            //EVENT
            if (eventPricing != null)
            {
                eventCost = eventPricing.Price;
                eventTax = eventPricing.Tax(hstRate);
            }

            // + Workshop Discounts
            if (hasWorkshopSession)
            {
                var totalWorkshopSessions = BillingUtil.GetTotalWorkshopSessions(context, eventRegistration.Id);
                var discount = BillingUtil.GetWorkshopSessionsPercentageDiscount(context, ((EntityReference)eventRegistration["oems_event"]).Id, totalWorkshopSessions);
                numberOfWorkshopsDiscountAmount = -discount * eventCost / 100;

                billingRequestAux["oems_numberofworkshopsdiscountpercentage"] = discount;
                billingRequestAux["oems_numberofworkshopsdiscountamount"] = numberOfWorkshopsDiscountAmount;
            }

            // + Additional fees
            var additionalFeeForMaterials = theEvent.Contains("oems_additionalfeeforeventmaterials") ? Convert.ToDecimal(theEvent["oems_additionalfeeforeventmaterials"]) : 0;
            if (additionalFeeForMaterials > 0)
            {
                billingRequestAux["oems_additionalfeeforeventmaterials"] = additionalFeeForMaterials;
            }
            var registrationFee = theEvent.Contains("oems_registrationfeeamount") ? Convert.ToDecimal(theEvent["oems_registrationfeeamount"]) : 0;
            if (registrationFee > 0)
            {
                billingRequestAux["oems_registrationfeeamount"] = registrationFee;
            }

            // + Event tax for fees and including discounts
            eventTax += PricingRule.CalculateTax(additionalFeeForMaterials + registrationFee + numberOfWorkshopsDiscountAmount, hstRate);

            //ACCOMODATION
            if (roomPricing != null)
            {
                accomodationCost = Math.Round(accomodationDays * roomPricing.Price, 2);
                accomodationTax = roomPricing.Tax(hstRate, accomodationDays);
            }

            //TICKETS
            ticketCost = Math.Round(ticketPrice * additionalTickets, 2);
            ticketTax = ticketApplyHst ? PricingRule.CalculateTax(hstRate, ticketCost) : 0m;

            if (hasTicketSection)
            {
                var ticketRequest = BillingUtil.GetRequest(context, eventRegistration.Id, "oems_ticketrequest");
                if (ticketRequest == null)
                {
                    ticketRequest = new Entity("oems_ticketrequest");
                    ticketRequest["oems_event"] = eventRegistration["oems_event"];
                    ticketRequest["oems_eventregistration"] = eventRegistration.ToEntityReference();

                }
                ticketRequest["oems_ticketcost"] = ticketCost + ticketTax;
                context.CreateOrUpdate(ticketRequest);
            }

            //TOTAL
            var totalAmountDue = eventCost + eventTax + registrationFee + additionalFeeForMaterials + numberOfWorkshopsDiscountAmount + accomodationCost + accomodationTax + ticketCost + ticketTax;

            //SUMMARY
            billingRequestAux["oems_paymentstatus"] = new OptionSetValue(PaymentStatus.Unpaid);
            billingRequestAux["oems_deferralamount"] = null;
            billingRequestAux["oems_accommodationdays"] = (decimal)accomodationDays;
            if (roomPricing != null)
            {
                billingRequestAux["oems_roomprice"] = roomPricing.Price;
            }
            billingRequestAux["oems_accommodationcost"] = accomodationCost;
            billingRequestAux["oems_accommodationtax"] = accomodationTax;
            billingRequestAux["oems_eventcost"] = eventCost;
            billingRequestAux["oems_eventtax"] = eventTax;
            billingRequestAux["oems_additionaltickets"] = (decimal)additionalTickets;
            billingRequestAux["oems_ticketprice"] = ticketPrice;
            billingRequestAux["oems_ticketcost"] = ticketCost;
            billingRequestAux["oems_tickettax"] = ticketTax;

            billingRequestAux["oems_totalamountdue"] = totalAmountDue;

            // save to billing request
            try
            {
                context.Detach(billingRequest);
                context.Attach(billingRequestAux);
                context.UpdateObject(billingRequestAux);
                log.Info("Saving billing calculation: " + this);
                context.SaveChanges();
            }
            catch (Exception e)
            {
                throw e.InnerException;
            }
        }

        public void Refund(Guid contactId, bool isWithdrawn = false)
        {
            //Check if there was a payment and refund
            // + Check if refund has to be performed
            bool substractRegistrationFee;
            decimal registrationFee;
            Entity billingRequest;

            var doRefund = BillingUtil.IsRefundRequired(context, this.theEvent, eventRegistration.Id, out substractRegistrationFee, out registrationFee, out billingRequest);
            if (doRefund)
            {
                // + Calculate refund value
                decimal percentage;
                decimal baseAmount;
                var refundAmount = BillingUtil.CalculateRefundValue(context, this.theEvent.Id, substractRegistrationFee, registrationFee, billingRequest, isWithdrawn, out baseAmount, out percentage);

                // + Refund process
                if (refundAmount > 0)
                {
                    // + Calculate refund components
                    var deferral = billingRequest.GetAttributeValue<decimal>("oems_deferralamount");
                    var totalToDeferralCredit = Math.Min(deferral, refundAmount);
                    var totalToCustomer = refundAmount - totalToDeferralCredit;

                    // + Is credit card?
                    // + Call Moneris API to refund and update billing request
                    var paymentTransaction =
                    (
                        from request in context.CreateQuery("oems_cctransaction")
                        where ((Guid)request["oems_cctransactionid"]) == billingRequest.GetAttributeValue<EntityReference>("oems_paymenttransaction").Id
                        select request
                    ).First();

                    var paymentMethod = paymentTransaction.Contains("oems_paymentmethod") ? ((OptionSetValue)paymentTransaction["oems_paymentmethod"]).Value : -1;
                    var isCreditCard = paymentMethod == PaymentMethod.CreditCard;

                    //Check for Moneris updates
                    bool isRefunded = false;
                    if (isCreditCard && totalToCustomer > 0)
                    {
                        var errorMessage = string.Empty;
                        var transactionId = paymentTransaction.GetAttributeValue<string>("oems_transactionnumber");
                        var requestId = paymentTransaction.GetAttributeValue<string>("oems_orderid");
                        isRefunded = MonerisUtil.Refund(context, billingRequest, requestId, totalToCustomer, "Registration Cancelled", transactionId, out errorMessage);
                        if (isRefunded && totalToDeferralCredit > 0)
                        {
                            // + Refund deferral into the contact
                            var contact = BillingUtil.GetContact(context, contactId);
                            var adeferral = contact.GetAttributeValue<decimal>("oems_deferralcreditamount");
                            adeferral += totalToDeferralCredit;
                            var auxContact = new Entity(contact.LogicalName);
                            auxContact.Id = contact.Id;
                            auxContact["oems_deferralcreditamount"] = adeferral;
                            context.UpdateObject(auxContact);
                        }
                    }

                    // + A a CC transaction for the refund was created and assigned to the billing request on the MonerisUtil.Refund
                    // + Update billing request refund components
                    var aux = new Entity(billingRequest.LogicalName);
                    aux.Id = billingRequest.Id;
                    aux["oems_baseamounttocalculaterefund"] = baseAmount;
                    aux["oems_refundpercentage"] = percentage;
                    aux["oems_totalamounttorefund"] = refundAmount;
                    aux["oems_amountrefundedtodeferralcredit"] = totalToDeferralCredit;
                    aux["oems_amountrefundedtocustomer"] = totalToCustomer;
                    aux["oems_refundtransaction"] = billingRequest["oems_refundtransaction"];

                    if (isCreditCard)
                    {
                        if (isRefunded)
                        {
                            aux["oems_paymentstatus"] = new OptionSetValue(PaymentStatus.Refunded);
                            eventRegistration["oems_billingstatus"] = new OptionSetValue(BillingStatus.None);
                        }
                        else
                        {
                            aux["oems_paymentstatus"] = new OptionSetValue(PaymentStatus.RefundFailed);
                            eventRegistration["oems_billingstatus"] = new OptionSetValue(BillingStatus.RefundFailed);
                        }

                        context.Update(eventRegistration);
                    }
                    else
                    {
                        aux["oems_paymentstatus"] = new OptionSetValue(PaymentStatus.ChequeRefundPending);
                    }

                    // + Update billing request status
                    context.Detach(billingRequest);
                    context.Attach(aux);
                    context.UpdateObject(aux);
                    context.SaveChanges();
                }
            }
        }

        public void RefundManually(Guid accountId, Guid billingRequestId)
        {
            Entity billingRequest =
            (
                from br in context.CreateQuery("oems_billingrequest")
                where br.GetAttributeValue<Guid>("oems_billingrequestid") == billingRequestId
                select br
            ).Single();

            var paymentTransaction =
            (
                from request in context.CreateQuery("oems_cctransaction")
                where request.GetAttributeValue<Guid>("oems_cctransactionid") == billingRequest.GetAttributeValue<EntityReference>("oems_paymenttransaction").Id
                select request
            ).First();

            var errorMessage = string.Empty;
            var totalToDeferralCredit = billingRequest.GetAttributeValue<decimal>("oems_amountrefundedtodeferralcredit");
            var totalToCustomer = billingRequest.GetAttributeValue<decimal>("oems_amountrefundedtocustomer");
            var transactionId = paymentTransaction.GetAttributeValue<string>("oems_transactionnumber");
            var requestId = paymentTransaction.GetAttributeValue<string>("oems_orderid");
            var isRefunded = MonerisUtil.Refund(context, billingRequest, requestId, totalToCustomer, "Registration Cancelled", transactionId, out errorMessage);
            if (isRefunded && totalToDeferralCredit > 0)
            {
                // + Refund deferral into the account
                var account = BillingUtil.GetAccount(context, accountId);
                var adeferral = account.GetAttributeValue<decimal>("oems_deferralcreditamount");
                adeferral += totalToDeferralCredit;
                var auxAccount = new Entity(account.LogicalName);
                auxAccount.Id = account.Id;
                auxAccount["oems_deferralcreditamount"] = adeferral;
                context.UpdateObject(auxAccount);
            }

            // + Update billing request refund components
            var aux = new Entity(billingRequest.LogicalName);
            aux.Id = billingRequest.Id;
            aux["oems_refundtransaction"] = billingRequest["oems_refundtransaction"];
            if (isRefunded)
            {
                aux["oems_paymentstatus"] = PaymentStatus.Refunded;

                var auxEventReg = new Entity(eventRegistration.LogicalName);
                auxEventReg.Id = eventRegistration.Id;
                eventRegistration["oems_billingstatus"] = new OptionSetValue(BillingStatus.None);
            }

            // + Update billing request status
            context.Detach(billingRequest);
            context.Attach(aux);
            context.UpdateObject(aux);
            context.SaveChanges();
        }

        public override string ToString()
        {
            return string.Format("TicketPrice: {0}, TicketApplyHst: {1}, AdditionalTickets: {2}, HasTicketSection: {3}, AccomodationDays: {4}, AccomodationCost: {5}, AccomodationTax: {6}, EventCost: {7}, EventTax: {8}, TicketCost: {9}, TicketTax: {10}", ticketPrice, ticketApplyHst, additionalTickets, hasTicketSection, accomodationDays, accomodationCost, accomodationTax, eventCost, eventTax, ticketCost, ticketTax);
        }

    }
}

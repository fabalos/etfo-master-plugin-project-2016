﻿using System;
using System.Linq;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Sdk;
using Moneris;
using System.Collections.Generic;
using Shared.Utilities;

namespace Omniware.ETFO.Plugin.Billing
{
    static internal class MonerisUtil
    {
        #region Moneris Response codes dictionary
        static Dictionary<int, Tuple<bool, string>> ResponseCodes = new Dictionary<int, Tuple<bool, string>>
        {
            //Approved
            { 0, new Tuple<bool, string>(true, "Approved")},
            { 1, new Tuple<bool, string>(true, "Approved")},
            { 2, new Tuple<bool, string>(true, "Approved")},
            { 3, new Tuple<bool, string>(true, "Approved")},
            { 4, new Tuple<bool, string>(true, "Approved")},
            { 5, new Tuple<bool, string>(true, "Approved")},
            { 6, new Tuple<bool, string>(true, "Approved")},
            { 7, new Tuple<bool, string>(true, "Approved")},
            { 8, new Tuple<bool, string>(true, "Approved")},
            { 9, new Tuple<bool, string>(true, "Approved")},
            { 23, new Tuple<bool, string>(true, "Approved")},
            { 24, new Tuple<bool, string>(true, "Approved")},
            { 25, new Tuple<bool, string>(true, "Approved")},
            { 26, new Tuple<bool, string>(true, "Approved")},
            { 27, new Tuple<bool, string>(true, "Approved")},
            { 28, new Tuple<bool, string>(true, "Approved")},
            { 29, new Tuple<bool, string>(true, "Approved")},
            //Declined Response Codes
            { 50, new Tuple<bool, string>(false, "Decline")},
            { 51, new Tuple<bool, string>(false, "Expired Card")},
            { 52, new Tuple<bool, string>(false, "PIN Retries Exceeded")},
            { 53, new Tuple<bool, string>(false, "No Sharing")},
            { 54, new Tuple<bool, string>(false, "No Security Module")},
            { 55, new Tuple<bool, string>(false, "Invalid Transaction")},
            { 56, new Tuple<bool, string>(false, "No Support")},
            { 57, new Tuple<bool, string>(false, "Lost Or Stolen Card")},
            { 58, new Tuple<bool, string>(false, "Invalid Status")},
            { 59, new Tuple<bool, string>(false, "Restricted Card")},
            { 60, new Tuple<bool, string>(false, "No Chequing/Saving Account")},
            { 61, new Tuple<bool, string>(false, "No PBF")},
            { 62, new Tuple<bool, string>(false, "PBF Update Error")},
            { 63, new Tuple<bool, string>(false, "Invalid Authorization Type")},
            { 64, new Tuple<bool, string>(false, "Bad TRACK 2")},
            { 65, new Tuple<bool, string>(false, "Adjustment Not Allowed")},
            { 66, new Tuple<bool, string>(false, "Invalid Credit Card Advance")},
            { 67, new Tuple<bool, string>(false, "Invalid Transaction Date")},
            { 68, new Tuple<bool, string>(false, "PTLF Error")},
            { 69, new Tuple<bool, string>(false, "Bad Message Error")},
            { 70, new Tuple<bool, string>(false, "No IDF")},
            { 71, new Tuple<bool, string>(false, "Invalid Route Authorization")},
            { 72, new Tuple<bool, string>(false, "Card On National NEG File")},
            { 73, new Tuple<bool, string>(false, "Invalid Route Service")},
            { 74, new Tuple<bool, string>(false, "Unable To Authorize")},
            { 75, new Tuple<bool, string>(false, "Invalid PAN Length")},
            { 76, new Tuple<bool, string>(false, "Low Funds")},
            { 77, new Tuple<bool, string>(false, "Pre-Auth Full")},
            { 78, new Tuple<bool, string>(false, "Duplicate Transaction")},
            { 79, new Tuple<bool, string>(false, "Maximum Online Refund")},
            { 80, new Tuple<bool, string>(false, "Maximum Offline Refund")},
            { 81, new Tuple<bool, string>(false, "Maximum Credit Per Refund")},
            { 82, new Tuple<bool, string>(false, "Number Of Times Used")},
            { 83, new Tuple<bool, string>(false, "Maximum Refund Credit")},
            { 84, new Tuple<bool, string>(false, "Duplicate Transaction -")},
            { 85, new Tuple<bool, string>(false, "Inquiry Not Allowed")},
            { 86, new Tuple<bool, string>(false, "Over Floor Limit")},
            { 87, new Tuple<bool, string>(false, "Maximum Number Of Refund")},
            { 88, new Tuple<bool, string>(false, "Place Call")},
            { 89, new Tuple<bool, string>(false, "CAF Status Inactive Or")},
            { 90, new Tuple<bool, string>(false, "Referral File Full")},
            { 91, new Tuple<bool, string>(false, "NEG File Problem")},
            { 92, new Tuple<bool, string>(false, "Advance Less Than Minimum")},
            { 93, new Tuple<bool, string>(false, "Delinquent")},
            { 94, new Tuple<bool, string>(false, "Over Table Limit")},
            { 95, new Tuple<bool, string>(false, "Amount Over Maximum")},
            { 96, new Tuple<bool, string>(false, "PIN Required")},
            { 97, new Tuple<bool, string>(false, "Mod 10 Check Failure")},
            { 98, new Tuple<bool, string>(false, "Force Post")},
            { 99, new Tuple<bool, string>(false, "Bad PBF")},
            { 100, new Tuple<bool, string>(false, "Unable To Process")},
            { 101, new Tuple<bool, string>(false, "Place Call")},
            { 102, new Tuple<bool, string>(false, "System Problem")},
            { 103, new Tuple<bool, string>(false, "NEG File Problem")},
            { 104, new Tuple<bool, string>(false, "CAF Problem")},
            { 105, new Tuple<bool, string>(false, "Card Not Supported")},
            { 106, new Tuple<bool, string>(false, "Amount Over Maximum")},
            { 107, new Tuple<bool, string>(false, "Over Daily Limit")},
            { 108, new Tuple<bool, string>(false, "CAF Problem")},
            { 109, new Tuple<bool, string>(false, "Advance Less Than Minimum")},
            { 110, new Tuple<bool, string>(false, "Number Of Times Used Exceeded")},
            { 111, new Tuple<bool, string>(false, "Delinquent")},
            { 112, new Tuple<bool, string>(false, "Over Table Limit")},
            { 113, new Tuple<bool, string>(false, "Time-out")},
            { 115, new Tuple<bool, string>(false, "PTLF Error")},
            { 121, new Tuple<bool, string>(false, "Administration File Problem")},
            { 122, new Tuple<bool, string>(false, "Unable To Validate PIN:")},
            //System Error
            { 150, new Tuple<bool, string>(false, "Merchant Not On File")},
            { 200, new Tuple<bool, string>(false, "Invalid Account")},
            { 201, new Tuple<bool, string>(false, "Incorrect PIN")},
            { 202, new Tuple<bool, string>(false, "Advance Less Than Minimum")},
            { 203, new Tuple<bool, string>(false, "Administrative Card Needed")},
            { 204, new Tuple<bool, string>(false, "Amount Over Maximum")},
            { 205, new Tuple<bool, string>(false, "Invalid Advance Amount")},
            { 206, new Tuple<bool, string>(false, "CAF Not Found")},
            { 207, new Tuple<bool, string>(false, "Invalid Transaction Date")},
            { 208, new Tuple<bool, string>(false, "Invalid Expiration Date")},
            { 209, new Tuple<bool, string>(false, "Invalid Transaction Code")},
            { 210, new Tuple<bool, string>(false, "PIN Key Sync Error")},
            { 212, new Tuple<bool, string>(false, "Destination Not Available")},
            { 251, new Tuple<bool, string>(false, "Error On Cash Amount")},
            { 252, new Tuple<bool, string>(false, "Debit Not Supported")},
            //AMEX Error
            { 426, new Tuple<bool, string>(false, "AMEX - Denial 12")},
            { 427, new Tuple<bool, string>(false, "AMEX - Invalid Merchant")},
            { 429, new Tuple<bool, string>(false, "AMEX - Account Error")},
            { 430, new Tuple<bool, string>(false, "AMEX - Expired Card")},
            { 431, new Tuple<bool, string>(false, "AMEX - Call Amex")},
            { 434, new Tuple<bool, string>(false, "AMEX - Call 03")},
            { 435, new Tuple<bool, string>(false, "AMEX - System Down")},
            { 436, new Tuple<bool, string>(false, "AMEX - Call 05")},
            { 437, new Tuple<bool, string>(false, "AMEX - Declined, Call")},
            { 438, new Tuple<bool, string>(false, "AMEX - Declined, Call")},
            { 439, new Tuple<bool, string>(false, "AMEX - Service Error")},
            { 440, new Tuple<bool, string>(false, "AMEX - Call Amex")},
            { 441, new Tuple<bool, string>(false, "AMEX - Amount Error")},
            //Credit Card Response
            { 475, new Tuple<bool, string>(false, "CREDIT CARD - Invalid Expiration Date")},
            { 476, new Tuple<bool, string>(false, "CREDIT CARD - CREDIT CARD - Invalid Transaction, Rejected")},
            { 477, new Tuple<bool, string>(false, "CREDIT CARD - Refer Call")},
            { 478, new Tuple<bool, string>(false, "CREDIT CARD - Decline, Pick Up Card, Call")},
            { 479, new Tuple<bool, string>(false, "CREDIT CARD - Decline, Pick Up Card")},
            { 480, new Tuple<bool, string>(false, "CREDIT CARD - Decline, Pick Up Card")},
            { 481, new Tuple<bool, string>(false, "CREDIT CARD - Decline")},
            { 482, new Tuple<bool, string>(false, "CREDIT CARD - Expired Card")},
            { 483, new Tuple<bool, string>(false, "CREDIT CARD - Refer")},
            { 484, new Tuple<bool, string>(false, "CREDIT CARD - Expired Card Refer­")},
            { 485, new Tuple<bool, string>(false, "CREDIT CARD - Not Authorized")},
            { 486, new Tuple<bool, string>(false, "CREDIT CARD - CVV")},
            { 487, new Tuple<bool, string>(false, "CREDIT CARD - Invalid CVV")},
            { 488, new Tuple<bool, string>(false, "CREDIT CARD - Invalid CVV")},
            { 489, new Tuple<bool, string>(false, "CREDIT CARD - Invalid CVV")},
            { 490, new Tuple<bool, string>(false, "CREDIT CARD - Invalid CVV")},
            //System Decline
            { 800, new Tuple<bool, string>(false, "Bad Format")},
            { 801, new Tuple<bool, string>(false, "Bad Data")},
            { 802, new Tuple<bool, string>(false, "Invalid Clerk ID")},
            { 809, new Tuple<bool, string>(false, "Bad Close")},
            { 810, new Tuple<bool, string>(false, "System Timeout - No response from Moneris Host")},
            { 811, new Tuple<bool, string>(false, "System Error")},
            { 821, new Tuple<bool, string>(false, "Bad Response Length")},
            { 877, new Tuple<bool, string>(false, "Invalid PIN Block")},
            { 878, new Tuple<bool, string>(false, "PIN Length Error")},
            { 880, new Tuple<bool, string>(false, "Final Response Packet of a Multi Packet Transaction")},
            { 881, new Tuple<bool, string>(false, "Continuation Response Packet of a Multi Packet Transaction")},
            { 882, new Tuple<bool, string>(false, "Download Aborted")},
            { 889, new Tuple<bool, string>(false, "MAC Key Sync Error")},
            { 898, new Tuple<bool, string>(false, "Bad MAC Value")},
            { 899, new Tuple<bool, string>(false, "Bad Sequence number ­ Terminal")},
            { 900, new Tuple<bool, string>(false, "Capture - PIN Tries Exceeded")},
            { 901, new Tuple<bool, string>(false, "Capture - Expired Card")},
            { 902, new Tuple<bool, string>(false, "Capture - NEG Capture")},
            { 903, new Tuple<bool, string>(false, "Capture - CAF Status 3")},
            { 904, new Tuple<bool, string>(false, "Capture - Advance less than Minimum")},
            { 905, new Tuple<bool, string>(false, "Capture - Num Times Used")},
            { 906, new Tuple<bool, string>(false, "Capture - Delinquent")},
            { 907, new Tuple<bool, string>(false, "Capture - Over Limit Table")},
            { 908, new Tuple<bool, string>(false, "Capture - Amount Over Maximum")},
            { 909, new Tuple<bool, string>(false, "Capture - Capture")},
            //Admin Declined
            { 960, new Tuple<bool, string>(false, "Initialization Failure - No Match on Merchant ID")},
            { 961, new Tuple<bool, string>(false, "Initialization Failure - No Match on PINPad")},
            { 962, new Tuple<bool, string>(false, "Initialization Failure - No Match on Printer ID")},
            { 963, new Tuple<bool, string>(false, "Initialization Failure - No Match on Poll Code")},
            { 964, new Tuple<bool, string>(false, "Initialization Failure - No Match on Concentrator ID")},
            { 965, new Tuple<bool, string>(false, "Invalid Software Version Number")},
            { 966, new Tuple<bool, string>(false, "Duplicate Terminal Name")},
            { 970, new Tuple<bool, string>(false, "Terminal/Clerk Table Full")},
            { 983, new Tuple<bool, string>(false, "Clerk Totals Unavailable: selected Clerk IDs do not exist or have zero totals")},
            { 989, new Tuple<bool, string>(false, "MAC Error on Transaction 95")},
            //Admin Declined
            { 990, new Tuple<bool, string>(false, "Chip Card declines a host")},
            { 991, new Tuple<bool, string>(false, "Chip Card removed before ICC" )}
        };
        #endregion

        public static bool Refund(CrmOrganizationServiceContext context, Entity billingRequest, string orderId, decimal amount, string description, string transactionNumber, out string errorMessage)
        {
            errorMessage = string.Empty;
            var configuration =
            (
                from c in context.CreateQuery("oems_configuration")
                select c
            ).FirstOrDefault();

            if (configuration == null)
            {
                throw new OEMSPluginException("No configuration record was found, refund was not applied");
            }

            if (!configuration.Contains("oems_monerishost") || !configuration.Contains("oems_monerisstoreid") || !configuration.Contains("oems_monerisapitoken") || !configuration.Contains("oems_moneriscrypt"))
            {
                throw new OEMSPluginException("Refund operation requires Moneris host, store ID, API token and crypt values. Please make sure this values are defined on the portal configuration");
            }

            //var host = "esqa.moneris.com";
            //var storeId = "store2";
            //var apiToken = "yesguy";
            //var crypt = "7";
            var host = configuration["oems_monerishost"].ToString();
            var storeId = configuration["oems_monerisstoreid"].ToString();
            var apiToken = configuration["oems_monerisapitoken"].ToString();
            var crypt = configuration["oems_moneriscrypt"].ToString();
            var orderIdPrefix = configuration.GetAttributeValue<string>("oems_monerisorderidprefix");
            if (String.IsNullOrEmpty(orderIdPrefix))
            {
                orderIdPrefix = "";
            }

            orderId = orderIdPrefix + orderId;

            var r = new Refund(orderId, amount.ToString("F"), transactionNumber, crypt);
            r.SetDynamicDescriptor(description);

            var mpgReq = new HttpsPostRequest(host, storeId, apiToken, r);

            try
            {
                var requestSuccess = false;
                var requestMessage = string.Empty;
                var receipt = mpgReq.GetReceipt();
                var isComplete = Convert.ToBoolean(receipt.GetComplete());

                //Gets the result code
                int code = -1;
                var responseCode = receipt.GetResponseCode();
                Int32.TryParse(responseCode, out code);

                if (!isComplete)
                {
                    requestMessage = "Payment service is not available to process the refund, please try again.";
                    errorMessage = requestMessage;
                }
                else
                {
                    if (code != null && code != -1)
                    {
                        requestSuccess = ResponseCodes[code].Item1;
                        requestMessage = ResponseCodes[code].Item2;
                    }
                    else
                    {
                        errorMessage = "Payment service is not available to process the refund (Invalid Code), please try again. Error code: " + responseCode;
                    }
                }

                //Save receipt values
                var refundTransaction = new Entity("oems_cctransaction");
                refundTransaction["oems_transactionamount"] = requestSuccess ? Convert.ToDecimal(receipt.GetTransAmount()) : (Decimal?)null;
                refundTransaction["oems_transactionnumber"] = receipt.GetTxnNumber();
                refundTransaction["oems_receiptid"] = receipt.GetReceiptId();
                refundTransaction["oems_transactiontype"] = receipt.GetTransType();
                refundTransaction["oems_referencenumber"] = receipt.GetReferenceNum();
                refundTransaction["oems_responsecode"] = responseCode; // receipt.GetResponseCode();
                refundTransaction["oems_isocode"] = receipt.GetISO();
                refundTransaction["oems_banktotals"] = receipt.GetBankTotals();
                refundTransaction["oems_message"] = requestMessage; // receipt.GetMessage();
                refundTransaction["oems_authcode"] = receipt.GetAuthCode();
                refundTransaction["oems_complete"] = receipt.GetComplete();
                refundTransaction["oems_transactiondate"] = receipt.GetTransDate();
                refundTransaction["oems_transactiontime"] = receipt.GetTransTime();
                refundTransaction["oems_ticket"] = receipt.GetTicket();
                refundTransaction["oems_timedout"] = receipt.GetTimedOut();
                refundTransaction["oems_type"] = new OptionSetValue(CreditCartTransactionType.REFUND_TRANSACTION);
                refundTransaction["oems_paymentstatus"] = requestSuccess ? new OptionSetValue(PaymentStatus.Refunded) : new OptionSetValue(PaymentStatus.RefundFailed);

                var refundTransactionId = context.Create(refundTransaction);

                //Add the new transaction to the refund list
                CrmTools crmTools = new CrmTools(context);
                crmTools.associateRelatedEntityToEntity
                (
                    new EntityReference()
                    {
                        LogicalName = refundTransaction.LogicalName,
                        Id = refundTransactionId
                    },
                    new EntityReference()
                    {
                        LogicalName = billingRequest.LogicalName,
                        Id = billingRequest.Id
                    },
                    "oems_oems_billingrequest_refund_transaction"
                );

                //Reference the new CC Transaction on the given billing request but dont update it yet on CRM
                billingRequest["oems_refundtransaction"] = new EntityReference(billingRequest.LogicalName, refundTransactionId);

                return requestSuccess;
            }
            catch (Exception e)
            {
                return false;
            }
        }
    }
}
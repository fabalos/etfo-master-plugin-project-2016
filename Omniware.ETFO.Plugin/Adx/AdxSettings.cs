﻿using Shared.Utilities;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETFOPlugin.Adx
{
    public class AdxSettings
    {
        IOrganizationService service;

        public AdxSettings(IOrganizationService _service){
            service = _service;
        }

        public string getSetting(string settingName){
            string ret = null;

  /*          QueryByAttribute query = new QueryByAttribute("adx_setting") { ColumnSet = new ColumnSet(true) };

            query.Attributes.Add("adx_name");
            query.Values.Add(settingName);

            Entity adx_setting = service.RetrieveMultiple(query).Entities.FirstOrDefault();*/

            CrmTools crmTools = new CrmTools(service);

            Entity adx_setting = crmTools.getEntity("adx_setting", "adx_name", settingName);

            ret = (string)adx_setting["adx_value"];

            return ret;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Omniware.ETFO.Plugin.DataWork
{
    public enum ValueFormatter
    {
        DATE = 0,
        DATETIME = 1
    }
 
    public class NameValuePair
    {
        public string name { get; set; }
        public string value { get; set; }
    }

    public class MasterItem
    {
        public string id { get; set; }
        public string name { get; set; }
    }

    public class ChildItem
    {
        public string id { get; set; }
        public string name { get; set; }
        public string parent { get; set; }
    }

    public class ElementOptions
    {
        public string label { get; set; }
        public string leftLabel { get; set; }
        public string placeholderText { get; set; }
        public bool? required { get; set; }
        public string enablesSection { get; set; }
        public bool? disabled { get; set; }
        public string backgroundColor {get;set;}

        public string title { get; set; }
        public bool? collapse { get; set; }
        public bool? open { get; set; }
        public bool? selected { get; set; }
        public bool? displayGroupHeaders { get; set; }
        public bool? displayColumns{ get; set; }

        public int? maxlength {get;set;}
        public int? minlength { get; set; }
        public int? width { get; set; }
        public bool? inline { get; set; }

        public int? min { get; set; }
        public int? max { get; set; }
        public int? step { get; set; }
        public int? minDocs { get; set; }
        public bool? displayAsRadios { get; set; }

        public Label[] labelColumns {get;set;}
        public FormElement newRowTemplate { get; set; }
        
    }

    public class Label
    {
      public string label {get;set;}
    }

    public class OptionValue
    {
        public string label { get; set; }
        public string value { get; set; }
        public string idValue { get; set; }
        public string selected  { get; set; }
        public Label[] labelColumns { get; set; }
        public string group {get;set;}
        public Object reference { get; set; }
        public string required { get; set; }
        public string type { get; set; }
    }

    public class FormElement
    {

        private bool isLoaded = false;

        public string id { get; set; }
        public string name { get; set; }
        public string type { get; set; }
        public int component { get; set; }

        public int? valueFormatter { get; set; }
        
        public FormElement[] children { get; set; }
        
        public ElementOptions options { get; set; }
        public string initialPath { get; set; }
        public string sourcePath { get; set; }
        public string targetPath { get; set; }
        public string deletePath { get; set; }
        public string oneToOneParentLookupPath { get; set; }


        public string[] preferredSchoolboardIds {get;set;}
        public string[] preferredSchoolIds { get; set; }
        public string[] preferredLocalIds { get; set; }


        //Data
        public string value { get; set; }
        public OptionValue[] selectValues { get; set; }
        public OptionValue[] checkboxValues { get; set; }
        public OptionValue[] radioValues { get; set; }

        private FormElement parentElement = null;
        public FormElement GetParentElement()
        {
          return parentElement;
        }
        
        public void AddChild(FormElement child)
        {
           if(children == null) children = new FormElement[0];

           List<FormElement> childrenList = new List<FormElement>(children);
           childrenList.Add(child);
           children = childrenList.ToArray();
        }

        public void ResetParents()
        {
           
           foreach(var c in children)
           {
              c.parentElement = this;
              c.ResetParents();
           }
           if(options.newRowTemplate != null) 
           {
             options.newRowTemplate.parentElement = this;
             options.newRowTemplate.ResetParents();
           }
        }

        public FormElement FindChildByTitle(string title)
        {
           FormElement result = null;
           foreach(var c in children)
           {
               if (c.options.title == title) result = c;
               else result = c.FindChildByTitle(title);
              if(result != null) break;
             
           }
           return result;
        }

        public FormElement FindChildByLabel(string label)
        {
            FormElement result = null;
            foreach (var c in children)
            {
                if (c.options.label == label) result = c;
                else result = c.FindChildByLabel(label);
                if (result != null) break;

            }
            return result;
        }

        public FormElement FindChildBySourcePath(string path)
        {
            FormElement result = null;
            foreach (var c in children)
            {
                if (c.sourcePath == path) result = c;
                else result = c.FindChildBySourcePath(path);
                if (result != null) break;

            }
            return result;
        }

        public bool DeleteChildByTitle(string title)
        {
            List<FormElement> chList = new List<FormElement>(children);
            foreach (var c in chList)
            {
                if (c.options.title == title)
                {
                   chList.Remove(c);
                   children = chList.ToArray();
                   return true;
                }
                else 
                {
                    if (c.DeleteChildByTitle(title)) return true;
                }
            }
            return false;
            
        }

        public bool DeleteChildByName(string name)
        {
            List<FormElement> chList = new List<FormElement>(children);
            foreach (var c in chList)
            {
                if (c.name == name)
                {
                    chList.Remove(c);
                    children = chList.ToArray();
                    return true;
                }
            }
            return false;

        }

        public bool GetIsLoaded()
        {
           return isLoaded;
        }

        public void SetIsLoaded(bool value)
        {
            isLoaded = value;
        }

        public OptionValue[] CloneOptionValues(OptionValue[] options)
        { 
           if(options == null) return null;
           OptionValue[] result = null;
           if(options.Length > 0)
           {
              result = new OptionValue[options.Length];
              for(int i = 0; i < options.Length; i++)
              {
                 OptionValue ov = new OptionValue();
                 ov.label = options[i].label;
                 ov.selected = options[i].selected;
                 ov.value = options[i].value;
                 ov.group = options[i].group;
                 if(options[i].labelColumns != null)
                 {
                    ov.labelColumns = new Label[options[i].labelColumns.Length];
                    for(int l=0; l < options[i].labelColumns.Length; l++)
                    {
                        ov.labelColumns[l] = new Label()
                                        {
                                           label = ov.labelColumns[l].label
                                        };
                                   
                    }
                 }
                   
                 result[i] = ov;
              }
           }
           return result;
        }

        public FormElement Clone()
        {
           FormElement result = new FormElement();

           result.id = id;
           result.name = name;
           result.type = type;
           result.value = value;
           result.valueFormatter = valueFormatter;
           result.options = new ElementOptions();
           result.options.disabled = options.disabled;
           result.options.title = options.title;
           result.options.open = options.open;
           result.options.label = options.label;

           result.options.placeholderText = options.placeholderText;
           result.options.required = options.required;
           result.options.enablesSection = options.enablesSection;

           result.options.title = options.title;
           result.options.collapse = options.collapse;
           result.options.open = options.open;
           result.options.selected = options.selected;
           result.options.newRowTemplate = options.newRowTemplate;
           result.options.displayColumns = options.displayColumns;
           result.options.displayGroupHeaders = options.displayGroupHeaders;

           result.options.required = options.required;
           result.options.minlength = options.minlength;
           result.options.maxlength = options.maxlength;
           result.options.inline = options.inline;

           result.options.min = options.min;
           result.options.max = options.max;
           result.options.step = options.step;


           result.component = component;


           if (options.labelColumns != null)
           {
               result.options.labelColumns = new Label[options.labelColumns.Length];
               for (int l = 0; l < options.labelColumns.Length; l++)
               {
                   result.options.labelColumns[l] = new Label()
                   {
                       label = options.labelColumns[l].label
                   };

               }
           }

           result.children = new FormElement[children.Length];
           for(int i = 0; i < children.Length; i++)
           {
              result.children[i] = children[i].Clone();
              result.children[i].parentElement = this;
           }

           result.sourcePath = sourcePath;
           result.targetPath = targetPath;
           result.deletePath = deletePath;
           result.oneToOneParentLookupPath = oneToOneParentLookupPath;
           result.selectValues = CloneOptionValues(selectValues);
           result.checkboxValues = CloneOptionValues(checkboxValues);
           result.radioValues = CloneOptionValues(radioValues);
           

           return result;
           //result.options.newRowTemplate - not copying template for clonses (row population - no need for templates)
        }

        public FormElement(string name, string type, string title, string value)
        {
            this.name = name;
            this.type = type;
            
            this.value = value;
            options = new ElementOptions();
            options.title = title;
            options.open = true;

            
        }

        public FormElement()
        {
            options = new ElementOptions();
            options.open = true;
        }

        public FormElement FindByName(string name)
        {
            if (this.name == name) return this;
            foreach (var ch in children)
            {
                FormElement result = ch.FindByName(name);
                if (result != null && result.name == name) return result;
            }
            return null;
        }

        public FormElement FindById(string id)
        {
            if (this.id == id) return this;
            foreach (var ch in children)
            {
                FormElement result = ch.FindById(id);
                if (result != null && result.id == id) return result;
            }

            if(options.newRowTemplate != null) return options.newRowTemplate.FindById(id);
            return null;
        }
        
        public static string GuidToString(Guid id)
        {
           return id.ToString().Replace("{", "").Replace("}", "");
        }
    }

    public static class EncodeJson
    {

        public static string EncodeNonAsciiCharacters(string value)
        {
            StringBuilder sb = new StringBuilder();
            foreach (char c in value)
            {
                if (c > 127)
                {
                    // This character is too big for ASCII
                    string encodedValue = "\\u" + ((int)c).ToString("x4");
                    sb.Append(encodedValue);
                }
                else
                {
                    sb.Append(c);
                }
            }
            return sb.ToString();
        }

        public static string DecodeEncodedNonAsciiCharacters(string value)
        {
            return Regex.Replace(
                value,
                @"\\u(?<Value>[a-zA-Z0-9]{4})",
                m =>
                {
                    return ((char)int.Parse(m.Groups["Value"].Value, NumberStyles.HexNumber)).ToString();
                });
        }
    }
}

﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using Omniware.ETFO.Plugin.Shared;
using Xrm;
using Omniware.ETFO.Plugin;
using System.Collections;
using log4net;

namespace Omniware.ETFO.Plugin.DataWork
{
    public class MetaData
    {
        static readonly ILog Log = LogHelper.GetLogger(typeof(MetaData));

        static int ExpiryMinutes = 5;  // todo can be made configurable
        static DateTime LastLoaded;

        OmniContext context;


        // cache collection of Crm entities by type ie Category etc. remember when it was loaded
        static readonly ConcurrentDictionary<string, EntityMetadata> cache
            = new ConcurrentDictionary<string, EntityMetadata>();



        public MetaData(OmniContext context)
        {
            this.context = context;
        }


        EntityMetadata LoadMetadata(string logicalName)
        {
            var retrieveEntityRequest = new RetrieveEntityRequest
            {
                EntityFilters = EntityFilters.All,
                LogicalName = logicalName
            };
            var retrieveEntityResponse = (RetrieveEntityResponse)context.Execute(retrieveEntityRequest);
            return  retrieveEntityResponse.EntityMetadata;
        }


        public  EntityMetadata GetEntityMetadata(string logicalName)
        {
            var sw = new Stopwatch(); sw.Start();
            if (DateTime.Now > LastLoaded.AddMinutes(ExpiryMinutes))
            {
                cache.Clear();
                LastLoaded = DateTime.Now;
                Log.Debug("MetaData Cache cleared");
            }
            var entityMetadata = cache.GetOrAdd(logicalName, s => LoadMetadata(s));

            sw.Stop();
            if( sw.ElapsedMilliseconds > 0) Log.DebugFormat("GetEntityMetadata {0} {1} msec",  logicalName, sw.ElapsedMilliseconds );
            return entityMetadata;
        }


        public  AttributeMetadata GetAttributeMetadata(string entityLogicalName, string attributeName)
        {
            EntityMetadata em = GetEntityMetadata(entityLogicalName);
            
            if(em == null) RaiseError("Could not load '"+entityLogicalName+"' entity metadata");
            
            foreach(var a in em.Attributes)
            {
                if(a.LogicalName == attributeName) return a;
            }
            RaiseError("Could not load '" + attributeName + "' attribute metadata");
            return null;
        }

        public  RelationshipMetadataBase GetRelationshipMetadata(string entityLogicalName, string relationshipName)
        {
            EntityMetadata em = GetEntityMetadata(entityLogicalName);

            if (em == null) RaiseError("Could not load '" + entityLogicalName + "' entity metadata");

            foreach (var r in em.ManyToManyRelationships)
            {
                if (r.SchemaName == relationshipName) return r;
            }

            foreach (var r in em.ManyToOneRelationships)
            {
                if (r.SchemaName == relationshipName) return r;
            }

            foreach (var r in em.OneToManyRelationships)
            {
                if (r.SchemaName == relationshipName) return r;
            }

            RaiseError("Could not load '" + relationshipName + "' relationship metadata");
            return null;
        }

        public static void RaiseError(string msg)
        {
            throw new OEMSPluginException(msg);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Omniware.ETFO.Plugin.Billing;
using Omniware.ETFO.Plugin.Shared;
using Xrm;
using Omniware.ETFO.Plugin;
using System.Collections;
using Microsoft.Xrm.Sdk.Metadata;
using Microsoft.Xrm.Sdk.Messages;
using Newtonsoft.Json;
using Microsoft.Xrm.Sdk.Query;
using log4net;
using System.Reflection;
using Omniware.ETFO.Plugin.FormComponents;

namespace Omniware.ETFO.Plugin.DataWork
{


    public class EntityCache
    {
        public Entity Entity { get; set; }
        public Entity Updated { get; set; }
        public string Path { get; set; }
        public Entity Root { get; set; }
        public bool Disassociate { get; set; }
    }



    public class AssociateData
    {

        public EntityCache Target { get; set; }
        public string RelationshipName { get; set; }


        public List<EntityCache> References = new List<EntityCache>();
    }

    public class ExistingRelationship
    {
        public EntityReference Parent { get; set; }
        public EntityReference Child { get; set; }
        public RelationshipMetadataBase Relationship { get; set; }
    }


    public delegate void OnCreateRecord(Entity entity, DataAccess da);




    public class DataAccess
    {
        ILog Log = LogHelper.GetLogger(typeof(DataAccess));

        const string PERSONALACCOMMODATIONREQUIRED = "Personal Accommodation Required";
        const string ACCOMMODATIONREQUIRED = "Accommodation Required";
        const string RADIONULL = "radio-null";

        //TODO
        //Boolean and picklists - how to map CRM values to the proper selections

        //Hashtable cache = null;
        //List<Entity> updatedCache = null;
        List<AssociateData> associateRequests = null;
        List<EntityCache> entityCache = null;
        List<Entity> referenceList = null;
        List<ExistingRelationship> existingRelationships = null;
        List<FormComponents.IFormElement> formComponents = new List<FormComponents.IFormElement>();

        int rowIndex = -1;

        public Entity RootEntity { get; set; }
        public OnCreateRecord OnCreateRecord;

        Hashtable newRows = new Hashtable();

        public MetaData metaData = null;


        public OmniContext context = null;


        List<Tuple<string, int>> FormElementBindingCache;

        public DataAccess(OmniContext context)
        {
            this.context = context;
            Init();
            var instances = from t in Assembly.GetExecutingAssembly().GetTypes()
                            where t.GetInterfaces().Contains(typeof(IFormElement))
                                     && t.GetConstructor(Type.EmptyTypes) != null
                            select Activator.CreateInstance(t) as IFormElement;

            foreach (var instance in instances)
            {
                formComponents.Add(instance);
            }

            LoadBindingElementsCache();
        }

        void LoadBindingElementsCache()
        {
            FormElementBindingCache = new List<Tuple<string, int>>() { new Tuple<string, int>("Username", BindingType.InitialPath) };

        }
        public void Init()
        {
            metaData = new MetaData(context);
            entityCache = new List<EntityCache>();
            referenceList = new List<Entity>();
            associateRequests = null;
        }

        public Entity FindInReferences(Guid id)
        {
            return referenceList.Find(x => id == x.Id);
        }

        protected EntityCache FindInCache(string path)
        {
            return entityCache.Find(e => e.Path == path);
        }

        protected EntityCache FindInCache(Guid id)
        {
            return entityCache.Find(e => e.Entity.Id != null && e.Entity.Id == id);
        }

        protected EntityCache AddToCache(Entity entity, string path, bool setUpdated = true)
        {
            EntityCache result = FindInCache(path);
            if (result == null)
            {
                result = new EntityCache();
                result.Path = path;

                result.Entity = entity;
                if (setUpdated)
                {
                    if (result.Entity.Id != Guid.Empty && !entity.Contains("createdflag"))
                    {
                        result.Updated = new Entity(entity.LogicalName);
                        result.Updated.Id = entity.Id;

                    }
                    else
                    {
                        result.Updated = result.Entity;
                    }

                }
                entityCache.Add(result);
            }
            return result;
        }

        protected void AddAssociateData(EntityCache entityItem, string relationshipName, EntityCache referenceItem, bool disassociate)
        {
            if (associateRequests == null) associateRequests = new List<AssociateData>();
            AssociateData ad = associateRequests.Find(x => x.Target.Entity.Id == entityItem.Entity.Id && x.RelationshipName == relationshipName);
            if (ad == null)
            {
                ad = new AssociateData();
                ad.Target = entityItem;
                ad.RelationshipName = relationshipName;
                associateRequests.Add(ad);
            }
            bool found = false;
            referenceItem.Disassociate = disassociate;
            foreach (var r in ad.References)
            {
                if (r.Entity.Id == referenceItem.Entity.Id)
                {
                    found = true;
                    break;
                }
            }
            if (!found)
            {
                ad.References.Add(referenceItem);
            }

        }




        public void MergeCacheItems(Guid id1, Guid id2)
        {
            return;
            //the code below does not really work
            //return
            EntityCache source = FindInCache(id1);
            EntityCache target = FindInCache(id2);
            if (target.Entity.Contains("createdflag"))
            {

                EntityCache tmp = target;
                target = source;
                source = tmp;

            }

            foreach (var a in source.Updated.Attributes)
            {
                if (!target.Updated.Contains(a.Key) && a.Key != "createdflag")
                {
                    SetAttributeValue(target, a.Key, a.Value);
                }
            }
            source.Updated = target.Updated;
            source.Entity = target.Entity;

        }

        public void SaveUpdateCache()
        {
            string debugInfo = "";
            try
            {
                List<Entity> createdEntities = new List<Entity>();
                debugInfo += "start;";
                Entity root = null;
                Guid releaseTimeId = Guid.Empty;
                Guid accommodationReqId = Guid.Empty;
                bool accommodationRequired = false;

                foreach (var ei in entityCache)
                {
                    //Not only the createdflag
                    if (ei.Entity.LogicalName == "oems_releasetimerequest" && ei.Updated.Attributes.Count < 2)
                    {
                        releaseTimeId = ei.Entity.Id;
                        continue;
                    }

                    if (ei.Entity.LogicalName == "oems_eventregistration")
                    {
                        root = ei.Entity;
                        accommodationRequired = ei.Updated.GetAttributeValue<bool>("oems_accommodationrequiredflag");
                    }

                    //if (ei.Entity.Contains("processed")) continue;//there can be merged "duplicates"
                    if (ei.Entity.Contains("createdflag")) ei.Entity.Attributes.Remove("createdflag");
                    debugInfo += "updating entity: " + ei.Entity.LogicalName + ";";
                    if (ei.Updated != null)
                    {
                        if (ei.Updated == ei.Entity)
                        {
                            if (ei.Entity.LogicalName == "oems_accommodationrequest")
                            {
                                if (!accommodationRequired)
                                {
                                    accommodationReqId = ei.Updated.Id;
                                    continue;
                                }

                                var request = BillingUtil.GetRequest(context, root.Id, "oems_accommodationrequest");
                                if (request != null)
                                {
                                    ei.Entity["oems_accommodationpricing"] = request["oems_accommodationpricing"];
                                }
                            }
                            else if (ei.Entity.LogicalName == "oems_caucusrequest")
                            {
                                ei.Entity["oems_event"] = root.GetAttributeValue<EntityReference>("oems_event");
                            }

                            ei.Updated.Id = context.Create(ei.Updated);
                            createdEntities.Add(ei.Updated);
                            debugInfo += "created;";
                        }
                        else
                        {
                            context.Update(ei.Updated);
                            debugInfo += "updated;";
                            foreach (var a in ei.Updated.Attributes)
                            {
                                ei.Entity[a.Key] = a.Value;
                            }

                        }
                    }
                }

                //return;
                if (associateRequests != null)
                {
                    foreach (var a in associateRequests)
                    {
                        if (a == null) debugInfo += "a = null;";
                        if (a.Target == null) debugInfo += "target = null;";
                        if (a.Target.Entity == null) debugInfo += "target.entity = null;";
                        if (metaData == null) debugInfo += "metaData = null;";
                        RelationshipMetadataBase rm = metaData.GetRelationshipMetadata(a.Target.Entity.LogicalName, a.RelationshipName);
                        if (rm == null) debugInfo += "relationship metadata = null;";
                        debugInfo += "before call;";
                        var existingReferences = GetRelatedRecords(a.Target.Entity, rm, null).ToList();
                        debugInfo += "after call;";
                        debugInfo += a.Target.Entity.LogicalName + ";";
                        int i = 0;
                        if (existingReferences != null)
                        {
                            while (i < existingReferences.Count())
                            {
                                Entity existingEnt = existingReferences[i];
                                bool found = false;
                                foreach (var r in a.References)
                                {
                                    if (a.Target.Entity.Id == existingEnt.Id && r.Disassociate != true)
                                    {
                                        found = true;
                                        a.References.Remove(r);
                                        break;
                                    }
                                }
                                if (found)
                                {
                                    existingReferences.Remove(existingEnt);
                                }
                                else i++;
                            }
                            if (existingReferences.Count > 0)
                            {
                                EntityReferenceCollection removeCollection = new EntityReferenceCollection();
                                foreach (var ea in existingReferences)
                                {
                                    removeCollection.Add(ea.ToEntityReference());
                                }
                                context.Disassociate(a.Target.Entity.LogicalName, a.Target.Entity.Id, new Relationship(a.RelationshipName), removeCollection);
                            }
                        }
                        var references = (from ei in a.References
                                          where ei.Disassociate != true
                                          select ei.Entity.ToEntityReference()).ToList();
                        debugInfo += "assocciating;";

                        //juan.bastidas - 20150424 - Tjis validation is broken so I ammended it a left a copy of the previous version in comments
                        //if (!references[0].Id.Equals(Guid.Empty))
                        if (references.Any() && !references.First().Id.Equals(Guid.Empty))
                        {
                            if (a.RelationshipName == "oems_accommodationrequest_to_singleroommedicalaccommodationrequest"
                                && accommodationReqId == Guid.Empty)
                            {
                                Entity accommodation = new Entity(a.Target.Entity.LogicalName);
                                accommodation.Id = a.Target.Entity.Id;
                                accommodation.Attributes["oems_singleroommedicalaccommodationrequest"] = references[0];
                                context.Update(accommodation);
                            }

                            if (a.RelationshipName == "oems_eventregistration_to_personalcareexemptionrequest")
                            {
                                Entity personalAccommodationRequest = (from e in context.CreateQuery("oems_personalaccommodationrequest")
                                                                       where (Guid)e["oems_eventregistration"] == a.Target.Entity.Id
                                                                       select e).FirstOrDefault();

                                Entity personalAccommodation = new Entity(personalAccommodationRequest.LogicalName);
                                personalAccommodation.Id = personalAccommodationRequest.Id;
                                personalAccommodation.Attributes["oems_personalcareexemptionrequest"] = references[0];
                                context.Update(personalAccommodation);

                                Entity personalCareReq = new Entity(references[0].LogicalName);
                                personalCareReq.Id = references[0].Id;
                                personalCareReq.Attributes["oems_personalaccommodationrequest"] = personalAccommodationRequest.ToEntityReference();
                                context.Update(personalCareReq);
                            }

                            if (releaseTimeId != Guid.Empty) references.Remove(new EntityReference("oems_releasetimerequest", releaseTimeId));
                            if (accommodationReqId != Guid.Empty)
                            {
                                switch (references[0].LogicalName)
                                {
                                    case "oems_accommodationrequest":
                                    case "oems_eventaccommodationschedule":
                                    case "oems_singleroommedicalaccommodationrequest":
                                        references = new List<EntityReference>();
                                        break;
                                }
                            }
                            if (references.Count > 0)
                            {
                                context.Associate(a.Target.Entity.LogicalName, a.Target.Entity.Id, new Relationship(a.RelationshipName), new EntityReferenceCollection(references));
                            }
                        }
                    }
                    if (OnCreateRecord != null)
                    {
                        foreach (var e in createdEntities)
                        {
                            var postCreateEntity = context.Retrieve(e.LogicalName, e.Id, new Microsoft.Xrm.Sdk.Query.ColumnSet(true));
                            OnCreateRecord(postCreateEntity, this);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error("Error saving updates: " + debugInfo + System.Environment.NewLine + ex.Message);
                throw ex;
            }
        }

        public void SetAttributeValue(EntityCache cacheItem, string attribute, object value)
        {
            if (cacheItem.Updated == null)
            {
                cacheItem.Updated = new Entity(cacheItem.Entity.LogicalName);
                cacheItem.Updated.Id = cacheItem.Entity.Id;
            }
            if (cacheItem.Updated.Contains(attribute)) cacheItem.Updated[attribute] = value;
            else cacheItem.Updated.Attributes.Add(attribute, value);
        }


        public IQueryable<Entity> GetRelatedRecords(Entity entity, RelationshipMetadataBase rm, string sortFieldName = null)
        {
            string relatedEntityName = null;
            return GetRelatedRecords(entity, rm, sortFieldName, ref relatedEntityName);
        }

        public IQueryable<Entity> GetRelatedRecords(Entity entity, RelationshipMetadataBase rm, string sortFieldName, ref string relatedEntityName)
        {
            var sw = new Stopwatch(); sw.Start();
            IQueryable<Entity> references = null;
            if (rm is OneToManyRelationshipMetadata)
            {
                OneToManyRelationshipMetadata otmrm = (OneToManyRelationshipMetadata)rm;
                /*
                if (!entity.Contains(otmrm.ReferencedAttribute))
                {
                    RaiseError(entity.LogicalName + " does not have '" + otmrm.ReferencedAttribute + "' attribute to expand the relationship: " + entity.LogicalName+" - rm.SchemaName");
                }
                 * */
                //RaiseError("Rererenced = '" + otmrm.ReferencedEntity + "." + otmrm.ReferencedAttribute + " Referencing = '" + otmrm.ReferencingEntity + "." + otmrm.ReferencingAttribute + "'");
                if (sortFieldName == null) sortFieldName = otmrm.ReferencingAttribute;
                relatedEntityName = otmrm.ReferencingEntity;

                //Log.Debug("Get Releated records => "+ relatedEntityName +"." + otmrm.ReferencingAttribute);

                if (relatedEntityName != "annotation")
                {
                    references = from r in context.CreateQuery(otmrm.ReferencingEntity)
                                 orderby r[sortFieldName]
                                 where (Guid)r[otmrm.ReferencingAttribute] == entity.Id && (int)r["statecode"] == 0
                                 select r;
                }
                else
                {
                    references = from r in context.CreateQuery(otmrm.ReferencingEntity)
                                 orderby r[sortFieldName]
                                 where (Guid)r[otmrm.ReferencingAttribute] == entity.Id
                                 select r;
                }


            }
            else if (rm is ManyToManyRelationshipMetadata)
            {
                ManyToManyRelationshipMetadata mtmrm = (ManyToManyRelationshipMetadata)rm;
                string currentEntityName = entity.LogicalName;
                string currentAttributeName = null;
                string referencedEntityName = null;
                string referencedAttributeName = null;

                //RaiseError("Relationship info: " + mtmrm.Entity1LogicalName + " : " + mtmrm.IntersectEntityName + " : " + mtmrm.Entity2LogicalName);


                if (mtmrm.Entity1LogicalName.ToLower() == currentEntityName.ToLower())
                {
                    referencedEntityName = mtmrm.Entity2LogicalName;
                    referencedAttributeName = mtmrm.Entity2IntersectAttribute;
                    currentAttributeName = mtmrm.Entity1IntersectAttribute;
                }
                else
                {
                    referencedEntityName = mtmrm.Entity1LogicalName;
                    referencedAttributeName = mtmrm.Entity1IntersectAttribute;
                    currentAttributeName = mtmrm.Entity2IntersectAttribute;

                }

                if (sortFieldName == null) sortFieldName = referencedAttributeName;


                relatedEntityName = referencedEntityName;

                //Log.Debug("Get Releated records => " + referencedEntityName + " via " + mtmrm.IntersectEntityName);
                references = from r in context.CreateQuery(referencedEntityName)
                             join ir in context.CreateQuery(mtmrm.IntersectEntityName) on r[referencedAttributeName] equals ir[referencedAttributeName]
                             orderby r[sortFieldName]
                             where (Guid)ir[currentAttributeName] == entity.Id
                             select r;
                /*
                if (entity.LogicalName.Contains("eventregi"))
                {
                    RaiseError(referencedEntityName + " : " + mtmrm.IntersectEntityName + " : " + referencedAttributeName + " : " + sortFieldName
                        + " : " + currentAttributeName);
                }*/

            }
            sw.Stop();
            if (sw.ElapsedMilliseconds > 1) Log.Debug("GetRelatedRecords: " + sw.ElapsedMilliseconds + " msec");
            return references;
        }


        public static string GetDateString(DateTime dt, bool dateOnly)
        {
            if (dateOnly) return dt.ToString("dd MMMM yyyy");
            else return dt.ToString("dd MMMM yyyy hh:mm");
        }

        public string GetStringAttributeValue(Entity e, string name, string defValue)
        {
            string result = defValue;

            int i = name.IndexOf("(");
            if (i > 0)
            {
                string functionName = name.Substring(0, i).Trim();
                name = name.Substring(i).Replace("(", "").Replace(")", "");
                string format = null;
                if (name.IndexOf(",") > 0)
                {
                    string[] parameters = name.Split(',');
                    name = parameters[0];
                    if (parameters.Length > 1)
                    {
                        format = parameters[1];
                    }

                }
                if (e.Contains(name) && e[name] != null)
                {
                    AttributeMetadata am = metaData.GetAttributeMetadata(e.LogicalName, name);

                    TimeZoneInfo timeZoneInfo;
                    DateTime dateTime;
                    timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");

                    if (functionName == "date" && format != null) //formatted date
                    {
                        format = format.Replace("&#44;", ",");
                        if (am.AttributeType == AttributeTypeCode.DateTime)
                        {
                            result = TimeZoneInfo.ConvertTime((DateTime)e[name], timeZoneInfo).ToString(format);
                        }
                    }
                    else //old logic
                    {
                        switch (functionName)
                        {
                            case "date":
                                if (am.AttributeType == AttributeTypeCode.DateTime)
                                {
                                    result = TimeZoneInfo.ConvertTime((DateTime)e[name], timeZoneInfo).ToShortDateString();
                                }
                                break;
                            case "dateTime":
                                if (am.AttributeType == AttributeTypeCode.DateTime)
                                {
                                    result = TimeZoneInfo.ConvertTime((DateTime)e[name], timeZoneInfo).ToString();
                                }
                                break;
                        }
                    }
                }
            }
            else if (e.Contains(name) && e[name] != null)
            {
                result = e[name].ToString();
            }



            return result;
        }

        public List<OptionValue> GetOptions(IQueryable<Entity> references, string displayFieldName, List<OptionValue> existingOptions, string lookupField, string groupField, string idElement = "", string elementName = "")
        {
            List<OptionValue> result = new List<OptionValue>();

            //Can't add statecode to linq query that already has joins etc - it gets confused, so will check in the loop
            string[] displayColumns = null;
            if (displayFieldName != null) displayColumns = displayFieldName.Split('-');
            foreach (var r in references)
            {
                Entity existing = FindInReferences(r.Id);
                if (existing == null) referenceList.Add(r);
                if (!r.Contains("statecode") || ((OptionSetValue)r["statecode"]).Value == 0)
                {
                    OptionValue existingOption = null;
                    if (existingOptions != null)
                        existingOption = existingOptions.Find(x => x.value == r.Id.ToString());

                    OptionValue newOption = new OptionValue()
                    {

                        value = lookupField == null ? r.Id.ToString() : ((EntityReference)r[lookupField]).Id.ToString(),
                        idValue = r.Id.ToString(),
                        selected = (existingOption != null).ToString().ToLower(),
                        reference = r
                    };

                    if (displayFieldName == null || displayColumns.Length == 1)
                    {
                        if (displayFieldName != null && r.Contains(displayFieldName))
                        {
                            if (r[displayFieldName].GetType().Name == "DateTime")
                            {
                                if (displayFieldName == "oems_accommodationdate")
                                {
                                    newOption.label = String.Format("{0:D}", Convert.ToDateTime(r[displayFieldName]));
                                } else
                                {
                                    newOption.label = Convert.ToDateTime(r[displayFieldName]).ToString("MMMM dd, yyyy");
                                }

                            }
                            else
                            {
                                newOption.label = r[displayFieldName].ToString();
                            }
                        }
                        else
                        {
                            newOption.label = r.Id.ToString();
                        }
                    }
                    else
                    {
                        newOption.labelColumns = new Label[displayColumns.Length];
                        bool checkBoxAdded = false;
                        for (int i = 0; i < displayColumns.Length; i++)
                        {
                            if (displayColumns[i] == "checkbox")
                            {
                                checkBoxAdded = true;
                                newOption.labelColumns[i] = new Label()
                                {
                                    label = "checkbox"
                                };
                            }
                            else if (displayColumns[i] == "status")
                            {
                                String statusLabel = "";

                                if (existingOption != null)
                                {
                                    Entity eo = (Entity)existingOption.reference;

                                    // get metadata for statuscode
                                    AttributeMetadata am = metaData.GetAttributeMetadata(eo.LogicalName, "statuscode");

                                    if (am.AttributeType == AttributeTypeCode.Status)
                                    {
                                        StatusAttributeMetadata pam = (StatusAttributeMetadata)am;
                                        List<OptionValue> options = new List<OptionValue>();
                                        OptionMetadata om = pam.OptionSet.Options.SingleOrDefault(o => o.Value == ((OptionSetValue)eo["statuscode"]).Value);
                                        statusLabel = om.Label.UserLocalizedLabel != null ? om.Label.UserLocalizedLabel.Label : "";
                                    }
                                }

                                newOption.labelColumns[i] = new Label()
                                {
                                    label = statusLabel
                                };
                            }
                            else
                            {
                                newOption.labelColumns[i] = new Label()
                                {
                                    label = GetStringAttributeValue(r, displayColumns[i], "")
                                };
                            }
                        }


                    }
                    if (groupField != null)
                    {
                        newOption.group = GetStringAttributeValue(r, groupField, "");
                    }

                    if (displayFieldName == "filename")
                    {
                        newOption.reference = null;
                    }

                    result.Add(newOption);
                }
            }
            return result;
        }


        public void SetValue(string path, string parentLookupPath, Entity root, string value, object objValue, Guid groupRowId)
        {
            AttributeMetadata am = null;
            RelationshipMetadataBase rm = null;
            string debugInfo = "";
            EntityCache cacheItem = null;
            string lookupField = null;

            try
            {
                path = "root." + path;

                if (!path.Contains("[row]") || groupRowId != Guid.Empty)
                {


                    path = path.Replace("[row]", groupRowId.ToString());
                    //if(targetPath != null) targetPath = targetPath.Replace("[row]", groupRowId.ToString());
                    //if (parentLookupPath != null) parentLookupPath = parentLookupPath.Replace("[row]", groupRowId.ToString());
                    Entity rowEntity = FindInReferences(groupRowId);
                    if (rowEntity != null)
                    {
                        AddToCache(rowEntity, "root." + groupRowId.ToString());
                    }

                }

                var currentValue = GetValue(path, null, parentLookupPath, root, out am, out rm, groupRowId, ref lookupField);
                if (currentValue is Entity) currentValue = ((Entity)currentValue).ToEntityReference();
                if (parentLookupPath != null && currentValue is EntityReference)
                {
                    SetValue(parentLookupPath, null, root, null, currentValue, groupRowId);
                }
                int i = path.LastIndexOf(".");
                string entityPath = path.Substring(0, i);
                cacheItem = FindInCache(entityPath);



                if (am != null)
                {

                    if (cacheItem != null)
                    {
                        Entity currentStep = cacheItem.Entity;
                        string s = path.Substring(i + 1);
                        if (am.AttributeType == AttributeTypeCode.String || am.AttributeType == AttributeTypeCode.Memo)
                        {
                            if (!currentStep.Contains(s) || currentStep[s] == null || (string)currentStep[s] != value)
                            {
                                SetAttributeValue(cacheItem, s, value);
                            }

                        }
                        else if (am.AttributeType == AttributeTypeCode.Integer)
                        {
                            if (value == null || value.Trim() == "") SetAttributeValue(cacheItem, s, null);
                            else if (!currentStep.Contains(s) || currentStep[s] == null || (int)currentStep[s] != int.Parse(value))
                            {
                                SetAttributeValue(cacheItem, s, int.Parse(value));
                            }
                        }
                        else if (am.AttributeType == AttributeTypeCode.Double)
                        {
                            if (value == null || value.Trim() == "") SetAttributeValue(cacheItem, s, null);
                            else if (!currentStep.Contains(s) || currentStep[s] == null || (double)currentStep[s] != double.Parse(value))
                            {
                                SetAttributeValue(cacheItem, s, double.Parse(value));
                            }
                        }
                        else if (am.AttributeType == AttributeTypeCode.Decimal)
                        {
                            if (value == null || value.Trim() == "") SetAttributeValue(cacheItem, s, null);
                            else if (!currentStep.Contains(s) || currentStep[s] == null || (decimal)currentStep[s] != decimal.Parse(value))
                            {
                                SetAttributeValue(cacheItem, s, decimal.Parse(value));
                            }
                        }
                        else if (am.AttributeType == AttributeTypeCode.Money)
                        {
                            if (value == null || value.Trim() == "") SetAttributeValue(cacheItem, s, null);
                            else if (!currentStep.Contains(s) || currentStep[s] == null || ((Money)currentStep[s]).Value != decimal.Parse(value))
                            {
                                SetAttributeValue(cacheItem, s, new Money(Decimal.Parse(value)));
                            }
                        }
                        else if (am.AttributeType == AttributeTypeCode.Boolean)
                        {
                            if (value == null || value.Trim() == "") SetAttributeValue(cacheItem, s, null);
                            else if (!currentStep.Contains(s) || currentStep[s] == null || (bool)currentStep[s] != Boolean.Parse(value))
                            {
                                SetAttributeValue(cacheItem, s, Boolean.Parse(value));
                            }
                        }
                        else if (am.AttributeType == AttributeTypeCode.DateTime)
                        {
                            if (value == null || value.Trim() == "") SetAttributeValue(cacheItem, s, null);
                            else if (!currentStep.Contains(s) || currentStep[s] == null || (DateTime)currentStep[s] != DateTime.Parse(value))
                            {
                                SetAttributeValue(cacheItem, s, DateTime.Parse(value));
                            }
                        }
                        else if (am.AttributeType == AttributeTypeCode.Memo)
                        {
                            if (value == null || value.Trim() == "") SetAttributeValue(cacheItem, s, null);
                            else if (!currentStep.Contains(s) || currentStep[s] == null || (string)currentStep[s] != value)
                            {
                                SetAttributeValue(cacheItem, s, value);
                            }
                        }
                        else if (am.AttributeType == AttributeTypeCode.Picklist)
                        {
                            if (!currentStep.Contains(s) || currentStep[s] == null || value == "" || ((OptionSetValue)currentStep[s]).Value != int.Parse(value))
                            {
                                if (value == "")
                                {
                                    SetAttributeValue(cacheItem, s, new OptionSetValue());
                                } else
                                {
                                    SetAttributeValue(cacheItem, s, new OptionSetValue(int.Parse(value)));
                                }

                            }
                        }
                        else if (am.AttributeType == AttributeTypeCode.Lookup)
                        {
                            debugInfo += " attributeType = Lookup;";
                            if (!currentStep.Contains(s) || currentStep[s] == null || (value != null && ((EntityReference)currentStep[s]).Id != Guid.Parse(value)) || (objValue != null && objValue is EntityReference && ((EntityReference)objValue).Id != ((EntityReference)currentStep[s]).Id))
                            {
                                Guid currentId = Guid.Empty;
                                if (currentStep.Contains(s))
                                {
                                    currentId = ((EntityReference)currentStep[s]).Id;
                                }
                                EntityReference er = null;
                                if (objValue is EntityReference)
                                {
                                    er = (EntityReference)objValue;
                                }
                                else
                                {
                                    LookupAttributeMetadata lam = (LookupAttributeMetadata)am;
                                    Entity ent = new Entity(lam.Targets[0]);
                                    ent.Id = Guid.Parse(value);
                                    er = ent.ToEntityReference();// new EntityReference(lam.Targets[0], Guid.Parse(value));
                                    AddToCache(ent, path);
                                }
                                debugInfo += " before SetValue;";
                                SetAttributeValue(cacheItem, s, er);
                                if (currentId != Guid.Empty)
                                {
                                    MergeCacheItems(er.Id, currentId);
                                }
                                debugInfo += " after SetValue;";
                            }
                        }

                    }
                    else
                    {
                        throw new OEMSPluginException("Cannot find updated item in the items cache: " + path);
                    }
                }

                else if (rm != null && rm is ManyToManyRelationshipMetadata) //configure relationships
                {


                    string otherEntity = null;
                    /*
                    if (rm is OneToManyRelationshipMetadata)
                    {
                        OneToManyRelationshipMetadata otmrm = (OneToManyRelationshipMetadata)rm;
                        otherEntity = otmrm.ReferencingEntity;
                    }
                    else */

                    if (rm is ManyToManyRelationshipMetadata)
                    {
                        ManyToManyRelationshipMetadata mtmrm = (ManyToManyRelationshipMetadata)rm;

                        string currentEntityName = cacheItem.Entity.LogicalName;
                        string currentAttributeName = null;
                        string referencedEntityName = null;
                        string referencedAttribyteName = null;
                        if (mtmrm.Entity1LogicalName.ToLower() == currentEntityName.ToLower())
                        {
                            referencedEntityName = mtmrm.Entity2LogicalName;
                            referencedAttribyteName = mtmrm.Entity2IntersectAttribute;
                            currentAttributeName = mtmrm.Entity1IntersectAttribute;
                        }
                        else
                        {
                            referencedEntityName = mtmrm.Entity1LogicalName;
                            referencedAttribyteName = mtmrm.Entity1IntersectAttribute;
                            currentAttributeName = mtmrm.Entity2IntersectAttribute;

                        }

                        otherEntity = referencedEntityName;


                    }
                    bool deleted = false;
                    if (value.IndexOf("deleted") > -1)
                    {
                        deleted = true;
                        value = value.Replace("deleted", "");
                    }
                    Guid referencedId = Guid.Parse(value);

                    EntityCache refEntity = new EntityCache();
                    refEntity.Entity = new Entity(otherEntity);
                    //refEntity.Entity.Id = Guid.NewGuid();
                    refEntity.Entity["createdflag"] = true;
                    refEntity.Entity.Id = referencedId;
                    AddAssociateData(cacheItem, rm.SchemaName, refEntity, deleted);
                }
                else if (rm != null) //configure relationships
                {
                    if (rm is OneToManyRelationshipMetadata && lookupField != null)
                    {
                        OneToManyRelationshipMetadata otmrm = (OneToManyRelationshipMetadata)rm;

                        bool deleted = false;
                        if (value.IndexOf("deleted") > -1)
                        {
                            deleted = true;
                            value = value.Replace("deleted", "");
                        }
                        Guid referencedId = Guid.Parse(value);
                        string otherEntity = null;
                        otherEntity = otmrm.ReferencingEntity;
                        LookupAttributeMetadata lam = (LookupAttributeMetadata)metaData.GetAttributeMetadata(otherEntity, lookupField);

                        Entity entity = new Entity(otherEntity);

                        entity["createdflag"] = true;

                        entity[lookupField] = new EntityReference(lam.Targets[0], referencedId);
                        entity[otmrm.ReferencingAttribute] = root.ToEntityReference();

                        var existingRelEntity = (from e in context.CreateQuery(otherEntity)
                                                 where (Guid)e[otmrm.ReferencingAttribute] == root.Id && (Guid)entity[lookupField] == referencedId
                                                 select e).FirstOrDefault();
                        if (deleted)
                        {
                            if (existingRelEntity != null)
                            {
                                context.Delete(existingRelEntity.LogicalName, existingRelEntity.Id);
                            }
                        }
                        else if (existingRelEntity == null)
                        {
                            entity.Id = Guid.NewGuid();
                            EntityCache refEntity = AddToCache(entity, value);
                        }


                        //AddAssociateData(cacheItem, rm.SchemaName, refEntity);
                    }
                    else if (rm is OneToManyRelationshipMetadata && lookupField == null)
                    {
                        OneToManyRelationshipMetadata otmrm = (OneToManyRelationshipMetadata)rm;

                        bool deleted = false;
                        if (value.IndexOf("deleted") > -1)
                        {
                            deleted = true;
                            value = value.Replace("deleted", "");
                        }
                        Guid referencedId = Guid.Parse(value);
                        string otherEntity = null;
                        otherEntity = otmrm.ReferencingEntity;
                        lookupField = otmrm.ReferencingAttribute;
                        LookupAttributeMetadata lam = (LookupAttributeMetadata)metaData.GetAttributeMetadata(otherEntity, lookupField);

                        Entity entity = new Entity(otherEntity);
                        Entity existingRelEntity = new Entity();

                        existingRelEntity = getRelationatedEntity(root.Id, referencedId);

                        if (deleted)
                        {
                            if (existingRelEntity.Id != Guid.Empty && existingRelationships.Count == 0)
                            {
                                context.Delete(existingRelEntity.LogicalName, existingRelEntity.Id);
                            }
                        }
                        else if (existingRelEntity.Id == Guid.Empty)
                        {
                            entity = getRelationatedEntity(((EntityReference)root.Attributes["oems_contact"]).Id, referencedId);
                            entity[otmrm.ReferencingAttribute] = root.ToEntityReference();
                            entity["createdflag"] = true;
                            entity.Attributes["objecttypecode"] = root.LogicalName;
                            entity.Id = Guid.NewGuid();
                            EntityCache refEntity = AddToCache(entity, value);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                debugInfo += "; CacheItem = " + (cacheItem != null ? cacheItem.Entity.LogicalName : "null cache item");
                debugInfo += (am == null ? ";empty attribute metadata;" : ";non-empty attribute metadata;");
                debugInfo += (rm == null ? ";empty relationship metadata;" : ";non-empty relationship metadata;");
                Log.Error("Error updating " + path + " value " + value + debugInfo + System.Environment.NewLine + ex.Message);
                throw ex;
            }
        }


        public Entity getRelationatedEntity(Guid RegardingId, Guid AnnotationId)
        {
            QueryExpression query = new QueryExpression("annotation");
            query.ColumnSet = new ColumnSet(true);
            query.Criteria.AddCondition("objectid", ConditionOperator.Equal, RegardingId);
            query.Criteria.AddCondition("annotationid", ConditionOperator.Equal, AnnotationId);

            var entities = context.RetrieveMultiple(query);

            if (entities.Entities.Count > 0)
            {
                return entities[0];
            }
            else
            {
                return new Entity();
            }
        }


        public object GetValue(string path, string targetPath, string parentLookupPath, Entity root)
        {
            AttributeMetadata am = null;
            RelationshipMetadataBase rm = null;
            string lookupField = null;
            return GetValue(path, targetPath, parentLookupPath, root, out am, out rm, Guid.Empty, ref lookupField);
        }

        public object GetValue(string initialPath, string sourcePath, string targetPath, string parentLookupPath, Entity root, FormElement element, string parentElementId = "")
        {
            string path = sourcePath;

            //Determine path
            var foundTuple = FormElementBindingCache.Find(x => x.Item1 == element.id);

            if (foundTuple != null)
            {
                if (foundTuple.Item2 == BindingType.InitialPath)
                {
                    path = initialPath;
                }
            }

            AttributeMetadata am = null;
            RelationshipMetadataBase rm = null;
            string lookupField = null;
            return GetValue(path, targetPath, parentLookupPath, root, out am, out rm, Guid.Empty, ref lookupField, element.name, element.id);
        }


        /*
                public object GetValue(string path, string targetPath, string parentLookupPath, Entity root, Guid groupRowId)
                {
                    AttributeMetadata am = null;
                    RelationshipMetadataBase rm = null;
                    string lookupField = null;
                    return GetValue(path, targetPath, parentLookupPath, root, out am, out rm, groupRowId, ref lookupField);
                }
        */

        public object GetValue(string path, string targetPath, string parentLookupPath, Entity root, out AttributeMetadata am, out RelationshipMetadataBase rm, Guid groupRowId, ref string lookupField, string parentElementName = "", string parentElementId = "", int rowNumber = -1)
        {
            am = null;
            rm = null;



            object result = null;
            Entity currentStep = null;
            string groupField = null;

            if (path != null && (!path.Contains("[row]") || groupRowId != Guid.Empty))
            {

                if (path.IndexOf("root.") != 0) path = "root." + path;
                if (groupRowId != Guid.Empty)
                {
                    path = path.Replace("[row]", groupRowId.ToString());
                    //if(targetPath != null) targetPath = targetPath.Replace("[row]", groupRowId.ToString());
                    //if (parentLookupPath != null) parentLookupPath = parentLookupPath.Replace("[row]", groupRowId.ToString());
                    Entity rowEntity = FindInReferences(groupRowId);
                    if (rowEntity != null)
                    {
                        AddToCache(rowEntity, "root." + groupRowId.ToString());
                    }
                }

                string[] steps = path.Split('.');
                // root;
                string currentPath = "";
                if (FindInCache("root") == null) AddToCache(root, "root");



                string prevPath = null;

                foreach (var s in steps)
                {
                    am = null;
                    prevPath = currentPath;
                    if (currentPath != "") currentPath += ".";
                    currentPath += s;

                    EntityCache cacheItem = FindInCache(currentPath);

                    if (cacheItem == null)
                    {

                        if (s.StartsWith("["))
                        {

                            int i = s.IndexOf("]");
                            //if(i == -1) RaiseError("Display name was not provided: '"+path+"'");


                            string relName = s.Substring(0, i);
                            relName = relName.Replace("[", "").Replace("]", "");
                            int li = relName.IndexOf(",config=");
                            if (li > 0)
                            {
                                string config = relName.Substring(li + ",config=".Length);
                                relName = relName.Substring(0, li);
                                string[] configParameters = config.Split('/');
                                foreach (var c in configParameters)
                                {
                                    string[] nameValue = c.Split('=');
                                    if (nameValue.Length == 2)
                                    {
                                        if (nameValue[0] == "lookup") lookupField = nameValue[1];
                                        if (nameValue[0] == "group")
                                        {
                                            groupField = nameValue[1];
                                        }
                                    }
                                }


                            }

                            string relOperator = "Options";
                            int opPosition = relName.IndexOf("/");
                            if (opPosition > -1)
                            {
                                relOperator = relName.Substring(opPosition + 1);
                                relName = relName.Substring(0, opPosition);
                            }


                            string displayFieldName = null;
                            int j = s.IndexOf("/", i);
                            if (j > -1) displayFieldName = s.Substring(j + 1);
                            string sortFieldName = null;

                            if (displayFieldName != null)
                            {
                                i = displayFieldName.IndexOf("/");
                                if (i > -1)
                                {
                                    sortFieldName = displayFieldName.Substring(i + 1);
                                    displayFieldName = displayFieldName.Substring(0, i);

                                }
                            }
                            rm = metaData.GetRelationshipMetadata(currentStep.LogicalName, relName);
                            if (rm != null)
                            {
                                string relatedEntityName = null;
                                var references = GetRelatedRecords(currentStep, rm, sortFieldName, ref relatedEntityName);


                                if (relOperator == "First")
                                {
                                    //Here check for the ID
                                    currentStep = references.FirstOrDefault();
                                    /*
                                    if (path.IndexOf("oems_eventregistration_to_accommodationrequest/First") > -1)
                                    {
                                        RaiseError(currentStep == null ? "Null" : "Not Null");
                                    }*/

                                    if (currentStep == null)
                                    {
                                        currentStep = new Entity(relatedEntityName);
                                        currentStep.Id = Guid.NewGuid();
                                        currentStep["createdflag"] = true;
                                        EntityCache newItem = AddToCache(currentStep, currentPath);
                                        EntityCache prevItem = FindInCache(prevPath);
                                        AddAssociateData(prevItem, rm.SchemaName, newItem, false);

                                        //
                                        //AddToCache(currentStep, currentPath);
                                        //SetAttributeValue(prevItem, s, currentStep.ToEntityReference());
                                    }
                                    else
                                    {
                                        AddToCache(currentStep, currentPath);
                                    }

                                }
                                else
                                {
                                    List<OptionValue> existingOptions = null;

                                    if (targetPath != null && (targetPath.IndexOf("[row]") < 0 || groupRowId != Guid.Empty))
                                    {
                                        RelationshipMetadataBase rmTarget = null;
                                        AttributeMetadata amTarget = null;
                                        string lookupFieldTarget = null;
                                        var existing = GetValue(targetPath, null, null, root, out amTarget, out rmTarget, groupRowId, ref lookupFieldTarget);
                                        if (existing is OptionValue[]) existingOptions = new List<OptionValue>((OptionValue[])existing);
                                    }

                                    if (targetPath != null && (targetPath.IndexOf("[row]") == 0) && path == "root.oems_event.[oems_event_to_eventchildcareschedule]/oems_childcareschedulename/oems_childcareschedulename")
                                    {
                                        targetPath = "[oems_eventregistration_to_childcarerequest/First].[oems_ChildCareRequestSchedule]";
                                        //targetPath = "[oems_eventregistration_to_childcarerequest].[oems_ChildCareRequestSchedule]";

                                        RelationshipMetadataBase rmTarget = null;
                                        AttributeMetadata amTarget = null;
                                        string lookupFieldTarget = null;
                                        //GetExistingSchedules(root, references,parentElementName);
                                        //var existing = GetValue(targetPath, null, null, root, out amTarget, out rmTarget, groupRowId, ref lookupFieldTarget,parentElementName,parentElementId);
                                        existingOptions = GetExistingSchedules(root, references, parentElementName);
                                        //if (existing is OptionValue[]) existingOptions = new List<OptionValue>((OptionValue[])existing);
                                    }


                                    List<OptionValue> options = GetOptions(references, displayFieldName, existingOptions, lookupField, groupField);
                                    return options.ToArray();
                                }


                            }
                            else throw new OEMSPluginException("Cannot locate relationship metadata for " + path);

                        }
                        else
                        {
                            if (am == null) am = metaData.GetAttributeMetadata(currentStep.LogicalName, s);

                            if (am.AttributeType == AttributeTypeCode.Lookup)
                            {

                                LookupAttributeMetadata lam = (LookupAttributeMetadata)am;
                                if (!currentStep.Contains(s) || currentStep[s] == null)
                                {
                                    EntityCache prevItem = FindInCache(prevPath);
                                    if (prevItem != null)
                                    {
                                        Entity newItem = new Entity(lam.Targets[0]);
                                        newItem.Id = Guid.NewGuid();
                                        //newItem["createdflag"] = true;
                                        currentStep = newItem;
                                        //AddToCache(currentStep, currentPath, false);
                                        SetAttributeValue(prevItem, s, newItem.ToEntityReference());
                                    }
                                }
                                else
                                {
                                    EntityReference nextRef = (EntityReference)currentStep[s];
                                    EntityCache cached = FindInCache(nextRef.Id);
                                    if (cached != null)
                                    {
                                        currentStep = cached.Entity;
                                        AddToCache(currentStep, currentPath);
                                    }
                                    else
                                    {
                                        currentStep = context.Retrieve(nextRef.LogicalName, nextRef.Id, new Microsoft.Xrm.Sdk.Query.ColumnSet(true));
                                        AddToCache(currentStep, currentPath);
                                    }
                                }
                                var em = metaData.GetEntityMetadata(currentStep.LogicalName);

                                if (path.IndexOf(s) + s.Length == path.Length)
                                {
                                    var references = context.CreateQuery(currentStep.LogicalName);
                                    references.Where(e => (int)e["statecode"] == 0);

                                    List<OptionValue> existingOptions = new List<OptionValue>();
                                    existingOptions.Add(new OptionValue()
                                    {
                                        value = currentStep.Id.ToString(),
                                        label = currentStep.Id.ToString()
                                    }
                                    );


                                    List<OptionValue> options = GetOptions(references, em.PrimaryNameAttribute, existingOptions, null, groupField);

                                    return options.ToArray();
                                }
                            }
                        }
                    }
                    else
                    {
                        currentStep = cacheItem.Entity;
                    }

                    if (result == null)
                    {
                        result = currentStep;
                    }

                    if (currentPath == path)
                    {
                        if (s.Contains("[") || s.Contains("]"))
                        {
                            result = currentStep;
                            AddToCache(currentStep, currentPath);
                        }
                        else
                        {

                            if (am == null) am = metaData.GetAttributeMetadata(currentStep.LogicalName, s);

                            if (am.AttributeType == AttributeTypeCode.String || am.AttributeType == AttributeTypeCode.Memo)
                            {
                                if (!currentStep.Contains(s) || currentStep[s] == null) result = null;
                                else result = (string)currentStep[s];
                            }
                            if (am.AttributeType == AttributeTypeCode.Boolean)
                            {
                                if (!currentStep.Contains(s) || currentStep[s] == null) result = null;
                                else result = (bool)currentStep[s];
                            }
                            else if (am.AttributeType == AttributeTypeCode.Picklist)
                            {

                                PicklistAttributeMetadata pam = (PicklistAttributeMetadata)am;
                                List<OptionValue> options = new List<OptionValue>();
                                foreach (var o in pam.OptionSet.Options)
                                {
                                    options.Add(new OptionValue()
                                    {
                                        label = o.Label.UserLocalizedLabel != null ? o.Label.UserLocalizedLabel.Label : o.Value.ToString(),
                                        value = o.Value.ToString(),
                                        selected = (currentStep.Contains(s) && currentStep[s] != null && ((OptionSetValue)currentStep[s]).Value == o.Value).ToString().ToLower()
                                    }
                                                );
                                }
                                options.Add(new OptionValue() {
                                    label = "",
                                    value = null,
                                    selected = ""
                                });
                                options = options.OrderBy(x => x.label).ToList<OptionValue>();
                                result = options.ToArray();
                            }
                            else if (am.AttributeType == AttributeTypeCode.Status)
                            {
                                StatusAttributeMetadata pam = (StatusAttributeMetadata)am;
                                List<OptionValue> options = new List<OptionValue>();

                                if (currentStep.Contains(s))
                                {
                                    OptionMetadata om = pam.OptionSet.Options.SingleOrDefault(o => o.Value == ((OptionSetValue)currentStep[s]).Value);

                                    return om.Label.UserLocalizedLabel != null ? om.Label.UserLocalizedLabel.Label : String.Empty;
                                }
                                else
                                {
                                    return String.Empty;
                                }
                            }
                            else if (am.AttributeType == AttributeTypeCode.Money)
                            {
                                if (!currentStep.Contains(s) || currentStep[s] == null) result = null;
                                else result = Decimal.Round(((Money)currentStep[s]).Value, 2, MidpointRounding.AwayFromZero);
                            }
                            else if (am.AttributeType == AttributeTypeCode.Double)
                            {
                                if (!currentStep.Contains(s) || currentStep[s] == null) result = null;
                                else result = (Double)currentStep[s];
                            }
                            else if (am.AttributeType == AttributeTypeCode.Decimal)
                            {
                                if (!currentStep.Contains(s) || currentStep[s] == null) result = null;
                                else result = (Decimal)currentStep[s];
                            }
                            else if (am.AttributeType == AttributeTypeCode.Integer)
                            {
                                if (!currentStep.Contains(s) || currentStep[s] == null) result = null;
                                else result = (int)currentStep[s];

                            }
                            else if (am.AttributeType == AttributeTypeCode.DateTime)
                            {
                                if (!currentStep.Contains(s) || currentStep[s] == null) result = null;
                                else result = (DateTime)currentStep[s];
                            }

                        }
                    }
                }
            }

            return result;
        }

        List<OptionValue> GetExistingSchedules(Entity root, IQueryable<Entity> references, string parentElementName)
        {
            if (root.Contains("oems_dynamicregformdata"))
            {
                var nameValuePairs = JsonConvert.DeserializeObject<DataWork.NameValuePair[]>(root["oems_dynamicregformdata"].ToString());

                //var previouslySavedValue = nameValuePairs.FirstOrDefault(nv => nv.name.EndsWith(parentElementName));
                var query = from NV in nameValuePairs
                            where NV.name.EndsWith(parentElementName)
                            select NV;

                List<NameValuePair> previouslySavedValue = new List<NameValuePair>();
                //Let's make the hack
                if (rowIndex > -1)
                {
                    string[] splittedName = parentElementName.Split('_');

                    string rowToFind = string.Concat("#" + rowIndex.ToString());

                    string valueToFind = null;

                    if (splittedName.Length > 0)
                        valueToFind = splittedName[1];
                    else
                        valueToFind = parentElementName;

                    previouslySavedValue = (from NNV in nameValuePairs
                                            where NNV.name.EndsWith(valueToFind)
                                            && NNV.name.Contains(rowToFind)
                                            select NNV).ToList<NameValuePair>();


                    if (previouslySavedValue.Count == 0)
                    {
                        previouslySavedValue = query.ToList();
                    }
                }

                List<OptionValue> optionValues = new List<OptionValue>();

                foreach (var r in references)
                {
                    NameValuePair found = (from PV in previouslySavedValue where PV.value.ToLower() == r.Id.ToString().Replace("{", "").Replace("}", "").ToLower() select PV).FirstOrDefault<NameValuePair>();

                    if (found != null)
                    {
                        OptionValue newOption = new OptionValue()
                        {

                            value = r.Id.ToString(),
                            idValue = r.Id.ToString(),
                            selected = false.ToString(),
                            reference = r
                        };

                        optionValues.Add(newOption);
                    }
                }

                return optionValues;
            }
            return null;
        }

        public void SaveElementData(DataWork.FormElement element, List<DataWork.NameValuePair> data, Entity rootEntity)
        {
            List<DataWork.NameValuePair> updatedData = data.FindAll(nvp => nvp.name.EndsWith(element.name));
            if (updatedData != null && updatedData.Count > 0)
            {

                foreach (var nvp in updatedData)
                {
                    if (nvp.name.Contains("ROWIDDELETE"))
                    {
                        string rowNumber = nvp.value;
                        AttributeMetadata am = null;
                        RelationshipMetadataBase rm = null;
                        string lookupField = null;

                        GetValue(element.targetPath, null, null, rootEntity, out am, out rm, Guid.Empty, ref lookupField);
                        //RelationshipMetadataBase rm = metaData.GetRelationshipMetadata(a.Target.Entity.LogicalName, a.RelationshipName);
                        if (rm is OneToManyRelationshipMetadata)
                        {
                            OneToManyRelationshipMetadata rmOtM = (OneToManyRelationshipMetadata)rm;
                            Guid rowGroupId = Guid.Parse(nvp.value);
                            if (rowGroupId != Guid.Empty)
                            {
                                try
                                {
                                    var existingEntity = context.Retrieve(rmOtM.ReferencingEntity, rowGroupId, new Microsoft.Xrm.Sdk.Query.ColumnSet());
                                    context.Delete(rmOtM.ReferencingEntity, rowGroupId);
                                }
                                catch (Exception ex)
                                {
                                    //It seems to be coming here twice for each deleted row
                                    Log.Warn("tried delete twice for each deleted row");
                                }
                            }
                        }
                    }

                    else
                    {
                        if (element.type == "checkbox" && nvp.value != "false") nvp.value = "true";

                        Guid rowGroupId = Guid.Empty;
                        string rowNumber = null;
                        string multiRowComponentId = null;
                        int rowNumberPos = nvp.name.IndexOf("#");

                        if (rowNumberPos > -1)
                        {
                            int rowNumberEnd = nvp.name.IndexOf("/", rowNumberPos);
                            rowNumber = nvp.name.Substring(rowNumberPos + 1, rowNumberEnd - rowNumberPos - 1);
                            int i = nvp.name.LastIndexOf("_", rowNumberPos);
                            int j = nvp.name.LastIndexOf("/", rowNumberPos);
                            if (i < j) i = j;
                            multiRowComponentId = nvp.name.Substring(i + 1, rowNumberPos - i - 1);
                        }

                        int rowIdSeparatorPos = nvp.name.LastIndexOf("_");
                        if (rowIdSeparatorPos > -1 && rowNumber != null)
                        {
                            int rowIdStart = nvp.name.LastIndexOf("/");
                            if (rowIdStart < rowIdSeparatorPos)
                            {
                                string rowId = nvp.name.Substring(rowIdStart + 1, rowIdSeparatorPos - rowIdStart - 1);
                                rowGroupId = Guid.Parse(rowId);
                            }
                        }



                        if (rowNumber != null)
                        {

                            multiRowComponentId = multiRowComponentId + rowNumber;

                            //Create new row entity
                            Entity ent = null;
                            if (newRows.ContainsKey(multiRowComponentId))
                            {
                                ent = (Entity)newRows[multiRowComponentId];
                            }
                            else
                            {
                                FormElement rowElement = element;
                                while (rowElement.type != "repeatableSection") rowElement = rowElement.GetParentElement();
                                AttributeMetadata am = null;
                                RelationshipMetadataBase rm = null;
                                string lookupField = null;
                                GetValue(rowElement.targetPath, null, null, rootEntity, out am, out rm, Guid.Empty, ref lookupField);
                                //RelationshipMetadataBase rm = metaData.GetRelationshipMetadata(a.Target.Entity.LogicalName, a.RelationshipName);
                                if (rm is OneToManyRelationshipMetadata)
                                {
                                    OneToManyRelationshipMetadata rmOtM = (OneToManyRelationshipMetadata)rm;
                                    if (rowGroupId != Guid.Empty)
                                    {
                                        ent = context.Retrieve(rmOtM.ReferencingEntity, rowGroupId, new Microsoft.Xrm.Sdk.Query.ColumnSet(true));
                                        referenceList.Add(ent);
                                        AddToCache(ent, "root." + ent.Id);
                                    }
                                    else
                                    {
                                        ent = new Entity(rmOtM.ReferencingEntity);
                                        ent[rmOtM.ReferencingAttribute] = rootEntity.ToEntityReference();
                                        ent.Id = Guid.NewGuid();

                                        ent["createdflag"] = true;
                                        referenceList.Add(ent);
                                        AddToCache(ent, "root." + ent.Id);
                                        newRows[multiRowComponentId] = ent;
                                    }
                                }
                            }
                            rowGroupId = ent.Id;
                        }

                        if (element.type == "checkbox" && nvp.value == "false" && element.deletePath != null)
                        {
                            GetValue(element.deletePath, null, null, rootEntity);
                            var cacheItem = FindInCache("root." + element.deletePath);
                            if (cacheItem != null && cacheItem.Entity != null)
                            {
                                try
                                {
                                    context.Delete(cacheItem.Entity.LogicalName, cacheItem.Entity.Id);
                                    entityCache.Remove(cacheItem);
                                }
                                catch (Exception ex)
                                {
                                }
                            }
                        }
                        SetValue(element.targetPath, element.oneToOneParentLookupPath, rootEntity, nvp.value, null, rowGroupId);
                    }
                }
            }
            else
            {
                //Hanlde nulls etc - they are not sent from the client
                if (element.type == "checkbox") SetValue(element.targetPath, element.oneToOneParentLookupPath, rootEntity, "false", null, Guid.Empty);
            }
        }

        public void SaveFormData(List<DataWork.FormElement> elements, List<DataWork.NameValuePair> data, Entity rootEntity)
        {
            NameValuePair personalAccommodationRequired = null;
            NameValuePair accommodationRequired = null;

            BillingCalculator billingCalculator = new BillingCalculator(this.context, rootEntity);
            newRows = new Hashtable();

            foreach (var element in elements)
            {
                //Fix for personal accommodation component
                if (element.id.Trim().ToUpper() == PERSONALACCOMMODATIONREQUIRED.Trim().ToUpper())
                {
                    personalAccommodationRequired = data.Find(x => x.name.EndsWith(element.name));
                }

            }

            foreach (var element in elements)
            {
                //Fix for accommodation component
                if (element.id.Trim().ToUpper() == ACCOMMODATIONREQUIRED.Trim().ToUpper())
                {
                    accommodationRequired = data.Find(x => x.name.EndsWith(element.name));
                }

                if (accommodationRequired == null && ValidateComponent(element, "Accommodation") == true) continue;

                bool saved = false;
                foreach (var f in formComponents)
                {
                    if (f.GetType() == typeof(FormComponents.PersonalAccommodation))
                    {
                        bool required = (personalAccommodationRequired != null) ? true : false;
                        ((FormComponents.PersonalAccommodation)f).SetPersonalAccommodationRequired(required);
                    }

                    if (f.SaveElementData(this, element, data, rootEntity))
                    {
                        saved = true;
                        break;
                    }
                }

                if (!saved)
                {
                    SaveElementData(element, data, rootEntity);
                    if (element.options.newRowTemplate != null)
                    {

                        SaveElementData(element, data, rootEntity);
                    }
                }

                billingCalculator.CollectBillableFormElement(element, data);

            }
            billingCalculator.SaveBillingRequest();
        }

        private bool ValidateComponent(FormElement element, string elementName)
        {
            var parent = element.GetParentElement();

            while (parent != null)
            {
                if (parent.id == elementName) return true;
                parent = parent.GetParentElement();
            }

            return false;
        }

        protected void ConvertToPlainList(List<DataWork.FormElement> elements, DataWork.FormElement current)
        {
            //if(current.type != "repeatableSection")  elements.Add(current);
            elements.Add(current);
            foreach (var e in current.children)
            {
                if (e.options.newRowTemplate != null) ConvertToPlainList(elements, e.options.newRowTemplate);
                ConvertToPlainList(elements, e);
            }

        }


        public void SaveForm(FormElement form, List<DataWork.NameValuePair> formData, oems_EventRegistration entity)
        {

            form.ResetParents();
            List<DataWork.FormElement> elements = new List<DataWork.FormElement>();
            ConvertToPlainList(elements, form);

            existingRelationships = new List<ExistingRelationship>();
            FormElement originalForm = form.Clone();
            LoadDynamicFormData(originalForm, entity);
            originalForm.ResetParents();
            Init();

            List<string> deletedRows = new List<string>();

            List<DataWork.FormElement> originalElements = new List<DataWork.FormElement>();
            ConvertToPlainList(originalElements, originalForm);
            int i = 0;
            while (i < originalElements.Count)
            {
                if (originalElements[i].name.Contains("ROWID"))
                {
                    var elem = originalElements[i];
                    originalElements.Remove(elem);
                    originalElements.Insert(0, elem);
                }
                i++;
            }
            foreach (var e in originalElements)
            {
                if (e.deletePath != null)
                {
                    NameValuePair nvp = formData.Find(x => x.name.Contains(e.name));
                    bool existsInData = nvp != null;
                    if (!existsInData)
                    {
                        //no data for the "enabling" checkbox

                        //1. Remove all related data
                        formData.RemoveAll(x => x.name.Contains(e.options.enablesSection));

                        //2. Add "delete" data if it used to be there
                        if (e.options.selected == true)
                        {
                            formData.Add(new NameValuePair()
                            {
                                value = "false",
                                name = e.name

                            }
                            );
                        }

                    }
                }
                else if (e.name.Contains("ROWID"))
                {
                    bool existsInData = formData.Find(x => x.name.Contains(e.value)) != null;
                    if (!existsInData)
                    {
                        //no data, no "hidden" for this row - it's been deleted

                        //somehow it gets added twice, so let's check first
                        if (formData.Find(x => x.value == e.value) == null)
                        {
                            formData.Insert(0, new NameValuePair()
                            {
                                value = e.value,
                                name = e.name.Replace("ROWID", "ROWIDDELETE")

                            }
                            );
                            deletedRows.Add(e.value);
                        }
                    }
                }
                else if (e.type == "listofcheckboxes" || e.type == "extendedlistofcheckboxes")
                {
                    foreach (var o in e.checkboxValues)
                    {
                        if (o.selected == "true")
                        {
                            bool existsInData = formData.Find(x => x.value == o.value) != null ||
                                                deletedRows.Find(s => e.name.Contains(s)) != null;
                            if (!existsInData)
                            {
                                formData.Insert(0, new NameValuePair()
                                {
                                    value = o.value + "deleted",
                                    name = e.name
                                }
                                 );
                            }
                        }
                    }
                }
                else if (e.type == "listofradios")
                {
                    foreach (var o in e.radioValues)
                    {
                        if (o.selected == "true")
                        {
                            bool existsInData = formData.Find(x => x.value == o.value) != null ||
                                                deletedRows.Find(s => e.name.Contains(s)) != null;
                            if (!existsInData)
                            {
                                formData.Insert(0, new NameValuePair()
                                {
                                    value = o.value + "deleted",
                                    name = e.name
                                }
                                 );
                            }
                        }
                    }
                }
            }

            formData.RemoveAll(x => x.name.Contains("ROWID_"));

            var lst = elements.FindAll(x => x.component != 0);
            foreach (var e in lst)
            {
                if (e.sourcePath == null) e.sourcePath = "";
                if (e.targetPath == null) e.targetPath = "";
            }

            elements.RemoveAll(x => x.sourcePath == null || x.targetPath == null);

            elements.Sort((x, y) => x.sourcePath.Split('.').Length.CompareTo(y.sourcePath.Split('.').Length));

            SaveFormData(elements.OrderBy(x => x.component).ToList<FormElement>(), formData, entity);
            SaveUpdateCache();

        }

        public void SyncEnablesSection(DataWork.FormElement root, DataWork.FormElement element)
        {
            if (element.options.enablesSection != null && element.options.enablesSection != "")
            {
                DataWork.FormElement section = root.FindByName(element.options.enablesSection);
                if (section != null) section.options.collapse = element.options.selected != true;
            }
            else
            {
                foreach (var c in element.children)
                {
                    SyncEnablesSection(root, c);
                }
            }
        }

        public void LoadDynamicFormData(DataWork.FormElement element, oems_EventRegistration root)
        {
            List<string> preferredSchoolIds = new List<string>();
            List<string> preferredLocalIds = new List<string>();
            List<string> preferredSchoolboardIds = new List<string>();
            //TODO - NEEDS RAZ DEFINITION
            //var accDefaults = from asbl in context.CreateQuery("oems_accountschoolboardlocal")
            //                  where (int)asbl["statecode"] == 0 && (Guid)asbl["oems_account"] == (Guid)root["oems_account"]
            //                  && (bool)asbl["oems_defaultflag"] == true
            //                  select asbl;

            //foreach (var a in accDefaults)
            //{
            //    if (a.Contains("oems_school") && a["oems_school"] != null) preferredSchoolIds.Add(DataWork.FormElement.GuidToString(((EntityReference)a["oems_school"]).Id));
            //    if (a.Contains("oems_schoolboard") && a["oems_schoolboard"] != null) preferredSchoolboardIds.Add(DataWork.FormElement.GuidToString(((EntityReference)a["oems_schoolboard"]).Id));
            //    if (a.Contains("oems_local") && a["oems_local"] != null) preferredLocalIds.Add(DataWork.FormElement.GuidToString(((EntityReference)a["oems_local"]).Id));
            //}



            //element.preferredLocalIds = preferredLocalIds.ToArray();
            //element.preferredSchoolIds = preferredSchoolIds.ToArray();
            //element.preferredSchoolboardIds = preferredSchoolboardIds.ToArray();

            LoadDynamicFormData(element, root, Guid.Empty);

            SyncEnablesSection(element, element);
        }


        public void LoadDynamicFormData(DataWork.FormElement element, oems_EventRegistration root, Guid groupRowId, int rowNumber = -1, string elementId = "")
        {
            var sw = new Stopwatch();

            foreach (var e in formComponents)
            {
                sw.Restart();
                var loadDynamicFormDataResult = e.LoadDynamicFormData(this, element, root, groupRowId);
                sw.Stop();
                if (sw.ElapsedMilliseconds > 0) Log.DebugFormat("LoadDynamicFormData {0} {1} msec", element.id, sw.ElapsedMilliseconds);

                if (loadDynamicFormDataResult) return;
            }

            object value = null;

            //try
            //{
            Microsoft.Xrm.Sdk.Metadata.AttributeMetadata am = null;
            Microsoft.Xrm.Sdk.Metadata.RelationshipMetadataBase rm = null;
            if (groupRowId != Guid.Empty) element.name = groupRowId.ToString().Replace("{", "").Replace("}", "") + "_" + element.name;
            string lookupField = null;
            //value = GetValue(element.sourcePath, element.targetPath, null, root, out am, out rm, groupRowId, ref lookupField);

            OptionSetValue currentStatus = (OptionSetValue)root["statuscode"];

            if (currentStatus.Value == EventRegistrationStatusReason.Initial)
            {
                if (element.initialPath != null)
                {
                    if (element.initialPath == RADIONULL)
                    {
                        value = null;
                    }
                    else
                    {
                        value = GetValue(element.initialPath, element.targetPath, null, root, out am, out rm, groupRowId, ref lookupField);
                    }
                }
                else
                {
                    value = GetValue(element.sourcePath, element.targetPath, null, root, out am, out rm, groupRowId, ref lookupField);
                }

            }
            else
            {
                //value = GetValue(element.sourcePath, element.targetPath, null, root, out am, out rm, groupRowId, ref lookupField);
                value = GetValue(element.initialPath, element.sourcePath, element.targetPath, lookupField, root, element, element.name);
            }

            // fall back to previously saved value
            if (value == null && root.oems_dynamicregformdata != null)
            {
                //todo pass it instead
                var nameValuePairs = JsonConvert.DeserializeObject<DataWork.NameValuePair[]>(root.oems_dynamicregformdata);
                //var previouslySavedValue = nameValuePairs.FirstOrDefault(nv => nv.name == element.id);
                var previouslySavedValue = nameValuePairs.FirstOrDefault(nv => nv.name.EndsWith(element.name));

                //NameValuePair previouslySavedValue = null;
                //Let's make the hack
                //This hack will prevent the duplicated record on the first time

                if (rowNumber > -1)
                {
                    string[] splittedName = element.name.Split('_');

                    string rowToFind = string.Concat("#" + rowNumber.ToString());

                    string valueToFind = null;

                    if (splittedName.Length > 0)
                        valueToFind = splittedName[1];
                    else
                        valueToFind = element.name;

                    previouslySavedValue = (from NNV in nameValuePairs
                                            where NNV.name.EndsWith(valueToFind)
                                            && NNV.name.Contains(rowToFind) select NNV).FirstOrDefault();

                }


                if (previouslySavedValue != null)
                {
                    if (element.type == "checkbox")
                    {
                        value = true;
                    }
                    else
                    {
                        value = previouslySavedValue.value;
                    }
                }
            }

            if (value != null)
            {
                if (value is DataWork.OptionValue[] &&
                    (element.valueFormatter == (int)ValueFormatter.DATE || element.valueFormatter == (int)ValueFormatter.DATETIME))
                {
                    foreach (var o in (DataWork.OptionValue[])value)
                    {
                        DateTime dt;
                        if (o.label != null && DateTime.TryParse(o.label, out dt))
                        {
                            if (element.valueFormatter == (int)ValueFormatter.DATE)
                            {
                                o.label = GetDateString(dt, true);
                            }
                            else
                            {
                                o.label = GetDateString(dt, false);
                            }
                        }
                    }
                }

                //if (element.type == "checkbox") element.options.selected = bool.Parse(value.ToString());
                bool? nullablebool = null;
                if (element.type == "checkbox")
                {
                    element.options.selected = (value.ToString() == string.Empty) ? nullablebool : bool.Parse(value.ToString());
                }
                else if (element.type == "repeatableSection")
                {

                    if (value is DataWork.OptionValue[] && element.options.newRowTemplate != null)
                    {
                        LoadDynamicFormData(element.options.newRowTemplate, root, groupRowId, -1, element.name);
                        DataWork.OptionValue[] rows = (DataWork.OptionValue[])value;
                        List<DataWork.FormElement> childElements = new List<DataWork.FormElement>();
                        if (element.children != null) childElements.AddRange(element.children);
                        for (int i = 0; i < rows.Length; i++)
                        {
                            rowIndex = i;
                            DataWork.FormElement newElement = element.options.newRowTemplate.Clone();
                            newElement.value = rows[i].value;
                            LoadDynamicFormData(newElement, root, Guid.Parse(rows[i].value), i, rows[i].value);

                            //Add row ID
                            FormElement rowId = new FormElement();
                            rowId.type = "hidden";
                            rowId.value = rows[i].value.Replace("{", "").Replace("}", "");
                            rowId.name = "ROWID_" + rowId.value + "_" + element.name;
                            rowId.children = new FormElement[0];
                            //element.value = rowId.value;
                            rowId.sourcePath = element.options.newRowTemplate.sourcePath;
                            rowId.targetPath = element.options.newRowTemplate.targetPath;
                            newElement.AddChild(rowId);

                            childElements.Add(newElement);


                        }

                        rowIndex = -1;

                        //Clean the row template
                        CleanRowTemplate(element.options.newRowTemplate);

                        element.children = childElements.ToArray();


                    }

                }
                else if (element.type == "selectbox")
                {
                    element.selectValues = (DataWork.OptionValue[])value;
                    foreach (var o in (DataWork.OptionValue[])value)
                    {
                        if (o.selected != null && o.selected.ToLower() == "true") element.value = o.value;

                    }

                }
                else if (element.type == "listofcheckboxes")
                {
                    element.checkboxValues = (DataWork.OptionValue[])value;

                    if (element.checkboxValues.Length == 0)
                    {
                        element.options.label = element.options.label + ": No options available";
                    }
                }
                else if (element.type == "listofradios")
                {
                    element.radioValues = (DataWork.OptionValue[])value;

                    if (element.radioValues.Length > 0)
                    {
                        element.radioValues = CreateNoneOption(element.radioValues);

                        DataWork.OptionValue tempValue = element.radioValues.Where(x => x.selected.ToLower() == "true").SingleOrDefault<OptionValue>();

                        if (tempValue == null)
                        {
                            element.value = element.radioValues.Where(x => x.label.ToLower() == "none").SingleOrDefault<OptionValue>().value;
                        }
                        else
                        {
                            element.value = tempValue.value;
                        }
                    }
                    else
                    {
                        element.options.label = element.options.label + ": No options available";
                    }

                }
                else if (element.type == "extendedlistofcheckboxes")
                {
                    element.checkboxValues = (DataWork.OptionValue[])value;
                }
                else if (element.type == "listofradios")
                {
                    element.radioValues = (DataWork.OptionValue[])value;
                }

                else
                {
                    if (value is DateTime)
                    {
                        if (element.valueFormatter == (int)ValueFormatter.DATE)
                        {
                            value = GetDateString((DateTime)value, true);
                        }
                        else
                        {
                            value = GetDateString((DateTime)value, false);
                        }
                    }
                    element.value = DataWork.DataAccess.JsonStringEscape(value.ToString());// + "type = "+ element.type;
                }

            }

            //if (element.type == "checkbox") element.options.selected = true;



            foreach (var c in element.children)
            {
                if (!c.GetIsLoaded())
                {
                    if (rowNumber > -1)
                        LoadDynamicFormData(c, root, groupRowId, rowNumber, c.name);
                    else
                        LoadDynamicFormData(c, root, groupRowId, -1, c.name);
                }
            }

            //}
            //catch (Exception ex)
            //{
            //    if (groupRowId != Guid.Empty) throw ex;
            //    element.type = "textinput";
            //    element.value = "error reading " + element.sourcePath + ". " + ex.Message;
            //}
            element.SetIsLoaded(true);


        }

        //This function will clean all the values for the newRowTemplate
        void CleanRowTemplate(FormElement rowTemplate)
        {
            if (rowTemplate.type != "legend")
            {
                rowTemplate.value = string.Empty;
                //rowTemplate.name = "{" + Guid.Empty.ToString() + "}";

                if (rowTemplate.checkboxValues != null && rowTemplate.checkboxValues.Length > 0)
                {
                    for (int i = 0; i < rowTemplate.checkboxValues.Length; i++)
                    {
                        rowTemplate.checkboxValues[i].selected = false.ToString().ToLower();
                    }
                }
            }

            foreach (FormElement child in rowTemplate.children)
            {
                CleanRowTemplate(child);
            }
        }

        public void AdjustFormElementToRoles(LocalPluginContext localContext, ref DataWork.FormElement element, Account userAccount, oems_Event eventEntity, Dictionary<string, string> roleElements, Entity oemsPortalFormTemplate, oems_EventRegistration eventRegistration)
        {

            var watch = new Stopwatch();
            watch.Start();

            // do not do it from scratch, it is very slow: List<Element> formElementList = formTemplate.getFormElements();
            // use previously stored data: 
            List<Element> formElementList = JsonConvert.DeserializeObject<List<Element>>((String)oemsPortalFormTemplate["oems_updatedata"]);

            Log.Info("DeserializeObject " + watch.ElapsedMilliseconds);
            watch.Restart();


            Dictionary<string, bool> approvedRole = new Dictionary<string, bool>();

            if (eventRegistration.GetAttributeValue<int>("statuscode") == EventRegistrationStatusReason.Approved)
                approvedRole.Add(roleElements["approve"], true);

            else
                approvedRole.Add(roleElements["approve"], false);

            AdjustFormElementToRole(localContext, ref element, formElementList, approvedRole);

            Log.Info("AdjustFormElementToRole Approve " + watch.ElapsedMilliseconds);
            watch.Restart();

            Dictionary<string, bool> roles = new Dictionary<string, bool>();

            if (userAccount.oems_MemberFlag ?? false)
                roles.Add(roleElements["member"], true);
            else
                roles.Add(roleElements["nonmember"], true);

            // TODO: change all the code for Is guest
            //if (IsGuest(userAccount, eventEntity))
            //    roles.Add(roleElements["guest"], true);

            if (IsPresident(userAccount))
                roles.Add(roleElements["president"], true);

            AdjustFormElementToRole(localContext, ref element, formElementList, roles);

            Log.Info("AdjustFormElementToRole All Roles " + watch.ElapsedMilliseconds);
            watch.Restart();
        }

        public void AdjustFormElementToRoles(LocalPluginContext localContext, ref DataWork.FormElement element, Contact userContact, oems_Event eventEntity, Dictionary<string, string> roleElements, Entity oemsPortalFormTemplate, oems_EventRegistration eventRegistration)
        {

            var watch = new Stopwatch();
            watch.Start();

            // do not do it from scratch, it is very slow: List<Element> formElementList = formTemplate.getFormElements();
            // use previously stored data: 
            List<Element> formElementList = JsonConvert.DeserializeObject<List<Element>>((String)oemsPortalFormTemplate["oems_updatedata"]);

            Log.Info("DeserializeObject " + watch.ElapsedMilliseconds);
            watch.Restart();


            Dictionary<string, bool> approvedRole = new Dictionary<string, bool>();

            if (eventRegistration.GetAttributeValue<int>("statuscode") == EventRegistrationStatusReason.Approved)
                approvedRole.Add(roleElements["approve"], true);

            else
                approvedRole.Add(roleElements["approve"], false);

            AdjustFormElementToRole(localContext, ref element, formElementList, approvedRole);

            Log.Info("AdjustFormElementToRole Approve " + watch.ElapsedMilliseconds);
            watch.Restart();

            Dictionary<string, bool> roles = new Dictionary<string, bool>();


            //if (userAccount.oems_MemberFlag ?? false)
            //    roles.Add(roleElements["member"], true);
            //else
            //    roles.Add(roleElements["nonmember"], true);


            if (userContact.oems_isnon_member ?? false)
                roles.Add(roleElements["nonmember"], true);
            else
                roles.Add(roleElements["member"], false);

            if (IsGuest(userContact, eventEntity))
                roles.Add(roleElements["guest"], true);

            if (IsPresident(userContact))
                roles.Add(roleElements["president"], true);

            AdjustFormElementToRole(localContext, ref element, formElementList, roles);

            Log.Info("AdjustFormElementToRole All Roles " + watch.ElapsedMilliseconds);
            watch.Restart();
        }

        public List<PropertyWithValue> GetProperties(LocalPluginContext localContext, Guid portalFormElementId, Dictionary<string, bool> roleElements)
        {
            List<PropertyWithValue> result = (from portalFormElement in localContext.OmniContext.oems_PortalFormElementSet
                                              join propertyValue in localContext.OmniContext.oems_FormElementPropertyValueSet on portalFormElement.Id equals propertyValue.oems_FormElement.Id
                                              join property in localContext.OmniContext.oems_FormElementPropertySet on propertyValue.oems_FormElementProperty.Id equals property.Id
                                              where portalFormElement.oems_PortalFormElementId == portalFormElementId
                                              select new PropertyWithValue
                                              {
                                                  Key = property.oems_PropertyCode,
                                                  Value = propertyValue.oems_PropertyValue
                                              }
                         ).ToList();
            return result;
        }

        public class PropertyWithValue
        {
            public Guid PortalFormElementId { get; set; }
            public Guid? PropertyId { get; set; }
            public Guid? PropertyValueId { get; set; }
            public string Key { get; set; }
            public int? Seq { get; set; }
            public object ConvertedValue { get; set; }  // avoid using it, left for historical reasons
            public string Value { get; set; }
        }

        //bool IsGuest(Account userAccount, oems_Event eventEntity)
        //{
        //    return eventEntity.oems_event_to_eventinvitedguest.Any(guest => guest.oems_Invitee.Id == userAccount.Id);
        //}

        bool IsGuest(Contact userContact, oems_Event eventEntity)
        {
            return eventEntity.oems_event_to_eventinvitedguest.Any(guest => guest.oems_Invitee.Id == userContact.Id);
        }

        bool IsPresident(Account userAccount)
        {

            const int PresidentRole = 347780000;
            const int PresidentDesignateRole = 347780001;

            return PresidentRole == userAccount.oems_Role || PresidentDesignateRole == userAccount.oems_Role;
        }

        bool IsPresident(Contact userContact)
        {
            bool result = false;
            mbr_local local = this.context.mbr_localSet.Where(x => x.mbr_PrimaryContact.Id == userContact.Id).SingleOrDefault<mbr_local>();

            if (local != null)
            {
                if (local.mbr_CurrentPresident != null)
                    result = local.mbr_CurrentPresident.Id == userContact.Id ? true : false;
            }
            return result;
        }

        protected bool IsPresident(Account userAccount, IOrganizationService service)
        {
            int? currValue = null;

            if (userAccount.oems_Role != null)
                currValue = userAccount.oems_Role.Value;

            Dictionary<String, int> values = GetNumericValues(service, userAccount.LogicalName, "oems_role");

            int president = values["President"];
            int presidentDesignate = values["President Designate"];

            if ((currValue == president) || (currValue == presidentDesignate))
            {
                return true;
            }


            return false;
        }

        protected Dictionary<String, int> GetNumericValues(IOrganizationService service, String entity, String attribute)
        {
            var sw = new Stopwatch(); sw.Start();
            RetrieveAttributeRequest request = new RetrieveAttributeRequest
            {
                EntityLogicalName = entity,
                LogicalName = attribute,
                RetrieveAsIfPublished = true
            };

            RetrieveAttributeResponse response = (RetrieveAttributeResponse)service.Execute(request);

            sw.Stop();
            Log.Debug("GetNumericValues " + entity + "." + attribute + " " + sw.ElapsedMilliseconds + " msec");
            switch (response.AttributeMetadata.AttributeType)
            {
                case AttributeTypeCode.Picklist:
                case AttributeTypeCode.State:
                case AttributeTypeCode.Status:
                    return ((EnumAttributeMetadata)response.AttributeMetadata).OptionSet.Options
                        .ToDictionary(key => key.Label.UserLocalizedLabel.Label, option => option.Value.Value);

                case AttributeTypeCode.Boolean:
                    Dictionary<String, int> values = new Dictionary<String, int>();

                    BooleanOptionSetMetadata metaData = ((BooleanAttributeMetadata)response.AttributeMetadata).OptionSet;

                    values[metaData.TrueOption.Label.UserLocalizedLabel.Label] = metaData.TrueOption.Value.Value;
                    values[metaData.FalseOption.Label.UserLocalizedLabel.Label] = metaData.FalseOption.Value.Value;

                    return values;

                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public void AdjustFormElementToRole(LocalPluginContext localContext, ref DataWork.FormElement element, oems_FormElementProperty[] propertyRoles)
        {
            bool shouldDelete = true;

            foreach (var e in element.children)
            {
                AdjustFormElementToRole(e, propertyRoles, out shouldDelete);

                if (shouldDelete)
                {
                    element.DeleteChildByName(e.name);
                }

            }

        }

        private Boolean deleteCheck(List<PropertyWithValue> properties, Dictionary<string, bool> roleElements)
        {
            bool shouldDelete = false;

            if (properties != null)
            {

                for (int i = 0; i < properties.Count; i++)
                {
                    foreach (KeyValuePair<string, bool> role in roleElements)
                    {
                        if (properties[i].Key.Contains(role.Key))
                        {
                            bool valueProp = true;
                            object elementValue = properties[i].Value;

                            if (elementValue != null && bool.TryParse(elementValue.ToString(), out valueProp))
                            {
                                if (valueProp == role.Value)
                                    shouldDelete = false;
                                else
                                    shouldDelete = true;

                                if (!shouldDelete)
                                {
                                    return shouldDelete;
                                }
                            }
                        }
                    }
                }
            }

            return shouldDelete;
        }

        private Boolean deleteCheck(Element elementBelongsToFormElement, Dictionary<string, bool> roleElements)
        {
            bool shouldDelete = false;

            if (elementBelongsToFormElement != null)
            {

                for (int i = 0; i < elementBelongsToFormElement.properties.Count; i++)
                {
                    foreach (KeyValuePair<string, bool> role in roleElements)
                    {
                        if (elementBelongsToFormElement.properties[i].ContainsKey(role.Key))
                        {
                            bool valueProp = true;
                            object elementValue = elementBelongsToFormElement.properties[i][role.Key];

                            if (elementValue != null && bool.TryParse(elementValue.ToString(), out valueProp))
                            {
                                if (valueProp == role.Value)
                                    shouldDelete = false;
                                else
                                    shouldDelete = true;

                                if (!shouldDelete)
                                {
                                    return shouldDelete;
                                }
                            }
                        }
                    }
                }
            }

            return shouldDelete;
        }

        public void AdjustFormElementToRole(LocalPluginContext localContext, ref DataWork.FormElement element, List<Element> elements, Dictionary<string, bool> roleElements)
        {

            DataWork.FormElement refElement;

            foreach (var e in element.children)
            {
                refElement = e;
                bool shouldDelete = false;

                //AdjustFormElementToRole(e, propertyRoles, out shouldDelete);
                Guid currID = Guid.Parse(e.name);
                Element elementBelongsToFormElement = findDescendant(currID, elements);

                if (elementBelongsToFormElement == null)
                {
                    List<PropertyWithValue> properties = GetProperties(localContext, currID, roleElements);
                    shouldDelete = deleteCheck(properties, roleElements);
                }
                else
                {
                    shouldDelete = deleteCheck(elementBelongsToFormElement, roleElements);
                }

                if (shouldDelete)
                {
                    element.DeleteChildByName(e.name);
                }
                else
                {
                    // adjust row tempaltes as well
                    if (e.options != null && e.options.newRowTemplate != null)
                    {
                        DataWork.FormElement refRowTemplateElement = e.options.newRowTemplate;
                        AdjustFormElementToRole(localContext, ref refRowTemplateElement, elements, roleElements);
                    }

                    AdjustFormElementToRole(localContext, ref refElement, elements, roleElements);
                }

            }
        }


        public Element findDescendant(Guid id, List<Element> nodelist)
        {
            Element result = null;
            foreach (Element element in nodelist)
            {
                if (element.elementId.Equals(id))
                    return element;
                else
                {
                    result = findDescendant(id, element.items);
                    if (result != null)
                    {
                        return result;
                    }
                }

            }
            return result;
        }



        public void AdjustFormElementToRole(DataWork.FormElement element, oems_FormElementProperty[] propertyRoles, out bool shouldDelete)
        {
            shouldDelete = true;


            for (int i = 0; i < propertyRoles.Length; i++)
            {
                foreach (oems_FormElementType type in propertyRoles[i].oems_formelementtype_to_formelementproperty)
                {

                    if (element.type == type.oems_SchemaTypeName)
                    {
                        shouldDelete = false;
                        break;
                    }
                }
                //}
            }

            if (shouldDelete)
            {
                element.DeleteChildByName(element.name);
            }
            else
            {
                foreach (var e in element.children)
                {
                    AdjustFormElementToRole(e, propertyRoles, out shouldDelete);

                    if (shouldDelete)
                    {
                        element.DeleteChildByName(e.name);
                    }

                }
            }

        }


        public static string JsonStringEscape(string s)
        {
            //return s;
            //Will do it on the portal side only
            return s.Replace("\n", "\\n").Replace("\r", "\\r");
        }


        private OptionValue[] CreateNoneOption(OptionValue[] currentOpions)
        {
            OptionValue newOption = new OptionValue()
            {
                label = "None",
                value = Guid.Empty.ToString(),
                selected = "false"
            };

            return AddOption(currentOpions, newOption);
        }



        private OptionValue[] AddOption(OptionValue[] currentOptions, OptionValue newOption)
        {
            OptionValue[] newOptions = new OptionValue[currentOptions.Length + 1];

            for (int i = 0; i < currentOptions.Length; i++)
            {
                newOptions[i] = currentOptions[i];
            }

            newOptions[newOptions.Length - 1] = newOption;

            return newOptions;
        }


    }
}

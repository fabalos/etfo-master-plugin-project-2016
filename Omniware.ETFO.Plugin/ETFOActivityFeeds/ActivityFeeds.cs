﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Client.Services;
using System.ServiceModel.Description;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using Xrm;

namespace ETFOActivityFeeds
{
    public class ActivityFeeds : IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {

            // create organization service 
            var context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
            var factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            var service = factory.CreateOrganizationService(context.UserId);

            EntityReference targetEntity = null;
            string relationshipName = string.Empty;
            EntityReferenceCollection relatedEntities = null;
            EntityReference relatedEntity = null;

            // Get the “Relationship” Key from context
            if (context.InputParameters.Contains("Relationship"))
            {
                relationshipName = context.InputParameters["Relationship"].ToString();
            }

            Entity messageEntity = GetMessageRule(service, context.MessageName.ToLower(), relationshipName);

            if (messageEntity.LogicalName != null)
            {
                // Get Entity 1 reference from “Target” Key from context
                if (context.InputParameters.Contains("Target") && context.InputParameters["Target"] is EntityReference)
                {
                    targetEntity = (EntityReference)context.InputParameters["Target"];
                }

                // Get Entity 2 reference from ” RelatedEntities” Key from context
                if (context.InputParameters.Contains("RelatedEntities") && context.InputParameters["RelatedEntities"] is EntityReferenceCollection)
                {
                    relatedEntities = context.InputParameters["RelatedEntities"] as EntityReferenceCollection;
                    relatedEntity = relatedEntities[0];
                }

                CreatePost(service, messageEntity, relatedEntity, targetEntity);
            }
        }

        private void CreatePost(IOrganizationService service, Entity messageEntity, EntityReference relatedEntity, EntityReference targetEntity)
        {
            //using (var crm = new XrmServiceContext(service))
            using (var crm = new OmniContext(service))
            {
                Entity relatedFullEntity = service.Retrieve(relatedEntity.LogicalName, relatedEntity.Id, new ColumnSet(true));

                string message = messageEntity.Attributes["oems_message"].ToString();
                string fieldNamePost = messageEntity.Attributes["oems_fieldname"].ToString();
                message = message.Replace("<name>", String.Format("@[{0},{1},\"{2}\"]",
                        GetObjectTypeCode(service, relatedEntity.LogicalName), relatedEntity.Id, relatedFullEntity.Attributes[fieldNamePost].ToString()));

                var entityPost = new Post
                {
                    RegardingObjectId = targetEntity,
                    
                    //Source. = new OptionSetValue(1),
                    Source = 1,
                    Text = message
                };

                crm.AddObject(entityPost);
                crm.SaveChanges();
            }
        }

        private string GetObjectTypeCode(IOrganizationService service, string entitylogicalname)
        {
            Entity entity = new Entity(entitylogicalname);
            RetrieveEntityRequest EntityRequest = new RetrieveEntityRequest();
            EntityRequest.LogicalName = entity.LogicalName;
            EntityRequest.EntityFilters = EntityFilters.All;
            RetrieveEntityResponse responseent = (RetrieveEntityResponse)service.Execute(EntityRequest);
            EntityMetadata ent = (EntityMetadata)responseent.EntityMetadata;
            string ObjectTypeCode = ent.ObjectTypeCode.ToString();
            return ObjectTypeCode;
        }

        private Entity GetMessageRule(IOrganizationService service, string action, string relationshipName)
        {
            Entity entity = new Entity();

            try
            {
                QueryExpression query = new QueryExpression("oems_messagerule");
                query.ColumnSet = new ColumnSet(true);
                query.Criteria.Conditions.Add(new ConditionExpression("oems_action", ConditionOperator.Equal, action));
                query.Criteria.Conditions.Add(new ConditionExpression("oems_relationshipname", ConditionOperator.Equal, relationshipName));
                query.Criteria.Conditions.Add(new ConditionExpression("oems_message", ConditionOperator.NotEqual, ""));
                query.Criteria.Conditions.Add(new ConditionExpression("oems_fieldname", ConditionOperator.NotEqual, ""));

                EntityCollection col = service.RetrieveMultiple(query);

                if (col.Entities.Count > 0)
                {
                    entity = service.RetrieveMultiple(query).Entities[0];
                }

                return entity;
            }
            catch (Exception ex)
            {
                throw new InvalidPluginExecutionException(ex.Message, ex.InnerException);
            }
        } 
    }
}

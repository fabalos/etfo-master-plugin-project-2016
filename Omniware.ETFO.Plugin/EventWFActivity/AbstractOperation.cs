﻿using EventWFActivity.Utilities;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Workflow;
using Shared.Utilities;
using System;
using System.Activities;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventWFActivity
{
    public abstract class AbstractOperation : IOperation
    {
        protected EventWFActivity wfActivity;
        protected CrmTools crmTools;
        protected Entity etfoSettings;

        public AbstractOperation( EventWFActivity _wfActivity)
        {
            wfActivity = _wfActivity;
            crmTools = new CrmTools(wfActivity.service);
            etfoSettings = crmTools.getEntity("oems_configuration", "statuscode", 1);
        }

        public abstract void operate();
    }
}

﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Workflow;
using System;
using System.Activities;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

/*
 * 
 *              Component Name:     EventWFActivity 
 *              
 *              Description:        This custom workflow activity is used for custom workflow step for CopyAndCreateEvent, 
 *                                  Publish Event MSCRM dialog process
 *              
 *              Author:             Lawrence Zhou
 *              Create Date:        Jan. 23th, 2014
 * 
 * */

namespace EventWFActivity
{
    public class EventWFActivity : CodeActivity
    {
        public const int RESULT_FAILED = 0;
        public const int RESULT_SUCCESS = 1;

        private const int OPERATION_COPY_EVENT = 1;
        private const int OPERATION_PUBLISH_EVENT = 2;
        private const int OPERATION_SECTION_EVENT = 3;

        public IOrganizationService service;
        public CodeActivityContext context;

        //for merge shared dll
        static EventWFActivity()
        {
          AppDomain.CurrentDomain.AssemblyResolve  += OnResolveAssembly;
        }

        public EventWFActivity()
        {

        }

        [Input("Operation Code")]
        [Default("1")]
        public InArgument<int> OperationCode { get; set; }

        [Input("Selected Event")]
        [ReferenceTarget("oems_event")]
        public InArgument<EntityReference> SelectedEvent { get; set; }

        #region Copy Event Input Properties

        [Input("New Event Name")]
        [Default("New Event")]
        public InArgument<string> NewEventName { get; set; }

        #endregion

        [Output("ResultCode")]
        public OutArgument<int> ResultCode { get; set; }

        [Output("ResultMessage")]
        public OutArgument<string> ResultMessage { get; set; }

        protected override void Execute(CodeActivityContext _context)
        {
            context = _context;

            //-----------------------------------
            // Workflow Context & Service Objects
            //-----------------------------------
            IWorkflowContext wfContext = context.GetExtension<IWorkflowContext>();
            IOrganizationServiceFactory serviceFactory = context.GetExtension<IOrganizationServiceFactory>();
            service = serviceFactory.CreateOrganizationService(wfContext.InitiatingUserId);

            int optCode = OperationCode.Get<int>(context);

            IOperation op = getOperation(optCode);
            
            if(op != null)
            {
                op.operate();
            }
            
        }

        private IOperation  getOperation(int optCode)
        {
            IOperation ret = null;

            switch (optCode)
            {
                case OPERATION_COPY_EVENT:
                    ret = getCopyEventOperation();
                    break;
                case OPERATION_PUBLISH_EVENT:
                    ret = getPublishEventOperation();
                    break;
                case OPERATION_SECTION_EVENT:
                    ret = getSectionEventOperation();
                    break;
            }

            return ret;
        }

        private IOperation getPublishEventOperation()
        {
            IOperation ret = null;

            try
            {
                //--------------------------
                // Obtain the Input Argument
                // Event EntityReference
                //--------------------------
                EntityReference selectedEventRef = SelectedEvent.Get<EntityReference>(context);

                ret = new PublishEventOperation(selectedEventRef, this);
            }
            catch (Exception ex)
            {
                throw ex; //remove this line if log the errors
                //TODO: Log Errors Here
            }

            return ret;
        }

        private IOperation getCopyEventOperation()
        {
            IOperation ret = null;

            try
            {
                //--------------------------
                // Obtain the Input Argument
                // Event EntityReference
                //--------------------------
                EntityReference selectedEventRef = SelectedEvent.Get<EntityReference>(context);
                string newName = NewEventName.Get<string>(context);

                ret = new CopyEventOperation(newName, selectedEventRef, this);

                //---------------------------
                // Retrieve the Event based
                // on the EntityReference Id
                //---------------------------
   /*             Entity oemsEvent = service.Retrieve(selectedEventRef.LogicalName, selectedEventRef.Id, new Microsoft.Xrm.Sdk.Query.ColumnSet(true));

                if (oemsEvent != null)
                {
                    string newName = NewEventName.Get<string>(context);
                    if (string.IsNullOrEmpty(newName) || string.IsNullOrWhiteSpace(newName))
                    {
                        newName = "New Event";
                    }
                    oemsEvent.Id = Guid.NewGuid();
                    oemsEvent.Attributes["oems_eventid"] = oemsEvent.Id;
                    oemsEvent.Attributes["oems_eventname"] = newName;
                    service.Create(oemsEvent);

                    //                    EntityReference entityRef = (EntityReference)oemsEvent.Attributes["oems_subcategory"];
                    //                    this.SubcategoryRef.Set(context, entityRef);
                }*/
            }
            catch (Exception ex)
            {
                throw ex; //remove this line if log the errors
                //TODO: Log Errors Here
            }

            return ret;
        }

        private IOperation getSectionEventOperation()
        {
            IOperation ret = null;

            try
            {
                //--------------------------
                // Obtain the Input Argument
                // Event EntityReference
                //--------------------------
                EntityReference selectedEventRef = SelectedEvent.Get<EntityReference>(context);
                string newName = NewEventName.Get<string>(context);

                ret = new CopyEventOperation(newName, selectedEventRef, this, true);
            }
            catch (Exception ex)
            {
                throw ex; //remove this line if log the errors
                //TODO: Log Errors Here
            }

            return ret;
        }

        //for merge shared dll
        private static Assembly OnResolveAssembly(object sender, ResolveEventArgs args)
        {

            Assembly executingAssembly = Assembly.GetExecutingAssembly();
            AssemblyName assemblyName = new AssemblyName(args.Name);
            string path = assemblyName.Name + ".dll";

            if (assemblyName.CultureInfo.Equals(CultureInfo.InvariantCulture) == false)
            {
                path = String.Format(@"{0}\{1}", assemblyName.CultureInfo, path);
            }

            using (Stream stream = executingAssembly.GetManifestResourceStream(path))
            {
                if (stream == null)
                    return null;
                byte[] assemblyRawBytes = new byte[stream.Length];
                stream.Read(assemblyRawBytes, 0, assemblyRawBytes.Length);
                return Assembly.Load(assemblyRawBytes);
            }
        }
    }
}

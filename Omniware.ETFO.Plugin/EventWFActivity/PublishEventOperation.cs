﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Workflow;
using System;
using System.Activities;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using EventWFActivity.Utilities;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Crm.Sdk.Messages;
using Shared.Utilities;
using Microsoft.Xrm.Sdk.Messages;
using Omniware.ETFO.Plugin;

namespace EventWFActivity
{
    class ErrorFieldMessage{
        public string FieldName {get;set;}
        public string ErrorMessage {get;set;}
    };

    public class StatusCode
    {
        //Invited Guest
        public const int InvitedSent = 347780000;
        public const int Accept = 347780001;

        //Local Delegation
        public const int Approved = 347780000;
    };

    public class PublishEventOperation : AbstractOperation
    {
        private EntityReference selectedEventRef;
        private ArrayList errorFieldList = new ArrayList();

        private StringManager stringManager;
        private string lcId;

        public PublishEventOperation(EntityReference _selectedEventRef, EventWFActivity _wfActivity)
            : base(_wfActivity)
        {
            selectedEventRef = _selectedEventRef;
            stringManager = new StringManager(_wfActivity.service);
        }

        public override void operate()
        {
            //try
            //{
                //---------------------------
                // Retrieve the Event based
                // on the EntityReference Id
                //---------------------------
                Entity oemsEvent = wfActivity.service.Retrieve(selectedEventRef.LogicalName, selectedEventRef.Id, new Microsoft.Xrm.Sdk.Query.ColumnSet(true));

                if (oemsEvent != null)
                {
                    // Validate the event input
                    if(validate(oemsEvent))
                    {
                        // if successful
                        // set event status to published


                        if (((OptionSetValue)oemsEvent["statuscode"]).Value == 1 && oemsEvent.GetAttributeValue<bool>("oems_workshopapplicationcomponentflag"))
                        {
                            oemsEvent["statuscode"] = new OptionSetValue(347780001);
                        }
                        else
                        {
                            oemsEvent["statuscode"] = new OptionSetValue(347780004);
                        }

                        //set the publishing state of event and related web page to "Published"
                        //oemsEvent["oems_webpagepublishingstate"] = crmTools.getEntity("adx_publishingstate", "adx_name", "Published").ToEntityReference();//new EntityReference("adx_publishingstate", new Guid("C20DD235-278E-4207-9A9A-F65185B392DC"));
                        if (etfoSettings != null)
                        {
                            oemsEvent["oems_webpagepublishingstate"] = CrmTools.getEnitiyAttributeSafely(etfoSettings, "oems_webpagepublishedstate");
                        }
                        else
                        {
                            oemsEvent["oems_webpagepublishingstate"] = crmTools.getEntity("adx_publishingstate", "adx_name", "Published").ToEntityReference();
                        }

                        wfActivity.service.Update(oemsEvent);

                        if (!ValidationTools.isSingleValueFieldEmpty(oemsEvent, "oems_webpage"))
                        {
                            EntityReference webpageRef = (EntityReference)oemsEvent["oems_webpage"];

                            Entity webpage = wfActivity.service.Retrieve(webpageRef.LogicalName, webpageRef.Id, new ColumnSet(true));
                            webpage["adx_publishingstateid"] = CrmTools.getEnitiyAttributeSafely(oemsEvent, "oems_webpagepublishingstate");//new EntityReference("adx_publishingstate", new Guid("C20DD235-278E-4207-9A9A-F65185B392DC"));
                            wfActivity.service.Update(webpage);
                        }

                        // set result code to success, and message indicating publishing is successful
                        this.wfActivity.ResultCode.Set(this.wfActivity.context, 1);
                        this.wfActivity.ResultMessage.Set(this.wfActivity.context, stringManager.getString(StringCodes.THIS_EVENT_HAS_BEEN_PUBLISHED, lcId));//"This event has been published.");

                    }else{
                        // if failed, set the result code to failed, set message indicating publishing is failed and which fields are invalid
                        string errorMessage = stringManager.getString(StringCodes.FOLLOWING_FIELDS_ARE_INVALID, lcId) + ":\n\n";//"Following fields are invalid:\n\n";

                        foreach(ErrorFieldMessage error in errorFieldList){
                            errorMessage += error.FieldName + ": ";
                            errorMessage += error.ErrorMessage + "\n";
                        }

                        errorMessage += "\n" + stringManager.getString(StringCodes.MODIFY_FIELDS_AND_PUBLISH_AGAIN, lcId);//"\nPlease modify invalid fields and publish again.";

                        this.wfActivity.ResultCode.Set(this.wfActivity.context, 0);
                        this.wfActivity.ResultMessage.Set(this.wfActivity.context, errorMessage);
                    }

                }
            //}
            //catch (Exception ex)
            //{
            //    throw ex;
            //    //TODO: Log Errors Here
            //}
        }

        private bool validate(Entity oemsEvent)
        {
            bool ret = true;

            //Course
            bool courseFlag = CrmTools.entityAttributeExists(oemsEvent, "oems_courseflag") ? (bool)oemsEvent["oems_courseflag"] : false;

            //CourseTerm
            bool courseTermFlag = CrmTools.entityAttributeExists(oemsEvent, "oems_coursetermflag") ? (bool)oemsEvent["oems_coursetermflag"] : false;

            // IsApplication
            bool flagWorkshopApplication = CrmTools.entityAttributeExists(oemsEvent, "oems_workshopapplicationcomponentflag") ? (bool)oemsEvent["oems_workshopapplicationcomponentflag"] : false;

            #region General

            //oems_eventname
            if (ValidationTools.isSingleValueFieldEmpty(oemsEvent, "oems_eventname"))
            {
                ret = false;
                ErrorFieldMessage error = new ErrorFieldMessage() { FieldName = stringManager.getString(StringCodes.FIELDNAME_EVENT_NAME, lcId), ErrorMessage = stringManager.getString(StringCodes.EVENT_NAME_EMPTY, lcId) };//"Event name is empty" };
                errorFieldList.Add(error);
            }
 
            //oems_subcategory
            if (ValidationTools.isSingleValueFieldEmpty(oemsEvent, "oems_subcategory"))
            {
                ret = false;
                ErrorFieldMessage error = new ErrorFieldMessage() { FieldName = stringManager.getString(StringCodes.FIELDNAME_SUBCATEGORY, lcId), ErrorMessage = stringManager.getString(StringCodes.SUBCATEGORY_EMPTY, lcId) };
                errorFieldList.Add(error);
            }

            //oems_eventyear
            OptionSetValue eventYear = null;
            if (ValidationTools.isSingleValueFieldEmpty(oemsEvent, "oems_eventyear"))
            {
                ret = false;
                ErrorFieldMessage error = new ErrorFieldMessage() { FieldName = stringManager.getString(StringCodes.FIELDNAME_EVENT_YEAR, lcId), ErrorMessage = stringManager.getString(StringCodes.EVENT_YEAR_EMPTY, lcId) };
                errorFieldList.Add(error);
            }
            else
            {
                eventYear = CrmTools.entityAttributeExists(oemsEvent, "oems_eventyear") ? (OptionSetValue)oemsEvent["oems_eventyear"] : null;
                if (ValidationTools.isYearBeforeCurrentYear(eventYear.Value))
                {
                    //The Year cannot be before the current Year
                    ret = false;
                    ErrorFieldMessage error = new ErrorFieldMessage() { FieldName = stringManager.getString(StringCodes.FIELDNAME_EVENT_YEAR, lcId), ErrorMessage = stringManager.getString(StringCodes.EVENT_YEAR_BEFORE_CURRENT_YEAR, lcId) };
                    errorFieldList.Add(error);
                }
            }

            if (!courseFlag && !courseTermFlag)
            {
                //oems_eventdescription
                if (ValidationTools.isSingleValueFieldEmpty(oemsEvent, "oems_eventdescription"))
                {
                    ret = false;
                    ErrorFieldMessage error = new ErrorFieldMessage() { FieldName = stringManager.getString(StringCodes.FIELDNAME_EVENT_DESCRIPTION, lcId), ErrorMessage = stringManager.getString(StringCodes.EVENT_DESCRIPTION_EMPTY, lcId) };
                    errorFieldList.Add(error);
                }
            }

            if(!courseFlag)
            {
                //oems_startdate, oems_enddate
                DateTime startDate = CrmTools.entityAttributeExists(oemsEvent, "oems_startdate") ? (DateTime)oemsEvent.Attributes["oems_startdate"] : DateTime.MinValue;
                DateTime endDate = CrmTools.entityAttributeExists(oemsEvent, "oems_enddate") ? (DateTime)oemsEvent.Attributes["oems_enddate"] : DateTime.MinValue;

                if (startDate == DateTime.MinValue || endDate == DateTime.MinValue)
                {
                    ret = false;
                    ErrorFieldMessage error = new ErrorFieldMessage() { FieldName = stringManager.getString(StringCodes.FIELDNAME_START_END_DATE, lcId), ErrorMessage = stringManager.getString(StringCodes.START_END_DATE_EMPTY, lcId) };
                    errorFieldList.Add(error);
                }
                else if (ValidationTools.isDateBeforToday(endDate))
                {
                    //Start Date cannot be before Today 
                    ret = false;
                    ErrorFieldMessage error = new ErrorFieldMessage() { FieldName = stringManager.getString(StringCodes.FIELDNAME_START_END_DATE, lcId), ErrorMessage = stringManager.getString(StringCodes.END_DATE_BEFORE_TODAY, lcId) };
                    errorFieldList.Add(error);
                }
                else if (ValidationTools.isDateBeforDate(endDate, startDate))
                {
                    //The End Date cannot be before the Start Date 
                    ret = false;
                    ErrorFieldMessage error = new ErrorFieldMessage() { FieldName = stringManager.getString(StringCodes.FIELDNAME_START_END_DATE, lcId), ErrorMessage = stringManager.getString(StringCodes.END_DATE_BEFORE_START_DATE, lcId) };
                    errorFieldList.Add(error);
                }
            }


            if (!courseTermFlag)
            {
                if (!courseFlag)
                {
                    //oems_eventlocation
                    if (ValidationTools.isSingleValueFieldEmpty(oemsEvent, "oems_eventlocation"))
                    {
                        ret = false;
                        ErrorFieldMessage error = new ErrorFieldMessage() { FieldName = stringManager.getString(StringCodes.FIELDNAME_EVENT_LOCATION, lcId), ErrorMessage = stringManager.getString(StringCodes.EVENT_LOCATION_EMPTY, lcId) };
                        errorFieldList.Add(error);
                    }
                    else
                    {
                        EntityReference eventLocation = (EntityReference)oemsEvent.Attributes["oems_eventlocation"];
                        Entity location = wfActivity.service.Retrieve(eventLocation.LogicalName, eventLocation.Id, new Microsoft.Xrm.Sdk.Query.ColumnSet(true));
                        if (!ValidationTools.isValidLocation(location))
                        {
                            ret = false;
                            ErrorFieldMessage error = new ErrorFieldMessage() { FieldName = stringManager.getString(StringCodes.FIELDNAME_EVENT_LOCATION, lcId), ErrorMessage = stringManager.getString(StringCodes.EVENT_LOCATION_INVALID, lcId) };
                            errorFieldList.Add(error);
                        }
                    }
                }
                

                //oems_budgetnumber
                if (ValidationTools.isSingleValueFieldEmpty(oemsEvent, "oems_budgetnumber"))
                {
                    ret = false;
                    ErrorFieldMessage error = new ErrorFieldMessage() { FieldName = stringManager.getString(StringCodes.FIELDNAME_BUDGET_NUMBER, lcId), ErrorMessage = stringManager.getString(StringCodes.BUDGET_NUMBER_EMPTY, lcId) };
                    errorFieldList.Add(error);
                }
            }
            else
            {
                //Web Term Description
                if (!flagWorkshopApplication && ValidationTools.isSingleValueFieldEmpty(oemsEvent, "oems_webtermdescription"))
                {
                    ret = false;
                    ErrorFieldMessage error = new ErrorFieldMessage() { FieldName = stringManager.getString(StringCodes.FIELDNAME_WEB_TERM_DESCRIPTION, lcId), ErrorMessage = stringManager.getString(StringCodes.WEB_TERM_DESCRIPTION_EMPTY, lcId) };
                    errorFieldList.Add(error);
                }
            }

            if (!courseFlag)
            {
                //oems_servicearea
                if (ValidationTools.isSingleValueFieldEmpty(oemsEvent, "oems_servicearea"))
                {
                    ret = false;
                    ErrorFieldMessage error = new ErrorFieldMessage() { FieldName = stringManager.getString(StringCodes.FIELDNAME_SERVICE_AREA, lcId), ErrorMessage = stringManager.getString(StringCodes.SERVICE_AREA_EMPTY, lcId) };
                    errorFieldList.Add(error);
                }
            }

            //oems_event_executive_staff_owners 
            if (!ValidationTools.isRelatedEntitiesEmpty(wfActivity.service, 
                new ColumnSet("oems_eventid"),
                selectedEventRef.LogicalName, 
                selectedEventRef.Id, 
                "systemuser", 
                new ConditionExpression("isdisabled", ConditionOperator.Equal, false), 
                "oems_event_executive_staff_owners"))
            {
                ret = false;
                ErrorFieldMessage error = new ErrorFieldMessage() { FieldName = stringManager.getString(StringCodes.FIELDNAME_EXECUTIVE_STAFF, lcId), ErrorMessage = stringManager.getString(StringCodes.EXECUTIVE_STAFF_LIST_EMPTY, lcId)};
                errorFieldList.Add(error);
            }

            //oems_event_support_staff_owners 
            if (!ValidationTools.isRelatedEntitiesEmpty(wfActivity.service,
                new ColumnSet("oems_eventid"),
                selectedEventRef.LogicalName,
                selectedEventRef.Id,
                "systemuser",
                new ConditionExpression("isdisabled", ConditionOperator.Equal, false),
                "oems_event_support_staff_owners"))
            {
                ret = false;
                ErrorFieldMessage error = new ErrorFieldMessage() { FieldName = stringManager.getString(StringCodes.FIELDNAME_SUPPORT_STAFF, lcId), ErrorMessage = stringManager.getString(StringCodes.SUPPORT_STAFF_LIST_EMPTY, lcId)};
                errorFieldList.Add(error);
            }

            #endregion

            #region Course

            if (courseFlag)
            {
                //Validate
                if (ValidationTools.isSingleValueFieldEmpty(oemsEvent, "oems_courseterm"))
                {
                    ret = false;
                    ErrorFieldMessage error = new ErrorFieldMessage() { FieldName = stringManager.getString(StringCodes.FIELDNAME_COURSE_TERM, lcId), ErrorMessage = stringManager.getString(StringCodes.COURSE_TERM_EMPTY, lcId) };
                    errorFieldList.Add(error);
                }

                if (ValidationTools.isSingleValueFieldEmpty(oemsEvent, "oems_coursecode"))
                {
                    ret = false;
                    ErrorFieldMessage error = new ErrorFieldMessage() { FieldName = stringManager.getString(StringCodes.FIELDNAME_COURSE_CODE, lcId), ErrorMessage = stringManager.getString(StringCodes.COURSE_CODE_EMPTY, lcId) };
                    errorFieldList.Add(error);
                }

                if (ValidationTools.isSingleValueFieldEmpty(oemsEvent, "oems_coursetopic"))
                {
                    ret = false;
                    ErrorFieldMessage error = new ErrorFieldMessage() { FieldName = stringManager.getString(StringCodes.FIELDNAME_COURSE_TOPIC, lcId), ErrorMessage = stringManager.getString(StringCodes.COURSE_TOPIC_EMPTY, lcId) };
                    errorFieldList.Add(error);
                }

                if (ValidationTools.isSingleValueFieldEmpty(oemsEvent, "oems_coursetargetgrades"))
                {
                    ret = false;
                    ErrorFieldMessage error = new ErrorFieldMessage() { FieldName = stringManager.getString(StringCodes.FIELDNAME_COURSE_TARGET_GRADES, lcId), ErrorMessage = stringManager.getString(StringCodes.COURSE_TARGET_GRADES_EMPTY, lcId) };
                    errorFieldList.Add(error);
                }

                if (ValidationTools.isSingleValueFieldEmpty(oemsEvent, "oems_coursedateslot"))
                {
                    ret = false;
                    ErrorFieldMessage error = new ErrorFieldMessage() { FieldName = stringManager.getString(StringCodes.FIELDNAME_COURSE_DATE_SLOT, lcId), ErrorMessage = stringManager.getString(StringCodes.COURSE_DATE_SLOT_EMPTY, lcId) };
                    errorFieldList.Add(error);
                }

                if (ValidationTools.isSingleValueFieldEmpty(oemsEvent, "oems_coursetimeslot"))
                {
                    ret = false;
                    ErrorFieldMessage error = new ErrorFieldMessage() { FieldName = stringManager.getString(StringCodes.FIELDNAME_COURSE_TIME_SLOT, lcId), ErrorMessage = stringManager.getString(StringCodes.COURSE_TIME_SLOT_EMPTY, lcId) };
                    errorFieldList.Add(error);
                }

                if (ValidationTools.isSingleValueFieldEmpty(oemsEvent, "oems_courselocation"))
                {
                    ret = false;
                    ErrorFieldMessage error = new ErrorFieldMessage() { FieldName = stringManager.getString(StringCodes.FIELDNAME_COURSE_LOCATION, lcId), ErrorMessage = stringManager.getString(StringCodes.COURSE_LOCATION_EMPTY, lcId) };
                    errorFieldList.Add(error);
                }

                if (ValidationTools.isSingleValueFieldEmpty(oemsEvent, "oems_courselocalhost"))
                {
                    ret = false;
                    ErrorFieldMessage error = new ErrorFieldMessage() { FieldName = stringManager.getString(StringCodes.FIELDNAME_COURSE_LOCAL_HOST, lcId), ErrorMessage = stringManager.getString(StringCodes.COURSE_LOCAL_HOST_EMPTY, lcId) };
                    errorFieldList.Add(error);
                }
            }

            #endregion

            #region Registration Application
            //Registration Minimum must be less that or equal to Registration Maximum
            //oems_registrationminimum/oems_registrationmaximum 238
            bool flag = CrmTools.entityAttributeExists(oemsEvent, "oems_registrationcomponentflag") ? (bool)oemsEvent["oems_registrationcomponentflag"] : false;
            if(flag)
            {
                int regMIn = 0;
                var hasRegMin = CrmTools.entityAttributeExists(oemsEvent, "oems_registrationminimum");
                if(!hasRegMin)
                {
                    ret = false;
                    ErrorFieldMessage error = new ErrorFieldMessage() { FieldName = stringManager.getString(StringCodes.FIELDNAME_REG_MIN_MAX, lcId), ErrorMessage = stringManager.getString(StringCodes.REG_MIN_OR_MAX_EMPTY, lcId)};
                    errorFieldList.Add(error);
                }
                else
                {
                    regMIn = (int)oemsEvent.Attributes["oems_registrationminimum"];
                }

                int regMax = 0;
                var hasRegMax = CrmTools.entityAttributeExists(oemsEvent, "oems_registrationmaximum");
                if(!hasRegMax)
                {
                    ret = false;
                    ErrorFieldMessage error = new ErrorFieldMessage() { FieldName = stringManager.getString(StringCodes.FIELDNAME_REG_MIN_MAX, lcId), ErrorMessage = stringManager.getString(StringCodes.REG_MIN_OR_MAX_EMPTY, lcId)};
                    errorFieldList.Add(error);
                }
                else
                {
                    regMax = (int)oemsEvent.Attributes["oems_registrationmaximum"];
                }


                if(regMax < regMIn)
                {
                    //Registration Minimum must be less that or equal to Registration Maximum
                    ret = false;
                    ErrorFieldMessage error = new ErrorFieldMessage() { FieldName = stringManager.getString(StringCodes.FIELDNAME_REG_MIN_MAX, lcId), ErrorMessage = stringManager.getString(StringCodes.REG_MIN_GREATER_THAN_MAX, lcId)};
                    errorFieldList.Add(error);
                }

                //#region Registration Application Per Registrant Type
                //oems_registrationopening/oems_registrationdeadline
                DateTime regOpenDate = CrmTools.entityAttributeExists(oemsEvent, "oems_registrationopening") ? (DateTime)oemsEvent.Attributes["oems_registrationopening"] : DateTime.MinValue;
                DateTime regDeadlineDate = CrmTools.entityAttributeExists(oemsEvent, "oems_registrationdeadline") ? (DateTime)oemsEvent.Attributes["oems_registrationdeadline"] : DateTime.MinValue;
            
                if (regOpenDate == DateTime.MinValue || regDeadlineDate == DateTime.MinValue)
                {   //registration start/end date is mandatory
                    ret = false;
                    ErrorFieldMessage error = new ErrorFieldMessage() { FieldName = stringManager.getString(StringCodes.FIELDNAME_REG_OPEN_DEADLINE, lcId), ErrorMessage = stringManager.getString(StringCodes.REG_OPEN_DEADLINE_EMPTY, lcId)};
                    errorFieldList.Add(error);
                }
                else if (ValidationTools.isDateBeforToday(regOpenDate))
                {   //Registration Start Date cannot be before Today
                    ret = false;
                    ErrorFieldMessage error = new ErrorFieldMessage() { FieldName = stringManager.getString(StringCodes.FIELDNAME_REG_OPEN, lcId), ErrorMessage = stringManager.getString(StringCodes.REG_OPEN_BEFORE_TODAY, lcId)};
                    errorFieldList.Add(error);
                }
                else if (ValidationTools.isDateBeforDate(regDeadlineDate, regOpenDate))
                {   //Registration Start Date cannot be after the Registration End Date
                    ret = false;
                    ErrorFieldMessage error = new ErrorFieldMessage() { FieldName = stringManager.getString(StringCodes.FIELDNAME_REG_OPEN_DEADLINE, lcId), ErrorMessage = stringManager.getString(StringCodes.REG_OPEN_AFTER_DEADLINE, lcId)};
                    errorFieldList.Add(error);
                }

                //oems_event_to_eventregistrationschedule, oems_eventregistrationschedule
                DataCollection<Entity> eventRegSchedules = crmTools.getRelatedEntities(
                                                        new ColumnSet("oems_eventid"),
                                                        selectedEventRef.LogicalName,
                                                        selectedEventRef.Id,
                                                        "oems_eventregistrationschedule",
                                                        "oems_event_to_eventregistrationschedule", new ConditionExpression("statuscode", ConditionOperator.Equal, 1), new ConditionExpression("oems_registrationtype", ConditionOperator.Equal, 347780000));
                if (eventRegSchedules == null)
                { // Must have at least one Registration Application Per Registrant Type defined
                    ret = false;
                    ErrorFieldMessage error = new ErrorFieldMessage() { FieldName = stringManager.getString(StringCodes.FIELDNAME_EVENT_REG_SCHEDULES, lcId), ErrorMessage = stringManager.getString(StringCodes.REG_SCHEDULE_LIST_EMPTY, lcId)};
                    errorFieldList.Add(error);
                }
                else
                {
                    foreach(Entity schedule in eventRegSchedules)
                    {
                        string regScheduleName = CrmTools.entityAttributeExists(schedule, "oems_registrationschedulename") ? (string)schedule["oems_registrationschedulename"] : ""; //oems_registrationschedulename
                        if(regOpenDate != null && regOpenDate != DateTime.MinValue)
                        {
                            if (schedule.Contains("oems_registrationopendatetime") && schedule["oems_registrationopendatetime"] != null)
                            {
                                DateTime scheduleStart = (DateTime)schedule["oems_registrationopendatetime"];

                                if(scheduleStart < regOpenDate){
                                    //Registration Start Date cannot be before the Start Date
                                    ret = false;
                                    ErrorFieldMessage error = new ErrorFieldMessage() { FieldName = stringManager.getString(StringCodes.FIELDNAME_REG_SCHEDULE, lcId), ErrorMessage = string.Format(stringManager.getString(StringCodes.SCHEDULE_START_BEFORE_OPEN, lcId), regScheduleName) };
                                    errorFieldList.Add(error);
                                }else if(scheduleStart < DateTime.Now){
                                    //Registration Start Date cannot be before Today
                                    ret = false;
                                    ErrorFieldMessage error = new ErrorFieldMessage() { FieldName = stringManager.getString(StringCodes.FIELDNAME_REG_SCHEDULE, lcId), ErrorMessage = string.Format(stringManager.getString(StringCodes.SCHEDULE_START_BEFORE_TODAY, lcId), regScheduleName) };
                                    errorFieldList.Add(error);
                                }
                                else if (scheduleStart > regDeadlineDate)
                                {
                                    //Registration Start Date cannot be after the Registration End Date
                                    ret = false;
                                    ErrorFieldMessage error = new ErrorFieldMessage() { FieldName = stringManager.getString(StringCodes.FIELDNAME_REG_SCHEDULE, lcId), ErrorMessage = string.Format(stringManager.getString(StringCodes.SCHEDULE_START_AFTER_DEADLINE, lcId), regScheduleName) };
                                    errorFieldList.Add(error);
                                }
                            }else{
                                //registration start date is mandatory
                                ret = false;
                                ErrorFieldMessage error = new ErrorFieldMessage() { FieldName = stringManager.getString(StringCodes.FIELDNAME_REG_SCHEDULE, lcId), ErrorMessage = string.Format(stringManager.getString(StringCodes.SCHEDULE_START_EMPTY, lcId), regScheduleName) };
                                errorFieldList.Add(error);
                            }
                        }

                        if (regDeadlineDate != null && regDeadlineDate != DateTime.MinValue)
                        {
                            if (schedule.Contains("oems_registrationopendatetime") && schedule["oems_registrationopendatetime"] != null)
                            {
                                DateTime scheduleEnd = (DateTime)schedule["oems_registrationopendatetime"];
                                if (scheduleEnd > regDeadlineDate)
                                {
                                    //Registration End Date cannot be after the End Date
                                    ret = false;
                                    ErrorFieldMessage error = new ErrorFieldMessage() { FieldName = stringManager.getString(StringCodes.FIELDNAME_REG_SCHEDULE, lcId), ErrorMessage = string.Format(stringManager.getString(StringCodes.SCHEDULE_END_AFTER_DEADLINE, lcId), regScheduleName) };
                                    errorFieldList.Add(error);
                                }
                            }else{
                                //registration end date is mandatory
                                ret = false;
                                ErrorFieldMessage error = new ErrorFieldMessage() { FieldName = stringManager.getString(StringCodes.FIELDNAME_REG_SCHEDULE, lcId), ErrorMessage = string.Format(stringManager.getString(StringCodes.SCHEDULE_START_EMPTY, lcId), regScheduleName) };
                                errorFieldList.Add(error);
                            }
                        }
                    }
                }

                //Must have a Registration Form Template
                //oems_event_to_formtemplate relationship
                /*if (!ValidationTools.isRelatedEntitiesEmpty(wfActivity.service,
                    new ColumnSet("oems_eventid"),
                    selectedEventRef.LogicalName,
                    selectedEventRef.Id,
                    "oems_formtemplate",
                    new ConditionExpression("statuscode", ConditionOperator.Equal, 1),
                    "oems_event_to_formtemplate"))*/
                if (ValidationTools.isSingleValueFieldEmpty(oemsEvent, "oems_registrationtemplate"))
                {//Must have a Registration Form Template
                    ret = false;
                    ErrorFieldMessage error = new ErrorFieldMessage() { FieldName = stringManager.getString(StringCodes.FIELDNAME_EVENT_TEMPLATES, lcId), ErrorMessage = stringManager.getString(StringCodes.EVENT_TEMPLATES_EMPTY, lcId) };
                    errorFieldList.Add(error);
                }
            }

            #endregion

            #region Presenter Application
          
            //Registration Minimum must be less that or equal to Registration Maximum
            //oems_registrationminimum/oems_registrationmaximum 238
            if (flagWorkshopApplication)
            {
                int regMIn = CrmTools.entityAttributeExists(oemsEvent, "oems_applicationminimun") ? (int)oemsEvent.Attributes["oems_applicationminimun"] : Int16.MinValue;
                int regMax = CrmTools.entityAttributeExists(oemsEvent, "oems_applicationmaximun") ? (int)oemsEvent.Attributes["oems_applicationmaximun"] : Int16.MaxValue;

                if (regMax < regMIn)
                {
                    //Registration Minimum must be less that or equal to Registration Maximum
                    ret = false;
                    ErrorFieldMessage error = new ErrorFieldMessage() { FieldName = stringManager.getString(StringCodes.FIELDNAME_WORK_MIN_MAX, lcId), ErrorMessage = stringManager.getString(StringCodes.WORK_MIN_GREATER_THAN_MAX, lcId) };
                    errorFieldList.Add(error);
                }

                DateTime regOpenDate = CrmTools.entityAttributeExists(oemsEvent, "oems_applicationopening") ? (DateTime)oemsEvent.Attributes["oems_applicationopening"] : DateTime.MinValue;
                DateTime regDeadlineDate = CrmTools.entityAttributeExists(oemsEvent, "oems_applicationdeadline") ? (DateTime)oemsEvent.Attributes["oems_applicationdeadline"] : DateTime.MinValue;

                if (regOpenDate == DateTime.MinValue || regDeadlineDate == DateTime.MinValue)
                {   //registration start/end date is mandatory
                    ret = false;
                    ErrorFieldMessage error = new ErrorFieldMessage() { FieldName = stringManager.getString(StringCodes.FIELDNAME_WORK_OPEN_DEADLINE, lcId), ErrorMessage = stringManager.getString(StringCodes.WORK_OPEN_DEADLINE_EMPTY, lcId) };
                    errorFieldList.Add(error);
                }
                else if (ValidationTools.isDateBeforToday(regOpenDate))
                {   //Registration Start Date cannot be before Today
                    ret = false;
                    ErrorFieldMessage error = new ErrorFieldMessage() { FieldName = stringManager.getString(StringCodes.FIELDNAME_WORK_OPEN, lcId), ErrorMessage = stringManager.getString(StringCodes.WORK_OPEN_BEFORE_TODAY, lcId) };
                    errorFieldList.Add(error);
                }
                else if (ValidationTools.isDateBeforDate(regDeadlineDate, regOpenDate))
                {   //Registration Start Date cannot be after the Registration End Date
                    ret = false;
                    ErrorFieldMessage error = new ErrorFieldMessage() { FieldName = stringManager.getString(StringCodes.FIELDNAME_WORK_OPEN_DEADLINE, lcId), ErrorMessage = stringManager.getString(StringCodes.WORK_OPEN_AFTER_DEADLINE, lcId) };
                    errorFieldList.Add(error);
                }

                //oems_event_to_eventregistrationschedule, oems_eventregistrationschedule
                DataCollection<Entity> eventRegSchedules = crmTools.getRelatedEntities(
                                                        new ColumnSet("oems_eventid"),
                                                        selectedEventRef.LogicalName,
                                                        selectedEventRef.Id,
                                                        "oems_eventapplicationschedule",
                                                        "oems_event_to_eventapplicationschedule",
                                                        new ConditionExpression("statuscode", ConditionOperator.Equal, 1));
                if (eventRegSchedules == null)
                { // Must have at least one Registration Application Per Registrant Type defined
                    ret = false;
                    ErrorFieldMessage error = new ErrorFieldMessage() { FieldName = stringManager.getString(StringCodes.FIELDNAME_EVENT_WORK_SCHEDULES, lcId), ErrorMessage = stringManager.getString(StringCodes.WORK_SCHEDULE_LIST_EMPTY, lcId) };
                    errorFieldList.Add(error);
                }
                else
                {

                    foreach (Entity schedule in eventRegSchedules)
                    {
                        string regScheduleName = CrmTools.entityAttributeExists(schedule, "oems_applicationschedulename") ? (string)schedule["oems_applicationschedulename"] : "";
                        if (regOpenDate != null && regOpenDate != DateTime.MinValue)
                        {

                            if (schedule.Contains("oems_applicationopendatetime") && schedule["oems_applicationopendatetime"] != null)
                            {
                                DateTime scheduleStart = (DateTime)schedule["oems_applicationopendatetime"];

                                if (scheduleStart < regOpenDate)
                                {
                                    //Registration Start Date cannot be before the Start Date
                                    ret = false;
                                    ErrorFieldMessage error = new ErrorFieldMessage() { FieldName = stringManager.getString(StringCodes.FIELDNAME_WORK_SCHEDULE, lcId), ErrorMessage = string.Format(stringManager.getString(StringCodes.SCHEDULE_WORK_START_BEFORE_OPEN, lcId), regScheduleName) };
                                    errorFieldList.Add(error);
                                }
                                else if (scheduleStart < DateTime.Now)
                                {
                                    //Registration Start Date cannot be before Today
                                    ret = false;
                                    ErrorFieldMessage error = new ErrorFieldMessage() { FieldName = stringManager.getString(StringCodes.FIELDNAME_WORK_SCHEDULE, lcId), ErrorMessage = string.Format(stringManager.getString(StringCodes.SCHEDULE_WORK_START_BEFORE_TODAY, lcId), regScheduleName) };
                                    errorFieldList.Add(error);
                                }
                                else if (scheduleStart > regDeadlineDate)
                                {
                                    //Registration Start Date cannot be after the Registration End Date
                                    ret = false;
                                    ErrorFieldMessage error = new ErrorFieldMessage() { FieldName = stringManager.getString(StringCodes.FIELDNAME_WORK_SCHEDULE, lcId), ErrorMessage = string.Format(stringManager.getString(StringCodes.SCHEDULE_WORK_START_AFTER_DEADLINE, lcId), regScheduleName) };
                                    errorFieldList.Add(error);
                                }
                            }
                            else
                            {
                                //registration start date is mandatory
                                ret = false;
                                ErrorFieldMessage error = new ErrorFieldMessage() { FieldName = stringManager.getString(StringCodes.FIELDNAME_WORK_SCHEDULE, lcId), ErrorMessage = string.Format(stringManager.getString(StringCodes.SCHEDULE_WORK_START_EMPTY, lcId), regScheduleName) };
                                errorFieldList.Add(error);
                            }
                        }

                        if (regDeadlineDate != null && regDeadlineDate != DateTime.MinValue)
                        {
                            if (schedule.Contains("oems_applicationopendatetime") && schedule["oems_applicationopendatetime"] != null)
                            {
                                DateTime scheduleEnd = (DateTime)schedule["oems_applicationopendatetime"];
                                if (scheduleEnd > regDeadlineDate)
                                {
                                    //Registration End Date cannot be after the End Date
                                    ret = false;
                                    ErrorFieldMessage error = new ErrorFieldMessage() { FieldName = stringManager.getString(StringCodes.FIELDNAME_WORK_SCHEDULE, lcId), ErrorMessage = string.Format(stringManager.getString(StringCodes.SCHEDULE_WORK_END_AFTER_DEADLINE, lcId), regScheduleName) };
                                    errorFieldList.Add(error);
                                }
                            }
                            else
                            {
                                //registration end date is mandatory
                                ret = false;
                                ErrorFieldMessage error = new ErrorFieldMessage() { FieldName = stringManager.getString(StringCodes.FIELDNAME_WORK_SCHEDULE, lcId), ErrorMessage = string.Format(stringManager.getString(StringCodes.SCHEDULE_WORK_START_EMPTY, lcId), regScheduleName) };
                                errorFieldList.Add(error);
                            }
                        }
                    }
                }

                if (courseTermFlag)
                {
                    //oems_instructorcourseagreementblankform
                    if (ValidationTools.isSingleValueFieldEmpty(oemsEvent, "oems_instructorcourseagreementblankform1"))
                    {
                        ret = false;
                        ErrorFieldMessage error = new ErrorFieldMessage() { FieldName = stringManager.getString(StringCodes.FIELDNAME_INSTRUCTOR_COURSE_AGREEMENT_BLANK_FORM, lcId), ErrorMessage = stringManager.getString(StringCodes.INSTRUCTOR_COURSE_AGREEMENT_BLANK_FORM_EMPTY, lcId) };
                        errorFieldList.Add(error);
                    }

                    //oems_instructorpayrollenrollmentblankform
                    if (ValidationTools.isSingleValueFieldEmpty(oemsEvent, "oems_instructorpayrollenrollmentblankform1"))
                    {
                        ret = false;
                        ErrorFieldMessage error = new ErrorFieldMessage() { FieldName = stringManager.getString(StringCodes.FIELDNAME_INSTRUCTOR_PAYROLLENROLLMENT_BLANK_FORM, lcId), ErrorMessage = stringManager.getString(StringCodes.INSTRUCTOR_PAYROLLENROLLMENT_BLANK_FORM_EMPTY, lcId) };
                        errorFieldList.Add(error);
                    }

                    //Sample Participation Letter
                    if (ValidationTools.isSingleValueFieldEmpty(oemsEvent, "oems_sampleparticipationletter"))
                    {
                        ret = false;
                        ErrorFieldMessage error = new ErrorFieldMessage() { FieldName = stringManager.getString(StringCodes.FIELDNAME_SAMPLE_PARTICIPATION_LETTER, lcId), ErrorMessage = stringManager.getString(StringCodes.SAMPLE_PARTICIPATION_LETTER_EMPTY, lcId) };
                        errorFieldList.Add(error);
                    }

                    //Letter Template
                    if (ValidationTools.isSingleValueFieldEmpty(oemsEvent, "oems_lettertemplate"))
                    {
                        ret = false;
                        ErrorFieldMessage error = new ErrorFieldMessage() { FieldName = stringManager.getString(StringCodes.FIELDNAME_LETTER_TEMPLATE, lcId), ErrorMessage = stringManager.getString(StringCodes.LETTER_TEMPLATE_EMPTY, lcId) };
                        errorFieldList.Add(error);
                    }

                    //Weeks of Availability
                    DataCollection<Entity> weeksResult = crmTools.getRelatedEntities(new ColumnSet("createdon"), oemsEvent.LogicalName, oemsEvent.Id, "oems_termweek", "oems_event_oems_termweek_event", new ConditionExpression("oems_event", ConditionOperator.Equal, oemsEvent.Id));
                    if (weeksResult == null || weeksResult.Count == 0)
                    {
                        ret = false;
                        ErrorFieldMessage error = new ErrorFieldMessage() { FieldName = stringManager.getString(StringCodes.FIELDNAME_WEEKS_OF_AVAILABILITY, lcId), ErrorMessage = stringManager.getString(StringCodes.WEEKS_OF_AVAILABILITY_EMPTY, lcId) };
                        errorFieldList.Add(error);
                    }

                    //Target Grades
                    List<Entity> targetResult = crmTools.getRelatedEntities(wfActivity.service, oemsEvent.ToEntityReference(), "oems_targetgrade", "oems_coursetermpresenterapplication");
                    if (targetResult == null || targetResult.Count == 0)
                    {
                        ret = false;
                        ErrorFieldMessage error = new ErrorFieldMessage() { FieldName = stringManager.getString(StringCodes.FIELDNAME_TARGET_GRADES, lcId), ErrorMessage = stringManager.getString(StringCodes.TARGET_GRADES_EMPTY, lcId) };
                        errorFieldList.Add(error);
                    }

                    //Application Topics
                    List<Entity> topicResult = crmTools.getRelatedEntities(wfActivity.service, oemsEvent.ToEntityReference(), "oems_coursetopic", "oems_coursetermpresenterapplication");
                    if (topicResult == null || topicResult.Count == 0)
                    {
                        ret = false;
                        ErrorFieldMessage error = new ErrorFieldMessage() { FieldName = stringManager.getString(StringCodes.FIELDNAME_APPLICATION_TOPICS, lcId), ErrorMessage = stringManager.getString(StringCodes.APPLICATION_TOPICS_EMPTY, lcId) };
                        errorFieldList.Add(error);
                    }

                    //if (ValidationTools.isSingleValueFieldEmpty(oemsEvent, "oems_registrationapplicationtemplate"))
                    //{//Must have a Registration Form Template
                    //    ret = false;
                    //    ErrorFieldMessage error = new ErrorFieldMessage() { FieldName = stringManager.getString(StringCodes.FIELDNAME_EVENT_TEMPLATES, lcId), ErrorMessage = stringManager.getString(StringCodes.EVENT_WORK_TEMPLATES_EMPTY, lcId) };
                    //    errorFieldList.Add(error);
                    //}
                }
            }

            #endregion

            #region Caucus
            //Must have a name and description defined
            //Each Caucus name and datetimeslot combination must be unique
            flag = CrmTools.entityAttributeExists(oemsEvent, "oems_caucuscomponentflag") ? ((bool)oemsEvent["oems_caucuscomponentflag"]) : false;

            if(flag){
                //oems_event_to_eventcaucus, oems_eventcaucus
                DataCollection<Entity> eventCaucuses = crmTools.getRelatedEntities( //394
                                                        new ColumnSet("oems_eventid"),
                                                        selectedEventRef.LogicalName,
                                                        selectedEventRef.Id,
                                                        "oems_eventcaucus",
                                                        new ConditionExpression("statuscode", ConditionOperator.Equal, 1),
                                                        "oems_event_to_eventcaucus");
                if (eventCaucuses == null)
                {
      //              ret = false;
      //              ErrorFieldMessage error = new ErrorFieldMessage() { FieldName = "Caucus", ErrorMessage = "Caucus list is empty" };
      //              errorFieldList.Add(error);
                }
                else
                {
                    //Each Caucus must have a name and description defined
                    //Each Caucus name and datetimeslot combination must be unique
                    ArrayList timeSlots = new ArrayList();
                    foreach (Entity caucus in eventCaucuses)
                    {
                        //oems_caucusname,
    //                    if(!caucus.Contains("oems_caucusname") && !string.IsNullOrEmpty((string)caucus["oems_caucusname"])){
                        if (ValidationTools.isSingleValueFieldEmpty(caucus, "oems_caucusname"))
                        {
                            ret = false;
                            ErrorFieldMessage error = new ErrorFieldMessage() { FieldName = stringManager.getString(StringCodes.FIELDNAME_CAUCUS, lcId), ErrorMessage = stringManager.getString(StringCodes.CAUCUS_SCHEDULE_NAME_EMPTY, lcId) };
                            errorFieldList.Add(error);
                        }

                        //oems_caucusdescription
    //                    if(!caucus.Contains("oems_caucusdescription") && !string.IsNullOrEmpty((string)caucus["oems_caucusdescription"])){
                        if (ValidationTools.isSingleValueFieldEmpty(caucus, "oems_caucusdescription"))
                        {
                            ret = false;
                            ErrorFieldMessage error = new ErrorFieldMessage() { FieldName = stringManager.getString(StringCodes.FIELDNAME_CAUCUS, lcId), ErrorMessage = stringManager.getString(StringCodes.CAUCUS_SCHEDULE_DESCRIPTION_EMPTY, lcId) };
                            errorFieldList.Add(error);
                        }

                        DateTime start = (DateTime)caucus["oems_startdatetime"];
                        DateTime end = (DateTime)caucus["oems_enddatetime"];

                        TimeSlot timeSlot = new TimeSlot(start, end, caucus["oems_caucusname"].ToString());
                        if(timeSlot.isValid()){
                            timeSlots.Add(timeSlot);
                        }else{
                            ret = false;
                            ErrorFieldMessage error = new ErrorFieldMessage() { FieldName = stringManager.getString(StringCodes.FIELDNAME_CAUCUS, lcId), ErrorMessage = stringManager.getString(StringCodes.CAUCUS_SCHEDULE_TIME_SLOT_INVALID, lcId) };
                            errorFieldList.Add(error);
                        }
                    }

                    if (ValidationTools.hasDuplicateTimeSlot(timeSlots,true))
                    {
                        //Each Caucus name and datetimeslot combination must be unique
                        ret = false;
                        ErrorFieldMessage error = new ErrorFieldMessage() { FieldName = stringManager.getString(StringCodes.FIELDNAME_CAUCUS, lcId), ErrorMessage = stringManager.getString(StringCodes.CAUCUSE_SCHEDULE_DUPLICATE_TIME_SLOT, lcId) };
                        errorFieldList.Add(error);
                    }
                }
            }

            #endregion

            #region Child Care

            //oems_childcareminimumage/oems_childcaremaximumage
            flag = CrmTools.entityAttributeExists(oemsEvent, "oems_childcarecomponentflag") ? ((bool)oemsEvent["oems_childcarecomponentflag"]) : false;

            if (flag)
            {
                int childcareMinAge = CrmTools.entityAttributeExists(oemsEvent, "oems_childcareminimumage") ? (int)oemsEvent.Attributes["oems_childcareminimumage"] : 0;
                int childcareMaxAge = CrmTools.entityAttributeExists(oemsEvent, "oems_childcaremaximumage") ? (int)oemsEvent.Attributes["oems_childcaremaximumage"] : 0;

                if ((childcareMaxAge == childcareMinAge) && (childcareMaxAge == 0))
                {
                    ret = false;
                    ErrorFieldMessage error = new ErrorFieldMessage() { FieldName = "Childcare Age Min/Max: ", ErrorMessage = "Registration Minimum and Maximum are both 0 or empty" };
                    errorFieldList.Add(error);
                }
                if (childcareMaxAge < childcareMinAge)
                {
                    //MinChildAge must be less than or equal to maxChildAge
                    ret = false;
                    ErrorFieldMessage error = new ErrorFieldMessage() { FieldName = stringManager.getString(StringCodes.FIELDNAME_CHILD_CARE, lcId), ErrorMessage = stringManager.getString(StringCodes.MIN_AGE_GREATER_THAN_MAX_AGE, lcId) };
                    errorFieldList.Add(error);
                }

                //oems_event_to_eventchildcareschedule, oems_eventchildcareschedule
                DataCollection<Entity> childcareSchedules = crmTools.getRelatedEntities(
                                                        new ColumnSet("oems_eventid"),
                                                        selectedEventRef.LogicalName,
                                                        selectedEventRef.Id,
                                                        "oems_eventchildcareschedule",
                                                        new ConditionExpression("statuscode", ConditionOperator.Equal, 1),
                                                        "oems_event_to_eventchildcareschedule");
                if (childcareSchedules == null)
                {
       //             ret = false;
       //             ErrorFieldMessage error = new ErrorFieldMessage() { FieldName = "Child Care", ErrorMessage = "Child Care list is empty" };
       ///             errorFieldList.Add(error);
                }
                else
                {
                    //create time slot list
                    ArrayList timeSlots = new ArrayList();
                    foreach (Entity schedule in childcareSchedules)
                    {
                        DateTime start = CrmTools.entityAttributeExists(oemsEvent, "oems_startdate") ? (DateTime)schedule["oems_startdatetime"] : DateTime.MinValue;
                        DateTime end = CrmTools.entityAttributeExists(oemsEvent, "oems_enddate") ? (DateTime)schedule["oems_enddatetime"] : DateTime.MinValue;

                        TimeSlot timeSlot = new TimeSlot(start, end);
                        if(timeSlot.isValid()){
                            timeSlots.Add(timeSlot);
                        }else{
                            ret = false;
                            ErrorFieldMessage error = new ErrorFieldMessage() { FieldName = stringManager.getString(StringCodes.FIELDNAME_CHILD_CARE, lcId), ErrorMessage = stringManager.getString(StringCodes.CHILDCARE_SCHEDULE_TIME_SLOT_INVALID, lcId) };
                            errorFieldList.Add(error);
                        }
                    }

                    //check for duplicate time slots
                    /*if(ValidationTools.hasDuplicateTimeSlot(timeSlots)){
                        //Each childcare schedule cannot have duplicate timeslots
                        ret = false;
                        ErrorFieldMessage error = new ErrorFieldMessage() { FieldName = stringManager.getString(StringCodes.FIELDNAME_CHILD_CARE, lcId), ErrorMessage = stringManager.getString(StringCodes.CHILDCARE_SCHEDULE_TIME_SLOTS_DUPLICATED, lcId)};
                        errorFieldList.Add(error);
                    }*/
                }
            }
            #endregion

            #region Accommodation
            //Must have a location defined (i.e. name, address, etc.)
            //oems_accommodationlocation
            flag = CrmTools.entityAttributeExists(oemsEvent, "oems_accommodationcomponentflag") ? ((bool)oemsEvent["oems_accommodationcomponentflag"]) : false;
            if (flag)
            {
                if (ValidationTools.isSingleValueFieldEmpty(oemsEvent, "oems_accommodationlocation")) //532
                {
                    //Must have a location defined (i.e. name, address, etc.)
                    ret = false;
                    ErrorFieldMessage error = new ErrorFieldMessage() { FieldName = "Accommodation", ErrorMessage = "Accommodation location is empty" };
                    errorFieldList.Add(error);
                }
            
                //oems_event_to_eventaccommodationschedule
                DataCollection<Entity> eventAccommodationSchedules = crmTools.getRelatedEntities(
                                                        new ColumnSet("oems_eventid"),
                                                        selectedEventRef.LogicalName,
                                                        selectedEventRef.Id,
                                                        "oems_eventaccommodationschedule",
                                                        new ConditionExpression("statuscode", ConditionOperator.Equal, 1),
                                                        "oems_event_to_eventaccommodationschedule");
                if (eventAccommodationSchedules == null)
                {
      //              ret = false;
      //              ErrorFieldMessage error = new ErrorFieldMessage() { FieldName = "Event Accommodation Schedules", ErrorMessage = "Accommodation Schedules list is empty" };
      //              errorFieldList.Add(error);
                }
                else
                {
                    //Each Accommodation schedule cannot have duplicate date slots

                    //create time slot list
                    ArrayList timeSlots = new ArrayList();
                    foreach (Entity schedule in eventAccommodationSchedules)
                    {
                        DateTime start = CrmTools.entityAttributeExists(schedule, "oems_accommodationdate") ? (DateTime)schedule["oems_accommodationdate"] : DateTime.MinValue;
                        DateTime end = CrmTools.entityAttributeExists(schedule, "oems_accommodationdate") ? (DateTime)schedule["oems_accommodationdate"] : DateTime.MinValue;

                        TimeSlot timeSlot = new TimeSlot(start, end);
                        if(timeSlot.isValid()){
                            timeSlots.Add(timeSlot);
                        }else{
                            ret = false;
                            ErrorFieldMessage error = new ErrorFieldMessage() { FieldName = stringManager.getString(StringCodes.FIELDNAME_ACCOMMODATION, lcId), ErrorMessage = stringManager.getString(StringCodes.ACCOMMODATION_SCHEDULE_TIME_SLOT_INVALID, lcId) };
                            errorFieldList.Add(error);
                        }
                    }

                    //check for duplicate time slots
                    if(ValidationTools.hasDuplicateTimeSlot(timeSlots)){
                        //Each childcare schedule cannot have duplicate timeslots
                        ret = false;
                        ErrorFieldMessage error = new ErrorFieldMessage() { FieldName = stringManager.getString(StringCodes.FIELDNAME_ACCOMMODATION, lcId), ErrorMessage = stringManager.getString(StringCodes.ACCOMMODATION_SCHEDULE_TIME_SLOTS_DUPLICATED, lcId) };
                        errorFieldList.Add(error);
                    }
                }
            }

            #endregion

            #region Invited Guest
            //Must have name and description defined
            //Must have an ETFO User Account defined
            //Cannot have duplicate  invitee User accounts

            flag = CrmTools.entityAttributeExists(oemsEvent, "oems_invitedguestcomponentflag") ? ((bool)oemsEvent["oems_invitedguestcomponentflag"]) : false;

            if (flag)
            {
                //oems_eventinvitedguest
                //oems_event_to_eventinvitedguest relationship
                DataCollection<Entity> invitedGuests = crmTools.getRelatedEntities(  //592
                                                        new ColumnSet("oems_eventid"),
                                                        selectedEventRef.LogicalName,
                                                        selectedEventRef.Id,
                                                        "oems_eventinvitedguest",
                                                        new ConditionExpression("statuscode", ConditionOperator.Equal, StatusCode.InvitedSent), // 347780000: Invited Sent 347780001: Accept
                                                        "oems_event_to_eventinvitedguest");
                if (invitedGuests == null)
                {
     //               ret = false;
     //               ErrorFieldMessage error = new ErrorFieldMessage() { FieldName = "Invited Guests", ErrorMessage = "Invited Guests list is empty" };
     //               errorFieldList.Add(error);
                }
                else
                {
                    //Each guest must have name and description defined
                    //Each guest must have an ETFO User Account defined
                    //Cannot have duplicate  invitee User accounts

                    ArrayList invitees = new ArrayList();
                    foreach (Entity guest in invitedGuests)
                    {
                        //Each guest must have name and description defined
                        string guestName = CrmTools.entityAttributeExists(guest, "oems_invitedguestname") ? (string)guest["oems_invitedguestname"] : null;
                        if(string.IsNullOrEmpty(guestName)){
                            ret = false;
                            ErrorFieldMessage error = new ErrorFieldMessage() { FieldName = stringManager.getString(StringCodes.FIELDNAME_INVITED_GUESTS, lcId), ErrorMessage = stringManager.getString(StringCodes.INVITED_GUEST_NAME_EMPTY, lcId) };
                            errorFieldList.Add(error);
                        }

                        //Need to change when the description field is added to invited guest entity
                        string guestDescription = CrmTools.entityAttributeExists(guest, "oems_inviteedescription") ? (string)guest["oems_inviteedescription"] : null; 
                        if(string.IsNullOrEmpty(guestDescription)){
                            ret = false;
                            ErrorFieldMessage error = new ErrorFieldMessage() { FieldName = stringManager.getString(StringCodes.FIELDNAME_INVITED_GUESTS, lcId), ErrorMessage = stringManager.getString(StringCodes.INVITED_GUEST_DESCRIPTION_EMPTY, lcId) };
                            errorFieldList.Add(error);
                        }

                        if (ValidationTools.isSingleValueFieldEmpty(guest, "oems_invitee"))
                        {//Each guest must have an ETFO User Account defined
                            ret = false;
                            ErrorFieldMessage error = new ErrorFieldMessage() { FieldName = stringManager.getString(StringCodes.FIELDNAME_INVITED_GUESTS, lcId), ErrorMessage = stringManager.getString(StringCodes.INVITED_GUEST_INVITEE_EMPTY, lcId) };
                            errorFieldList.Add(error);
                        }else{
                            EntityReference invitee = (EntityReference)guest["oems_invitee"];
                            invitees.Add(invitee);
                        }
                    }

                    if(ValidationTools.hasDuplicateEntityReference(invitees)){
                        //Cannot have duplicate  invitee User accounts
                        ret = false;
                        ErrorFieldMessage error = new ErrorFieldMessage() { FieldName = stringManager.getString(StringCodes.FIELDNAME_INVITED_GUESTS, lcId), ErrorMessage = stringManager.getString(StringCodes.INVITED_GUEST_LIST_INVITEES_DUPLICATED, lcId) };
                        errorFieldList.Add(error);
                    }
                }
            }

            #endregion

            //651
            #region Local Delegation 
            //Cannot have duplicate  locals (in other words, each local can only specify one limit)
            //Each Local must have a limit defined > 0
            flag = CrmTools.entityAttributeExists(oemsEvent, "oems_localdelegationcomponentflag") ? ((bool)oemsEvent["oems_localdelegationcomponentflag"]) : false;

            if (flag)
            {
                //oems_event_to_eventlocal, oems_eventlocal
                DataCollection<Entity> localDelegations = crmTools.getRelatedEntities(
                                                        new ColumnSet("oems_eventid"),
                                                        selectedEventRef.LogicalName,
                                                        selectedEventRef.Id,
                                                        "oems_eventlocal",
                                                        new ConditionExpression("statuscode", ConditionOperator.Equal, StatusCode.Approved), //347780000: Approved
                                                        "oems_event_to_eventlocal");
                if (localDelegations == null)
                {
     //              ret = false;
     //               ErrorFieldMessage error = new ErrorFieldMessage() { FieldName = "Local Delegation", ErrorMessage = "Local Delegation list is empty" };
     //               errorFieldList.Add(error);
                }
                else
                {
                
                    ArrayList locals = new ArrayList();
                    foreach (Entity localDelegation in localDelegations)
                    {
                        int limit = CrmTools.entityAttributeExists(localDelegation, "oems_localdelegationlimit") ? (int)localDelegation["oems_localdelegationlimit"] : 0;
                    
                        if(limit <= 0){
                            //Each Local must have a limit defined > 0
                            ret = false;
                            ErrorFieldMessage error = new ErrorFieldMessage() { FieldName = stringManager.getString(StringCodes.FIELDNAME_LOCAL_DELEGATION, lcId), ErrorMessage = stringManager.getString(StringCodes.LOCAL_DELEGATION_LIMIT_NEGATIVE, lcId) };
                            errorFieldList.Add(error);
                        }

                        if(ValidationTools.isSingleValueFieldEmpty(localDelegation, "oems_local")){

                        }else{
                            EntityReference local = (EntityReference)localDelegation["oems_local"];
                            locals.Add(local);
                        }
                    }

                    if (ValidationTools.hasDuplicateEntityReference(locals))
                    {
                        //Cannot have duplicate locals (in other words, each local can only specify one limit)
                        ret = false;
                        ErrorFieldMessage error = new ErrorFieldMessage() { FieldName = stringManager.getString(StringCodes.FIELDNAME_LOCAL_DELEGATION, lcId), ErrorMessage = stringManager.getString(StringCodes.LOCAL_DELEGATION_LOCALS_DUPLICATED, lcId) };
                        errorFieldList.Add(error);
                    }
                }


                DataCollection<Entity> attendingVotingList = crmTools.getRelatedEntities(
                                                        new ColumnSet("oems_eventid"),
                                                        selectedEventRef.LogicalName,
                                                        selectedEventRef.Id,
                                                        "oems_attendingasoption",
                                                        new ConditionExpression("statecode", ConditionOperator.Equal, StateCode.Active), //347780000: Approved
                                                        "oems_event_attending_as_voting");

                if (attendingVotingList == null)
                {
                    DataCollection<Entity> attendingNonVotingList = crmTools.getRelatedEntities(
                                                        new ColumnSet("oems_eventid"),
                                                        selectedEventRef.LogicalName,
                                                        selectedEventRef.Id,
                                                        "oems_attendingasoption",
                                                        new ConditionExpression("statecode", ConditionOperator.Equal, StateCode.Active), //347780000: Approved
                                                        "oems_event_attending_as_nonvoting");
                    if (attendingNonVotingList == null)
                    {
                        ret = false;
                        ErrorFieldMessage error = new ErrorFieldMessage() { FieldName = "Local Delegation", ErrorMessage = "Attending As list is empty" };
                        errorFieldList.Add(error);
                    }
                }
            }

            #endregion

            #region Release Time
            //Cannot have duplicate release time date slots
            flag = CrmTools.entityAttributeExists(oemsEvent, "oems_releasetimecomponentflag") ? ((bool)oemsEvent["oems_releasetimecomponentflag"]) : false;

            if (flag)
            {
                //oems_event_to_eventreleasetimeschedule, oems_eventreleasetimeschedule 715
                DataCollection<Entity> releaseTimeSchedules = crmTools.getRelatedEntities(
                                                        new ColumnSet("oems_eventid"),
                                                        selectedEventRef.LogicalName,
                                                        selectedEventRef.Id,
                                                        "oems_eventreleasetimeschedule",
                                                        new ConditionExpression("statuscode", ConditionOperator.Equal, 1),
                                                        "oems_event_to_eventreleasetimeschedule");
                if (releaseTimeSchedules == null)
                {
      //              ret = false;
      //              ErrorFieldMessage error = new ErrorFieldMessage() { FieldName = "Release Time", ErrorMessage = "Release Time list is empty" };
      //              errorFieldList.Add(error);
                }
                else
                {
                    //create time slot list
                    ArrayList timeSlots = new ArrayList();
                    foreach (Entity schedule in releaseTimeSchedules)
                    {
                        DateTime start = CrmTools.entityAttributeExists(schedule, "oems_releasedate") ? (DateTime)schedule["oems_releasedate"] : DateTime.MinValue;
                        DateTime end = CrmTools.entityAttributeExists(schedule, "oems_releasedate") ? (DateTime)schedule["oems_releasedate"] : DateTime.MinValue;

                        TimeSlot timeSlot = new TimeSlot(start, end);
                        if(timeSlot.isValid()){
                            timeSlots.Add(timeSlot);
                        }else{
                            ret = false;
                            ErrorFieldMessage error = new ErrorFieldMessage() { FieldName = stringManager.getString(StringCodes.FIELDNAME_RELEASE_TIME, lcId), ErrorMessage = stringManager.getString(StringCodes.RELEASE_TIME_SLOT_INVALID, lcId) };
                            errorFieldList.Add(error);
                        }
                    }

                    //check for duplicate time slots
                    if(ValidationTools.hasDuplicateTimeSlot(timeSlots)){
                        //Cannot have duplicate release time date slots
                        ret = false;
                        ErrorFieldMessage error = new ErrorFieldMessage() { FieldName = stringManager.getString(StringCodes.FIELDNAME_RELEASE_TIME, lcId), ErrorMessage = stringManager.getString(StringCodes.RELEASE_TIME_SLOTS_DUPLICATED, lcId) };
                        errorFieldList.Add(error);
                    }
                }
            }

            #endregion

            #region Waiting List

            flag = CrmTools.entityAttributeExists(oemsEvent, "oems_waitinglistcomponentflag") ? ((bool)oemsEvent["oems_waitinglistcomponentflag"]) : false;

            if (flag)
            {
                decimal regMIn = CrmTools.entityAttributeExists(oemsEvent, "oems_waitinglistsize") ? (decimal)oemsEvent.Attributes["oems_waitinglistsize"] : 0;
                if (regMIn == 0)
                {
                    //Waiting List Size is Equal to zero.
                    ret = false;
                    ErrorFieldMessage error = new ErrorFieldMessage() { FieldName = stringManager.getString(StringCodes.FIELDNAME_WAITINGLIST_SIZE, lcId), ErrorMessage = stringManager.getString(StringCodes.WAITINGLIST_SIZE_EQUAL_ZERO, lcId) };
                    errorFieldList.Add(error);
                }

                int regMax = CrmTools.entityAttributeExists(oemsEvent, "oems_registrationmaximum") ? (int)oemsEvent.Attributes["oems_registrationmaximum"] : 0;
                if (regMax == 0)
                {
                    //Reg Maximum is Equal to zero.
                    ret = false;
                    ErrorFieldMessage error = new ErrorFieldMessage() { FieldName = stringManager.getString(StringCodes.FIELDNAME_REG_MAX_SIZE, lcId), ErrorMessage = stringManager.getString(StringCodes.REG_MAX_SIZE_EQUAL_ZERO, lcId) };
                    errorFieldList.Add(error);
                }
            }

            #endregion

            #region Billing

            bool billFlag = CrmTools.entityAttributeExists(oemsEvent, "oems_billingacceptcheque") ? (bool)oemsEvent["oems_billingacceptcheque"] : false;

            if (billFlag)
	        {
		        DateTime billingChequeDueDate = CrmTools.entityAttributeExists(oemsEvent, "oems_billingchequeduedate") ? (DateTime)oemsEvent.Attributes["oems_billingchequeduedate"] : DateTime.MinValue;
            
                if (billingChequeDueDate == DateTime.MinValue || billingChequeDueDate == DateTime.MinValue)
                {   //registration start/end date is mandatory
                    ret = false;
                    ErrorFieldMessage error = new ErrorFieldMessage() { FieldName = stringManager.getString(StringCodes.FIELDNAME_CHEQUE_DUE_DATE, lcId), ErrorMessage = stringManager.getString(StringCodes.CHEQUE_DUE_DATE_EMPTY, lcId) };
                    errorFieldList.Add(error);
                }
	        }       

            #endregion

            #region CourseTerm

            //if (courseTermFlag)
            //{
            //    //Target Grades
            //    List<Entity> targetResult = crmTools.getRelatedEntities(wfActivity.service, oemsEvent.ToEntityReference(), "oems_targetgrade");
            //    if (targetResult == null || targetResult.Count == 0)
            //    {
            //        ret = false;
            //        ErrorFieldMessage error = new ErrorFieldMessage() { FieldName = stringManager.getString(StringCodes.FIELDNAME_TARGET_GRADES, lcId), ErrorMessage = stringManager.getString(StringCodes.TARGET_GRADES_EMPTY, lcId) };
            //        errorFieldList.Add(error);
            //    }

            //    //Application Topics
            //    List<Entity> topicResult = crmTools.getRelatedEntities(wfActivity.service, oemsEvent.ToEntityReference(), "oems_coursetopic");
            //    if (topicResult == null || topicResult.Count == 0)
            //    {
            //        ret = false;
            //        ErrorFieldMessage error = new ErrorFieldMessage() { FieldName = stringManager.getString(StringCodes.FIELDNAME_APPLICATION_TOPICS, lcId), ErrorMessage = stringManager.getString(StringCodes.APPLICATION_TOPICS_EMPTY, lcId) };
            //        errorFieldList.Add(error);
            //    }
            //}

            #endregion

            return ret;
        }

    }

    
}

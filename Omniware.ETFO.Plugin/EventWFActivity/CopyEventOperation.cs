﻿using EventWFActivity.Utilities;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Workflow;
using Shared.Utilities;
using System;
using System.Activities;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Omniware.ETFO.Plugin;

namespace EventWFActivity
{
    public class ComponentIndicatior
    {
        public const int NotIncluded = 1;
        public const int Mandatory = 2;
        public const int Optional = 3;
    };

    public class StateCode
    {
        public const int Active = 0;
        public const int Inactive = 1;
    };

    public class CopyEventOperation : AbstractOperation
    {
        private string newName;
        private bool doSection;
        private EntityReference selectedEventRef;

        public CopyEventOperation(string _newName, EntityReference _selectedEventRef, EventWFActivity _wfActivity, bool _doSection = false)
            : base(_wfActivity)
        {
            newName = _newName;
            doSection = _doSection;
            selectedEventRef = _selectedEventRef;
        }

        public override void operate()
        {
            try
            {
                //---------------------------
                // Retrieve the Event based
                // on the EntityReference Id
                //---------------------------
                var columns = new ColumnSet
                (
                    "oems_eventname", 
                    "oems_eventid", 
                    "oems_webpage", 
                    "oems_webpagepublishingstate", 
                    "statuscode", 
                    "oems_subcategory", 
                    "oems_eventyear",
                    "oems_eventdescription",
                    "oems_startdate",
                    "oems_enddate",
                    "oems_servicearea",
                    "oems_budgetnumber",
                    "oems_budgetnumber2",
                    "oems_eventlocation",
                    "ownerid",
                    "oems_applicationtemplate",

                    //Flags
                    "oems_registrationcomponentflag",
                    "oems_caucuscomponentflag", 
                    "oems_childcarecomponentflag", 
                    "oems_accommodationcomponentflag", 
                    "oems_invitedguestcomponentflag", 
                    "oems_localdelegationcomponentflag", 
                    "oems_releasetimecomponentflag",
                    "oems_personalaccommodationcomponentflag",
                    "oems_ticketcomponentflag",
                    "oems_billingcomponentflag",
                    "oems_workshopapplicationcomponentflag",
                    "oems_workshopcomponentflag",
                    "oems_waitinglistcomponentflag",
                    "oems_coursetermflag",
                    "oems_courseflag"
                );

                if (doSection)
                {
                    //Templates
                    columns.AddColumn("oems_registrationtemplate");
                    columns.AddColumn("oems_applicationtemplate");

                    //Status
                    columns.AddColumn("statuscode");
                }

                var oemsEvent = wfActivity.service.Retrieve(selectedEventRef.LogicalName, selectedEventRef.Id, columns);

                if (oemsEvent != null)
                {
                    #region copy general attributes

                    // string newName = NewEventName.Get<string>(context);
                    if (string.IsNullOrEmpty(newName) || string.IsNullOrWhiteSpace(newName))
                    {
                        newName = oemsEvent["oems_eventname"] + " - Copy";//"New Event";
                    }

                    oemsEvent["oems_copiedfromevent"] = oemsEvent.ToEntityReference();

                    oemsEvent.Id = Guid.NewGuid();
                    oemsEvent["oems_eventid"] = oemsEvent.Id;
                    oemsEvent["oems_eventname"] = newName;

                    //clear the webpage
                    oemsEvent["oems_webpage"] = null;
                    oemsEvent["oems_webpagepublishingstate"] = null;

                    //statuscode
                    if (!doSection)
                    {
                        oemsEvent["statuscode"] = new OptionSetValue(1);
                    }

                    wfActivity.service.Create(oemsEvent);

                    #endregion

                    #region copy the related entities

                    //copy the related entities

                    //executive staff
                    var executiveStaffs = crmTools.getRelatedEntities(
                                                        new ColumnSet("oems_eventid"),
                                                        selectedEventRef.LogicalName,
                                                        selectedEventRef.Id,
                                                        "systemuser",
                                                        new ConditionExpression("isdisabled", ConditionOperator.Equal, false),
                                                        "oems_event_executive_staff_owners");
                    if (executiveStaffs != null)
                    {
                        crmTools.associateRelatedEntitiesToEntity(executiveStaffs,
                                                        new EntityReference() { LogicalName = oemsEvent.LogicalName, Id = oemsEvent.Id },
                                                        "oems_event_executive_staff_owners");
                    }

                    //support staff
                    var supportStaffs = crmTools.getRelatedEntities(
                                                        new ColumnSet("oems_eventid"),
                                                        selectedEventRef.LogicalName,
                                                        selectedEventRef.Id,
                                                        "systemuser",
                                                        new ConditionExpression("isdisabled", ConditionOperator.Equal, false),
                                                        "oems_event_support_staff_owners");
                    if (supportStaffs != null)
                    {
                        crmTools.associateRelatedEntitiesToEntity(supportStaffs,
                                                        new EntityReference() { LogicalName = oemsEvent.LogicalName, Id = oemsEvent.Id },
                                                        "oems_event_support_staff_owners");
                    }

                    //Event Schedule, oems_event_to_eventschedule, oems_eventschedule
                    var eventSchedules = crmTools.getRelatedEntities(
                                                        new ColumnSet("oems_eventid"),
                                                        selectedEventRef.LogicalName,
                                                        selectedEventRef.Id,
                                                        "oems_eventschedule",
                                                        new ConditionExpression("statecode", ConditionOperator.Equal, StateCode.Active),
                                                        "oems_event_to_eventschedule");
                    if (eventSchedules != null)
                    {
                        foreach (Entity schedule in eventSchedules)
                        {
                            Entity newSchedule = new Entity("oems_eventschedule");
                            newSchedule.Id = Guid.NewGuid();
                            newSchedule["oems_eventscheduleid"] = newSchedule.Id;

                            newSchedule["oems_event"] = new EntityReference() { LogicalName = oemsEvent.LogicalName, Id = oemsEvent.Id };//schedule["oems_event"];
                            newSchedule["oems_eventschedulename"] = schedule["oems_eventschedulename"];
                            newSchedule["oems_startdatetime"] = schedule["oems_startdatetime"];
                            newSchedule["oems_enddatetime"] = schedule["oems_enddatetime"];

                            this.wfActivity.service.Create(newSchedule);

                        }
                    }

                    #endregion

                    #region Personal Accommodation

                    var personalAccommodations = crmTools.getRelatedEntities(
                                                    new ColumnSet("oems_eventid"),
                                                    selectedEventRef.LogicalName,
                                                    selectedEventRef.Id,
                                                    "oems_personalaccommodationoption",
                                                    new ConditionExpression("statecode", ConditionOperator.Equal, StateCode.Active),
                                                    "oems_event_personal_accommodation_config");

                    if (personalAccommodations != null)
                    {
                        crmTools.associateRelatedEntitiesToEntity(personalAccommodations, new EntityReference() { LogicalName = oemsEvent.LogicalName, Id = oemsEvent.Id }, "oems_event_personal_accommodation_config");
                    }

                    #endregion

                    #region Attending As Config

                    var attendingAsOptions = crmTools.getRelatedEntities(
                                                    new ColumnSet("oems_eventid"),
                                                    selectedEventRef.LogicalName,
                                                    selectedEventRef.Id,
                                                    "oems_attendingasoption",
                                                    new ConditionExpression("statecode", ConditionOperator.Equal, StateCode.Active),
                                                    "oems_event_attending_as_config");

                    if (attendingAsOptions != null)
                    {
                        crmTools.associateRelatedEntitiesToEntity(attendingAsOptions, new EntityReference() { LogicalName = oemsEvent.LogicalName, Id = oemsEvent.Id }, "oems_event_attending_as_config");
                    }

                    #endregion

                    //copy components sections according to sub-category
                    //var subCategoryRf = (EntityReference)oemsEvent["oems_subcategory"];
                    //var subCategory = wfActivity.service.Retrieve(subCategoryRf.LogicalName, subCategoryRf.Id, new Microsoft.Xrm.Sdk.Query.ColumnSet(true));

                    #region Registration

                    //Registration
                    //setEventComponentOptionAccordingToSubCategory(subCategory, "oems_registrationcomponent", oemsEvent, "oems_registrationcomponentflag");

                    var flag = oemsEvent.GetAttributeValue<bool>("oems_registrationcomponentflag");
                    if (flag)
                    {
                        //copy the registration requred fields
                        var tempEvent = wfActivity.service.Retrieve(selectedEventRef.LogicalName, selectedEventRef.Id, new ColumnSet("oems_registrationminimum", 
                                                                                                                                    "oems_registrationmaximum", 
                                                                                                                                    "oems_registrationopening", 
                                                                                                                                    "oems_registrationdeadline",
                                                                                                                                    "oems_registrationtemplate"));

                        oemsEvent["oems_registrationminimum"] = CrmTools.getEnitiyAttributeSafely(tempEvent, "oems_registrationminimum");
                        oemsEvent["oems_registrationmaximum"] = CrmTools.getEnitiyAttributeSafely(tempEvent, "oems_registrationmaximum");
                        oemsEvent["oems_registrationopening"] = CrmTools.getEnitiyAttributeSafely(tempEvent, "oems_registrationopening");
                        oemsEvent["oems_registrationdeadline"] = CrmTools.getEnitiyAttributeSafely(tempEvent, "oems_registrationdeadline");
                        oemsEvent["oems_registrationtemplate"] = CrmTools.getEnitiyAttributeSafely(tempEvent, "oems_registrationtemplate");

                        var eventRegSchedules = crmTools.getRelatedEntities(
                                                    new ColumnSet("oems_eventid"),
                                                    selectedEventRef.LogicalName,
                                                    selectedEventRef.Id,
                                                    "oems_eventregistrationschedule",
                                                    new ConditionExpression("statecode", ConditionOperator.Equal, StateCode.Active),
                                                    "oems_event_to_eventregistrationschedule");

                        if (eventRegSchedules != null)
                        {
                            foreach (var schedule in eventRegSchedules)
                            {
                                var newSchedule = new Entity("oems_eventregistrationschedule");
                                newSchedule.Id = Guid.NewGuid();
                                newSchedule["oems_eventregistrationscheduleid"] = newSchedule.Id;

                                newSchedule["oems_event"] = new EntityReference() { LogicalName = oemsEvent.LogicalName, Id = oemsEvent.Id };//schedule["oems_event"];
                                newSchedule["oems_registrationschedulename"] = CrmTools.getEnitiyAttributeSafely(schedule, "oems_registrationschedulename");
                                newSchedule["oems_registranttype"] = CrmTools.getEnitiyAttributeSafely(schedule, "oems_registranttype");
                                newSchedule["oems_registrationopendatetime"] = CrmTools.getEnitiyAttributeSafely(schedule, "oems_registrationopendatetime");
                                newSchedule["oems_registrationclosedatetime"] = CrmTools.getEnitiyAttributeSafely(schedule, "oems_registrationclosedatetime");

                                this.wfActivity.service.Create(newSchedule);
                            }
                        }
                    }

                    #endregion

                    #region Caucus

                    flag =  oemsEvent.GetAttributeValue<bool>("oems_caucuscomponentflag");
                    if (flag)
                    {
                        var eventCaucuses = crmTools.getRelatedEntities(
                                                        new ColumnSet("oems_eventid"),
                                                        selectedEventRef.LogicalName,
                                                        selectedEventRef.Id,
                                                        "oems_eventcaucus",
                                                        new ConditionExpression("statecode", ConditionOperator.Equal, StateCode.Active),
                                                        "oems_event_to_eventcaucus");
                        if (eventCaucuses != null)
                        {
                            foreach (var schedule in eventCaucuses)
                            {
                                var newCaucus = new Entity("oems_eventcaucus");
                                newCaucus.Id = Guid.NewGuid();
                                newCaucus["oems_eventcaucusid"] = newCaucus.Id;

                                newCaucus["oems_event"] = new EntityReference() { LogicalName = oemsEvent.LogicalName, Id = oemsEvent.Id };//schedule["oems_event"];
                                newCaucus["oems_caucusname"] = CrmTools.getEnitiyAttributeSafely(schedule, "oems_caucusname");
                                newCaucus["oems_caucusdescription"] = CrmTools.getEnitiyAttributeSafely(schedule, "oems_caucusdescription");
                                newCaucus["oems_startdatetime"] = CrmTools.getEnitiyAttributeSafely(schedule, "oems_startdatetime");
                                newCaucus["oems_enddatetime"] = CrmTools.getEnitiyAttributeSafely(schedule, "oems_enddatetime");
                                newCaucus["oems_roomassignment"] = CrmTools.getEnitiyAttributeSafely(schedule, "oems_roomassignment");
                                newCaucus["oems_organizer"] = CrmTools.getEnitiyAttributeSafely(schedule, "oems_organizer");
                                newCaucus["oems_chair"] = CrmTools.getEnitiyAttributeSafely(schedule, "oems_chair");

                                this.wfActivity.service.Create(newCaucus);
                            }
                        }
                    }

                    #endregion

                    #region Child Care

                    flag =  oemsEvent.GetAttributeValue<bool>("oems_childcarecomponentflag");
                    if (flag)
                    {
                        var tempEvent = wfActivity.service.Retrieve(selectedEventRef.LogicalName, selectedEventRef.Id, new ColumnSet(new string[]{
                                                                                                                    "oems_childcareminimumage",
                                                                                                                    "oems_childcaremaximumage"
                                                                                                                }));

                        oemsEvent["oems_childcareminimumage"] = CrmTools.getEnitiyAttributeSafely(tempEvent, "oems_childcareminimumage");
                        oemsEvent["oems_childcaremaximumage"] = CrmTools.getEnitiyAttributeSafely(tempEvent, "oems_childcaremaximumage");

                        var childcareSchedules = crmTools.getRelatedEntities(
                                                    new ColumnSet("oems_eventid"),
                                                    selectedEventRef.LogicalName,
                                                    selectedEventRef.Id,
                                                    "oems_eventchildcareschedule",
                                                    new ConditionExpression("statecode", ConditionOperator.Equal, StateCode.Active),
                                                    "oems_event_to_eventchildcareschedule");
                        if (childcareSchedules != null)
                        {
                            foreach (var schedule in childcareSchedules)
                            {
                                var newSchedule = new Entity("oems_eventchildcareschedule");
                                newSchedule.Id = Guid.NewGuid();
                                newSchedule["oems_eventchildcarescheduleid"] = newSchedule.Id;

                                newSchedule["oems_event"] = new EntityReference() { LogicalName = oemsEvent.LogicalName, Id = oemsEvent.Id };
                                newSchedule["oems_childcareschedulename"] = CrmTools.getEnitiyAttributeSafely(schedule, "oems_childcareschedulename");
                                newSchedule["oems_startdatetime"] = CrmTools.getEnitiyAttributeSafely(schedule, "oems_startdatetime");
                                newSchedule["oems_enddatetime"] = CrmTools.getEnitiyAttributeSafely(schedule, "oems_enddatetime");

                                this.wfActivity.service.Create(newSchedule);

                                //Room assignments
                                var roomAssignments = crmTools.getRelatedEntities(
                                                    new ColumnSet("oems_eventchildcarescheduleid"),
                                                    schedule.LogicalName,
                                                    schedule.Id,
                                                    "oems_eventchildcareroomassignment",
                                                    new ConditionExpression("statecode", ConditionOperator.Equal, StateCode.Active),
                                                    "oems_eventchildcareschedule_to_eventchildcareroomassignment");

                                if (roomAssignments != null)
                                {
                                    foreach (var roomAssignment in roomAssignments)
                                    {
                                        var newRoomAssignment = new Entity("oems_eventchildcareroomassignment");
                                        newRoomAssignment.Id = Guid.NewGuid();
                                        newRoomAssignment["oems_eventchildcareroomassignmentid"] = newRoomAssignment.Id;

                                        newRoomAssignment["oems_event"] = new EntityReference() { LogicalName = oemsEvent.LogicalName, Id = oemsEvent.Id };
                                        newRoomAssignment["oems_childcareschedule"] = new EntityReference() { LogicalName = newSchedule.LogicalName, Id = newSchedule.Id };
                                        newRoomAssignment["oems_roomname"] = CrmTools.getEnitiyAttributeSafely(roomAssignment, "oems_roomname");
                                        newRoomAssignment["oems_childcareprovider"] = CrmTools.getEnitiyAttributeSafely(roomAssignment, "oems_childcareprovider");

                                        this.wfActivity.service.Create(newRoomAssignment);
                                    }
                                }
                            }
                        }
                    }

                    #endregion

                    #region Accommodation

                    flag =  oemsEvent.GetAttributeValue<bool>("oems_accommodationcomponentflag");
                    if (flag)
                    {
                        var tempEvent = wfActivity.service.Retrieve(selectedEventRef.LogicalName, selectedEventRef.Id, new ColumnSet(new string[]{
                                                                                                                    "oems_accommodationlocation"
                                                                                                                }));

                        oemsEvent["oems_accommodationlocation"] = CrmTools.getEnitiyAttributeSafely(tempEvent, "oems_accommodationlocation");

                        //Accomodation schedules
                        var eventAccommodationSchedules = crmTools.getRelatedEntities(
                                                    new ColumnSet("oems_eventid"),
                                                    selectedEventRef.LogicalName,
                                                    selectedEventRef.Id,
                                                    "oems_eventaccommodationschedule",
                                                    new ConditionExpression("statecode", ConditionOperator.Equal, StateCode.Active),
                                                    "oems_event_to_eventaccommodationschedule");
                        if (eventAccommodationSchedules != null)
                        {
                            foreach (var schedule in eventAccommodationSchedules)
                            {
                                var newSchedule = new Entity("oems_eventaccommodationschedule");
                                newSchedule.Id = Guid.NewGuid();
                                newSchedule["oems_eventaccommodationscheduleid"] = newSchedule.Id;

                                newSchedule["oems_event"] = new EntityReference() { LogicalName = oemsEvent.LogicalName, Id = oemsEvent.Id };//schedule["oems_event"];
                                newSchedule["oems_accommodationdate"] = CrmTools.getEnitiyAttributeSafely(schedule, "oems_accommodationdate");

                                this.wfActivity.service.Create(newSchedule);
                            }
                        }

                        //Accomodation pricing
                        var eventAccommodationPrincings = crmTools.getRelatedEntities(
                                                    new ColumnSet("oems_eventid"),
                                                    selectedEventRef.LogicalName,
                                                    selectedEventRef.Id,
                                                    "oems_accommodationpricing",
                                                    new ConditionExpression("statecode", ConditionOperator.Equal, StateCode.Active),
                                                    "oems_oems_event_oems_accommodationpricing_Event");
                        if (eventAccommodationPrincings != null)
                        {
                            foreach (var pricing in eventAccommodationPrincings)
                            {
                                var newPricing = new Entity("oems_accommodationpricing");
                                newPricing.Id = Guid.NewGuid();
                                newPricing["oems_accommodationpricingid"] = newPricing.Id;

                                newPricing["oems_event"] = new EntityReference() { LogicalName = oemsEvent.LogicalName, Id = oemsEvent.Id };//schedule["oems_event"];
                                newPricing["oems_name"] = CrmTools.getEnitiyAttributeSafely(pricing, "oems_name");
                                newPricing["oems_participanttype"] = CrmTools.getEnitiyAttributeSafely(pricing, "oems_participanttype");
                                newPricing["oems_price"] = CrmTools.getEnitiyAttributeSafely(pricing, "oems_price");
                                newPricing["oems_applyhst"] = CrmTools.getEnitiyAttributeSafely(pricing, "oems_applyhst");
                                
                                this.wfActivity.service.Create(newPricing);
                            }
                        }
                    }
                    #endregion

                    #region Invited Guest

                    flag =  oemsEvent.GetAttributeValue<bool>("oems_invitedguestcomponentflag");
                    if (flag)
                    {
                        var invitedGuests = crmTools.getRelatedEntities(
                                                    new ColumnSet("oems_eventid"),
                                                    selectedEventRef.LogicalName,
                                                    selectedEventRef.Id,
                                                    "oems_eventinvitedguest",
                                                    new ConditionExpression("statecode", ConditionOperator.Equal, StateCode.Active),
                                                    "oems_event_to_eventinvitedguest");
                        if (invitedGuests != null)
                        {
                            foreach (var guest in invitedGuests)
                            {
                                var newGuest = new Entity("oems_eventinvitedguest");
                                newGuest.Id = Guid.NewGuid();
                                newGuest["oems_eventinvitedguestid"] = newGuest.Id;
                                newGuest["oems_event"] = new EntityReference() { LogicalName = oemsEvent.LogicalName, Id = oemsEvent.Id };//schedule["oems_event"];

                                newGuest["oems_invitedguestname"] = CrmTools.getEnitiyAttributeSafely(guest, "oems_invitedguestname");
                                newGuest["oems_inviteedescription"] = CrmTools.getEnitiyAttributeSafely(guest, "oems_inviteedescription"); 
                                newGuest["oems_invitee"] = CrmTools.getEnitiyAttributeSafely(guest, "oems_invitee");
                                newGuest["statuscode"] = 1; //Created

                                this.wfActivity.service.Create(newGuest);
                            }
                        }
                    }
                    #endregion

                    #region Local Delegation

                    flag =  oemsEvent.GetAttributeValue<bool>("oems_localdelegationcomponentflag");
                    if (flag)
                    {
                        var tempEvent = wfActivity.service.Retrieve(selectedEventRef.LogicalName, selectedEventRef.Id, new ColumnSet(new string[]{
                                                                                                                    "oems_localdelegationopendatetime",
                                                                                                                    "oems_localdelegationclosedatetime"
                                                                                                                }));

                        oemsEvent["oems_localdelegationopendatetime"] = CrmTools.getEnitiyAttributeSafely(tempEvent, "oems_localdelegationopendatetime");
                        oemsEvent["oems_localdelegationclosedatetime"] = CrmTools.getEnitiyAttributeSafely(tempEvent, "oems_localdelegationclosedatetime");

                        //// get list of existing event locals and compare to copied event
                        //var localDelegations = crmTools.getRelatedEntities(
                        //                           new ColumnSet("oems_eventid"),
                        //                           selectedEventRef.LogicalName,
                        //                           selectedEventRef.Id,
                        //                           "oems_eventlocal",
                        //                           new ConditionExpression("statecode", ConditionOperator.Equal, StateCode.Active),
                        //                           "oems_event_to_eventlocal");

                        //if (localDelegations != null)
                        //{
                        //    foreach (var localDelegation in localDelegations)
                        //    {
                        //        var newEventLocal = new Entity("oems_eventlocal");
                        //        newEventLocal.Id = Guid.NewGuid();
                        //        newEventLocal["oems_eventlocalid"] = newEventLocal.Id;

                        //        newEventLocal["oems_event"] = new EntityReference() { LogicalName = oemsEvent.LogicalName, Id = oemsEvent.Id };
                        //        newEventLocal["oems_eventlocalname"] = CrmTools.getEnitiyAttributeSafely(localDelegation, "oems_eventlocalname");
                        //        newEventLocal["oems_local"] = CrmTools.getEnitiyAttributeSafely(localDelegation, "oems_local");
                        //        newEventLocal["oems_localdelegationlimit"] = CrmTools.getEnitiyAttributeSafely(localDelegation, "oems_localdelegationlimit");
                        //        newEventLocal["statuscode"] = 1;

                        //        this.wfActivity.service.Create(newEventLocal);
                        //    }
                        //}
                    }

                    #endregion

                    #region Release Time

                    flag =  oemsEvent.GetAttributeValue<bool>("oems_releasetimecomponentflag");
                    if (flag)
                    {
                        var releaseTimeSchedules = crmTools.getRelatedEntities(
                                                    new ColumnSet("oems_eventid"),
                                                    selectedEventRef.LogicalName,
                                                    selectedEventRef.Id,
                                                    "oems_eventreleasetimeschedule",
                                                    new ConditionExpression("statecode", ConditionOperator.Equal, StateCode.Active),
                                                    "oems_event_to_eventreleasetimeschedule");

                        if (releaseTimeSchedules != null)
                        {
                            foreach (var schedule in releaseTimeSchedules)
                            {
                                var newSchedule = new Entity("oems_eventreleasetimeschedule");
                                newSchedule.Id = Guid.NewGuid();
                                newSchedule["oems_eventreleasetimescheduleid"] = newSchedule.Id;

                                newSchedule["oems_event"] = new EntityReference() { LogicalName = oemsEvent.LogicalName, Id = oemsEvent.Id };//schedule["oems_event"];
                                newSchedule["oems_eventreleasetimeschedulename"] = CrmTools.getEnitiyAttributeSafely(schedule, "oems_eventreleasetimeschedulename");
                                newSchedule["oems_releasedate"] = CrmTools.getEnitiyAttributeSafely(schedule, "oems_releasedate");

                                this.wfActivity.service.Create(newSchedule);
                            }
                        }
                    }

                    #endregion

                    #region Tickets

                    flag =  oemsEvent.GetAttributeValue<bool>("oems_ticketcomponentflag");
                    if (flag)
                    {
                        var tempEvent = wfActivity.service.Retrieve(selectedEventRef.LogicalName, selectedEventRef.Id, new ColumnSet(new string[]{
                                                                                                                    "oems_ticketprice",
                                                                                                                    "oems_ticketapplyhst"
                                                                                                                }));

                        oemsEvent["oems_ticketprice"] = CrmTools.getEnitiyAttributeSafely(tempEvent, "oems_ticketprice");
                        oemsEvent["oems_ticketapplyhst"] = CrmTools.getEnitiyAttributeSafely(tempEvent, "oems_ticketapplyhst");
                    }
                    
                    #endregion

                    #region Billing

                    flag =  oemsEvent.GetAttributeValue<bool>("oems_billingcomponentflag");
                    if (flag)
                    {
                        var tempEvent = wfActivity.service.Retrieve(selectedEventRef.LogicalName, selectedEventRef.Id, new ColumnSet(new string[]{
                                                                                                                    "oems_billingchequeduedate",
                                                                                                                    "oems_billingacceptcheque",
                                                                                                                    "oems_billingpaylater",
                                                                                                                    "oems_additionalfeeforeventmaterials",
                                                                                                                    "oems_registrationfeeamount",
                                                                                                                    "oems_billingportaldescription",
                                                                                                                    "oems_coursehistorytimeframefordiscountsmonths",
                                                                                                                    "oems_donotautomaticallyrefundoncancellation",
                                                                                                                    "oems_refundlessregistrationfee"
                                                                                                                }));

                        oemsEvent["oems_billingchequeduedate"] = CrmTools.getEnitiyAttributeSafely(tempEvent, "oems_billingchequeduedate");
                        oemsEvent["oems_billingacceptcheque"] = CrmTools.getEnitiyAttributeSafely(tempEvent, "oems_billingacceptcheque");
                        oemsEvent["oems_billingpaylater"] = CrmTools.getEnitiyAttributeSafely(tempEvent, "oems_billingpaylater");
                        oemsEvent["oems_additionalfeeforeventmaterials"] = CrmTools.getEnitiyAttributeSafely(tempEvent, "oems_additionalfeeforeventmaterials");
                        oemsEvent["oems_registrationfeeamount"] = CrmTools.getEnitiyAttributeSafely(tempEvent, "oems_registrationfeeamount");
                        oemsEvent["oems_billingportaldescription"] = CrmTools.getEnitiyAttributeSafely(tempEvent, "oems_billingportaldescription");
                        oemsEvent["oems_coursehistorytimeframefordiscountsmonths"] = CrmTools.getEnitiyAttributeSafely(tempEvent, "oems_coursehistorytimeframefordiscountsmonths");
                        oemsEvent["oems_donotautomaticallyrefundoncancellation"] = CrmTools.getEnitiyAttributeSafely(tempEvent, "oems_donotautomaticallyrefundoncancellation");
                        oemsEvent["oems_refundlessregistrationfee"] = CrmTools.getEnitiyAttributeSafely(tempEvent, "oems_refundlessregistrationfee");

                        //Event pricing
                        var eventPrincings = crmTools.getRelatedEntities(
                                                    new ColumnSet("oems_eventid"),
                                                    selectedEventRef.LogicalName,
                                                    selectedEventRef.Id,
                                                    "oems_eventpricing",
                                                    new ConditionExpression("statecode", ConditionOperator.Equal, StateCode.Active),
                                                    "oems_oems_event_oems_eventpricing_Event");
                        if (eventPrincings != null)
                        {
                            foreach (var pricing in eventPrincings)
                            {
                                var newPricing = new Entity("oems_eventpricing");
                                newPricing.Id = Guid.NewGuid();
                                newPricing["oems_eventpricingid"] = newPricing.Id;

                                newPricing["oems_event"] = new EntityReference() { LogicalName = oemsEvent.LogicalName, Id = oemsEvent.Id };
                                newPricing["oems_name"] = CrmTools.getEnitiyAttributeSafely(pricing, "oems_name");
                                newPricing["oems_participanttype"] = CrmTools.getEnitiyAttributeSafely(pricing, "oems_participanttype");
                                newPricing["oems_price"] = CrmTools.getEnitiyAttributeSafely(pricing, "oems_price");
                                newPricing["oems_applyhst"] = CrmTools.getEnitiyAttributeSafely(pricing, "oems_applyhst");
                                
                                this.wfActivity.service.Create(newPricing);
                            }
                        }

                        //Discounts
                        var discounts = crmTools.getRelatedEntities(
                                                    new ColumnSet("oems_eventid"),
                                                    selectedEventRef.LogicalName,
                                                    selectedEventRef.Id,
                                                    "oems_eventworkshopdiscount",
                                                    new ConditionExpression("statecode", ConditionOperator.Equal, StateCode.Active),
                                                    "oems_oems_event_oems_eventworkshopdiscount");
                        if (discounts != null)
                        {
                            foreach (var discount in discounts)
                            {
                                var newDiscount = new Entity("oems_eventworkshopdiscount");
                                newDiscount.Id = Guid.NewGuid();
                                newDiscount["oems_eventworkshopdiscountid"] = newDiscount.Id;

                                newDiscount["oems_event"] = new EntityReference() { LogicalName = oemsEvent.LogicalName, Id = oemsEvent.Id };
                                newDiscount["oems_name"] = CrmTools.getEnitiyAttributeSafely(discount, "oems_name");
                                newDiscount["oems_numberofworkshops"] = CrmTools.getEnitiyAttributeSafely(discount, "oems_numberofworkshops");
                                newDiscount["oems_discountpercentage"] = CrmTools.getEnitiyAttributeSafely(discount, "oems_discountpercentage");

                                this.wfActivity.service.Create(newDiscount);
                            }
                        }

                        //Refund policies
                        var refundPolicies = crmTools.getRelatedEntities(
                                                    new ColumnSet("oems_eventid"),
                                                    selectedEventRef.LogicalName,
                                                    selectedEventRef.Id,
                                                    "oems_eventrefundpolicy",
                                                    new ConditionExpression("statecode", ConditionOperator.Equal, StateCode.Active),
                                                    "oems_event_oems_eventrefundpolicy_event");
                        if (refundPolicies != null)
                        {
                            foreach (var refundPolicy in refundPolicies)
                            {
                                var newRefundPolicy = new Entity("oems_eventrefundpolicy");
                                newRefundPolicy.Id = Guid.NewGuid();
                                newRefundPolicy["oems_eventrefundpolicyid"] = newRefundPolicy.Id;

                                newRefundPolicy["oems_event"] = new EntityReference() { LogicalName = oemsEvent.LogicalName, Id = oemsEvent.Id };
                                newRefundPolicy["oems_name"] = CrmTools.getEnitiyAttributeSafely(refundPolicy, "oems_name");
                                newRefundPolicy["oems_cancellationdate"] = CrmTools.getEnitiyAttributeSafely(refundPolicy, "oems_cancellationdate");
                                newRefundPolicy["oems_refundpercentage"] = CrmTools.getEnitiyAttributeSafely(refundPolicy, "oems_refundpercentage");

                                this.wfActivity.service.Create(newRefundPolicy);
                            }
                        }
                    }

                    #endregion

                    #region Presenter Application

                    flag = oemsEvent.GetAttributeValue<bool>("oems_workshopapplicationcomponentflag");
                    if (flag || (flag == false && oemsEvent.GetAttributeValue<EntityReference>("oems_applicationtemplate") != null))
                    {
                        var tempEvent = wfActivity.service.Retrieve(selectedEventRef.LogicalName, selectedEventRef.Id, new ColumnSet("oems_applicationopening", 
                                                                                                                                    "oems_applicationdeadline",
                                                                                                                                    "oems_applicationtemplate",
                                                                                                                                    "oems_instructorcourseagreementblankform1",
                                                                                                                                    "oems_instructorpayrollenrollmentblankform1",
                                                                                                                                    "oems_sampleparticipationletter",
                                                                                                                                    "oems_lettertemplate"));

                        oemsEvent["oems_applicationopening"] = CrmTools.getEnitiyAttributeSafely(tempEvent, "oems_applicationopening");
                        oemsEvent["oems_applicationdeadline"] = CrmTools.getEnitiyAttributeSafely(tempEvent, "oems_applicationdeadline");
                        oemsEvent["oems_applicationtemplate"] = CrmTools.getEnitiyAttributeSafely(tempEvent, "oems_applicationtemplate");
                        oemsEvent["oems_instructorcourseagreementblankform1"] = CrmTools.getEnitiyAttributeSafely(tempEvent, "oems_instructorcourseagreementblankform1");
                        oemsEvent["oems_instructorpayrollenrollmentblankform1"] = CrmTools.getEnitiyAttributeSafely(tempEvent, "oems_instructorpayrollenrollmentblankform1");
                        oemsEvent["oems_sampleparticipationletter"] = CrmTools.getEnitiyAttributeSafely(tempEvent, "oems_sampleparticipationletter");
                        oemsEvent["oems_lettertemplate"] = CrmTools.getEnitiyAttributeSafely(tempEvent, "oems_lettertemplate");

                        var eventAppSchedules = crmTools.getRelatedEntities(
                                                    new ColumnSet("oems_eventid"),
                                                    selectedEventRef.LogicalName,
                                                    selectedEventRef.Id,
                                                    "oems_eventapplicationschedule",
                                                    new ConditionExpression("statecode", ConditionOperator.Equal, StateCode.Active),
                                                    "oems_event_to_eventapplicationschedule");

                        if (eventAppSchedules != null)
                        {
                            foreach (var schedule in eventAppSchedules)
                            {
                                var newSchedule = new Entity("oems_eventapplicationschedule");
                                newSchedule.Id = Guid.NewGuid();
                                newSchedule["oems_eventapplicationscheduleid"] = newSchedule.Id;

                                newSchedule["oems_event"] = new EntityReference() { LogicalName = oemsEvent.LogicalName, Id = oemsEvent.Id };//schedule["oems_event"];
                                newSchedule["oems_applicationschedulename"] = CrmTools.getEnitiyAttributeSafely(schedule, "oems_applicationschedulename");
                                newSchedule["oems_applicanttype"] = CrmTools.getEnitiyAttributeSafely(schedule, "oems_applicanttype");
                                newSchedule["oems_applicationopendatetime"] = CrmTools.getEnitiyAttributeSafely(schedule, "oems_applicationopendatetime");
                                newSchedule["oems_applicationclosedatetime"] = CrmTools.getEnitiyAttributeSafely(schedule, "oems_applicationclosedatetime");

                                this.wfActivity.service.Create(newSchedule);
                            }
                        }

                        //Target Grades
                        var targetGrades = crmTools.getRelatedEntities(wfActivity.service, selectedEventRef, "oems_targetgrade");

                        if (targetGrades != null)
                        {
                            crmTools.setLookupField(targetGrades, oemsEvent.ToEntityReference(), "oems_coursetermpresenterapplication");
                        }

                        //Application Topics 
                        var applicationTopics = crmTools.getRelatedEntities(wfActivity.service, selectedEventRef, "oems_coursetopic");

                        if (applicationTopics != null)
                        {
                            crmTools.setLookupField(applicationTopics, oemsEvent.ToEntityReference(), "oems_coursetermpresenterapplication");
                        }

                        //Weeks of Availability
                        var weeks = crmTools.getRelatedEntities(wfActivity.service, selectedEventRef, "oems_termweek","oems_event");

                        if (weeks != null)
                        {
                            foreach (var week in weeks)
                            {
                                Entity newWeek = new Entity(week.LogicalName);
                                newWeek["oems_name"] = CrmTools.getEnitiyAttributeSafely(week, "oems_name");
                                newWeek["oems_event"] = oemsEvent.ToEntityReference();
                                wfActivity.service.Create(newWeek);
                            }
                        }
                    }

                    #endregion

                    #region Workshop

                    flag =  oemsEvent.GetAttributeValue<bool>("oems_workshopcomponentflag");
                    if (flag)
                    {
                        //Worshops
                        var workshopMappings = new Dictionary<Guid, Guid>();
                        var workshops = crmTools.getRelatedEntities(
                                                    new ColumnSet("oems_eventid"),
                                                    selectedEventRef.LogicalName,
                                                    selectedEventRef.Id,
                                                    "oems_eventworkshop",
                                                    new ConditionExpression("statecode", ConditionOperator.Equal, StateCode.Active),
                                                    "oems_event_to_eventworkshop");

                        if (workshops != null)
                        {
                            foreach (var workshop in workshops)
                            {
                                var newWorkshop = new Entity("oems_eventworkshop");
                                newWorkshop.Id = Guid.NewGuid();
                                newWorkshop["oems_eventworkshopid"] = newWorkshop.Id;

                                newWorkshop["oems_event"] = new EntityReference() { LogicalName = oemsEvent.LogicalName, Id = oemsEvent.Id };//schedule["oems_event"];
                                newWorkshop["oems_workshopname"] = CrmTools.getEnitiyAttributeSafely(workshop, "oems_workshopname");
                                newWorkshop["oems_workshopcode"] = CrmTools.getEnitiyAttributeSafely(workshop, "oems_workshopcode");
                                newWorkshop["oems_workshopdescription"] = CrmTools.getEnitiyAttributeSafely(workshop, "oems_workshopdescription");
                                newWorkshop["oems_minimumattendees"] = CrmTools.getEnitiyAttributeSafely(workshop, "oems_minimumattendees");
                                newWorkshop["oems_maximumattendees"] = CrmTools.getEnitiyAttributeSafely(workshop, "oems_maximumattendees");
                                newWorkshop["oems_workshopapplication"] = CrmTools.getEnitiyAttributeSafely(workshop, "oems_workshopapplication");
                                workshopMappings.Add(workshop.Id, newWorkshop.Id);

                                this.wfActivity.service.Create(newWorkshop);
                            }
                        }

                        //Sessions
                        var sessions = crmTools.getRelatedEntities(
                                                    new ColumnSet("oems_eventid"),
                                                    selectedEventRef.LogicalName,
                                                    selectedEventRef.Id,
                                                    "oems_eventsession",
                                                    new ConditionExpression("statecode", ConditionOperator.Equal, StateCode.Active),
                                                    "oems_event_to_eventsession");

                        if (sessions != null)
                        {
                            foreach (var session in sessions)
                            {
                                var newSession = new Entity("oems_eventsession");
                                newSession.Id = Guid.NewGuid();
                                newSession["oems_eventsessionid"] = newSession.Id;

                                newSession["oems_event"] = new EntityReference() { LogicalName = oemsEvent.LogicalName, Id = oemsEvent.Id };//schedule["oems_event"];
                                newSession["oems_sessionname"] = CrmTools.getEnitiyAttributeSafely(session, "oems_sessionname");
                                newSession["oems_startdatetime"] = CrmTools.getEnitiyAttributeSafely(session, "oems_startdatetime");
                                newSession["oems_enddatetime"] = CrmTools.getEnitiyAttributeSafely(session, "oems_enddatetime");

                                this.wfActivity.service.Create(newSession);

                                //Workshop sessions
                                var workshopSessions = crmTools.getRelatedEntities(
                                                    new ColumnSet("oems_eventsessionid"),
                                                    session.LogicalName,
                                                    session.Id,
                                                    "oems_eventworkshopsession",
                                                    new ConditionExpression("statecode", ConditionOperator.Equal, StateCode.Active),
                                                    "oems_eventsession_to_eventworkshopsession");

                                if (workshopSessions != null)
                                {
                                    foreach (var workshopSession in workshopSessions)
                                    {
                                        var newWorkshopSession = new Entity("oems_eventworkshopsession");
                                        newWorkshopSession.Id = Guid.NewGuid();
                                        newWorkshopSession["oems_eventworkshopsessionid"] = newWorkshopSession.Id;

                                        //Workshop ID
                                        var wref = (EntityReference)workshopSession["oems_eventworkshop"];
                                        var wid = workshopMappings[wref.Id];

                                        newWorkshopSession["oems_event"] = new EntityReference() { LogicalName = oemsEvent.LogicalName, Id = oemsEvent.Id };
                                        newWorkshopSession["oems_eventsession"] = new EntityReference() { LogicalName = newSession.LogicalName, Id = newSession.Id };
                                        newWorkshopSession["oems_eventworkshop"] = new EntityReference() { LogicalName = wref.LogicalName, Id = wid };
                                        newWorkshopSession["oems_roomassignment"] = CrmTools.getEnitiyAttributeSafely(workshopSession, "oems_roomassignment");
                                        newWorkshopSession["oems_workshopsessionname"] = CrmTools.getEnitiyAttributeSafely(workshopSession, "oems_workshopsessionname");

                                        this.wfActivity.service.Create(newWorkshopSession);
                                    }
                                }
                            }
                        }
                    }

                    #endregion

                    #region Waiting List

                    flag =  oemsEvent.GetAttributeValue<bool>("oems_waitinglistcomponentflag");
                    if (flag)
                    {
                        var tempEvent = wfActivity.service.Retrieve(selectedEventRef.LogicalName, selectedEventRef.Id, new ColumnSet(new string[]{
                                                                                                                    "oems_waitinglistmanualflag",
                                                                                                                    "oems_waitinglistsize",
                                                                                                                    "oems_waitinglistofferperiodhours"
                                                                                                                }));

                        oemsEvent["oems_waitinglistmanualflag"] = CrmTools.getEnitiyAttributeSafely(tempEvent, "oems_waitinglistmanualflag");
                        oemsEvent["oems_waitinglistsize"] = CrmTools.getEnitiyAttributeSafely(tempEvent, "oems_waitinglistsize");
                        oemsEvent["oems_waitinglistofferperiodhours"] = CrmTools.getEnitiyAttributeSafely(tempEvent, "oems_waitinglistofferperiodhours");
                    }

                    #endregion

                    #region Course

                    flag = oemsEvent.GetAttributeValue<bool>("oems_courseflag");
                    if (flag)
                    {
                        //copy the course requred fields
                        var tempEvent = wfActivity.service.Retrieve(selectedEventRef.LogicalName, selectedEventRef.Id, new ColumnSet("oems_courseterm", 
                                                                                                                                        "oems_coursetopic",
                                                                                                                                        "oems_subcategory",
                                                                                                                                        "oems_coursedescription",
                                                                                                                                        "oems_coursetargetgrades",
                                                                                                                                        "oems_budgetnumber",
                                                                                                                                        "oems_budgetnumber2"));
                        oemsEvent["oems_presenters"] = CrmTools.getEnitiyAttributeSafely(tempEvent, "oems_presenters");
                        oemsEvent["oems_courseterm"] = CrmTools.getEnitiyAttributeSafely(tempEvent, "oems_courseterm");
                        oemsEvent["oems_coursetopic"] = CrmTools.getEnitiyAttributeSafely(tempEvent, "oems_coursetopic");
                        oemsEvent["oems_subcategory"] = CrmTools.getEnitiyAttributeSafely(tempEvent, "oems_subcategory");
                        oemsEvent["oems_coursedescription"] = CrmTools.getEnitiyAttributeSafely(tempEvent, "oems_coursedescription");
                        oemsEvent["oems_coursetargetgrades"] = CrmTools.getEnitiyAttributeSafely(tempEvent, "oems_coursetargetgrades");
                        oemsEvent["oems_budgetnumber"] = CrmTools.getEnitiyAttributeSafely(tempEvent, "oems_budgetnumber");
                        oemsEvent["oems_budgetnumber2"] = CrmTools.getEnitiyAttributeSafely(tempEvent, "oems_budgetnumber2");

                        EntityReference courseTerm = (EntityReference)CrmTools.getEnitiyAttributeSafely(tempEvent, "oems_courseterm");
                        var term = wfActivity.service.Retrieve(courseTerm.LogicalName, courseTerm.Id, new ColumnSet("oems_eventyear"));
                        oemsEvent["oems_eventyear"] = CrmTools.getEnitiyAttributeSafely(term, "oems_eventyear");
                    }

                    #endregion

                    #region Course Term

                    flag = oemsEvent.GetAttributeValue<bool>("oems_coursetermflag");
                    if (flag)
                    {
                        //copy the course term requred fields
                        var tempEvent = wfActivity.service.Retrieve(selectedEventRef.LogicalName, selectedEventRef.Id, new ColumnSet("oems_coursetermcode", 
                                                                                                                                        "oems_webtermdescription", 
                                                                                                                                        "oems_autoapproveflag",
                                                                                                                                        "oems_defaultregistrationstartdate",
                                                                                                                                        "oems_defaultregistrationenddate",
                                                                                                                                        "oems_instructorcourseagreementblankform1",
                                                                                                                                        "oems_instructorpayrollenrollmentblankform1"));

                        oemsEvent["oems_coursetermcode"] = CrmTools.getEnitiyAttributeSafely(tempEvent, "oems_coursetermcode");
                        oemsEvent["oems_webtermdescription"] = CrmTools.getEnitiyAttributeSafely(tempEvent, "oems_webtermdescription");
                        oemsEvent["oems_autoapproveflag"] = CrmTools.getEnitiyAttributeSafely(tempEvent, "oems_autoapproveflag");
                        oemsEvent["oems_defaultregistrationstartdate"] = CrmTools.getEnitiyAttributeSafely(tempEvent, "oems_defaultregistrationstartdate");
                        oemsEvent["oems_defaultregistrationenddate"] = CrmTools.getEnitiyAttributeSafely(tempEvent, "oems_defaultregistrationenddate");

                        //Date slots
                        List<Entity> dateslots = crmTools.getRelatedEntities(wfActivity.service, selectedEventRef, "oems_coursedateslots", "oems_event");

                        if (dateslots != null)
                        {
                            foreach (var item in dateslots)
                            {
                                Entity newDateSlot = new Entity(item.LogicalName);
                                newDateSlot["oems_enddate"] = CrmTools.getEnitiyAttributeSafely(item, "oems_enddate");
                                newDateSlot["oems_startdate"] = CrmTools.getEnitiyAttributeSafely(item, "oems_startdate");
                                newDateSlot["oems_week_number"] = CrmTools.getEnitiyAttributeSafely(item, "oems_week_number");
                                newDateSlot["oems_event"] = oemsEvent.ToEntityReference();
                                newDateSlot["oems_name"] = CrmTools.getEnitiyAttributeSafely(item, "oems_name");
                                wfActivity.service.Create(newDateSlot);
                            }
                        }

                        //Time slots
                        var timeslots = crmTools.getRelatedEntities(wfActivity.service, selectedEventRef, "oems_coursetimeslot");
                        if (timeslots != null)
                        {
                            crmTools.setLookupField(timeslots, oemsEvent.ToEntityReference(), "oems_courseterm"); 
                        }

                        //Course Topics
                        var courseTopics = crmTools.getRelatedEntities(wfActivity.service, selectedEventRef, "oems_coursetopic");
                        if (courseTopics != null)
                        {
                            crmTools.setLookupField(courseTopics, oemsEvent.ToEntityReference(), "oems_courseterm"); 
                        }

                        //Local Hosts
                        var localHosts = crmTools.getRelatedEntities(wfActivity.service, selectedEventRef, "oems_courselocalhost");
                        if (localHosts != null)
                        {
                            crmTools.setLookupField(localHosts, oemsEvent.ToEntityReference(), "oems_courseterm"); 
                        }

                        //Course locations
                        var courseLocations = crmTools.getRelatedEntities(wfActivity.service, selectedEventRef,"oems_courselocation");
                        if (courseLocations != null)
                        {
                            crmTools.setLookupField(courseLocations, oemsEvent.ToEntityReference(), "oems_courseterm"); 
                        }

                        //Target Grades
                        var courseTargetGrades = crmTools.getRelatedEntities(wfActivity.service, selectedEventRef, "oems_targetgrade");
                        if (courseTargetGrades != null)
                        {
                            crmTools.setLookupField(courseTargetGrades, oemsEvent.ToEntityReference(), "oems_courseterm"); 
                        }
                    }

                    #endregion

                    #region Web Page

                    //recover the webpage and publishing state
                    var tempEvent1 = wfActivity.service.Retrieve(selectedEventRef.LogicalName, selectedEventRef.Id, new ColumnSet(new string[]{
                                                                                                                    "oems_webpage",
                                                                                                                    "oems_webpagepublishingstate",
                                                                                                                    "oems_webpagereleasedate",
                                                                                                                    "oems_webpageexpirationdate",
                                                                                                                    "oems_eventaccesspublicflag",
                                                                                                                    "oems_eventaccessmemberflag",
                                                                                                                    "oems_eventaccesspresidentflag",
                                                                                                                    "oems_eventaccessnonmemberstaffflag",
                                                                                                                    "oems_eventaccessnonmemberteacherflag",
                                                                                                                    "oems_eventaccessnonmemberexternalflag"
                                                                                                                }));

                    var webPageRef = (EntityReference)CrmTools.getEnitiyAttributeSafely(tempEvent1, "oems_webpage");
                    oemsEvent["oems_webpagepublishingstate"] = crmTools.getEntity("adx_publishingstate", "adx_name", "Draft").ToEntityReference();
                    oemsEvent["oems_webpagereleasedate"] = CrmTools.getEnitiyAttributeSafely(tempEvent1, "oems_webpagereleasedate");
                    oemsEvent["oems_webpageexpirationdate"] = CrmTools.getEnitiyAttributeSafely(tempEvent1, "oems_webpageexpirationdate");
                    oemsEvent["oems_eventaccesspublicflag"] = CrmTools.getEnitiyAttributeSafely(tempEvent1, "oems_eventaccesspublicflag");
                    oemsEvent["oems_eventaccessmemberflag"] = CrmTools.getEnitiyAttributeSafely(tempEvent1, "oems_eventaccessmemberflag");
                    oemsEvent["oems_eventaccesspresidentflag"] = CrmTools.getEnitiyAttributeSafely(tempEvent1, "oems_eventaccesspresidentflag");
                    oemsEvent["oems_eventaccessnonmemberstaffflag"] = CrmTools.getEnitiyAttributeSafely(tempEvent1, "oems_eventaccessnonmemberstaffflag");
                    oemsEvent["oems_eventaccessnonmemberteacherflag"] = CrmTools.getEnitiyAttributeSafely(tempEvent1, "oems_eventaccessnonmemberteacherflag");
                    oemsEvent["oems_eventaccessnonmemberexternalflag"] = CrmTools.getEnitiyAttributeSafely(tempEvent1, "oems_eventaccessnonmemberexternalflag");
                    oemsEvent["oems_displayexecutivestaffowners"] = CrmTools.getEnitiyAttributeSafely(tempEvent1, "oems_displayexecutivestaffowners");
                    oemsEvent["oems_displaysupportstaffowners"] = CrmTools.getEnitiyAttributeSafely(tempEvent1, "oems_displaysupportstaffowners");
                    
                    if (webPageRef != null)
                    {
                        //Create a copy of the web page
                        var webPage = wfActivity.service.Retrieve
                        (
                            webPageRef.LogicalName, 
                            webPageRef.Id, new ColumnSet
                            (
                                "adx_name",
                                "adx_websiteid",
                                "adx_parentpageid",
                                "adx_partialurl",
                                "adx_displaydate",
                                "adx_pagetemplateid",
                                "adx_publishingstateid",
                                "adx_releasedate",
                                "adx_expirationdate",
                                "adx_webform",
                                "adx_entityform",
                                "adx_entitylist",
                                "oems_subcategoryid",
                                "adx_title",
                                "oems_bannertitle",
                                "oems_htmltodisplay",
                                "oems_presenterhtml",
                                "oems_registrationhtml",
                                "adx_copy",
                                "adx_summary",
                                "oems_sidebar",
                                "adx_enablecomments",
                                "adx_enableratings",
                                "adx_enabletracking",
                                "adx_hiddenfromsitemap",
                                "adx_commentpolicy",
                                "adx_navigation",
                                "adx_authorid",
                                "adx_category",
                                "adx_subjectid",
                                "adx_displayorder",
                                "adx_excludefromsearch",
                                "adx_masterwebpageid",
                                "adx_image",
                                "adx_imageurl",
                                "adx_editorialcomments",
                                "oems_headerclass",
                                "statecode",
                                "statuscode"
                            )
                        );
                        var newWebPage = new Entity("adx_webpage");
                        newWebPage.Id = Guid.NewGuid();
                        newWebPage["adx_webpageid"] = newWebPage.Id;
                        newWebPage["oems_eventid"] = new EntityReference() { LogicalName = oemsEvent.LogicalName, Id = oemsEvent.Id };
                        newWebPage["adx_name"] = "Web Page for " + newName;
                        newWebPage["adx_websiteid"] = CrmTools.getEnitiyAttributeSafely(webPage, "adx_websiteid");
                        newWebPage["adx_parentpageid"] = CrmTools.getEnitiyAttributeSafely(webPage, "adx_parentpageid");
                        newWebPage["adx_partialurl"] = newWebPage.Id.ToString();
                        newWebPage["adx_displaydate"] = CrmTools.getEnitiyAttributeSafely(webPage, "adx_displaydate");
                        newWebPage["adx_pagetemplateid"] = CrmTools.getEnitiyAttributeSafely(webPage, "adx_pagetemplateid");
                        newWebPage["adx_publishingstateid"] = crmTools.getEntity("adx_publishingstate", "adx_name", "Draft").ToEntityReference();
                        newWebPage["adx_releasedate"] = CrmTools.getEnitiyAttributeSafely(webPage, "adx_releasedate");
                        newWebPage["adx_expirationdate"] = CrmTools.getEnitiyAttributeSafely(webPage, "adx_expirationdate");
                        newWebPage["adx_webform"] = CrmTools.getEnitiyAttributeSafely(webPage, "adx_webform");
                        newWebPage["adx_entityform"] = CrmTools.getEnitiyAttributeSafely(webPage, "adx_entityform");
                        newWebPage["adx_entitylist"] = CrmTools.getEnitiyAttributeSafely(webPage, "adx_entitylist");
                        newWebPage["oems_subcategoryid"] = CrmTools.getEnitiyAttributeSafely(webPage, "oems_subcategoryid");
                        newWebPage["adx_title"] = CrmTools.getEnitiyAttributeSafely(webPage, "adx_title");
                        newWebPage["oems_bannertitle"] = CrmTools.getEnitiyAttributeSafely(webPage, "oems_bannertitle");
                        newWebPage["oems_htmltodisplay"] = CrmTools.getEnitiyAttributeSafely(webPage, "oems_htmltodisplay");
                        newWebPage["oems_presenterhtml"] = CrmTools.getEnitiyAttributeSafely(webPage, "oems_presenterhtml");
                        newWebPage["oems_registrationhtml"] = CrmTools.getEnitiyAttributeSafely(webPage, "oems_registrationhtml");
                        newWebPage["adx_copy"] = CrmTools.getEnitiyAttributeSafely(webPage, "adx_copy");
                        newWebPage["adx_summary"] = CrmTools.getEnitiyAttributeSafely(webPage, "adx_summary");
                        newWebPage["oems_sidebar"] = CrmTools.getEnitiyAttributeSafely(webPage, "oems_sidebar");
                        newWebPage["adx_enablecomments"] = CrmTools.getEnitiyAttributeSafely(webPage, "adx_enablecomments");
                        newWebPage["adx_enableratings"] = CrmTools.getEnitiyAttributeSafely(webPage, "adx_enableratings");
                        newWebPage["adx_enabletracking"] = CrmTools.getEnitiyAttributeSafely(webPage, "adx_enabletracking");
                        newWebPage["adx_hiddenfromsitemap"] = CrmTools.getEnitiyAttributeSafely(webPage, "adx_hiddenfromsitemap");
                        newWebPage["adx_commentpolicy"] = CrmTools.getEnitiyAttributeSafely(webPage, "adx_commentpolicy");
                        newWebPage["adx_navigation"] = CrmTools.getEnitiyAttributeSafely(webPage, "adx_navigation");
                        newWebPage["adx_authorid"] = CrmTools.getEnitiyAttributeSafely(webPage, "adx_authorid");
                        newWebPage["adx_category"] = CrmTools.getEnitiyAttributeSafely(webPage, "adx_category");
                        newWebPage["adx_subjectid"] = CrmTools.getEnitiyAttributeSafely(webPage, "adx_subjectid");
                        newWebPage["adx_displayorder"] = CrmTools.getEnitiyAttributeSafely(webPage, "adx_displayorder");
                        newWebPage["adx_excludefromsearch"] = CrmTools.getEnitiyAttributeSafely(webPage, "adx_excludefromsearch");
                        newWebPage["adx_masterwebpageid"] = CrmTools.getEnitiyAttributeSafely(webPage, "adx_masterwebpageid");
                        newWebPage["adx_image"] = CrmTools.getEnitiyAttributeSafely(webPage, "adx_image");
                        newWebPage["adx_imageurl"] = CrmTools.getEnitiyAttributeSafely(webPage, "adx_imageurl");
                        newWebPage["adx_editorialcomments"] = CrmTools.getEnitiyAttributeSafely(webPage, "adx_editorialcomments");
                        newWebPage["oems_headerclass"] = CrmTools.getEnitiyAttributeSafely(webPage, "oems_headerclass");
                        newWebPage["statecode"] = CrmTools.getEnitiyAttributeSafely(webPage, "statecode");
                        newWebPage["statuscode"] = CrmTools.getEnitiyAttributeSafely(webPage, "statuscode");
                        var wpid = wfActivity.service.Create(newWebPage);

                        oemsEvent["oems_webpage"] = new EntityReference(newWebPage.LogicalName, newWebPage.Id);
                    }

                    wfActivity.service.Update(oemsEvent);

                    #endregion

                    #region User Privileges

                    var userPrivileges = crmTools.getRelatedEntities(
                                                    new ColumnSet("oems_eventid"),
                                                    selectedEventRef.LogicalName,
                                                    selectedEventRef.Id,
                                                    "oems_eventuserprivilege",
                                                    new ConditionExpression("statecode", ConditionOperator.Equal, StateCode.Active),
                                                    "oems_event_to_eventuserprivilege");

                    if (userPrivileges != null)
                    {
                        foreach (var userPrivilege in userPrivileges)
                        {
                            var newUserPrivilege = new Entity("oems_eventuserprivilege");
                            newUserPrivilege.Id = Guid.NewGuid();
                            newUserPrivilege["oems_eventuserprivilegeid"] = newUserPrivilege.Id;

                            newUserPrivilege["oems_event"] = new EntityReference() { LogicalName = oemsEvent.LogicalName, Id = oemsEvent.Id };
                            newUserPrivilege["oems_user"] = CrmTools.getEnitiyAttributeSafely(userPrivilege, "oems_user");
                            newUserPrivilege["oems_privilege"] = CrmTools.getEnitiyAttributeSafely(userPrivilege, "oems_privilege");
                            newUserPrivilege["oems_expirydate"] = CrmTools.getEnitiyAttributeSafely(userPrivilege, "oems_expirydate");

                            this.wfActivity.service.Create(newUserPrivilege);
                        }
                    }

                    #endregion

                    #region User Registrations

                    if (doSection)
                    {
                        var fetch =
                        @" 
                            <fetch distinct='false' mapping='logical' aggregate='false'> 
                                <entity name='oems_eventregistration'>
                                    <attribute name='oems_eventregistrationid' alias='oems_eventregistrationid' />
                                    <attribute name='statuscode' alias='statuscode' />
                                    <attribute name='oems_registrationtype' alias='oems_registrationtype' />
                                    <attribute name='oems_account' alias='oems_account' />
                                    <attribute name='oems_dynamicregformdata' alias='oems_dynamicregformdata' />
                                    <filter type='and'>
                                        <condition attribute='oems_event' operator='eq' value='{0}' />
                                        <condition attribute='oems_registrationtype' operator='eq' value='{1}' />
                                        <condition attribute='statuscode' operator='eq' value='{2}' />
                                    </filter>
                                    <link-entity name='oems_waitinglistrequest' from='oems_eventregistration' to='oems_eventregistrationid'>
			                            <filter type='and'>
                                            <condition attribute='oems_event' operator='eq' value='{0}' />
                                            <filter type='or'>
                                                <condition attribute='statuscode' operator='eq' value='{3}' />
                                            </filter>
                                        </filter>
		                            </link-entity>
                                </entity>
                            </fetch>
                        ";
                        fetch = String.Format(fetch, selectedEventRef.Id, Convert.ToInt32(RegistrationType.ATTENDEE), EventRegistrationStatusReason.Created, WaitingListStatus.Waiting);
                        var registrations = wfActivity.service.RetrieveMultiple(new FetchExpression(fetch));
                        var max = oemsEvent.GetAttributeValue<int?>("oems_registrationmaximum") ?? -1;
                        var i = 0;
                        foreach (var eventRegistration in registrations.Entities)
                        {
                            i++;
                            if (max != -1 && i > max)
                            {
                                continue;
                            }
                            var newEventRegistration = new Entity("oems_eventregistration")
                            {
                                Id = Guid.NewGuid()
                            };
                            newEventRegistration["oems_eventregistrationid"] = newEventRegistration.Id;
                            newEventRegistration["oems_event"] = new EntityReference() { LogicalName = oemsEvent.LogicalName, Id = oemsEvent.Id };
                            newEventRegistration["oems_account"] = CrmTools.getEnitiyAttributeSafely(eventRegistration, "oems_account");
                            newEventRegistration["oems_registrationtype"] = CrmTools.getEnitiyAttributeSafely(eventRegistration, "oems_registrationtype");

                            this.wfActivity.service.Create(newEventRegistration);

                            //Update reg form data
                            newEventRegistration["oems_dynamicregformdata"] = CrmTools.getEnitiyAttributeSafely(eventRegistration, "oems_dynamicregformdata");
                            wfActivity.service.Update(newEventRegistration);

                            //Delete the old registration
                            this.wfActivity.service.Delete(eventRegistration.LogicalName, eventRegistration.Id);
                        }
                    }

                    #endregion

                    this.wfActivity.ResultCode.Set(this.wfActivity.context, 1);
                    this.wfActivity.ResultMessage.Set(this.wfActivity.context, "This event has been copied.");

                    //                    EntityReference entityRef = (EntityReference)oemsEvent.Attributes["oems_subcategory"];
                    //                    this.SubcategoryRef.Set(context, entityRef);
                }
            }
            catch (Exception ex)
            {
                throw ex;
                //TODO: Log Errors Here
            }
        }

        private void setEventComponentOptionAccordingToSubCategory(Entity subCategory, string componentName, Entity oemsEvent, string componentFlagName)
        {
            int component = ((OptionSetValue)subCategory[componentName]).Value;
            switch (component)
            {
                case ComponentIndicatior.NotIncluded:
                    oemsEvent[componentFlagName] = false;
                    break;

                case ComponentIndicatior.Mandatory:
                    oemsEvent[componentFlagName] = true;
                    break;

                case ComponentIndicatior.Optional:
                    //keep the same
                    break;
            }
        }
    }
}

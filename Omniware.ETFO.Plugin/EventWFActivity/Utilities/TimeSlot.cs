﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventWFActivity.Utilities
{
    public class TimeSlot
    {
        public DateTime Start {get; set;}
        public DateTime End { get; set; }
        public string EventName { get; set; }

        public TimeSlot(DateTime _start, DateTime _end){
            Start = _start;
            End = _end;
        }

        public TimeSlot(DateTime _start, DateTime _end,string _eventName)
        {
            Start = _start;
            End = _end;
            EventName = _eventName;
        }

        public bool overlap(TimeSlot slot){
            bool ret = false;

            if ((slot.Start >= this.Start) && (slot.Start < this.End) || (slot.End > this.Start) && (slot.End <= this.End) ||

             (this.Start >= slot.Start) && (this.Start < slot.End) || (this.End > slot.Start) && (this.End <= slot.End))
            {
                ret = true;
            }

            return ret;
        }

        public bool isValid(){
            bool ret = false;
            ret = Start <= End;
            return ret;
        }
    }
}

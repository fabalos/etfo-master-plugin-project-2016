﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;
using Shared.Utilities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventWFActivity.Utilities
{
    public class ValidationTools
    {
    //--------------------- form tools ---------------------------
        public static bool isDateFieldEmpty(Entity entity, string fieldName)
        {
            bool ret = true;

            DateTime value = CrmTools.entityAttributeExists(entity, fieldName) ? (DateTime)entity.Attributes[fieldName] : DateTime.MinValue;

            ret = value == DateTime.MinValue || value == DateTime.MaxValue;

            return ret;
        }
        
        public static bool isStringFieldEmpty(Entity entity, string fieldName)
        {
            bool ret = true;

            string value = CrmTools.entityAttributeExists(entity, fieldName) ? (string)entity.Attributes[fieldName] : null;

            bool bExists = entity.Attributes.Contains(fieldName);

            ret = string.IsNullOrEmpty(value);

            return ret;
        }

        public static bool isLookupFieldEmpty(Entity entity, string fieldName)
        {
            bool ret = true;

            EntityReference reference = CrmTools.entityAttributeExists(entity, fieldName) ? (EntityReference)entity.Attributes[fieldName] : null;

            ret = reference == null;

            return ret;
        }

        public static bool isSingleValueFieldEmpty(Entity entity, string fieldName){
            bool ret = true;

            ret = !CrmTools.entityAttributeExists(entity, fieldName);

            return ret;
        }


    //--------------------- basic tools ---------------------------
        public static bool isYearBeforeCurrentYear(int year){
            bool ret = false;

            ret = DateTime.Now.Year > year;

            return ret;
        }

        public static bool isDateBeforToday(DateTime date){
            bool ret = false;

            DateTime now = DateTime.Now;

    /*        if(date.Year < now.Year){
                ret = true ;
            }else if(date.Month < now.Month){
                ret = true;
            }else if(date.Day < now.Day){
                ret = true;
            }*/

            ret = DateTime.Compare(date, now) < 0; 

            return ret;
        }

        public static bool isDateBeforDate(DateTime date1, DateTime date2)
        {
            bool ret = false;

            ret = date1 < date2;

            return ret;
        }

        public static bool isValidLocation(Entity location)
        {
            bool ret = true;

            string locationName = CrmTools.entityAttributeExists(location, "oems_locationname") ? (string)location.Attributes["oems_locationname"] : null;
            string locationAddressLine1 = CrmTools.entityAttributeExists(location, "oems_addressline1") ? (string)location.Attributes["oems_addressline1"] : null;
            string locationCity = CrmTools.entityAttributeExists(location, "oems_city") ? (string)location.Attributes["oems_city"] : null;
            OptionSetValue locationProvince = CrmTools.entityAttributeExists(location, "oems_province") ? (OptionSetValue)location.Attributes["oems_province"] : null;
            OptionSetValue locationCountry = CrmTools.entityAttributeExists(location, "oems_country") ? (OptionSetValue)location.Attributes["oems_country"] : null;
            string locationPostalCode = CrmTools.entityAttributeExists(location, "oems_postalcode") ? (string)location.Attributes["oems_postalcode"] : null;
            string locationPortalDisplayText = CrmTools.entityAttributeExists(location, "oems_portaldisplaytext") ? (string)location.Attributes["oems_portaldisplaytext"] : null;

            if(string.IsNullOrEmpty(locationName) ||
                string.IsNullOrEmpty(locationAddressLine1) ||
                string.IsNullOrEmpty(locationCity) ||
                locationProvince == null ||
                locationCountry == null ||
                string.IsNullOrEmpty(locationPostalCode) ||
                string.IsNullOrEmpty(locationPortalDisplayText)){
                    ret = false;
                }
            return ret;
        }

        public static bool isRelatedEntitiesEmpty(IOrganizationService service, ColumnSet columns, string entityName, Guid entityId, string relatedEntityName, ConditionExpression condition, string schemaName)
        {
            bool ret = true;

            QueryExpression query = new QueryExpression();
            query.EntityName = relatedEntityName; //"systemuser";
            query.ColumnSet = new ColumnSet(true);//new ColumnSet("systemuserid");
            Relationship relationship = new Relationship();

            query.Criteria = new FilterExpression();
   //         query.Criteria.AddCondition(new ConditionExpression("isdisabled", ConditionOperator.Equal, false));
            query.Criteria.AddCondition(condition);
            // name of relationship between team & systemuser
            relationship.SchemaName = schemaName; //"teammembership_association";
            RelationshipQueryCollection relatedEntity = new RelationshipQueryCollection();
            relatedEntity.Add(relationship, query);
            RetrieveRequest request = new RetrieveRequest();
            request.RelatedEntitiesQuery = relatedEntity;
            request.ColumnSet = columns; //new ColumnSet("teamid");
            request.Target = new EntityReference
            {
                Id = entityId, //teamId
                LogicalName = entityName   //"team"

            };
            RetrieveResponse response = (RetrieveResponse)service.Execute(request);

            if (((DataCollection<Relationship, EntityCollection>)(((RelatedEntityCollection)(response.Entity.RelatedEntities)))).Contains(new Relationship(schemaName)) &&
                ((DataCollection<Relationship, EntityCollection>)(((RelatedEntityCollection)(response.Entity.RelatedEntities))))[new Relationship(schemaName)].Entities.Count > 0)
                ret = true;
            else
                ret = false;

            return ret;
        }
/*
        public static DataCollection<Entity> getRelatedEntities(IOrganizationService service, ColumnSet columns, string entityName, Guid entityId, string relatedEntityName, ConditionExpression condition, string schemaName){
            
            QueryExpression query = new QueryExpression();
            query.EntityName = relatedEntityName; 
            query.ColumnSet = new ColumnSet(true);
            Relationship relationship = new Relationship();

            query.Criteria = new FilterExpression();
            query.Criteria.AddCondition(condition);

            relationship.SchemaName = schemaName; 
            RelationshipQueryCollection relatedEntity = new RelationshipQueryCollection();
            relatedEntity.Add(relationship, query);
            RetrieveRequest request = new RetrieveRequest();
            request.RelatedEntitiesQuery = relatedEntity;
            request.ColumnSet = columns; 
            request.Target = new EntityReference
            {
                Id = entityId, 
                LogicalName = entityName   

            };
            RetrieveResponse response = (RetrieveResponse)service.Execute(request);

            if (((DataCollection<Relationship, EntityCollection>)(((RelatedEntityCollection)(response.Entity.RelatedEntities)))).Contains(new Relationship(schemaName)) &&
                ((DataCollection<Relationship, EntityCollection>)(((RelatedEntityCollection)(response.Entity.RelatedEntities))))[new Relationship(schemaName)].Entities.Count > 0)
                return ((DataCollection<Relationship, EntityCollection>)(((RelatedEntityCollection)(response.Entity.RelatedEntities))))[new Relationship(schemaName)].Entities;
            else
                return null;
        }
*/
        public static bool hasDuplicateTimeSlot(ArrayList timeSlots,bool caucus =false)
        {
            bool ret = false;

         //   object[] timeSlots = timeSlotList.ToArray();

            for(int i=0;i<timeSlots.Count;i++){
                TimeSlot slot1 = (TimeSlot)timeSlots[i];
                
                if(slot1.isValid()){
                    for (int j = i + 1; j < timeSlots.Count; j++)
                    {
                        TimeSlot slot2 = (TimeSlot)timeSlots[j];
                        if(slot2.isValid()){
                            if (slot1.overlap(slot2))
                            {
                                if (caucus) 
                                {
                                    if (slot1.EventName == slot2.EventName)
                                        return true;
                                    else
                                        return false;
                                }
                                return true;
                            }
                        }
                    }
                }
            }

            return ret;
        }

        public static bool hasDuplicateEntityReference(ArrayList efList)
        {
            bool ret = false;

            //   object[] timeSlots = timeSlotList.ToArray();

            for (int i = 0; i < efList.Count; i++)
            {
                EntityReference e1 = (EntityReference)efList[i];

                for (int j = i + 1; j < efList.Count; j++)
                {
                    EntityReference e2 = (EntityReference)efList[j];
                    if (isSameEntity(e1,e2))
                    {
                        return true;
                    }
                }
            }

            return ret;
        }

        public static bool isSameEntity(EntityReference e1, EntityReference e2){
            return (e1.Id.Equals(e2.Id)) && (e1.LogicalName.Equals(e2.LogicalName));
        }
    }
}

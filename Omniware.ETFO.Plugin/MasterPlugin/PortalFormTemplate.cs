﻿using System;
using System.Diagnostics;
using System.IO;
using System.Collections;
using System.Xml.Serialization;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Omniware.ETFO.Plugin.Shared;
using Xrm;

using System.Web.Script.Serialization;
using Microsoft.Xrm.Client.Collections;
using Microsoft.Crm.Sdk.Messages;
using Newtonsoft.Json;
using log4net;

namespace Omniware.ETFO.Plugin
{

    public class PortalFormTemplate : Plugin
    {

        ILog Log = LogHelper.GetLogger(typeof(PortalFormTemplate));


        public PortalFormTemplate()
            : base(typeof(PortalFormTemplate))
        {
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "Retrieve", "oems_portalformtemplate", new Action<LocalPluginContext>(RetrieveFormTemplate)));
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "Update", "oems_portalformtemplate", new Action<LocalPluginContext>(UpdateFormTemplate)));
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "Create", "oems_portalformtemplate", new Action<LocalPluginContext>(CreateFormTemplate)));
        }

        protected void CreateFormTemplate(LocalPluginContext localContext)
        {
            bool nullinfo = false;

            if (localContext == null)
            {
                throw new ArgumentNullException("localContext");
            }

            // stop recurrsive calls
            if (localContext.PluginExecutionContext.Depth > 2)
                return;

            //Entity oemsPortalFormTemplate = new Entity("oems_portalformtemplate");
            //oemsPortalFormTemplate.Id = localContext.Entity.Id;

            var sw = new Stopwatch(); sw.Start();

            oems_portalformtemplate oemsPortalFormTemplate = localContext.Entity.ToEntity<oems_portalformtemplate>();

            PortalFormTemplateConfigurator formConfigurator = new PortalFormTemplateConfigurator(localContext, oemsPortalFormTemplate.Id);

            String formConfiguratorData = null;

            if (oemsPortalFormTemplate.oems_SourceFormTemplate != null)
            {
                formConfiguratorData = formConfigurator.GetFormTemplateData();
            }
            else if (oemsPortalFormTemplate.oems_CopiedFormTemplate != null)
            {
                formConfiguratorData = formConfigurator.CopyFormTemplate(ref oemsPortalFormTemplate);
            }

            oemsPortalFormTemplate["oems_retrievedata"] = formConfiguratorData;
            oemsPortalFormTemplate["oems_updatedata"] = formConfiguratorData;

            // generate form elements
            if (oemsPortalFormTemplate["oems_updatedata"] != null && oemsPortalFormTemplate["oems_updatedata"] != string.Empty)
            {
                formConfigurator.UpdateFormTemplate(oemsPortalFormTemplate);
                nullinfo = true;
            }
            localContext.OmniContext.SaveChanges();

            // generate form data
            if (nullinfo)
            {
                PortalFormTemplateGenerator formGenerator = new PortalFormTemplateGenerator();
                String formData = formGenerator.GetFormData(localContext, oemsPortalFormTemplate.Id);
                oemsPortalFormTemplate["oems_formdata"] = formData;
            }

            localContext.OrganizationService.Update(oemsPortalFormTemplate);

            sw.Stop();
            Log.InfoFormat("CreateFormTemplate({0}) {1} msec ", oemsPortalFormTemplate.oems_name, sw.ElapsedMilliseconds);
        }

        protected void UpdateFormTemplate(LocalPluginContext localContext)
        {
            if (localContext == null)
                throw new ArgumentNullException("localContext");

            // stop recurrsive calls
            if (localContext.PluginExecutionContext.Depth != 1)
                return;

            var sw = new Stopwatch(); sw.Start();
            oems_portalformtemplate preImageOemsPortalFormTemplate = localContext.PluginExecutionContext.PreEntityImages["PreImage"].ToEntity<oems_portalformtemplate>();
            oems_portalformtemplate oemsPortalFormTemplate = localContext.Entity.ToEntity<oems_portalformtemplate>();

            if (oemsPortalFormTemplate.GetAttributeValue<bool>("oems_regenerate") ||
                 (
                    oemsPortalFormTemplate.GetAttributeValue<string>("oems_updatedata") != null &&
                    preImageOemsPortalFormTemplate.GetAttributeValue<string>("oems_updatedata") != oemsPortalFormTemplate.GetAttributeValue<string>("oems_updatedata")
                 )
               )
            {

                if (oemsPortalFormTemplate.GetAttributeValue<string>("oems_updatedata") != null)
                {
                    oemsPortalFormTemplate["oems_retrievedata"] =
                        oemsPortalFormTemplate.GetAttributeValue<string>("oems_updatedata");

                    Log.InfoFormat("Updating  elements of template '{0}' ", oemsPortalFormTemplate.Id);
                    PortalFormTemplateConfigurator formConfigurator = new PortalFormTemplateConfigurator(localContext, oemsPortalFormTemplate.Id);
                    formConfigurator.UpdateFormTemplate(oemsPortalFormTemplate);
                    localContext.OmniContext.SaveChanges();
                }
                else
                {
                    Log.Warn("oems_updatedata == null");
                }

                // update form data
                Log.InfoFormat("Regenerating form json for template '{0}' ", oemsPortalFormTemplate.Id);
                PortalFormTemplateGenerator formGenerator = new PortalFormTemplateGenerator();
                String jsonData = formGenerator.GetFormData(localContext, oemsPortalFormTemplate.Id);
                oemsPortalFormTemplate["oems_formdata"] = jsonData;
                oemsPortalFormTemplate["oems_regenerate"] = false;
                localContext.OrganizationService.Update(oemsPortalFormTemplate);
            }

            sw.Stop();
            //            Log.Debug("oems_updatedata empty ?" + String.IsNullOrEmpty(oemsPortalFormTemplate.GetAttributeValue<string>("oems_updatedata")));
            //            Log.Debug("oems_retrievedata empty ?" + String.IsNullOrEmpty(oemsPortalFormTemplate.GetAttributeValue<string>("oems_retrievedata")));
            Log.Debug("oems_retrievedata == oems_updatedata : " + (oemsPortalFormTemplate.GetAttributeValue<string>("oems_retrievedata") == oemsPortalFormTemplate.GetAttributeValue<string>("oems_updatedata")));

            Log.InfoFormat("UpdateFormTemplate({0}) {1} msec ", oemsPortalFormTemplate.oems_name, sw.ElapsedMilliseconds);

        }


        protected void RetrieveFormTemplate(LocalPluginContext localContext)
        {
            if (localContext == null)
            {
                throw new ArgumentNullException("localContext");
            }

            // stop recurrsive calls
            if (localContext.PluginExecutionContext.Depth != 1)
                return;

            var sw = new Stopwatch(); sw.Start();
            oems_portalformtemplate oemsPortalFormTemplate = ((Entity)localContext.PluginExecutionContext.OutputParameters["BusinessEntity"]).ToEntity<oems_portalformtemplate>();
            PortalFormTemplateConfigurator formConfigurator = new PortalFormTemplateConfigurator(localContext, oemsPortalFormTemplate.Id);

            var formTemplateData = formConfigurator.GetFormTemplateData();
            oemsPortalFormTemplate["oems_retrievedata"] = formTemplateData;
            // keep oems_updatedata in sync even if regeneration not requested
            oemsPortalFormTemplate["oems_updatedata"] = formTemplateData;

            //Log.Debug("oems_updatedata empty ?" +  String.IsNullOrEmpty( oemsPortalFormTemplate.GetAttributeValue<string>("oems_updatedata")) );
            //Log.Debug("oems_retrievedata empty ?" + String.IsNullOrEmpty(oemsPortalFormTemplate.GetAttributeValue<string>("oems_retrievedata")));
            Log.Debug("oems_retrievedata == oems_updatedata : " + (oemsPortalFormTemplate.GetAttributeValue<string>("oems_retrievedata") == oemsPortalFormTemplate.GetAttributeValue<string>("oems_updatedata")));
            sw.Stop();
            Log.InfoFormat("RetrieveFormTemplate({0}) {1} msec", oemsPortalFormTemplate.oems_name, sw.ElapsedMilliseconds);

        }

    }
}

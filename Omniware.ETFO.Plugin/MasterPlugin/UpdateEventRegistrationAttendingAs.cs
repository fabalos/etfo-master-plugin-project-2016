﻿using System;
using System.IO;
using System.Collections;
using System.Xml.Serialization;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Xrm;
using Microsoft.ApplicationBlocks.Data;
using System.Web.Script.Serialization;
using Microsoft.Xrm.Client.Collections;
using Microsoft.Crm.Sdk.Messages;
using Newtonsoft.Json;

namespace Omniware.ETFO.Plugin
{
    public class UpdateEventRegistrationAttendingAs : Plugin
    {

        public UpdateEventRegistrationAttendingAs()
            : base(typeof(UpdateEventRegistrationAttendingAs))
        {
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "Update", "oems_eventregistration", new Action<LocalPluginContext>(UpdateAttendingAs)));
        }

        private void associateAttendingAs(LocalPluginContext localContext, EntityReference oemsEventRegistration, AttendingAsOption attendingAsOption)
        {
            AssociateEntitiesRequest associateRequest = new AssociateEntitiesRequest
            {
                Moniker1 = oemsEventRegistration,
                Moniker2 = new EntityReference(oems_attendingasoption.EntityLogicalName, attendingAsOption.id),
                RelationshipName = "oems_eventregistration_attending_as",
            };

            string relationshipDirection = (attendingAsOption.isVotingRole.Value) ? "oems_eventregistration_attending_as_voting" : "oems_eventregistration_attending_as_nonvotng";


            AssociateEntitiesRequest associateRequestRegistrationRelationship = new AssociateEntitiesRequest
            {
                Moniker1 = oemsEventRegistration,
                Moniker2 = new EntityReference(oems_attendingasoption.EntityLogicalName, attendingAsOption.id),
                RelationshipName = relationshipDirection,
            };

            try
            {
                localContext.OmniContext.Execute(associateRequest);
                localContext.OmniContext.Execute(associateRequestRegistrationRelationship);
            }
            catch (Exception e)
            {
                var error = e.ToString();// nothing happens
            }
        }

        private void disassociateAttendingAs(LocalPluginContext localContext, EntityReference oemsEventRegistration, AttendingAsOption attendingAsOption)
        {
            DisassociateEntitiesRequest disassociateRequest = new DisassociateEntitiesRequest
            {
                Moniker1 = oemsEventRegistration,
                Moniker2 = new EntityReference(oems_attendingasoption.EntityLogicalName,attendingAsOption.id),
                RelationshipName = "oems_eventregistration_attending_as",
            };

            string relationshipDirection = (attendingAsOption.isVotingRole.Value) ? "oems_eventregistration_attending_as_voting" : "oems_eventregistration_attending_as_nonvotng";

            DisassociateEntitiesRequest disassociateRequestRegistrationRelationship = new DisassociateEntitiesRequest
            {
                Moniker1 = oemsEventRegistration,
                Moniker2 = new EntityReference(oems_attendingasoption.EntityLogicalName, attendingAsOption.id),
                RelationshipName = relationshipDirection,
            };

            try
            {
                localContext.OmniContext.Execute(disassociateRequest);
                localContext.OmniContext.Execute(disassociateRequestRegistrationRelationship);
            }
            catch (Exception e)
            {
                var error = e.ToString();// nothing happens
            }
        }

        private void processAttendingAs(LocalPluginContext localContext, AttendingAs attendingAs, ref oems_EventRegistration eventRegistration)
        {

            // loop through attending as configuration list
            // find where isSelected = true and selectedId = Guid.Empty --- Add
            // find where isSelected = false and selectedId = <some GUID> --- Delete

            foreach (var attendingAsOption in attendingAs.attendingAsList)
            {
                // NEW
                if (attendingAsOption.isSelected == true && attendingAsOption.selected == null)
                {
                    //new EntityReference(oems_attendingasoption.EntityLogicalName.ToLower(), attendingAsOption.id)
                    associateAttendingAs(localContext, eventRegistration.ToEntityReference(), attendingAsOption);

                    if (attendingAsOption.isOther == true)
                        eventRegistration.oems_AttendingAsOther = attendingAsOption.otherInput;
                }
                // DELETE
                else if (!attendingAsOption.isSelected == true && attendingAsOption.selected != null)
                {
                    disassociateAttendingAs(localContext, eventRegistration.ToEntityReference(), attendingAsOption);
                }
            }
        }

        protected void UpdateAttendingAs(LocalPluginContext localContext)
        {
            if (localContext == null)
                throw new ArgumentNullException("localContext");

            // stop recurrsive calls
            if (localContext.PluginExecutionContext.Depth != 1)
                return;

            // get Event Registration
            oems_EventRegistration oemsEventRegistration = localContext.Entity.ToEntity<oems_EventRegistration>();

            // get event
            oems_Event oemsEvent = oemsEventRegistration.oems_event_to_eventregistration;

            AttendingAs attendingAs = JsonConvert.DeserializeObject<AttendingAs>(oemsEventRegistration.oems_UpdateAttendingAsData);

            // save personal accommodation
            processAttendingAs(localContext, attendingAs, ref oemsEventRegistration);
        }
    }
}

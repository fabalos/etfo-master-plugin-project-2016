﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Omniware.ETFO.Plugin
{
    public class PortalForm
    {
        public string FormName { get; set; }
        public string FormId { get; set; }
        public PageElement[] Children { get; set; }
        
    }

    public class PageElement
    {
        public string Id { get; set; }

        public string FontFamily { get; set; }
        public string FontSize { get; set; }
        public int? FontWeight { get; set; }
        public string Color { get; set; }
        public string FieldFormat { get; set; }
        public bool? Mandatory { get; set; }
        public bool? MandatoryValue { get; set; }
        public bool? MultipleRows { get; set; }

        public string Value { get; set; }

        public string Label { get; set; }
        public int? DisplaySequence { get; set; }
        public bool? IndentChildren { get; set; }
        
        public int? Layout { get; set; }
        public bool? ReadOnly { get; set; }


        public string RuleData { get; set; }

        public int? ElementType { get; set; }
        public int? FieldType { get; set; }

        public PageElement[] Children { get; set; }

        public string DefaultValue { get; set; }
        public int? MaxLen { get; set; }
        public int? MaxNumericValue { get; set; }
        public int? MinNumericValue { get; set; }
        
        public string[] OptionLabels;
        public string[] OptionValues;

        

    }


}

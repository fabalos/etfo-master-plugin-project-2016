﻿using System;
using System.Diagnostics;
using System.Linq;
using Microsoft.Xrm.Sdk;
using Omniware.ETFO.Plugin.Shared;
using Xrm;
using log4net;
using Shared.Utilities;
using System.Collections.Generic;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Messages;

namespace Omniware.ETFO.Plugin
{
    public class EventChildCareSchedule : Plugin
    {
        protected string Config;
        protected string SecConfig;
        private ILog _log = LogHelper.GetLogger(typeof(EventChildCareSchedule));

        public EventChildCareSchedule(string unsecureConfig, string secureConfig)
            : base(typeof(BillingRequest))
        {
            if (unsecureConfig != null)
            {
                Config = unsecureConfig;
            }

            if (secureConfig != null)
            {
                SecConfig = secureConfig;
            }

            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(20, "Create", "oems_eventchildcareschedule", new Action<LocalPluginContext>(CheckOverlapping)));
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "RetrieveMultiple", "oems_eventchildcareschedule", new Action<LocalPluginContext>(CountChildrenAssigned)));
        }

        protected void CountChildrenAssigned(LocalPluginContext localContext)
        {
            if (localContext == null)
                throw new ArgumentNullException("localContext");

            //Stop recurrsive calls
            if (localContext.PluginExecutionContext.Depth != 1)
                return;

            if (localContext.PluginExecutionContext.InputParameters.Contains("Query"))
            {
                QueryExpression query = (QueryExpression)localContext.PluginExecutionContext.InputParameters["Query"];

                if (query.ColumnSet.Columns.Contains("oems_childrenassigned"))
                {
                    if (localContext.PluginExecutionContext.OutputParameters.Contains("BusinessEntityCollection"))
                    {
                        EntityCollection collection = (EntityCollection)localContext.PluginExecutionContext.OutputParameters["BusinessEntityCollection"];
                        string[] columns = { "oems_childcarerequestid" };

                        foreach (var entity in collection.Entities)
                        {
                            DataCollection<Entity> result = GetRelatedEntitDataFromManyToManyRelation("oems_childcarerequest", columns, "oems_eventchildcareschedule_oems_childcare", entity.ToEntityReference(), localContext.OmniContext);
                            entity.Attributes["oems_childrenassigned"] = result == null ? 0 : result.Count();
                            localContext.OmniContext.Attach(entity);
                            localContext.OmniContext.UpdateObject(entity);
                        }
                    }
                }
            }
        }

        protected void CheckOverlapping(LocalPluginContext localContext)
        {
            StringManager stringManager = new StringManager(localContext.OrganizationService);

            if (localContext == null)
                throw new ArgumentNullException("localContext");

            //Stop recurrsive calls
            if (localContext.PluginExecutionContext.Depth != 1)
                return;

            Entity schedule = localContext.Entity;

            //List<Entity> childCareSchedules = (from c in localContext.OmniContext.CreateQuery("oems_eventchildcareschedule")
            //                             where ((EntityReference)c["oems_event"]).Id == ((EntityReference)schedule.Attributes["oems_event"]).Id
            //                              select c).ToList<Entity>();

            //DateTime newStartDate = schedule.GetAttributeValue<DateTime>("oems_startdatetime");
            //DateTime newEndDate = schedule.GetAttributeValue<DateTime>("oems_enddatetime");

            //foreach (var sch in childCareSchedules)
            //{
            //    DateTime existingStartDate = sch.GetAttributeValue<DateTime>("oems_startdatetime");
            //    DateTime existingEndtDate = sch.GetAttributeValue<DateTime>("oems_enddatetime");

            //    if ((newStartDate >= existingStartDate && newStartDate <= existingEndtDate) ||
            //        (newEndDate >= existingStartDate && newStartDate <= existingEndtDate))
            //    {
            //        string validationMessage = stringManager.getString(StringCodes.CHILD_CARE_OVERLAPPING_VALIDATION_MESSAGE, "", "You cannot create a schedule that overlaps an existing schedule. Please choose another start/end time.");
            //        throw new InvalidPluginExecutionException(validationMessage);
            //    }
            //}
        }

        public static DataCollection<Entity> GetRelatedEntitDataFromManyToManyRelation(string entityToRetrieve, string[] columnsToRetieve, string relationName, EntityReference targetEntity, IOrganizationService service)
        {
            DataCollection<Entity> result = null;

            QueryExpression query = new QueryExpression();
            query.EntityName = entityToRetrieve;
            query.ColumnSet = new ColumnSet(columnsToRetieve);
            Relationship relationship = new Relationship();

            relationship.SchemaName = relationName;

            RelationshipQueryCollection relatedEntity = new RelationshipQueryCollection();
            relatedEntity.Add(relationship, query);
            RetrieveRequest request = new RetrieveRequest();
            request.RelatedEntitiesQuery = relatedEntity;
            //request.ColumnSet = new ColumnSet(targetEntity.LogicalName + "id");
            request.ColumnSet = new ColumnSet(true);
            request.Target = targetEntity;
            RetrieveResponse response = (RetrieveResponse)service.Execute(request);


            if (((DataCollection<Relationship, EntityCollection>)(((RelatedEntityCollection)(response.Entity.RelatedEntities)))).Contains(new Relationship(relationName)) && ((DataCollection<Relationship, EntityCollection>)(((RelatedEntityCollection)(response.Entity.RelatedEntities))))[new Relationship(relationName)].Entities.Count > 0)
            {
                result = ((DataCollection<Relationship, EntityCollection>)(((RelatedEntityCollection)(response.Entity.RelatedEntities))))[new Relationship(relationName)].Entities;
            }

            return result;
        }
    }
}

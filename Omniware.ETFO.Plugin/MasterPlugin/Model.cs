﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using System.ComponentModel;
using Microsoft.Xrm.Sdk;

namespace Omniware.ETFO.Plugin
{
    public class Element
    {
        public Guid id { get; set; }
        public Guid? elementId { get; set; }
        public Guid? sourceId { get; set; }
        public String text { get; set; }
        public Boolean @checked { get; set; }
        public int index { get; set; }
        public Boolean expanded { get; set; }
        public String spriteCssClass { get; set; }
        public Boolean mandatory { get; set; }
        public List<Dictionary<string, object>> properties { get; set; }
        public List<Element> items { get; set; }     
    }

    public class Template
    {
        public Guid id { get; set; }
        public Guid sourceId { get; set; }
    }

    public class PropertyConfiguration
    {
        public Guid id { get; set; }
        public Guid optionId { get; set; }
        public String text { get; set; }
        public Boolean @checked { get; set; }
        public Boolean hasChildren { get; set; }
        public Boolean expanded { get; set; }
        public List<PropertyConfiguration> items { get; set; }
    }

    public class EventInformation
    {
        public Boolean showSection { get; set; }

        public Boolean showEventName { get; set; }
        public String eventName { get; set; }

        public Boolean showEventDate { get; set; }
        public String eventDate { get; set; }

        public Boolean showEventLocation { get; set; }
        public String eventLocation { get; set; }

        public Boolean showAcommodationtLocation { get; set; }
        public String accommodationtLocation { get; set; }

        public Boolean showRegistrationDeadline { get; set; }
        public String registrationDeadline { get; set; }

        public Boolean showEventAdminUserList { get; set; }
        public List<EventAdminUser> eventAdminUserList { get; set; }

        public Boolean showEventExecutiveUserList { get; set; }
        public List<EventAdminUser> eventExecutiveUserList { get; set; }

        public String portalBillingDescription { get; set; }

        public int? eventStatus { get; set; }

        public String applicationDeadLine { get; set; }

        public String courseCode { get; set; }

        public String coursePresenters { get; set; }

        public String courseTopic { get; set; }

        public String courseLocation { get; set; }

        public String courseTargetGrades { get; set; }

        public String courseInitialOferedDate { get; set; }

        public String courseFinalOferedDate { get; set; }

        public String courseInitialDailyTime { get; set; }

        public String courseFinalDailyTime { get; set; }

        public String courseDescription { get; set; }
       
    }

    public class EventAdminUser
    {
        public String fullName { get; set; }
        public String emailAddress { get; set; }
        public String telephoneNumber { get; set; }
        public String faxeNumber { get; set; }
    }

    public class EventRegistrationForm
    {
        // general drop downs
        // form fields
        public String registrationNumber { get; set; }
        public Option registrationStatus { get; set; }
        public EventInformation eventInformation { get; set; }
        public PersonalInformation personalInformation { get; set; }
        public SelfIdentification selfIdentification { get; set; }
        public AttendingAs attendingAs { get; set; }
        public AccommodationRequest accommodationRequest { get; set; }
        public SingleRoomMedicalAccommodationRequest singleRoomMedicalAccommodationRequest { get; set; }
        public PersonalAccommodationRequest personalAccommodationRequest { get; set; }
        public PersonalCareExemptionRequest personalCareExemptionRequest { get; set; }
        public Caucus caucus { get; set; }
        public ReleaseTimeRequest releaseTimeRequest { get; set; }
        public ChildCare childCare { get; set; }
        public TicketRequest ticketRequest { get; set; }
    }

    public class PersonalInformation
    {
        public Boolean showSection { get; set; }

        // drop down field values
        public List<Lookup> localList { get; set; }
        public List<Lookup> teacherTypeList { get; set; }

        // form fields
        public Boolean showFirstName { get; set; }
        public String firstName { get; set; }

        public Boolean showLastName { get; set; }
        public String lastName { get; set; }

        public Boolean showName { get; set; }
        public String name { get; set; }

        public Boolean isMember { get; set; }

        public Boolean showETFOMemberId { get; set; }
        public String ETFOMemberId { get; set; }

        public Boolean showLocal { get; set; }
        public Lookup local { get; set; }

        public Boolean showTeacherType { get; set; }
        public Lookup teachType { get; set; }

        public Boolean showAddressGroup { get; set; }

        public Boolean showAddressLine1 { get; set; }
        public String addressLine1 { get; set; }

        public Boolean showAddressLinee { get; set; }
        public String addressLine2 { get; set; }

        public Boolean showCity { get; set; }
        public String city { get; set; }

        public Boolean showProvinceState { get; set; }
        public String provinceState { get; set; }

        public Boolean showPostalCodeZip { get; set; }
        public String postalCodeZip { get; set; }

        public Boolean showCountry { get; set; }
        public String country { get; set; }

        public Boolean showContactGroup { get; set; }

        public Boolean showHomePhone { get; set; }
        public String homePhone { get; set; }

        public Boolean showWorkPhone { get; set; }
        public String workPhone { get; set; }

        public Boolean showCellPhone { get; set; }
        public String cellPhone { get; set; }

        public Boolean showFax { get; set; }
        public String fax { get; set; }

        public Boolean showEmailAddress { get; set; }
        public String emailAddress { get; set; }
    }

    public class CourseListing
    {
        public Guid TermId { get; set; }
        public string TermName { get; set; }
        public string Topic { get; set; }
        public DateTime? InitialOferedDate { get; set; }
        public DateTime? FinalOferedDate { get; set; }
        public string CourseCode { get; set; }
        public string TargetGrades { get; set; }
        public string InitialDailyTime { get; set; }
        public string FinalDailyTime { get; set; }
        public string CourseDescription { get; set; }
        public List<string> CoursePrices { get; set; }
        public string Instructor { get; set; }

        public string Location { get; set; }

        public int? EventStatus { get; set; }
    }

    public class AttendingAs
    {
        public Boolean showSection { get; set; }

        public Boolean showSectionMessage { get; set; }
        public String sectionMessage { get; set; }

        public Boolean showAttendingAsComponent { get; set; }
        public List<AttendingAsOption> attendingAsList { get; set; }
    }

    public class SelfIdentification
    {
        public Boolean showSection { get; set; }

        public Boolean showSectionMessage { get; set; }
        public String sectionMessage { get; set; }

        public Boolean showIsAboriginal { get; set; }
        [DefaultValue(false)]
        public Boolean? isAboriginal { get; set; }

        public Boolean showIsDisabled { get; set; }
        [DefaultValue(false)]
        public Boolean? isDisabled { get; set; }

        public Boolean showIsRacialMinority { get; set; }
        [DefaultValue(false)]
        public Boolean? isRacialMinority { get; set; }

        public Boolean showIsLesbianGayOther { get; set; }
        [DefaultValue(false)]
        public Boolean? isLesbianGayOther { get; set; }

        public Boolean showIsWoman { get; set; }
        [DefaultValue(false)]
        public Boolean? isWoman { get; set; }
    }

    public class AccommodationRequest
    {
        public Boolean showSection { get; set; }
        // drop down field values
        public List<Lookup> roomTypeList { get; set; }

        public Boolean showAccommodationScheduleComponent { get; set; }
        public List<AccommodationSchedule> accommodationScheduleList { get; set; }

        public Boolean showSingleRoomMessage { get; set; }
        public String singleRoomMessage { get; set; }

        // form field
        public Guid accommodationRequestId { get; set; }
        public String requestId { get; set; }

        public Boolean showIsRequiredGroup { get; set; }
        [DefaultValue(false)]
        public Boolean? isRequired { get; set; }

        public Boolean showRoomType { get; set; }
        public Lookup roomType { get; set; }

        public Boolean showIsSmokingRoomRequired { get; set; }
        [DefaultValue(false)]
        public Boolean? isSmokingRoomRequired { get; set; }

        public Boolean showRoomateComponent { get; set; }
        [DefaultValue(false)]
        public Boolean? isRoomateRequired { get; set; }
        [DefaultValue(false)]
        public Boolean? isRoomateETFOAssigned { get; set; }
        public String shareWithName { get; set; }

        public Boolean showRequestStatus { get; set; }
        public Option requestStatus { get; set; }

        public Boolean showRequestNotes { get; set; }
        public String requestNotes { get; set; }
    }

    public class SingleRoomMedicalAccommodationRequest
    {
        public Boolean showSingleRoomMedicalAccommodationGroup { get; set; }

        public Guid singleRoomMedicalAccommodationRequestId { get; set; }
        public String requestId { get; set; }

        public Boolean showSectionMessage { get; set; }
        public String sectionMessage { get; set; }

        public Boolean showIsRequiredGroup { get; set; }
        [DefaultValue(false)]
        public Boolean? isRequired { get; set; }

        public Boolean showReason { get; set; }
        public String reason { get; set; }

        public Boolean showMedicalNoteFile { get; set; }
        public String medicalNoteFile { get; set; }

        public Boolean showRequestStatus { get; set; }
        public Option requestStatus { get; set; }

        public Boolean showRequestNotes { get; set; }
        public String requestNotes { get; set; }
    }


    public class AccommodationSchedule
    {
        [DefaultValue(false)]
        public Boolean? isSelected { get; set; }
        public Guid id { get; set; }
        public EntityReference selected { get; set; }
        public String accommodationDate { get; set; }
    }

    public class PersonalAccommodationRequest
    {
        public Boolean showSection { get; set; }
        public List<Option> requestStatusList { get; set; }

        public Boolean showIsRequiredGroup { get; set; }
        [DefaultValue(false)]
        public Boolean? isRequired { get; set; }

        public Boolean showRequestStatus { get; set; }
        public Option requestStatus { get; set; }

        public Boolean showRequestNotes { get; set; }
        public String requestNotes { get; set; }

        public Boolean showPersonalAccommodationComponent { get; set; }
        public List<PersonalAccommodation> personalAccommodationList { get; set; }
    }

    public class PersonalAccommodation
    {
        [DefaultValue(false)]
        public Boolean? isSelected { get; set; }
        public Guid id { get; set; }
        public Guid selectedId { get; set; }
        public String name { get; set; }
        public String description { get; set; }
        public Option optionType { get; set; }
        [DefaultValue(false)]
        public Boolean? isEventSelected { get; set; }
        [DefaultValue(false)]
        public Boolean? isHotelSelected { get; set; }
        [DefaultValue(false)]
        public Boolean? showUserSpecifiedDetails { get; set; }
        public String userSpecifiedDetails { get; set; }
        public Option requestStatus { get; set; }
        public String requestNotes { get; set; }
        public int level { get; set; }
        public Guid? parent { get; set; }
        public bool isLeaf { get; set; }
        public bool expanded { get; set; }
        public bool loaded { get; set; }
    }

    public class PersonalCareExemptionRequest
    {
        public Boolean showPersonalCareExemptionGroup { get; set; }

        public Guid personalCareExemptionRequestId { get; set; }
        public String requestId { get; set; }

        public Boolean showSectionMessage { get; set; }
        public String sectionMessage { get; set; }

        public Boolean showIsRequiredGroup { get; set; }
        [DefaultValue(false)]
        public Boolean? isRequired { get; set; }

        public Boolean showSpeecialNeeds { get; set; }
        public String speecialNeeds { get; set; }

        public Boolean showEstmatedCosts { get; set; }
        public String estmatedCosts { get; set; }

        public Boolean showDailyAllowance { get; set; }
        public Decimal? dailyAllowance { get; set; }

        public Boolean showValidUntil { get; set; }
        public String validUntil { get; set; }

        public Boolean showAuthorizedBy { get; set; }
        public String authorizedBy { get; set; }

        public Boolean showAuthorizationDate { get; set; }
        public String authorizationDate { get; set; }

        public Boolean showRequestStatus { get; set; }
        public Option requestStatus { get; set; }

        public Boolean showRequestNotes { get; set; }
        public String requestNotes { get; set; }
    }

    public class AttendingAsConfiguration
    {
        public List<AttendingAsOption> attendingAsList { get; set; }
    }

    public class AttendingAsOption
    {
        [DefaultValue(false)]
        public Boolean? isSelected { get; set; }
        public Guid id { get; set; }
        public EntityReference selected { get; set; }
        public String name { get; set; }
        [DefaultValue(false)]
        public Boolean? isVotingRole { get; set; }
        [DefaultValue(false)]
        public Boolean? isGuestRole { get; set; }
        [DefaultValue(false)]
        public Boolean? isOther { get; set; }
        public String otherInput { get; set; }
        public String colour { get; set; }
        public int? displaySequence { get; set; } 
    }

    public class Caucus
    {
        public Boolean showSection { get; set; }

        public Boolean showName { get; set; }
        public Boolean showDescription { get; set; }
        public Boolean showTimePeriod { get; set; }

        public List<CaucusSchedule> caucusScheduleList { get; set; }
    }

    public class CaucusSchedule
    {
        public String date { get; set; }
        public List<CaucusSession> caucusSessionList { get; set; }
    }

    public class CaucusSession
    {
        [DefaultValue(false)]
        public Boolean? isSelected { get; set; }
        public Guid id { get; set; }
        public Guid selectedId { get; set; }
        public String name { get; set; }
        public String description { get; set; }
        public String timePeriod { get; set; }
        public DateTime startDateTime { get; set; }
        public DateTime endDateTime { get; set; }
    }

    public class ChildCare
    {
        public Boolean showSection { get; set; }
        public int minChildAge { get; set; }
        public int maxChildAge { get; set; }

        public Boolean showName { get; set; }
        public Boolean showAge { get; set; }
        public Boolean showChildCareScheduleComponent { get; set; }

        public List<ChildCareSchedule> childCareScheduleList { get; set; }
        public List<ChildCareRequest> childCareRequestList { get; set; }
    }

    public class ChildCareRequest
    {
        public Guid? id { get; set; }
        public String name { get; set; }
        public int? age { get; set; }
        public List<ChildCareSchedule> childCareScheduleList { get; set; }
    }

    public class ChildCareSchedule
    {
        public String date { get; set; }
        public List<ChildCareScheduleOption> childCareScheduleOptionList { get; set; }
    }

    public class ChildCareScheduleOption
    {
        [DefaultValue(false)]
        public Boolean? isSelected { get; set; }
        public Guid id { get; set; }
        public EntityReference selected { get; set; }
        public String timePeriod { get; set; }
        public DateTime startDateTime { get; set; }
        public DateTime endDateTime { get; set; }
    }

    public class ReleaseTimeRequest
    {
        public Boolean showSection { get; set; }

        // grid columns
        public List<Option> releaseTimePeriodList { get; set; }

        public Boolean showIsRequiredGroup { get; set; }
        [DefaultValue(false)]
        public Boolean? isRequired { get; set; }

        public Boolean showSchoolBoardLocalComponent { get; set; }
        public List<SchoolBoardLocal> schoolBoardLocalList { get; set; }

        public Boolean showSchoolCity { get; set; }
        public Boolean showSchoolPhone { get; set; }
        public Boolean showSchoolFax { get; set; }
        public Boolean showSchoolPrincipalName { get; set; }

        public Boolean showReleaseTimeScheduleComponent { get; set; }
        public List<ReleaseTimeSchedule> releaseTimeScheduleList { get; set; }

        public Boolean showExtraReleaseTimeGroup { get; set; }
        [DefaultValue(false)]
        public Boolean? isExtraReleaseTimeRequired { get; set; }

        public Boolean showExtraReleaseTimeComponent { get; set; }
        public List<ExtraReleaseTimeSchedule> extraReleaseTimeScheduleList { get; set; }

        public Boolean showRequestStatus { get; set; }
        public Option requestStatus { get; set; }

        public Boolean showRequestNotes { get; set; }
        public String requestNotes { get; set; }
    }

    public class ReleaseTimeSchedule
    {
        [DefaultValue(false)]
        public Boolean? isSelected { get; set; }
        public Guid id { get; set; }
        public Guid selectedId { get; set; }
        public String date { get; set; }
        public int? selectedPeriod { get; set; }
    }

    public class ExtraReleaseTimeSchedule
    {
        public String id { get; set; }
        public Guid? realId { get; set; }
        public DateTime date { get; set; }
        public int? selectedPeriod { get; set; }
        public String reason { get; set; }
    }

    public class TicketRequest
    {
        public Boolean showSection { get; set; }
        public Boolean isInvitedGuest { get; set; }
        public Boolean isLocalPresident { get; set; }

        public Boolean showInvitedGuestMessage { get; set; }
        public String invitedGuestMessage { get; set; }

        public Boolean showLocalPresidentMessage { get; set; }
        public String localPresidentMessage { get; set; }

        public Boolean showTicketQuanity { get; set; }
        public int ticketQuanity { get; set; }

        public Boolean showAdditionalTicketQuantity { get; set; }
        public int additionalTicketQuantity { get; set; }

        public Decimal additionalTicketPrice { get; set; }

        public Boolean showAdditionalTicketCost { get; set; }
        public Decimal additionalTicketCost { get; set; }

        public Boolean showLocalName { get; set; }
        public String localName { get; set; }

        public Boolean showLocalDelegationLimit { get; set; }
        public int? localDelegationLimit { get; set; }
    }

    public class PersonalAccommodationConfiguration
    {
        public List<PersonalAccommodationOption> personalAccommodationList { get; set; }
    }

    public class PersonalAccommodationOption
    {
        [DefaultValue(false)]
        public Boolean? isSelected { get; set; }
        public Guid id { get; set; }
        public EntityReference selected { get; set; }
        public String name { get; set; }
        public int level { get; set; }
        public Guid? parent { get; set; }
        public bool isLeaf { get; set; }
        public bool expanded { get; set; }
        public bool loaded { get; set; }
    }

    public class Lookup
    {
        public Guid id { get; set; }
        public String label { get; set; }

        public Lookup setLabel(String inLabel)
        {
            this.label = inLabel;
            return this;
        }

        public EntityReference toEntityReference(String logicalEntityName)
        {
            if (this.id != Guid.Empty)
                return new EntityReference(logicalEntityName, this.id);
            else
                return null;
        }
    }

    public class Option
    {
        public int? value { get; set; }
        public String label { get; set; }

        public Option setLabel(String inLabel)
        {
            this.label = inLabel;
            return this;
        }
    }

    public class SchoolBoardLocal
    {
        [DefaultValue(false)]
        public Boolean? isSelected { get; set; }
        public Guid id { get; set; }
        public String name { get; set; }
        public Lookup schoolBoard { get; set; }
        public Lookup local { get; set; }
        public School school { get; set; }
        [DefaultValue(false)]
        public Boolean? isDefault { get; set; }
    }

    public class SchoolBoardList
    {
        public List<SchoolBoard> schoolBoardList { get; set; }
    }

    public class SchoolBoard
    {
        public Lookup schoolBoard { get; set; }
        public List<School> schoolList { get; set; }
        public List<Lookup> localList { get; set; }
    }

    public class School
    {

        public Lookup school { get; set; }
        public String city { get; set; }
        public String phone { get; set; }
        public String fax { get; set; }
        public String principalName { get; set; }


        public String id { get; set; }
        public String parent { get; set; }
        public String name { get; set; }
    }

    public class Form
    {
        public int source { get; set; }
        public Guid id { get; set; }
        public List<Section> sectionList { get; set; }

        public Section getSection(String bindingName)
        {
            if (sectionList == null) return new Section { isSelected = false };

            return sectionList.SingleOrDefault(x => x.bindingName == bindingName);
        }
    }

    public class Section
    {
        [DefaultValue(false)]
        public Boolean isSelected { get; set; }
        [DefaultValue(false)]
        public Boolean? isRequired { get; set; }
        public Guid id { get; set; }
        public Guid selectedId { get; set; }
        public String name { get; set; }
        public String bindingName { get; set; }
        public String description { get; set; }
        public int? displaySequence { get; set; }
        public bool ShouldSerializedisplaySequence()
        {
            return displaySequence != null;
        }
        [DefaultValue(false)]
        public Boolean? mandatoryFlag { get; set; }
        public List<Field> fieldList { get; set; }
        public String visibilityRule { get; set; }

        public Field getField(String bindingName)
        {
            if (fieldList == null) return new Field { isSelected = false };

            return fieldList.SingleOrDefault(x => x.bindingName == bindingName);
        }
    }

    public class Field
    {
        [DefaultValue(false)]
        public Boolean isSelected { get; set; }
        [DefaultValue(false)]
        public Boolean? isRequired { get; set; }
        public Guid id { get; set; }
        public Guid selectedId { get; set; }
        public String name { get; set; }
        public String bindingName { get; set; }
        public String displayText { get; set; }
        public int? displaySequence { get; set; }
        public bool ShouldSerializedisplaySequence()
        {
            return displaySequence != null;
        }
        public String description { get; set; }
        public Option fieldType { get; set; }
        //public String fieldType { get; set; }
        [DefaultValue(false)]
        public Boolean? systemDefinedFlag { get; set; }
        [DefaultValue(false)]
        public Boolean? mandatoryFlag { get; set; }
        [DefaultValue(false)]
        public Boolean? mandatoryValueFlag { get; set; }
        public Decimal? minValue { get; set; }
        public bool ShouldSerializeminValue()
        {
            return minValue != null;
        }
        public Decimal? maxValue { get; set; }
        public bool ShouldSerializemaxValue()
        {
            return maxValue != null;
        }
        public List<FieldValue> fieldValueList { get; set; }
        public List<Field> childrenFieldList { get; set; }
        public String visibilityRule { get; set; }

        public Field getChildField(String bindingName)
        {
            if (childrenFieldList == null) return new Field { isSelected = false };

            return childrenFieldList.SingleOrDefault(x => x.bindingName == bindingName);
        }
    }

    public class FieldValue
    {
        public String value { get; set; }
    }

}

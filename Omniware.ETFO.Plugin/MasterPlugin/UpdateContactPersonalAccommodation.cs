﻿using System;
using System.IO;
using System.Collections;
using System.Xml.Serialization;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Xrm;
using Microsoft.ApplicationBlocks.Data;
using System.Web.Script.Serialization;
using Microsoft.Xrm.Client.Collections;
using Microsoft.Crm.Sdk.Messages;
using Newtonsoft.Json;

namespace Omniware.ETFO.Plugin
{
    public class UpdateContactPersonalAccommodation : Plugin
    {

        public UpdateContactPersonalAccommodation()
            : base(typeof(UpdateContactPersonalAccommodation))
        {
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "Update", "contact", new Action<LocalPluginContext>(UpdateAccountPersonalAccommodationRequest)));
        }

        private void addAccountPersonalAccommodation(LocalPluginContext localContext, PersonalAccommodation personalAccommodation, Contact contact)
        {
            oems_AccountPersonalAccommodation accountPersonalAccommodation = new oems_AccountPersonalAccommodation
            {
                oems_PersonalAccommodationOption = new EntityReference { LogicalName = oems_PersonalAccommodationOption.EntityLogicalName, Id = personalAccommodation.id },
                //oems_Account = account.ToEntityReference(),
                //Attributes = contact.ToEntityReference(),
                //Attributes["oems_contact"] = contact.ToEntityReference(),
                //oems_Contact = contact.ToEntityReference(),
                oems_EventFlag = personalAccommodation.isEventSelected,
                oems_HotelFlag = personalAccommodation.isHotelSelected,
                oems_UserSpecifiedDetail = personalAccommodation.userSpecifiedDetails
            };

            accountPersonalAccommodation.Attributes["oems_contact"] = contact.ToEntityReference();

            accountPersonalAccommodation.oems_AccountPersonalAccommodationId = localContext.OmniContext.Create(accountPersonalAccommodation);
        }

        private void deleteAccountPersonalAccommodation(LocalPluginContext localContext, PersonalAccommodation personalAccommodation, Contact contact)
        {
            localContext.OmniContext.Delete(oems_AccountPersonalAccommodation.EntityLogicalName, personalAccommodation.selectedId);
        }

        private void updateAccountPersonalAccommodation(LocalPluginContext localContext, PersonalAccommodation personalAccommodation, Contact contact)
        {
            // get account personal accommodation
            oems_AccountPersonalAccommodation accountPersonalAccommodation =
                (from a in localContext.OmniContext.oems_AccountPersonalAccommodationSet
                 where (a.oems_AccountPersonalAccommodationId == personalAccommodation.selectedId)
                 select a).Single();
            
            accountPersonalAccommodation.oems_EventFlag = personalAccommodation.isEventSelected;
            accountPersonalAccommodation.oems_HotelFlag = personalAccommodation.isHotelSelected;
            accountPersonalAccommodation.oems_UserSpecifiedDetail = personalAccommodation.userSpecifiedDetails;

            localContext.OmniContext.UpdateObject(accountPersonalAccommodation);
        }

        private void processAccountPersonalAccommodation(LocalPluginContext localContext, List<PersonalAccommodation> personalAccommodationList, Contact contact)
        {

            // loop through personalAccommodation
            // find where isSelected = true and selectedId = Guid.Empty --- Add
            // find where isSelected = false and selectedId = <some GUID> --- Delete
            // find where isSelected = true and selectedId != Guid.Empty --- Update

            foreach (var personalAccommodation in personalAccommodationList)
            {
                if (personalAccommodation.isEventSelected == true || personalAccommodation.isHotelSelected == true)
                    personalAccommodation.isSelected = true;
                else
                    personalAccommodation.isSelected = false;

                // NEW
                if (personalAccommodation.isSelected == true && personalAccommodation.selectedId == Guid.Empty)
                    addAccountPersonalAccommodation(localContext, personalAccommodation, contact);

                // DELETE
                else if (personalAccommodation.isSelected == false && personalAccommodation.selectedId != Guid.Empty)
                    deleteAccountPersonalAccommodation(localContext, personalAccommodation, contact);

                // UPDATE
                else if (personalAccommodation.isSelected == true && personalAccommodation.selectedId != Guid.Empty)
                    updateAccountPersonalAccommodation(localContext, personalAccommodation, contact);
            }
        }

        protected void UpdateAccountPersonalAccommodationRequest(LocalPluginContext localContext)
        {
            if (localContext == null)
                throw new ArgumentNullException("localContext");

            // stop recurrsive calls
            if (localContext.PluginExecutionContext.Depth != 1)
                return;

            // get Account
            Contact contact = localContext.Entity.ToEntity<Contact>();

            PersonalAccommodationRequest personalAccommodationRequest = JsonConvert.DeserializeObject<PersonalAccommodationRequest>(contact.Attributes["oems_UpdatePersonalAccommodationData"].ToString());
            processAccountPersonalAccommodation(localContext, personalAccommodationRequest.personalAccommodationList, contact);
            localContext.OmniContext.SaveChanges();
        }


    }

}

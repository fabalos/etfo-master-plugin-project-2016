﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Workflow;
using System.Activities;

namespace Omniware.ETFO.Common
{
    public class FormatDateTime : CodeActivity 
    {
        
        [Input("Input Date")]
        public InArgument<DateTime> InDateTime {get;set;}

        [Input("Optional Input Date")]
        public InArgument<DateTime> OptionalDateTime { get; set; }

        [Input("Format")]
        public InArgument<string> Format { get; set; }

        [Output("Date Formatted")]
        public OutArgument<string> DateFormatted { get; set; }

        [Output("Optional Date Formatted")]
        public OutArgument<string> OptionalDateFormatted { get; set; }

        public FormatDateTime() 
        { }

        protected override void Execute(CodeActivityContext context)
        {
            string result = string.Empty;
            string optionalResult = string.Empty;

            string var_format = Format.Get(context);

            DateTime var_datetime = InDateTime.Get(context).ToLocalTime();
            DateTime var_optionalDate = OptionalDateTime.Get(context);

            if (!string.IsNullOrEmpty(var_format) && var_datetime != null)
            {
                result = var_datetime.ToString(var_format);

                if (var_optionalDate != null) 
                {
                    optionalResult = var_optionalDate.ToString(var_format);
                }
            }


            DateFormatted.Set(context, result);
            OptionalDateFormatted.Set(context, optionalResult);
        }
    }
}

﻿using System;
using System.Diagnostics;
using System.Linq;
using Microsoft.Xrm.Sdk;
using Omniware.ETFO.Plugin.Shared;
using Xrm;
using log4net;
using Shared.Utilities;
using System.Collections.Generic;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Messages;
using System.Text;

namespace Omniware.ETFO.Plugin
{
    public class WebPage : Plugin
    {
        protected string Config;
        protected string SecConfig;
        private ILog _log = LogHelper.GetLogger(typeof(WebPage));

        public WebPage(string unsecureConfig, string secureConfig)
            : base(typeof(WebPage))
        {
            if (unsecureConfig != null)
            {
                Config = unsecureConfig;
            }

            if (secureConfig != null)
            {
                SecConfig = secureConfig;
            }

            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "Update", "adx_webpage", new Action<LocalPluginContext>(UpdateEventUrl)));
        }

        protected void UpdateEventUrl(LocalPluginContext localContext)
        {
            if (localContext == null)
                throw new ArgumentNullException("localContext");

            //Stop recurrsive calls
            if (localContext.PluginExecutionContext.Depth != 1)
                return;

            Entity webPage = localContext.Entity;
            StringBuilder str = new StringBuilder();

            webPage = GetWebPage(localContext.OmniContext, webPage.Id);

            EntityReference eventWebPage = webPage.GetAttributeValue<EntityReference>("oems_eventid");
            str.Insert(0, "/" + webPage.GetAttributeValue<string>("adx_partialurl"));

            if (eventWebPage != null)
            {
                Entity parent = GetWebPage(localContext.OmniContext, webPage.GetAttributeValue<EntityReference>("adx_parentpageid").Id);

                while (parent.GetAttributeValue<EntityReference>("adx_parentpageid") != null)
                {
                    str.Insert(0, "/" + parent.GetAttributeValue<string>("adx_partialurl"));
                    parent = GetWebPage(localContext.OmniContext, parent.GetAttributeValue<EntityReference>("adx_parentpageid").Id);
                }

                str.Insert(0,GetPortalUrl(localContext.OmniContext));

                Entity aux = new Entity(eventWebPage.LogicalName);
                aux.Id = eventWebPage.Id;
                aux["oems_eventurl"] = str.ToString();
                localContext.OmniContext.Update(aux);
            }

        }

        private string GetPortalUrl(OmniContext context)
        {
            var config = (from a in context.oems_ConfigurationSet
                           select a).FirstOrDefault();

            string portalUrl = config.oems_PortalURL;
            portalUrl = portalUrl.Split(new string[] { "/api" }, StringSplitOptions.None)[0];
            return portalUrl;
        }

        private Entity GetWebPage(OmniContext context, Guid webPageId)
        {
            var webPage = (from a in context.Adx_webpageSet
                           where a.Adx_webpageId == webPageId
                           select a).FirstOrDefault();

            return webPage;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xrm;

namespace Omniware.ETFO.Plugin
{
    static class ProfileHelper
    {
        public static Contact UpdateContactFromAccount(Account account)
        {
            Contact contact = account.account_primary_contact;

            contact.FirstName = account.oems_FirstName;
            contact.LastName = account.oems_LastName;
            contact.oems_MemberNumber = account.oems_ETFOMemberId;
            contact.oems_LocalId = account.oems_Local;
            contact.oems_TeacherTypeId = account.oems_TeacherType;
            contact.Address1_Line1 = account.Address1_Line1;
            contact.Address1_Line2 = account.Address1_Line2;
            contact.Address1_City = account.Address1_City;
            contact.Address1_StateOrProvince = account.Address1_StateOrProvince;
            contact.Address1_PostalCode = account.Address1_PostalCode;
            contact.Address1_Country = account.Address1_Country;
            contact.Telephone1 = account.Telephone1;
            contact.Telephone2 = account.Telephone2;
            contact.MobilePhone = account.Telephone3;
            contact.Fax = account.Fax;
            contact.EMailAddress1 = account.EMailAddress1;
            contact.oems_OCTnumber = account.oems_OCTNumber;
            contact.oems_SchoolId = account.oems_School;
            contact.oems_SchoolBoardId = account.oems_SchoolBoard;
            contact.BirthDate = account.oems_Birthday;
            contact.oems_NonMemberType = account.oems_NonMemberType;

            return contact;
        }

        public static Account UpdateAccountFromContract(Contact contact)
        {
            Account account = contact.contact_customer_accounts;
            if (account.oems_MemberFlag.HasValue && (!account.oems_MemberFlag.Value))
            {
                account.oems_FirstName = contact.FirstName;
                account.oems_LastName = contact.LastName;
                account.Name = contact.FullName;
                account.oems_ETFOMemberId = contact.oems_MemberNumber;
                account.oems_Local = contact.oems_LocalId;
                account.oems_TeacherType = contact.oems_TeacherTypeId;
                account.Address1_Line1 = contact.Address1_Line1;
                account.Address1_Line2 = contact.Address1_Line2;
                account.Address1_City = contact.Address1_City;
                account.Address1_StateOrProvince = contact.Address1_StateOrProvince;
                account.Address1_PostalCode = contact.Address1_PostalCode;
                account.Address1_Country = contact.Address1_Country;
                account.Telephone1 = contact.Telephone1;
                account.Telephone2 = contact.Telephone2;
                account.Telephone3 = contact.MobilePhone;
                account.Fax = contact.Fax;
                account.EMailAddress1 = contact.Attributes["adx_username"].ToString();
                account.oems_OCTNumber = contact.oems_OCTnumber;
                account.oems_School = contact.oems_SchoolId;
                account.oems_SchoolBoard = contact.oems_SchoolBoardId;
                account.oems_Birthday = contact.BirthDate;
                account.oems_NonMemberType = contact.oems_NonMemberType;

            }
            else
            {
                account.EMailAddress1 = contact.Attributes["adx_username"].ToString();
            }
            return account;
        }
    }
}

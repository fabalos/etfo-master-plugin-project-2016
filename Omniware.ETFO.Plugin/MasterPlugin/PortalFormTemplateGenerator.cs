﻿using System;
using System.Diagnostics;
using System.IO;
using System.Collections;
using System.Xml.Serialization;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Omniware.ETFO.Plugin.Shared;
using Xrm;

using System.Web.Script.Serialization;
using Microsoft.Xrm.Client.Collections;
using Microsoft.Crm.Sdk.Messages;
using Newtonsoft.Json;
using log4net;

namespace Omniware.ETFO.Plugin
{

    public class PortalFormTemplateGenerator
    {
        ILog Log = LogHelper.GetLogger(typeof(PortalFormTemplateGenerator));

        List<FormComponents.IFormElement> formComponents = new List<FormComponents.IFormElement>();

        // cache all form elements attached to this template
        private List<oems_PortalFormElement> allTemplateElementEntities;

        // cache all form element properties and their values 
        private List<Tuple<string, string, string, Guid>> allPropertiesWithValues;



        public PortalFormTemplateGenerator()
        {
            formComponents.Add(new FormComponents.ReleaseTime());
            formComponents.Add(new FormComponents.PersonalAccommodation());
            formComponents.Add(new FormComponents.SchoolBoardLocal());
            formComponents.Add(new FormComponents.RoomPricingFormElement());
            formComponents.Add(new FormComponents.EventPricingFormElement());
            formComponents.Add(new FormComponents.WorkshopSession());
            formComponents.Add(new FormComponents.AttachedDocuments());
            formComponents.Add(new FormComponents.TargetGradesComponent());
        }

        string FindPropertyByName(List<Tuple<string, string, string, Guid>> properties, string name, string defValue)
        {
           var result = properties.Find(x => x.Item1.ToUpper() == name.ToUpper());
           if(result == null) return defValue;
           else return result.Item3;
        }

        string FindPropertyByCode(List<Tuple<string, string, string, Guid>> properties, string code, string defValue)
        {
            var result = properties.Find(x => x.Item2.ToUpper() == code.ToUpper());
            if (result == null) return defValue;
            else if (result.Item3 == null) return defValue;
            else return result.Item3;
        }

        DataWork.FormElement CreateElement(LocalPluginContext localContext, Entity oems_FormElement)
        {
            string step = "";
            

            DataWork.FormElement result = null;


            var properties = (from v in allPropertiesWithValues
                             where v.Item4  == oems_FormElement.Id
                              select v).ToList();
                             

            //throw new Exception(properties.Count().ToString());
            
            result = new DataWork.FormElement();

            result.id = oems_FormElement.Contains("oems_formelementname") && oems_FormElement["oems_formelementname"] != null ? (string)oems_FormElement["oems_formelementname"] : null;
            result.name = oems_FormElement.Id.ToString();

            result.options.label = FindPropertyByCode(properties, "pr_Label", "");
            result.options.title = result.options.label;

            result.options.leftLabel = FindPropertyByCode(properties, "pr_LeftLabel", "");

            result.options.backgroundColor = FindPropertyByCode(properties,"pr_Color","");

            result.value = null;
            result.valueFormatter = FindPropertyByCode(properties, "pr_Date", null) == null ?
                                            (FindPropertyByCode(properties, "pr_DateTime", null) == null ? null : (int?)1) : 0;
            
            //oems_FormElement.Contains("oems_valueformatter") && oems_FormElement["oems_valueformatter"] != null ? (int?)((OptionSetValue)oems_FormElement["oems_valueformatter"]).Value : null;
            
            EntityReference elementTypeRef =  (EntityReference)oems_FormElement["oems_formelementtype"];
            Entity elementType = localContext.OmniContext.Retrieve(elementTypeRef.LogicalName, elementTypeRef.Id, new ColumnSet(true));
            
            string schemaTypeName = null;

            if (elementType.Contains("oems_schematypename"))
                schemaTypeName = (string)elementType["oems_schematypename"];

                result.type = schemaTypeName;
            
            switch (schemaTypeName)
            {
                case "releasetime":
                    result.type = "section";
                    result.component = (int) FORM_COMPONENT.ReleaseTime;
                    if (result.sourcePath == null) result.sourcePath = "";
                    break;
                case "personalaccommodation":
                    result.type = "section";
                    result.component = (int) FORM_COMPONENT.PersonalAccommodation;
                    if (result.sourcePath == null) result.sourcePath = "";
                    break;
                case "schoolboardlocal":
                    result.type = "section";
                    result.component = (int) FORM_COMPONENT.SchoolBoardLocal;
                    if (result.sourcePath == null) result.sourcePath = "";
                    break;
                // these values really do not serve any purpose other then preventing DataAccess.SaveForm from filtering those elements out
                case "roomprice":                    
                    result.component = (int) FORM_COMPONENT.RoomPricing;
                    if (result.sourcePath == null) result.sourcePath = "";
                    break;
                case "eventprice":                    
                    result.component = (int) FORM_COMPONENT.EventPricing;
                    if (result.sourcePath == null) result.sourcePath = "";
                    break;
                case "workshopsession":
                    result.component = (int)FORM_COMPONENT.WorkshopSession;
                    if (result.sourcePath == null) result.sourcePath = "";
                    break;
                case "attachments":
                    result.component = (int)FORM_COMPONENT.Attachments;
                    if (result.sourcePath == null) result.sourcePath = "";
                    break;
                case "targetgrades":
                    result.component = (int)FORM_COMPONENT.TargetGrades;
                    if (result.sourcePath == null) result.sourcePath = "";
                    break;
                default:
                    break;
            }

            


            if(schemaTypeName == "legend")
            {
                result.value = FindPropertyByCode(properties, "pr_Message", null);
                if (result.value != null) result.value = DataWork.DataAccess.JsonStringEscape(result.value);
            }
            if(oems_FormElement.Contains("oems_databinding"))
            {
                EntityReference bindingRef = (EntityReference)oems_FormElement["oems_databinding"];
                Entity binding = localContext.OmniContext.Retrieve(bindingRef.LogicalName, bindingRef.Id, new ColumnSet(true));
                result.sourcePath = binding.Contains("oems_sourcepath") && binding["oems_sourcepath"] != null ? (string)binding["oems_sourcepath"] : null;
                result.targetPath = binding.Contains("oems_targetpath") && binding["oems_targetpath"] != null ? (string)binding["oems_targetpath"] : null;
                result.deletePath = binding.Contains("oems_deletepath") && binding["oems_deletepath"] != null ? (string)binding["oems_deletepath"] : null;

                result.initialPath = binding.Contains("oems_initialpath") && binding["oems_initialpath"] != null ? (string)binding["oems_initialpath"] : null;
            }

            
            //result.oneToOneParentLookupPath = oems_FormElement.Contains("oems_onetooneparentlookuppath") && oems_FormElement["oems_onetooneparentlookuppath"] != null ? (string)oems_FormElement["oems_onetooneparentlookuppath"] : null;
            result.options.disabled = FindPropertyByCode(properties, "pr_ReadOnly", "false").ToLower() == "true";

            result.options.required = FindPropertyByCode(properties, "pr_Required", "false").ToLower() == "true";
            result.options.inline = FindPropertyByCode(properties, "pr_InlineFlag", "false").ToLower() == "true"; 

            int val;
            bool boolVal;
            if(int.TryParse(FindPropertyByCode(properties, "pr_MinLength", "false"), out val))
            {
              result.options.minlength = val;
            }
            if (int.TryParse(FindPropertyByCode(properties, "pr_MaxLength", null), out val))
            {
                result.options.maxlength = val;
            }
            if (int.TryParse(FindPropertyByCode(properties, "pr_BootstrapWidth", null), out val))
            {
                result.options.width = val;
            }

            result.options.displayColumns = FindPropertyByCode(properties, "pr_DisplayColumnFlag", "false").ToLower() == "true";
            result.options.displayGroupHeaders = FindPropertyByCode(properties, "pr_DisplayGroupFlag", "false").ToLower() == "true";
            if(FindPropertyByCode(properties, "pr_ColumnTitles", null) != null)
            {
                 string[] columnLabels = FindPropertyByCode(properties, "pr_ColumnTitles", "").Split('/');
                 result.options.labelColumns = new DataWork.Label[columnLabels.Length];
                 for(int i = 0; i< columnLabels.Length; i++)
                 {
                    result.options.labelColumns[i] = new DataWork.Label(){
                                          label = columnLabels[i]                   
                                 };
                 }
            }

            
            if (int.TryParse(FindPropertyByCode(properties, "pr_MinValue", null), out val))
            {
                result.options.min =  val;
            }
            if (int.TryParse(FindPropertyByCode(properties, "pr_MaxValue", null), out val))
            {
                result.options.max =  val;
            }
            if (int.TryParse(FindPropertyByCode(properties, "pr_MinDocs", null), out val))
            {
                result.options.minDocs = val;
            }
            if (bool.TryParse(FindPropertyByCode(properties, "pr_DisplayAsRadioButtons", null), out boolVal))
            {
                result.options.displayAsRadios = boolVal;
            }

            if (int.TryParse(FindPropertyByCode(properties, "pr_IncrementStep", null), out val))
            {
                result.options.step = val;
            }
            else
            {
                result.options.step = 1;
            }


            if (oems_FormElement.Contains("oems_expandingformelementcode") && oems_FormElement["oems_expandingformelementcode"] != null)
            {
                // find id of expanding element
                oems_PortalFormElement expandingFormElement = 
                    (from a in localContext.OmniContext.oems_PortalFormElementSet
                     where a.oems_FormTemplate.Id == ((EntityReference)oems_FormElement["oems_formtemplate"]).Id
                        && a.oems_FormElementCode == (String)oems_FormElement["oems_expandingformelementcode"]
                     select a).SingleOrDefault();

                if (expandingFormElement != null)
                {
                    result.options.enablesSection = expandingFormElement.Id.ToString();
                }
                else
                {
                    // log warning, not sure if it is critical
                    Log.WarnFormat("expanding form elmenent with code {0} not found", oems_FormElement["oems_expandingformelementcode"]); 
                }

            }

            /*
            foreach (var fc in formComponents)
            {
                //if(fc.LoadComponent(localContext, oems_FormElement, result)) return result;
            }
            */

            
            AddChildren(localContext, result, oems_FormElement.Id);
            
            return result;

        }

         void AddChildren(LocalPluginContext localContext, DataWork.FormElement element, Guid elementId)
        {
            
            var elements = (from e in allTemplateElementEntities
                           where e.statecode == 0 && e.oems_ParentFormElement != null && e.oems_ParentFormElement.Id == elementId 
                           orderby e.oems_displaysequence ascending
                           select e);

            ArrayList children = new ArrayList();
            foreach (var e in elements)
            {
                children.Add(CreateElement(localContext, e));
            }

            if (element.type == "repeatableSection" && children.Count > 0)
            {
                element.children = new DataWork.FormElement[0];
                element.options.newRowTemplate = (DataWork.FormElement)children[0];
            }
            else
            {
                element.children = new DataWork.FormElement[children.Count];
                for (int i = 0; i < children.Count; i++)
                {
                    element.children[i] = (DataWork.FormElement)children[i];
                }
            }
        }

        public String GetFormData(LocalPluginContext localContext, Guid formTemplateId)
        {


            var sw = new Stopwatch(); sw.Start();

            allTemplateElementEntities = (from portalFormElement in localContext.OmniContext.oems_PortalFormElementSet
                                          where portalFormElement.oems_FormTemplate.Id == formTemplateId
                                          select portalFormElement).ToList<oems_PortalFormElement>();
            allPropertiesWithValues =
                (from portalFormElement in localContext.OmniContext.oems_PortalFormElementSet
                 join propertyValue in localContext.OmniContext.oems_FormElementPropertyValueSet on portalFormElement.Id equals propertyValue.oems_FormElement.Id
                 join property in localContext.OmniContext.oems_FormElementPropertySet on propertyValue.oems_FormElementProperty.Id equals property.Id
                 where portalFormElement.oems_FormTemplate.Id == formTemplateId
                 select new Tuple<string, string, string, Guid>(property.oems_PropertyName, property.oems_PropertyCode, propertyValue.oems_PropertyValue, propertyValue.oems_FormElement.Id)).ToList();

            oems_PortalFormElement topLevelElement =
                (from portalFormElement in allTemplateElementEntities
                 where portalFormElement.oems_ParentFormElement == null
                    && portalFormElement.oems_FormElementName == "Template"
                 select portalFormElement).SingleOrDefault();

            if (topLevelElement == null)
                throw new InvalidPluginExecutionException("Form must have root form element named Template");

            DataWork.FormElement form = CreateElement(localContext, topLevelElement);

            var jsonData = JsonConvert.SerializeObject(form);

            Log.DebugFormat("GetFormData ({0}) {1} msec", formTemplateId, sw.ElapsedMilliseconds);
            return jsonData;
        }
    }
}

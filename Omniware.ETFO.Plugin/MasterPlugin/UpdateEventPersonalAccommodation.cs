﻿using System;
using System.IO;
using System.Collections;
using System.Xml.Serialization;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Xrm;
using Microsoft.ApplicationBlocks.Data;
using System.Web.Script.Serialization;
using Microsoft.Xrm.Client.Collections;
using Microsoft.Crm.Sdk.Messages;
using Newtonsoft.Json;

namespace Omniware.ETFO.Plugin
{
    public class UpdateEventPersonalAcommodation : Plugin
    {

        public UpdateEventPersonalAcommodation()
            : base(typeof(UpdateEventPersonalAcommodation))
        {
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "Update", "oems_event", new Action<LocalPluginContext>(UpdatePersonalAccommodationConfig)));
        }

        private void associatePersonalAccommodation(LocalPluginContext localContext, EntityReference oemsEvent, EntityReference personalAccommodationOption)
        {
            AssociateEntitiesRequest associateRequest = new AssociateEntitiesRequest
            {
                Moniker1 = oemsEvent,
                Moniker2 = personalAccommodationOption,
                RelationshipName = "oems_event_personal_accommodation_config",
            };

            try
            {
                localContext.OmniContext.Execute(associateRequest);
            }
            catch (Exception)
            {
                // nothing happens
            }
        }

        private void disassociatePersonalAccommodation(LocalPluginContext localContext, EntityReference oemsEvent, EntityReference personalAccommodationOption)
        {
            DisassociateEntitiesRequest disassociateRequest = new DisassociateEntitiesRequest
            {
                Moniker1 = oemsEvent,
                Moniker2 = personalAccommodationOption,
                RelationshipName = "oems_event_personal_accommodation_config",
            };

            try
            {
                localContext.OmniContext.Execute(disassociateRequest);
            }
            catch (Exception)
            {
                // nothing happens
            }
        }

        private void processPersonalAccommodation(LocalPluginContext localContext, List<PersonalAccommodationOption> personalAccommodationOptionList, oems_Event oemsEvent)
        {

            // loop through personalAccommodationConfiguration
            // find where isSelected = true and selectedId = Guid.Empty --- Add
            // find where isSelected = false and selectedId = <some GUID> --- Delete

            foreach (var personalAccommodation in personalAccommodationOptionList)
            {
                // NEW
                if (personalAccommodation.isSelected == true && personalAccommodation.selected == null)
                    associatePersonalAccommodation(localContext, oemsEvent.ToEntityReference(), new EntityReference("oems_personalaccommodationoption", personalAccommodation.id));

                // DELETE
                else if (!personalAccommodation.isSelected == true && personalAccommodation.selected != null)
                    disassociatePersonalAccommodation(localContext, oemsEvent.ToEntityReference(), personalAccommodation.selected);

            }
        }

        protected void UpdatePersonalAccommodationConfig(LocalPluginContext localContext)
        {
            if (localContext == null)
                throw new ArgumentNullException("localContext");

            // stop recurrsive calls
            if (localContext.PluginExecutionContext.Depth != 1)
                return;

            // get Event
            oems_Event oemsEvent = localContext.Entity.ToEntity<oems_Event>();
            oems_Event preImageOemsEvent = localContext.PluginExecutionContext.PreEntityImages["PreImage"].ToEntity<oems_Event>();

            if (((oemsEvent.Contains("oems_updatepersonalaccommodationdata") && oemsEvent["oems_updatepersonalaccommodationdata"] != null &&
                 preImageOemsEvent.Contains("oems_updatepersonalaccommodationdata") && preImageOemsEvent["oems_updatepersonalaccommodationdata"] != null &&
                 (string)preImageOemsEvent["oems_updatepersonalaccommodationdata"] != (string)oemsEvent["oems_updatepersonalaccommodationdata"])) ||
               ((oemsEvent.Contains("oems_updatepersonalaccommodationdata") && oemsEvent["oems_updatepersonalaccommodationdata"] != null &&
                 (!preImageOemsEvent.Contains("oems_updatepersonalaccommodationdata") || (preImageOemsEvent.Contains("oems_updatepersonalaccommodationdata") && preImageOemsEvent["oems_updatepersonalaccommodationdata"] == null)))))
            {

                PersonalAccommodationConfiguration personalAccommodationConfiguration = JsonConvert.DeserializeObject<PersonalAccommodationConfiguration>(oemsEvent.oems_UpdatePersonalAccommodationData);

                processPersonalAccommodation(localContext, personalAccommodationConfiguration.personalAccommodationList, oemsEvent);
            }
        }

    }
}

﻿using System;
using System.IO;
using System.Collections;
using System.Xml.Serialization;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Xrm;
using Microsoft.ApplicationBlocks.Data;
using System.Web.Script.Serialization;
using Microsoft.Xrm.Client.Collections;
using Microsoft.Crm.Sdk.Messages;

namespace Omniware.ETFO.Plugin
{
    public class EventRegistrationSchedule : Plugin
    {

        public EventRegistrationSchedule()
            : base(typeof(EventRegistrationSchedule))
        {
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "Update", "oems_eventregistrationschedule", new Action<LocalPluginContext>(UpdateEventRegistration)));
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "Create", "oems_eventregistrationschedule", new Action<LocalPluginContext>(CreateEventRegistration)));
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(20, "Delete", "oems_eventregistrationschedule", new Action<LocalPluginContext>(DeleteEventRegistration)));
        }

        protected void UpdateEventRegistration(LocalPluginContext localContext)
        {
            
            if (localContext == null)
                throw new ArgumentNullException("localContext");

            // stop recurrsive calls
            if (localContext.PluginExecutionContext.Depth != 1)
                return;

            oems_EventRegistrationSchedule eventRegistrationSchedule = localContext.PluginExecutionContext.PostEntityImages["PostImage"].ToEntity<oems_EventRegistrationSchedule>();
            /*
            oems_EventRegistrationSchedule eventRegistrationSchedule =
                (from a in localContext.OmniContext.oems_EventRegistrationScheduleSet
                 where a.oems_EventRegistrationScheduleId == localContext.Entity.Id
                 select a).FirstOrDefault();*/
            UpdateEventDeadlines(localContext, eventRegistrationSchedule, false);
        }

        protected void CreateEventRegistration(LocalPluginContext localContext)
        {
            if (localContext == null)
                throw new ArgumentNullException("localContext");

            // stop recurrsive calls
            if (localContext.PluginExecutionContext.Depth != 1)
                return;

            oems_EventRegistrationSchedule eventRegistrationSchedule = localContext.PluginExecutionContext.PostEntityImages["PostImage"].ToEntity<oems_EventRegistrationSchedule>();
            /*
            oems_EventRegistrationSchedule eventRegistrationSchedule =
                (from a in localContext.OmniContext.oems_EventRegistrationScheduleSet
                 where a.oems_EventRegistrationScheduleId == localContext.Entity.Id
                 select a).FirstOrDefault();*/
            UpdateEventDeadlines(localContext, eventRegistrationSchedule, false);
        }

        protected void DeleteEventRegistration(LocalPluginContext localContext)
        {
            if (localContext == null)
                throw new ArgumentNullException("localContext");

            // stop recurrsive calls
            if (localContext.PluginExecutionContext.Depth != 1)
                return;

            oems_EventRegistrationSchedule eventRegistrationSchedule = localContext.PluginExecutionContext.PreEntityImages["PreImage"].ToEntity<oems_EventRegistrationSchedule>();
            /*
                (from a in localContext.OmniContext.oems_EventRegistrationScheduleSet
                 where a.oems_EventRegistrationScheduleId == localContext.EntityReference.Id
                 select a).FirstOrDefault();*/
            UpdateEventDeadlines(localContext, eventRegistrationSchedule, true);
        }

        protected void UpdateEventDeadlines(LocalPluginContext localContext, oems_EventRegistrationSchedule eventRegistrationSchedule, bool deleting)
        {
            if (eventRegistrationSchedule.oems_Event == null)
            {
                return;
            }
            
            oems_Event oemsEvent =
                 (from a in localContext.OmniContext.oems_EventSet
                 where a.oems_EventId == eventRegistrationSchedule.oems_Event.Id
                 select a).FirstOrDefault();

            if (oemsEvent == null)
            {

                return;
            }

            var query = localContext.OmniContext.oems_EventRegistrationScheduleSet;


            query = query.Where(r => r.oems_Event.Id == oemsEvent.oems_EventId);
            query = query.Where(r => r.statecode.Value == 0);
            if (deleting)
            {
                query = query.Where(r => r.Id != eventRegistrationSchedule.Id);
            }
            DateTime? registrationOpening = null;
            DateTime? registrationDeadline = null;

            var registrations = query.ToList();
            foreach (var r in registrations)
            {
                //oems_registrationopendatetime
                //oems_registrationclosedatetime
                if (registrationOpening == null && r.oems_RegistrationOpenDateTime != null ||
                    registrationOpening != null && r.oems_RegistrationOpenDateTime != null && registrationOpening.Value > r.oems_RegistrationOpenDateTime.Value)
                {
                    registrationOpening = r.oems_RegistrationOpenDateTime;
                }
                if (registrationDeadline == null && r.oems_RegistrationCloseDateTime != null ||
                    registrationDeadline != null && r.oems_RegistrationCloseDateTime != null && registrationDeadline.Value < r.oems_RegistrationCloseDateTime.Value)
                {
                    registrationDeadline = r.oems_RegistrationCloseDateTime;
                }
            }

            //Entity entity = new Entity("oems_event");
            //entity.Id = oemsEvent.Id;
            
            //entity["oems_registrationdeadline"] = registrationDeadline;
            //entity["oems_registrationopening"] = registrationOpening;

            oemsEvent.oems_RegistrationDeadline = registrationDeadline;
            oemsEvent.oems_RegistrationOpening = registrationOpening;

            // if reg opening > today and current status = open or closed then status = published
            // if reg opening <= than today and deadline > today and current status = published or closed then status = open
            // if reg deadline <= today and current status = published or open then status = closed
            //if (registrationOpening > DateTime.Now 
            //    && (oemsEvent.statuscode == (int)EventStatusReason.OPEN_REGISTRATION || oemsEvent.statuscode == (int)EventStatusReason.CLOSE_REGISTRATION))
            //    oemsEvent.statuscode = (int)EventStatusReason.PUBLISH_REGISTRATION;
            //else if (registrationOpening <= DateTime.Now && registrationDeadline > DateTime.Now
            //    && (oemsEvent.statuscode == (int)EventStatusReason.PUBLISH_REGISTRATION || oemsEvent.statuscode == (int)EventStatusReason.CLOSE_REGISTRATION))
            //    oemsEvent.statuscode = (int)EventStatusReason.OPEN_REGISTRATION;
            //else if (registrationDeadline <= DateTime.Now
            //    && (oemsEvent.statuscode == (int)EventStatusReason.PUBLISH_REGISTRATION || oemsEvent.statuscode == (int)EventStatusReason.OPEN_REGISTRATION))
            //    oemsEvent.statuscode = (int)EventStatusReason.CLOSE_REGISTRATION;

            localContext.OmniContext.UpdateObject(oemsEvent);
            localContext.OmniContext.SaveChanges();
          //  localContext.OrganizationService.Update(entity);
        }
    }
}


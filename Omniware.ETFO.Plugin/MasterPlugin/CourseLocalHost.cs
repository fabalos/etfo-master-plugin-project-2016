﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Omniware.ETFO.Plugin.DataWork;
using Omniware.ETFO.Plugin.FormComponents;
using Omniware.ETFO.Plugin.Shared;
using Xrm;
using log4net;
using Microsoft.Crm.Sdk.Messages;

namespace Omniware.ETFO.Plugin
{
    public class CourseLocalHost : Plugin
    {
        private ILog _log = LogHelper.GetLogger(typeof(CourseLocalHost));

        public CourseLocalHost()
            : base(typeof(CourseLocalHost))
        {
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(20, "Update", "oems_courselocalhost", new Action<LocalPluginContext>(CopyCourseLocalHost)));
        }

        protected void CopyCourseLocalHost(LocalPluginContext localContext)
        {
            if (localContext == null)
                throw new ArgumentNullException("localContext");
            Entity courseLocalHost = localContext.Entity;

            Entity fullCourseLocalHost = (from c in localContext.OmniContext.CreateQuery("oems_courselocalhost")
                                          where (Guid)c["oems_courselocalhostid"] == courseLocalHost.Id
                                      select c
                                ).FirstOrDefault();

            Entity newCourseLocalHost = new Entity(courseLocalHost.LogicalName);

            newCourseLocalHost.Attributes["oems_courseterm"] = courseLocalHost.GetAttributeValue<EntityReference>("oems_courseterm");
            newCourseLocalHost.Attributes["oems_name"] = fullCourseLocalHost.GetAttributeValue<string>("oems_name");
            newCourseLocalHost.Attributes["oems_local"] = fullCourseLocalHost.GetAttributeValue<EntityReference>("oems_local");
            newCourseLocalHost.Attributes["oems_schoolboard"] = fullCourseLocalHost.GetAttributeValue<EntityReference>("oems_schoolboard");
            newCourseLocalHost.Attributes["oems_localhostmanager1cell"] = fullCourseLocalHost.GetAttributeValue<string>("oems_localhostmanager1cell");
            newCourseLocalHost.Attributes["oems_localhostmanager1city"] = fullCourseLocalHost.GetAttributeValue<string>("oems_localhostmanager1city");
            newCourseLocalHost.Attributes["oems_localhostmanager1email"] = fullCourseLocalHost.GetAttributeValue<string>("oems_localhostmanager1email");
            newCourseLocalHost.Attributes["oems_localhostmanager1phone"] = fullCourseLocalHost.GetAttributeValue<string>("oems_localhostmanager1phone");
            newCourseLocalHost.Attributes["oems_localhostmanager1postalcode"] = fullCourseLocalHost.GetAttributeValue<string>("oems_localhostmanager1postalcode");
            newCourseLocalHost.Attributes["oems_localhostmanager1province"] = fullCourseLocalHost.GetAttributeValue<string>("oems_localhostmanager1province");
            newCourseLocalHost.Attributes["oems_localhostmanager1street"] = fullCourseLocalHost.GetAttributeValue<string>("oems_localhostmanager1street");
            newCourseLocalHost.Attributes["oems_localhostmanager2cell"] = fullCourseLocalHost.GetAttributeValue<string>("oems_localhostmanager2cell");
            newCourseLocalHost.Attributes["oems_localhostmanager2city"] = fullCourseLocalHost.GetAttributeValue<string>("oems_localhostmanager2city");
            newCourseLocalHost.Attributes["oems_localhostmanager2email"] = fullCourseLocalHost.GetAttributeValue<string>("oems_localhostmanager2email");
            newCourseLocalHost.Attributes["oems_localhostmanager2phone"] = fullCourseLocalHost.GetAttributeValue<string>("oems_localhostmanager2phone");
            newCourseLocalHost.Attributes["oems_localhostmanager2postalcode"] = fullCourseLocalHost.GetAttributeValue<string>("oems_localhostmanager2postalcode");
            newCourseLocalHost.Attributes["oems_localhostmanager2province"] = fullCourseLocalHost.GetAttributeValue<string>("oems_localhostmanager2province");
            newCourseLocalHost.Attributes["oems_localhostmanager2street"] = fullCourseLocalHost.GetAttributeValue<string>("oems_localhostmanager2street");
            newCourseLocalHost.Attributes["oems_localmanager1firstname"] = fullCourseLocalHost.GetAttributeValue<string>("oems_localmanager1firstname");
            newCourseLocalHost.Attributes["oems_localmanager2firstname"] = fullCourseLocalHost.GetAttributeValue<string>("oems_localmanager2firstname");
            newCourseLocalHost.Attributes["oems_localmanager1lastname"] = fullCourseLocalHost.GetAttributeValue<string>("oems_localmanager1lastname");
            newCourseLocalHost.Attributes["oems_localmanager2lastname"] = fullCourseLocalHost.GetAttributeValue<string>("oems_localmanager2lastname");
            

            try
            {
                localContext.OrganizationService.Create(newCourseLocalHost);
                localContext.Entity.Attributes.Remove("oems_courseterm");
            }
            catch (Exception e)
            {
                throw e;
            }
        }

    }
}

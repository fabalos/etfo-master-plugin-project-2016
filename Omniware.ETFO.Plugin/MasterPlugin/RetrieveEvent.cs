﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Omniware.ETFO.Plugin.Shared;
using Xrm;
using Microsoft.Crm.Sdk.Messages;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using log4net;

namespace Omniware.ETFO.Plugin
{
    public class RetrieveEvent : Plugin
    {

        ILog Log = LogHelper.GetLogger(typeof(RetrieveEvent));

        public RetrieveEvent()
            : base(typeof(RetrieveEvent))
        {
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "Retrieve", "oems_event", new Action<LocalPluginContext>(ProcessRetrieveEvent)));
        }


        void ProcessRetrieveEvent(LocalPluginContext localContext)
        {
            if (localContext.PluginExecutionContext.Depth != 1) return;

            Entity entity = (Entity)localContext.PluginExecutionContext.OutputParameters["BusinessEntity"];

            // get Event
            oems_Event oemsEvent =
                (from a in localContext.OmniContext.oems_EventSet
                 where a.oems_EventId == entity.Id
                 select a).Single();

            RetrievePersonalAccommodationConfig(localContext, oemsEvent);
            RetrieveAttendingAsConfig(localContext, oemsEvent);
            RetrieveEventInformation(localContext, oemsEvent);
        } 

        private List<PersonalAccommodationOption> getPersonalAccommodationOption(LocalPluginContext localContext, List<oems_PersonalAccommodationOption> oemsPersonalAccommodationOptionList, List<oems_PersonalAccommodationOption> oemsEventPersonalAccommodationOptionList)
        {
            List<PersonalAccommodationOption> personalAccommodationOptionList =
            (from a in oemsPersonalAccommodationOptionList
             join b in oemsEventPersonalAccommodationOptionList on a.oems_PersonalAccommodationOptionId equals b.oems_PersonalAccommodationOptionId into c
             from d in c.DefaultIfEmpty()
             orderby a.oems_DisplaySequence
             select new PersonalAccommodationOption
             {
                 isSelected = (d != null),
                 id = (Guid)a.oems_PersonalAccommodationOptionId,
                 selected = (d != null ? a.ToEntityReference() : null),
                 name = a.oems_OptionName,
                 level = (int)a.oems_Level,
                 parent = (a.oems_ParentOption != null ? a.oems_ParentOption.Id : (Guid?)null ),
                 isLeaf = (a.oems_OptionType != PersonalAccommodationType.Header),
                 expanded = true,
                 loaded = true
             }
            ).ToList<PersonalAccommodationOption>();

            return personalAccommodationOptionList;
        }

        void RetrievePersonalAccommodationConfig(LocalPluginContext localContext, oems_Event oemsEvent)
        {

            var sw = new Stopwatch(); sw.Start();

            // get the the personal accommodation options
            PersonalAccommodationConfiguration personalAccommodationConfiguration = new PersonalAccommodationConfiguration();
            personalAccommodationConfiguration.personalAccommodationList =
                getPersonalAccommodationOption
                (localContext
                , (from a in localContext.OmniContext.oems_PersonalAccommodationOptionSet
                   select a).ToList<oems_PersonalAccommodationOption>()
                , oemsEvent.oems_event_personal_accommodation_config.ToList<oems_PersonalAccommodationOption>()
                );

            // return the json of the set
            var json = JsonConvert.SerializeObject(personalAccommodationConfiguration);

            ((Entity)localContext.PluginExecutionContext.OutputParameters["BusinessEntity"])["oems_retrievepersonalaccommodationdata"] = json;

            sw.Stop(); Log.DebugFormat("RetrievePersonalAccommodationConfig {0} msec", sw.ElapsedMilliseconds); 

            //oemsEvent.oems_RetrievePersonalAccommodationData = json.ToString();
            //localContext.OmniContext.UpdateObject(oemsEvent);
            //localContext.OmniContext.SaveChanges();
        }

        private List<AttendingAsOption> getAttendingAsOption(LocalPluginContext localContext, List<oems_attendingasoption> oemsAttendingAsOptionList, List<oems_attendingasoption> oemsEventAttendingAsOptionList)
        {
            List<AttendingAsOption> attendingAsOptionList =
            (from a in oemsAttendingAsOptionList
             join b in oemsEventAttendingAsOptionList on a.oems_attendingasoptionId equals b.oems_attendingasoptionId into c
             from d in c.DefaultIfEmpty()
             select new AttendingAsOption
             {
                 isSelected = (d != null),
                 id = (Guid)a.oems_attendingasoptionId,
                 selected = (d != null ? a.ToEntityReference() : null),
                 name = a.oems_OptionName,
                 isVotingRole = (bool)a.oems_VotingRoleFlag,
                 isGuestRole = (bool)a.oems_GuestRoleFlag,
                 isOther = (bool)a.oems_OtherFlag,
                 colour = a.oems_Colour,
                 displaySequence = a.oems_DisplaySequence
             }
            ).OrderBy(x => x.displaySequence).ToList<AttendingAsOption>();

            return attendingAsOptionList;
        }

        protected void RetrieveAttendingAsConfig(LocalPluginContext localContext, oems_Event oemsEvent)
        {

            var sw = new Stopwatch(); sw.Start();


            // get the personal accommodation options
            AttendingAsConfiguration attendingAsConfiguration = new AttendingAsConfiguration();
            
            var oemsEventAttendingAsOptionList = oemsEvent.oems_event_attending_as_config.ToList<oems_attendingasoption>();

            var oemsAttendingAsOptionList = (from a in localContext.OmniContext.oems_attendingasoptionSet
                                             select a).ToList<oems_attendingasoption>();

            attendingAsConfiguration.attendingAsList = getAttendingAsOption( localContext, oemsAttendingAsOptionList, oemsEventAttendingAsOptionList);

            // return the json of the set
            var json = JsonConvert.SerializeObject(attendingAsConfiguration);

            ((Entity)localContext.PluginExecutionContext.OutputParameters["BusinessEntity"])["oems_retrieveattendingasdata"] = json;

            sw.Stop(); Log.DebugFormat("RetrieveAttendingAsConfig {0} msec", sw.ElapsedMilliseconds); 

            //oemsEvent.oems_RetrieveAttendingAsData = json.ToString();

            //localContext.OmniContext.UpdateObject(oemsEvent);
            //localContext.OmniContext.SaveChanges();
        }

        private EventInformation getEventInformation(LocalPluginContext localContext, oems_Event oemsEvent)
        {
            Regex regex = new Regex(@"(\r\n|\r|\n)+");

            EventInformation eventInformation = new EventInformation();

            eventInformation.eventName = oemsEvent.oems_EventName;

            int? getTimeZoneCode = RetrieveCurrentUsersSettings(localContext.OrganizationService);

            if (oemsEvent.oems_StartDate != null)
            {
                
                DateTime startDate = RetrieveLocalTimeFromUTCTime((DateTime)oemsEvent.oems_StartDate, getTimeZoneCode, localContext.OrganizationService);

                if (oemsEvent.oems_EndDate != null && oemsEvent.oems_StartDate != oemsEvent.oems_EndDate)
                {
                    DateTime endDate = RetrieveLocalTimeFromUTCTime((DateTime)oemsEvent.oems_EndDate, getTimeZoneCode, localContext.OrganizationService);
                    //eventInformation.eventDate = ((DateTime)oemsEvent.oems_StartDate).ToString("f") + " - " + ((DateTime)oemsEvent.oems_EndDate).ToString("f");
                    eventInformation.eventDate = (startDate).ToString("f") + " - " + (endDate).ToString("f");
                }
                else
                {
                    //eventInformation.eventDate = ((DateTime)oemsEvent.oems_StartDate).ToString("f");
                    eventInformation.eventDate = (startDate).ToString("f");
                }
            }
            else
            {
                eventInformation.eventDate = "";
            }

            if (oemsEvent.oems_location_to_event != null && oemsEvent.oems_location_to_event.oems_PortalDisplayText != null)
                eventInformation.eventLocation =  regex.Replace(oemsEvent.oems_location_to_event.oems_PortalDisplayText, "<br/>");

            if (oemsEvent.oems_accommodationlocation_to_event != null && oemsEvent.oems_accommodationlocation_to_event.oems_PortalDisplayText != null)
                eventInformation.accommodationtLocation = regex.Replace(oemsEvent.oems_accommodationlocation_to_event.oems_PortalDisplayText, "<br/>");

            eventInformation.portalBillingDescription = regex.Replace(oemsEvent.oems_BillingPortalDescription ?? "", "<br/>"); 

            if (oemsEvent.oems_RegistrationDeadline != null)
            {
                DateTime deadLine = RetrieveLocalTimeFromUTCTime((DateTime)oemsEvent.oems_RegistrationDeadline, getTimeZoneCode, localContext.OrganizationService);
                //eventInformation.registrationDeadline = ((DateTime)oemsEvent.oems_RegistrationDeadline).ToString("f");
                eventInformation.registrationDeadline = (deadLine).ToString("f");
            }

            //Support staff owners
            eventInformation.showEventAdminUserList = oemsEvent.GetAttributeValue<bool>("oems_displaysupportstaffowners");
            eventInformation.eventAdminUserList =
                (from a in oemsEvent.oems_event_support_staff_owners
                 select new EventAdminUser
                 {
                     fullName = a.FullName,
                     emailAddress = a.InternalEMailAddress,
                     telephoneNumber = a.Address1_Telephone1,
                     faxeNumber = a.Address1_Fax
                 }
                ).ToList<EventAdminUser>();

            //Executive staff owners
            eventInformation.showEventExecutiveUserList = oemsEvent.GetAttributeValue<bool>("oems_displayexecutivestaffowners");
            eventInformation.eventExecutiveUserList =
                (from a in oemsEvent.oems_event_executive_staff_owners
                 select new EventAdminUser
                 {
                     fullName = a.FullName,
                     emailAddress = a.InternalEMailAddress,
                     telephoneNumber = a.Address1_Telephone1,
                     faxeNumber = a.Address1_Fax
                 }
                ).ToList<EventAdminUser>();

            if (oemsEvent.oems_ApplicationDeadline != null)
	        {
		        DateTime deadLine = RetrieveLocalTimeFromUTCTime((DateTime)oemsEvent.oems_ApplicationDeadline, getTimeZoneCode, localContext.OrganizationService);
                eventInformation.applicationDeadLine = (deadLine).ToString("f");
	        }

            if (oemsEvent.statuscode != null)
            {
                eventInformation.eventStatus = oemsEvent.statuscode;
            }

            eventInformation.courseCode = oemsEvent.GetAttributeValue<string>("oems_coursecode");
            eventInformation.coursePresenters = oemsEvent.GetAttributeValue<string>("oems_presenters");
            //if (oemsEvent.GetAttributeValue<EntityReference>("oems_courseofferingapplication") != null)
            //{
            //    var presenters = (from coa in localContext.OmniContext.CreateQuery("oems_eventregistration")
            //                      where coa.GetAttributeValue<Guid>("oems_eventregistrationid") == oemsEvent.GetAttributeValue<EntityReference>("oems_courseofferingapplication").Id
            //                      select new
            //                      {
            //                          Presenter = coa.GetAttributeValue<string>("oems_firstname") + " " + coa.GetAttributeValue<string>("oems_lastname"),
            //                          CoPresenter1 = coa.GetAttributeValue<string>("oems_copresenter1firstname") + " " + coa.GetAttributeValue<string>("oems_copresenter1lastname"),
            //                          CoPresenter2 = coa.GetAttributeValue<string>("oems_copresenter2firstname") + " " + coa.GetAttributeValue<string>("oems_copresenter2lastname"),
            //                          CoPresenter3 = coa.GetAttributeValue<string>("oems_copresenter3firstname") + " " + coa.GetAttributeValue<string>("oems_copresenter3lastname")
            //                      }).ToList();

            //    if (presenters.Count == 1)
            //    {
            //        var info = presenters.FirstOrDefault();

            //        if (!string.IsNullOrWhiteSpace(info.Presenter))
            //        {
            //            eventInformation.coursePresenters += info.Presenter;
            //        }

            //        if (!string.IsNullOrWhiteSpace(info.CoPresenter1))
            //        {
            //            eventInformation.coursePresenters += " - " + info.CoPresenter1;
            //        }

            //        if (!string.IsNullOrWhiteSpace(info.CoPresenter2))
            //        {
            //            eventInformation.coursePresenters += " - " + info.CoPresenter2;
            //        }

            //        if (!string.IsNullOrWhiteSpace(info.CoPresenter3))
            //        {
            //            eventInformation.coursePresenters += " - " + info.CoPresenter3;
            //        }
            //    }
            //}

            if (oemsEvent.GetAttributeValue<EntityReference>("oems_coursetopic") != null)
            {
                eventInformation.courseTopic = oemsEvent.GetAttributeValue<EntityReference>("oems_coursetopic").Name;
            }

            if (oemsEvent.GetAttributeValue<EntityReference>("oems_courselocation") != null)
            {
                eventInformation.courseLocation = oemsEvent.GetAttributeValue<EntityReference>("oems_courselocation").Name;
            }

            if (oemsEvent.GetAttributeValue<EntityReference>("oems_coursetargetgrades") != null)
            {
                eventInformation.courseTargetGrades = oemsEvent.GetAttributeValue<EntityReference>("oems_coursetargetgrades").Name;
            }

            if (oemsEvent.GetAttributeValue<EntityReference>("oems_coursedateslot") != null)
            {
                var courseDates = (from cd in localContext.OmniContext.CreateQuery("oems_coursedateslots")
                                   where cd.GetAttributeValue<Guid>("oems_coursedateslotsid") == oemsEvent.GetAttributeValue<EntityReference>("oems_coursedateslot").Id
                            select cd).FirstOrDefault();

                eventInformation.courseInitialOferedDate = (courseDates.GetAttributeValue<DateTime>("oems_startdate")).ToString("MMM dd yyyy");
                eventInformation.courseFinalOferedDate = (courseDates.GetAttributeValue<DateTime>("oems_enddate")).ToString("MMM dd yyyy");
            }

            if (oemsEvent.GetAttributeValue<EntityReference>("oems_coursetimeslot") != null)
            {
                var time = (from ct in localContext.OmniContext.CreateQuery("oems_coursetimeslot") 
                            where ((EntityReference)ct["oems_coursetimeslotid"]).Id == oemsEvent.GetAttributeValue<EntityReference>("oems_coursetimeslot").Id
                            select ct).FirstOrDefault();

                eventInformation.courseInitialDailyTime = time.GetAttributeValue<string>("oems_starttime");
                eventInformation.courseFinalDailyTime = time.GetAttributeValue<string>("oems_endtime");
            }

            eventInformation.courseDescription = oemsEvent.GetAttributeValue<string>("oems_coursedescription");

            return eventInformation;
        }

        protected void RetrieveEventInformation(LocalPluginContext localContext, oems_Event oemsEvent)
        {

            var sw = new Stopwatch(); sw.Start();


            EventInformation eventInformation = getEventInformation(localContext, oemsEvent);

            // return the json of the set
            var json = JsonConvert.SerializeObject(eventInformation);

            ((Entity)localContext.PluginExecutionContext.OutputParameters["BusinessEntity"])["oems_retrieveeventinformation"] = json;

            sw.Stop(); Log.DebugFormat("RetrieveEventInformation {0} msec", sw.ElapsedMilliseconds);
        }


        /// <summary>
        /// Retrieves the current users timezone code
        /// </summary>
        /// <param name="service"> IOrganizationService </param>
        /// <returns></returns>
        private int? RetrieveCurrentUsersSettings(IOrganizationService service)
        {
            var currentUserSettings = service.RetrieveMultiple(
                new QueryExpression("usersettings")
                    {
                        ColumnSet = new ColumnSet("timezonecode"),
                        Criteria = new FilterExpression
                            {
                                Conditions =

                                    {
                                        new ConditionExpression("systemuserid", ConditionOperator.EqualUserId)
                                    }
                            }
                    }).Entities[0].ToEntity<Entity>();

            //return time zone code

            return (int?) currentUserSettings.Attributes["timezonecode"];
        }

        /// <summary>
        ///  Retrive the local time from the UTC time.
        /// </summary>
        /// <param name="utcTime">UTC Date time which needs to convert to Local DateTime </param>
        /// <;param name="timeZoneCode">TimeZoneCode </param>
        /// <param name="service">OrganizationService service</param>
        /// <returns></returns>
        private DateTime RetrieveLocalTimeFromUTCTime(DateTime utcTime, int? timeZoneCode, IOrganizationService service)
        {
            if (!timeZoneCode.HasValue)
                return DateTime.Now;

            var request = new LocalTimeFromUtcTimeRequest
            {
                TimeZoneCode = timeZoneCode.Value,
                UtcTime = utcTime.ToUniversalTime()
            };

            var response = (LocalTimeFromUtcTimeResponse)service.Execute(request);
            return response.LocalTime;
        }

    }
}

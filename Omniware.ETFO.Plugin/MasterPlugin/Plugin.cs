﻿using System.Diagnostics;
using Omniware.ETFO.Plugin.Shared;
using log4net;

namespace Omniware.ETFO.Plugin
{
    using System;
    using System.Collections.ObjectModel;
    using System.Globalization;
    using System.Linq;
    using System.ServiceModel;
    using System.Data.SqlClient;
    using Microsoft.Xrm.Sdk;
    using Microsoft.Xrm.Sdk.Query;
    using Xrm;



    public class LocalPluginContext
    {
        internal IServiceProvider ServiceProvider
        {
            get;

            private set;
        }

        internal IOrganizationService OrganizationService
        {
            get;

            private set;
        }

        internal IPluginExecutionContext PluginExecutionContext
        {
            get;

            private set;
        }

        internal ITracingService TracingService
        {
            get;

            private set;
        }

        internal OmniContext OmniContext
        {
            get;

            private set;
        }

        internal Entity Entity
        {
            get;

            private set;
        }

        internal EntityReference EntityReference
        {
            get;

            private set;
        }

        internal ColumnSet ColumnSet
        {
            get;

            private set;
        }

        internal SqlTransaction SqlTransaction
        {
            get;

            private set;
        }

        private LocalPluginContext()
        {
        }

        internal LocalPluginContext(IServiceProvider serviceProvider)
        {
            if (serviceProvider == null)
            {
                throw new ArgumentNullException("serviceProvider");
            }

            // Obtain the execution context service from the service provider.
            this.PluginExecutionContext = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));

            // Obtain the tracing service from the service provider.
            this.TracingService = (ITracingService)serviceProvider.GetService(typeof(ITracingService));

            // Obtain the Organization Service factory service from the service provider
            IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));

            // Use the factory to generate the Organization Service.
            this.OrganizationService = factory.CreateOrganizationService(this.PluginExecutionContext.UserId);

            // Set Omniware Context
            this.OmniContext = new OmniContext(this.OrganizationService);

            // Set Target
            if (this.PluginExecutionContext.InputParameters.Contains("Target"))
                if (this.PluginExecutionContext.InputParameters["Target"] is Entity)
                    this.Entity = (Entity)this.PluginExecutionContext.InputParameters["Target"];
                else if (this.PluginExecutionContext.InputParameters["Target"] is EntityReference)
                    this.EntityReference = (EntityReference)this.PluginExecutionContext.InputParameters["Target"];

            // Set ColumnSet
            if (this.PluginExecutionContext.InputParameters.Contains("ColumnSet"))
                this.ColumnSet = (ColumnSet)this.PluginExecutionContext.InputParameters["ColumnSet"];

            // Set SqlTranaction
            try
            {
                object platformContext = this.PluginExecutionContext.GetType().InvokeMember("PlatformContext", System.Reflection.BindingFlags.GetProperty, null, this.PluginExecutionContext, null);
                this.SqlTransaction = (SqlTransaction)platformContext.GetType().InvokeMember("SqlTransaction", System.Reflection.BindingFlags.GetProperty, null, platformContext, null);
            }
            catch (Exception ex)
            {
                //AS: PlatformContext Does not work in the async mode
            }
        }

        internal void Trace(string message)
        {
            if (string.IsNullOrWhiteSpace(message) || this.TracingService == null)
            {
                return;
            }

            if (this.PluginExecutionContext == null)
            {
                this.TracingService.Trace(message);
            }
            else
            {
                this.TracingService.Trace(
                    "{0}, Correlation Id: {1}, Initiating User: {2}",
                    message,
                    this.PluginExecutionContext.CorrelationId,
                    this.PluginExecutionContext.InitiatingUserId);
            }
        }
    }

    /// <summary>
    /// Base class for all Plugins.
    /// </summary>    
    public class Plugin : IPlugin
    {
        

        private Collection<Tuple<int, string, string, Action<LocalPluginContext>>> registeredEvents;

        

        /// <summary>
        /// Gets the List of events that the plug-in should fire for. Each List
        /// Item is a <see cref="System.Tuple"/> containing the Pipeline Stage, Message and (optionally) the Primary Entity. 
        /// In addition, the fourth parameter provide the delegate to invoke on a matching registration.
        /// </summary>
        protected Collection<Tuple<int, string, string, Action<LocalPluginContext>>> RegisteredEvents
        {
            get
            {
                if (this.registeredEvents == null)
                {
                    this.registeredEvents = new Collection<Tuple<int, string, string, Action<LocalPluginContext>>>();
                }

                return this.registeredEvents;
            }
        }

        /// <summary>
        /// Gets or sets the name of the child class.
        /// </summary>
        /// <value>The name of the child class.</value>
        protected string ChildClassName
        {
            get;

            private set;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Plugin"/> class.
        /// </summary>
        /// <param name="childClassName">The <see cref=" cred="Type"/> of the derived class.</param>
        internal Plugin(Type childClassName)
        {
            this.ChildClassName = childClassName.ToString();
        }

        /// <summary>
        /// Executes the plug-in.
        /// </summary>
        /// <param name="serviceProvider">The service provider.</param>
        /// <remarks>
        /// For improved performance, Microsoft Dynamics CRM caches plug-in instances. 
        /// The plug-in's Execute method should be written to be stateless as the constructor 
        /// is not called for every invocation of the plug-in. Also, multiple system threads 
        /// could execute the plug-in at the same time. All per invocation state information 
        /// is stored in the context. This means that you should not use global variables in plug-ins.
        /// </remarks>
        public void Execute(IServiceProvider serviceProvider)
        {
            var logger = LogHelper.GetLogger(this.GetType()); // initallize late to be able to use the actual implementation class name

            if (serviceProvider == null)
            {
                throw new ArgumentNullException("serviceProvider");
            }

            // Construct the Local plug-in context.
            LocalPluginContext localcontext = new LocalPluginContext(serviceProvider);

            localcontext.Trace(string.Format(CultureInfo.InvariantCulture, "Entered {0}.Execute()", this.ChildClassName));
            //logger.Debug("Executing plugin ");

         //   try
         //   {
                // Iterate over all of the expected registered events to ensure that the plugin
                // has been invoked by an expected event
                // For any given plug-in event at an instance in time, we would expect at most 1 result to match.
            /*
                Action<LocalPluginContext> entityAction =
                    (from a in this.RegisteredEvents
                     where (
                     a.Item1 == localcontext.PluginExecutionContext.Stage &&
                     a.Item2 == localcontext.PluginExecutionContext.MessageName &&
                     (string.IsNullOrWhiteSpace(a.Item3) ? true : a.Item3 == localcontext.PluginExecutionContext.PrimaryEntityName)
                     )
                     select a.Item4).FirstOrDefault();
            */

            var pluginExecutionContext = localcontext.PluginExecutionContext;

            var entityActionQuery =
                 from a in this.RegisteredEvents
                 where (
                 a.Item1 == pluginExecutionContext.Stage &&
                 a.Item2 == pluginExecutionContext.MessageName &&
                 (string.IsNullOrWhiteSpace(a.Item3) ? true : a.Item3 == pluginExecutionContext.PrimaryEntityName)
                 )
                 select a.Item4;
            try
            {
                foreach (Action<LocalPluginContext> entityAction in entityActionQuery)
                {

                    if (entityAction != null)
                    {
                        var message = string.Format("{0} is firing for Entity: {1}, Message: {2}", this.ChildClassName,
                                                    pluginExecutionContext.PrimaryEntityName,
                                                    pluginExecutionContext.MessageName);
                        localcontext.Trace(message);

                        //  this information will be included in each log message from this thread
                        LogicalThreadContext.Properties["ID"] = string.Format("{0} {1} {2}",
                                                                              pluginExecutionContext.MessageName,
                                                                              pluginExecutionContext.PrimaryEntityName,
                                                                              pluginExecutionContext.PrimaryEntityId
                            );
                        //redundant logger.Debug(message);

                        var stopWatch = new Stopwatch();
                        stopWatch.Start();

                        entityAction.Invoke(localcontext);

                        stopWatch.Stop();

                        if (stopWatch.ElapsedMilliseconds == 0)
                        {
                            logger.DebugFormat("PLUGIN (recursive) Executed in {0} msec ", stopWatch.ElapsedMilliseconds);
  
                        } 
                        else if (stopWatch.ElapsedMilliseconds < 2000)
                        {
                            logger.InfoFormat("PLUGIN Executed in {0} msec",stopWatch.ElapsedMilliseconds);
                        }
                        else
                        {
                            logger.WarnFormat("SLOW PLUGIN Executed  in {0} msec", stopWatch.ElapsedMilliseconds);                            
                        }

                        // now exit - if the derived plug-in has incorrectly registered overlapping event registrations,
                        // guard against multiple executions.
                        //return;
                    }
                }
            }
            catch (OEMSPluginException ex)
            {
                logger.Error(ex + ex.StackTrace);
                throw new InvalidPluginExecutionException(ex.Message, ex);
            }
            catch (Exception anyOtherException)
            {
            
                var st = new StackTrace(anyOtherException, true);
                // Get the top stack frame
                var frame = st.GetFrame(0);
                // Get the line number from the stack frame
                var line = frame.GetFileLineNumber();
                logger.Error(anyOtherException + anyOtherException.StackTrace + "The failure started on the line: " + frame.ToString());
                throw new InvalidPluginExecutionException(anyOtherException.Message, anyOtherException);
            }
         /*   }
            catch (FaultException<OrganizationServiceFault> e)
            {
                localcontext.Trace(string.Format(CultureInfo.InvariantCulture, "Exception: {0}", e.ToString()));

                // Handle the exception.
                throw;
            }
            finally
            {
                localcontext.Trace(string.Format(CultureInfo.InvariantCulture, "Exiting {0}.Execute()", this.ChildClassName));
            } */
        }
    }
}
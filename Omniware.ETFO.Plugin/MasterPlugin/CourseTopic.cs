﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Omniware.ETFO.Plugin.DataWork;
using Omniware.ETFO.Plugin.FormComponents;
using Omniware.ETFO.Plugin.Shared;
using Xrm;
using log4net;
using Microsoft.Crm.Sdk.Messages;

namespace Omniware.ETFO.Plugin
{
    public class CourseTopic : Plugin
    {
        private ILog _log = LogHelper.GetLogger(typeof(CourseTopic));

        public CourseTopic()
            : base(typeof(CourseTopic))
        {
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(20, "Update", "oems_coursetopic", new Action<LocalPluginContext>(CopyCourseTopic)));
        }

        protected void CopyCourseTopic(LocalPluginContext localContext)
        {
            if (localContext == null)
                throw new ArgumentNullException("localContext");
            Entity courseTopic = localContext.Entity;

            Entity fullCourseTopic = (from c in localContext.OmniContext.CreateQuery("oems_coursetopic")
                                      where (Guid)c["oems_coursetopicid"] == courseTopic.Id
                                  select c
                                ).FirstOrDefault();

            Entity newCourseTopic = new Entity(courseTopic.LogicalName);

            newCourseTopic.Attributes["oems_courseterm"] = courseTopic.GetAttributeValue<EntityReference>("oems_courseterm");
            newCourseTopic.Attributes["oems_coursetermpresenterapplication"] = courseTopic.GetAttributeValue<EntityReference>("oems_coursetermpresenterapplication");
            newCourseTopic.Attributes["oems_name"] = fullCourseTopic.GetAttributeValue<string>("oems_name");

            try
            {
                localContext.OrganizationService.Create(newCourseTopic);

                localContext.Entity.Attributes.Remove("oems_courseterm");
                localContext.Entity.Attributes.Remove("oems_coursetermpresenterapplication");
            }
            catch (Exception e)
            {
                throw e;
            }
        }

    }
}

﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Omniware.ETFO.Plugin.Shared;
using Xrm;
using Newtonsoft.Json;
using log4net;

namespace Omniware.ETFO.Plugin
{
    public class PortalFormTemplateConfigurator
    {
        ILog Log = LogHelper.GetLogger(typeof(PortalFormTemplateConfigurator));

        private readonly LocalPluginContext localContext;
        private readonly Guid formTemplateId;

        // cache all form elements attached to this template
        private List<oems_PortalFormElement> allTemplateElementEntities;

        // cache all form element properties and their values 
        private List<PropertyWithValue> allPropertiesWithValues;

        public PortalFormTemplateConfigurator(LocalPluginContext localContext, Guid formTemplateId)
        {
            this.localContext = localContext;
            this.formTemplateId = formTemplateId;
            allTemplateElementEntities = (from portalFormElement in localContext.OmniContext.oems_PortalFormElementSet
                                          where portalFormElement.oems_FormTemplate.Id == formTemplateId
                                          select portalFormElement).ToList<oems_PortalFormElement>();
            allPropertiesWithValues =
                (from portalFormElement in localContext.OmniContext.oems_PortalFormElementSet
                 join propertyValue in localContext.OmniContext.oems_FormElementPropertyValueSet on portalFormElement.Id equals propertyValue.oems_FormElement.Id
                 join property in localContext.OmniContext.oems_FormElementPropertySet on propertyValue.oems_FormElementProperty.Id equals property.Id
                 where portalFormElement.oems_FormTemplate.Id == formTemplateId
                 select new PropertyWithValue
                 {
                     PortalFormElementId = portalFormElement.Id,
                     PropertyId = property.oems_FormElementPropertyId,
                     PropertyValueId = propertyValue.oems_FormElementPropertyValueId,
                     Key = property.oems_PropertyCode,
                     Seq = property.oems_SequenceNumber ?? int.MinValue,
                     Value = propertyValue.oems_PropertyValue,
                     ConvertedValue = getConvertedValue(propertyValue.oems_PropertyValue, property.oems_PropertyType ?? 0)
                 }
                    ).ToList();
        }


        private List<Element> getFormElements(Guid? parentFormElementId, Boolean isMaster, Boolean isCopy)
        {
            var sw = new Stopwatch(); sw.Start();
            Log.DebugFormat("Start Function getFormElements");

            List<Element> formElementList = GetAllTemplateElementEntities()
                .Where(el => (el.oems_ParentFormElement == null && parentFormElementId == null)
                             ||
                             (el.oems_ParentFormElement != null && el.oems_ParentFormElement.Id == parentFormElementId))
                .Select( a=>
                new Element
                 {
                     id = a.oems_PortalFormElementId.Value,
                     elementId = !isCopy ? a.oems_PortalFormElementId.Value : Guid.Empty,
                     sourceId = a.oems_SourceFormElement != null ? a.oems_SourceFormElement.Id : Guid.Empty,
                     text = a.oems_FormElementName,
                     @checked = true,
                     index = a.oems_displaysequence ?? 0,
                     expanded = true,
                     spriteCssClass = null,
                     mandatory = a.oems_MandatoryFlag ?? false,
                     properties = getFormElementProperties( a, isMaster),
                     items = getFormElements(a.oems_PortalFormElementId, isMaster, isCopy)
                 }
                ).ToList();

            sw.Stop();
            Log.DebugFormat("getFormElements({0}) count: {1}  {2} msec", formTemplateId, formElementList.Count, sw.ElapsedMilliseconds);
            
            return formElementList;
        }

        // load and cache all oems_PortalFormElement's belonging to this template
        List<oems_PortalFormElement> GetAllTemplateElementEntities()
        {
            
            return allTemplateElementEntities;

            var sw = new Stopwatch(); sw.Start();
            Log.DebugFormat("Start Function GetAllTemplateEntities");
            allTemplateElementEntities = (from portalFormElement in localContext.OmniContext.oems_PortalFormElementSet
                                          where portalFormElement.oems_FormTemplate.Id == formTemplateId
                                          select portalFormElement).ToList<oems_PortalFormElement>();
            sw.Stop();
            Log.DebugFormat("GetAllTemplateEntities({0}) count: {1}  {2} msec", formTemplateId, allTemplateElementEntities.Count, sw.ElapsedMilliseconds);
            
            return allTemplateElementEntities;
        }


        class PropertyWithValue
        {
            public Guid PortalFormElementId { get; set; }
            public Guid? PropertyId { get; set; }
            public Guid? PropertyValueId { get; set; }
            public string Key { get; set; }
            public int? Seq { get; set; }
            public object ConvertedValue { get; set; }  // avoid using it, left for historical reasons
            public string Value { get; set; }
        }

        // load and cache all properties related to this template with their values
        List<PropertyWithValue> GetAllPropertiesWithValues()
        {
            return allPropertiesWithValues;
            var sw = new Stopwatch(); sw.Start();
            allPropertiesWithValues = 
                (   from portalFormElement in localContext.OmniContext.oems_PortalFormElementSet
                    join propertyValue in localContext.OmniContext.oems_FormElementPropertyValueSet on portalFormElement.Id equals propertyValue.oems_FormElement.Id 
                    join property in localContext.OmniContext.oems_FormElementPropertySet on propertyValue.oems_FormElementProperty.Id equals property.Id
                    where portalFormElement.oems_FormTemplate.Id == formTemplateId
                    select new PropertyWithValue
                        {
                            PortalFormElementId = portalFormElement.Id,
                            PropertyId = property.oems_FormElementPropertyId,
                            PropertyValueId = propertyValue.oems_FormElementPropertyValueId,
                            Key = property.oems_PropertyCode,
                            Seq = property.oems_SequenceNumber ?? int.MinValue,
                            Value =  propertyValue.oems_PropertyValue,
                            ConvertedValue =  getConvertedValue(propertyValue.oems_PropertyValue, property.oems_PropertyType ?? 0) 
                        }
                    ).ToList();

            sw.Stop();
            Log.DebugFormat("GetAllPropertiesWithValues({0}) count: {1}  {2} msec", formTemplateId, allPropertiesWithValues.Count, sw.ElapsedMilliseconds);
            
            return allPropertiesWithValues;
        }

        private Object getConvertedValue(String propertyValue, int propertyType)
        {
            if (propertyValue == null)
                return null;

            Object returnValue;

            switch (propertyType)
            {
                case 2:
                    returnValue = Convert.ToBoolean(propertyValue);
                    break;
                case 3:
                    returnValue = Convert.ToDecimal(propertyValue);
                    break;
                default:
                    returnValue = propertyValue;
                    break;
            }

            return returnValue;
        }

        // get list of properties
        private List<Dictionary<string, object>> getFormElementProperties( oems_PortalFormElement oemsFormElement, Boolean isMaster)
        {
            List<Dictionary<string, object>> formElementPropertyList = new List<Dictionary<string, object>>();


            var propertiesWithValues = GetAllPropertiesWithValues()
                .Where(pv => pv.PortalFormElementId == oemsFormElement.Id)
                //remove possible multiple values .. only one value per property, sort
                .GroupBy(pv => pv.Key)
                .ToDictionary(g => g.Key, g => g.First())
                .Values
                .OrderBy(pv => pv.Seq)
                .ToList();

            foreach (var item in propertiesWithValues)
            {
                Dictionary<string, object> property = new Dictionary<string, object>();
                Object value = item.ConvertedValue;

                // if isMaster then get full options for personal accommodaiton and attending as
                if (item.Key == ElementProperty.PersonalAccommodation)
                {
                    // get personal accommodation options
                    if (isMaster)
                    {
                        // convert value to object
                        List<PropertyConfiguration> itemList = JsonConvert.DeserializeObject<List<PropertyConfiguration>>(item.ConvertedValue.ToString());
                        List<PropertyConfiguration> masterList = getMasterPersonalAccommodationList(localContext, null);

                        value = getMergerdPropertyConfiguration(masterList, itemList);
                    }
                }
                else if (item.Key == ElementProperty.AttendingAs)
                {
                    // get personal accommodation options
                    if (isMaster)
                    {
                        // convert value to object
                        List<PropertyConfiguration> itemList = JsonConvert.DeserializeObject<List<PropertyConfiguration>>(item.ConvertedValue.ToString());
                        List<PropertyConfiguration> masterList = getMasterAttendingAsList(localContext);

                        value = getMergerdPropertyConfiguration(masterList, itemList);
                    }
                }

                property.Add(item.Key, value);
                formElementPropertyList.Add(property);
            }

            return formElementPropertyList;
        }

        private List<PropertyConfiguration> getMasterPersonalAccommodationList(LocalPluginContext localContext, Guid? parentOptionId)
        {
            List<PropertyConfiguration> personalAccommodationList =
                (from a in localContext.OmniContext.oems_PersonalAccommodationOptionSet
                 where a.oems_ParentOption.Id == parentOptionId
                 select new PropertyConfiguration
                 {
                     id = a.oems_PersonalAccommodationOptionId.Value,
                     optionId = a.oems_PersonalAccommodationOptionId.Value,
                     text = a.oems_OptionName,
                     @checked = false,
                     expanded = true,
                     items = getMasterPersonalAccommodationList(localContext, a.oems_PersonalAccommodationOptionId.Value)
                 }
                ).ToList<PropertyConfiguration>();

            if (personalAccommodationList.Count == 0)
                return null;

            setHasChildren(ref personalAccommodationList);

            return personalAccommodationList;
        }

        private List<PropertyConfiguration> setHasChildren(ref List<PropertyConfiguration> propertyConfigurationList)
        {
            foreach (var propertyConfiguration in propertyConfigurationList)
            {
                if (propertyConfiguration.items != null)
                {
                    propertyConfiguration.hasChildren = true;
                    List<PropertyConfiguration> children = propertyConfiguration.items;
                    setHasChildren(ref children);
                }
                else
                {
                    propertyConfiguration.hasChildren = false;
                }
            }

            return propertyConfigurationList;
        }

        private List<PropertyConfiguration> getMasterAttendingAsList(LocalPluginContext localContext)
        {
            PropertyConfiguration votingRoles = new PropertyConfiguration
            {
                id = new Guid("00000000-0000-0000-0000-000000000001"),
                optionId = new Guid("00000000-0000-0000-0000-000000000001"),
                text = "Voting Role",
                @checked = false,
                hasChildren = true,
                expanded = true,
                items = (from a in localContext.OmniContext.oems_attendingasoptionSet
                         where a.oems_VotingRoleFlag == true
                         select new PropertyConfiguration
                         {
                             id = a.oems_attendingasoptionId.Value,
                             optionId = a.oems_attendingasoptionId.Value,
                             text = a.oems_OptionName,
                             @checked = false,
                             hasChildren = false,
                             expanded = true,
                             items = null
                         }
                        ).ToList<PropertyConfiguration>()
            };

            PropertyConfiguration nonVotingRoles = new PropertyConfiguration
            {
                id = new Guid("00000000-0000-0000-0000-000000000002"),
                optionId = new Guid("00000000-0000-0000-0000-000000000002"),
                text = "Non-Voting Role",
                @checked = false,
                hasChildren = true,
                expanded = true,
                items = (from a in localContext.OmniContext.oems_attendingasoptionSet
                         where a.oems_VotingRoleFlag != true
                         select new PropertyConfiguration
                         {
                             id = a.oems_attendingasoptionId.Value,
                             optionId = a.oems_attendingasoptionId.Value,
                             text = a.oems_OptionName,
                             @checked = false,
                             hasChildren = false,
                             expanded = true,
                             items = null
                         }
                        ).ToList<PropertyConfiguration>()
            };

            List<PropertyConfiguration> attendingAsList = new List<PropertyConfiguration>();
            attendingAsList.Add(votingRoles);
            attendingAsList.Add(nonVotingRoles);

            return attendingAsList;
        }

        private List<Element> getOrderedList(List<Element> formElementList)
        {
            List<Element> orderedList = formElementList.OrderBy(a => a.index).ToList();

            var i = 1;

            // resort children
            foreach (var formElement in orderedList)
            {
                formElement.index = i++;
                if (formElement.items != null)
                    formElement.items = getOrderedList(formElement.items);
            }

            return orderedList;
        }

        private List<Element> getMergedFormElements(List<Element> formElementList1, List<Element> formElementList2)
        {
            if (formElementList1 == null)
                formElementList1 = new List<Element>();

            if (formElementList2 == null)
                formElementList2 = new List<Element>();

            List<Element> mergeList =
                (from a in formElementList1
                 join b in formElementList2 on a.id equals b.sourceId into c
                 from d in c.DefaultIfEmpty()
                 select new Element
                 {
                     id = a.id,
                     elementId = (d != null ? d.id : Guid.Empty),
                     sourceId = a.id,
                     text = (d != null ? d.text : a.text),
                     @checked = (a.mandatory != true ? (formElementList2.Count == 0 ? false : (d != null ? d.@checked : false)) : true), 
                     index = (d != null ? d.index : a.index),
                     expanded = true, //(d != null ? d.expanded : a.expanded),
                     spriteCssClass = (d != null ? d.spriteCssClass : a.spriteCssClass),
                     mandatory = (d != null ? d.mandatory : a.mandatory),
                     properties = (d != null ? getMergedProperties(a.properties, d.properties) : a.properties),
                     items = getMergedFormElements(a.items, d != null ? d.items : null)
                 }).ToList<Element>();

            List<Element> customItemList =
                (from a in formElementList2
                 where a.sourceId == Guid.Empty
                 select a).ToList<Element>();

            List<Element> fullMergeList = (mergeList.Concat(customItemList)).ToList<Element>();

            return fullMergeList;
        }

        private List<Element> getMergedFormElementsFirstTime(List<Element> formElementList1, List<Element> formElementList2)
        {
            if (formElementList1 == null)
                formElementList1 = new List<Element>();

            if (formElementList2 == null)
                formElementList2 = new List<Element>();

            List<Element> mergeList =
                (from a in formElementList1
                 join b in formElementList2 on a.id equals b.sourceId into c
                 from d in c.DefaultIfEmpty()
                 select new Element
                 {
                     id = a.id,
                     elementId = (d != null ? d.id : Guid.Empty),
                     sourceId = a.id,
                     text = (d != null ? d.text : a.text),
                     @checked = (a.mandatory != true ? (a != null ? a.@checked : false) : true),
                     index = (d != null ? d.index : a.index),
                     expanded = true, //(d != null ? d.expanded : a.expanded),
                     spriteCssClass = (d != null ? d.spriteCssClass : a.spriteCssClass),
                     mandatory = (d != null ? d.mandatory : a.mandatory),
                     properties = (d != null ? getMergedProperties(a.properties, d.properties) : a.properties),
                     items = getMergedFormElementsFirstTime(a.items, d != null ? d.items : null)
                 }).ToList<Element>();

            List<Element> customItemList =
                (from a in formElementList2
                 where a.sourceId == Guid.Empty
                 select a).ToList<Element>();

            List<Element> fullMergeList = (mergeList.Concat(customItemList)).ToList<Element>();

            return fullMergeList;
        }

        private List<Dictionary<string, object>> getMergedProperties(List<Dictionary<string, object>> propertyList1, List<Dictionary<string, object>> propertyList2)
        {
            if (propertyList1 == null)
                propertyList1 = new List<Dictionary<string, object>>();

            if (propertyList2 == null)
                propertyList2 = new List<Dictionary<string, object>>();

            List<Dictionary<string, object>> mergedPropertyList =
                (from a in propertyList1
                 join b in propertyList2 on a.First().Key equals b.First().Key into c
                 from d in c.DefaultIfEmpty()
                 select (d != null ? 
                        (d.First().Key != ElementProperty.PersonalAccommodation && d.First().Key != ElementProperty.AttendingAs ?
                         d : getMergerdPropertyConfiguration(a, d)) 
                        : a)
                ).ToList<Dictionary<string, object>>();

            return mergedPropertyList;
        }

        private Dictionary<string, object> getMergerdPropertyConfiguration(Dictionary<string, object> property1, Dictionary<string, object> property2)
        {
            List<PropertyConfiguration> propertyConfiguration1 = JsonConvert.DeserializeObject<List<PropertyConfiguration>>(property1.First().Value.ToString());
            List<PropertyConfiguration> propertyConfiguration2 = JsonConvert.DeserializeObject<List<PropertyConfiguration>>(property2.First().Value.ToString());

            List<PropertyConfiguration> mergedPropertyConfiguration = getMergerdPropertyConfiguration(propertyConfiguration1, propertyConfiguration2);

            Dictionary<string, object> returnData = new Dictionary<string, object>();
            returnData.Add(property1.First().Key, mergedPropertyConfiguration);

            return returnData;        
        }

        private List<PropertyConfiguration> getMergerdPropertyConfiguration(List<PropertyConfiguration> property1, List<PropertyConfiguration> property2)
        {
            if (property1 == null)
                property1 = new List<PropertyConfiguration>();

            if (property2 == null)
                property2 = new List<PropertyConfiguration>();

            List<PropertyConfiguration> mergedPropertyConfiguration =
                (from a in property1
                 join b in property2 on a.optionId equals b.optionId into c
                 from d in c.DefaultIfEmpty()
                 select new PropertyConfiguration
                 {
                     id = a.optionId,
                     optionId = a.optionId,
                     text = (d != null ? d.text : a.text),
                     @checked = (d != null ? d.@checked : false),
                     hasChildren = (d != null ? d.hasChildren : a.hasChildren),
                     expanded = (d != null ? d.expanded : a.expanded),
                     items = getMergerdPropertyConfiguration(a.items, d != null ? d.items : null)
                 }
                ).ToList<PropertyConfiguration>();

            return mergedPropertyConfiguration;
        }

        public String GetFormTemplateData( )
        {
            // get form template
            Template formTemplate
                = (from a in localContext.OmniContext.oems_portalformtemplateSet
                   where a.oems_portalformtemplateId == formTemplateId
                   select new Template 
                     {
                         id = a.oems_portalformtemplateId.Value, 
                         sourceId = a.oems_SourceFormTemplate != null ? a.oems_SourceFormTemplate.Id : Guid.Empty  
                     }).FirstOrDefault();

            List<Element> outputFormElementList;

            // get source form template and merge
            if (formTemplate.sourceId != Guid.Empty)
            {
                var sourceFormTemplateConfigurator = new PortalFormTemplateConfigurator(localContext, formTemplate.sourceId);
                List<Element> sourceFormElementList = sourceFormTemplateConfigurator.getFormElements( null, false, false);
                List<Element> formElementList =       this.getFormElements( null, false, false);

                // merge
                List<Element> mergedFormElementList = null;
                if (localContext.PluginExecutionContext.MessageName == "Create")
                {
                    mergedFormElementList = getMergedFormElementsFirstTime(sourceFormElementList, formElementList);
                }
                else 
                {
                    mergedFormElementList = getMergedFormElements(sourceFormElementList, formElementList);
                }

                outputFormElementList = mergedFormElementList;
            }
            else
            {
                List<Element> formElementList = getFormElements(  null, true, false);

                outputFormElementList = formElementList;
            }

            // resequence index
            List<Element> retrieveData = getOrderedList(outputFormElementList);

            var json = JsonConvert.SerializeObject(retrieveData);

            return json;

        }

        private void create(Element formElement, EntityReference formTemplate, EntityReference parentFormElement, int indx)
        {
            // create form element for form template
            oems_PortalFormElement oemsFormElement;

            if (formElement.sourceId != Guid.Empty)
            {
                // get source form element
                oems_PortalFormElement sourceFormElement =
                    (from a in localContext.OmniContext.oems_PortalFormElementSet
                     where a.oems_PortalFormElementId == formElement.sourceId
                     select a).FirstOrDefault();

                if (sourceFormElement == null)
                    return;

                oemsFormElement = new oems_PortalFormElement
                     {
                         oems_FormElementName = formElement.text,
                         oems_FormElementType = sourceFormElement.oems_FormElementType,
                         oems_FormTemplate = formTemplate,
                         oems_ParentFormElement = parentFormElement,
                         oems_SourceFormElement = sourceFormElement.ToEntityReference(),
                         oems_displaysequence = indx,
                         oems_DataBinding = sourceFormElement.oems_DataBinding,
                         oems_FormElementCode = sourceFormElement.oems_FormElementCode,
                         oems_ExpandingFormElementCode = sourceFormElement.oems_ExpandingFormElementCode,
                         oems_FormBuilderHiddenFlag = sourceFormElement.oems_FormBuilderHiddenFlag,
                         oems_MandatoryFlag = formElement.mandatory,
                         oems_SystemDefinedFlag = sourceFormElement.oems_SystemDefinedFlag
                     };
            }
            else
            {
                // this should only be for HTML types
                oems_Configuration configuration = 
                      (from c in localContext.OmniContext.oems_ConfigurationSet
                       where c.statecode == 0
                       select c).FirstOrDefault();

                oemsFormElement = new oems_PortalFormElement
                     {
                         oems_FormElementName = formElement.text,
                         oems_FormElementType = configuration.oems_HTMLFormElementType,
                         oems_FormTemplate = formTemplate,
                         oems_ParentFormElement = parentFormElement,
                         oems_displaysequence = indx,
                         oems_MandatoryFlag = formElement.mandatory,
                         oems_SystemDefinedFlag = false
                     };
            }


            // create record
            oemsFormElement.oems_PortalFormElementId = localContext.OmniContext.Create(oemsFormElement);

            // set properties
            foreach (var property in formElement.properties)
            {
                String key = property.First().Key;
                String value = property.First().Value != null ? property.First().Value.ToString() : null;

                oems_FormElementProperty oemsFormElementProperty =
                     (from a in localContext.OmniContext.oems_FormElementPropertySet
                      where a.oems_PropertyCode == key
                      select a).FirstOrDefault();

                oems_FormElementPropertyValue oemsFormElementPropertyValue = new oems_FormElementPropertyValue
                {
                    oems_FormElement = oemsFormElement.ToEntityReference(),
                    oems_FormElementProperty = oemsFormElementProperty.ToEntityReference(),
                    oems_PropertyName = oemsFormElementProperty.oems_PropertyName,
                    oems_PropertyValue = value
                };

                // create record
                localContext.OmniContext.Create(oemsFormElementPropertyValue);
            }


            processFormElementList( formElement.items, formTemplate, oemsFormElement.ToEntityReference());
        }

        private void update( Element formElement, EntityReference formTemplate, int indx)
        {

            oems_PortalFormElement oemsFormElement = new oems_PortalFormElement
                {
                    Id = formElement.elementId.Value,
                    oems_FormElementName = formElement.text,
                    oems_displaysequence = indx
                };

            var existingElement = GetAllTemplateElementEntities().First(el => el.Id == formElement.elementId);

            if (existingElement.oems_FormElementName != formElement.text || existingElement.oems_displaysequence != indx)
            {
                localContext.OmniContext.Update(oemsFormElement);
                Log.DebugFormat("Updating form element {0} {1} {2}", formElement.elementId.Value, formElement.text,
                                formElement.index);
            }


            // update properties
            foreach (var propertyArray in formElement.properties)
            {
                var property = propertyArray.First(); // why it was done like this ?

                string key = property.Key;
                string value = property.Value != null ? property.Value.ToString() : null;


                // find exising property value if exists
                var existingPropertyValue = GetAllPropertiesWithValues()
                    .FirstOrDefault(pv => pv.PortalFormElementId == formElement.elementId && pv.Key == property.Key);


                if (existingPropertyValue != null)
                {
                    if (existingPropertyValue.Value != value) // only update if it changed
                    {
                        var updatedFormElementPropertyValue = new oems_FormElementPropertyValue
                            {
                                Id = (Guid) existingPropertyValue.PropertyValueId,
                                oems_PropertyValue = value
                            };
                        localContext.OmniContext.Attach(updatedFormElementPropertyValue);
                        localContext.OmniContext.UpdateObject(updatedFormElementPropertyValue);
                        Log.DebugFormat("Updating {0} {1}={2} ", " ", existingPropertyValue.Key, value);
                    }
                }
                else // create new
                {
                    oems_FormElementProperty oemsFormElementProperty =
                        (from a in localContext.OmniContext.oems_FormElementPropertySet
                         where a.oems_PropertyCode == key
                         select a).FirstOrDefault();

                    if (oemsFormElementProperty == null) return;

                    var createdFormElementPropertyValue = new oems_FormElementPropertyValue
                        {
                            oems_FormElement = oemsFormElement.ToEntityReference(),
                            oems_FormElementProperty = oemsFormElementProperty.ToEntityReference(),
                            oems_PropertyName = oemsFormElementProperty.oems_PropertyName,
                            oems_PropertyValue = value
                        };

                    // create record
                    localContext.OmniContext.Create(createdFormElementPropertyValue);
                    Log.DebugFormat("Creating {0} {1}={2} ", oemsFormElementProperty.oems_PropertyName, key, value);
                }
            }

            processFormElementList(formElement.items, formTemplate, oemsFormElement.ToEntityReference());
 
        }

        private void delete( Element formElement)
        {
            // delete form element for form template, CRM will do cascade delete of children
            if (formElement.elementId != Guid.Empty)
                localContext.OmniContext.Delete(oems_PortalFormElement.EntityLogicalName, formElement.elementId.Value);

            if (formElement.items != null)
                foreach (var childElement in formElement.items)
                {
                    delete( childElement);
                }
        }

        private void processFormElementList( List<Element> formElementList, EntityReference formTemplate, EntityReference parentFormElement)
        {
            if (formElementList == null)
                return;

            int i = 0;

            foreach (var formElement in formElementList)
            {
                if (formElement.elementId == null)
                    formElement.elementId = Guid.Empty;

                if (formElement.sourceId == null)
                    formElement.sourceId = Guid.Empty;

                // check if any children have checked then parent is also checked
                if (formElement.items != null)
                {
                    int count = formElement.items.Count(a => a.@checked == true);
                    if (count > 0)
                        formElement.@checked = true;
                }

                // NEW
                if (formElement.@checked == true && formElement.elementId == Guid.Empty)
                    create( formElement, formTemplate, parentFormElement, i++);

                // DELETE
                if (!formElement.@checked == true && formElement.elementId != Guid.Empty)
                    delete( formElement);

                // UPDATE
                if (formElement.@checked == true && formElement.elementId != Guid.Empty)
                    update( formElement, formTemplate,  i++);

            }
        }

        public void UpdateFormTemplate( Entity oemsPortalFormTemplate)
        {
            // convert data from json to object
            List<Element> formElementList = JsonConvert.DeserializeObject<List<Element>>((String)oemsPortalFormTemplate["oems_updatedata"]);

            processFormElementList( formElementList, oemsPortalFormTemplate.ToEntityReference(), null);

        }


        // copy form elements
        public String CopyFormTemplate( ref oems_portalformtemplate oemsPortalFormTemplate)
        {
            // get copied form templated
            oems_portalformtemplate copiedTemplate = localContext.OmniContext.Retrieve(oems_portalformtemplate.EntityLogicalName, oemsPortalFormTemplate.oems_CopiedFormTemplate.Id, new ColumnSet("oems_sourceformtemplate")).ToEntity<oems_portalformtemplate>();
            
            // set source on template to original
            oemsPortalFormTemplate.oems_SourceFormTemplate = copiedTemplate.oems_SourceFormTemplate;

            var copiedFromTemplateConfigurator = new PortalFormTemplateConfigurator(localContext, oemsPortalFormTemplate.oems_CopiedFormTemplate.Id);
            List<Element> formElementList = copiedFromTemplateConfigurator.getFormElements( null, false, true);

            var json = JsonConvert.SerializeObject(formElementList);

            return json;
        }


    }

}

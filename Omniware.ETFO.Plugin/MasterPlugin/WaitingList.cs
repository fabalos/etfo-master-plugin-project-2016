﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Omniware.ETFO.Plugin.DataWork;
using Omniware.ETFO.Plugin.FormComponents;
using Omniware.ETFO.Plugin.Shared;
using Xrm;
using log4net;
using Microsoft.Crm.Sdk.Messages;
using Omniware.ETFO.Plugin.Billing;

namespace Omniware.ETFO.Plugin
{
    public class WaitingList : Plugin
    {
        protected string Config;
        protected string SecConfig;
        private ILog _log = LogHelper.GetLogger(typeof (WaitingList));

        public WaitingList(string unsecureConfig, string secureConfig)
            : base(typeof (WaitingList))
        {
            if (unsecureConfig != null)
            {
                Config = unsecureConfig;
            }

            if (secureConfig != null)
            {
                SecConfig = secureConfig;
            }

            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "Update", "oems_eventregistration", new Action<LocalPluginContext>(RecalculateEventCapacityStatus)));
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "Update", "oems_waitinglistrequest", new Action<LocalPluginContext>(RecalculateEventCapacityStatus)));
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "Create", "oems_waitinglistrequest", new Action<LocalPluginContext>(RecalculateEventCapacityStatus)));
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "Delete", "oems_waitinglistrequest", new Action<LocalPluginContext>(RecalculateEventCapacityStatus)));
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "Update", "oems_billingrequest", new Action<LocalPluginContext>(RecalculateEventCapacityStatus)));
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "Update", "oems_event", new Action<LocalPluginContext>(RecalculateEventCapacityStatus)));
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "Create", "oems_waitinglistrequest", new Action<LocalPluginContext>(SetAttendeeRegistrationListFlag)));
        }

        private void SetAttendeeRegistrationListFlag(LocalPluginContext localContext)
        {
            if (localContext == null)
                throw new ArgumentNullException("localContext");

            Entity waitingReq = localContext.Entity;
            EntityReference reg = waitingReq.GetAttributeValue<EntityReference>("oems_eventregistration");

            Entity registration = new Entity(reg.LogicalName);
            registration.Id = reg.Id;
            registration["oems_attendeeregistrationlistflag"] = false;
            registration["oems_waitingliststatus"] = waitingReq.GetAttributeValue<OptionSetValue>("statuscode");

            try
            {
                localContext.OrganizationService.Update(registration);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        protected void RecalculateEventCapacityStatus(LocalPluginContext localContext)
        {
            if (localContext == null)
                throw new ArgumentNullException("localContext");

            //Stop recurrsive calls
            if (localContext.PluginExecutionContext.Depth != 1)
                return;

            var sw = new Stopwatch(); 
            sw.Start();

            //Event registration, waiting list request or billing request, all of them have the same oems_event property
            var entity =
                (localContext.Entity != null && localContext.Entity.LogicalName.Equals("oems_event", StringComparison.InvariantCultureIgnoreCase))
                ? localContext.Entity 
                : 
                (
                    localContext.PluginExecutionContext.MessageName.Equals("Delete", StringComparison.InvariantCultureIgnoreCase) 
                    ? localContext.PluginExecutionContext.PreEntityImages["PreImage"] 
                    : localContext.PluginExecutionContext.PostEntityImages["PostImage"]
                );

            var eventId = localContext.Entity == null || !localContext.Entity.LogicalName.Equals("oems_event", StringComparison.InvariantCultureIgnoreCase) ? entity.GetAttributeValue<EntityReference>("oems_event").Id : entity.GetAttributeValue<Guid>("oems_eventid");

            var evt =
            (
                from e in localContext.OmniContext.CreateQuery("oems_event")
                where (Guid)e["oems_eventid"] == eventId
                select e
            ).Single();

            //Check if event has waiting list component
            var waitingListComponent = evt.GetAttributeValue<bool>("oems_waitinglistcomponentflag");
            if (!waitingListComponent)
            {
                return;
            }
            _log.DebugFormat("Event '{0}' has waiting list component ", evt.GetAttributeValue<string>("oems_eventname"));

            //Event capacity, waiting list size, waiting list full
            var registrationMaximum = evt.GetAttributeValue<int>("oems_registrationmaximum");
            var waitingListSize = (int)evt.GetAttributeValue<decimal>("oems_waitinglistsize");
            var wasWaitingListFull = evt.GetAttributeValue<bool>("oems_waitinglistfullflag");

            //Get the number of registrations with status SUBMITTED or APPROVED or CREATED with CC PENDING
            // + SUBMITTED or APPROVED
            var fetch =
            @" 
                <fetch distinct='false' mapping='logical' aggregate='true'> 
                    <entity name='oems_eventregistration'> 
                        <attribute name='oems_eventregistrationid' alias='count' aggregate='count'/>
                        <filter type='and'>
                            <condition attribute='oems_event' operator='eq' value='{0}' />
                            <filter type='or'>
                                <condition attribute='statuscode' operator='eq' value='{1}' />
                                <condition attribute='statuscode' operator='eq' value='{2}' />
                            </filter>
                        </filter>
                    </entity>
                </fetch>
            ";
            fetch = String.Format(fetch, eventId, EventRegistrationStatusReason.Submitted, EventRegistrationStatusReason.Approved);
            var result = localContext.OmniContext.RetrieveMultiple(new FetchExpression(fetch));
            var totalSubmittedOrApproved = result.Entities.Any() ? (int)((AliasedValue)result.Entities.Single()["count"]).Value : 0;
           _log.DebugFormat("totalSubmittedOrApproved = {0}", totalSubmittedOrApproved);

            // + CREATED with CC PENDING
            fetch =
            @" 
                <fetch distinct='false' mapping='logical' aggregate='true'> 
                    <entity name='oems_eventregistration'> 
                        <attribute name='oems_eventregistrationid' alias='count' aggregate='count'/> 
                        <filter type='and'>
                            <condition attribute='oems_event' operator='eq' value='{0}' />
                            <condition attribute='statuscode' operator='eq' value='{1}' />
                        </filter>
                        <link-entity name='oems_billingrequest' from='oems_eventregistration' to='oems_eventregistrationid'>
			                <filter type='and'>
					            <condition attribute='oems_paymentstatus' operator='eq' value='{2}' />
					            <condition attribute='oems_paymentmethod' operator='eq' value='{3}' />
				            </filter>
		                </link-entity>
                    </entity>
                </fetch>
            ";
            fetch = String.Format(fetch, eventId, EventRegistrationStatusReason.Created, PaymentStatus.CreditCardPending, PaymentMethod.CreditCard);
            result = localContext.OmniContext.RetrieveMultiple(new FetchExpression(fetch));
            var totalCreditCardPending = result.Entities.Any() ? (int)((AliasedValue)result.Entities.Single()["count"]).Value : 0;
            _log.DebugFormat("totalCreditCardPending = {0}", totalCreditCardPending);

            //Total people on the waiting list
            fetch =
            @" 
                <fetch distinct='false' mapping='logical' aggregate='true'> 
                    <entity name='oems_waitinglistrequest'> 
                        <attribute name='oems_waitinglistrequestid' alias='count' aggregate='count'/> 
                        <filter type='and'>
                            <condition attribute='oems_event' operator='eq' value='{0}' />
                        </filter>
                    </entity>
                </fetch>
            ";
            fetch = String.Format(fetch, eventId);
            result = localContext.OmniContext.RetrieveMultiple(new FetchExpression(fetch));
            var totalPeopleWaitingList = result.Entities.Any() ? (int)((AliasedValue)result.Entities.Single()["count"]).Value : 0;
            _log.DebugFormat("totalPeopleWaitingList = {0}", totalPeopleWaitingList);

            //Total people waiting on the waiting list
            fetch =
            @" 
                <fetch distinct='false' mapping='logical' aggregate='true'> 
                    <entity name='oems_waitinglistrequest'> 
                        <attribute name='oems_waitinglistrequestid' alias='count' aggregate='count'/> 
                        <filter type='and'>
                            <condition attribute='oems_event' operator='eq' value='{0}' />
                            <filter type='or'>
                                <condition attribute='statuscode' operator='eq' value='{1}' />
                                <condition attribute='statuscode' operator='eq' value='{2}' />
                            </filter>
                        </filter>
                    </entity>
                </fetch>
            ";
            fetch = String.Format(fetch, eventId, WaitingListStatus.Waiting, WaitingListStatus.Offered);
            result = localContext.OmniContext.RetrieveMultiple(new FetchExpression(fetch));
            var totalPeopleWaitingListWaiting = result.Entities.Any() ? (int)((AliasedValue)result.Entities.Single()["count"]).Value : 0;
            _log.DebugFormat("totalPeopleWaitingListWaiting = {0}", totalPeopleWaitingListWaiting);

            //Is registration full?
            var isRegistrationFull = totalSubmittedOrApproved + totalCreditCardPending >= registrationMaximum;

            //Update event values
            evt.SetAttributeValue<decimal>("oems_waitinglisttotalcount", (decimal)totalPeopleWaitingList);
            evt.SetAttributeValue<decimal>("oems_waitinglistwaitingcount", (decimal)totalPeopleWaitingListWaiting);
            evt.SetAttributeValue<bool>("oems_eventfullflag", isRegistrationFull);

            SetStateRequest state = null;
            var waitingListFull = totalPeopleWaitingListWaiting >= waitingListSize;
            var eventStatusChanged = false;
            var eventStatus = evt.GetAttributeValue<int>("statuscode");
            if (waitingListFull)
            {
                evt.SetAttributeValue<bool>("oems_waitinglistfullflag", true);
                if (eventStatus == (int)EventStatusReason.OPEN_REGISTRATION)
                {
                    state = new SetStateRequest
                    {
                        State = new OptionSetValue((int) CommonStatus.ACTIVE),
                        Status = new OptionSetValue((int)EventStatusReason.CLOSE_REGISTRATION),
                        EntityMoniker = evt.ToEntityReference()
                    };

                    eventStatusChanged = true;
                    eventStatus = (int)EventStatusReason.CLOSE_REGISTRATION;
                }
            }
            else
            {
                evt.SetAttributeValue<bool>("oems_waitinglistfullflag", false);

                //If was full before
                if (wasWaitingListFull && IsRegistrationOpen(evt) && eventStatus == (int)EventStatusReason.CLOSE_REGISTRATION)
                {
                    state = new SetStateRequest
                    {
                        State = new OptionSetValue((int)CommonStatus.ACTIVE),
                        Status = new OptionSetValue((int)EventStatusReason.OPEN_REGISTRATION),
                        EntityMoniker = evt.ToEntityReference()
                    };

                    eventStatusChanged = true;
                    eventStatus = (int)EventStatusReason.OPEN_REGISTRATION;
                }
            }

            //Save changes
            localContext.OmniContext.UpdateObject(evt);        
            localContext.OmniContext.SaveChanges();
            if (state != null)
            {
                localContext.OmniContext.Execute(state);
            }

            SetStatusDates(localContext);

            sw.Stop();

            //entity name, message name and total plugin time are logged automatically
            _log.InfoFormat( "RecalculateEventCapacityStatus: Waiting list total count: {0}, Waiting count: {1}, Event full: {2}, Waiting list full: {3} ",
                     totalPeopleWaitingList, totalPeopleWaitingListWaiting, isRegistrationFull, waitingListFull);

            _log.InfoFormat("RecalculateEventCapacityStatus: Event status {0} {1} , elapsed {2} ms",
                eventStatusChanged ? "Changed" : "Unchanged",
                ((EventStatusReason)eventStatus).ToString(), sw.ElapsedMilliseconds
            );
        }

        private void SetStatusDates(LocalPluginContext localContext)
        {
            if (localContext.Entity.LogicalName == "oems_waitinglistrequest" && localContext.PluginExecutionContext.MessageName == "Update")
            {
                Entity wlRequest = new Entity();
                wlRequest.Id = localContext.Entity.Id;
                wlRequest.LogicalName = localContext.Entity.LogicalName;

                Entity wait = localContext.OmniContext.Retrieve("oems_waitinglistrequest", wlRequest.Id,new ColumnSet(true));
                Entity oems_event = localContext.OmniContext.Retrieve("oems_event", ((EntityReference)wait.Attributes["oems_event"]).Id, new ColumnSet(true));

                int wlStatus = localContext.Entity.GetAttributeValue<OptionSetValue>("statuscode").Value;

                if (WaitingListStatus.Accepted == wlStatus)
                {
                    wlRequest["oems_accepteddate"] = DateTime.UtcNow;
                }
                else if (WaitingListStatus.Declined == wlStatus)
                    {
                        wlRequest["oems_declineddate"] = DateTime.UtcNow;
                    }
                    else if (WaitingListStatus.Expired == wlStatus)
                        {
                            wlRequest["oems_expireddate"] = DateTime.UtcNow;
                        }
                        else if (WaitingListStatus.Removed == wlStatus)
                            {
                                wlRequest["oems_removeddate"] = DateTime.UtcNow;
                            }
                            else if (WaitingListStatus.Offered == wlStatus)
                                {
                                    wlRequest["oems_offerdate"] = DateTime.UtcNow;
                                    wlRequest["oems_targetexpirydate"] =
                                        DateTime.UtcNow.AddHours(Convert.ToDouble(oems_event.GetAttributeValue<decimal>("oems_waitinglistofferperiodhours")));
                                }
                                else if (WaitingListStatus.Cancelled == wlStatus)
                                    {
                                        wlRequest["oems_cancelleddate"] = DateTime.UtcNow;
                                    }

                try 
	            {
                    localContext.OrganizationService.Update(wlRequest);
                    SaveWaitingListStatusReg(localContext);
                    UpdateEventRegistrationStatus(localContext, wlStatus);
	            }
	            catch (Exception e)
	            {
		            throw e;
	            }
            }
        }

        private void SaveWaitingListStatusReg(LocalPluginContext localContext)
        {
            Entity entity = localContext.Entity;

            Entity waiting = localContext.OrganizationService.Retrieve(entity.LogicalName,entity.Id,new ColumnSet("oems_eventregistration"));

            EntityReference reg = waiting.GetAttributeValue<EntityReference>("oems_eventregistration");

            Entity registration = new Entity(reg.LogicalName);
            registration.Id = reg.Id;
            registration["oems_waitingliststatus"] = entity.GetAttributeValue<OptionSetValue>("statuscode");

            try
            {
                localContext.OrganizationService.Update(registration);
            }
            catch (Exception e)
            {                
                throw e;
            }
        }

        private void UpdateEventRegistrationStatus(LocalPluginContext localContext, int status)
        {
            Entity entity = localContext.Entity;
            SetStateRequest state = null;
            Entity waiting = localContext.OrganizationService.Retrieve(entity.LogicalName, entity.Id, new ColumnSet("oems_eventregistration"));

            EntityReference reg = waiting.GetAttributeValue<EntityReference>("oems_eventregistration");

            try
            {

                if (status == WaitingListStatus.Removed)
	            {
		             state = new SetStateRequest
                    {
                        State = new OptionSetValue((int)CommonStatus.ACTIVE),
                        Status = new OptionSetValue((int)EventRegistrationStatusReason.Rejected),
                        EntityMoniker = reg
                    };

	            }else if(status == WaitingListStatus.Cancelled)
                    {
                        state = new SetStateRequest
                        {
                            State = new OptionSetValue((int)CommonStatus.ACTIVE),
                            Status = new OptionSetValue((int)EventRegistrationStatusReason.Cancelled),
                            EntityMoniker = reg
                        };
                    }

                if(state != null) localContext.OmniContext.Execute(state);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private static bool IsRegistrationOpen(Entity evt)
        {
            if (evt == null)
            {
                return false;
            }

            var registrationOpening = evt.GetAttributeValue<DateTime?>("oems_registrationopening");
            var registrationDeadline = evt.GetAttributeValue<DateTime?>("oems_registrationdeadline");

            if (!registrationOpening.HasValue || !registrationDeadline.HasValue)
            {
                return false;
            }

            return registrationOpening.Value < DateTime.UtcNow && registrationDeadline.Value > DateTime.UtcNow;
        }
    }
}

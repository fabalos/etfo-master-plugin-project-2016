﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Omniware.ETFO.Plugin.DataWork;
using Omniware.ETFO.Plugin.FormComponents;
using Omniware.ETFO.Plugin.Shared;
using Xrm;
using log4net;
using Microsoft.Crm.Sdk.Messages;

namespace Omniware.ETFO.Plugin
{
    public class TargetGrade : Plugin
    {
        private ILog _log = LogHelper.GetLogger(typeof(TargetGrade));

        public TargetGrade()
            : base(typeof(TargetGrade))
        {
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(20, "Update", "oems_targetgrade", new Action<LocalPluginContext>(CopyTargetGrade)));
        }

        protected void CopyTargetGrade(LocalPluginContext localContext)
        {
            if (localContext == null)
                throw new ArgumentNullException("localContext");
            Entity targetGrade = localContext.Entity;

            Entity fullTargetGrade = (from c in localContext.OmniContext.CreateQuery("oems_targetgrade")
                                      where (Guid)c["oems_targetgradeid"] == targetGrade.Id
                                      select c
                                ).FirstOrDefault();

            Entity newTargetGrade = new Entity(targetGrade.LogicalName);

            newTargetGrade.Attributes["oems_courseterm"] = targetGrade.GetAttributeValue<EntityReference>("oems_courseterm");
            newTargetGrade.Attributes["oems_coursetermpresenterapplication"] = targetGrade.GetAttributeValue<EntityReference>("oems_coursetermpresenterapplication");
            newTargetGrade.Attributes["oems_name"] = fullTargetGrade.GetAttributeValue<string>("oems_name");
            newTargetGrade.Attributes["oems_order"] = fullTargetGrade.GetAttributeValue<int>("oems_order");
            newTargetGrade.Attributes["oems_presenterapplicationdefaultflag"] = false;
            newTargetGrade.Attributes["oems_masterflag"] = false;

            try
            {
                localContext.OrganizationService.Create(newTargetGrade);

                localContext.Entity.Attributes.Remove("oems_courseterm");
                localContext.Entity.Attributes.Remove("oems_coursetermpresenterapplication");
            }
            catch (Exception e)
            {
                throw e;
            }
        }

    }
}

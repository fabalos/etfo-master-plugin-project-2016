﻿using System;
using System.IO;
using System.Collections;
using System.Xml.Serialization;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Xrm;
using Microsoft.ApplicationBlocks.Data;
using System.Web.Script.Serialization;
using Microsoft.Xrm.Client.Collections;
using Microsoft.Crm.Sdk.Messages;

namespace Omniware.ETFO.Plugin
{
    public class ETFOManagedAccount : Plugin
    {

        public ETFOManagedAccount()
            : base(typeof(ETFOManagedAccount))
        {
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(20, "Create", "oems_etfomanagedaccount", new Action<LocalPluginContext>(ActivateAccount)));
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "Create", "oems_etfomanagedaccount", new Action<LocalPluginContext>(DeleteAccount)));
            
        }

        protected void ActivateAccount(LocalPluginContext localContext)
        {
            if (localContext == null)
                throw new ArgumentNullException("localContext");

            // stop recurrsive calls
            if (localContext.PluginExecutionContext.Depth != 1)
                return;

            
            oems_etfomanagedaccount oems_EtfoManagedAccount = localContext.Entity.ToEntity<oems_etfomanagedaccount>();
            string memberId = oems_EtfoManagedAccount.oems_name;
            Account account = null;

            if (memberId != null)
            {
                Guid memberIdGuid = Guid.Parse(memberId);
                account = (from a in localContext.OmniContext.AccountSet
                           where a.Id == memberIdGuid 
                           select a).FirstOrDefault();

                //localContext.OrganizationService.Delete(oems_EtfoManagedAccount.LogicalName, oems_EtfoManagedAccount.Id);
                if (account == null) throw new OEMSPluginException("ETFO:Cannot find member account!");
                if (account.oems_MemberFlag != true) throw new OEMSPluginException("ETFO:This is not a member account!");
                if (account.StateCode == (int)AccountStatus.ACTIVE) throw new OEMSPluginException("ETFO:This member account is already active!");
                
                //if (account.oems_etfomanaged == true) throw new OEMSPluginException("This member account is already managed!");


                SetStateRequest state = new SetStateRequest();
                state.State = new OptionSetValue((int)AccountStatus.ACTIVE);
                state.Status =
                    new OptionSetValue((int)AccountStatusReason.ACTIVE);

                state.EntityMoniker = account.ToEntityReference();
                SetStateResponse stateSet = (SetStateResponse)localContext.OrganizationService.Execute(state);

                Account updateAccount = new Account();
                updateAccount.Id = account.Id;
                updateAccount.LogicalName = account.LogicalName;
                updateAccount.oems_etfomanaged = true;

                localContext.OrganizationService.Update(updateAccount);

            }
            else
            {
                throw new OEMSPluginException("ETFO:Please choose an account!");
            }
        }

        protected void DeleteAccount(LocalPluginContext localContext)
        {
            if (localContext == null)
                throw new ArgumentNullException("localContext");

            // stop recurrsive calls
            if (localContext.PluginExecutionContext.Depth != 1)
                return;

            localContext.OrganizationService.Delete(localContext.Entity.LogicalName, localContext.Entity.Id);

        }

       
    }
}
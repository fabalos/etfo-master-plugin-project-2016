﻿using System;
using System.Diagnostics;
using System.Linq;
using Microsoft.Xrm.Sdk;
using Omniware.ETFO.Plugin.Shared;
using Xrm;
using log4net;
using Shared.Utilities;
using Omniware.ETFO.Plugin.Billing;

namespace Omniware.ETFO.Plugin
{
    public class BillingRequest : Plugin
    {
        protected string Config;
        protected string SecConfig;
        private ILog _log = LogHelper.GetLogger(typeof(BillingRequest));

        private static class PaymentStatus
        {
            public static int Unpaid = 347780000;
            public static int ChequePending = 347780001;
            public static int CreditCardPending = 347780002;
            public static int Paid = 347780003;
            public static int Refunded = 347780004;
            public static int Partial = 347780005;
            public static int ChequeRefundPending = 347780006;
            public static int PayLater = 347780007;
            public static int Cancelled = 347780008;
            public static int Failed = 347780009;
        }

        public BillingRequest(string unsecureConfig, string secureConfig)
            : base(typeof(BillingRequest))
        {
            if (unsecureConfig != null)
            {
                Config = unsecureConfig;
            }

            if (secureConfig != null)
            {
                SecConfig = secureConfig;
            }

            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "Update", "oems_billingrequest", new Action<LocalPluginContext>(SaveOldTransactions)));
        }

        protected void SaveOldTransactions(LocalPluginContext localContext)
        {
            if (localContext == null)
                throw new ArgumentNullException("localContext");

            //Stop recurrsive calls
            if (localContext.PluginExecutionContext.Depth != 1)
                return;

            var sw = new Stopwatch();
            sw.Start();

            //Retrieve the BR images
            var preImageBillingRequest = localContext.PluginExecutionContext.PreEntityImages["PreImage"].ToEntity<oems_billingrequest>();
            var postImageBillingRequest = localContext.PluginExecutionContext.PostEntityImages["PostImage"].ToEntity<oems_billingrequest>();

            //Gets the pre / post transactions images
            var prePaymentTransactionIdRef = preImageBillingRequest.GetAttributeValue<EntityReference>("oems_paymenttransaction");
            var postPaymentTransactionIdRef = postImageBillingRequest.GetAttributeValue<EntityReference>("oems_paymenttransaction");

            //On cancellation BR does not have related payment transaction
            if (postPaymentTransactionIdRef != null)
            {
                if (prePaymentTransactionIdRef == null || postPaymentTransactionIdRef.Id != prePaymentTransactionIdRef.Id)
                {
                    // Add the new transaction to the list
                    CrmTools crmTools = new CrmTools(localContext.OmniContext);
                    crmTools.associateRelatedEntityToEntity(postPaymentTransactionIdRef,
                        new EntityReference()
                        {
                            LogicalName = postImageBillingRequest.LogicalName,
                            Id = postImageBillingRequest.Id
                        },
                        "oems_oems_billingrequest_oems_cctransaction");
                }

                // Check if the latest transaction needs to be updated
                var prePaymentStatus = preImageBillingRequest.GetAttributeValue<OptionSetValue>("oems_paymentstatus");
                var postPaymentStatus = postImageBillingRequest.GetAttributeValue<OptionSetValue>("oems_paymentstatus");
                if (prePaymentStatus.Value != postPaymentStatus.Value && postPaymentStatus.Value == PaymentStatus.Unpaid)
                {
                    //Update the old payment transaction status to failed                    
                    var oldPaymentTransaction =
                        (from c in localContext.OmniContext.CreateQuery("oems_cctransaction")
                         where c.GetAttributeValue<Guid>("oems_cctransactionid") == prePaymentTransactionIdRef.Id
                         select c).FirstOrDefault();

                    if (oldPaymentTransaction != null)
                    {
                        if (prePaymentStatus.Value == PaymentStatus.ChequePending ||
                            prePaymentStatus.Value == PaymentStatus.CreditCardPending)
                        {
                            var aux = new Entity(oldPaymentTransaction.LogicalName);
                            aux.Id = oldPaymentTransaction.Id;
                            aux["oems_paymentstatus"] = new OptionSetValue(PaymentStatus.Failed);
                            localContext.OmniContext.Detach(oldPaymentTransaction);
                            localContext.OmniContext.Attach(aux);
                            localContext.OmniContext.UpdateObject(aux);
                        }
                    }
                }
            }
            else if (prePaymentTransactionIdRef != null)
            {
                var prePaymentStatus = preImageBillingRequest.GetAttributeValue<OptionSetValue>("oems_paymentstatus");
                var postPaymentStatus = postImageBillingRequest.GetAttributeValue<OptionSetValue>("oems_paymentstatus");

                if (prePaymentStatus.Value != postPaymentStatus.Value && postPaymentStatus.Value == PaymentStatus.Unpaid)
                {
                    //Update the old payment transaction status to failed                    
                    var oldPaymentTransaction =
                        (from c in localContext.OmniContext.CreateQuery("oems_cctransaction")
                         where c.GetAttributeValue<Guid>("oems_cctransactionid") == prePaymentTransactionIdRef.Id
                         select c).FirstOrDefault();

                    if (oldPaymentTransaction != null)
                    {
                        if (postPaymentStatus.Value == PaymentStatus.Unpaid)
                        {
                            var aux = new Entity(oldPaymentTransaction.LogicalName);
                            aux.Id = oldPaymentTransaction.Id;
                            aux["oems_paymentstatus"] = new OptionSetValue(PaymentStatus.Cancelled);
                            localContext.OmniContext.Detach(oldPaymentTransaction);
                            localContext.OmniContext.Attach(aux);
                            localContext.OmniContext.UpdateObject(aux);
                        }
                    }
                }
            }

            //Check if has manually refunds
            var preManuallyRefund = preImageBillingRequest.GetAttributeValue<bool>("oems_refundmanuallyflag");
            var postManuallyRefund = postImageBillingRequest.GetAttributeValue<bool>("oems_refundmanuallyflag");
            if (preManuallyRefund != postManuallyRefund && postManuallyRefund)
            {
                var eventRegistrationid = postImageBillingRequest.GetAttributeValue<EntityReference>("oems_eventregistration").Id;
                var eventRegistration = (from c in localContext.OmniContext.CreateQuery("oems_eventregistration")
                                         where c.GetAttributeValue<Guid>("oems_eventregistrationid") == eventRegistrationid
                                         select c).Single();

                var oemsEvent = (from c in localContext.OmniContext.CreateQuery("oems_event")
                                 where c.GetAttributeValue<Guid>("oems_eventid") == eventRegistration.GetAttributeValue<EntityReference>("oems_event").Id
                                 select c).Single();

                var billingCalculator = new BillingCalculator(localContext.OmniContext, eventRegistration, oemsEvent);
                billingCalculator.RefundManually(eventRegistration.GetAttributeValue<EntityReference>("oems_contact").Id, postImageBillingRequest.Id);
                //billingCalculator.RefundManually(eventRegistration.GetAttributeValue<EntityReference>("oems_account").Id, postImageBillingRequest.Id);
            }

            localContext.OmniContext.SaveChanges();
            sw.Stop();
        }
    }
}
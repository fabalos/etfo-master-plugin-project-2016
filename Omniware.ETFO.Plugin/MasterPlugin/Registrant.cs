﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Omniware.ETFO.Plugin.MasterPlugin
{
    public class Registrant
    {
        public Guid? RegistrationID { get; set; }
        public Guid? DelgationID { get; set; }
        public Guid? AccountID { get; set; }
        public Guid? ContactID { get; set; }
        public string RegistrantFirstName { get; set; }
        public string RegistrantLastName { get; set; }
        public string LocalName { get; set; }
        public string EventName { get; set; }
        public int? RegistrationStateCode { get; set; }
        public int? RegistrationStatusId { get; set; }
        public string RegistrationStatusText { get; set; }
        public int? EventStatus { get; set; }
        public bool VotingStatus { get; set; }
        public String AttendingAsJSON { get; set; }
        public bool InDelegationList { get; set; }
        public String AttendingAsOther { get; set; }
    }
}

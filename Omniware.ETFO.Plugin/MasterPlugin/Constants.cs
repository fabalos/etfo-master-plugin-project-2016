﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Omniware.ETFO.Plugin
{

    public enum FORM_COMPONENT
    {
        ReleaseTime = 1,
        PersonalAccommodation = 2,
        SchoolBoardLocal = 3,
        RoomPricing = 4,
        EventPricing = 5,
        WorkshopSession =6,
        Attachments=7,
        TargetGrades=8
    }

    public enum CommonStatus
    {
        ACTIVE = 0,
        INACTIVE = 1
    }

    public enum CommonStatusReason
    {
        INACTIVE = 2
    }

    public static class PersonalAccommodationType
    {
        public const int Header = 1;
        public const int EventHotel = 2;
        public const int EventOnly = 3;
        public const int HotelOnly = 4;
    }

    public static class FormSource
    {
        public const int Base = 1;
        public const int Category = 2;
        public const int Subcategory = 3;
        public const int Event = 4;
    }

    public static class EventRegistrationStatusReason
    {
        public const int Initial = 347780004;
        public const int Approved = 347780003;
        public const int Rejected = 347780006;
        public const int Submitted = 347780001;
        public const int Created = 1;
        public const int Cancelled = 347780007;
        public const int Withdrawn = 347780008;
    }

    public static class EventLocalStatusReason
    {
        public const int Creating = 1;
        public const int Submitted = 347780002;
        public const int Approved = 347780000;
        public const int Revisit = 347780001;
    }

    public static class CreditCartTransactionType
    {
        public const int REFUND_TRANSACTION = 347780000;
        public const int PAYMENT_TRANSACTION = 347780001;
    }

    public enum RegistrationType
    {
        ATTENDEE = 347780000,
        PRESENTER = 347780001
    }

    public enum EventStatusReason
    {
        PUBLISH_REGISTRATION = 347780004,
        CLOSE_REGISTRATION = 347780006,
        OPEN_REGISTRATION = 347780005,
        END_EVENT = 347780008,
        COMPLETED = 347780009,
        CANCELLED = 347780010,
        CAPACITYEXCEEDED = 347780011,
        PUBLISHED_APPLICATION = 347780001,
        OPEN_APPLICATION = 347780002,
        CLOSE_APPLICATION = 347780003
    }

    public enum ReleaseTimeRequestStatus
    {
        ACTIVE = 0,
        INACTIVE = 1
    }

    public enum ReleaseTimeRequestStatusReason
    {
        REJECTED = 347780005,
        SUBMITTED = 1,
        APRROVED = 347780000,
        APPROVALPENDING = 347780002,
        CANCELLED = 347780004,
        WITHDRAWN = 347780006
    }

    public enum ChildCareRequestStatus
    {
        ACTIVE = 0,
        INACTIVE = 1
    }

    public enum ChildCareRequestStatusReason
    {
        REJECTED = 347780003,
        CANCELLED = 347780004,
        WITHDRAWN = 347780005
    }

    public enum CaucusRequestStatus
    {
        ACTIVE = 0,
        INACTIVE = 1
    }

    public enum CaucusRequestStatusReason
    {
        REJECTED = 347780003,
        CANCELLED = 347780004,
        WITHDRAWN = 347780005
    }

    public enum AccomodationRequestStatus
    {
        ACTIVE = 0,
        INACTIVE = 1
    }

    public enum AccomodationRequestStatusReason
    {
        REJECTED = 347780003,
        CANCELLED = 347780004,
        WITHDRAWN = 347780005
    }

    public enum EventAttendeeStatus
    {
        ACTIVE = 0,
        INACTIVE = 1
    }


    public enum EventAttendeeStatusReason
    {
        ACTIVE = 0,
        INACTIVE = 2,
        CANCELLED = 347780005,
        WITHDRAWN = 347780006
    }

    public enum SingleRoomMedicalAccomodationRequestStatus
    {
        ACTIVE = 0,
        INACTIVE = 1
    }

    public enum SingleRoomMedicalAccomodationRequestStatusReason
    {
        REJECTED = 347780007,
        CANCELLED = 347780005,
        WITHDRAWN = 347780006
    }

    public enum PersonalAccomodationRequestStatus
    {
        ACTIVE = 0,
        INACTIVE = 1
    }

    public enum PersonalAccomodationRequestStatusReason
    {
        REJECTED = 347780005,
        CANCELLED = 347780007,
        WITHDRAWN = 347780006,
    }

    public enum PersonalAccomodationRequestOptionStatus
    {
        ACTIVE = 0,
        INACTIVE = 1
    }

    public enum PersonalAccomodationRequestOptionStatusReason
    {
        Requested = 1,
        Approved = 347780000,
        Rejected = 347780007,
        Cancelled = 347780006,
        Withdrawn = 347780005
    }

    public enum TicketRequestStatus
    {
        ACTIVE = 0,
        INACTIVE = 1
    }

    public enum TicketRequestStatusReason
    {
        REJECTED = 347780002,
        CANCELLED = 347780003,
        WITHDRAWN = 347780004
    }

    public enum PersonalCareExcemptionRequestStatus
    {
        ACTIVE = 0,
        INACTIVE = 1
    }
    
    public enum PersonalCareExcemptionRequestStatusReason
    {
        REJECTED = 347780007,
        CANCELLED = 347780006,
        WITHDRAWN = 347780005
    }

    public enum EventInvitedGuestStatus
    {
        ACTIVE = 0,
        INACTIVE = 1
    }

    public enum EventInvitedGuestStatusReason
    {
        CANCELLED = 347780005,
        WITHDRAWN = 347780006
    }

    public enum AccountStatus
    {
        ACTIVE = 0,
        INACTIVE = 1
    }

    public enum AccountStatusReason
    {
        ACTIVE = 1,
        INACTIVE = 2
    }

    public enum AccountRole
    {
        PRESIDENT = 347780000,
        PRESIDENT_DESIGNATE = 347780001
    }

    public static class ElementProperty
    {
        public const String PersonalAccommodation = "pr_PersonalAccommodationList";
        public const String AttendingAs = "pr_AttendingAsRolesList";
    }

    public enum FormTemplateType
    {
        UNIVERSE = 1,
        SUBCATEGORY = 2,
        EVENT = 3
    }

    public static class NonMemberType
    {
        public const int TEACHER = 1;
        public const int NON_EDUCATOR = 2;
        public const int STAFF = 3;
    }

    public static class BillingParticipantType
    {
        public const int MEMBER = 347780000;
        public const int NON_MEMBER_EDUCATOR_OR_EXTERNAL= 347780001;
        public const int STAFF = 347780002;
        public const int INVITED_GUEST = 347780003;
        public const int ALL_OTHER = 347780004;
    }

    public static class WaitingListStatus
    {
        public static int Accepted = 1;
        public static int Declined = 347780000;
        public static int Expired = 347780001;
        public static int Offered = 347780002;
        public static int Removed = 347780003;
        public static int Waiting = 347780004;
        public static int Cancelled = 347780005;
    }

    public static class BindingType 
    {
        public static int InitialPath = 1;
        public static int SourcePath = 2;
        public static int TargetPath = 3;
    }
}

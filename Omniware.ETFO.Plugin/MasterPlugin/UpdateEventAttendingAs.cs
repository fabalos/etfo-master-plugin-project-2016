﻿using System;
using System.IO;
using System.Collections;
using System.Xml.Serialization;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Xrm;
using Microsoft.ApplicationBlocks.Data;
using System.Web.Script.Serialization;
using Microsoft.Xrm.Client.Collections;
using Microsoft.Crm.Sdk.Messages;
using Newtonsoft.Json;

namespace Omniware.ETFO.Plugin
{
    public class UpdateEventAttendingAs : Plugin
    {

        public UpdateEventAttendingAs()
            : base(typeof(UpdateEventAttendingAs))
        {
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "Update", "oems_event", new Action<LocalPluginContext>(UpdateEventAttendingAsConfig)));
        }

        private void associateAttendingAs(LocalPluginContext localContext, EntityReference oemsEvent, EntityReference AttendingAsOption)
        {
            AssociateEntitiesRequest associateRequest = new AssociateEntitiesRequest
            {
                Moniker1 = oemsEvent,
                Moniker2 = AttendingAsOption,
                RelationshipName = "oems_event_attending_as_config",
            };

            Entity attendingAs = localContext.OrganizationService.Retrieve(AttendingAsOption.LogicalName,AttendingAsOption.Id,new ColumnSet(true));

            bool votingRole = (bool)attendingAs["oems_votingroleflag"];

            AssociateEntitiesRequest associateRequestVotingNonVotingRole = null;
            if (votingRole)
            {
                associateRequestVotingNonVotingRole = new AssociateEntitiesRequest
                {
                    Moniker1 = oemsEvent,
                    Moniker2 = AttendingAsOption,
                    RelationshipName = "oems_event_attending_as_voting"
                };
            }
            else 
            {
                associateRequestVotingNonVotingRole = new AssociateEntitiesRequest
                {
                    Moniker1 = oemsEvent,
                    Moniker2 = AttendingAsOption,
                    RelationshipName = "oems_event_attending_as_nonvoting"
                };
            }

            try
            {
                localContext.OmniContext.Execute(associateRequest);
                localContext.OmniContext.Execute(associateRequestVotingNonVotingRole);
            }
            catch (Exception)
            {
                // nothing happens
            }
        }

        private void disassociateAttendingAs(LocalPluginContext localContext, EntityReference oemsEvent, EntityReference AttendingAsOption)
        {
            DisassociateEntitiesRequest disassociateRequest = new DisassociateEntitiesRequest
            {
                Moniker1 = oemsEvent,
                Moniker2 = AttendingAsOption,
                RelationshipName = "oems_event_attending_as_config",
            };

            Entity attendingAs = localContext.OrganizationService.Retrieve(AttendingAsOption.LogicalName, AttendingAsOption.Id, new ColumnSet(true));

            bool votingRole = (bool)attendingAs["oems_votingroleflag"];

            DisassociateEntitiesRequest disassociateRequestVotingNonVotingRole = null;
            if (votingRole)
            {
                disassociateRequestVotingNonVotingRole = new DisassociateEntitiesRequest
                {
                    Moniker1 = oemsEvent,
                    Moniker2 = AttendingAsOption,
                    RelationshipName = "oems_event_attending_as_voting"
                };
            }
            else
            {
                disassociateRequestVotingNonVotingRole = new DisassociateEntitiesRequest
                {
                    Moniker1 = oemsEvent,
                    Moniker2 = AttendingAsOption,
                    RelationshipName = "oems_event_attending_as_nonvoting"
                };
            }

            try
            {
                localContext.OmniContext.Execute(disassociateRequest);
                localContext.OmniContext.Execute(disassociateRequestVotingNonVotingRole);
            }
            catch (Exception)
            {
                // nothing happens
            }
        }

        private void processAttendingAs(LocalPluginContext localContext, List<AttendingAsOption> attendingAsOptionList, oems_Event oemsEvent)
        {

            // loop through attending as configuration list
            // find where isSelected = true and selectedId = Guid.Empty --- Add
            // find where isSelected = false and selectedId = <some GUID> --- Delete

            foreach (var attendingAs in attendingAsOptionList)
            {
                // NEW
                if (attendingAs.isSelected == true && attendingAs.selected == null)
                    associateAttendingAs(localContext, oemsEvent.ToEntityReference(), new EntityReference("oems_attendingasoption", attendingAs.id));

                // DELETE
                else if (!attendingAs.isSelected == true && attendingAs.selected != null)
                    disassociateAttendingAs(localContext, oemsEvent.ToEntityReference(), attendingAs.selected);
            }
        }

        protected void UpdateEventAttendingAsConfig(LocalPluginContext localContext)
        {
            if (localContext == null)
                throw new ArgumentNullException("localContext");

            // stop recurrsive calls
            if (localContext.PluginExecutionContext.Depth != 1)
                return;
            
            oems_Event preImageOemsEvent = localContext.PluginExecutionContext.PreEntityImages["PreImage"].ToEntity<oems_Event>();
            oems_Event oemsEvent = localContext.Entity.ToEntity<oems_Event>();

            if (((oemsEvent.Contains("oems_updateattendingasdata") && oemsEvent["oems_updateattendingasdata"] != null &&
                 preImageOemsEvent.Contains("oems_updateattendingasdata") && preImageOemsEvent["oems_updateattendingasdata"] != null && // check if oems_updatedata has changed
                 (string)preImageOemsEvent["oems_updateattendingasdata"] != (string)oemsEvent["oems_updateattendingasdata"])) ||
               ((oemsEvent.Contains("oems_updateattendingasdata") && oemsEvent["oems_updateattendingasdata"] != null && // check if oems_updatedata update for first time
                 (!preImageOemsEvent.Contains("oems_updateattendingasdata") || (preImageOemsEvent.Contains("oems_updateattendingasdata") && preImageOemsEvent["oems_updateattendingasdata"] == null)))))
            {
                AttendingAsConfiguration attendingAsConfiguration = JsonConvert.DeserializeObject<AttendingAsConfiguration>(oemsEvent.oems_UpdateAttendingAsData);

                processAttendingAs(localContext, attendingAsConfiguration.attendingAsList, oemsEvent);
            }
        }

    }
}

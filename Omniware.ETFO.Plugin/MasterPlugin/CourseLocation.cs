﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Omniware.ETFO.Plugin.DataWork;
using Omniware.ETFO.Plugin.FormComponents;
using Omniware.ETFO.Plugin.Shared;
using Xrm;
using log4net;
using Microsoft.Crm.Sdk.Messages;

namespace Omniware.ETFO.Plugin
{
    public class CourseLocation : Plugin
    {
        private ILog _log = LogHelper.GetLogger(typeof(CourseLocation));

        public CourseLocation()
            : base(typeof(CourseLocation))
        {
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(20, "Update", "oems_courselocation", new Action<LocalPluginContext>(CopyCourseLocation)));
        }

        protected void CopyCourseLocation(LocalPluginContext localContext)
        {
            if (localContext == null)
                throw new ArgumentNullException("localContext");
            Entity courseLocation = localContext.Entity;

            Entity fullCourseLocation = (from c in localContext.OmniContext.CreateQuery("oems_courselocation")
                                      where (Guid)c["oems_courselocationid"] == courseLocation.Id
                                      select c
                                ).FirstOrDefault();

            Entity newCourseLocation = new Entity(courseLocation.LogicalName);

            newCourseLocation.Attributes["oems_courseterm"] = courseLocation.GetAttributeValue<EntityReference>("oems_courseterm");
            newCourseLocation.Attributes["oems_addressline1"] = fullCourseLocation.GetAttributeValue<string>("oems_addressline1");
            newCourseLocation.Attributes["oems_addressline2"] = fullCourseLocation.GetAttributeValue<string>("oems_addressline2");
            newCourseLocation.Attributes["oems_city"] = fullCourseLocation.GetAttributeValue<string>("oems_city");
            newCourseLocation.Attributes["oems_locationdescription"] = fullCourseLocation.GetAttributeValue<string>("oems_locationdescription");
            newCourseLocation.Attributes["oems_phone"] = fullCourseLocation.GetAttributeValue<string>("oems_phone");
            newCourseLocation.Attributes["oems_postalcode"] = fullCourseLocation.GetAttributeValue<string>("oems_postalcode");
            newCourseLocation.Attributes["oems_province"] = fullCourseLocation.GetAttributeValue<OptionSetValue>("oems_province");
            newCourseLocation.Attributes["oems_name"] = fullCourseLocation.GetAttributeValue<string>("oems_name");

            try
            {
                localContext.OrganizationService.Create(newCourseLocation);
                localContext.Entity.Attributes.Remove("oems_courseterm");
            }
            catch (Exception e)
            {
                throw e;
            }
        }

    }
}

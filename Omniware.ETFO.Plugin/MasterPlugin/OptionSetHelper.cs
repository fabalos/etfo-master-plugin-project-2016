﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using Xrm;

namespace Omniware.ETFO.Plugin
{
    class OptionSetHelper
    {
        public List<OptionMetadata> optionList;

        public void setGlobalOptionSet(OmniContext omniContext, string optionsetName)
        {
            RetrieveOptionSetRequest retrieveOptionSetRequest =
                new RetrieveOptionSetRequest
                {
                    Name = optionsetName
                };

            // Execute the request.
            RetrieveOptionSetResponse retrieveOptionSetResponse =
                (RetrieveOptionSetResponse)omniContext.Execute(retrieveOptionSetRequest);

            // Access the retrieved OptionSetMetadata.
            OptionSetMetadata retrievedOptionSetMetadata = (OptionSetMetadata)retrieveOptionSetResponse.OptionSetMetadata;

            // Get the current options list for the retrieved attribute.
            optionList = retrievedOptionSetMetadata.Options.ToList<OptionMetadata>();
        }


        public void setLocalOptionSet(OmniContext omniContext, string entityName, string attributeName)
        {
            string AttributeName = attributeName;
            string EntityLogicalName = entityName;

            RetrieveEntityRequest retrieveDetails = new RetrieveEntityRequest
            {
                EntityFilters = EntityFilters.All,
                LogicalName = EntityLogicalName
            };

            RetrieveEntityResponse retrieveEntityResponseObj = (RetrieveEntityResponse)omniContext.Execute(retrieveDetails);
            
            EntityMetadata metadata = retrieveEntityResponseObj.EntityMetadata;
            
            PicklistAttributeMetadata picklistMetadata = 
                metadata.Attributes.FirstOrDefault(attribute => String.Equals
                    (attribute.LogicalName, attributeName, StringComparison.OrdinalIgnoreCase)) as PicklistAttributeMetadata;

            OptionSetMetadata retrievedOptionSetMetadata = picklistMetadata.OptionSet;

            // Get the current options list for the retrieved attribute.
            optionList = retrievedOptionSetMetadata.Options.ToList<OptionMetadata>();
        }

        public String getOptionLabel(int optionsetValue)
        {
            String optionsetSelectedText = String.Empty;

            foreach (OptionMetadata optionMetadata in optionList)
            {
                if (optionMetadata.Value.Value == optionsetValue)
                {
                    optionsetSelectedText = optionMetadata.Label.UserLocalizedLabel.Label;
                    break;
                }
            }

            return optionsetSelectedText;
        }
    }
}

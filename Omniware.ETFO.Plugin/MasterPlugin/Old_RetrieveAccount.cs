﻿using System;
using System.IO;
using System.Collections;
using System.Xml.Serialization;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Xrm;
using Microsoft.ApplicationBlocks.Data;
using System.Web.Script.Serialization;
using Microsoft.Xrm.Client.Collections;
using Microsoft.Crm.Sdk.Messages;
using Newtonsoft.Json;

namespace Omniware.ETFO.Plugin
{
    public class RetrieveAccount : Plugin
    {

        public RetrieveAccount()
            : base(typeof(RetrieveAccount))
        {
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "Retrieve", "account", new Action<LocalPluginContext>(RetrieveAccountPersonalAccommodation)));
        }

        private List<PersonalAccommodation> getPersonalAccommodation(LocalPluginContext localContext, List<oems_PersonalAccommodationOption> oemsPersonalAccommodationOptionList, List<oems_AccountPersonalAccommodation> oemsAccountPersonalAccommodationList)
        {
            List<PersonalAccommodation> personalAccommodationList =
            (from a in oemsPersonalAccommodationOptionList
             join b in oemsAccountPersonalAccommodationList on a.oems_PersonalAccommodationOptionId equals b.oems_PersonalAccommodationOption.Id into c
             from d in c.DefaultIfEmpty()
             select new PersonalAccommodation
             {
                 isSelected = (d != null ? true : false),
                 id = (Guid)a.oems_PersonalAccommodationOptionId,
                 selectedId = (d != null ? (Guid)d.oems_AccountPersonalAccommodationId : Guid.Empty),
                 name = a.oems_OptionName,
                 description = a.oems_Description,
                 optionType = new Option { value = a.oems_OptionType, label = a.FormattedValues["oems_optiontype"] },
                 isEventSelected = (d != null ? (bool)d.oems_EventFlag : false),
                 isHotelSelected = (d != null ? (bool)d.oems_HotelFlag : false),
                 showUserSpecifiedDetails = (bool)a.oems_UserSpecifiedDetailFlag,
                 userSpecifiedDetails = (d != null ? d.oems_UserSpecifiedDetail : null),
                 requestStatus = new Option(),
                 requestNotes = null,
                 level = (int)a.oems_Level,
                 parent = (a.oems_ParentOption != null ? a.oems_ParentOption.Id : (Guid?)null ),
                 isLeaf = (a.oems_OptionType != PersonalAccommodationType.Header ? true : false),
                 expanded = true,
                 loaded = true
             }
            ).ToList<PersonalAccommodation>();

            return personalAccommodationList;
        }

        protected void RetrieveAccountPersonalAccommodation(LocalPluginContext localContext)
        {
            if (localContext == null)
                throw new ArgumentNullException("localContext");

            // stop recurrsive calls
            if (localContext.PluginExecutionContext.Depth != 1)
                return;

            Entity entity = (Entity)localContext.PluginExecutionContext.OutputParameters["BusinessEntity"];

            // get Account
            Account account =
                (from a in localContext.OmniContext.AccountSet
                 where a.AccountId == entity.Id
                 select a).Single();

            // get the the personal accommodation
            PersonalAccommodationRequest personalAccommodationRequest = new PersonalAccommodationRequest();
            personalAccommodationRequest.personalAccommodationList =
                getPersonalAccommodation
                (localContext
                , (from a in localContext.OmniContext.oems_PersonalAccommodationOptionSet
                   select a).ToList<oems_PersonalAccommodationOption>()
                , account.oems_account_to_accountpersonalaccommodation.ToList<oems_AccountPersonalAccommodation>()
                );

            // return the json of the set
            var json = JsonConvert.SerializeObject(personalAccommodationRequest);

            entity["oems_retrievepersonalaccommodationdata"] = json.ToString();

            //account.oems_RetrievePersonalAccommodationData = json.ToString();

            //localContext.OmniContext.UpdateObject(account);
            //localContext.OmniContext.SaveChanges();
        }

    }
}

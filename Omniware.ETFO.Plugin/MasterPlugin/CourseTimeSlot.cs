﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Omniware.ETFO.Plugin.DataWork;
using Omniware.ETFO.Plugin.FormComponents;
using Omniware.ETFO.Plugin.Shared;
using Xrm;
using log4net;
using Microsoft.Crm.Sdk.Messages;

namespace Omniware.ETFO.Plugin
{
    public class CourseTimeSlot : Plugin
    {
        private ILog _log = LogHelper.GetLogger(typeof(CourseTimeSlot));

        public CourseTimeSlot()
            : base(typeof(CourseTimeSlot))
        {
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(20, "Update", "oems_coursetimeslot", new Action<LocalPluginContext>(CopyCourseTimeSlot)));
        }

        protected void CopyCourseTimeSlot(LocalPluginContext localContext)
        {
            if (localContext == null)
                throw new ArgumentNullException("localContext");
            
            Entity coursetimeslot = localContext.OrganizationService.Retrieve(localContext.Entity.LogicalName,localContext.Entity.Id,new ColumnSet(true));

            Entity fullTargetGrade = (from c in localContext.OmniContext.CreateQuery("oems_coursetimeslot")
                                      where (Guid)c["oems_coursetimeslotid"] == coursetimeslot.Id
                                      select c
                                ).FirstOrDefault();

            Entity newCoursetimeslot = new Entity(coursetimeslot.LogicalName);

            newCoursetimeslot.Attributes["oems_courseterm"] = localContext.Entity.GetAttributeValue<EntityReference>("oems_courseterm");
            newCoursetimeslot.Attributes["oems_name"] = fullTargetGrade.GetAttributeValue<string>("oems_name");
            newCoursetimeslot.Attributes["oems_starttime"] = coursetimeslot.GetAttributeValue<string>("oems_starttime");
            newCoursetimeslot.Attributes["oems_endtime"] = coursetimeslot.GetAttributeValue<string>("oems_endtime");

            try
            {
                
                localContext.Entity.Attributes.Remove("oems_courseterm");
                localContext.Entity.Attributes.Remove("oems_coursetermpresenterapplication");

                localContext.OrganizationService.Create(newCoursetimeslot);
                
            }
            catch (Exception e)
            {
                throw e;
            }
        }

    }
}

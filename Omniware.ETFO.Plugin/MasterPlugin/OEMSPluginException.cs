﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Omniware.ETFO.Plugin
{
    public class OEMSPluginException: Exception
    {
        public OEMSPluginException(string message): base(message)
        {
            
        }

    }
}

﻿using System;
using System.IO;
using System.Collections;
using System.Xml.Serialization;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Xrm;
using Microsoft.ApplicationBlocks.Data;
using System.Web.Script.Serialization;
using Microsoft.Xrm.Client.Collections;
using Microsoft.Crm.Sdk.Messages;

namespace Omniware.ETFO.Plugin
{
    public class EventApplicationSchedule : Plugin
    {
        public EventApplicationSchedule()
            : base(typeof(EventApplicationSchedule))
        {
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "Update", "oems_eventapplicationschedule", new Action<LocalPluginContext>(UpdateEventApplication)));
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "Create", "oems_eventapplicationschedule", new Action<LocalPluginContext>(CreateEventApplication)));
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(20, "Delete", "oems_eventapplicationschedule", new Action<LocalPluginContext>(DeleteEventApplication)));
        }

        protected void UpdateEventApplication(LocalPluginContext localContext)
        {
            
            if (localContext == null)
                throw new ArgumentNullException("localContext");

            // stop recurrsive calls
            if (localContext.PluginExecutionContext.Depth != 1)
                return;

            Entity eventRegistrationSchedule = (Entity)localContext.PluginExecutionContext.PostEntityImages["PostImage"];
 
            UpdateEventDeadlines(localContext, eventRegistrationSchedule, false);
        }

        protected void CreateEventApplication(LocalPluginContext localContext)
        {
            if (localContext == null)
                throw new ArgumentNullException("localContext");

            // stop recurrsive calls
            if (localContext.PluginExecutionContext.Depth != 1)
                return;

            Entity eventApplicationSchedule = (Entity)localContext.PluginExecutionContext.PostEntityImages["PostImage"];
            
            UpdateEventDeadlines(localContext, eventApplicationSchedule, false);
        }

        protected void DeleteEventApplication(LocalPluginContext localContext)
        {
            if (localContext == null)
                throw new ArgumentNullException("localContext");

            // stop recurrsive calls
            if (localContext.PluginExecutionContext.Depth != 1)
                return;

            Entity eventApplicationSchedule = (Entity)localContext.PluginExecutionContext.PreEntityImages["PreImage"];

            UpdateEventDeadlines(localContext, eventApplicationSchedule, true);
        }

        protected void UpdateEventDeadlines(LocalPluginContext localContext, Entity eventApplicationSchedule, bool deleting)
        {
            if (eventApplicationSchedule.GetAttributeValue<EntityReference>("oems_event") == new EntityReference())
            {
                return;
            }
            
            var oemsEvent =
                 (from a in localContext.OmniContext.CreateQuery("oems_event")
                 where (Guid)a["oems_eventid"] == eventApplicationSchedule.GetAttributeValue<EntityReference>("oems_event").Id
                 select a).FirstOrDefault();

            if (oemsEvent == null)
            {

                return;
            }

            var query = localContext.OmniContext.CreateQuery("oems_eventapplicationschedule");


            query = query.Where(r => (Guid)r["oems_event"] == oemsEvent.Id);
            query = query.Where(r => ((OptionSetValue)r["statecode"]).Value == 0);
            if (deleting)
            {
                query = query.Where(r => r.Id != eventApplicationSchedule.Id);
            }
            DateTime? applicationOpening = null;
            DateTime? applicationDeadline = null;

            var registrations = query.ToList();
            foreach (var r in registrations)
            {
                if (applicationOpening == null && r.Attributes.Contains("oems_applicationopendatetime") ||
                    applicationOpening != null && r.Attributes.Contains("oems_applicationopendatetime") && applicationOpening.Value > (DateTime)r.Attributes["oems_applicationopendatetime"])
                {
                    applicationOpening = (DateTime)r.Attributes["oems_applicationopendatetime"];
                }
                if (applicationDeadline == null && r.Attributes.Contains("oems_applicationclosedatetime") ||
                    applicationDeadline != null && r.Attributes.Contains("oems_applicationclosedatetime") && applicationDeadline.Value < (DateTime)r.Attributes["oems_applicationclosedatetime"])
                {
                    applicationDeadline = (DateTime)r.Attributes["oems_applicationclosedatetime"];
                }
            }

            oemsEvent.Attributes["oems_applicationdeadline"] = applicationDeadline;
            oemsEvent.Attributes["oems_applicationopening"] = applicationOpening;

            localContext.OmniContext.UpdateObject(oemsEvent);
            localContext.OmniContext.SaveChanges();

        }
    }
}

﻿using System;
using System.IO;
using System.Collections;
using System.Xml.Serialization;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Xrm;
using Microsoft.ApplicationBlocks.Data;
using System.Web.Script.Serialization;
using Microsoft.Xrm.Client.Collections;
using Microsoft.Crm.Sdk.Messages;
using Newtonsoft.Json;
using Microsoft.Xrm.Sdk.Messages;
using Shared.Utilities;

namespace Omniware.ETFO.Plugin
{
    public class Event : Plugin
    {

        public Event()
            : base(typeof(Event))
        {
            //base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "SetState", "oems_event", new Action<LocalPluginContext>(UpdateRelationships)));
            //base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "SetStateDynamicEntity", "oems_event", new Action<LocalPluginContext>(UpdateRelationships)));
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(20, "Update", "oems_event", new Action<LocalPluginContext>(ValidateStatusCodes)));
            //base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(20, "Update", "oems_event", new Action<LocalPluginContext>(BeforeUpdateCreate)));
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "Create", "oems_event", new Action<LocalPluginContext>(RegistrationTemplate)));
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "Update", "oems_event", new Action<LocalPluginContext>(RegistrationTemplate)));
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "Create", "oems_event", new Action<LocalPluginContext>(CopyRelatedCourseRecords)));
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "Create", "oems_event", new Action<LocalPluginContext>(CopyTargetGrades)));
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "Update", "oems_event", new Action<LocalPluginContext>(CopyTargetGrades)));
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "Update", "oems_event", new Action<LocalPluginContext>(CopyBudgetNumbers)));
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "Update", "oems_event", new Action<LocalPluginContext>(CopyBlankForms)));
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "Create", "oems_event", new Action<LocalPluginContext>(CopyBudgetNumbers)));
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "Create", "oems_event", new Action<LocalPluginContext>(CopyBlankForms)));
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "Update", "oems_event", new Action<LocalPluginContext>(SendCoursePackageEmails)));
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(10, "Delete", "oems_event", new Action<LocalPluginContext>(DeleteEvent_PreValidate)));
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(20, "Delete", "oems_event", new Action<LocalPluginContext>(DeleteEvent_PreOperation)));
        }

        protected void SendCoursePackageEmails(LocalPluginContext localContext)
        {
            if (localContext == null)
            {
                throw new ArgumentNullException("localContext");
            }

            // stop update call from retrieve plugin
            if (localContext.PluginExecutionContext.Depth != 1)
                return;

            Entity course = (Entity)localContext.Entity;

            if (!course.GetAttributeValue<bool>("oems_coursepackageavailableflag")) return;

            try
            {
                List<Entity> eventAttendees = (from e in localContext.OmniContext.CreateQuery("oems_eventattendee")
                                               where ((EntityReference)e["oems_event"]).Id == course.Id
                                               select e
                                ).ToList<Entity>();

                foreach (var attendee in eventAttendees)
                {
                    //Account account = (from a in localContext.OmniContext.AccountSet
                    //                   where (Guid)a.AccountId == ((EntityReference)attendee["oems_account"]).Id
                    //                   select a).FirstOrDefault();

                    
                    Contact contact = (from c in localContext.OmniContext.ContactSet
                                       where (Guid)c.ContactId == ((EntityReference)attendee["oems_contact"]).Id
                                       select c).FirstOrDefault();

                    CrmTools.RunEmailNotification(contact, localContext.OrganizationService, StringCodes.COURSE_PACKAGE_EMAIL_FROM, StringCodes.COURSE_PACKAGE_EMAIL_HTML_BODY,
                    StringCodes.COURSE_PACKAGE_EMAIL_SUBJECT);

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void CopyTargetGrades(LocalPluginContext localContext)
        {
            if (localContext == null)
            {
                throw new ArgumentNullException("localContext");
            }

            // stop update call from retrieve plugin
            if (localContext.PluginExecutionContext.Depth != 1)
                return;

            Entity courseTerm = (Entity)localContext.Entity;
            //bool courseTermFlag = courseTerm.GetAttributeValue<bool>("oems_coursetermflag");

            //if (courseTermFlag)
            //{
            //    CopyCourseTermTargetGrades(localContext, courseTerm.ToEntityReference());
            //}

            bool applicationFlag = courseTerm.GetAttributeValue<bool>("oems_workshopapplicationcomponentflag");

            if (applicationFlag)
            {
                CopyApplicationTargetGrades(localContext, courseTerm.ToEntityReference());
            }
        }

        private void CopyCourseTermTargetGrades(LocalPluginContext localContext, EntityReference courseTermRef)
        {
            // stop update call from retrieve plugin
            if (localContext.PluginExecutionContext.Depth != 1)
                return;
            try
            {
                //List<Entity> targetGrades = (from t in localContext.OmniContext.CreateQuery("oems_targetgrade")
                //                             where (EntityReference)t["oems_courseterm"] == null &&
                //                                   (EntityReference)t["oems_coursetermpresenterapplication"] == null
                //                             select t
                //                ).ToList<Entity>();
                List<Entity> targetGrades = (from t in localContext.OmniContext.CreateQuery("oems_targetgrade")
                                             where t["oems_presenterapplicationdefaultflag"] != null
                                             && (bool)t["oems_presenterapplicationdefaultflag"] == true
                                             select t
                                ).ToList<Entity>();

                Entity newTargetGrade;

                foreach (var item in targetGrades)
                {
                    newTargetGrade = new Entity(item.LogicalName);
                    newTargetGrade.Attributes["oems_courseterm"] = courseTermRef;
                    newTargetGrade.Attributes["oems_name"] = item.GetAttributeValue<string>("oems_name");
                    newTargetGrade.Attributes["oems_presenterapplicationdefaultflag"] = false;
                    localContext.OmniContext.Create(newTargetGrade);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private void CopyApplicationTargetGrades(LocalPluginContext localContext, EntityReference courseTermRef)
        {
            try
            {
                List<Entity> targetGrades = (from t in localContext.OmniContext.CreateQuery("oems_targetgrade")
                                             where t["oems_presenterapplicationdefaultflag"] != null
                                             && (bool)t["oems_presenterapplicationdefaultflag"] == true
                                             select t
                                ).ToList<Entity>();

                Entity newTargetGrade;

                foreach (var item in targetGrades)
                {
                    newTargetGrade = new Entity(item.LogicalName);
                    newTargetGrade.Attributes["oems_coursetermpresenterapplication"] = courseTermRef;
                    newTargetGrade.Attributes["oems_name"] = item.GetAttributeValue<string>("oems_name");
                    newTargetGrade.Attributes["oems_order"] = item.GetAttributeValue<int>("oems_order");
                    newTargetGrade.Attributes["oems_presenterapplicationdefaultflag"] = false;
                    newTargetGrade.Attributes["oems_masterflag"] = false;
                    localContext.OmniContext.Create(newTargetGrade);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        protected void CopyRelatedCourseRecords(LocalPluginContext localContext)
        {
            if (localContext == null)
            {
                throw new ArgumentNullException("localContext");
            }

            // stop update call from retrieve plugin
            if (localContext.PluginExecutionContext.Depth != 1)
                return;

            Entity oems_Event = (Entity)localContext.Entity;
            EntityReference courseTermRef = oems_Event.GetAttributeValue<EntityReference>("oems_courseterm");

            if (courseTermRef != null)
            {
                CopyStaffOwners(localContext, courseTermRef, oems_Event.ToEntityReference());
            }
        }

        protected void CopyBudgetNumbers(LocalPluginContext localContext)
        {
            if (localContext == null)
            {
                throw new ArgumentNullException("localContext");
            }

            // stop update call from retrieve plugin
            if (localContext.PluginExecutionContext.Depth != 1)
                return;

            Entity oems_Event = (Entity)localContext.Entity;

            string[] fields = { "oems_budgetnumber", "oems_budgetnumber2" };
            Entity evt = null;

            try
            {
                foreach (var field in fields)
                {
                    EntityReference budgetCodeRef = oems_Event.GetAttributeValue<EntityReference>(field);

                    if (budgetCodeRef == null) continue;

                    Entity fullBudgetCode = (from c in localContext.OmniContext.CreateQuery("oems_budgetcode")
                                             where (Guid)c["oems_budgetcodeid"] == budgetCodeRef.Id
                                             select c
                                ).FirstOrDefault();

                    Entity newBudgetCode = new Entity(fullBudgetCode.LogicalName);
                    newBudgetCode.Attributes["oems_budgetnumber"] = fullBudgetCode.GetAttributeValue<string>("oems_budgetnumber");
                    newBudgetCode.Attributes["oems_masterflag"] = false;
                    newBudgetCode.Id = localContext.OmniContext.Create(newBudgetCode);

                    if (evt == null)
                    {
                        evt = new Entity(oems_Event.LogicalName);
                        evt.Id = oems_Event.Id;
                    }

                    evt.Attributes[field] = newBudgetCode.ToEntityReference();
                }

                if (evt != null) localContext.OmniContext.Update(evt);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        protected void CopyBlankForms(LocalPluginContext localContext)
        {
            if (localContext == null)
            {
                throw new ArgumentNullException("localContext");
            }

            // stop update call from retrieve plugin
            if (localContext.PluginExecutionContext.Depth != 1)
                return;

            Entity oems_Event = (Entity)localContext.Entity;

            string[] fields = { "oems_instructorcourseagreementblankform1", "oems_instructorpayrollenrollmentblankform1", "oems_lettertemplate", "oems_sampleparticipationletter" };
            Entity evt = null;

            try
            {
                foreach (var field in fields)
                {
                    EntityReference eventAttachRef = oems_Event.GetAttributeValue<EntityReference>(field);

                    if (eventAttachRef == null) continue;

                    Entity fullEventAttach = (from c in localContext.OmniContext.CreateQuery("oems_eventattachment")
                                              where (Guid)c["oems_eventattachmentid"] == eventAttachRef.Id
                                              select c
                                ).FirstOrDefault();

                    Entity newEventAttachRef = new Entity(fullEventAttach.LogicalName);
                    newEventAttachRef.Attributes["oems_name"] = fullEventAttach.GetAttributeValue<string>("oems_name");
                    newEventAttachRef.Attributes["oems_masterflag"] = false;
                    newEventAttachRef.Id = localContext.OmniContext.Create(newEventAttachRef);

                    Annotation annotation = (from c in localContext.OmniContext.AnnotationSet
                                             where c.ObjectId.Id == fullEventAttach.Id
                                             select c
                                ).FirstOrDefault();

                    if (annotation != null)
                    {
                        Entity newAnnotation = new Entity();
                        newAnnotation.Attributes["filename"] = annotation.GetAttributeValue<string>("filename");
                        newAnnotation.Attributes["subject"] = annotation.GetAttributeValue<string>("subject");
                        newAnnotation.Attributes["documentbody"] = annotation.GetAttributeValue<string>("documentbody");
                        newAnnotation.Attributes["objectid"] = newEventAttachRef.ToEntityReference();
                    }

                    if (evt == null)
                    {
                        evt = new Entity(oems_Event.LogicalName);
                        evt.Id = oems_Event.Id;
                    }

                    evt.Attributes[field] = newEventAttachRef.ToEntityReference();
                }

                if (evt != null) localContext.OmniContext.Update(evt);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        protected void SetTrackingFields(LocalPluginContext localContext)
        {
            localContext.Entity["oems_decisiontimestamp"] = DateTime.UtcNow;
            localContext.Entity["oems_decisionuser"] = new EntityReference("systemuser", localContext.PluginExecutionContext.UserId);
        }

        protected void CopyStaffOwners(LocalPluginContext localContext, EntityReference courseTerm, EntityReference course)
        {
            DataCollection<Entity> executiveOwners = GetRelatedEntitDataFromManyToManyRelation("systemuser", "oems_event_executive_staff_owners", courseTerm, localContext.OrganizationService);
            EntityReferenceCollection supportOwnersCollection = new EntityReferenceCollection();
            EntityReferenceCollection executiveOwnersCollection = new EntityReferenceCollection();
            Relationship relExecutiveOwners = new Relationship("oems_event_executive_staff_owners");
            Relationship relSupportOwners = new Relationship("oems_event_support_staff_owners");

            if (executiveOwners != null)
            {
                foreach (var target in executiveOwners)
                {
                    executiveOwnersCollection.Add(target.ToEntityReference());
                }

                localContext.OrganizationService.Associate(course.LogicalName, course.Id, relExecutiveOwners, executiveOwnersCollection);
            }


            DataCollection<Entity> supportOwners = GetRelatedEntitDataFromManyToManyRelation("systemuser", "oems_event_support_staff_owners", courseTerm, localContext.OrganizationService);

            if (supportOwners != null)
            {
                foreach (var topic in supportOwners)
                {
                    supportOwnersCollection.Add(topic.ToEntityReference());
                }

                localContext.OrganizationService.Associate(course.LogicalName, course.Id, relSupportOwners, supportOwnersCollection);
            }
        }

        protected void CopyTopicsToCourseTerm(LocalPluginContext localContext, EntityReference eventEntityReference)
        {
            Guid eventId = localContext.Entity.Id;

            List<Entity> courseTopics = (from c in localContext.OmniContext.CreateQuery("oems_coursetopic")
                                         where ((EntityReference)c["oems_coursetermpresenterapplication"]).Id == eventId
                                         select c
                                ).ToList<Entity>();

            ExecuteMultipleRequest requestWithResults = new ExecuteMultipleRequest()
            {
                // Assign settings that define execution behavior: continue on error, return responses. 
                Settings = new ExecuteMultipleSettings()
                {
                    ContinueOnError = false,
                    ReturnResponses = true
                },
                // Create an empty organization request collection.
                Requests = new OrganizationRequestCollection()
            };

            foreach (var item in courseTopics)
            {
                Entity aux = new Entity(item.LogicalName);
                aux.Attributes["oems_name"] = item.GetAttributeValue<string>("oems_name");
                aux.Attributes["oems_courseterm"] = item.GetAttributeValue<EntityReference>("oems_coursetermpresenterapplication");
                CreateRequest createRequest = new CreateRequest { Target = aux };
                requestWithResults.Requests.Add(createRequest);
            }

            ExecuteMultipleResponse responseWithResults = (ExecuteMultipleResponse)localContext.OmniContext.Execute(requestWithResults);

        }

        public static DataCollection<Entity> GetRelatedEntitDataFromManyToManyRelation(string entityToRetrieve, string relationName, EntityReference targetEntity, IOrganizationService service)
        {
            DataCollection<Entity> result = null;

            QueryExpression query = new QueryExpression();
            query.EntityName = entityToRetrieve;
            query.ColumnSet = new ColumnSet(true);
            Relationship relationship = new Relationship();

            relationship.SchemaName = relationName;

            RelationshipQueryCollection relatedEntity = new RelationshipQueryCollection();
            relatedEntity.Add(relationship, query);
            RetrieveRequest request = new RetrieveRequest();
            request.RelatedEntitiesQuery = relatedEntity;
            request.ColumnSet = new ColumnSet(true);
            request.Target = targetEntity;
            RetrieveResponse response = (RetrieveResponse)service.Execute(request);


            if (((DataCollection<Relationship, EntityCollection>)(((RelatedEntityCollection)(response.Entity.RelatedEntities)))).Contains(new Relationship(relationName)) && ((DataCollection<Relationship, EntityCollection>)(((RelatedEntityCollection)(response.Entity.RelatedEntities))))[new Relationship(relationName)].Entities.Count > 0)
            {
                result = ((DataCollection<Relationship, EntityCollection>)(((RelatedEntityCollection)(response.Entity.RelatedEntities))))[new Relationship(relationName)].Entities;
            }

            return result;
        }

        protected void CancelRelatedRecords(LocalPluginContext localContext, Guid eventId, int status, int statusReason)
        {
            if (statusReason == (int)EventStatusReason.CANCELLED)
            {

                SetStateRequest state = new SetStateRequest();

                Microsoft.Xrm.Sdk.Messages.RetrieveEntityRequest rq = new Microsoft.Xrm.Sdk.Messages.RetrieveEntityRequest
                {
                    EntityFilters = Microsoft.Xrm.Sdk.Metadata.EntityFilters.Relationships,
                    LogicalName = "oems_event"
                };
                Microsoft.Xrm.Sdk.Messages.RetrieveEntityResponse rr = (Microsoft.Xrm.Sdk.Messages.RetrieveEntityResponse)localContext.OrganizationService.Execute(rq);
                foreach (var r in rr.EntityMetadata.OneToManyRelationships)
                {
                    string entityName = r.ReferencingEntity;
                    string attributeName = r.ReferencingAttribute;

                    if (entityName != "oems_eventregistration")
                    {
                        //throw new OEMSPluginException(entityName + ":" + attributeName);
                        if (entityName.ToLower().IndexOf("oems") > -1)
                        {
                            var entities = from e in localContext.OmniContext.CreateQuery(entityName)
                                           where (int)e["statecode"] == 0 && (Guid)e[attributeName] == eventId
                                           select e;
                            foreach (var e in entities)
                            {
                                state.State = new OptionSetValue((int)CommonStatus.INACTIVE);
                                state.Status = new OptionSetValue((int)CommonStatusReason.INACTIVE);
                                state.EntityMoniker = e.ToEntityReference();
                                SetStateResponse stateSet = (SetStateResponse)localContext.OrganizationService.Execute(state);
                            }
                        }
                    }

                }
            }
        }

        protected void UpdateRelationships(LocalPluginContext localContext)
        {
            // JASBIR - OCT 31/2014
            // WE DO NOT NEED TO CANCEL THE RELATED RECORDS AND SET THEM TO INACTIVE
            // CANCELLING THE REGISTRATION WILL TAKE CARE OF THE ASSOCIATIONS ON THE EVENT REGISTRATION


            //if (localContext == null)
            //{
            //    throw new ArgumentNullException("localContext");
            //}

            //// stop update call from retrieve plugin
            //if (localContext.PluginExecutionContext.Depth != 1)
            //    return;




            //if (localContext.PluginExecutionContext.InputParameters.Contains("EntityMoniker") &&
            //      localContext.PluginExecutionContext.InputParameters["EntityMoniker"] is EntityReference)
            //{
            //    var entityRef = (EntityReference)localContext.PluginExecutionContext.InputParameters["EntityMoniker"];
            //    var status = (OptionSetValue)localContext.PluginExecutionContext.InputParameters["State"];
            //    var statusReason = (OptionSetValue)localContext.PluginExecutionContext.InputParameters["Status"];

            //    CancelRelatedRecords(localContext, entityRef.Id, status.Value, statusReason.Value);

            //}
        }


        protected void ValidateStatusCodes(LocalPluginContext localContext)
        {
            if (localContext == null)
            {
                throw new ArgumentNullException("localContext");
            }

            // stop update call from retrieve plugin
            if (localContext.PluginExecutionContext.Depth != 1)
                return;

            oems_Event oems_Event = localContext.Entity.ToEntity<oems_Event>();
            if (oems_Event.statuscode == null) return;

            if (oems_Event.statuscode.Value == (int)EventStatusReason.CANCELLED)
            {
                oems_Event pre_oemsEvent = localContext.PluginExecutionContext.PreEntityImages["PreImage"].ToEntity<oems_Event>();
                if (pre_oemsEvent.statuscode.Value == (int)EventStatusReason.END_EVENT ||
                    pre_oemsEvent.statuscode.Value == (int)EventStatusReason.COMPLETED ||
                    pre_oemsEvent.statuscode.Value == (int)EventStatusReason.CANCELLED)
                {
                    throw new OEMSPluginException("Cannot cancel this event.");
                }
                else
                {
                    cancelRelatedRegistration(localContext, oems_Event.ToEntityReference());

                    Entity fullEvent = localContext.OrganizationService.Retrieve(oems_Event.LogicalName, oems_Event.Id, new ColumnSet("oems_coursetermflag"));

                    if (fullEvent.GetAttributeValue<bool>("oems_coursetermflag"))
                    {
                        CancelCourses(localContext, oems_Event.ToEntityReference());
                    }
                }


                SetTrackingFields(localContext);
            }
            else
            {
                if (oems_Event.statuscode.Value == (int)EventStatusReason.PUBLISHED_APPLICATION)
                {
                    CopyTopicsToCourseTerm(localContext, oems_Event.ToEntityReference());
                }
            }


        }

        private void CancelCourses(LocalPluginContext localContext, EntityReference eventEntityRef)
        {
            var courses = (from c in localContext.OmniContext.CreateQuery("oems_event")
                           where c["oems_courseterm"] == eventEntityRef
                           where (int)c["statuscode"] != (int)EventStatusReason.CANCELLED
                              && (int)c["statuscode"] != (int)EventStatusReason.COMPLETED
                           select c);

            var requestWithResults = new ExecuteMultipleRequest()
            {
                Settings = new ExecuteMultipleSettings()
                {
                    ContinueOnError = false,
                    ReturnResponses = true
                },
                Requests = new OrganizationRequestCollection()
            };

            foreach (var course in courses)
            {
                SetStateRequest state = new SetStateRequest();
                state.State = new OptionSetValue((int)CommonStatus.ACTIVE);
                state.Status = new OptionSetValue((int)EventStatusReason.CANCELLED);
                state.EntityMoniker = course.ToEntityReference();
                requestWithResults.Requests.Add(state);
            }

            ExecuteMultipleResponse responseWithResults = (ExecuteMultipleResponse)localContext.OmniContext.Execute(requestWithResults);
        }

        private void cancelRelatedRegistration(LocalPluginContext localContext, EntityReference eventId)
        {
            var registrations = (from c in localContext.OmniContext.CreateQuery("oems_eventregistration")
                                 where c["oems_event"] == eventId
                                 where (int)c["statuscode"] == EventRegistrationStatusReason.Created
                                     || (int)c["statuscode"] == EventRegistrationStatusReason.Submitted
                                     || (int)c["statuscode"] == EventRegistrationStatusReason.Approved
                                 select c);

            var requestWithResults = new ExecuteMultipleRequest()
            {
                Settings = new ExecuteMultipleSettings()
                {
                    ContinueOnError = false,
                    ReturnResponses = true
                },
                Requests = new OrganizationRequestCollection()
            };

            foreach (var reg in registrations)
            {
                SetStateRequest state = new SetStateRequest();
                state.State = new OptionSetValue((int)CommonStatus.ACTIVE);
                state.Status = new OptionSetValue((int)EventRegistrationStatusReason.Cancelled);
                state.EntityMoniker = reg.ToEntityReference();
                requestWithResults.Requests.Add(state);
            }

            ExecuteMultipleResponse responseWithResults = (ExecuteMultipleResponse)localContext.OmniContext.Execute(requestWithResults);

        }
        /*
        protected void BeforeUpdateCreate(LocalPluginContext localContext)
        {
            if (localContext == null)
            {
                throw new ArgumentNullException("localContext");
            }

            // stop update call from retrieve plugin
            if (localContext.PluginExecutionContext.Depth != 1)
                return;

            if(localContext.Entity.Contains("oems_registrationapplicationcomponentflag") &&
               localContext.Entity["oems_registrationapplicationcomponentflag"] != null &&
               (bool)localContext.Entity["oems_registrationapplicationcomponentflag"] == true)
            {
               EntityReference subcategoryRef = null;
               Entity preEvent = localContext.OmniContext.Retrieve(localContext.Entity.LogicalName,
                          localContext.Entity.Id, new ColumnSet("oems_subcategory", "oems_registrationtemplate"));
                     
               
               if(localContext.Entity.Contains("oems_subcategory"))
               {
                  if(localContext.Entity["oems_subcategory"] != null)
                  {
                     subcategoryRef = (EntityReference)localContext.Entity["oems_subcategory"];
                  }
               }
               else
               {
                  if(localContext.PluginExecutionContext.MessageName.ToUpper() == "UPDATE")
                  {
                     if(preEvent.Contains("oems_subcategory") && preEvent["oems_subcategory"] != null)
                     {
                         subcategoryRef = (EntityReference)preEvent["oems_subcategory"];
                     }
                  }
               }
               if(subcategoryRef != null)
               {
                   Entity subcategory = localContext.OmniContext.Retrieve(subcategoryRef.LogicalName,
                     subcategoryRef.Id, new ColumnSet("oems_registrationtemplate"));
                   if (subcategory.Contains("oems_registrationtemplate") && subcategory["oems_registrationtemplate"] != null)
                   {
                       localContext.Entity["oems_registrationtemplate"] = subcategory["oems_registrationtemplate"];
                   }

               }
               if (!localContext.Entity.Contains("oems_registrationtemplate") || localContext.Entity["oems_registrationtemplate"] == null)
               {
                  var configuration = (from c in localContext.OmniContext.CreateQuery("oems_configuration")
                                      where (int)c["statecode"] == 0
                                      select c).FirstOrDefault();
                  if (configuration != null && configuration.Contains("oems_defaultregistrationtemplate") && configuration["oems_defaultregistrationtemplate"] != null)
                  {
                      localContext.Entity["oems_registrationtemplate"] = configuration["oems_defaultregistrationtemplate"];
                  }
               }
            }
        }
        */

        protected void RegistrationTemplate(LocalPluginContext localContext)
        {
            if (localContext == null)
            {
                throw new ArgumentNullException("localContext");
            }

            // stop update call from retrieve plugin
            if (localContext.PluginExecutionContext.Depth != 1)
                return;

            Entity oemsEvent = null;

            if (localContext.PluginExecutionContext.PostEntityImages.Contains("PostImage") && localContext.PluginExecutionContext.PostEntityImages["PostImage"] is Entity)
            {
                oemsEvent = localContext.PluginExecutionContext.PostEntityImages["PostImage"].ToEntity<oems_Event>();
            }
            else
            {
                oemsEvent = localContext.Entity.ToEntity<oems_Event>();
            }

            if (oemsEvent.Contains("oems_registrationcomponentflag") == false
                && oemsEvent.Contains("oems_workshopapplicationcomponentflag") == false) return;

            if (!oemsEvent.Attributes.Contains("oems_subcategory"))
            {
                throw new ArgumentNullException("Subcategory is null");
            }



            oems_Event preImage = null;

            if (localContext.PluginExecutionContext.PreEntityImages.Contains("EventPreImage") && localContext.PluginExecutionContext.PreEntityImages["EventPreImage"] is Entity)
            {
                preImage = localContext.PluginExecutionContext.PreEntityImages["EventPreImage"].ToEntity<oems_Event>();
            }


            if (preImage != null && preImage.oems_Subcategory != null && ((EntityReference)oemsEvent.Attributes["oems_subcategory"]).Id != preImage.oems_Subcategory.Id)
            {
                throw new ArgumentNullException("Cannot update event subcategory!");
            }

            //if (preImage == null || oemsEvent.oems_RegistrationApplicationComponentFlag != preImage.oems_RegistrationApplicationComponentFlag)
            if (preImage == null || preImage.oems_RegistrationTemplate == null)
            {
                /*if (oemsEvent.oems_RegistrationApplicationComponentFlag == true)
                {
                    if (oemsEvent.oems_RegistrationTemplate == null)
                    {
                 * */
                // create portal form template
                Entity formTemplate = new oems_portalformtemplate();
                Entity applicationFormTemplate = null;

                Entity copiedEvent = null;

                // if copied from event is not null then copy template
                if (oemsEvent.Attributes.Contains("oems_copiedfromevent"))
                    copiedEvent = localContext.OmniContext.Retrieve(oems_Event.EntityLogicalName, ((EntityReference)oemsEvent.Attributes["oems_copiedfromevent"]).Id, new ColumnSet(new string[] { "oems_registrationtemplate", "oems_registrationapplicationtemplate" })).ToEntity<oems_Event>();

                if (copiedEvent != null && (copiedEvent.Attributes.Contains("oems_registrationtemplate") || copiedEvent.Attributes.Contains("oems_applicationtemplate")))
                {
                    if (copiedEvent.Attributes.Contains("oems_registrationtemplate"))
                        formTemplate.Attributes["oems_copiedformtemplate"] = copiedEvent.Attributes["oems_registrationtemplate"];

                    if (copiedEvent.Attributes.Contains("oems_applicationtemplate"))
                        applicationFormTemplate.Attributes["oems_copiedformtemplate"] = copiedEvent.Attributes["oems_registrationapplicationtemplate"];
                }
                else
                {

                    // get subcategory for template            
                    Entity subcategory = localContext.OmniContext.Retrieve(oems_Subcategory.EntityLogicalName,
                                ((EntityReference)oemsEvent.Attributes["oems_subcategory"]).Id, new ColumnSet(new string[] { "oems_registrationtemplate", "oems_registrationapplicationtemplate" })).ToEntity<oems_Subcategory>();

                    if ((!subcategory.Attributes.Contains("oems_registrationtemplate")) && (!subcategory.Attributes.Contains("oems_registrationapplicationtemplate")))
                        return;

                    if (subcategory.Contains("oems_registrationtemplate"))
                    {
                        formTemplate.Attributes["oems_sourceformtemplate"] = subcategory.Attributes["oems_registrationtemplate"];
                    }

                    if (subcategory.Contains("oems_registrationapplicationtemplate"))
                    {
                        applicationFormTemplate = new oems_portalformtemplate();
                        applicationFormTemplate.Attributes["oems_sourceformtemplate"] = subcategory.Attributes["oems_registrationapplicationtemplate"];
                    }

                }

                // create registration template
                if (!oemsEvent.Attributes.Contains("oems_registrationtemplate") && oemsEvent.Attributes.Contains("oems_registrationcomponentflag")
                        && (bool)oemsEvent.Attributes["oems_registrationcomponentflag"] == true)
                {
                    formTemplate.Attributes["oems_event"] = oemsEvent.ToEntityReference();
                    formTemplate.Attributes["oems_name"] = oemsEvent.Attributes["oems_eventname"].ToString() + " Registration Template";

                    // create form template
                    formTemplate.Id = localContext.OmniContext.Create(formTemplate);
                    oemsEvent.Attributes["oems_registrationtemplate"] = formTemplate.ToEntityReference();
                }

                //Create the form Application Template
                if (!oemsEvent.Attributes.Contains("oems_applicationtemplate") && oemsEvent.Attributes.Contains("oems_workshopapplicationcomponentflag")
                        && (bool)oemsEvent.Attributes["oems_workshopapplicationcomponentflag"] == true)
                {
                    if (applicationFormTemplate != null)
                    {
                        applicationFormTemplate.Attributes["oems_event"] = oemsEvent.ToEntityReference();
                        applicationFormTemplate.Attributes["oems_name"] = oemsEvent.Attributes["oems_eventname"].ToString() + " Application Template";
                        applicationFormTemplate.Id = localContext.OmniContext.Create(applicationFormTemplate);

                        oemsEvent.Attributes["oems_applicationtemplate"] = applicationFormTemplate.ToEntityReference();
                    }
                }

                bool update = false;

                if (formTemplate != null)
                {
                    if (formTemplate.Id != Guid.Empty)
                    {
                        update = true;
                    }
                }

                if (applicationFormTemplate != null)
                {
                    if (applicationFormTemplate.Id != Guid.Empty)
                    {
                        update = true;
                    }
                }

                if (update)
                    localContext.OmniContext.Update(oemsEvent);

            }

        }

        protected void DeleteEvent_PreValidate(LocalPluginContext localContext)
        {
            if (localContext == null)
            {
                throw new ArgumentNullException("localContext");
            }

            // stop update call from retrieve plugin
            if (localContext.PluginExecutionContext.Depth != 1)
                return;

            try
            {
                EntityReference evt = localContext.EntityReference;

                // Courses validation
                var courses = (from c in localContext.OmniContext.CreateQuery("oems_event")
                               where c.GetAttributeValue<EntityReference>("oems_courseterm").Id == evt.Id
                               select c).ToList<Entity>();

                if (courses.Any())
                {
                    throw new InvalidPluginExecutionException("You cannot delete an event with related courses");
                }

                List<Entity> deleteList = new List<Entity>();
                #region Delete Related components

                // Accomodation Request
                var accRequest = (from c in localContext.OmniContext.CreateQuery("oems_accommodationrequest")
                                  where c.GetAttributeValue<EntityReference>("oems_event").Id == evt.Id
                                  select c).ToList<Entity>();
                deleteList.AddRange(accRequest);

                // Caucus Request
                var caucusRequest = (from c in localContext.OmniContext.CreateQuery("oems_caucusrequest")
                                     where c.GetAttributeValue<EntityReference>("oems_event").Id == evt.Id
                                     select c).ToList<Entity>();
                deleteList.AddRange(caucusRequest);

                // Child Care Request
                var childCareRequest = (from c in localContext.OmniContext.CreateQuery("oems_childcarerequest")
                                        where c.GetAttributeValue<EntityReference>("oems_event").Id == evt.Id
                                        select c).ToList<Entity>();
                deleteList.AddRange(childCareRequest);

                // Course Topic
                var courseTopicRequest = (from c in localContext.OmniContext.CreateQuery("oems_coursetopic")
                                          where c.GetAttributeValue<EntityReference>("oems_coursetermpresenterapplication").Id == evt.Id
                                          select c).ToList<Entity>();
                deleteList.AddRange(courseTopicRequest);

                // Event Attendee
                var attendeeRequest = (from c in localContext.OmniContext.CreateQuery("oems_eventattendee")
                                       where c.GetAttributeValue<EntityReference>("oems_event").Id == evt.Id
                                       select c).ToList<Entity>();
                deleteList.AddRange(attendeeRequest);

                // Event Schedule
                var schedule = (from c in localContext.OmniContext.CreateQuery("oems_eventschedule")
                                where c.GetAttributeValue<EntityReference>("oems_applicationschedulesid").Id == evt.Id
                                select c).ToList<Entity>();
                deleteList.AddRange(schedule);

                // Event Workshop Session Request
                var workshopSessionRequest = (from c in localContext.OmniContext.CreateQuery("oems_eventworkshopsessionrequest")
                                              where c.GetAttributeValue<EntityReference>("oems_event").Id == evt.Id
                                              select c).ToList<Entity>();
                deleteList.AddRange(workshopSessionRequest);

                // Personal Accommodation Request
                var perpersonalAccRequest = (from c in localContext.OmniContext.CreateQuery("oems_personalaccommodationrequest")
                                             where c.GetAttributeValue<EntityReference>("oems_event").Id == evt.Id
                                             select c).ToList<Entity>();
                deleteList.AddRange(perpersonalAccRequest);

                // Personal Care Exemption Request
                var personaCareRequest = (from c in localContext.OmniContext.CreateQuery("oems_personalcareexemptionrequest")
                                          where c.GetAttributeValue<EntityReference>("oems_event").Id == evt.Id
                                          select c).ToList<Entity>();
                deleteList.AddRange(personaCareRequest);

                // Release Time Request
                var releaseTimeRequest = (from c in localContext.OmniContext.CreateQuery("oems_releasetimerequest")
                                          where c.GetAttributeValue<EntityReference>("oems_event").Id == evt.Id
                                          select c).ToList<Entity>();
                deleteList.AddRange(releaseTimeRequest);

                // Shortcut
                var shortcut = (from c in localContext.OmniContext.CreateQuery("adx_shortcut")
                                where c.GetAttributeValue<EntityReference>("oems_eventvideo").Id == evt.Id
                                select c).ToList<Entity>();
                deleteList.AddRange(shortcut);

                // Single Room Medical Accomodation 
                var srmRequest = (from c in localContext.OmniContext.CreateQuery("oems_singleroommedicalaccommodationrequest")
                                  where c.GetAttributeValue<EntityReference>("oems_event").Id == evt.Id
                                  select c).ToList<Entity>();
                deleteList.AddRange(srmRequest);

                // Target Grade
                var targetGrade = (from c in localContext.OmniContext.CreateQuery("oems_targetgrade")
                                   where c.GetAttributeValue<EntityReference>("oems_coursetermpresenterapplication").Id == evt.Id
                                   select c).ToList<Entity>();
                deleteList.AddRange(targetGrade);

                // Tiquet Request
                var ticketRequest = (from c in localContext.OmniContext.CreateQuery("oems_ticketrequest")
                                     where c.GetAttributeValue<EntityReference>("oems_event").Id == evt.Id
                                     select c).ToList<Entity>();
                deleteList.AddRange(ticketRequest);

                // Waiting List Request
                var wlRequest = (from c in localContext.OmniContext.CreateQuery("oems_waitinglistrequest")
                                 where c.GetAttributeValue<EntityReference>("oems_event").Id == evt.Id
                                 select c).ToList<Entity>();
                deleteList.AddRange(wlRequest);

                #endregion

                foreach (var item in deleteList)
                {
                    localContext.PluginExecutionContext.SharedVariables["EventRelation_" + item.Id.ToString()] = item.LogicalName;
                }
            }
            catch (Exception e)
            {
                throw (e);
            }
        }

        protected void DeleteEvent_PreOperation(LocalPluginContext localContext)
        {
            if (localContext == null)
            {
                throw new ArgumentNullException("localContext");
            }

            // stop update call from retrieve plugin
            if (localContext.PluginExecutionContext.Depth != 1)
                return;

            try
            {
                // find webpages to delete
                var query = from a in localContext.OmniContext.Adx_webpageSet where a.oems_EventId.Id == localContext.EntityReference.Id select a;
                foreach (var webpage in query)
                {
                    localContext.OmniContext.Delete(Adx_webpage.EntityLogicalName, webpage.Id);
                }

                //Look Up References
                var evt = (from c in localContext.OmniContext.CreateQuery("oems_event")
                           where c.GetAttributeValue<Guid>("oems_eventid") == localContext.EntityReference.Id
                           select c).FirstOrDefault();

                if (evt != null)
                {
                    // Course Package
                    if (evt.GetAttributeValue<EntityReference>("oems_coursepackagedocument1") != null)
                    {
                        localContext.OrganizationService.Delete("oems_eventattachment",
                            evt.GetAttributeValue<EntityReference>("oems_coursepackagedocument1").Id);
                    }

                    // Instructor Course Agreement Blank Form
                    if (evt.GetAttributeValue<EntityReference>("oems_instructorcourseagreementblankform1") != null)
                    {
                        localContext.OrganizationService.Delete("oems_eventattachment",
                            evt.GetAttributeValue<EntityReference>("oems_instructorcourseagreementblankform1").Id);
                    }

                    // Instructor Payroll Enrollment Blank Form
                    if (evt.GetAttributeValue<EntityReference>("oems_instructorpayrollenrollmentblankform1") != null)
                    {
                        localContext.OrganizationService.Delete("oems_eventattachment",
                            evt.GetAttributeValue<EntityReference>("oems_instructorpayrollenrollmentblankform1").Id);
                    }

                    // Sample Participation Letter
                    if (evt.GetAttributeValue<EntityReference>("oems_sampleparticipationletter") != null)
                    {
                        localContext.OrganizationService.Delete("oems_eventattachment",
                            evt.GetAttributeValue<EntityReference>("oems_sampleparticipationletter").Id);
                    }

                    // Letter Template
                    if (evt.GetAttributeValue<EntityReference>("oems_lettertemplate") != null)
                    {
                        localContext.OrganizationService.Delete("oems_eventattachment",
                            evt.GetAttributeValue<EntityReference>("oems_lettertemplate").Id);
                    }

                    // Budget Code 1
                    if (evt.GetAttributeValue<EntityReference>("oems_budgetnumber") != null)
                    {
                        localContext.OrganizationService.Delete("oems_budgetcode",
                            evt.GetAttributeValue<EntityReference>("oems_budgetnumber").Id);
                    }

                    // Budget Code 2
                    if (evt.GetAttributeValue<EntityReference>("oems_budgetnumber2") != null)
                    {
                        localContext.OrganizationService.Delete("oems_budgetcode",
                            evt.GetAttributeValue<EntityReference>("oems_budgetnumber2").Id);
                    }
                }

                var relationsToDelete = localContext.PluginExecutionContext.ParentContext.SharedVariables.Where(rel => rel.Key.StartsWith("EventRelation_")).ToList();
                foreach (var relation in relationsToDelete)
                {
                    var entityName = relation.Value.ToString();
                    var entityId = new Guid(relation.Key.Replace("EventRelation_", ""));
                    localContext.OrganizationService.Delete(entityName, entityId);
                }

                //var x = 0;
                //var b = 5 / x;
            }
            catch (Exception e)
            {
                throw (e);
            }
        }
    }

}


﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Microsoft.Xrm.Sdk;
using Xrm;
using Microsoft.ApplicationBlocks.Data;

namespace Omniware.ETFO.Plugin
{
    public class SequenceGenerator : Plugin
    {

        public SequenceGenerator()
            : base(typeof(SequenceGenerator))
        {
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(20, "Create", oems_EventRegistration.EntityLogicalName.ToLower(), new Action<LocalPluginContext>(EventRegistrationSequence)));
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(20, "Create", oems_AccommodationRequest.EntityLogicalName.ToLower(), new Action<LocalPluginContext>(RequestSequence)));
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(20, "Create", oems_SingleRoomMedicalAccommodationRequest.EntityLogicalName.ToLower(), new Action<LocalPluginContext>(RequestSequence)));
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(20, "Create", oems_PersonalAccommodationRequest.EntityLogicalName.ToLower(), new Action<LocalPluginContext>(RequestSequence)));
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(20, "Create", oems_PersonalAccommodationRequestOption.EntityLogicalName.ToLower(), new Action<LocalPluginContext>(RequestSequence)));
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(20, "Create", oems_PersonalCareExemptionRequest.EntityLogicalName.ToLower(), new Action<LocalPluginContext>(RequestSequence)));
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(20, "Create", oems_CaucusRequest.EntityLogicalName.ToLower(), new Action<LocalPluginContext>(RequestSequence)));
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(20, "Create", oems_ReleaseTimeRequest.EntityLogicalName.ToLower(), new Action<LocalPluginContext>(RequestSequence)));
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(20, "Create", oems_ChildCareRequest.EntityLogicalName.ToLower(), new Action<LocalPluginContext>(RequestSequence)));
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(20, "Create", oems_TicketRequest.EntityLogicalName.ToLower(), new Action<LocalPluginContext>(RequestSequence)));
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(20, "Create", oems_billingrequest.EntityLogicalName.ToLower(), new Action<LocalPluginContext>(RequestSequenceAndName)));
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(20, "Create", oems_eventworkshopsessionrequest.EntityLogicalName.ToLower(), new Action<LocalPluginContext>(RequestSequenceAndName)));
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(20, "Create", "oems_waitinglistrequest", new Action<LocalPluginContext>(RequestSequence)));
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(20, "Create", "oems_cctransaction", new Action<LocalPluginContext>(TransactionSequenceAndName)));
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(20, "Create", "oems_event", new Action<LocalPluginContext>(EventSequence)));
        }

        private Int64 getSeqeunce(String sequenceName, LocalPluginContext localContext)
        {
            DataSet result = SqlHelper.ExecuteDataset(localContext.SqlTransaction, CommandType.Text, "select next value for omniware." + sequenceName + " as nextval");

            Int64 nextVal = (from seq in result.Tables[0].AsEnumerable()
                             select seq.Field<Int64>("nextval")).FirstOrDefault();

            return nextVal;
        }

        protected void EventRegistrationSequence(LocalPluginContext localContext)
        {
            if (localContext == null)
            {
                throw new ArgumentNullException("localContext");
            }

            oems_EventRegistration entity = localContext.Entity.ToEntity<oems_EventRegistration>();

            entity.oems_RegistrationNumber = getSeqeunce("event_registration_seq", localContext).ToString();
        }

        protected void RequestSequence(LocalPluginContext localContext)
        {
            if (localContext == null)
            {
                throw new ArgumentNullException("localContext");
            }

            Entity entity = localContext.Entity;

            entity["oems_requestid"] = getSeqeunce("request_seq", localContext).ToString();
        }

        // for those entities which were defined with oems_name as primary key 
        protected void RequestSequenceAndName(LocalPluginContext localContext)
        {
            if (localContext == null)
            {
                throw new ArgumentNullException("localContext");
            }

            Entity entity = localContext.Entity;

            var sequenceValue = getSeqeunce("request_seq", localContext).ToString();
            entity["oems_requestid"] = sequenceValue;
            entity["oems_name"] = sequenceValue;
        }

        protected void TransactionSequenceAndName(LocalPluginContext localContext)
        {
            if (localContext == null)
            {
                throw new ArgumentNullException("localContext");
            }

            Entity entity = localContext.Entity;

            var sequenceValue = getSeqeunce("request_seq", localContext).ToString();
            entity["oems_orderid"] = sequenceValue;
            entity["oems_name"] = sequenceValue;
        }

        protected void EventSequence(LocalPluginContext localContext)
        {
            if (localContext == null)
            {
                throw new ArgumentNullException("localContext");
            }

            Entity entity = localContext.Entity;

            var sequenceValue = getSeqeunce("event_seq", localContext).ToString();
            entity["oems_event_id"] = sequenceValue;
        }
    }

}

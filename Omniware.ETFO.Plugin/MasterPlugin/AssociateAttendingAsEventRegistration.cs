﻿using System;
using System.IO;
using System.Collections;
using System.Xml.Serialization;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Xrm;
using Microsoft.ApplicationBlocks.Data;
using System.Web.Script.Serialization;
using Microsoft.Xrm.Client.Collections;
using Microsoft.Crm.Sdk.Messages;
using Newtonsoft.Json;
using Microsoft.Xrm.Sdk.Messages;

namespace Omniware.ETFO.Plugin
{
    /// <summary>
    /// This plugin ensures that the registration values are copied to local delegation.
    /// </summary>
    public class AssociateAttendingAsEventRegistration: IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            ITracingService _tracingService = (ITracingService)serviceProvider.GetService(typeof(ITracingService));

            // First, get the context from execution event
            IPluginExecutionContext context = (IPluginExecutionContext)
            serviceProvider.GetService(typeof(IPluginExecutionContext));

            // Obtain the organization service reference.
            IOrganizationServiceFactory serviceFactory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            IOrganizationService _orgService = serviceFactory.CreateOrganizationService(context.UserId);

            UpdateAttendingAsRelationships(context, _orgService);
        }


        private void UpdateAttendingAsRelationships(IPluginExecutionContext localContext,IOrganizationService organizationService)
        {
            string relationship = string.Empty;
            EntityReference parentEntity = null;
            Entity completeParentEntity = null;
            EntityReferenceCollection relatedEntities = null;
            OmniContext omnicontext = new OmniContext(organizationService);

            if ((localContext.MessageName == "Associate") || (localContext.MessageName == "Disassociate"))
            {
                bool associate = (localContext.MessageName == "Associate") ? true : false;

                if (localContext.InputParameters.Contains("Relationship"))
                {
                    relationship = localContext.InputParameters["Relationship"].ToString();
                    relationship = relationship.Trim('.');

                    if ((relationship == "oems_eventregistration_attending_as_nonvotng") || (relationship == "oems_eventregistration_attending_as")
                        || (relationship == "oems_eventregistration_attending_as_voting"))
                    {
                        //Add to attending as
                        // Get the parent entity reference from the Target context
                        if (localContext.InputParameters.Contains("Target") && localContext.InputParameters["Target"] is EntityReference)
                        {
                            parentEntity = (EntityReference)localContext.InputParameters["Target"];
                            completeParentEntity = omnicontext.Retrieve(parentEntity.LogicalName, parentEntity.Id, new ColumnSet(true));
                        }
                        // Get the related child entity reference from the "RelatedEntities" context.InputParameters

                        if (localContext.InputParameters.Contains("RelatedEntities") && localContext.InputParameters["RelatedEntities"] is EntityReferenceCollection)
                        {
                            relatedEntities = localContext.InputParameters["RelatedEntities"] as EntityReferenceCollection;
                            
                        }

                        EntityReference oems_event = (EntityReference)completeParentEntity.Attributes["oems_event"];

                        
                            int countVotingOptions = 0;
                            string realtionShipdirection = null;

                            foreach (EntityReference eref in relatedEntities)
                            {
                                bool votingOpt = false;


                                if (associate)
                                {
                                    if (checkIfExistsOnEventAndVotingRole(oems_event.Id, eref.Id, organizationService, out votingOpt))
                                    {
                                        if (countVotingOptions == 0)
                                        {
                                            countVotingOptions++;


                                            if (ContainsVotingOption(parentEntity.Id,relationship, organizationService))
                                            {
                                                throw (new Exception("The registration already contains a voting option, you can not add more than one. (You should remove first to change the option)"));
                                            }

                                            if ((relationship != "oems_eventregistration_attending_as_voting") && (relationship != "oems_eventregistration_attending_as")) 
                                            {
                                                throw (new Exception("You can not add a NON VOTING OPTION to the Attending As Voting Role relationship"));
                                            }

                                            realtionShipdirection = (relationship == "oems_eventregistration_attending_as_nonvotng") || (relationship == "oems_eventregistration_attending_as_voting") ? "oems_eventregistration_attending_as" : "oems_eventregistration_attending_as_voting";

                                            CallAssociateRequest(organizationService, parentEntity, eref, realtionShipdirection, associate);

                                            if (relationship == "oems_eventregistration_attending_as_voting")
                                            {
                                                checkEventLocalStatus(omnicontext, completeParentEntity,eref);
                                            }
                                        }
                                        else
                                        {
                                            throw (new Exception("You can not add more than one Voting Option to this Event Registration"));
                                        }
                                    }
                                    else
                                    {
                                        if ((relationship != "oems_eventregistration_attending_as_nonvotng") && (relationship != "oems_eventregistration_attending_as")) 
                                        {
                                            throw (new Exception("You can not add a NON VOTING OPTION to the Attending As Voting Role relationship"));
                                        }

                                        realtionShipdirection = (relationship == "oems_eventregistration_attending_as_nonvotng") ? "oems_eventregistration_attending_as" : "oems_eventregistration_attending_as_nonvotng";

                                        CallAssociateRequest(organizationService, parentEntity, eref, realtionShipdirection, associate);
                                    }
                                }
                                else 
                                {
                                    Entity childEntity = omnicontext.Retrieve(eref.LogicalName, eref.Id, new ColumnSet(true));

                                    if ((bool)childEntity.Attributes["oems_votingroleflag"])
                                    {
                                        //if (completeParentEntity.GetAttributeValue<bool>("oems_localdelegationflag"))
                                        //{
                                        //    checkEventLocalStatus(omnicontext, completeParentEntity, eref);
                                        //}
                                        
                                        realtionShipdirection = (relationship == "oems_eventregistration_attending_as_nonvotng") || (relationship == "oems_eventregistration_attending_as_voting") ? "oems_eventregistration_attending_as" : "oems_eventregistration_attending_as_voting";
                                        CallAssociateRequest(organizationService, parentEntity, eref, realtionShipdirection, associate);
                                    }
                                    else 
                                    {
                                        realtionShipdirection = (relationship == "oems_eventregistration_attending_as_nonvotng") || (relationship == "oems_eventregistration_attending_as_voting") ? "oems_eventregistration_attending_as" : "oems_eventregistration_attending_as_nonvotng";
                                        CallAssociateRequest(organizationService, parentEntity, eref, realtionShipdirection, associate);
                                    }

                                }
                            }                          
                    }
                }
            }
            
        }

        private void checkEventLocalStatus(OmniContext omnicontext, Entity completeParentEntity, EntityReference eref)
        {
            if (completeParentEntity.Attributes.Contains("oems_eventlocal"))
            {
                EntityReference local = completeParentEntity.GetAttributeValue<EntityReference>("oems_eventlocal");
                var voting = (from c in omnicontext.CreateQuery(eref.LogicalName)
                              where (Guid)c["oems_attendingasoptionid"] == eref.Id
                                 select c).FirstOrDefault();

                var lastVoting = completeParentEntity.GetAttributeValue<string>("oems_lastvotingsaved");
                if (lastVoting == null || (lastVoting != Convert.ToString(voting.Attributes["oems_optionname"])))
                {
                    var eventLocal = (from c in omnicontext.CreateQuery(local.LogicalName)
                                      where (Guid)c["oems_eventlocalid"] == local.Id
                                      select c).FirstOrDefault();

                    if (((OptionSetValue)eventLocal.Attributes["statuscode"]).Value == EventLocalStatusReason.Approved)
                    {
                        SetStateRequest eventLocalStatusc = new SetStateRequest();
                        eventLocalStatusc.EntityMoniker = local;
                        eventLocalStatusc.State = new OptionSetValue((int)CommonStatus.ACTIVE);
                        eventLocalStatusc.Status = new OptionSetValue(EventLocalStatusReason.Revisit);
                        omnicontext.Execute(eventLocalStatusc);
                    }
                }
            }
        }


        bool ContainsVotingOption(Guid eventRegistrationId,string relationship,IOrganizationService _OrgService) 
        {
            bool result = false;

            string fetchVotingOptions = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='true'>
                      <entity name='oems_attendingasoption'>
                        <attribute name='oems_attendingasoptionid' />
                        <attribute name='oems_optionname' />
                        <attribute name='createdon' />
                        <order attribute='oems_optionname' descending='false' />
                        <link-entity name='oems_eventregistration_attending_as_voting' from='oems_attendingasoptionid' to='oems_attendingasoptionid' visible='false' intersect='true'>
                          <link-entity name='oems_eventregistration' from='oems_eventregistrationid' to='oems_eventregistrationid' alias='ab'>
                            <filter type='and'>
                              <condition attribute='oems_eventregistrationid' operator='eq'  value='" + eventRegistrationId.ToString() + @"' />
                            </filter>
                          </link-entity>
                        </link-entity>
                      </entity>
                    </fetch>";

            EntityCollection resultVoting = _OrgService.RetrieveMultiple(new FetchExpression(fetchVotingOptions));


            if ((relationship == "oems_eventregistration_attending_as") && resultVoting.Entities.Count > 0) 
            {
                return true;
            }

            if ((relationship == "oems_eventregistration_attending_as_voting") && resultVoting.Entities.Count > 1) 
            {
                return true;
            }

            return result;
        }


       
        bool checkIfExistsOnEventAndVotingRole(Guid eventId, Guid optionId, IOrganizationService _OrgService, out bool isVotingOpt) 
        {
            bool result = false;

            isVotingOpt = false;

            string fetchNonVoting = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='true'>
              <entity name='oems_attendingasoption'>
                <attribute name='oems_attendingasoptionid' />
                <attribute name='oems_optionname' />
                <attribute name='createdon' />
                <order attribute='oems_optionname' descending='false' />
                <filter type='and'>
                    <condition attribute='oems_attendingasoptionid' operator='eq' value='" + optionId.ToString() + @"' />
                </filter>
                <link-entity name='oems_event_attending_as_nonvoting' from='oems_attendingasoptionid' to='oems_attendingasoptionid' visible='false' intersect='true'>
                  <link-entity name='oems_event' from='oems_eventid' to='oems_eventid' alias='ab'>
                    <filter type='and'>
                      <condition attribute='oems_eventid' operator='eq' value='" + eventId.ToString() +@"'/>
                       
                    </filter>
                  </link-entity>
                </link-entity>
              </entity>
            </fetch>";

            string fetchVoting = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='true'>
              <entity name='oems_attendingasoption'>
                <attribute name='oems_attendingasoptionid' />
                <attribute name='oems_optionname' />
                <attribute name='createdon' />
                <order attribute='oems_optionname' descending='false' />
                <filter type='and'>
                  <condition attribute='oems_attendingasoptionid' operator='eq' value='" + optionId.ToString() + @"' />
                </filter>
                <link-entity name='oems_event_attending_as_voting' from='oems_attendingasoptionid' to='oems_attendingasoptionid' visible='false' intersect='true'>
                  <link-entity name='oems_event' from='oems_eventid' to='oems_eventid' alias='ak'>
                    <filter type='and'>
                      <condition attribute='oems_eventid' operator='eq' value='" + eventId.ToString() + @"' />
                    </filter>
                  </link-entity>
                </link-entity>
              </entity>
            </fetch>";

            EntityCollection resultVoting = _OrgService.RetrieveMultiple(new FetchExpression(fetchVoting));
            EntityCollection resultNonVoting = _OrgService.RetrieveMultiple(new FetchExpression(fetchNonVoting));

            if ((resultVoting.Entities.Count > 0) | (resultNonVoting.Entities.Count > 0))
            {
                if (resultVoting.Entities.Count > 0)
                {
                    isVotingOpt = true;
                    result = true;
                }
            }
            else 
            {
                throw(new Exception("This Attending As option is not selected for the event"));
            }


            return result;
        }

       

        private void CallAssociateRequest(IOrganizationService _OrgService, EntityReference oemsEventRegistration, 
            EntityReference attendingAsOption,string relationship, bool associate)
        {
            object request = null;
            if (associate)
            {
                request = new AssociateEntitiesRequest
                {
                    Moniker1 = oemsEventRegistration,
                    Moniker2 = attendingAsOption,
                    RelationshipName = relationship,
                };
            }
            else 
            {
                request = new DisassociateEntitiesRequest
                {
                    Moniker1 = oemsEventRegistration,
                    Moniker2 = attendingAsOption,
                    RelationshipName = relationship,
                };
            }

            try
            {
                _OrgService.Execute((OrganizationRequest)request);
            }
            catch (Exception e)
            {
                throw (e);
            }
        }



        
    }
}

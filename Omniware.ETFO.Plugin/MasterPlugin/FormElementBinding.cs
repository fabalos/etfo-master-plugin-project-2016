﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Omniware.ETFO.Plugin
{
    public class FormElementBinding: Plugin
    {
        public FormElementBinding()
            : base(typeof(FormElementBinding))
        {
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "Update", "oems_formelementbinding", new Action<LocalPluginContext>(UpdateRelatedFormTemplates)));

        }

        public void UpdateRelatedFormTemplates(LocalPluginContext localContext)
        {
            if (localContext.PluginExecutionContext.Depth != 1) return;

            IOrganizationService service = localContext.OrganizationService;
            Entity oems_formelementbinding = localContext.PluginExecutionContext.PostEntityImages["PostImage"];

            DataCollection<Entity> relatedEntities = GetRelatedElements(service, oems_formelementbinding.Id);

            List<Entity> updatableTemplates = GetUpdatableTemplates(service, relatedEntities);

            foreach (Entity e in updatableTemplates) 
            {
                e.Attributes["oems_regenerate"] = true;

                service.Update(e);
            }
        }


        DataCollection<Entity> GetRelatedElements(IOrganizationService service, Guid formelementbindingId) 
        {
            string relationName = "oems_formelementbinding_to_portalformelement";
            DataCollection<Entity> result = null;
            
            QueryExpression query = new QueryExpression();
            query.EntityName = "oems_portalformelement";
            query.ColumnSet = new ColumnSet(true);


            Relationship relationship = new Relationship();

            relationship.SchemaName = relationName;

            RelationshipQueryCollection relatedEntity = new RelationshipQueryCollection();
            relatedEntity.Add(relationship, query);
            RetrieveRequest request = new RetrieveRequest();
            request.RelatedEntitiesQuery = relatedEntity;
            request.ColumnSet = new ColumnSet(true);
            request.Target = new EntityReference("oems_formelementbinding", formelementbindingId);
            RetrieveResponse response = (RetrieveResponse)service.Execute(request);

            result = ((DataCollection<Relationship, EntityCollection>)(((RelatedEntityCollection)(response.Entity.RelatedEntities))))[new Relationship(relationName)].Entities;

            return result;
        }

        List<Entity> GetUpdatableTemplates(IOrganizationService service, DataCollection<Entity> formElements) 
        {
            List<Entity> entities = new List<Entity>();

            foreach (Entity e in formElements) 
            {
                EntityReference eref = (EntityReference)e.Attributes["oems_formtemplate"];
                if (entities.Count == 0)
                {

                    entities.Add(GetRelatedFormTemlates(service, eref.Id, eref.LogicalName));
                }
                else 
                {
                    if (entities.Find(x => x.Id == eref.Id) == null) 
                    {
                        entities.Add(GetRelatedFormTemlates(service, eref.Id, eref.LogicalName));
                    }
                }
            }

            return entities;
        }

        Entity GetRelatedFormTemlates(IOrganizationService service, Guid portalFormTemplateId,string entityName) 
        {
            Entity portalFormTemplate = service.Retrieve(entityName, portalFormTemplateId, new ColumnSet(true));

            return portalFormTemplate;

        }
        
    }
}

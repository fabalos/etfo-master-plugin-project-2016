﻿using System;
using System.Diagnostics;
using System.IO;
using System.Collections;
using System.Xml.Serialization;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Omniware.ETFO.Plugin.Shared;
using Xrm;
using Microsoft.ApplicationBlocks.Data;
using System.Web.Script.Serialization;
using Microsoft.Xrm.Client.Collections;
using Microsoft.Crm.Sdk.Messages;
using Newtonsoft.Json;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using log4net;
using Omniware.ETFO.Plugin.Billing;
using Omniware.ETFO.Plugin.DataWork;
using Shared.Utilities;
using Omniware.ETFO.Plugin.MasterPlugin;

namespace Omniware.ETFO.Plugin
{
    public class EventRegistration : Plugin
    {
        protected string config;
        protected string secConfig;

        private ILog Log = LogHelper.GetLogger(typeof(EventRegistration));

        public EventRegistration(string unsecureConfig, string secureConfig)
            : base(typeof(EventRegistration))
        {
            if (unsecureConfig != null)
            {
                config = unsecureConfig;
            }

            if (secureConfig != null)
            {
                secConfig = secureConfig;
            }


            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "Retrieve", "oems_eventregistration", new Action<LocalPluginContext>(RetrieveEventRegistration)));
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(20, "Update", "oems_eventregistration", new Action<LocalPluginContext>(ValidateStatusCodes)));
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "Update", "oems_eventregistration", new Action<LocalPluginContext>(UpdateEventRegistration)));
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "SetState", "oems_eventregistration", new Action<LocalPluginContext>(SetStateCode)));
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "SetStateDynamicEntity", "oems_eventregistration", new Action<LocalPluginContext>(SetStateCode)));
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(20, "Create", "oems_eventregistration", new Action<LocalPluginContext>(SetAttendeeRegistrationListFlag)));
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "Create", "oems_eventregistration", new Action<LocalPluginContext>(SetAccountPersonalAccommodation)));

        }

        protected void SetAttendeeRegistrationListFlag(LocalPluginContext localContext)
        {
            if (localContext == null)
                throw new ArgumentNullException("localContext");

            localContext.Entity["oems_attendeeregistrationlistflag"] = true;
        }

        protected void RetrieveEventRegistration(LocalPluginContext localContext)
        {
            if (localContext == null)
                throw new ArgumentNullException("localContext");


            // stop recurrsive calls
            if (localContext.PluginExecutionContext.Depth != 1)
                return;

            Entity entity = (Entity)localContext.PluginExecutionContext.OutputParameters["BusinessEntity"];


            // get EventRegistration
            oems_EventRegistration oemsEventRegistration =
                (from a in localContext.OmniContext.oems_EventRegistrationSet
                 where a.oems_EventRegistrationId == entity.Id
                 select a).Single();


            // get Event
            oems_Event oemsEvent =
                (from a in localContext.OmniContext.oems_EventSet
                 where a.oems_EventId == oemsEventRegistration.oems_Event.Id
                 select a).Single();


            string[] eventColumns = { "oems_registrationtemplate", "oems_applicationtemplate" };
            Entity fullEvent = localContext.OrganizationService.Retrieve(oemsEvent.LogicalName, oemsEvent.Id, new ColumnSet(eventColumns));
            Entity fullRegistration = localContext.OrganizationService.Retrieve(oemsEventRegistration.LogicalName, oemsEventRegistration.Id, new ColumnSet("oems_registrationtype"));
            Entity oemsPortalFormTemplate = new Entity();

            if (fullRegistration.Attributes.Contains("oems_registrationtype") &&
                    ((OptionSetValue)fullRegistration.Attributes["oems_registrationtype"]).Value == Convert.ToInt32(RegistrationType.PRESENTER))
            {
                oemsPortalFormTemplate =
                    localContext.OrganizationService.Retrieve("oems_portalformtemplate", ((EntityReference)fullEvent.Attributes["oems_applicationtemplate"]).Id, new ColumnSet("oems_formdata", "oems_updatedata"));
            }
            else
            {
                oemsPortalFormTemplate =
                        localContext.OrganizationService.Retrieve("oems_portalformtemplate", ((EntityReference)fullEvent.Attributes["oems_registrationtemplate"]).Id, new ColumnSet("oems_formdata", "oems_updatedata"));
            }

            //Account userAccount = localContext.OmniContext.AccountSet.Where(x => x.Id == oemsEventRegistration.oems_Account.Id).Single<Account>();
            Contact userContact = localContext.OmniContext.ContactSet.Where(x => x.Id == oemsEventRegistration.oems_Contact.Id).Single<Contact>();

            
            if (oemsPortalFormTemplate.Id != Guid.Empty)
            {
                Dictionary<string, string> rolesInfo = LoadRolesInformation(config);

                DataWork.FormElement dynamicForm = JsonConvert.DeserializeObject<DataWork.FormElement>(oemsPortalFormTemplate.Attributes["oems_formdata"].ToString());

                var watch = new Stopwatch();
                watch.Start();

                DataWork.DataAccess dataAccess = new DataWork.DataAccess(localContext.OmniContext);

                if (watch.ElapsedMilliseconds > 1) Log.Info("new DataAccess" + watch.ElapsedMilliseconds);
                watch.Restart();

                dataAccess.AdjustFormElementToRoles(localContext, ref dynamicForm, userContact, oemsEvent, rolesInfo, oemsPortalFormTemplate, oemsEventRegistration);

                Log.DebugFormat("AdjustFormElementToRoles {0} msec", watch.ElapsedMilliseconds);
                watch.Restart();

                dataAccess.LoadDynamicFormData(dynamicForm, oemsEventRegistration);

                Log.DebugFormat("Overall LoadDynamicFormData {0} msec", watch.ElapsedMilliseconds);
                watch.Restart();

                string dynamicregformjson = JsonConvert.SerializeObject(dynamicForm).Replace("\"selected\":", "\"checked\":").Replace("\"true\"", "true").Replace("\"false\"", "false").Replace("\\\\r\\\\n", "\\n");
                dynamicregformjson = EncodeJson.EncodeNonAsciiCharacters(dynamicregformjson);
                entity["oems_dynamicregformjson"] = dynamicregformjson;
            }

            // set attenging as value
            AttendingAs attendingAs = RetrieveAttendingAs(localContext, oemsEvent, oemsEventRegistration);
            if (attendingAs != null)
            {
                var attendingAsJSON = JsonConvert.SerializeObject(attendingAs);
                entity["oems_retrieveattendingasdata"] = attendingAsJSON.ToString();
            }

            //Annotation annotation = RetrieveAnnotation(localContext, oemsEvent, oemsEventRegistration);
            //if (attendingAs != null)
            //{
            //    var annotationJSON = JsonConvert.SerializeObject(annotation);
            //    entity["oems_retrieveannotationdata"] = annotationJSON.ToString();
            //}

        }

        public void SetEventFieldOnCreate(Entity entity, DataWork.DataAccess da)
        {

            if (da.RootEntity != null)
            {
                Microsoft.Xrm.Sdk.Metadata.AttributeMetadata amEvent = null;
                try
                {
                    amEvent = da.metaData.GetAttributeMetadata(entity.LogicalName, "oems_event");
                }
                catch (Exception ex)
                {
                    try
                    {
                        amEvent = da.metaData.GetAttributeMetadata(entity.LogicalName, "oems_eventid");
                    }
                    catch (Exception ex1)
                    {
                    }
                }

                Microsoft.Xrm.Sdk.Metadata.AttributeMetadata amEventReg = null;
                try
                {
                    amEventReg = da.metaData.GetAttributeMetadata(entity.LogicalName, "oems_eventregistration");
                }
                catch (Exception ex)
                {
                    try
                    {
                        amEventReg = da.metaData.GetAttributeMetadata(entity.LogicalName, "oems_eventregistrationid");
                    }
                    catch (Exception ex1)
                    {
                    }
                }

                Entity updEntity = null;

                if (amEvent != null && !entity.Contains(amEvent.LogicalName))
                {
                    if (updEntity == null) updEntity = new Entity(entity.LogicalName);
                    updEntity.Id = entity.Id;
                    updEntity[amEvent.LogicalName] = da.RootEntity["oems_event"];
                }

                if (amEventReg != null && !entity.Contains(amEventReg.LogicalName))
                {
                    if (updEntity == null) updEntity = new Entity(entity.LogicalName);
                    updEntity.Id = entity.Id;
                    updEntity[amEventReg.LogicalName] = da.RootEntity.ToEntityReference();
                }

                if (updEntity != null)
                {
                    da.context.Update(updEntity);
                }
            }
        }

        protected void SetTrackingFields(LocalPluginContext localContext)
        {
            localContext.Entity["oems_decisiontimestamp"] = DateTime.UtcNow;
            localContext.Entity["oems_decisionuser"] = new EntityReference("systemuser", localContext.PluginExecutionContext.UserId);
        }

        //Set the payment method to create the notification tasks
        protected void SetPaymentMethod(LocalPluginContext localContext, Guid registrationId)
        {
            Entity billingRequest = BillingUtil.GetRequest(localContext.OmniContext, registrationId, "oems_billingrequest");
            if (billingRequest != null)
            {
                localContext.Entity["oems_paymentmethod"] = billingRequest.GetAttributeValue<OptionSetValue>("oems_paymentmethod");
            }
        }


        //TODO Mauro
        protected void copyRegistrationFields(LocalPluginContext localContext, ref oems_EventRegistration eventReg)
        {
            Guid registrationId = eventReg.Id;

            oems_EventRegistration reg = (from a in localContext.OmniContext.oems_EventRegistrationSet
                                          where a.oems_EventRegistrationId == registrationId
                                          select a).Single();

            //Guid accountId = reg.GetAttributeValue<EntityReference>("oems_account").Id;
            Guid contactId = reg.GetAttributeValue<EntityReference>("oems_contact").Id;

            //Account account = (from a in localContext.OmniContext.AccountSet
            //                   where a.AccountId == accountId
            //                   select a).Single();

            Contact contact = (from a in localContext.OmniContext.ContactSet
                               where a.ContactId == contactId
                               select a).Single();

            if (reg.GetAttributeValue<string>("username") == null)
            {
                //contact.oems_emerge
                eventReg.Attributes["oems_username"] = contact.Adx_username;
                eventReg.Attributes["oems_emergencycontact_description"] = contact.oems_EmergencyContactDescription;
                eventReg.Attributes["oems_emergencycontact_name"] = contact.oems_emergencycontact_name;
                eventReg.Attributes["oems_emergencycontact_phone"] = contact.oems_emergencycontactphone;
            }
        }

        protected void ValidateStatusCodes(LocalPluginContext localContext)
        {
            if (localContext == null)
                throw new ArgumentNullException("localContext");

            // stop recurrsive calls
            if (localContext.PluginExecutionContext.Depth != 1)
                return;

            oems_EventRegistration oemsEventRegistration = localContext.Entity.ToEntity<oems_EventRegistration>();
            if (oemsEventRegistration.statuscode == null)
            {
                return;
            }

            try
            {
                oems_EventRegistration pre_oemsEventRegistration = localContext.PluginExecutionContext.PreEntityImages["PreImage"].ToEntity<oems_EventRegistration>();
                Microsoft.Xrm.Client.CrmEntityReference preOemsEventRef = pre_oemsEventRegistration.oems_Event;
                Microsoft.Xrm.Client.CrmEntityReference oemsEventRef = oemsEventRegistration.oems_Event;
                if (oemsEventRef == null) oemsEventRef = preOemsEventRef;

                if (oemsEventRef == null)
                {
                    throw new OEMSPluginException("There is no event for this registration.");
                }

                oems_Event oemsEvent = (from a in localContext.OmniContext.oems_EventSet where a.oems_EventId == oemsEventRef.Id select a).Single();

                bool pushReleaseTimeRequests = false;
                if (oemsEventRegistration.statuscode.Value == EventRegistrationStatusReason.Cancelled)
                {
                    if (pre_oemsEventRegistration.statuscode != EventRegistrationStatusReason.Approved
                        && pre_oemsEventRegistration.statuscode != EventRegistrationStatusReason.Created
                        && pre_oemsEventRegistration.statuscode != EventRegistrationStatusReason.Submitted
                        && pre_oemsEventRegistration.statuscode != EventRegistrationStatusReason.Cancelled)
                        throw new OEMSPluginException("This registration cannot be cancelled.");

                    //if (pre_oemsEventRegistration.statuscode == EventRegistrationStatusReason.Approved && oemsEvent.statuscode.Value != (int)EventStatusReason.CANCELLED
                    //    && localContext.PluginExecutionContext.Depth == 1)
                    //{
                    //    throw new OEMSPluginException("Cannot cancel registrations for this event.");
                    //}

                    SetPaymentMethod(localContext, oemsEventRegistration.Id);
                    checkEventLocalStatus(localContext, oemsEventRegistration);
                    oemsEventRegistration.Attributes["oems_cancelledflag"] = true;
                    pushReleaseTimeRequests = true;
                    SetTrackingFields(localContext);
                }
                else if (oemsEventRegistration.statuscode.Value == EventRegistrationStatusReason.Withdrawn)
                {
                    if (oemsEvent.statuscode.Value == (int)EventStatusReason.END_EVENT || oemsEvent.statuscode.Value == (int)EventStatusReason.COMPLETED
                        || oemsEvent.statuscode.Value == (int)EventStatusReason.CANCELLED)
                        throw new OEMSPluginException("Cannot withdraw registrations for this event.");

                    if (pre_oemsEventRegistration.statuscode != EventRegistrationStatusReason.Approved)
                    {
                        throw new OEMSPluginException("This registration cannot be withdrawn.");
                    }

                    SetPaymentMethod(localContext, oemsEventRegistration.Id);
                    oemsEventRegistration.Attributes["oems_withdrawnflag"] = true;
                    checkEventLocalStatus(localContext, oemsEventRegistration);
                    pushReleaseTimeRequests = true;
                    SetTrackingFields(localContext);
                }
                else if (oemsEventRegistration.statuscode.Value == EventRegistrationStatusReason.Approved && pre_oemsEventRegistration.statuscode.Value != EventRegistrationStatusReason.Approved)
                {
                    if (oemsEvent.statuscode.Value == (int)EventStatusReason.END_EVENT || oemsEvent.statuscode.Value == (int)EventStatusReason.COMPLETED || oemsEvent.statuscode.Value == (int)EventStatusReason.CANCELLED)
                    {
                        throw new OEMSPluginException("Related event is already closed.");
                    }

                    if (pre_oemsEventRegistration.statuscode == null
                        || ((pre_oemsEventRegistration.statuscode.Value != EventRegistrationStatusReason.Submitted)
                            && (pre_oemsEventRegistration.statuscode.Value != EventRegistrationStatusReason.Approved)
                            && (pre_oemsEventRegistration.statuscode.Value != EventRegistrationStatusReason.Rejected)))
                    {
                        throw new OEMSPluginException("This registration cannot be approved.");
                    }

                    //var releaseTimeRequests =
                    //    from a in localContext.OmniContext.oems_ReleaseTimeRequestSet
                    //    where a.oems_EventRegistration.Id == oemsEventRegistration.Id
                    //    select a;

                    //foreach (var r in releaseTimeRequests)
                    //{
                    //    if (r.statecode == 0 && r.statuscode != (int)ReleaseTimeRequestStatusReason.EVENTOWNERAPPROVED)
                    //    {
                    //        throw new OEMSPluginException("Please approve related release time requests first.");
                    //    }
                    //}

                    oemsEventRegistration["oems_approvedflag"] = true;
                    SendCoursePackageEmailNotification(localContext, oemsEventRegistration.Id);
                    pushReleaseTimeRequests = true;
                    SetTrackingFields(localContext);
                }
                else if (oemsEventRegistration.statuscode.Value == EventRegistrationStatusReason.Rejected && pre_oemsEventRegistration.statuscode.Value != EventRegistrationStatusReason.Rejected)
                {
                    if ((pre_oemsEventRegistration.statuscode != EventRegistrationStatusReason.Submitted
                        && pre_oemsEventRegistration.statuscode != EventRegistrationStatusReason.Approved))
                    {
                        throw new InvalidPluginExecutionException("This registration cannot be rejected.");
                    }

                    checkEventLocalStatus(localContext, oemsEventRegistration);
                    pushReleaseTimeRequests = true;
                    SetTrackingFields(localContext);
                }
                else if (oemsEventRegistration.statuscode.Value == EventRegistrationStatusReason.Submitted && pre_oemsEventRegistration.statuscode.Value != EventRegistrationStatusReason.Submitted)
                {
                    CheckOverlapping(localContext, oemsEventRegistration.Id, oemsEventRef.Id);

                    if (HasWaitingListRequest(localContext, oemsEventRegistration.Id))
                    {
                        oemsEventRegistration["oems_attendeeregistrationlistflag"] = true;
                    }

                    if (pre_oemsEventRegistration.statuscode.Value == EventRegistrationStatusReason.Initial
                        || pre_oemsEventRegistration.statuscode.Value == EventRegistrationStatusReason.Created)
                    {
                        oemsEventRegistration["oems_submittedflag"] = true;
                    }

                    if (pre_oemsEventRegistration.statuscode.Value == EventRegistrationStatusReason.Initial)
                    {
                        copyRegistrationFields(localContext, ref oemsEventRegistration);
                    }
                }
                else if (oemsEventRegistration.statuscode.Value == EventRegistrationStatusReason.Initial)
                {
                    if (pre_oemsEventRegistration.statuscode.Value == EventRegistrationStatusReason.Initial)
                    {
                        copyRegistrationFields(localContext, ref oemsEventRegistration);
                    }
                }

                //throw new Exception(pushReleaseTimeRequests.ToString());
                //Push release time request changes to member records if needed
                if (pushReleaseTimeRequests)
                {
                    var releaseTimeRequests = from a in localContext.OmniContext.oems_ReleaseTimeRequestSet where a.oems_EventRegistration.Id == oemsEventRegistration.Id select a;
                    foreach (var r in releaseTimeRequests)
                    {
                        Entity upd = new Entity(r.LogicalName);
                        upd.Id = r.Id;
                        upd["oems_pushtomemberrecords"] = true;
                        localContext.OmniContext.Update(upd);

                        SetStateRequest setState = new SetStateRequest();
                        setState.EntityMoniker = r.ToEntityReference();
                        setState.State = new OptionSetValue();
                        setState.State.Value = 0;
                        setState.Status = new OptionSetValue();
                        setState.Status.Value = (oemsEventRegistration.statuscode.Value == EventRegistrationStatusReason.Approved) ? (int)ReleaseTimeRequestStatusReason.APPROVALPENDING : (int)ReleaseTimeRequestStatusReason.CANCELLED;
                        SetStateResponse setStateResponse = (SetStateResponse)localContext.OmniContext.Execute(setState);
                        //if(oemsEventRegistration.statuscode.Value != EventRegistrationStatusReason.Approved)
                        //{
                        //       SetStateRequest setState = new SetStateRequest();
                        //       setState.EntityMoniker = r.ToEntityReference();
                        //       setState.State = new OptionSetValue();
                        //       setState.State.Value = 0;
                        //       setState.Status = new OptionSetValue();
                        //       setState.Status.Value = (int)ReleaseTimeRequestStatusReason.CANCELLED;
                        //       SetStateResponse setStateResponse = (SetStateResponse)localContext.OmniContext.Execute(setState);
                        //}
                    }
                }
            }
            catch (OEMSPluginException ex)
            {
                throw new OEMSPluginException(ex.Message);
            }
        }


        //TODO FEDERICO
        private void CheckOverlapping(LocalPluginContext localContext, Guid registrationId, Guid eventId)
        {
            StringManager stringManager = new StringManager(localContext.OrganizationService);

            Entity evt = (from a in localContext.OmniContext.CreateQuery("oems_event")
                          where (Guid)a["oems_eventid"] == eventId &&
                                (bool)a["oems_courseflag"] == true
                          select a).FirstOrDefault();

            if (evt == null) return;

            Entity fullReg = (from r in localContext.OmniContext.CreateQuery("oems_eventregistration")
                              where (Guid)r["oems_eventregistrationid"] == registrationId
                              select r).FirstOrDefault();

            EntityReference courseTermRef = evt.GetAttributeValue<EntityReference>("oems_courseterm");

            List<Entity> courses = (from c in localContext.OmniContext.CreateQuery("oems_event")
                                    join r in localContext.OmniContext.CreateQuery("oems_eventregistration") on
                                        (Guid)c["oems_eventid"] equals ((EntityReference)r["oems_event"]).Id
                                    where ((EntityReference)r["oems_contact"]).Id == ((EntityReference)fullReg["oems_contact"]).Id
                                    where ((EntityReference)c["oems_courseterm"]).Id == courseTermRef.Id
                                            && (Guid)c["oems_eventid"] != evt.Id
                                    select c).ToList<Entity>();

            if (courses == null) return;

            DateTime actualStartDate = evt.GetAttributeValue<DateTime>("oems_startdate");
            DateTime actualEndDate = evt.GetAttributeValue<DateTime>("oems_enddate");

            foreach (var course in courses)
            {
                DateTime courseStartDate = course.GetAttributeValue<DateTime>("oems_startdate");
                DateTime courseEndDate = course.GetAttributeValue<DateTime>("oems_enddate");

                if ((actualStartDate >= courseStartDate && actualStartDate <= courseEndDate) ||
                    (actualEndDate >= courseStartDate && actualStartDate <= courseEndDate))
                {
                    StringBuilder message = new StringBuilder(course.GetAttributeValue<string>("oems_eventname"));
                    message.Append(" ,").Append(courseStartDate).Append(" to ").Append(courseEndDate).Append(".");

                    string validationMessage = stringManager.getString(StringCodes.COURSE_OVERLAPPING_VALIDATION_MESSAGE, "");

                    validationMessage = validationMessage.Replace("#Course Information#", message.ToString());

                    throw new InvalidPluginExecutionException(validationMessage);
                }

            }
        }

        private void SendCoursePackageEmailNotification(LocalPluginContext localContext, Guid regId)
        {
            oems_EventRegistration fullReg = (from r in localContext.OmniContext.oems_EventRegistrationSet
                                              where (Guid)r.Id == regId
                                              select r).FirstOrDefault();

            Entity evt = (from a in localContext.OmniContext.CreateQuery("oems_event")
                          where (Guid)a["oems_eventid"] == fullReg.oems_Event.Id
                          select a).FirstOrDefault();

            if (evt.GetAttributeValue<bool>("oems_coursepackageavailableflag"))
            {
                //Contact change done
                //Account account = (from a in localContext.OmniContext.AccountSet
                //                   where (Guid)a.AccountId == ((EntityReference)fullReg["oems_account"]).Id
                //                   select a).FirstOrDefault();

                Contact contact = (from c in localContext.OmniContext.ContactSet
                                   where (Guid)c.ContactId == ((EntityReference)fullReg["oems_contact"]).Id
                                   select c).FirstOrDefault();

                CrmTools.RunEmailNotification(contact, localContext.OrganizationService, StringCodes.COURSE_PACKAGE_EMAIL_FROM, StringCodes.COURSE_PACKAGE_EMAIL_HTML_BODY,
                    StringCodes.COURSE_PACKAGE_EMAIL_SUBJECT);
            }


        }

        private bool HasWaitingListRequest(LocalPluginContext localContext, Guid regId)
        {
            var result = (from wl in localContext.OmniContext.CreateQuery("oems_waitinglistrequest")
                          where wl.GetAttributeValue<EntityReference>("oems_eventregistration").Id == regId
                          select wl).FirstOrDefault();

            if (result != null)
            {
                return true;
            }

            return false;
        }

        private void checkEventLocalStatus(LocalPluginContext localContext, oems_EventRegistration oemsEventRegistration)
        {
            var fullRegistration = (from c in localContext.OmniContext.CreateQuery(oemsEventRegistration.LogicalName)
                                    where (Guid)c["oems_eventregistrationid"] == oemsEventRegistration.Id
                                    select c).FirstOrDefault();

            if (fullRegistration.GetAttributeValue<bool>("oems_localdelegationflag"))
            {
                EntityReference local = fullRegistration.GetAttributeValue<EntityReference>("oems_eventlocal");

                var eventLocal = (from c in localContext.OmniContext.CreateQuery(local.LogicalName)
                                  where (Guid)c["oems_eventlocalid"] == local.Id
                                  select c).FirstOrDefault();

                if (((OptionSetValue)eventLocal.Attributes["statuscode"]).Value == EventLocalStatusReason.Approved)
                {
                    SetStateRequest eventLocalStatusc = new SetStateRequest();
                    eventLocalStatusc.EntityMoniker = local;
                    eventLocalStatusc.State = new OptionSetValue((int)CommonStatus.ACTIVE);
                    eventLocalStatusc.Status = new OptionSetValue(EventLocalStatusReason.Revisit);
                    localContext.OrganizationService.Execute(eventLocalStatusc);
                }
            }
        }

        protected void UpdateEventRegistration(LocalPluginContext localContext)
        {
            if (localContext == null)
                throw new ArgumentNullException("localContext");

            // stop recurrsive calls
            if (localContext.PluginExecutionContext.Depth != 1)
                return;

            var preImageOemsEventRegistration = localContext.PluginExecutionContext.PreEntityImages["PreImage"].ToEntity<oems_EventRegistration>();
            var oemsEventRegistration = localContext.Entity.ToEntity<oems_EventRegistration>();
            var evt = localContext.OmniContext.Retrieve(preImageOemsEventRegistration.oems_Event.LogicalName, preImageOemsEventRegistration.oems_Event.Id, new ColumnSet("oems_billingcomponentflag"));
            var billingComponentFlag = ((bool)evt["oems_billingcomponentflag"]);

            #region Check for registration data updates
            if (oemsEventRegistration.Contains("oems_dynamicregformdata") && oemsEventRegistration["oems_dynamicregformdata"] != null &&
                (
                    !preImageOemsEventRegistration.Contains("oems_dynamicregformdata") ||
                    preImageOemsEventRegistration["oems_dynamicregformdata"] == null ||
                    billingComponentFlag ||
                    (string)preImageOemsEventRegistration["oems_dynamicregformdata"] != (string)oemsEventRegistration["oems_dynamicregformdata"])
                )
            {
                oems_EventRegistration postImageOemsEventRegistration = localContext.OmniContext.Retrieve(oemsEventRegistration.LogicalName, oemsEventRegistration.Id, new ColumnSet(true)).ToEntity<oems_EventRegistration>();
                oems_Event oemsEvent = (from a in localContext.OmniContext.oems_EventSet where a.oems_EventId == postImageOemsEventRegistration.oems_Event.Id select a).Single();
                string[] eventColumns = { "oems_registrationtemplate", "oems_applicationtemplate" };

                Entity fullEvent = localContext.OrganizationService.Retrieve(oemsEvent.LogicalName, oemsEvent.Id, new ColumnSet(eventColumns));
                //Contact Change Done
                Entity fullRegistration = localContext.OrganizationService.Retrieve(oemsEventRegistration.LogicalName, oemsEventRegistration.Id, new ColumnSet("oems_contact", "oems_registrationtype"));
                Entity oemsPortalFormTemplate = new Entity();
                if (fullRegistration.Attributes.Contains("oems_registrationtype"))
                {
                    if (((OptionSetValue)fullRegistration.Attributes["oems_registrationtype"]).Value ==
                        Convert.ToInt32(RegistrationType.ATTENDEE))
                    {
                        oemsPortalFormTemplate =
                            localContext.OrganizationService.Retrieve("oems_portalformtemplate", ((EntityReference)fullEvent.Attributes["oems_registrationtemplate"]).Id, new ColumnSet("oems_formdata"));
                    }
                    else if (((OptionSetValue)fullRegistration.Attributes["oems_registrationtype"]).Value ==
                             Convert.ToInt32(RegistrationType.PRESENTER))
                    {
                        oemsPortalFormTemplate =
                            localContext.OrganizationService.Retrieve("oems_portalformtemplate", ((EntityReference)fullEvent.Attributes["oems_applicationtemplate"]).Id, new ColumnSet("oems_formdata"));
                    }
                }

                DataWork.FormElement form = JsonConvert.DeserializeObject<DataWork.FormElement>(oemsPortalFormTemplate.Attributes["oems_formdata"].ToString());
                List<DataWork.NameValuePair> listData = new List<DataWork.NameValuePair>(JsonConvert.DeserializeObject<DataWork.NameValuePair[]>((string)oemsEventRegistration["oems_dynamicregformdata"]));

                var query = new QueryExpression()
                {
                    EntityName = "oems_attendingasoption",
                    ColumnSet = new ColumnSet(true)
                };

                var link = query.AddLink("oems_eventregistration_attending_as_voting", "oems_attendingasoptionid", "oems_attendingasoptionid");
                link.LinkCriteria = new FilterExpression()
                {
                    Conditions =
                    {
                        new ConditionExpression("oems_eventregistrationid", ConditionOperator.Equal, postImageOemsEventRegistration.Id)
                    }
                };

                var votingEntities = localContext.OmniContext.RetrieveMultiple(query);
                if (votingEntities.Entities.Count > 0)
                {
                    postImageOemsEventRegistration.Attributes["oems_lastvotingsaved"] =
                        (string)votingEntities[0].Attributes["oems_optionname"];
                }
                else
                {
                    postImageOemsEventRegistration.Attributes["oems_lastvotingsaved"] = "";
                }

                bool statusInitial = false;

                statusInitial = (postImageOemsEventRegistration.statuscode == EventRegistrationStatusReason.Initial ? true : false);

                DataWork.DataAccess dataAccess = new DataWork.DataAccess(localContext.OmniContext);
                dataAccess.RootEntity = postImageOemsEventRegistration;
                dataAccess.OnCreateRecord += SetEventFieldOnCreate;


                dataAccess.SaveForm(form, listData, postImageOemsEventRegistration);

                //Loads he attendingAs Json
                SetAttendingAsData(localContext.OmniContext, fullEvent.Id, fullRegistration.Id);

                // update status
                if (statusInitial)
                {
                    SetStateRequest jj_SetCustomEntituStatus = new SetStateRequest();
                    // Add entity refrence for which you want to set the status 
                    // In Entity refrence first paramter is Entity name and second paramter is ENtity Id                   
                    jj_SetCustomEntituStatus.EntityMoniker =
                        new EntityReference(oems_EventRegistration.EntityLogicalName, postImageOemsEventRegistration.Id);
                    // Set State Value (Entity statecode/status code field value)
                    jj_SetCustomEntituStatus.State = new OptionSetValue((int)CommonStatus.ACTIVE);
                    // Set Status value (Entity statuscode/status reason filed value)
                    jj_SetCustomEntituStatus.Status = new OptionSetValue(EventRegistrationStatusReason.Created);
                    // Pass request to CRM Service execute method
                    localContext.OmniContext.Execute(jj_SetCustomEntituStatus);
                }

            }  // end of if block which has 100 lines in it!
            else
            {
                Log.Info("No changes detected in the reg form, SaveForm not fired.");
            }
            #endregion
            #region If necessary do refund

            if (oemsEventRegistration.statuscode != null && oemsEventRegistration.statuscode.Value != preImageOemsEventRegistration.statuscode.Value)
            {
                if (oemsEventRegistration.statuscode.Value == EventRegistrationStatusReason.Cancelled ||
                    oemsEventRegistration.statuscode.Value == EventRegistrationStatusReason.Withdrawn ||
                    oemsEventRegistration.statuscode.Value == EventRegistrationStatusReason.Rejected)
                {
                    oems_Event oemsEvent = (
                        from a in localContext.OmniContext.oems_EventSet
                        where a.oems_EventId == preImageOemsEventRegistration.oems_Event.Id
                        select a).Single();

                    bool isWithdrawn = oemsEventRegistration.statuscode.Value == EventRegistrationStatusReason.Withdrawn;
                    var billingCalculator = new BillingCalculator(localContext.OmniContext, oemsEventRegistration, oemsEvent);
                    billingCalculator.Refund(preImageOemsEventRegistration.oems_Contact.Id, isWithdrawn);
                }
            }

            #endregion
        }

        private void SetAttendingAsData(OmniContext serviceContext, Guid eventId, Guid registrationId)
        {
            // get event attending as options
            var oemsEvent = (from a in serviceContext.oems_EventSet where a.oems_EventId == eventId select a).Single();
            var eventAttendingAsList =
            (
                from a in oemsEvent.oems_event_attending_as_config
                select new AttendingAsOption
                {
                    id = a.oems_attendingasoptionId.Value,
                    selected = a.ToEntityReference(),
                    name = a.oems_OptionName,
                    isVotingRole = a.oems_VotingRoleFlag,
                    isGuestRole = a.oems_GuestRoleFlag,
                    isOther = a.oems_OtherFlag,
                    colour = a.oems_Colour
                }
            ).ToList<AttendingAsOption>();

            AttendingAs attendingAs = new AttendingAs
            {
                attendingAsList =
                (
                    from a in eventAttendingAsList
                    join b in serviceContext.oems_eventregistration_attending_asSet
                        .Where(z => z.oems_eventregistrationid == registrationId) on a.id equals b.oems_attendingasoptionid into c
                    from d in c.DefaultIfEmpty()
                    select new AttendingAsOption
                    {
                        isSelected = (d != null ? true : false),
                        id = a.id,
                        selected = (d != null ? a.selected : null),
                        name = a.name,
                        isVotingRole = a.isVotingRole,
                        isGuestRole = a.isGuestRole,
                        isOther = a.isOther,
                        otherInput = string.Empty,//res.AttendingAsOther,
                        colour = a.colour
                    }
                ).ToList<AttendingAsOption>()
            };

            try
            {
                Entity aux = new Entity("oems_eventregistration");
                aux.Id = registrationId;
                //aux.Attributes["oems_attendingasjson"] = JsonConvert.SerializeObject(attendingAs);
                aux.Attributes["oems_updateattendingasdata"] = JsonConvert.SerializeObject(attendingAs);
                serviceContext.Update(aux);
            }
            catch (Exception e)
            {
                throw new InvalidPluginExecutionException(e.Message);
            }
        }

        private static string GetOptionSetValueLabel(OmniContext service, string logicalName, string attribute, OptionSetValue option)
        {
            string optionLabel = String.Empty;

            RetrieveAttributeRequest attributeRequest = new RetrieveAttributeRequest
            {
                EntityLogicalName = logicalName,
                LogicalName = attribute,
                RetrieveAsIfPublished = true
            };

            RetrieveAttributeResponse attributeResponse = (RetrieveAttributeResponse)service.Execute(attributeRequest);
            AttributeMetadata attrMetadata = (AttributeMetadata)attributeResponse.AttributeMetadata;
            EnumAttributeMetadata picklistMetadata = (EnumAttributeMetadata)attrMetadata;

            // For every status code value within all of our status codes values
            //  (all of the values in the drop down list)
            foreach (OptionMetadata optionMeta in
                picklistMetadata.OptionSet.Options)
            {
                // Check to see if our current value matches
                if (optionMeta.Value == option.Value)
                {
                    // If our numeric value matches, set the string to our status code
                    //  label
                    optionLabel = optionMeta.Label.UserLocalizedLabel.Label;
                }
            }

            return optionLabel;
        }

        protected void SetAccountPersonalAccommodation(LocalPluginContext localContext)
        {
            if (localContext == null)
                throw new ArgumentNullException("localContext");

            // stop recurrsive calls
            if (localContext.PluginExecutionContext.Depth != 1)
                return;

            oems_EventRegistration oemsEventRegistration = localContext.Entity.ToEntity<oems_EventRegistration>();

            //EntityReference account = oemsEventRegistration.GetAttributeValue<EntityReference>("oems_account");

            EntityReference contact = oemsEventRegistration.GetAttributeValue<EntityReference>("oems_contact");
            EntityReference oems_event = oemsEventRegistration.GetAttributeValue<EntityReference>("oems_event");

            List<oems_AccountPersonalAccommodation> accountPA = (from apa in localContext.OmniContext.oems_AccountPersonalAccommodationSet
                                                                 where apa.oems_Contact.Id == contact.Id
                                                                 select apa).ToList<oems_AccountPersonalAccommodation>();

            string[] columns = { "oems_optiontype", "oems_optionname" };
            DataCollection<Entity> eventAccommodations = GetRelatedEntitDataFromManyToManyRelation("oems_personalaccommodationoption", columns, "oems_event_personal_accommodation_config", oems_event, localContext.OmniContext);

            if (eventAccommodations == null) return;

            bool flag = false;
            Entity personalAccommodationRequest = new Entity("oems_personalaccommodationrequest");

            foreach (var apa in accountPA)
            {
                foreach (var eventAccommodation in eventAccommodations)
                {
                    if (apa.oems_PersonalAccommodationOption.Id == eventAccommodation.Id)
                    {
                        if (!flag)
                        {
                            personalAccommodationRequest["oems_eventregistration"] = oemsEventRegistration.ToEntityReference();
                            personalAccommodationRequest["oems_event"] = oems_event;
                            personalAccommodationRequest.Id = localContext.OmniContext.Create(personalAccommodationRequest);
                            flag = true;

                            Entity reg = new Entity(oemsEventRegistration.LogicalName);
                            reg.Id = oemsEventRegistration.Id;
                            reg["oems_personalaccommodationrequiredflag"] = true;
                            localContext.OmniContext.Update(reg);
                        }

                        Entity existingRequestOption = new Entity("oems_personalaccommodationrequestoption");
                        existingRequestOption["oems_personalaccommodationoption"] = (EntityReference)apa["oems_personalaccommodationoption"];
                        existingRequestOption["oems_eventregistration"] = oemsEventRegistration.ToEntityReference();
                        existingRequestOption["oems_personalaccommodationrequest"] = personalAccommodationRequest.ToEntityReference();

                        int? optionType = ((Xrm.oems_PersonalAccommodationOption)(eventAccommodation)).oems_OptionType;


                        if (apa.oems_EventFlag == true && (optionType == 2 || optionType == 3))
                        {
                            existingRequestOption["oems_eventflag"] = true;
                        }

                        if (apa.oems_HotelFlag == true && (optionType == 2 || optionType == 4))
                        {
                            existingRequestOption["oems_hotelflag"] = true;
                        }

                        if (apa.oems_UserSpecifiedDetail != null && apa.oems_UserSpecifiedDetail != "")
                        {
                            existingRequestOption["oems_userspecifieddetails"] = apa.oems_UserSpecifiedDetail;
                        }

                        existingRequestOption.Id = localContext.OmniContext.Create(existingRequestOption);

                    }
                }
            }

        }

        public static DataCollection<Entity> GetRelatedEntitDataFromManyToManyRelation(string entityToRetrieve, string[] columnsToRetieve, string relationName, EntityReference targetEntity, IOrganizationService service)
        {
            DataCollection<Entity> result = null;

            QueryExpression query = new QueryExpression();
            query.EntityName = entityToRetrieve;
            query.ColumnSet = new ColumnSet(columnsToRetieve);
            Relationship relationship = new Relationship();

            relationship.SchemaName = relationName;

            RelationshipQueryCollection relatedEntity = new RelationshipQueryCollection();
            relatedEntity.Add(relationship, query);
            RetrieveRequest request = new RetrieveRequest();
            request.RelatedEntitiesQuery = relatedEntity;
            //request.ColumnSet = new ColumnSet(targetEntity.LogicalName + "id");
            request.ColumnSet = new ColumnSet(true);
            request.Target = targetEntity;
            RetrieveResponse response = (RetrieveResponse)service.Execute(request);


            if (((DataCollection<Relationship, EntityCollection>)(((RelatedEntityCollection)(response.Entity.RelatedEntities)))).Contains(new Relationship(relationName)) && ((DataCollection<Relationship, EntityCollection>)(((RelatedEntityCollection)(response.Entity.RelatedEntities))))[new Relationship(relationName)].Entities.Count > 0)
            {
                result = ((DataCollection<Relationship, EntityCollection>)(((RelatedEntityCollection)(response.Entity.RelatedEntities))))[new Relationship(relationName)].Entities;
            }

            return result;
        }

        protected void CancelLinkedRequests(LocalPluginContext localContext, Guid eventRegistrationId, int eventRegistrationStatus)
        {

            SetStateRequest state = new SetStateRequest();

            var childCareRequests = from ccr in localContext.OmniContext.oems_ChildCareRequestSet
                                    where (int)ccr["statecode"] == 0 && ccr.oems_EventRegistration.Id == eventRegistrationId
                                    select ccr;
            foreach (var ccr in childCareRequests)
            {

                state.State = new OptionSetValue((int)ChildCareRequestStatus.ACTIVE);
                if (eventRegistrationStatus == EventRegistrationStatusReason.Withdrawn)
                {
                    state.Status = new OptionSetValue((int)ChildCareRequestStatusReason.WITHDRAWN);
                }
                else
                {
                    state.Status = new OptionSetValue((int)ChildCareRequestStatusReason.CANCELLED);
                }


                state.EntityMoniker = ccr.ToEntityReference();

                // Execute the Request
                SetStateResponse stateSet = (SetStateResponse)localContext.OrganizationService.Execute(state);

            }

            var accomodationRequests = from ar in localContext.OmniContext.oems_AccommodationRequestSet
                                       where (int)ar["statecode"] == 0 && ar.oems_EventRegistration.Id == eventRegistrationId
                                       select ar;
            foreach (var ar in accomodationRequests)
            {

                state.State = new OptionSetValue((int)AccomodationRequestStatus.ACTIVE);
                if (eventRegistrationStatus == EventRegistrationStatusReason.Withdrawn)
                {
                    // as per Brian's state diagram any withdrawl of reg form should go to cancelled status of release time request
                    state.Status = new OptionSetValue((int)ReleaseTimeRequestStatusReason.CANCELLED);
                    //state.Status = new OptionSetValue((int)ReleaseTimeRequestStatusReason.WITHDRAWN);
                }
                else
                {
                    state.Status = new OptionSetValue((int)AccomodationRequestStatusReason.CANCELLED);
                }

                // Point the Request to the case whose state is being changed
                state.EntityMoniker = ar.ToEntityReference();

                // Execute the Request
                SetStateResponse stateSet = (SetStateResponse)localContext.OrganizationService.Execute(state);

            }

            var releaseTimeRequests = from rtr in localContext.OmniContext.oems_ReleaseTimeRequestSet
                                      where (int)rtr["statecode"] == 0 && rtr.oems_EventRegistration.Id == eventRegistrationId
                                      select rtr;
            foreach (var rtr in releaseTimeRequests)
            {

                state.State = new OptionSetValue((int)ReleaseTimeRequestStatus.ACTIVE);
                if (eventRegistrationStatus == EventRegistrationStatusReason.Withdrawn)
                {
                    state.Status = new OptionSetValue((int)ReleaseTimeRequestStatusReason.WITHDRAWN);
                }
                else
                {
                    state.Status = new OptionSetValue((int)ReleaseTimeRequestStatusReason.CANCELLED);
                }

                // Point the Request to the case whose state is being changed
                state.EntityMoniker = rtr.ToEntityReference();

                // Execute the Request
                SetStateResponse stateSet = (SetStateResponse)localContext.OrganizationService.Execute(state);

            }

            var caucusRequests = from cr in localContext.OmniContext.oems_CaucusRequestSet
                                 where (int)cr["statecode"] == 0 && cr.oems_EventRegistration.Id == eventRegistrationId
                                 select cr;
            foreach (var cr in caucusRequests)
            {

                state.State = new OptionSetValue((int)CaucusRequestStatus.ACTIVE);
                if (eventRegistrationStatus == EventRegistrationStatusReason.Withdrawn)
                {
                    state.Status = new OptionSetValue((int)CaucusRequestStatusReason.WITHDRAWN);
                }
                else
                {
                    state.Status = new OptionSetValue((int)CaucusRequestStatusReason.CANCELLED);
                }

                // Point the Request to the case whose state is being changed
                state.EntityMoniker = cr.ToEntityReference();

                // Execute the Request
                SetStateResponse stateSet = (SetStateResponse)localContext.OrganizationService.Execute(state);

            }

            var eventAttendees = from ea in localContext.OmniContext.oems_EventAttendeeSet
                                 where (int)ea["statecode"] == 0 && ea.oems_EventRegistration.Id == eventRegistrationId
                                 select ea;
            foreach (var ea in eventAttendees)
            {

                state.State = new OptionSetValue((int)EventAttendeeStatus.ACTIVE);
                if (eventRegistrationStatus == EventRegistrationStatusReason.Withdrawn)
                {
                    state.Status = new OptionSetValue((int)EventAttendeeStatusReason.WITHDRAWN);
                }
                else
                {
                    state.Status = new OptionSetValue((int)EventAttendeeStatusReason.CANCELLED);
                }
                state.EntityMoniker = ea.ToEntityReference();
                SetStateResponse stateSet = (SetStateResponse)localContext.OrganizationService.Execute(state);
            }

            var singleRoomMedicalRequests = from srm in localContext.OmniContext.oems_SingleRoomMedicalAccommodationRequestSet
                                            where (int)srm["statecode"] == 0 && srm.oems_EventRegistration.Id == eventRegistrationId
                                            select srm;
            foreach (var srm in singleRoomMedicalRequests)
            {

                state.State = new OptionSetValue((int)SingleRoomMedicalAccomodationRequestStatus.ACTIVE);
                if (eventRegistrationStatus == EventRegistrationStatusReason.Withdrawn)
                {
                    state.Status = new OptionSetValue((int)SingleRoomMedicalAccomodationRequestStatusReason.WITHDRAWN);
                }
                else
                {
                    state.Status = new OptionSetValue((int)SingleRoomMedicalAccomodationRequestStatusReason.CANCELLED);
                }
                state.EntityMoniker = srm.ToEntityReference();
                SetStateResponse stateSet = (SetStateResponse)localContext.OrganizationService.Execute(state);
            }

            var personalAccomodationRequests = from par in localContext.OmniContext.oems_PersonalAccommodationRequestSet
                                               where (int)par["statecode"] == 0 && par.oems_EventRegistration.Id == eventRegistrationId
                                               select par;
            foreach (var par in personalAccomodationRequests)
            {
                var personalAccomodationOptions = from pao in localContext.OmniContext.oems_PersonalAccommodationRequestOptionSet
                                                  where (int)pao["statecode"] == 0 && pao.oems_PersonalAccommodationRequest.Id == par.Id
                                                  select pao;
                foreach (var pao in personalAccomodationOptions)
                {

                    state.State = new OptionSetValue((int)PersonalAccomodationRequestOptionStatus.ACTIVE);
                    if (eventRegistrationStatus == EventRegistrationStatusReason.Withdrawn)
                    {
                        state.Status = new OptionSetValue((int)PersonalAccomodationRequestOptionStatusReason.Withdrawn);
                    }
                    else
                    {
                        state.Status = new OptionSetValue((int)PersonalAccomodationRequestOptionStatusReason.Cancelled);
                    }
                    state.EntityMoniker = pao.ToEntityReference();
                    SetStateResponse stateSet1 = (SetStateResponse)localContext.OrganizationService.Execute(state);
                }

                state.State = new OptionSetValue((int)PersonalAccomodationRequestStatus.ACTIVE);
                if (eventRegistrationStatus == EventRegistrationStatusReason.Withdrawn)
                {
                    state.Status = new OptionSetValue((int)PersonalAccomodationRequestStatusReason.WITHDRAWN);
                }
                else
                {
                    state.Status = new OptionSetValue((int)PersonalAccomodationRequestStatusReason.CANCELLED);
                }
                state.EntityMoniker = par.ToEntityReference();
                SetStateResponse stateSet = (SetStateResponse)localContext.OrganizationService.Execute(state);
            }

            var personalCareExemptionRequests = from pce in localContext.OmniContext.oems_PersonalCareExemptionRequestSet
                                                where (int)pce["statecode"] == 0 && pce.oems_EventRegistration.Id == eventRegistrationId
                                                select pce;
            foreach (var pce in personalCareExemptionRequests)
            {

                state.State = new OptionSetValue((int)PersonalCareExcemptionRequestStatus.ACTIVE);
                if (eventRegistrationStatus == EventRegistrationStatusReason.Withdrawn)
                {
                    state.Status = new OptionSetValue((int)PersonalCareExcemptionRequestStatusReason.WITHDRAWN);
                }
                else
                {
                    state.Status = new OptionSetValue((int)PersonalCareExcemptionRequestStatusReason.CANCELLED);
                }
                state.EntityMoniker = pce.ToEntityReference();
                SetStateResponse stateSet = (SetStateResponse)localContext.OrganizationService.Execute(state);
            }

            var eventInvitedGuests = from eig in localContext.OmniContext.oems_EventInvitedGuestSet
                                     where (int)eig["statecode"] == 0 && eig.oems_EventRegistration.Id == eventRegistrationId
                                     select eig;
            foreach (var eig in eventInvitedGuests)
            {

                state.State = new OptionSetValue((int)EventInvitedGuestStatus.ACTIVE);
                if (eventRegistrationStatus == EventRegistrationStatusReason.Withdrawn)
                {
                    state.Status = new OptionSetValue((int)EventInvitedGuestStatusReason.WITHDRAWN);
                }
                else
                {
                    state.Status = new OptionSetValue((int)EventInvitedGuestStatusReason.CANCELLED);
                }
                state.EntityMoniker = eig.ToEntityReference();
                SetStateResponse stateSet = (SetStateResponse)localContext.OrganizationService.Execute(state);
            }
        }

        protected void SetStateCode(LocalPluginContext localContext)
        {
            if (localContext == null)
                throw new ArgumentNullException("localContext");

            // stop recurrsive calls
            if (localContext.PluginExecutionContext.Depth != 1)
                return;


            if (localContext.PluginExecutionContext.InputParameters.Contains("EntityMoniker") &&
                   localContext.PluginExecutionContext.InputParameters["EntityMoniker"] is EntityReference)
            {
                var entityRef = (EntityReference)localContext.PluginExecutionContext.InputParameters["EntityMoniker"];
                var state = (OptionSetValue)localContext.PluginExecutionContext.InputParameters["State"];
                var status = (OptionSetValue)localContext.PluginExecutionContext.InputParameters["Status"];

                if (status.Value == EventRegistrationStatusReason.Cancelled ||
                    status.Value == EventRegistrationStatusReason.Withdrawn)
                {
                    oems_Event oemsEvent =
                       (from a in localContext.OmniContext.oems_EventSet
                        join er in localContext.OmniContext.oems_EventRegistrationSet on a.oems_EventId equals er.oems_Event.Id
                        where er.oems_EventRegistrationId == entityRef.Id
                        select a).Single();
                    if (oemsEvent.statuscode != (int)EventStatusReason.CANCELLED)
                    {
                        CancelLinkedRequests(localContext, entityRef.Id, status.Value);
                    }
                }
            }
        }

        private AttendingAs RetrieveAttendingAs(LocalPluginContext localContext, oems_Event oemsEvent, oems_EventRegistration eventRegistration)
        {
            AttendingAs attendingAs = new AttendingAs
            {
                attendingAsList =
                    (from a in oemsEvent.oems_event_attending_as_config
                     join b in eventRegistration.oems_eventregistration_attending_as on a.oems_attendingasoptionId equals b.oems_attendingasoptionId into c
                     from d in c.DefaultIfEmpty()
                     select new AttendingAsOption
                     {
                         isSelected = (d != null ? true : false),
                         id = (Guid)a.oems_attendingasoptionId,
                         selected = (d != null ? a.ToEntityReference() : null),
                         name = a.oems_OptionName,
                         isVotingRole = a.oems_VotingRoleFlag,
                         isGuestRole = a.oems_GuestRoleFlag,
                         isOther = a.oems_OtherFlag,
                         otherInput = eventRegistration.oems_AttendingAsOther, // need to fix this
                         colour = a.oems_Colour,
                         displaySequence = a.oems_DisplaySequence
                     }
                    ).OrderBy(x => x.displaySequence).ToList<AttendingAsOption>()
            };

            return attendingAs;
        }

        protected Dictionary<string, string> LoadRolesInformation(string configValue)
        {
            string[] roles = configValue.Split(',');

            Dictionary<string, string> rolList = null;

            if (roles.Length > 0)
            {
                rolList = new Dictionary<string, string>();


                for (int i = 0; i < roles.Length; i++)
                {
                    string[] roleElements = roles[i].Split('|');



                    rolList.Add(roleElements[0], roleElements[1]);
                }
            }

            return rolList;
        }

        private string LoadApproveProperty(string configValue)
        {
            string[] properties = configValue.Split(',');
            string result = null;

            if (properties.Length > 0)
            {
                for (int i = 0; i < properties.Length; i++)
                {
                    if (properties[i].ToLower().StartsWith("approve"))
                    {
                        string[] values = properties[i].Split('=');
                        result = values[1];
                    }
                }
            }

            return result;
        }

        private string GetAttributeDisplayName(OmniContext service, string entitySchemaName, string attributeSchemaName)
        {
            RetrieveAttributeRequest retrieveAttributeRequest = new RetrieveAttributeRequest
            {
                EntityLogicalName = entitySchemaName,
                LogicalName = attributeSchemaName,
                RetrieveAsIfPublished = true
            };
            RetrieveAttributeResponse retrieveAttributeResponse = (RetrieveAttributeResponse)service.Execute(retrieveAttributeRequest);
            AttributeMetadata retrievedAttributeMetadata = (AttributeMetadata)retrieveAttributeResponse.AttributeMetadata;
            return retrievedAttributeMetadata.DisplayName.UserLocalizedLabel.Label;
        }

        private string RetrieveAttributeMetadataPicklistValue(IOrganizationService crmService, int addTypeValue, string entityLogicalName, string attributeLogicalName)
        {
            RetrieveAttributeRequest objRetrieveAttributeRequest;

            objRetrieveAttributeRequest = new RetrieveAttributeRequest();

            objRetrieveAttributeRequest.EntityLogicalName = entityLogicalName;

            objRetrieveAttributeRequest.LogicalName = attributeLogicalName;

            int indexAddtypeCode = addTypeValue - 1;


            // Execute the request
            RetrieveAttributeResponse attributeResponse = (RetrieveAttributeResponse)crmService.Execute(objRetrieveAttributeRequest);

            PicklistAttributeMetadata objPckLstAttMetadata = new PicklistAttributeMetadata();

            ICollection<object> objCollection = attributeResponse.Results.Values;

            objPckLstAttMetadata.OptionSet = ((EnumAttributeMetadata)(objCollection.ElementAt(0))).OptionSet;

            Microsoft.Xrm.Sdk.Label objLabel = objPckLstAttMetadata.OptionSet.Options[indexAddtypeCode].Label;

            string lblAddTypeLabel = objLabel.LocalizedLabels.ElementAt(0).Label;

            return lblAddTypeLabel;

        }

    }
}
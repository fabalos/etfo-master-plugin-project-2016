﻿using System;
using System.IO;
using System.Collections;
using System.Xml.Serialization;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Xrm;
using Microsoft.ApplicationBlocks.Data;
using System.Web.Script.Serialization;
using Microsoft.Xrm.Client.Collections;
using Microsoft.Crm.Sdk.Messages;

namespace Omniware.ETFO.Plugin
{
    // THIS CODE IS NOT CURRENTLY USED
    public class PortalFormElement : Plugin
    {

        public PortalFormElement()
            : base(typeof(EventRegistration))
        {
// this is not used 
//            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "Create", "oems_portalformelement", new Action<LocalPluginContext>(GenerateFormData)));
//            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "Update", "oems_portalformelement", new Action<LocalPluginContext>(GenerateFormData)));

        }


        protected void RegenerateForm(LocalPluginContext localContext, Entity oems_FormElement)
        {
            if (oems_FormElement.Contains("oems_formtemplate"))
            {

                Entity oems_portalFormTemplate = new Entity("oems_portalformtemplate");
                oems_portalFormTemplate.Id = ((EntityReference)oems_FormElement["oems_formtemplate"]).Id;
                oems_portalFormTemplate["oems_regenerate"] = true;
                localContext.OrganizationService.Update(oems_portalFormTemplate);
            }
            else if (oems_FormElement.Contains("oems_parentelement"))
            {
                oems_FormElement = localContext.OmniContext.Retrieve(((EntityReference)oems_FormElement["oems_parentformelement"]).LogicalName, 
                                    ((EntityReference)oems_FormElement["oems_parentformelement"]).Id,
                                    new ColumnSet(true));
                RegenerateForm(localContext, oems_FormElement);
                
            }
        }

        protected void GenerateFormData(LocalPluginContext localContext)
        {
            if (localContext.PluginExecutionContext.Depth != 1) return;
            
            Entity oems_FormElement = localContext.PluginExecutionContext.PostEntityImages["PostImage"];
            RegenerateForm(localContext, oems_FormElement);

        }
    }
}

﻿using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xrm;

namespace EventTeamPlugin
{
    public class EventHelper
    {
        public static Guid[] GetUsersFromRole(IOrganizationService service, Guid roleId, Guid businessUnitId)
        {
            //var usersTotal = from users in context.CreateQuery("SystemUserRoles")
            //                  where (Guid)users["RoleId"] == roleId
            //                  select users;
            Guid[] userIds = null;
            OmniContext context = new OmniContext(service);
            //IQueryable<SystemUserRoles> roles = context.SystemUserRolesSet.Where(x => x.RoleId == roleId);
            //IQueryable<Entity> users = context.CreateQuery("SystemUser").Where(x => (bool)x.Attributes["isdisabled"] == false);

            QueryExpression query = new QueryExpression("systemuser");
            query.ColumnSet = new ColumnSet(new string[] { "systemuserid" });
            query.Distinct = true;
            query.Criteria = new FilterExpression();
            query.Criteria.AddCondition("businessunitid", ConditionOperator.Equal, businessUnitId);
            query.Criteria.AddCondition("isdisabled", ConditionOperator.Equal,false);
            query.AddLink("systemuserroles", "systemuserid", "systemuserid").
                LinkCriteria.AddCondition("roleid", ConditionOperator.Equal, roleId);

            var users = context.RetrieveMultiple(query);

            ArrayList arrayRoles = new ArrayList();
            foreach (var sur in users.Entities)
            {
                arrayRoles.Add(sur.Id);
            }

            userIds = (Guid[])arrayRoles.ToArray(typeof(Guid));
            return userIds;
        }
        void AssignUserToTeam(Guid _teamId, Guid _userId, IOrganizationService service)
        {
            AssignRequest assignRequest = new AssignRequest()
            {
                Assignee = new EntityReference
                {
                    LogicalName = "team",
                    Id = _teamId
                },
  
                Target = new EntityReference("systemuser", _userId)
            };

            service.Execute(assignRequest);
        }

        public static EntityReference GetUserAdmin(IOrganizationService service)
        {
            QueryExpression userAdminQuery = new QueryExpression
            {
                EntityName = "oems_configuration",
                ColumnSet = new ColumnSet("oems_systemadminuserid")
            };

            Entity userAdmin = service.RetrieveMultiple(userAdminQuery).Entities[0];

            return userAdmin.GetAttributeValue<EntityReference>("oems_systemadminuserid");
        }

        public static void AssignEventToTeam(Guid _teamId, Guid _eventId, IOrganizationService service)
        {
            AssignRequest assignRequest = new AssignRequest()
            {
                Assignee = new EntityReference
                {
                    //LogicalName = Team.EntityLogicalName,
                    LogicalName = "team",
                    Id = _teamId
                },

                //Target = new EntityReference(Account.EntityLogicalName, _userId)
                //systemuser
                Target = new EntityReference("oems_event", _eventId)
            };

            try
            {
                service.Execute(assignRequest);
            }
            catch (Exception e)
            {
                try
                {
                     service.Execute(assignRequest);
                }
                catch (Exception x)
                {
                    try
                    {
                        service.Execute(assignRequest);
                    }
                    catch (Exception b)
                    {
                        
                        throw b;
                    }

                }
            }

            
        }

        public static Entity GetDefaultBusinessUnit(IOrganizationService service)
        {
            QueryExpression defaultbusinessUnitQuery = new QueryExpression
            {
                EntityName = "businessunit",
                ColumnSet = new ColumnSet(true),
                Criteria =
                {
                    Conditions =
                    {
                        new ConditionExpression("parentbusinessunitid", 
                            ConditionOperator.Null)
                    }
                }
            };

            Entity defaultBusinessUnit = service.RetrieveMultiple(defaultbusinessUnitQuery).Entities[0];

            return defaultBusinessUnit;
        }

        public static Entity GetDefaultTeam(IOrganizationService service, string teamName) 
        {
            QueryExpression defaultbusinessUnitQuery = new QueryExpression
            {
                EntityName = "team",
                ColumnSet = new ColumnSet(true),
                Criteria =
                {
                    Conditions =
                    {
                        new ConditionExpression("name", 
                            ConditionOperator.Equal,teamName)
                    }
                }
            };

            Entity defaultTeam = service.RetrieveMultiple(defaultbusinessUnitQuery).Entities[0];

            return defaultTeam;
        }

       

        public static Entity GetTeamByName(string teamName,IOrganizationService service) 
        {
            QueryExpression query = new QueryExpression
            {
                EntityName = "team",
                ColumnSet = new ColumnSet("teamid"),
                Criteria = new FilterExpression
                {
                    Conditions =
                    {
                        // You would replace the condition below with an actual role
                        // name, or skip this query if you had a role id.
                        new ConditionExpression
                        {
                            AttributeName = "name",
                            Operator = ConditionOperator.Equal,
                            Values = {teamName}
                        }
                    }
                }
            };

            Entity team = null;
            DataCollection<Entity> result = null;
            result = service.RetrieveMultiple(query).Entities;

            team = result[0];
            

            return team;
        }

        public static void AssignEntityToTeam(Guid _teamId, Guid _entityId, string logicalName, IOrganizationService service)
        {
            AssignRequest assignRequest = new AssignRequest()
            {
                Assignee = new EntityReference
                {
                    //LogicalName = Team.EntityLogicalName,
                    LogicalName = "team",
                    Id = _teamId
                },

                //Target = new EntityReference(Account.EntityLogicalName, _userId)
                //systemuser
                Target = new EntityReference(logicalName, _entityId)
            };

            service.Execute(assignRequest);
        }

        public static void AddRoleToTeam(string roleName, IOrganizationService service, Guid _teamId)
        {
            QueryExpression query = new QueryExpression
            {
                EntityName = "role",
                ColumnSet = new ColumnSet("roleid"),
                Criteria = new FilterExpression
                {
                    Conditions =
                    {
                        // You would replace the condition below with an actual role
                        // name, or skip this query if you had a role id.
                        new ConditionExpression
                        {
                            AttributeName = "name",
                            Operator = ConditionOperator.Equal,
                            Values = {roleName}
                        }
                    }
                }
            };

            Entity role = service.RetrieveMultiple(query).Entities[0];


            // Add the role to the team.
            service.Associate(
                   "team",
                   _teamId,
                   new Relationship("teamroles_association"),
                   new EntityReferenceCollection() { new EntityReference("role", role.Id) });

        }

        public static void AssignUsersToTeam(Guid teamId, IOrganizationService service, Guid[] membersId)
        {
            if (membersId.Length > 0)
            {
                AddMembersTeamRequest addRequest = new AddMembersTeamRequest();

                addRequest.TeamId = teamId;
                addRequest.MemberIds = membersId;

                service.Execute(addRequest);
            }
        }

        public static void RemoveUsersToTeam(Guid teamId, IOrganizationService service, Guid[] membersId)
        {
            RemoveMembersTeamRequest addRequest = new RemoveMembersTeamRequest();

            addRequest.TeamId = teamId;
            addRequest.MemberIds = membersId;
            service.Execute(addRequest);
        }


        public static EntityReference GetRelatedTeam(IOrganizationService service, Guid eventId)
        {
            //string relationName = "team_oems_event";
            //DataCollection<Entity> result = null;
            //Entity resultEntity = null;

            //QueryExpression query = new QueryExpression();
            //query.EntityName = "team";
            //query.ColumnSet = new ColumnSet(true);
            //Relationship relationship = new Relationship();

            //relationship.SchemaName = "team_oems_event";

            //RelationshipQueryCollection relatedEntity = new RelationshipQueryCollection();
            //relatedEntity.Add(relationship, query);
            //RetrieveRequest request = new RetrieveRequest();
            //request.RelatedEntitiesQuery = relatedEntity;
            //request.ColumnSet = new ColumnSet("oems_event" + "id");
            //request.Target = new EntityReference("oems_event", eventId);
            //RetrieveResponse response = (RetrieveResponse)service.Execute(request);


            //if (((DataCollection<Relationship, EntityCollection>)(((RelatedEntityCollection)(response.Entity.RelatedEntities)))).Contains(new Relationship(relationName)) && ((DataCollection<Relationship, EntityCollection>)(((RelatedEntityCollection)(response.Entity.RelatedEntities))))[new Relationship(relationName)].Entities.Count > 0)
            //{
            //    result = ((DataCollection<Relationship, EntityCollection>)(((RelatedEntityCollection)(response.Entity.RelatedEntities))))[new Relationship(relationName)].Entities;
            //    resultEntity = result[0];
            //}

            Entity team = service.Retrieve("oems_event", eventId, new ColumnSet("owningteam"));
            if (team != null)
            {
                return team.GetAttributeValue<EntityReference>("owningteam");
            }

            return null;
        }

        public static bool IsTeamMember(Guid TeamID, Guid UserID, IOrganizationService service)
        {
            QueryExpression query = new QueryExpression("team");
            query.ColumnSet = new ColumnSet(true);
            query.Criteria.AddCondition(new ConditionExpression("teamid", ConditionOperator.Equal, TeamID));
            LinkEntity link = query.AddLink("teammembership", "teamid", "teamid");
            link.LinkCriteria.AddCondition(new ConditionExpression("systemuserid", ConditionOperator.Equal, UserID));

            try
            {
                var results = service.RetrieveMultiple(query);
                if (results.Entities.Count > 0) { return true; } else { return false; }
            }
            catch (Exception ex)
            {
                // Do your Error Handling here
                throw ex;
            }

        }

        public static DataCollection<Entity> GetRelatedEntitDataFromManyToManyRelation(string entityToRetrieve, string[] columnsToRetieve, string relationName, EntityReference targetEntity, IOrganizationService service)
        {
            DataCollection<Entity> result = null;

            QueryExpression query = new QueryExpression();
            query.EntityName = entityToRetrieve;
            query.ColumnSet = new ColumnSet(columnsToRetieve);
            Relationship relationship = new Relationship();

            relationship.SchemaName = relationName;

            RelationshipQueryCollection relatedEntity = new RelationshipQueryCollection();
            relatedEntity.Add(relationship, query);
            RetrieveRequest request = new RetrieveRequest();
            request.RelatedEntitiesQuery = relatedEntity;
            //request.ColumnSet = new ColumnSet(targetEntity.LogicalName + "id");
            request.ColumnSet = new ColumnSet(true);
            request.Target = targetEntity;
            RetrieveResponse response = (RetrieveResponse)service.Execute(request);


            if (((DataCollection<Relationship, EntityCollection>)(((RelatedEntityCollection)(response.Entity.RelatedEntities)))).Contains(new Relationship(relationName)) && ((DataCollection<Relationship, EntityCollection>)(((RelatedEntityCollection)(response.Entity.RelatedEntities))))[new Relationship(relationName)].Entities.Count > 0)
            {
                result = ((DataCollection<Relationship, EntityCollection>)(((RelatedEntityCollection)(response.Entity.RelatedEntities))))[new Relationship(relationName)].Entities;
            }

            return result;
        }

    }
}

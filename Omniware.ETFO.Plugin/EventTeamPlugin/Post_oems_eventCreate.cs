﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Client;
using Microsoft.Crm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Crm.Sdk.Messages;
using System.Collections;

namespace EventTeamPlugin
{
    public class Post_oems_eventCreate : IPlugin
    {
       

        public void Execute(IServiceProvider serviceProvider)
        {
             ITracingService tracingService =
                (ITracingService)serviceProvider.GetService(typeof(ITracingService));

            // Obtain the execution context from the service provider.
            IPluginExecutionContext context = (IPluginExecutionContext)
                serviceProvider.GetService(typeof(IPluginExecutionContext));

            // The InputParameters collection contains all the data passed in the message request.
            if(context.InputParameters.Contains("Target") &&
                context.InputParameters["Target"] is Entity)
            {
                // Obtain the target entity from the input parameters.
                Entity entity = (Entity)context.InputParameters["Target"];

                // Verify that the target entity represents an account.
                // If not, this plug-in was not registered correctly.
                if (entity.LogicalName != "oems_event")
                    return;

                try
                {
                    //Create a team
                    IOrganizationServiceFactory serviceFactory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
                    IOrganizationService service = serviceFactory.CreateOrganizationService(context.UserId);

                    QueryExpression defaultbusinessUnitQuery = new QueryExpression
                    {
                        EntityName = "businessunit",
                        ColumnSet = new ColumnSet("businessunitid"),
                        Criteria =
                        {
                            Conditions =
                    {
                        new ConditionExpression("parentbusinessunitid", 
                            ConditionOperator.Null)
                    }
                        }
                    };

                    Entity defaultBusinessUnit = service.RetrieveMultiple(defaultbusinessUnitQuery).Entities[0];
                    Entity fullEvent = service.Retrieve(entity.LogicalName,entity.Id,new ColumnSet(true));

                    Entity team = new Entity("team");

                    team.Attributes.Add("name", string.Concat("Event - ", fullEvent["oems_event_id"], " Team"));

                    team.Attributes.Add("businessunitid", new EntityReference("businessunit", defaultBusinessUnit.Id));

                    try
                    {
                        team.Id = service.Create(team);
                    }
                    catch (Exception ex)
                    {
                        throw new InvalidPluginExecutionException("Create Team: " + ex.Message);
                    }


                    Guid roleGuid = Guid.Empty;
                    string role = GetRoleName(service, out roleGuid);

                    ArrayList membersUsers = new ArrayList();

                    //Guid[] moreUsers = EventHelper.GetUsersFromRole(service, roleGuid, defaultBusinessUnit.Id);
                    //membersUsers.AddRange(moreUsers);

                    EventHelper.AddRoleToTeam(role, service, team.Id);

                    //EventHelper.AssignUsersToTeam(team.Id, service, (Guid[])membersUsers.ToArray(typeof(Guid)));

                    try
                    {
                        EventHelper.AssignEventToTeam(team.Id, entity.Id, service);
                    }
                    catch (Exception ex)
                    {
                        throw new InvalidPluginExecutionException("Assign team to event process failed :" + ex.Message);
                    }

                }
                catch (Exception e)
                {
                    if (e.Message == "An object with the specified name already exists.")
                    {
                        throw new InvalidPluginExecutionException("An event with this name already exists. Please choose a different name");
                    }
                }
            }
        }


        

        void getRelatedUsers(Guid teamId,IOrganizationService service) 
        {
            //Users by team
            QueryExpression query = new QueryExpression();
            query.EntityName = "systemuser";
            query.ColumnSet = new ColumnSet("systemuserid");
            Relationship relationship = new Relationship();

            query.Criteria = new FilterExpression();
            query.Criteria.AddCondition(new ConditionExpression("isdisabled", ConditionOperator.Equal, false));
            // name of relationship between team & systemuser
            relationship.SchemaName = "teammembership_association";
            RelationshipQueryCollection relatedEntity = new RelationshipQueryCollection();
            relatedEntity.Add(relationship, query);
            RetrieveRequest request = new RetrieveRequest();
            request.RelatedEntitiesQuery = relatedEntity;
            request.ColumnSet = new ColumnSet("teamid");
            request.Target = new EntityReference
            {
                Id = teamId,
                LogicalName = "team"

            };
            RetrieveResponse response = (RetrieveResponse)service.Execute(request); 
        }

        string GetRoleName(IOrganizationService service,out Guid roleId)
        {
            string roleName = null;
            roleId = Guid.Empty;
            ConditionExpression condition1 = new ConditionExpression();
            condition1.AttributeName = "statecode";
            condition1.Operator = ConditionOperator.Equal;
            condition1.Values.Add("Active");

            FilterExpression filter1 = new FilterExpression();
            filter1.Conditions.Add(condition1);

            QueryExpression query = new QueryExpression("oems_configuration");
            query.ColumnSet = new ColumnSet(true);

            query.Criteria.AddFilter(filter1);
            EntityCollection result = service.RetrieveMultiple(query);

            if (result.Entities.Count > 0)
            {
                EntityReference erefRole = (EntityReference)result.Entities[0].Attributes["oems_teamsecurityrole"];

                Entity secRole = service.Retrieve(erefRole.LogicalName, erefRole.Id, new ColumnSet("name"));

                roleId = secRole.Id;
                roleName = secRole.Attributes["name"].ToString();

            }
            else 
            {
                throw new InvalidPluginExecutionException("Configuration is missing");
            }

            return roleName;
        }

        Guid GetRoleId(IOrganizationService service)
        {
            Guid roleId = Guid.Empty;
            ConditionExpression condition1 = new ConditionExpression();
            condition1.AttributeName = "statecode";
            condition1.Operator = ConditionOperator.Equal;
            condition1.Values.Add("Active");

            FilterExpression filter1 = new FilterExpression();
            filter1.Conditions.Add(condition1);

            QueryExpression query = new QueryExpression("oems_configuration");
            query.ColumnSet = new ColumnSet(true);

            query.Criteria.AddFilter(filter1);
            EntityCollection result = service.RetrieveMultiple(query);

            if (result.Entities.Count > 0)
            {
                EntityReference erefRole = (EntityReference)result.Entities[0].Attributes["oems_teamsecurityrole"];

                Entity secRole = service.Retrieve(erefRole.LogicalName, erefRole.Id, new ColumnSet("name"));

                roleId = secRole.Id;

            }
            else
            {
                throw new InvalidPluginExecutionException("Configuration is missing");
            }

            return roleId;
        }
    }
}

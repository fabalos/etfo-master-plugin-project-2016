﻿using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xrm;

namespace EventTeamPlugin
{
    public class Post_oems_eventDelete : IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            ITracingService tracingService =
                (ITracingService)serviceProvider.GetService(typeof(ITracingService));

            // Obtain the execution context from the service provider.
            IPluginExecutionContext context = (IPluginExecutionContext)
                serviceProvider.GetService(typeof(IPluginExecutionContext));

            // The InputParameters collection contains all the data passed in the message request.
            if (context.InputParameters.Contains("Target") &&
                context.InputParameters["Target"] is EntityReference)
            {
                // Obtain the target entity from the input parameters.
                EntityReference entity = (EntityReference)context.InputParameters["Target"];

                // Verify that the target entity represents an account.
                // If not, this plug-in was not registered correctly.
                if (entity.LogicalName != "oems_event")
                    return;

                try
                {
                    IOrganizationServiceFactory serviceFactory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
                    IOrganizationService service = serviceFactory.CreateOrganizationService(context.UserId);

                    if (context.PreEntityImages["PreImage"].Contains("owningteam"))
                    {
                        EntityReference team = (EntityReference)context.PreEntityImages["PreImage"].Attributes["owningteam"];

                        EntityReference userAdmin = EventHelper.GetUserAdmin(service);

                        if (userAdmin == null)
                        {
                            throw new InvalidPluginExecutionException("You have attempted to delete an event without a valid System Admin Userid configured in the Configuration entity. " +
                            "Enter a valid userid and try again.");
                        }

                        //Should be reasigned to the admin user, and the admin user shoudl be found by it's domain name
                        ReassignObjectsOwnerRequest reassignRequest = new ReassignObjectsOwnerRequest()
                        {
                            FromPrincipal = team,
                            ToPrincipal = userAdmin
                        };

                        service.Execute(reassignRequest);

                        service.Delete(team.LogicalName, team.Id);
                    }

                    if (context.PreEntityImages["PreImage"].Contains("oems_webpage"))
                    {
                        EntityReference webPage = (EntityReference)context.PreEntityImages["PreImage"].Attributes["oems_webpage"];
                        service.Delete(webPage.LogicalName,webPage.Id);
                    }

                    OmniContext omniContext = new OmniContext(service);


                    //Delete Course / CourseTerm components
                    List<Entity> deleteList = new List<Entity>();

                    var targetGrades = (from c in omniContext.CreateQuery("oems_targetgrade")
                                        where c["oems_courseterm"] == entity
                                        select c).ToList<Entity>();

                    deleteList.AddRange(targetGrades);

                    var topics = (from c in omniContext.CreateQuery("oems_coursetopic")
                                  where c["oems_courseterm"] == entity
                                  select c).ToList<Entity>();

                    deleteList.AddRange(topics);

                    var localHosts = (from c in omniContext.CreateQuery("oems_courselocalhost")
                                      where c["oems_courseterm"] == entity
                                      select c).ToList<Entity>();

                    deleteList.AddRange(localHosts);

                    var locations = (from c in omniContext.CreateQuery("oems_courselocation")
                                     where c["oems_courseterm"] == entity
                                     select c).ToList<Entity>();

                    deleteList.AddRange(locations);

                    var timeSlots = (from c in omniContext.CreateQuery("oems_coursetimeslot")
                                     where c["oems_courseterm"] == entity
                                     select c).ToList<Entity>();

                    deleteList.AddRange(timeSlots);


                    foreach (var item in deleteList)
                    {
                        service.Delete(item.LogicalName, item.Id);
                    }
                }
                catch (Exception e)
                {
                    throw (e);
                }
            }
        
        }
    }
}

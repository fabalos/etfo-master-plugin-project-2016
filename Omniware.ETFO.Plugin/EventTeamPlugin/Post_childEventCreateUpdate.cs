﻿using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EventTeamPlugin
{
    public class Post_childEventCreateUpdate:IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            ITracingService _tracingService = (ITracingService)serviceProvider.GetService(typeof(ITracingService));

            // First, get the context from execution event
            IPluginExecutionContext context = (IPluginExecutionContext)
            serviceProvider.GetService(typeof(IPluginExecutionContext));

            // Obtain the organization service reference.
            IOrganizationServiceFactory serviceFactory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            IOrganizationService _orgService = serviceFactory.CreateOrganizationService(context.UserId);

            string relationship = string.Empty;

            //Make sure were not in an infinite loop
            if (context.Depth != 1 && context.Depth !=3)
            {
                return;
            }

            // First we need to test to see if this is in fact an Associate event
            if ((context.MessageName == "Create") || (context.MessageName == "Update"))
            {
                // Now get the "Relationship" from context. The term "Relationship here is literal not
                // Not to be confused with the name of your entities relationship name.    
                if (context.InputParameters.Contains("Target") && context.InputParameters["Target"] is Entity)
                {
                    // Obtain the target entity from the input parameters.
                    Entity currentEntity = (Entity)context.InputParameters["Target"];
                    //Reload Current entity to retrieve all parameters
                    currentEntity = _orgService.Retrieve(currentEntity.LogicalName, currentEntity.Id, new ColumnSet(true));

                    RetrieveEntityRequest reqMedatadaCurEntity = new RetrieveEntityRequest() { LogicalName = currentEntity.LogicalName, EntityFilters = Microsoft.Xrm.Sdk.Metadata.EntityFilters.All };

                    RetrieveEntityResponse resMetadataCurEntity = (RetrieveEntityResponse)_orgService.Execute(reqMedatadaCurEntity);

                    if (CanBeAssigned(resMetadataCurEntity))
                    {


                        RetrieveEntityRequest reqRelMetadata = new RetrieveEntityRequest() { LogicalName = currentEntity.LogicalName, EntityFilters = EntityFilters.Relationships };

                        RetrieveEntityRequest reqRelMetadataOems_Event = new RetrieveEntityRequest() { LogicalName = "oems_event", EntityFilters = EntityFilters.Relationships };

                        RetrieveEntityResponse resRelMetadata = (RetrieveEntityResponse)_orgService.Execute(reqRelMetadata);
                        RetrieveEntityResponse resRelMetadataOems_Event = (RetrieveEntityResponse)_orgService.Execute(reqRelMetadataOems_Event);

                        //resRelMetadata.EntityMetadata.OneToManyRelationships[1].ReferencedAttribute
                        CheckRelationship(resRelMetadataOems_Event.EntityMetadata.OneToManyRelationships,
                            resRelMetadata.EntityMetadata.ManyToOneRelationships,
                            currentEntity, _orgService);
                    }
                }
            }

            
        }

        void CheckRelationship(OneToManyRelationshipMetadata[] parentRelations, OneToManyRelationshipMetadata[] childRelations, Entity currentEntity,IOrganizationService service) 
        {
            for (int i = 0; i < parentRelations.Length; i++) 
            {
                for (int j = 0; j < childRelations.Length; j++) 
                {
                    if (parentRelations[i].SchemaName == childRelations[j].SchemaName) 
                    {
                        CheckTeam(currentEntity,childRelations[j],service);
                    }
                }
            }

           
        }

        bool CanBeAssigned(RetrieveEntityResponse response) 
        {
            
            foreach (SecurityPrivilegeMetadata secu in response.EntityMetadata.Privileges) 
            {
                if (secu.Name.StartsWith("prvAssign")) 
                {
                    return true;
                }
            }

            return false;
        }

        private void CheckTeam(Entity currentEntity,OneToManyRelationshipMetadata relation,IOrganizationService service)
        {
            if (currentEntity.Attributes.ContainsKey(relation.ReferencingAttribute))
            {
                if (currentEntity.Attributes.ContainsKey("owningteam"))
                {
                    EntityReference TeamRelated = (EntityReference)currentEntity.Attributes["owningteam"];
                    EntityReference relatedEvent = (EntityReference)currentEntity.Attributes[relation.ReferencingAttribute];

                    EntityReference team = EventHelper.GetRelatedTeam(service, relatedEvent.Id);

                    if (TeamRelated.Id != team.Id)
                    {
                        EventHelper.AssignEntityToTeam(team.Id, currentEntity.Id,currentEntity.LogicalName, service);
                    }
                }
                else
                {
                    EntityReference relatedEvent = (EntityReference)currentEntity.Attributes[relation.ReferencingAttribute];
                    EntityReference team = EventHelper.GetRelatedTeam(service, relatedEvent.Id);

                    if (team != null)
                    {
                        EventHelper.AssignEntityToTeam(team.Id, currentEntity.Id, currentEntity.LogicalName, service);
                    }
                }

            }
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Client;
using Microsoft.Crm.Sdk;
using Microsoft.Crm.Sdk.Messages;

namespace EventTeamPlugin
{
    public class Post_oems_eventDisassociate : IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            ITracingService _tracingService = (ITracingService)serviceProvider.GetService(typeof(ITracingService));

            // First, get the context from execution event
            IPluginExecutionContext context = (IPluginExecutionContext)
            serviceProvider.GetService(typeof(IPluginExecutionContext));

            // Obtain the organization service reference.
            IOrganizationServiceFactory serviceFactory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            IOrganizationService _orgService = serviceFactory.CreateOrganizationService(context.UserId);

            EntityReference parentEntity = null;

            EntityReferenceCollection relatedEntities = null;

            EntityReference childtEntity = null;

            string relationship = string.Empty;

            //Make sure were not in an infinite loop
            if (context.Depth > 1)
            {
                return;
            }

            // First we need to test to see if this is in fact an Associate event
            if (context.MessageName == "Disassociate")
            {
                // Now get the "Relationship" from context. The term "Relationship here is literal not
                // Not to be confused with the name of your entities relationship name.    
                if (context.InputParameters.Contains("Relationship"))
                {
                    relationship = context.InputParameters["Relationship"].ToString();
                    relationship = relationship.Trim('.');

                    if ((relationship == "oems_event_executive_staff_owners") || relationship == "oems_event_support_staff_owners")
                    {
                        Entity completeParentEntity;
                        // Get the parent entity reference from the Target context
                        if (context.InputParameters.Contains("Target") && context.InputParameters["Target"] is EntityReference)
                        {
                            parentEntity = (EntityReference)context.InputParameters["Target"];
                            completeParentEntity = _orgService.Retrieve(parentEntity.LogicalName,parentEntity.Id,new ColumnSet(true));
                        }
                        // Get the related child entity reference from the "RelatedEntities" context.InputParameters

                        if (context.InputParameters.Contains("RelatedEntities") && context.InputParameters["RelatedEntities"] is EntityReferenceCollection)
                        {
                            relatedEntities = context.InputParameters["RelatedEntities"] as EntityReferenceCollection;
                            childtEntity = relatedEntities[0];

                            Entity completeChildEntity = _orgService.Retrieve(childtEntity.LogicalName, childtEntity.Id, new ColumnSet(true));

                            if (relationship == "oems_event_executive_staff_owners") 
                            {
                                //Check if the other relationship exists if not remove
                                //Relationship otherRealtionship = new Relationship("oems_event_support_staff_owners");
                                //IEnumerable<Entity> other = completeParentEntity.GetRelatedEntities(context, otherRealtionship);
                                if (CheckIfExistInOtherRealationship(_orgService, "oems_event_support_staff_owners", "systemuser", parentEntity)) 
                                {
                                    //Remove members
                                    RemoveRelation(_orgService, childtEntity.Id, parentEntity.Id);
                                }
                            }

                            if (relationship == "oems_event_support_staff_owners")
                            {
                                //Check if the other relationship exists if not remove
                                if (CheckIfExistInOtherRealationship(_orgService, "oems_event_executive_staff_owners", "systemuser", parentEntity))
                                {
                                    //Remove members
                                    RemoveRelation(_orgService, childtEntity.Id, parentEntity.Id);
                                }
                            }
                            //Now that we have both the parentEntity and the childtEntity we can take some action like sending an email or creating a Activity record
                        }


                    }
                }
            }
            else
            {
                if (context.MessageName == "Associate")
                {
                    if (context.InputParameters.Contains("Relationship"))
                    {
                        relationship = context.InputParameters["Relationship"].ToString();
                        relationship = relationship.Trim('.');

                        if ((relationship == "oems_event_executive_staff_owners") || relationship == "oems_event_support_staff_owners")
                        {
                            Entity completeParentEntity;
                            // Get the parent entity reference from the Target context
                            if (context.InputParameters.Contains("Target") && context.InputParameters["Target"] is EntityReference)
                            {
                                parentEntity = (EntityReference)context.InputParameters["Target"];
                                completeParentEntity = _orgService.Retrieve(parentEntity.LogicalName, parentEntity.Id, new ColumnSet(true));
                            }
                            // Get the related child entity reference from the "RelatedEntities" context.InputParameters

                            if (context.InputParameters.Contains("RelatedEntities") && context.InputParameters["RelatedEntities"] is EntityReferenceCollection)
                            {
                                relatedEntities = context.InputParameters["RelatedEntities"] as EntityReferenceCollection;
                                childtEntity = relatedEntities[0];

                                Entity completeChildEntity = _orgService.Retrieve(childtEntity.LogicalName, childtEntity.Id, new ColumnSet(true));
                                EntityReference team = EventHelper.GetRelatedTeam(_orgService, parentEntity.Id);
                                System.Guid[] entityId = new Guid[] { completeChildEntity.Id};

                                if (team != null)
                                {
                                    EventHelper.AssignUsersToTeam(team.Id, _orgService, entityId);
                                }
                            }
                        }
                    }
                }
            }

        }

        void RemoveRelation(IOrganizationService service, Guid userId,Guid eventId) 
        {
            Guid[] user = new Guid[1];
            user[0] = userId;
            EntityReference team = EventHelper.GetRelatedTeam(service, eventId);

            if (EventHelper.IsTeamMember(team.Id, userId, service))
            {
                EventHelper.RemoveUsersToTeam(team.Id, service, user);
            }
        }

       

        bool CheckIfExistInOtherRealationship(IOrganizationService service, string relationshipName,string eretrieveLogicalName,EntityReference target) 
        {
            string[] columns = new string[1];
            columns[0] = "systemuserid";
            DataCollection<Entity> result = EventHelper.GetRelatedEntitDataFromManyToManyRelation(eretrieveLogicalName,columns, relationshipName,target,service);

            if (result == null) 
            {
                return false;
            }
            return true;
        }

    }

}
                   

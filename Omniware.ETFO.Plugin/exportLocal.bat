@echo off
echo.
echo Update solution directory with the content of Local
echo.

setlocal enableextensions enabledelayedexpansion

if "" == "%crm_connection%" (
	echo Environment Variable crm_connection not defined 
	exit /b 1
) 


set connection="%crm_connection%"
set solutionname="OEMSAdministration"

set solutionDir="..\CRMSolution" 
set file="..\tmp\OEMSAdministrationLocal.zip"
set tools="..\tools"
rem /logpath:%log% seems to cause the export to hang 
rem set log="..\tmp\export.log"

mkdir ..\tmp
echo on

del %file%
%tools%\Solution.Cmd.Helper.exe /operation:export /solutionname:%solutionname% /crmconnectionstring:%connection% /solutionfilepath:%file% || ( Echo EXPORT Error & EXIT /B 1)

rem Packager sometimes will not update webresources until they are deleted first 
mkdir %solutionDir%
rmdir %solutionDir% /s /q || ( Echo EXTRACT Error Files may be in use & EXIT /B 1)
%tools%\solutionPackager 	   /action:extract    /folder:%solutionDir%  /zipfile:%file% /packagetype:Unmanaged /allowWrite:yes /allowDelete:Yes /clobber || ( Echo PACKAGE Error & EXIT /B 1)

@echo off

endlocal
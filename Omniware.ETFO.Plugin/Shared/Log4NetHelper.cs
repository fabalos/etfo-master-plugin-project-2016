﻿using System;
using System.IO;
using System.Reflection;
using System.Security.Principal;
using log4net;
using log4net.Config;

namespace Omniware.ETFO.Plugin.Shared
{
    // ensures Log4Net is configured once on first log requests
    public class LogHelper
    {
        private static volatile bool isConfigured = false;
        private static object monitor = "monitor";

        static void Configure()
        {
            lock (monitor)
            {
                if (!isConfigured)
                {

/*                  Set variables if properties are used
                    GlobalContext.Properties["LogDir"] =  @"c:\logs"; // read it from the configuration

                    WindowsIdentity identity = WindowsIdentity.GetCurrent();
                    string appPoolName = identity != null
                                             ? identity.Name.Replace("IIS APPPOOL", "").Replace("\\", "").Replace(" ", "_")
                                             : string.Empty;

                    GlobalContext.Properties["Identity"] = appPoolName;
*/

                    string processName = System.Diagnostics.Process.GetCurrentProcess().ProcessName;
                    GlobalContext.Properties["Name"] = processName;

                    var config = Assembly.GetExecutingAssembly().GetManifestResourceStream("Omniware.ETFO.Plugin.Config.log4net.config");
                    XmlConfigurator.Configure(config);
                    LogManager.GetLogger(typeof(LogHelper)).Info("Logging initialized for process "+processName);

                    isConfigured = true; // make sure it is initialized only one in each process
                }
            }
        }

        // lazily configures log4net and returns logger for the class
        public static ILog GetLogger(Type type)
        {
            Configure();
            return  LogManager.GetLogger(type);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Utilities
{
    public class StringCodes
    {
        #region publish event strings

        public const string THIS_EVENT_HAS_BEEN_PUBLISHED = "THIS_EVENT_HAS_BEEN_PUBLISHED";
        public const string FOLLOWING_FIELDS_ARE_INVALID = "FOLLOWING_FIELDS_ARE_INVALID";
        public const string MODIFY_FIELDS_AND_PUBLISH_AGAIN = "MODIFY_FIELDS_AND_PUBLISH_AGAIN";

        #region General

        public const string FIELDNAME_EVENT_NAME = "FIELDNAME_EVENT_NAME";
        public const string EVENT_NAME_EMPTY = "EVENT_NAME_EMPTY";
        public const string FIELDNAME_SUBCATEGORY = "FIELDNAME_SUBCATEGORY";
        public const string SUBCATEGORY_EMPTY = "SUBCATEGORY_EMPTY";
        public const string FIELDNAME_EVENT_YEAR = "FIELDNAME_EVENT_YEAR";
        public const string EVENT_YEAR_EMPTY = "EVENT_YEAR_EMPTY";
        public const string EVENT_YEAR_BEFORE_CURRENT_YEAR = "EVENT_YEAR_BEFORE_CURRENT_YEAR";
        public const string FIELDNAME_EVENT_DESCRIPTION = "FIELDNAME_EVENT_DESCRIPTION";
        public const string EVENT_DESCRIPTION_EMPTY = "EVENT_DESCRIPTION_EMPTY";
        public const string FIELDNAME_START_END_DATE = "FIELDNAME_START_END_DATE";
        public const string START_END_DATE_EMPTY = "START_END_DATE_EMPTY";
        public const string END_DATE_BEFORE_TODAY = "END_DATE_BEFORE_TODAY";
        public const string END_DATE_BEFORE_START_DATE = "END_DATE_BEFORE_START_DATE";
        public const string FIELDNAME_EVENT_LOCATION = "FIELDNAME_EVENT_LOCATION";
        public const string EVENT_LOCATION_EMPTY = "EVENT_LOCATION_EMPTY";
        public const string EVENT_LOCATION_INVALID = "EVENT_LOCATION_INVALID";
        public const string FIELDNAME_BUDGET_NUMBER = "FIELDNAME_BUDGET_NUMBER";
        public const string BUDGET_NUMBER_EMPTY = "BUDGET_NUMBER_EMPTY";
        public const string FIELDNAME_SERVICE_AREA = "FIELDNAME_SERVICE_AREA";
        public const string SERVICE_AREA_EMPTY = "SERVICE_AREA_EMPTY";
        public const string FIELDNAME_EXECUTIVE_STAFF = "FIELDNAME_EXECUTIVE_STAFF";
        public const string EXECUTIVE_STAFF_LIST_EMPTY = "EXECUTIVE_STAFF_LIST_EMPTY";
        public const string FIELDNAME_SUPPORT_STAFF = "FIELDNAME_SUPPORT_STAFF";
        public const string SUPPORT_STAFF_LIST_EMPTY = "SUPPORT_STAFF_LIST_EMPTY";

        #endregion General

        #region Registration Application

        public const string FIELDNAME_REG_MIN_MAX = "FIELDNAME_REG_MIN_MAX";
        public const string REG_MIN_OR_MAX_EMPTY = "REG_MIN_OR_MAX_EMPTY";
        public const string REG_MIN_GREATER_THAN_MAX = "REG_MIN_GREATER_THAN_MAX";
        public const string FIELDNAME_REG_OPEN_DEADLINE = "FIELDNAME_REG_OPEN_DEADLINE";
        public const string REG_OPEN_DEADLINE_EMPTY = "REG_OPEN_DEADLINE_EMPTY";
        public const string FIELDNAME_REG_OPEN = "FIELDNAME_REG_OPEN";
        public const string REG_OPEN_BEFORE_TODAY = "REG_OPEN_BEFORE_TODAY";
        public const string REG_OPEN_AFTER_DEADLINE = "REG_OPEN_AFTER_DEADLINE";
        public const string FIELDNAME_EVENT_REG_SCHEDULES = "FIELDNAME_EVENT_REG_SCHEDULES";
        public const string REG_SCHEDULE_LIST_EMPTY = "REG_SCHEDULE_LIST_EMPTY";
        public const string FIELDNAME_REG_SCHEDULE = "FIELDNAME_REG_SCHEDULE";
        public const string SCHEDULE_START_BEFORE_OPEN = "SCHEDULE_START_BEFORE_OPEN";
        public const string SCHEDULE_START_BEFORE_TODAY = "SCHEDULE_START_BEFORE_TODAY";
        public const string SCHEDULE_START_AFTER_DEADLINE = "SCHEDULE_START_AFTER_DEADLINE";
        public const string SCHEDULE_START_EMPTY = "SCHEDULE_START_EMPTY";
        public const string SCHEDULE_END_AFTER_DEADLINE = "SCHEDULE_END_AFTER_DEADLINE";
        public const string SCHEDULE_END_EMPTY = "SCHEDULE_END_EMPTY";
        public const string FIELDNAME_EVENT_TEMPLATES = "FIELDNAME_EVENT_TEMPLATES";
        public const string EVENT_TEMPLATES_EMPTY = "EVENT_TEMPLATES_EMPTY";

        #endregion Registration Application

        #region Presenter Application

        public const string FIELDNAME_WORK_MIN_MAX = "FIELDNAME_WORK_MIN_MAX";
        public const string WORK_MIN_GREATER_THAN_MAX = "WORK_MIN_GREATER_THAN_MAX";
        public const string FIELDNAME_WORK_OPEN_DEADLINE = "FIELDNAME_WORK_OPEN_DEADLINE";
        public const string WORK_OPEN_DEADLINE_EMPTY = "WORK_OPEN_DEADLINE_EMPTY";
        public const string FIELDNAME_WORK_OPEN = "FIELDNAME_WORK_OPEN";
        public const string WORK_OPEN_BEFORE_TODAY = "WORK_OPEN_BEFORE_TODAY";
        public const string WORK_OPEN_AFTER_DEADLINE = "WORK_OPEN_AFTER_DEADLINE";
        public const string WORK_SCHEDULE_LIST_EMPTY = "WORK_SCHEDULE_LIST_EMPTY";
        public const string FIELDNAME_EVENT_WORK_SCHEDULES = "FIELDNAME_EVENT_WORK_SCHEDULES";
        public const string FIELDNAME_WORK_SCHEDULE = "FIELDNAME_WORK_SCHEDULE";
        public const string SCHEDULE_WORK_START_BEFORE_OPEN = "SCHEDULE_START_BEFORE_OPEN";
        public const string SCHEDULE_WORK_START_BEFORE_TODAY = "SCHEDULE_START_BEFORE_TODAY";
        public const string SCHEDULE_WORK_START_AFTER_DEADLINE = "SCHEDULE_START_AFTER_DEADLINE";
        public const string SCHEDULE_WORK_START_EMPTY = "SCHEDULE_START_EMPTY";
        public const string SCHEDULE_WORK_END_AFTER_DEADLINE = "SCHEDULE_END_AFTER_DEADLINE";
        public const string EVENT_WORK_TEMPLATES_EMPTY = "EVENT_WORK_TEMPLATES_EMPTY";
        public const string FIELDNAME_INSTRUCTOR_COURSE_AGREEMENT_BLANK_FORM = "FIELDNAME_INSTRUCTOR_COURSE_AGREEMENT_BLANK_FORM";
        public const string FIELDNAME_INSTRUCTOR_PAYROLLENROLLMENT_BLANK_FORM = "FIELDNAME_INSTRUCTOR_PAYROLLENROLLMENT_BLANK_FORM";
        public const string INSTRUCTOR_COURSE_AGREEMENT_BLANK_FORM_EMPTY = "INSTRUCTOR_COURSE_AGREEMENT_BLANK_FORM_EMPTY";
        public const string INSTRUCTOR_PAYROLLENROLLMENT_BLANK_FORM_EMPTY = "INSTRUCTOR_PAYROLLENROLLMENT_BLANK_FORM_EMPTY";
        public const string FIELDNAME_WEEKS_OF_AVAILABILITY = "FIELDNAME_WEEKS_OF_AVAILABILITY";
        public const string WEEKS_OF_AVAILABILITY_EMPTY = "WEEKS_OF_AVAILABILITY_EMPTY";
        public const string FIELDNAME_APPLICATION_TOPICS = "FIELDNAME_APPLICATION_TOPICS";
        public const string APPLICATION_TOPICS_EMPTY = "APPLICATION_TOPICS_EMPTY";
        public const string FIELDNAME_TARGET_GRADES = "FIELDNAME_TARGET_GRADES";
        public const string TARGET_GRADES_EMPTY = "TARGET_GRADES_EMPTY";
        public const string FIELDNAME_SAMPLE_PARTICIPATION_LETTER = "FIELDNAME_SAMPLE_PARTICIPATION_LETTER";
        public const string SAMPLE_PARTICIPATION_LETTER_EMPTY = "SAMPLE_PARTICIPATION_LETTER_EMPTY";
        public const string FIELDNAME_LETTER_TEMPLATE = "FIELDNAME_LETTER_TEMPLATE";
        public const string LETTER_TEMPLATE_EMPTY = "LETTER_TEMPLATE_EMPTY";


        #endregion Presenter Application

        #region Caucus

        public const string FIELDNAME_CAUCUS = "FIELDNAME_CAUCUS";
        public const string CAUCUS_SCHEDULE_NAME_EMPTY = "CAUCUS_SCHEDULE_NAME_EMPTY";
        public const string CAUCUS_SCHEDULE_DESCRIPTION_EMPTY = "CAUCUS_SCHEDULE_DESCRIPTION_EMPTY";
        public const string CAUCUS_SCHEDULE_TIME_SLOT_INVALID = "CAUCUS_SCHEDULE_TIME_SLOT_INVALID";
        public const string CAUCUSE_SCHEDULE_DUPLICATE_TIME_SLOT = "CAUCUSE_SCHEDULE_DUPLICATE_TIME_SLOT";

        #endregion Caucus

        #region Child Care

        public const string FIELDNAME_CHILDCARE_AGE = "FIELDNAME_CHILDCARE_AGE";
        public const string MIN_AGE_GREATER_THAN_MAX_AGE = "MIN_AGE_GREATER_THAN_MAX_AGE";
        public const string FIELDNAME_CHILD_CARE = "FIELDNAME_CHILD_CARE";
        public const string CHILDCARE_SCHEDULE_TIME_SLOT_INVALID = "CHILDCARE_SCHEDULE_TIME_SLOT_INVALID";
        public const string CHILDCARE_SCHEDULE_TIME_SLOTS_DUPLICATED = "CHILDCARE_SCHEDULE_TIME_SLOTS_DUPLICATED";

        #endregion Child Care

        #region Accommodation

        public const string FIELDNAME_ACCOMMODATION = "FIELDNAME_ACCOMMODATION";
        public const string ACCOMMODATION_LOCATION_EMPTY = "ACCOMMODATION_LOCATION_EMPTY";
        public const string ACCOMMODATION_SCHEDULE_TIME_SLOT_INVALID = "ACCOMMODATION_SCHEDULE_TIME_SLOT_INVALID";
        public const string ACCOMMODATION_SCHEDULE_TIME_SLOTS_DUPLICATED = "ACCOMMODATION_SCHEDULE_TIME_SLOTS_DUPLICATED";

        #endregion Accommodation

        #region Invited Guest

        public const string FIELDNAME_INVITED_GUESTS = "FIELDNAME_INVITED_GUESTS";
        public const string INVITED_GUEST_NAME_EMPTY = "INVITED_GUEST_NAME_EMPTY";
        public const string INVITED_GUEST_DESCRIPTION_EMPTY = "INVITED_GUEST_DESCRIPTION_EMPTY";
        public const string INVITED_GUEST_INVITEE_EMPTY = "INVITED_GUEST_INVITEE_EMPTY";
        public const string INVITED_GUEST_LIST_INVITEES_DUPLICATED = "INVITED_GUEST_LIST_INVITEES_DUPLICATED";

        #endregion Invited Guest

        #region Local Delegation

        public const string FIELDNAME_LOCAL_DELEGATION = "FIELDNAME_LOCAL_DELEGATION";
        public const string LOCAL_DELEGATION_LIMIT_NEGATIVE = "LOCAL_DELEGATION_LIMIT_NEGATIVE";
        public const string LOCAL_DELEGATION_LOCALS_DUPLICATED = "LOCAL_DELEGATION_LOCALS_DUPLICATED";

        #endregion Local Delegation

        #region Release Time

        public const string FIELDNAME_RELEASE_TIME = "FIELDNAME_RELEASE_TIME";
        public const string RELEASE_TIME_SLOT_INVALID = "RELEASE_TIME_SLOT_INVALID";
        public const string RELEASE_TIME_SLOTS_DUPLICATED = "RELEASE_TIME_SLOTS_DUPLICATED";

        #endregion Release Time

        #region Waiting List

        public const string WAITINGLIST_SIZE_EQUAL_ZERO = "WAITINGLIST_SIZE_EQUAL_ZERO";
        public const string FIELDNAME_WAITINGLIST_SIZE = "FIELDNAME_WAITINGLIST_SIZE";
        public const string REG_MAX_SIZE_EQUAL_ZERO = "REG_MAX_SIZE_EQUAL_ZERO";
        public const string FIELDNAME_REG_MAX_SIZE = "FIELDNAME_REG_MAX_SIZE";

        #endregion

        #region Billing

        public static string FIELDNAME_CHEQUE_DUE_DATE = "FIELDNAME_CHEQUE_DUE_DATE";
        public static string CHEQUE_DUE_DATE_EMPTY = "CHEQUE_DUE_DATE_EMPTY";

        #endregion

        #region Course Term

        public const string FIELDNAME_WEB_TERM_DESCRIPTION = "FIELDNAME_WEB_TERM_DESCRIPTION";
        public const string WEB_TERM_DESCRIPTION_EMPTY = "WEB_TERM_DESCRIPTION_EMPTY";

        #endregion

        #region Course Package Email

        public const string COURSE_PACKAGE_EMAIL_FROM = "COURSE_PACKAGE_EMAIL_FROM";
        public const string COURSE_PACKAGE_EMAIL_HTML_BODY = "COURSE_PACKAGE_EMAIL_HTML_BODY";
        public const string COURSE_PACKAGE_EMAIL_SUBJECT = "COURSE_PACKAGE_EMAIL_SUBJECT";

        #endregion

        #region Course

        public const string COURSE_OVERLAPPING_VALIDATION_MESSAGE = "COURSE_OVERLAPPING_VALIDATION_MESSAGE";
        public const string COURSE_LOCATION_EMPTY = "COURSE_LOCATION_EMPTY";
        public const string COURSE_LOCAL_HOST_EMPTY = "COURSE_LOCAL_HOST_EMPTY";
        public const string COURSE_TIME_SLOT_EMPTY = "COURSE_TIME_SLOT_EMPTY";
        public const string COURSE_DATE_SLOT_EMPTY = "COURSE_DATE_SLOT_EMPTY";
        public const string COURSE_TARGET_GRADES_EMPTY = "COURSE_TARGET_GRADES_EMPTY";
        public const string COURSE_TOPIC_EMPTY = "COURSE_TOPIC_EMPTY";
        public const string COURSE_TERM_EMPTY = "COURSE_TERM_EMPTY";
        public const string COURSE_CODE_EMPTY = "COURSE_CODE_EMPTY";
        public const string FIELDNAME_COURSE_TERM = "FIELDNAME_COURSE_TERM";
        public const string FIELDNAME_COURSE_CODE = "FIELDNAME_COURSE_CODE";
        public const string FIELDNAME_COURSE_TARGET_GRADES = "FIELDNAME_COURSE_TARGET_GRADES";
        public const string FIELDNAME_COURSE_DATE_SLOT = "FIELDNAME_COURSE_DATE_SLOT";
        public const string FIELDNAME_COURSE_TIME_SLOT = "FIELDNAME_COURSE_TIME_SLOT";
        public const string FIELDNAME_COURSE_TOPIC = "FIELDNAME_COURSE_TOPIC";
        public const string FIELDNAME_COURSE_LOCAL_HOST = "FIELDNAME_COURSE_LOCAL_HOST";
        public const string FIELDNAME_COURSE_LOCATION = "FIELDNAME_COURSE_LOCATION";

        #endregion

        #region Event Child Care Schedule

        public const string CHILD_CARE_OVERLAPPING_VALIDATION_MESSAGE = "CHILD_CARE_OVERLAPPING_VALIDATION_MESSAGE";

        #endregion

        #endregion publish event strings
    }
}

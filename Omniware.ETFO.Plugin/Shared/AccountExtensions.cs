﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xrm;

namespace Omniware.ETFO.Plugin.Shared
{
    public static class AccountExtensions
    {
        public static bool IsMember(this Account account )
        {
            return account.oems_MemberFlag.GetValueOrDefault();
        }

        
        public static bool IsNonMemberEducatorOrExternal(this Account account)
        {
            return !account.IsMember() && 
                (account.oems_NonMemberType.GetValueOrDefault() == NonMemberType.TEACHER 
                 || account.oems_NonMemberType.GetValueOrDefault() == NonMemberType.NON_EDUCATOR);  
        } 

        public static bool IsNonMemberStaff(this Account account)
        {
            return !account.IsMember() && 
                (account.oems_NonMemberType.GetValueOrDefault() == NonMemberType.STAFF);  
        } 


        public static bool IsGuestOfEvent(this Account account, oems_Event theEvent)
        {
            return theEvent.oems_event_to_eventinvitedguest.Any(guest => guest.oems_Invitee.Id == account.Id);  
        }

        public static bool IsGuestOfEvent(this Contact contact, oems_Event theEvent)
        {
            return theEvent.oems_event_to_eventinvitedguest.Any(guest => guest.oems_Invitee.Id == contact.Id);
        }


    }
}

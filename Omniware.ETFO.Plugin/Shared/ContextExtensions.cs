﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Sdk;

namespace Omniware.ETFO.Plugin.Shared
{


    public static class ContextExtensions
    {
        // smarter save method ala Hibernate
        public static void CreateOrUpdate(this CrmOrganizationServiceContext omniContext, Entity entity)
        {
            if (entity.Id == Guid.Empty)
            {
                omniContext.AddObject(entity);
            }
            else
            {
                omniContext.UpdateObject(entity);
            }
        }


    }

}

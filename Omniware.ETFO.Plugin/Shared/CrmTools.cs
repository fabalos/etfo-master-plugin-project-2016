﻿using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xrm;

namespace Shared.Utilities
{
    public class CrmTools
    {
        private IOrganizationService service;

        public CrmTools(IOrganizationService _service){
           service = _service;
        }

        public void associateRelatedEntityToEntity(EntityReference relatedEntity, EntityReference entity, string relationShipName)
        {
            // Creating EntityReferenceCollection for the related entity
            EntityReferenceCollection relatedEntities = new EntityReferenceCollection();

            // Add the related entity 
            relatedEntities.Add(relatedEntity);

            Relationship relationship = new Relationship(relationShipName);

            service.Associate(entity.LogicalName, entity.Id, relationship, relatedEntities);

        }

        public void associateRelatedEntitiesToEntity(EntityReferenceCollection relatedEntities, EntityReference entity, string relationShipName)
        {
            Relationship relationship = new Relationship(relationShipName);

            service.Associate(entity.LogicalName, entity.Id, relationship, relatedEntities);
        }

        public void associateRelatedEntitiesToEntity(DataCollection<Entity> relatedEntities, EntityReference entity, string relationShipName)
        {
            EntityReferenceCollection relatedEntityRFs = new EntityReferenceCollection();

            foreach (Entity _entity in relatedEntities)
            {
                EntityReference entityRef = new EntityReference() { LogicalName = _entity.LogicalName, Id = _entity.Id };
                relatedEntityRFs.Add(entityRef);
            }

            associateRelatedEntitiesToEntity(relatedEntityRFs, entity, relationShipName);
        }

        public void associateRelatedEntitiesToEntity(List<Entity> relatedEntities, EntityReference entity, string relationShipName)
        {
            EntityReferenceCollection relatedEntityRFs = new EntityReferenceCollection();

            foreach (Entity _entity in relatedEntities)
            {
                EntityReference entityRef = new EntityReference() { LogicalName = _entity.LogicalName, Id = _entity.Id };
                relatedEntityRFs.Add(entityRef);
            }

            associateRelatedEntitiesToEntity(relatedEntityRFs, entity, relationShipName);
        }

        public void setLookupField(List<Entity> relatedEntities, EntityReference entity, string lookupName)
        {
            foreach (Entity _entity in relatedEntities)
            {
                Entity newEntity = new Entity(_entity.LogicalName);
                newEntity[lookupName] = entity;
                newEntity.Id = _entity.Id;
                service.Update(newEntity);
            }
        }


        public DataCollection<Entity> getRelatedEntitDataFromManyToManyRelation(string entityToRetrieve, string relationName, EntityReference targetEntity, IOrganizationService service)
        {
            DataCollection<Entity> result = null;

            QueryExpression query = new QueryExpression();
            query.EntityName = entityToRetrieve;
            query.ColumnSet = new ColumnSet(true);
            Relationship relationship = new Relationship();

            relationship.SchemaName = relationName;

            RelationshipQueryCollection relatedEntity = new RelationshipQueryCollection();
            relatedEntity.Add(relationship, query);
            RetrieveRequest request = new RetrieveRequest();
            request.RelatedEntitiesQuery = relatedEntity;
            request.ColumnSet = new ColumnSet(true);
            request.Target = targetEntity;
            RetrieveResponse response = (RetrieveResponse)service.Execute(request);


            if (((DataCollection<Relationship, EntityCollection>)(((RelatedEntityCollection)(response.Entity.RelatedEntities)))).Contains(new Relationship(relationName)) && ((DataCollection<Relationship, EntityCollection>)(((RelatedEntityCollection)(response.Entity.RelatedEntities))))[new Relationship(relationName)].Entities.Count > 0)
            {
                result = ((DataCollection<Relationship, EntityCollection>)(((RelatedEntityCollection)(response.Entity.RelatedEntities))))[new Relationship(relationName)].Entities;
            }

            return result;
        }

        public List<Entity> getRelatedEntities(IOrganizationService service, EntityReference oems_event, string entityName) 
        {
            Xrm.OmniContext context = new Xrm.OmniContext(service);
            List<Entity> fullList = (from c in context.CreateQuery(entityName)
                 where (Guid)c["oems_courseterm"] == oems_event.Id
                 select c
              ).ToList<Entity>();
        
         return fullList;
        }

        public List<Entity> getRelatedEntities(IOrganizationService service, EntityReference oems_event, string entityName, string relatedEventField)
        {
            Xrm.OmniContext context = new Xrm.OmniContext(service);
            List<Entity> fullList = (from c in context.CreateQuery(entityName)
                                     where ((EntityReference)c[relatedEventField]).Id == oems_event.Id
                                     select c
              ).ToList<Entity>();

            return fullList;
        }

        public EntityCollection getRelatedEntitiesForEvent(IOrganizationService service, EntityReference oems_event, string entityName, string relatedEventField)
        {
            QueryExpression query = new QueryExpression(entityName);
            query.ColumnSet.AllColumns = true;
            query.Criteria.AddCondition(relatedEventField, ConditionOperator.Equal, oems_event.Id);

            var result = service.RetrieveMultiple(query);
            return result;

        }

        public DataCollection<Entity> getRelatedEntities(ColumnSet columns, string entityName, Guid entityId, string relatedEntityName, ConditionExpression condition, string schemaName)
        {

            QueryExpression query = new QueryExpression();
            query.EntityName = relatedEntityName;
            query.ColumnSet = new ColumnSet(true);
            Relationship relationship = new Relationship();

            query.Criteria = new FilterExpression();
            query.Criteria.AddCondition(condition);

            relationship.SchemaName = schemaName;
            RelationshipQueryCollection relatedEntity = new RelationshipQueryCollection();
            relatedEntity.Add(relationship, query);
            RetrieveRequest request = new RetrieveRequest();
            request.RelatedEntitiesQuery = relatedEntity;
            request.ColumnSet = columns;
            request.Target = new EntityReference
            {
                Id = entityId,
                LogicalName = entityName

            };
            RetrieveResponse response = (RetrieveResponse)service.Execute(request);

            if (((DataCollection<Relationship, EntityCollection>)(((RelatedEntityCollection)(response.Entity.RelatedEntities)))).Contains(new Relationship(schemaName)) &&
                ((DataCollection<Relationship, EntityCollection>)(((RelatedEntityCollection)(response.Entity.RelatedEntities))))[new Relationship(schemaName)].Entities.Count > 0)
                return ((DataCollection<Relationship, EntityCollection>)(((RelatedEntityCollection)(response.Entity.RelatedEntities))))[new Relationship(schemaName)].Entities;
            else
                return null;
        }

        public DataCollection<Entity> getRelatedEntities(ColumnSet columns, string entityName, Guid entityId, string relatedEntityName, string schemaName, params ConditionExpression[] expressions)
        {

            QueryExpression query = new QueryExpression();
            query.EntityName = relatedEntityName;
            query.ColumnSet = new ColumnSet(true);
            Relationship relationship = new Relationship();

            query.Criteria = new FilterExpression();

            foreach (ConditionExpression exp in expressions)
            {
                query.Criteria.AddCondition(exp);
            }


            relationship.SchemaName = schemaName;
            RelationshipQueryCollection relatedEntity = new RelationshipQueryCollection();
            relatedEntity.Add(relationship, query);
            RetrieveRequest request = new RetrieveRequest();
            request.RelatedEntitiesQuery = relatedEntity;
            request.ColumnSet = columns;
            request.Target = new EntityReference
            {
                Id = entityId,
                LogicalName = entityName

            };
            RetrieveResponse response = (RetrieveResponse)service.Execute(request);

            if (((DataCollection<Relationship, EntityCollection>)(((RelatedEntityCollection)(response.Entity.RelatedEntities)))).Contains(new Relationship(schemaName)) &&
                ((DataCollection<Relationship, EntityCollection>)(((RelatedEntityCollection)(response.Entity.RelatedEntities))))[new Relationship(schemaName)].Entities.Count > 0)
                return ((DataCollection<Relationship, EntityCollection>)(((RelatedEntityCollection)(response.Entity.RelatedEntities))))[new Relationship(schemaName)].Entities;
            else
                return null;
        }

        public void disAssociateRelatedEntityToEntity(EntityReference relatedEntity, EntityReference entity, string relationShipName)
        {
            // Creating EntityReferenceCollection for the related entity
            EntityReferenceCollection relatedEntities = new EntityReferenceCollection();

            // Add the related entity 
            relatedEntities.Add(relatedEntity);

            Relationship relationship = new Relationship(relationShipName);

            service.Disassociate(entity.LogicalName, entity.Id, relationship, relatedEntities);

        }

        public Entity getEntity(string entityName, string attributeName, object attributeValue)
        {
            Entity ret = null;

            ret = getEntity(entityName, attributeName, attributeValue, new ColumnSet(true));

            return ret;
        }

        public Entity getEntity(string entityName, string attributeName, object attributeValue, ColumnSet columns)
        {
            Entity ret = null;

            var query = new QueryByAttribute(entityName) { ColumnSet = columns };

            query.Attributes.AddRange(attributeName);
            query.Values.AddRange(attributeValue);

            EntityCollection retrieved = service.RetrieveMultiple(query);
            if (retrieved != null && retrieved.Entities != null && retrieved.Entities.Count > 0)
            {
                ret = retrieved.Entities[0];
            }

            return ret; //service.RetrieveMultiple(query).Entities.FirstOrDefault();
        }

        public static object getEnitiyAttributeSafely(Entity entity, string attributeName)
        {
            object ret = null;

            if (entityAttributeExists(entity, attributeName))
            {
                ret = entity[attributeName];
            }

            return ret;
        }

        public static bool entityAttributeExists(Entity entity, string attributeName)
        {
            bool ret = false;
            
            ret = entity != null && entity.Contains(attributeName) && entity[attributeName] != null;

            return ret;
        }

        public OptionMetadataCollection getOptionsSet(string entityName, string attributeName)
        {
            var retrieveAttributeRequest = new RetrieveAttributeRequest();

            retrieveAttributeRequest.EntityLogicalName = entityName;
            retrieveAttributeRequest.LogicalName = attributeName;
            retrieveAttributeRequest.RetrieveAsIfPublished = true;

            var retrieveAttributeResponse = (RetrieveAttributeResponse)service.Execute(retrieveAttributeRequest);
            var optionMetadataCollection = (((EnumAttributeMetadata)retrieveAttributeResponse.AttributeMetadata).OptionSet).Options;

            return optionMetadataCollection;
        }

        public void assignWebPageAccessControlRuleToWebRole(EntityReference webpageAccessControlRole, string webRoleName)
        {
            EntityReference webRoleRf = getEntity("adx_webrole", "adx_name", webRoleName).ToEntityReference();
            associateRelatedEntityToEntity(webpageAccessControlRole, webRoleRf, "adx_webpageaccesscontrolrule_webrole");
        }

        public void removeWebPageAccessControlRuleFromWebRole(EntityReference webpageAccessControlRole, string webRoleName){
            EntityReference webRoleRf = getEntity("adx_webrole", "adx_name", webRoleName).ToEntityReference();
            disAssociateRelatedEntityToEntity(webpageAccessControlRole, webRoleRf, "adx_webpageaccesscontrolrule_webrole");
        }

        public bool isWebPageAccessRuleAssignedToWebRole(EntityReference webPageAccessRuleRef, EntityReference webRoleRef)
        {
            bool ret = false;

            DataCollection<Entity> webPageAccessRules = getRelatedEntities(
                                                    new ColumnSet("adx_webroleid"),
                                                    webRoleRef.LogicalName,
                                                    webRoleRef.Id,
                                                    "adx_webpageaccesscontrolrule",
                                                    new ConditionExpression("adx_webpageaccesscontrolruleid", ConditionOperator.Equal, webPageAccessRuleRef.Id),
                                                    "adx_webpageaccesscontrolrule_webrole");
            if (webPageAccessRules != null && webPageAccessRules.Count > 0)
            {
                ret = true;
            }

            return ret;
        }

        public Entity createWebPageAccessRule(Entity oemsEvent, string websiteName)
        {
            //create restrict read webpage access control rule on the event webpage
            Entity readWebPageAccessControlRule = new Entity("adx_webpageaccesscontrolrule");

            readWebPageAccessControlRule["adx_name"] = "Restrict Read";// to event: " + ((Guid)oemsEvent["oems_eventid"]).ToString();//(string)eventWebPage["adx_name"];
            readWebPageAccessControlRule["adx_websiteid"] = getEntity("adx_website", "adx_name", websiteName).ToEntityReference();
            readWebPageAccessControlRule["adx_webpageid"] = oemsEvent["oems_webpage"];
            readWebPageAccessControlRule["adx_right"] = new OptionSetValue(2); // 1: Grant Change, 2: Restrict Read

            readWebPageAccessControlRule.Id = service.Create(readWebPageAccessControlRule);

            return readWebPageAccessControlRule;
        }
/*
        public Entity getWebPageAccessRule(EntityReference webPageRef){
            Entity ret = null;

            var query = new QueryByAttribute("adx_webpageaccesscontrolrule") { ColumnSet = new ColumnSet(true) };

            query.Attributes.AddRange("adx_webpageid");
            query.Values.AddRange(webPageRef);

            EntityCollection retrieved = service.RetrieveMultiple(query);
            if (retrieved != null && retrieved.Entities != null && retrieved.Entities.Count > 0)
            {
                ret = retrieved.Entities[0];
            }

            return ret;
        }
*/
        public Entity createWebRole(string webRoleName, string websiteName)
        {
            Entity webRole = new Entity("adx_webrole");

            webRole["adx_name"] = webRoleName;
            webRole["adx_websiteid"] = getEntity("adx_website", "adx_name", websiteName).ToEntityReference();

            webRole.Id = service.Create(webRole);

            return webRole;
        }

        public static void RunEmailNotification(Account account, IOrganizationService service, string from, string body, string subject)
        {
            StringManager stringManager = new StringManager(service);
            OmniContext omniCon = new OmniContext(service);

            string fromSystemUser = stringManager.getString(from, "");

            SystemUser fromUser = (from u in omniCon.SystemUserSet
                                   where u.FullName == fromSystemUser
                                   select u).FirstOrDefault();

            var emailTo = new ActivityParty { AddressUsed = account.EMailAddress1 };
            var emailFrom = new ActivityParty { PartyId = fromUser.ToEntityReference() };

            var htmlBody = new StringBuilder();

            htmlBody.Append(stringManager.getString(body, ""));

            var email = new Email
            {
                To = new[] { emailTo },
                From = new[] { emailFrom },
                Subject = stringManager.getString(subject, ""),
                Description = htmlBody.ToString()
            };

            omniCon.AddObject(email);
            omniCon.SaveChanges();

            var trackingTokenEmailRequest = new GetTrackingTokenEmailRequest();
            var trackingTokenEmailResponse = (GetTrackingTokenEmailResponse)omniCon.Execute(trackingTokenEmailRequest);

            var sendEmailreq = new SendEmailRequest
            {
                EmailId = email.Id,
                TrackingToken = trackingTokenEmailResponse.TrackingToken,
                IssueSend = true
            };

            var sendEmailresp = (SendEmailResponse)omniCon.Execute(sendEmailreq);
        }

        public static void RunEmailNotification(Contact contact, IOrganizationService service, string from, string body, string subject)
        {
            StringManager stringManager = new StringManager(service);
            OmniContext omniCon = new OmniContext(service);

            string fromSystemUser = stringManager.getString(from, "");

            SystemUser fromUser = (from u in omniCon.SystemUserSet
                                   where u.FullName == fromSystemUser
                                   select u).FirstOrDefault();

            var emailTo = new ActivityParty { AddressUsed = contact.EMailAddress1};
            var emailFrom = new ActivityParty { PartyId = fromUser.ToEntityReference() };

            var htmlBody = new StringBuilder();

            htmlBody.Append(stringManager.getString(body, ""));

            var email = new Email
            {
                To = new[] { emailTo },
                From = new[] { emailFrom },
                Subject = stringManager.getString(subject, ""),
                Description = htmlBody.ToString()
            };

            omniCon.AddObject(email);
            omniCon.SaveChanges();

            var trackingTokenEmailRequest = new GetTrackingTokenEmailRequest();
            var trackingTokenEmailResponse = (GetTrackingTokenEmailResponse)omniCon.Execute(trackingTokenEmailRequest);

            var sendEmailreq = new SendEmailRequest
            {
                EmailId = email.Id,
                TrackingToken = trackingTokenEmailResponse.TrackingToken,
                IssueSend = true
            };

            var sendEmailresp = (SendEmailResponse)omniCon.Execute(sendEmailreq);
        }


    }
}

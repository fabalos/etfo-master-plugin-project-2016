﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Utilities
{
    public class StringManager
    {
        private IOrganizationService service;
        private CrmTools crmTools;

        public StringManager(IOrganizationService _service)
        {
            service = _service;
            crmTools = new CrmTools(service);
        }
        
        public string getString(string stringCode, string lcId, string defaultMessage = ""){
            
            string ret = null;

            Entity msg = null;

            //retrieve oems_message by messageCode
            Entity message = crmTools.getEntity("oems_message", "oems_messagecode", stringCode);

            if (message == null)
            {
                if(defaultMessage != "")
                {
                    return defaultMessage;
                }
                else
                {
                    throw new InvalidPluginExecutionException("The following message string is unassigned: " + stringCode + ". See Settings > Extensions > Messages.");
                }
            }

            //retrieve related localized message by lcId if there is any
            DataCollection<Entity> localizedMessages = null;

            if(!string.IsNullOrEmpty(lcId)){
                localizedMessages =  crmTools.getRelatedEntities(
                                        new ColumnSet("oems_messageid"),
                                        message.LogicalName,
                                        message.Id,
                                        "oems_localizedmessage",
                                        new ConditionExpression("oems_localeid", ConditionOperator.Equal, lcId),
                                        "oems_message_to_localizedmessage");
            }else{
                localizedMessages = crmTools.getRelatedEntities(
                                    new ColumnSet("oems_messageid"),
                                    message.LogicalName,
                                    message.Id,
                                    "oems_localizedmessage",
                                    new ConditionExpression("statuscode", ConditionOperator.Equal, 1),
                                    "oems_message_to_localizedmessage");        
            }

            if(localizedMessages != null && localizedMessages.Count > 0){
                msg = localizedMessages[0];
                if (msg != null)
                {
                    ret = (string)msg["oems_localizedmessage"];
                }
            }else{
                msg = message;
                if (msg != null)
                {
                    ret = (string)msg["oems_defaultmessage"];
                }
            }
            
            return ret;
        }
    }
}

@echo off
setlocal enableextensions enabledelayedexpansion


if "" == "%crm_connection%" (
	echo Environment Variable crm_connection not defined 
	exit /b 1
) 

if "%1" == "clean" (
	echo Local customizations will be overwriten 
	set import_opts=/overwriteunmanagedcustomizations
)

echo Checking if the code needs to be rebuild
set PATH=C:\Windows\Microsoft.NET\Framework\v4.0.30319;%PATH%
msbuild ..\Omniware.ETFO.Plugin.sln  /p:Configuration=Debug


set connection="%crm_connection%"
set solutionname="OEMSAdministration"

set solutionDir="..\CRMSolution" 
set file="..\tmp\OEMSAdministrationLocal.zip"
set tools="..\tools"
set log="..\tmp\import.log"
set solutionimportlog="..\tmp\solutionImport.log"

mkdir ..\tmp
echo on

del %file%

%tools%\solutionPackager /action:pack /zipfile:%file% /folder:%solutionDir% /packagetype:Unmanaged /map:solutionPackagerMapDebug.xml /allowDelete:Yes  /clobber  || ( Echo PACK Error & EXIT /B 1)
%tools%\Solution.Cmd.Helper.exe /operation:import %import_opts%  /solutionname:%solutionname% /crmconnectionstring:%connection% /solutionfilepath:%file% /publishworkflows /logpath:%log% /logfilepath:%solutionimportlog% || ( Echo IMPORT Error & EXIT /B 1)
%tools%\Solution.Cmd.Helper.exe /operation:publish /solutionname:%solutionname% /crmconnectionstring:%connection%  || ( Echo PUBLISH Error & EXIT /B 1)

@echo off
endlocal
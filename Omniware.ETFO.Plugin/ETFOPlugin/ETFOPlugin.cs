﻿using ETFOPlugin.MessageProcessors;
using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;


/*
 * 
 *              Component Name:     ETFOPlugin 
 *              
 *              Description:        This plugin is designed to handle crm events on all entities
 *              
 *              Author:             Lawrence Zhou
 *              Create Date:        Feb. 5th, 2014
 * 
 * */

namespace ETFOPlugin
{
    public class ETFOPlugin : IPlugin
    {
        private string config;

        //for merge shared dll
        static ETFOPlugin()
        {
          AppDomain.CurrentDomain.AssemblyResolve  += OnResolveAssembly;
        }

        public ETFOPlugin(string _config)
        {
            config = _config;
        }

        public void Execute(IServiceProvider serviceProvider)
        {
            // Obtain the execution context from the service provider.
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));

            IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            IOrganizationService service = factory.CreateOrganizationService(context.UserId);

            IMessageProcessor processor = ProcessorFactory.createMessageProcessor(config, context, service);

            if (processor != null)
            {
                processor.process(serviceProvider);
            }
        }

        //for merge shared dll
        private static Assembly OnResolveAssembly(object sender, ResolveEventArgs args)
        {

            Assembly executingAssembly = Assembly.GetExecutingAssembly();
            AssemblyName assemblyName = new AssemblyName(args.Name);
            string path = assemblyName.Name + ".dll";

            if (assemblyName.CultureInfo.Equals(CultureInfo.InvariantCulture) == false)
            {
                path = String.Format(@"{0}\{1}", assemblyName.CultureInfo, path);
            }

            using (Stream stream = executingAssembly.GetManifestResourceStream(path))
            {
                if (stream == null)
                    return null;
                byte[] assemblyRawBytes = new byte[stream.Length];
                stream.Read(assemblyRawBytes, 0, assemblyRawBytes.Length);
                return Assembly.Load(assemblyRawBytes);
            }
        }
    }
}

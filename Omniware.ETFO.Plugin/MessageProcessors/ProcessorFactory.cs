﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETFOPlugin.MessageProcessors
{
    public class ProcessorFactory
    {
        public static IMessageProcessor createMessageProcessor(string config, IPluginExecutionContext context, IOrganizationService service)
        {
            IMessageProcessor ret = null;

            string messageName = context.MessageName.ToLower();
            Entity entity = ProcessorFactory.getEntityFromContext(context, service);

            if (entity != null)
            {
                if (entity.LogicalName == "contact")
                {
                    switch (messageName)
                    {
                        case "create":
                            ret = new ContactCreateProcessor(config);
                            break;
                        case "update":
                            ret = new ContactUpdateProcessor(config);
                            break;
                        case "delete":
                            ret = new ContactDeleteProcessor(config);
                            break;
                        default:
                            ret = null;
                            break;
                    }
                }
                else if (entity.LogicalName == "account")
                {
                    switch (messageName)
                    {
                        case "create":
                            ret = new AccountCreateProcessor(config);
                            break;
                        case "update":
                            ret = new AccountUpdateProcessor(config);
                            break;
                        case "delete":
                            ret = new AccountDeleteProcessor(config);
                            break;
                        default:
                            ret = null;
                            break;
                    }
                }
                else if (entity.LogicalName == "oems_event")
                {
                    switch (messageName)
                    {
                        case "create":
                            ret = new EventCreateProcessor(config);
                            break;
                        case "update":
                            ret = new EventUpdateProcessor(config);
                            break;
                        case "delete":
                            ret = new EventDeleteProcessor(config);
                            break;
                        default:
                            ret = null;
                            break;
                    }
                }
                else if (entity.LogicalName == "adx_webpage")
                {
                    switch (messageName)
                    {
                        case "create":
   //                         ret = new AdxWebPageCreateProcessor(config);
                            break;
                        case "update":
                            ret = new AdxWebPageUpdateProcessor(config);
                            break;
                        case "delete":
   //                         ret = new AdxWebPageDeleteProcessor(config);
                            break;
                        default:
                            ret = null;
                            break;
                    }
                }
                else if (entity.LogicalName == "oems_eventinvitedguest")
                {
                    switch (messageName)
                    {
                        case "create":
                            ret = new InvitedGuestCreateProcessor(config);
                            break;
                        case "update":
                            ret = new InvitedGuestUpdateProcessor(config);
                            break;
                        case "delete":
                            ret = new InvitedGuestDeleteProcessor(config);
                            break;
                        default:
                            ret = null;
                            break;
                    }
                }

            }

            return ret;
        }

        public static Entity getEntityFromContext(IPluginExecutionContext context, IOrganizationService service)
        {
            Entity entity = null;
            EntityReference entityRef = null;

            if (context.InputParameters.Contains("Target"))
            {
                if (context.InputParameters["Target"] is Entity)
                {
                    entity = (Entity)context.InputParameters["Target"];
                }
                else if (context.InputParameters["Target"] is EntityReference)
                {
                    entityRef = (EntityReference)context.InputParameters["Target"];

                    ColumnSet attributes = new ColumnSet(true);
                    entity = service.Retrieve(entityRef.LogicalName, entityRef.Id, attributes);
                }
            }

            return entity;
        }
    }
}

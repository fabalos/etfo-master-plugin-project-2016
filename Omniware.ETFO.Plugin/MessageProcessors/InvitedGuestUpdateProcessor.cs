﻿using ETFOPlugin.Adx;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Metadata;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETFOPlugin.MessageProcessors
{
    public class InvitedGuestUpdateProcessor : InviteGuestMessageProcessor
    {
        public InvitedGuestUpdateProcessor(string config)
            : base(config)
        {
        }

        public override void process(IServiceProvider serviceProvider)
        {
            base.process(serviceProvider);

            // Verify that the target entity represents an invited guest.
            // If not, this plug-in was not registered correctly.
            if (entity.LogicalName != "oems_eventinvitedguest")
            {
                return;
            }

            try
            {
                //retrieve the pre and post update entity
                Entity preImage = (Entity)context.PreEntityImages["PreUpdateImage"];
                Entity postImage = (Entity)context.PostEntityImages["PostUpdateImage"];

                //if invitee is updated, assign the event page invited guest web role to new invitee

                //remove invited guest web role from old invitee
                removeInvitedGuestWebRole(preImage);

                //assign invited guest web role to new invitee
                assignInvitedGuestWebRole(postImage);
            }
            catch (Exception e)
            {
                tracingService.Trace("Exception: {0}", e.ToString());
                throw;
            }
        }
    }
}

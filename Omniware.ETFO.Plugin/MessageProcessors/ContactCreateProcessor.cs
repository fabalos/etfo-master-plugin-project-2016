﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Shared.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xrm;

namespace ETFOPlugin.MessageProcessors
{
    public class ContactCreateProcessor : ContactMessageProcessor
    {
        

        public ContactCreateProcessor(string config)
            : base(config)
        {
        }

        public override void process(IServiceProvider serviceProvider)
        {
            base.process(serviceProvider);

            // Verify that the target _entity represents a contact.
            // If not, this plug-in was not registered correctly.
            if (entity.LogicalName != "contact")
            {
                return;
            }

            try
            {
                /*
                    1) If Account.oems_MemberFlag = false and oems_NonMemberType = Staff >> assign adx_webrole = "Non-Member (Staff)"
                    2) If Account.oems_MemberFlag = false and oems_NonMemberType = Teacher >> assign adx_webrole = "Non-Member (Teacher)"
                    3) If Account.oems_MemberFlag = false and oems_NonMemberType = Non-Educator >> assign adx_webrole = "Non-Member (External)"
                    4) If Account.oems_MemberFlag = true >> assign adx_webrole = "Member"
                    5) If Account.oems_Role = President >> assign adx_webrole = "Local President"
                    6) If Account.oems_Role = President-Designate >> assign adx_webrole = "Local President-Designate"
                 */
                //if (parentAccountRf != null){
                //Entity parentAccount = service.Retrieve(parentAccountRf.LogicalName, parentAccountRf.Id, new ColumnSet(true));

                //bool is_non_member = entity.GetAttributeValue<bool>("oems_isnon_member");
                ////int nonmemberFlag = ((OptionSetValue)entity["oems_nonmembertype"]).Value; //1: Teacher, 2: Non-Educator, 3: Staff
                //int nonmemberFlag = entity.GetAttributeValue<int>("oems_nonmembertype");
                ////int role = ((OptionSetValue)entity["oems_role"]).Value;//347,780,000: President, 347,780,001: President-Deligate



                //if (is_non_member)
                //{
                //    switch(nonmemberFlag){
                //        case 1: //Teacher
                //            assignWebRoleToContact(entity.ToEntityReference(), "Non-Member (Teacher)");
                //            break;
                //        case 2: //Non-Educator
                //            assignWebRoleToContact(entity.ToEntityReference(), "Non-Member (External)");
                //            break;
                //        case 3: //Staff
                //            assignWebRoleToContact(entity.ToEntityReference(), "Non-Member (Staff)");
                //            break;

                //    }
                //}else
                //{
                //    assignWebRoleToContact(entity.ToEntityReference(), "Member");
                //}
                
                //OmniContext myContext = new OmniContext(base.service);

                //mbr_local local = myContext.mbr_localSet.Where(x => x.mbr_PrimaryContact.Id == entity.Id).SingleOrDefault<mbr_local>();

                //if (local != null)
                //{
                //    if (local.mbr_CurrentPresident != null)
                //    {
                //        if (local.mbr_CurrentPresident.Id == entity.Id)
                //        {
                //            assignWebRoleToContact(entity.ToEntityReference(), "Local President");
                //        }
                //    }
                //}

            }
            catch (Exception e)
            {
                tracingService.Trace("Exception: {0}", e.ToString());
                throw e;
            }
        }
    }
}

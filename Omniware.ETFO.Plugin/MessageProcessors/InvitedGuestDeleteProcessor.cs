﻿using ETFOPlugin.Adx;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Metadata;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETFOPlugin.MessageProcessors
{
    public class InvitedGuestDeleteProcessor : InviteGuestMessageProcessor
    {
        public InvitedGuestDeleteProcessor(string config)
            : base(config)
        {
        }

        public override void process(IServiceProvider serviceProvider)
        {
            base.process(serviceProvider);

            // Verify that the target entity represents an invited guest.
            // If not, this plug-in was not registered correctly.
            if (entity.LogicalName != "oems_eventinvitedguest")
            {
                return;
            }

            try
            {
                //remove the event page invited guest web role from invitee account's primary contact
                entity = crmTools.getEntity(entity.LogicalName, "oems_eventinvitedguestid", entity.Id, new ColumnSet(new string[] { 
                                                                                                                         "oems_event",
                                                                                                                         "oems_invitee"
                                                                                                                                 }));

                removeInvitedGuestWebRole(entity);
            }
            catch (Exception e)
            {
                tracingService.Trace("Exception: {0}", e.ToString());
                throw;
            }
        }
    }
}

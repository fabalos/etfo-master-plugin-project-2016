﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Shared.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETFOPlugin.MessageProcessors
{
    public class AdxWebPageUpdateProcessor : MessageProcessorBase
    {
        public AdxWebPageUpdateProcessor(string config)
            : base(config)
        {
        }

        public override void process(IServiceProvider serviceProvider)
        {
            base.process(serviceProvider);

            // Verify that the target _entity represents a contact.
            // If not, this plug-in was not registered correctly.
            if (entity.LogicalName != "adx_webpage")
            {
                return;
            }

            try
            {
                //if any one of the attributes has been changed
                if (entity.Contains("adx_publishingstateid") || entity.Contains("adx_releasedate") || entity.Contains("adx_expirationdate"))
                {
                    //retrieve all the fields
                    Entity webPage = crmTools.getEntity("adx_webpage", "adx_webpageid", entity.Id, new ColumnSet(new string[] {
                                                                                                               "adx_publishingstateid",
                                                                                                               "adx_releasedate",
                                                                                                               "adx_expirationdate"
                                                                                                               }));

                    //get the adx_webpage reference
                     DataCollection<Entity> events = crmTools.getRelatedEntities(
                                                        new ColumnSet("adx_webpageid"),
                                                        entity.LogicalName,
                                                        entity.Id,
                                                        "oems_event",
                                                        new ConditionExpression("oems_webpage", ConditionOperator.Equal, webPage.ToEntityReference().Id),
                                                        "oems_webpage_to_event");
                     if (events != null && events.Count > 0 && events[0] != null)
                    {
                        Entity oemsEvent = (Entity) events[0];
       /*                 oemsEvent["oems_webpagepublishingstate"] = CrmTools.getEnitiyAttributeSafely(webPage,"adx_publishingstateid");
                        oemsEvent["oems_webpagereleasedate"] = CrmTools.getEnitiyAttributeSafely(webPage, "adx_releasedate"); 
                        oemsEvent["oems_webpageexpirationdate"] = CrmTools.getEnitiyAttributeSafely(webPage, "adx_expirationdate"); 

                        service.Update(oemsEvent);

*/

                        //only update when the webpage's publishing state when the data are different, or it will cause endless loops
                        bool bUpdate = false;

                        object obj1 = CrmTools.getEnitiyAttributeSafely(oemsEvent, "oems_webpagepublishingstate");
                        object obj2 = CrmTools.getEnitiyAttributeSafely(webPage, "adx_publishingstateid");

                        if (obj1 != null && obj2 != null)
                        {
                            EntityReference eventPublishingState = (EntityReference)obj1;
                            EntityReference webpagePublishingState = (EntityReference)obj2;

                            if (!eventPublishingState.Equals(webpagePublishingState))
                            {
                                bUpdate = true;
                                oemsEvent["oems_webpagepublishingstate"] = webpagePublishingState;
                            }
                        }
                        else if (obj1 != null || obj2 != null)
                        {
                            bUpdate = true;
                            oemsEvent["oems_webpagepublishingstate"] = obj2;
                        }

                        obj1 = CrmTools.getEnitiyAttributeSafely(oemsEvent, "oems_webpagereleasedate");
                        obj2 = CrmTools.getEnitiyAttributeSafely(webPage, "adx_releasedate");

                        if (obj1 != null && obj2 != null)
                        {
                            DateTime eventWebpageReleaseDate = (DateTime)obj1;
                            DateTime adxWebpageReleaseDate = (DateTime)obj2;

                            if (!eventWebpageReleaseDate.Equals(adxWebpageReleaseDate))
                            {
                                bUpdate = true;
                                oemsEvent["oems_webpagereleasedate"] = adxWebpageReleaseDate;
                            }
                        }
                        else if (obj1 != null || obj2 != null)
                        {
                            bUpdate = true;
                            oemsEvent["oems_webpagereleasedate"] = obj2;
                        }

                        obj1 = CrmTools.getEnitiyAttributeSafely(oemsEvent, "oems_webpageexpirationdate");
                        obj2 = CrmTools.getEnitiyAttributeSafely(webPage, "adx_expirationdate");

                        if (obj1 != null && obj2 != null)
                        {
                            DateTime eventWebpageExpirationDate = (DateTime)obj1;
                            DateTime adxWebpageExpirationDate = (DateTime)obj2;

                            if (!eventWebpageExpirationDate.Equals(adxWebpageExpirationDate))
                            {
                                bUpdate = true;
                                oemsEvent["oems_webpageexpirationdate"] = adxWebpageExpirationDate;
                            }
                        }
                        else if (obj1 != null || obj2 != null)
                        {
                            bUpdate = true;
                            webPage["oems_webpageexpirationdate"] = obj2;
                        }

                        if (bUpdate)
                        {
                            service.Update(oemsEvent);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                tracingService.Trace("Exception: {0}", e.ToString());
                throw;
            }
        }
    }
}

﻿using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETFOPlugin.MessageProcessors
{
    public class AccountCreateProcessor : AccountMessageProcessor
    {
        public AccountCreateProcessor(string config)
            : base(config)
        {
        }

        public override void process(IServiceProvider serviceProvider)
        {
            base.process(serviceProvider);

            // Verify that the target entity represents a contact.
            // If not, this plug-in was not registered correctly.
            if (entity.LogicalName != "account")
            {
                return;
            }

            try
            {
                
            }
            catch (Exception e)
            {
                tracingService.Trace("Exception: {0}", e.ToString());
                throw;
            }

        }
    }
}

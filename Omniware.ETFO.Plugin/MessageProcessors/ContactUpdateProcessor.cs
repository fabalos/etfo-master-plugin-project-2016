﻿using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xrm;

namespace ETFOPlugin.MessageProcessors
{
    public class ContactUpdateProcessor : ContactMessageProcessor
    {
        public ContactUpdateProcessor(string config)
            : base(config)
        {
        }

        public override void process(IServiceProvider serviceProvider)
        {
            base.process(serviceProvider);

            // Verify that the target _entity represents a contact.
            // If not, this plug-in was not registered correctly.
            if (entity.LogicalName != "contact")
            {
                return;
            }

            if (context.Depth > 1) 
            {
                return;
            }

            try
            {
                if (entity.Attributes.ContainsKey("adx_username"))
                {
                    //Verify
                    entity["emailaddress1"] = entity["adx_username"];
                    service.Update(entity);
                }

                bool is_non_member = entity.GetAttributeValue<bool>("oems_isnon_member");
                //int nonmemberFlag = ((OptionSetValue)entity["oems_nonmembertype"]).Value; //1: Teacher, 2: Non-Educator, 3: Staff
                OptionSetValue nonmembertype = entity.GetAttributeValue<OptionSetValue>("oems_nonmembertype");
                if (nonmembertype != null)
                {
                    int nonmemberFlag = nonmembertype.Value;
                    //int role = ((OptionSetValue)entity["oems_role"]).Value;//347,780,000: President, 347,780,001: President-Deligate



                    if (is_non_member)
                    {
                        switch (nonmemberFlag)
                        {
                            case 1: //Teacher
                                assignWebRoleToContact(entity.ToEntityReference(), "Non-Member (Teacher)");
                                break;
                            case 2: //Non-Educator
                                assignWebRoleToContact(entity.ToEntityReference(), "Non-Member (External)");
                                break;
                            case 3: //Staff
                                assignWebRoleToContact(entity.ToEntityReference(), "Non-Member (Staff)");
                                break;

                        }
                    }
                    else
                    {
                        assignWebRoleToContact(entity.ToEntityReference(), "Member");
                    }

                    OmniContext myContext = new OmniContext(base.service);

                    mbr_local local = myContext.mbr_localSet.Where(x => x.mbr_PrimaryContact.Id == entity.Id).SingleOrDefault<mbr_local>();

                    if (local != null)
                    {
                        if (local.mbr_CurrentPresident != null)
                        {
                            if (local.mbr_CurrentPresident.Id == entity.Id)
                            {
                                assignWebRoleToContact(entity.ToEntityReference(), "Local President");
                            }
                        }
                    }
                }

            }
            catch (Exception e)
            {
                tracingService.Trace("Exception: {0}", e.ToString());
                throw;
            }
        }

        
    }
}

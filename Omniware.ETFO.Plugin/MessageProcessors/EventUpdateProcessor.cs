﻿using ETFOPlugin.Adx;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Crm.Sdk.Messages;
using Shared.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETFOPlugin.MessageProcessors
{
    public class EventUpdateProcessor : EventMessageProcessor
    {
        public EventUpdateProcessor(string config)
            : base(config)
        {
        }

        public override void process(IServiceProvider serviceProvider)
        {
            base.process(serviceProvider);

            // Verify that the target _entity represents a contact.
            // If not, this plug-in was not registered correctly.
            if (entity.LogicalName != "oems_event" || context.Depth != 1)
            {
                return;
            }

            try
            {

                #region publishing related updating
                //if any webpage publishing related attributes has been changed
                if (entity.Contains("oems_webpagepublishingstate") || entity.Contains("oems_webpagereleasedate") || entity.Contains("oems_webpageexpirationdate"))
                {
                    //retrieve related fields
                    Entity oemsEvent = service.Retrieve(entity.LogicalName, entity.Id, new ColumnSet(new string[] {
                                                                                                               "oems_webpagepublishingstate",
                                                                                                               "oems_webpagereleasedate",
                                                                                                               "oems_webpageexpirationdate",
                                                                                                               "oems_webpage"     
                                                                                                               }));

                    //get the adx_webpage reference
                    EntityReference webPageRf = (EntityReference)CrmTools.getEnitiyAttributeSafely(oemsEvent, "oems_webpage");

                    if (webPageRf != null && oemsEvent != null)
                    {
                        //update the mapping attributes in related adx_webpage
                        Entity webPage = service.Retrieve(webPageRf.LogicalName, webPageRf.Id, new ColumnSet(new string[] {
                                                                                                               "adx_publishingstateid",
                                                                                                               "adx_releasedate",
                                                                                                               "adx_expirationdate"
                                                                                                               }));//new Entity() { LogicalName = webPageRf.LogicalName, Id = webPageRf.Id };

                        //only update when the webpage's publishing state when the data are different, or it will cause endless loops
                        bool bUpdate = false;

                        object obj1 = CrmTools.getEnitiyAttributeSafely(oemsEvent, "oems_webpagepublishingstate");
                        object obj2 = CrmTools.getEnitiyAttributeSafely(webPage, "adx_publishingstateid");

                        if (obj1 != null && obj2 != null)
                        {
                            EntityReference eventPublishingState = (EntityReference)obj1;
                            EntityReference webpagePublishingState = (EntityReference)obj2;

                            if (!eventPublishingState.Equals(webpagePublishingState))
                            {
                                bUpdate = true;
                                webPage["adx_publishingstateid"] = eventPublishingState;
                            }
                        }
                        else if (obj1 != null || obj2 != null)
                        {
                            bUpdate = true;
                            webPage["adx_publishingstateid"] = obj1;
                        }

                        obj1 = CrmTools.getEnitiyAttributeSafely(oemsEvent, "oems_webpagereleasedate");
                        obj2 = CrmTools.getEnitiyAttributeSafely(webPage, "adx_releasedate");

                        if (obj1 != null && obj2 != null)
                        {
                            DateTime eventWebpageReleaseDate = (DateTime)obj1;
                            DateTime adxWebpageReleaseDate = (DateTime)obj2;

                            if (!eventWebpageReleaseDate.Equals(adxWebpageReleaseDate))
                            {
                                bUpdate = true;
                                webPage["adx_releasedate"] = eventWebpageReleaseDate;
                            }
                        }
                        else if (obj1 != null || obj2 != null)
                        {
                            bUpdate = true;
                            webPage["adx_releasedate"] = obj1;
                        }

                        obj1 = CrmTools.getEnitiyAttributeSafely(oemsEvent, "oems_webpageexpirationdate");
                        obj2 = CrmTools.getEnitiyAttributeSafely(webPage, "adx_expirationdate");

                        if (obj1 != null && obj2 != null)
                        {
                            DateTime eventWebpageExpirationDate = (DateTime)obj1;
                            DateTime adxWebpageExpirationDate = (DateTime)obj2;

                            if (!eventWebpageExpirationDate.Equals(adxWebpageExpirationDate))
                            {
                                bUpdate = true;
                                webPage["adx_expirationdate"] = eventWebpageExpirationDate;
                            }
                        }
                        else if (obj1 != null || obj2 != null)
                        {
                            bUpdate = true;
                            webPage["adx_expirationdate"] = obj1;
                        }

                        if (bUpdate)
                        {
                            service.Update(webPage);
                        }
                    }
                }
                #endregion publishing related updating

                //              string webPageAccessRuleName = "Restrict Read to event: " + ((Guid)entity["oems_eventid"]).ToString();
                string websiteName = adxSettings.getSetting("EventWebsiteName"); // ETFO Portals

                #region the event access flags are updated
                //if any of the access flags are updated
                if (entity.Contains("oems_invitedguestcomponentflag") || entity.Contains("oems_eventaccesspublicflag") || entity.Contains("oems_eventaccessmemberflag") ||
                    entity.Contains("oems_eventaccesspresidentflag") || entity.Contains("oems_eventaccessnonmemberstaffflag") ||
                    entity.Contains("oems_eventaccessnonmemberteacherflag") || entity.Contains("oems_eventaccessnonmemberexternalflag"))
                {
                    //retrieve all access flags
                    Entity oemsEvent = service.Retrieve(entity.LogicalName, entity.Id, new ColumnSet(new string[] {
                                                                                                               "oems_eventname",
                                                                                                               "oems_invitedguestcomponentflag",
                                                                                                               "oems_eventaccesspublicflag",
                                                                                                               "oems_eventaccessmemberflag",
                                                                                                               "oems_eventaccesspresidentflag",
                                                                                                               "oems_eventaccessnonmemberstaffflag",
                                                                                                               "oems_eventaccessnonmemberteacherflag",
                                                                                                               "oems_eventaccessnonmemberexternalflag",
                                                                                                               "oems_webpage"
                                                                                                               }));

                    bool bInvitedGuest = CrmTools.entityAttributeExists(oemsEvent, "oems_invitedguestcomponentflag") ? (bool)oemsEvent["oems_invitedguestcomponentflag"] : false;
                    bool publicFlag = CrmTools.entityAttributeExists(oemsEvent, "oems_eventaccesspublicflag") ? (bool)oemsEvent["oems_eventaccesspublicflag"] : false;
                    bool memberFlag = CrmTools.entityAttributeExists(oemsEvent, "oems_eventaccessmemberflag") ? (bool)oemsEvent["oems_eventaccessmemberflag"] : false;
                    bool presidentFlag = CrmTools.entityAttributeExists(oemsEvent, "oems_eventaccesspresidentflag") ? (bool)oemsEvent["oems_eventaccesspresidentflag"] : false;
                    bool nonmemberstaffFlag = CrmTools.entityAttributeExists(oemsEvent, "oems_eventaccessnonmemberstaffflag") ? (bool)oemsEvent["oems_eventaccessnonmemberstaffflag"] : false;
                    bool nonmemberteacherFlag = CrmTools.entityAttributeExists(oemsEvent, "oems_eventaccessnonmemberteacherflag") ? (bool)oemsEvent["oems_eventaccessnonmemberteacherflag"] : false;
                    bool nonmemberexternalFlag = CrmTools.entityAttributeExists(oemsEvent, "oems_eventaccessnonmemberexternalflag") ? (bool)oemsEvent["oems_eventaccessnonmemberexternalflag"] : false;

                    //retrieve restrict read webpage access rule
                    EntityReference webPageRf = (EntityReference)CrmTools.getEnitiyAttributeSafely(oemsEvent, "oems_webpage");
                    Entity readWebPageAccessControlRule = crmTools.getEntity("adx_webpageaccesscontrolrule", "adx_webpageid", webPageRf.Id);
                    EntityReference readWebPageAccessControlRuleRef;

                    if (readWebPageAccessControlRule == null)
                    {
                        readWebPageAccessControlRule = crmTools.createWebPageAccessRule(oemsEvent, websiteName);
                    }

                    readWebPageAccessControlRuleRef = readWebPageAccessControlRule.ToEntityReference();

                    //remove web access rule from all web roles
                    crmTools.removeWebPageAccessControlRuleFromWebRole(readWebPageAccessControlRuleRef, "Member");
                    crmTools.removeWebPageAccessControlRuleFromWebRole(readWebPageAccessControlRuleRef, "Local President");
                    crmTools.removeWebPageAccessControlRuleFromWebRole(readWebPageAccessControlRuleRef, "Local President-Designate");
                    crmTools.removeWebPageAccessControlRuleFromWebRole(readWebPageAccessControlRuleRef, "Non-Member (Staff)");
                    crmTools.removeWebPageAccessControlRuleFromWebRole(readWebPageAccessControlRuleRef, "Non-Member (Teacher)");
                    crmTools.removeWebPageAccessControlRuleFromWebRole(readWebPageAccessControlRuleRef, "Non-Member (External)");


                    //invited guest
                    string webRoleName = "Invited Guest for event: " + ((Guid)entity["oems_eventid"]).ToString();
                    Entity invitedGuestWebRole = crmTools.getEntity("adx_webrole", "adx_name", webRoleName);

                    // deactive invited guests if flag = false
                    if (!bInvitedGuest)
                    {
                        //remove the event invited guest web role
                        if (invitedGuestWebRole != null)
                        {
                            service.Delete(invitedGuestWebRole.LogicalName, invitedGuestWebRole.Id);
                        }

                        //deactive any event invitees that are on the event
                        DataCollection<Entity> invitedGuests = crmTools.getRelatedEntities(
                                            new ColumnSet(true),
                                            oemsEvent.LogicalName,
                                            oemsEvent.Id,
                                            "oems_eventinvitedguest",
                                            new ConditionExpression("statecode", ConditionOperator.Equal, 0), // active 
                                            "oems_event_to_eventinvitedguest");

                        if (invitedGuests != null)
                        {
                            foreach (Entity invitee in invitedGuests)
                            {
                                SetStateRequest jj_SetCustomEntituStatus = new SetStateRequest();
                                // Add entity refrence for which you want to set the status 
                                // In Entity refrence first paramter is Entity name and second paramter is ENtity Id                   
                                jj_SetCustomEntituStatus.EntityMoniker = new EntityReference("oems_eventinvitedguest", invitee.Id);
                                // Set State Value (Entity statecode/status code field value)
                                jj_SetCustomEntituStatus.State = new OptionSetValue(1); // deactive
                                // Set Status value (Entity statuscode/status reason filed value)
                                jj_SetCustomEntituStatus.Status = new OptionSetValue(2); // inactive
                                // Pass request to CRM Service execute method
                                service.Execute(jj_SetCustomEntituStatus);
                            }
                        }
                    }

                    //if public access is not enabled, add web access rule to web role accordingly
                    if (!publicFlag)
                    {
                        EntityReference invitedGuestWebRoleRef;

                        if (bInvitedGuest)
                        {
                            if (invitedGuestWebRole == null)
                            {
                                //if the invited guest web role is not find, create the web role and assign webpage acces role to it
                                //create the event invited guest web role
                                invitedGuestWebRole = crmTools.createWebRole(webRoleName, websiteName);
                            }

                            invitedGuestWebRoleRef = invitedGuestWebRole.ToEntityReference();

                            if (!crmTools.isWebPageAccessRuleAssignedToWebRole(readWebPageAccessControlRuleRef, invitedGuestWebRoleRef))
                            {
                                //assign webpage access rule to invited guest web role
                                crmTools.assignWebPageAccessControlRuleToWebRole(readWebPageAccessControlRuleRef, (string)invitedGuestWebRole["adx_name"]);
                            }
                        }

                        if (memberFlag)
                        {
                            // assgin page access to Member web role
                            crmTools.assignWebPageAccessControlRuleToWebRole(readWebPageAccessControlRuleRef, "Member");
                        }

                        if (presidentFlag)
                        {
                            // assgin page access to Local President and Local President-Designate web role
                            crmTools.assignWebPageAccessControlRuleToWebRole(readWebPageAccessControlRuleRef, "Local President");
                            crmTools.assignWebPageAccessControlRuleToWebRole(readWebPageAccessControlRuleRef, "Local President-Designate");
                        }

                        if (nonmemberstaffFlag)
                        {
                            // assgin page access to Non-Member (Staff) web role
                            crmTools.assignWebPageAccessControlRuleToWebRole(readWebPageAccessControlRuleRef, "Non-Member (Staff)");
                        }

                        if (nonmemberteacherFlag)
                        {
                            // assgin page access to Non-Member (Teacher) web role
                            crmTools.assignWebPageAccessControlRuleToWebRole(readWebPageAccessControlRuleRef, "Non-Member (Teacher)");
                        }

                        if (nonmemberexternalFlag)
                        {
                            // assgin page access to Non-Member (External) web role
                            crmTools.assignWebPageAccessControlRuleToWebRole(readWebPageAccessControlRuleRef, "Non-Member (External)");
                        }
                    }
                    else
                    {
                        //if it's public, remove the web page access rule
                        service.Delete(readWebPageAccessControlRule.LogicalName, readWebPageAccessControlRule.Id);
                    }
                }

                #endregion the event flags are updated

            }
            catch (Exception e)
            {
                tracingService.Trace("Exception: {0}", e.ToString());
                throw e;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETFOPlugin.MessageProcessors
{
    public class EventDeleteProcessor : EventMessageProcessor
    {
        public EventDeleteProcessor(string config)
            : base(config)
        {
        }

        public override void process(IServiceProvider serviceProvider)
        {
            base.process(serviceProvider);

            // Verify that the target _entity represents a contact.
            // If not, this plug-in was not registered correctly.
            if (entity.LogicalName != "oems_event")
            {
                return;
            }

            try
            {

            }
            catch (Exception e)
            {
                tracingService.Trace("Exception: {0}", e.ToString());
                throw e;
            }
        }
    }
}

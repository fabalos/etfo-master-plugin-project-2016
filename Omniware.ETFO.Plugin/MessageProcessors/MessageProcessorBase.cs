﻿using Shared.Utilities;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using ETFOPlugin.Adx;

namespace ETFOPlugin.MessageProcessors
{
    public class MessageProcessorBase : IMessageProcessor
    {
        //      protected string _config;
   //     private string _xrmConnString;

        protected IOrganizationService service;
        protected ITracingService tracingService;
        protected IPluginExecutionContext context;
        protected OrganizationServiceContext orgContext;
        protected Entity entity;
        protected EntityReference entityRef;
        protected CrmTools crmTools;
        protected Entity etfoSettings;
        protected AdxSettings adxSettings;

  /*      public string XrmConnString
        {
            get
            {
                return _xrmConnString;
            }
        }*/

        public MessageProcessorBase(string config)
        {
            //            _config = config;
            parseConfigXml(config);
        }

        public void parseConfigXml(string configXml)
        {
            if (!string.IsNullOrEmpty(configXml))
            {
                XmlDocument doc = new XmlDocument();

                doc.LoadXml(configXml);

/*                XmlNode node = doc.SelectSingleNode("/configurations/connectionStrings/XrmConnString");

                _xrmConnString = node.Attributes["connectionString"].Value;
 */
            }
            else
            {
//                _xrmConnString = "connect string";
            }
        }

        public virtual void process(IServiceProvider serviceProvider)
        {
            tracingService = (ITracingService)serviceProvider.GetService(typeof(ITracingService));

            // Obtain the execution context from the service provider.
            context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));

            IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            service = factory.CreateOrganizationService(context.UserId);

            orgContext = new OrganizationServiceContext(service);

            crmTools = new CrmTools(service);

            entity = ProcessorFactory.getEntityFromContext(context, service);

            etfoSettings = crmTools.getEntity("oems_configuration", "statuscode", 1);
            
            adxSettings = new AdxSettings(service);
        }

        protected void assignWebRoleToContact(EntityReference contact, string webRoleName)
        {
            EntityReference webRoleRf = crmTools.getEntity("adx_webrole", "adx_name", webRoleName).ToEntityReference();
            crmTools.associateRelatedEntityToEntity(webRoleRf, contact, "adx_webrole_contact");
        }

        protected void removeWebRoleFromContact(EntityReference contact, string webRoleName)
        {
            EntityReference webRoleRf = crmTools.getEntity("adx_webrole", "adx_name", webRoleName).ToEntityReference();
            crmTools.disAssociateRelatedEntityToEntity(webRoleRf, contact, "adx_webrole_contact");
        }

        protected void assignWebRolesToContact(Entity account, EntityReference loginContact)
        {
            // remove all web roles associated to the contact?

            DataCollection<Entity> webRoles = crmTools.getRelatedEntities(
                                                        new ColumnSet("contactid"),
                                                        loginContact.LogicalName,
                                                        loginContact.Id,
                                                        "adx_webrole",
                                                        new ConditionExpression("statuscode", ConditionOperator.Equal, 1),
                                                        "adx_webrole_contact");

            if (webRoles != null)
            {
                foreach (Entity webrole in webRoles)
                {
                    if ((bool)webrole["oems_accountrole"])
                        crmTools.disAssociateRelatedEntityToEntity(webrole.ToEntityReference(), loginContact, "adx_webrole_contact");
                }
            }

            // assign new web roles to contact
            bool memberFlag = (bool)account["oems_memberflag"];

            if (!memberFlag)
            {
                if (account.Contains("oems_nonmembertype") && account["oems_nonmembertype"] != null)
                {
                    int nonmemberFlag = ((OptionSetValue)account["oems_nonmembertype"]).Value; //1: Teacher, 2: Non-Teacher, 3: Staff

                    switch (nonmemberFlag)
                    {
                        case 1: //Teacher
                            assignWebRoleToContact(loginContact, "Non-Member (Teacher)");
                            break;
                        case 2: //Non-Teacher
                            assignWebRoleToContact(loginContact, "Non-Member (External)");
                            break;
                        case 3: //Staff
                            assignWebRoleToContact(loginContact, "Non-Member (Staff)");
                            break;
                    }
                }
            }
            else
            {
                assignWebRoleToContact(loginContact, "Member");
            }

            if (account.Contains("oems_role") && account["oems_role"] != null)
            {
                int role = ((OptionSetValue)account["oems_role"]).Value;//347,780,000: President, 347,780,001: President-Deligate

                switch (role)
                {
                    case 347780000://Local President
                        assignWebRoleToContact(loginContact, "Local President");
                        break;
                    case 347780001://President-Deligate
                        assignWebRoleToContact(loginContact, "Local President-Designate");
                        break;
                }
            }
        }
    }

    public class ConnectionResult
    {
        public Guid record2Id;
        public Guid? connectionId;
    }

    public enum SystemAccountStatus
    {
        Active = 100000000,
        Disabled = 100000001,
        INUSE = 100000002,
        Reserved = 100000003
    }
}

﻿using ETFOPlugin.Adx;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Metadata;
using Microsoft.Xrm.Sdk.Query;
using Shared.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETFOPlugin.MessageProcessors
{
    public class InviteGuestMessageProcessor : MessageProcessorBase
    {
        public InviteGuestMessageProcessor(string config)
            : base(config)
        {
        }

        public override void process(IServiceProvider serviceProvider)
        {
            base.process(serviceProvider);

            // Verify that the target entity represents an invited guest.
            // If not, this plug-in was not registered correctly.
            if (entity.LogicalName != "oems_eventinvitedguest")
            {
                return;
            }

            try
            {
                //remove the event page invited guest web role from invitee account's primary contact
            }
            catch (Exception e)
            {
                tracingService.Trace("Exception: {0}", e.ToString());
                throw;
            }
        }

        protected void assignInvitedGuestWebRole(Entity invitedGuest){
            try
            {
                //retrieve the required attributes
                entity = crmTools.getEntity(invitedGuest.LogicalName, "oems_eventinvitedguestid", invitedGuest.Id, new ColumnSet(new string[] { 
                                                                                                                         "oems_event",
                                                                                                                         "oems_invitee"
                                                                                                                                 }));

                //test if event attribute exists
                if (CrmTools.entityAttributeExists(entity, "oems_event"))
                {
                    //get the event page
                    Entity invitedGuestWebRole;
                    EntityReference eventRef = (EntityReference)entity["oems_event"];
                    Entity oemsEvent = service.Retrieve(eventRef.LogicalName, eventRef.Id, new ColumnSet(new string[] { "oems_webpage", "oems_eventaccesspublicflag" }));

                    bool publicFlag = CrmTools.entityAttributeExists(oemsEvent, "oems_eventaccesspublicflag") ? (bool)oemsEvent["oems_eventaccesspublicflag"] : false;

                    if (!publicFlag && CrmTools.entityAttributeExists(oemsEvent, "oems_webpage"))
                    {
                        string websiteName = adxSettings.getSetting("EventWebsiteName"); // ETFO Portals

                        //retrieve event webpage access rule
                        EntityReference webPageRf = (EntityReference)CrmTools.getEnitiyAttributeSafely(oemsEvent, "oems_webpage");
                        Entity readWebPageAccessControlRule = crmTools.getEntity("adx_webpageaccesscontrolrule", "adx_webpageid", webPageRf.Id);
                        EntityReference readWebPageAccessControlRuleRef;

                        if (readWebPageAccessControlRule == null)
                        {
                            readWebPageAccessControlRule = crmTools.createWebPageAccessRule(oemsEvent, websiteName);
                        }

                        readWebPageAccessControlRuleRef = readWebPageAccessControlRule.ToEntityReference();

                        //retrieve event invited guest web role
                        string webRoleName = "Invited Guest for event: " + ((Guid)oemsEvent["oems_eventid"]).ToString();
                        invitedGuestWebRole = crmTools.getEntity("adx_webrole", "adx_name", webRoleName);

                        if (invitedGuestWebRole == null)
                        {
                            //if the invited guest web role is not find, create the web role and assign webpage acces rule to it
                            //create the event invited guest web role
                            invitedGuestWebRole = crmTools.createWebRole(webRoleName, websiteName);
                            crmTools.assignWebPageAccessControlRuleToWebRole(readWebPageAccessControlRuleRef, (string)invitedGuestWebRole["adx_name"]);
                        }

                        //get invitee (account) primary contact
                        if (invitedGuestWebRole != null && CrmTools.entityAttributeExists(entity, "oems_invitee"))
                        {
                            EntityReference inviteeRef = (EntityReference)entity["oems_invitee"];
                            Entity account = service.Retrieve(inviteeRef.LogicalName, inviteeRef.Id, new ColumnSet(new string[] { "primarycontactid" }));

                            if (CrmTools.entityAttributeExists(account, "primarycontactid"))
                            {
                                //assign the event page invited guest web role to the contact
                                EntityReference primaryContactRef = (EntityReference)account["primarycontactid"];

                                assignWebRoleToContact(primaryContactRef, webRoleName);
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                tracingService.Trace("Exception: {0}", e.ToString());
                throw;
            }
        }

        protected void removeInvitedGuestWebRole(Entity invitedGuest){

            if (CrmTools.entityAttributeExists(invitedGuest, "oems_event"))
            {
                //get the event page 
                Entity invitedGuestWebRole;
                EntityReference eventRef = (EntityReference)invitedGuest["oems_event"];
                Entity oemsEvent = service.Retrieve(eventRef.LogicalName, eventRef.Id, new ColumnSet(new string[] { "oems_webpage" }));

                if (CrmTools.entityAttributeExists(oemsEvent, "oems_webpage"))
                {
                    //retrieve event invited guest web role
                    EntityReference webPageRf = (EntityReference)CrmTools.getEnitiyAttributeSafely(oemsEvent, "oems_webpage");
                    string webRoleName = "Invited Guest for event: " + ((Guid)oemsEvent["oems_eventid"]).ToString();
                    invitedGuestWebRole = crmTools.getEntity("adx_webrole", "adx_name", webRoleName);

                    //get invitee (account) primary contact
                    if (invitedGuestWebRole != null && CrmTools.entityAttributeExists(invitedGuest, "oems_invitee"))
                    {
                        EntityReference inviteeRef = (EntityReference)invitedGuest["oems_invitee"];
                        Entity account = service.Retrieve(inviteeRef.LogicalName, inviteeRef.Id, new ColumnSet(new string[] { "primarycontactid" }));

                        if (CrmTools.entityAttributeExists(account, "primarycontactid"))
                        {
                            //assign the event page invited guest web role to the contact
                            EntityReference primaryContactRef = (EntityReference)account["primarycontactid"];

                            removeWebRoleFromContact(primaryContactRef, webRoleName);
                        }
                    }
                }
            }
        }
    }
}

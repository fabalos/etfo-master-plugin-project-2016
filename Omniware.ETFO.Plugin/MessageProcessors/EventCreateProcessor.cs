﻿using ETFOPlugin.Adx;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Metadata;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETFOPlugin.MessageProcessors
{
    public class EventCreateProcessor : EventMessageProcessor
    {


        public EventCreateProcessor(string config)
            : base(config)
        {
        }

        public override void process(IServiceProvider serviceProvider)
        {
            base.process(serviceProvider);

            // Verify that the target _entity represents a contact.
            // If not, this plug-in was not registered correctly.
            if (entity.LogicalName != "oems_event")
            {
                return;
            }

            //try
            //{
            // create an adx_webpage
            Entity eventWebPage = createEventWebPage(entity);

            entity["oems_eventurl"] = GetEventUrl(eventWebPage);

            // link adx_webpage to event 
            entity["oems_webpage"] = eventWebPage.ToEntityReference();

            // set the publishing status of event to "Draft"
            if (etfoSettings != null)
            {
                entity["oems_webpagepublishingstate"] = etfoSettings["oems_webpagedraftstate"];
            }
            else
            {
                entity["oems_webpagepublishingstate"] = crmTools.getEntity("adx_publishingstate", "adx_name", "Draft").ToEntityReference();
            }

            service.Update(entity);


            // create the grant read web control role for the adx_webpage and assign 
            // it to the proper adx web roles if oems_eventaccesspublicflag is not true
            bool publicFlag = entity.Contains("oems_eventaccesspublicflag") && entity["oems_eventaccesspublicflag"] != null ? (bool)entity["oems_eventaccesspublicflag"] : false;
            bool memberFlag = entity.Contains("oems_eventaccessmemberflag") && entity["oems_eventaccessmemberflag"] != null ? (bool)entity["oems_eventaccessmemberflag"] : false;
            bool presidentFlag = entity.Contains("oems_eventaccesspresidentflag") && entity["oems_eventaccesspresidentflag"] != null ? (bool)entity["oems_eventaccesspresidentflag"] : false;
            bool nonmemberstaffFlag = entity.Contains("oems_eventaccessnonmemberstaffflag") && entity["oems_eventaccessnonmemberstaffflag"] != null ? (bool)entity["oems_eventaccessnonmemberstaffflag"] : false;
            bool nonmemberteacherFlag = entity.Contains("oems_eventaccessnonmemberteacherflag") && entity["oems_eventaccessnonmemberteacherflag"] != null ? (bool)entity["oems_eventaccessnonmemberteacherflag"] : false;
            bool nonmemberexternalFlag = entity.Contains("oems_eventaccessnonmemberexternalflag") && entity["oems_eventaccessnonmemberexternalflag"] != null ? (bool)entity["oems_eventaccessnonmemberexternalflag"] : false;

            bool invitedguestcomponentFlag = entity.Contains("oems_invitedguestcomponentflag") && entity["oems_invitedguestcomponentflag"] != null ? (bool)entity["oems_invitedguestcomponentflag"] : false;

            if (!publicFlag)
            {

                //create restrict read webpage access control rule on the event webpage
                string websiteName = adxSettings.getSetting("EventWebsiteName"); // ETFO Portals

                //retrieve the rule in case the update created the rule
                Entity readWebPageAccessControlRule = crmTools.getEntity("adx_webpageaccesscontrolrule", "adx_webpageid", ((EntityReference)entity["oems_webpage"]).Id);
                EntityReference readWebPageAccessControlRuleRef;

                if (readWebPageAccessControlRule == null)
                {
                    readWebPageAccessControlRule = crmTools.createWebPageAccessRule(entity, websiteName);
                }

                readWebPageAccessControlRuleRef = readWebPageAccessControlRule.ToEntityReference();

                if (invitedguestcomponentFlag)
                {
                    string webRoleName = "Invited Guest for event: " + ((Guid)entity["oems_eventid"]).ToString();
                    Entity invitedGuestWebRole = crmTools.getEntity("adx_webrole", "adx_name", webRoleName);
                    EntityReference invitedGuestWebRoleRef;
                    if (invitedGuestWebRole == null)
                    {
                        //create the event invited guest web role
                        invitedGuestWebRole = crmTools.createWebRole(webRoleName, websiteName);
                    }

                    invitedGuestWebRoleRef = invitedGuestWebRole.ToEntityReference();

                    if (!crmTools.isWebPageAccessRuleAssignedToWebRole(readWebPageAccessControlRuleRef, invitedGuestWebRoleRef))
                    {
                        //assign webpage access role to invited guest web role
                        crmTools.assignWebPageAccessControlRuleToWebRole(readWebPageAccessControlRuleRef, (string)invitedGuestWebRole["adx_name"]);
                    }
                }

                if (memberFlag)
                {
                    // assgin page access to Member web role
                    //crmTools.assignWebPageAccessControlRuleToWebRole(readWebPageAccessControlRuleRef, "Member");
                    assignWebPageAccessControlRuleToWebRole(readWebPageAccessControlRuleRef, "Member");
                }

                if (presidentFlag)
                {
                    // assgin page access to Local President and Local President-Designate web role
                    //crmTools.assignWebPageAccessControlRuleToWebRole(readWebPageAccessControlRuleRef, "Local President");
                    //crmTools.assignWebPageAccessControlRuleToWebRole(readWebPageAccessControlRuleRef, "Local President-Designate");
                    assignWebPageAccessControlRuleToWebRole(readWebPageAccessControlRuleRef, "Local President");
                    assignWebPageAccessControlRuleToWebRole(readWebPageAccessControlRuleRef, "Local President-Designate");
                }

                if (nonmemberstaffFlag)
                {
                    // assgin page access to Non-Member (Staff) web role
                    //crmTools.assignWebPageAccessControlRuleToWebRole(readWebPageAccessControlRuleRef, "Non-Member (Staff)");
                    assignWebPageAccessControlRuleToWebRole(readWebPageAccessControlRuleRef, "Non-Member (Staff)");
                }

                if (nonmemberteacherFlag)
                {
                    // assgin page access to Non-Member (Teacher) web role
                    //crmTools.assignWebPageAccessControlRuleToWebRole(readWebPageAccessControlRuleRef, "Non-Member (Teacher)");
                    assignWebPageAccessControlRuleToWebRole(readWebPageAccessControlRuleRef, "Non-Member (Teacher)");
                }


                if (nonmemberexternalFlag)
                {
                    // assgin page access to Non-Member (External) web role
                    //crmTools.assignWebPageAccessControlRuleToWebRole(readWebPageAccessControlRuleRef, "Non-Member (External)");
                    assignWebPageAccessControlRuleToWebRole(readWebPageAccessControlRuleRef, "Non-Member (External)");
                }
            }


            //}
            //catch (Exception e)
            //{
            //    tracingService.Trace("Exception: {0}", e.ToString());
            //    throw e;
            //}

        }

        /*   private void assignWebPageAccessControlRuleToWebRole(EntityReference webpageAccessControlRole, string webRoleName){
               EntityReference webRoleRf = crmTools.getEntity("adx_webrole", "adx_name", webRoleName).ToEntityReference();
               crmTools.associateRelatedEntityToEntity(webpageAccessControlRole, webRoleRf, "adx_webpageaccesscontrolrule_webrole"); 
           }*/

        private string GetEventUrl(Entity webPage)
        { 
            StringBuilder str = new StringBuilder();
            str.Insert(0, "/" + webPage.GetAttributeValue<string>("adx_partialurl"));

            Entity parent = service.Retrieve(webPage.LogicalName, webPage.GetAttributeValue<EntityReference>("adx_parentpageid").Id
                                                , new ColumnSet("adx_parentpageid", "adx_partialurl"));

            while (parent.GetAttributeValue<EntityReference>("adx_parentpageid") != null)
            {
                str.Insert(0, "/" + parent.GetAttributeValue<string>("adx_partialurl"));
                parent = service.Retrieve(webPage.LogicalName, parent.GetAttributeValue<EntityReference>("adx_parentpageid").Id
                                                , new ColumnSet("adx_parentpageid", "adx_partialurl"));
            }

            str.Insert(0,GetPortalUrl());

            return str.ToString();
        }

        private string GetPortalUrl()
        {
            var config = crmTools.getEntity("oems_configuration","oems_name","DO NOT DELETE",new ColumnSet("oems_portalurl"));

            string portalUrl = config.GetAttributeValue<string>("oems_portalurl");
            portalUrl = portalUrl.Split(new string[] { "/api" }, StringSplitOptions.None)[0];
            return portalUrl;
        }

        private void assignWebPageAccessControlRuleToWebRole(EntityReference readWebPageAccessControlRuleRef, string webRolName)
        {
            Entity webRole = crmTools.getEntity("adx_webrole", "adx_name", webRolName);
            if (webRole != null && !crmTools.isWebPageAccessRuleAssignedToWebRole(readWebPageAccessControlRuleRef, webRole.ToEntityReference()))
            {
                crmTools.assignWebPageAccessControlRuleToWebRole(readWebPageAccessControlRuleRef, webRolName);
            }
        }

        private Entity createEventWebPage(Entity oemsEvent)
        {
            #region Initialize

            //required fields
            string eventName = (string)oemsEvent["oems_eventname"];
            EntityReference subcategoryRef = (EntityReference)oemsEvent["oems_subcategory"];
            Entity subcategory = crmTools.getEntity("oems_subcategory", "oems_subcategoryid", subcategoryRef.Id.ToString());

            string websiteName = adxSettings.getSetting("EventWebsiteName"); // ETFO Portals
            EntityReference website = crmTools.getEntity("adx_website", "adx_name", websiteName).ToEntityReference();

            EntityReference parentPageRef = (EntityReference)subcategory["oems_webpage"];

            string partialUrl = oemsEvent.Id.ToString();

            EntityReference pageTemplate = new EntityReference();

            if (oemsEvent.GetAttributeValue<bool>("oems_coursetermflag") == true)
            {
                pageTemplate = (EntityReference)crmTools.getEntity("adx_pagetemplate", "adx_name", "Course Term Page").ToEntityReference();
            }
            else
            {
                string pageTemplateName = adxSettings.getSetting("EventPageTemplateName"); // Page
                pageTemplate = (EntityReference)crmTools.getEntity("adx_pagetemplate", "adx_name", pageTemplateName).ToEntityReference();
            }

            //string publishingStateName = adxSettings.getSetting("EventPublishingStateName"); // Published
            //EntityReference publishingState = crmTools.getEntity("adx_publishingstate", "adx_name", publishingStateName).ToEntityReference();
            EntityReference publishingState = crmTools.getEntity("adx_publishingstate", "adx_name", "Draft").ToEntityReference();

            //optional fields
            Entity eventSummaryFormat = crmTools.getEntity("adx_setting", "adx_name", "EventSummaryFormat");

            string summaryFormat = "{0} {1}";
            if (eventSummaryFormat != null)
            {
                summaryFormat = (string)eventSummaryFormat["adx_value"];
            }

            /*            OptionMetadataCollection accommodationOptions = crmTools.getOptionsSet("oems_subcategory", "oems_accommodationcomponent");
                        int accommodationOption = subcategory.GetAttributeValue<OptionSetValue>("oems_accommodationcomponent").Value;
                        string accommodationValue = accommodationOptions[accommodationOption - 1].Label.UserLocalizedLabel.Label.ToString();
                        string accommodationLabel = "Registration Application";*/

            #endregion Initialize

            #region Create Web Page

            Entity eventWebPage = new Entity("adx_webpage");

            eventWebPage.Attributes["adx_name"] = "Web Page for " + eventName;
            eventWebPage.Attributes["adx_title"] = eventName;
            eventWebPage.Attributes["adx_websiteid"] = website;
            eventWebPage.Attributes["adx_parentpageid"] = parentPageRef;
            eventWebPage.Attributes["adx_pagetemplateid"] = pageTemplate;
            eventWebPage.Attributes["adx_publishingstateid"] = publishingState;
            eventWebPage.Attributes["adx_partialurl"] = partialUrl;
            eventWebPage.Attributes["oems_subcategoryid"] = subcategoryRef;
            if (oemsEvent.Contains("oems_webpagereleasedate"))
            {
                eventWebPage.Attributes["adx_releasedate"] = oemsEvent["oems_webpagereleasedate"];
            }
            if (oemsEvent.Contains("oems_webpageexpirationdate"))
            {
                eventWebPage.Attributes["adx_expirationdate"] = oemsEvent["oems_webpageexpirationdate"];
            }
            //            eventWebPage.Attributes["adx_copy"] = string.Format(summaryFormat, accommodationLabel, accommodationValue);
            eventWebPage.Attributes["adx_displayorder"] = 999;
            eventWebPage.Attributes["adx_hiddenfromsitemap"] = true;
            eventWebPage.Attributes["oems_eventid"] = oemsEvent.ToEntityReference();

            try
            {
                eventWebPage.Id = service.Create(eventWebPage);
            }
            catch (Exception e)
            {

                throw new InvalidPluginExecutionException(e.ToString());
            }

            //eventWebPage.Id = service.Create(eventWebPage);

            #endregion Create Web Page

            #region Touch Parent Page for cache invalidation

            Entity parentPage = new Entity(parentPageRef.LogicalName);
            parentPage.Id = parentPageRef.Id;
            //Entity parentPage = crmTools.getEntity("adx_webpage", "adx_webpageid", parentPageRef.Id.ToString());
            //parentPage.Id = parentPage.GetAttributeValue<Guid>("adx_webpageid");
            parentPage.Attributes["modifiedon"] = System.DateTime.Now;
            service.Update(parentPage);

            #endregion Touch Parent Page for cache invalidation

            return eventWebPage;
        }
    }
}

﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Shared.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETFOPlugin.MessageProcessors
{
    public class AccountUpdateProcessor : AccountMessageProcessor
    {
        public AccountUpdateProcessor(string config)
            : base(config)
        {
        }

        public override void process(IServiceProvider serviceProvider)
        {
            base.process(serviceProvider);

            // Verify that the target entity represents a contact.
            // If not, this plug-in was not registered correctly.
            if (entity.LogicalName != "account")
            {
                return;
            }

            try
            {
                //if any one of the flags has been changed
                if(entity.Contains("oems_memberflag") || entity.Contains("oems_nonmembertype") || entity.Contains("oems_role")){
                    //retrieve all the fields
                    Entity account = crmTools.getEntity("account", "accountid", entity.Id);

                    //there must be a portal login contact associated to account
                    EntityReference loginContact = (EntityReference)CrmTools.getEnitiyAttributeSafely(account, "primarycontactid");

                    if (loginContact != null)
                    {
                        //assgin new web roles according to 
                        assignWebRolesToContact(account, loginContact);
                    }
                    else
                    {
                        //get the login contact from server
                        loginContact = (EntityReference)CrmTools.getEnitiyAttributeSafely(account, "primarycontactid");

                        if (loginContact != null)
                        {
                            assignWebRolesToContact(account, loginContact);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                tracingService.Trace("Exception: {0}", e.ToString());
                throw;
            }
        }
    }
}

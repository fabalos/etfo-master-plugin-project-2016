﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETFOCreateLocalEvent
{
    public class LocalEvent : IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
            IOrganizationServiceFactory serviceFactory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            IOrganizationService service = serviceFactory.CreateOrganizationService(context.UserId);
            OrganizationServiceContext serviceContext = new OrganizationServiceContext(service);

            if (context.InputParameters.Contains("Target") && context.InputParameters["Target"] is Entity)
            {
                Entity oemsEvent = (Entity)context.InputParameters["Target"];
                Entity preImage = null;

                if (context.Depth == 1)
                {
                    if (context.PreEntityImages.Contains("EventPreImage") && context.PreEntityImages["EventPreImage"] is Entity)
                    {
                        preImage = context.PreEntityImages["EventPreImage"]; //on create no preimage
                    }

                    if (preImage == null || Convert.ToBoolean(oemsEvent["oems_localdelegationcomponentflag"]) != Convert.ToBoolean(preImage["oems_localdelegationcomponentflag"]))
                    {
                        if (Convert.ToBoolean(oemsEvent.Attributes["oems_localdelegationcomponentflag"]) == true)
                        {
                            // get list of event locals from copied event
                            if (oemsEvent.Contains("oems_copiedfromevent") && oemsEvent.Attributes["oems_copiedfromevent"] != null)
                                createLocalEvents(service, oemsEvent.Id, ((EntityReference)oemsEvent.Attributes["oems_copiedfromevent"]).Id);
                            else if (preImage != null && preImage.Contains("oems_copiedfromevent") && preImage.Attributes["oems_copiedfromevent"] != null)
                                createLocalEvents(service, oemsEvent.Id, ((EntityReference)preImage.Attributes["oems_copiedfromevent"]).Id);
                            else
                                createLocalEvents(service, oemsEvent.Id, null);
                        }
                        else
                        {
                            if (context.MessageName == "Update")
                            {
                                deleteRelatedLocalEvents(service, oemsEvent.Id);
                            }
                        }
                    }
                }
            }

        }

        private EntityCollection getRelatedLocalEvents(IOrganizationService service, Guid eventId)
        {
            try
            {
                QueryExpression query = new QueryExpression("oems_eventlocal");
                query.ColumnSet = new ColumnSet("oems_eventlocalid");
                query.Criteria.AddCondition(new ConditionExpression("oems_event", ConditionOperator.Equal, eventId));

                return service.RetrieveMultiple(query);
            }
            catch (Exception ex)
            {
                throw new InvalidPluginExecutionException(ex.Message, ex.InnerException);
            }
        }

        private EntityCollection getAllLocals(IOrganizationService service)
        {
            QueryExpression query = new QueryExpression("oems_local");
            query.ColumnSet = new ColumnSet("oems_localname");
            query.Criteria.AddCondition(new ConditionExpression("statecode", ConditionOperator.Equal, "Active"));

            try
            {
                return service.RetrieveMultiple(query);
            }
            catch (Exception ex)
            {
                throw new InvalidPluginExecutionException(ex.Message, ex.InnerException);
            }
        }

        public EntityCollection getCopyEventLocals(IOrganizationService service, Guid copyEventId)
        {
            QueryExpression query = new QueryExpression("oems_eventlocal");
            query.ColumnSet = new ColumnSet("oems_local", "oems_localdelegationlimit");
            query.Criteria.AddCondition(new ConditionExpression("oems_event", ConditionOperator.Equal, copyEventId));

            try
            {
                return service.RetrieveMultiple(query);
            }
            catch (Exception ex)
            {
                throw new InvalidPluginExecutionException(ex.Message, ex.InnerException);
            }
        }

        private void deleteRelatedLocalEvents(IOrganizationService service, Guid eventId)
        {
            try
            {
                EntityCollection collection = getRelatedLocalEvents(service, eventId);

                foreach (var entity in collection.Entities)
                {
                    service.Delete("oems_eventlocal", entity.Id);
                }

            }
            catch (Exception ex)
            {

                throw new InvalidPluginExecutionException(ex.Message, ex.InnerException);
            }
        }

        private void createLocalEvents(IOrganizationService service, Guid eventId, Guid? copyEventId)
        {
            EntityCollection locals = getAllLocals(service);
            EntityCollection copyEventLocals = new EntityCollection();

            if (copyEventId != null)
                copyEventLocals = getCopyEventLocals(service, copyEventId.Value);

            // merge with locals
            var collection = 
                from a in locals.Entities
                join b in copyEventLocals.Entities on a.Id equals ((EntityReference)b.Attributes["oems_local"]).Id into c
                from d in c.DefaultIfEmpty()
                select new
                {
                    local = a.ToEntityReference(),
                    localName = a.Attributes["oems_localname"],
                    delegationLimit = d != null && d.Contains("oems_localdelegationlimit") ? d.Attributes["oems_localdelegationlimit"] : null
                };

            
            Entity localEvent;
            try
            {
                foreach (var eventLocal in collection)
                {
                    localEvent = new Entity("oems_eventlocal");
                    localEvent.Attributes.Add("oems_local", eventLocal.local);
                    localEvent.Attributes.Add("oems_event", new EntityReference("oems_event", eventId));
                    localEvent.Attributes.Add("oems_eventlocalname", eventLocal.localName);
                    localEvent.Attributes.Add("oems_localdelegationlimit", eventLocal.delegationLimit);
                    service.Create(localEvent);
                }
            }
            catch (Exception ex)
            {
                throw new InvalidCastException(ex.Message, ex.InnerException);
            }

        }
    }


}

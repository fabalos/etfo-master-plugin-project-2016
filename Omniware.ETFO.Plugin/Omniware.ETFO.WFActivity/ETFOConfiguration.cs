﻿using System;
using System.Activities;
using System.Collections.ObjectModel;

using System.Linq;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Linq;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Workflow;

namespace Omniware.ETFO.WFActivity
{
    public sealed class ETFOConfiguration : CodeActivity
    {
        protected override void Execute(CodeActivityContext executionContext)
        {

            IWorkflowContext context = executionContext.GetExtension<IWorkflowContext>();
            IOrganizationServiceFactory serviceFactory = executionContext.GetExtension<IOrganizationServiceFactory>();
            IOrganizationService orgService = serviceFactory.CreateOrganizationService(context.InitiatingUserId);
            OrganizationServiceContext orgContext = new OrganizationServiceContext(orgService);
            var config = (from c in orgContext.CreateQuery("oems_configuration")
                          select c).FirstOrDefault();
            if (config == null) Result.Set(executionContext, null);
            else Result.Set(executionContext, config.ToEntityReference());
        }


        [Output("Result")]
        [ReferenceTarget("oems_configuration")]
        public OutArgument<EntityReference> Result { get; set; }
    }


}

﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Omniware.ETFO.Plugin.DataWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Omniware.ETFO.Plugin.FormComponents
{
    public class TargetGradesComponent : IFormElement
    {
        public bool LoadDynamicFormData(DataWork.DataAccess dataAccess, DataWork.FormElement element, Microsoft.Xrm.Sdk.Entity root, Guid groupRowId)
        {
            if (element.component == (int)FORM_COMPONENT.TargetGrades)
            {
                EntityReference oems_event = (EntityReference)root["oems_event"];

                //Obtain the elements

                EntityCollection ec = GetRelatedTargetGrades(dataAccess, oems_event);

                EntityReference currSelected = root.Contains("oems_targetgrade") ? (EntityReference)root["oems_targetgrade"] : null;

                CreateOptions(ec, element, currSelected);

                element.type = "selectbox";


                return true;
            }

            return false;
        }

        EntityCollection GetRelatedTargetGrades(DataWork.DataAccess dataAccess,EntityReference oems_event) 
        {
            QueryExpression query = new QueryExpression()
            {
                EntityName = "oems_targetgrade",
                ColumnSet = new ColumnSet("oems_name"),
                LinkEntities = 
                        {
                            new LinkEntity
                            {
                                LinkFromEntityName = "oems_targetgrade",
                                LinkFromAttributeName = "oems_targetgradeid",
                                LinkToEntityName = "oems_targetgrade_oems_event",
                                LinkToAttributeName = "oems_targetgradeid",
                                LinkCriteria = new FilterExpression
                                {
                                    FilterOperator = LogicalOperator.And,
                                    Conditions = 
                                    {
                                        new ConditionExpression
                                        {
                                            AttributeName = "oems_eventid",
                                            Operator = ConditionOperator.Equal,
                                            Values = { oems_event.Id }
                                        }
                                    }
                                }
                            }
                        }
            };

            EntityCollection ec = dataAccess.context.RetrieveMultiple(query);

            return ec;

        }

        public bool SaveElementData(DataWork.DataAccess dataAccess, DataWork.FormElement element, List<DataWork.NameValuePair> data, Microsoft.Xrm.Sdk.Entity root)
        {
            if (element.component == (int)FORM_COMPONENT.TargetGrades)
            {
                 EntityReference oems_event = (EntityReference)root["oems_event"];


                 EntityCollection ec = GetRelatedTargetGrades(dataAccess, oems_event);

                 List<DataWork.NameValuePair> lookforData = new List<DataWork.NameValuePair>();


                 foreach (var entity in ec.Entities) 
                 {
                    var el = data.Find(x => x.value.EndsWith(entity.Id.ToString()));

                    if (el != null) 
                    {
                        SaveTargetGrade(root.Id, root.LogicalName, dataAccess, Guid.Parse(el.value));
                    }
                 }


                return true;
            }

            return false;
        }

        void SaveTargetGrade(Entity root,DataAccess dataAccess,OptionValue opt) 
        {
            EntityReference eref = new EntityReference("oems_targetgrade", Guid.Parse(opt.value));

            root.Attributes["oems_targetgrade"] = eref;

            dataAccess.context.Update(root);
        }

        void SaveTargetGrade(Guid rootId, string rootLogicalName, DataAccess dataAccess, Guid id)
        {
            EntityReference eref = new EntityReference("oems_targetgrade", id);

            Entity registration = new Entity(rootLogicalName);
            registration.Id = rootId;
            registration.Attributes["oems_targetgrade"] = eref;

            dataAccess.context.Update(registration);
        }

        void CreateOptions(EntityCollection targetGrades, DataWork.FormElement element,EntityReference currentSelected) 
        {
            int amountOfEntities = targetGrades.Entities.Count;

            element.selectValues = new DataWork.OptionValue[amountOfEntities];

            for(int i= 0;i<amountOfEntities;i++)
            {

                DataWork.OptionValue opt = new DataWork.OptionValue();

                opt.label = targetGrades[i].Attributes["oems_name"].ToString();
                opt.idValue = targetGrades[i].Id.ToString();

                opt.value = targetGrades[i].Id.ToString();

                if (currentSelected != null && targetGrades[i].Id == currentSelected.Id)
                {

                    element.value = targetGrades[i].Id.ToString();
                }
               

                element.selectValues[i] = opt;
            }
           
        }
    }
}

﻿using EventTeamPlugin;
using Microsoft.Xrm.Sdk;
using Omniware.ETFO.Plugin.DataWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Omniware.ETFO.Plugin.FormComponents
{
    public class WorkshopSession : IFormElement
    {
        const string firstChoice = "oemsWorkshopSessionFirstChoice";
        const string secondChoice = "oemsWorkshopSessionSecondChoice";
        const string thirdChoice = "oemsWorkshopSessionThirdChoice";

        public bool LoadDynamicFormData(DataWork.DataAccess dataAccess, DataWork.FormElement element, Microsoft.Xrm.Sdk.Entity root, Guid groupRowId)
        {
            if (element.component == (int)FORM_COMPONENT.WorkshopSession)
            {
                element.type = "section";
                List<DataWork.FormElement> children = new List<DataWork.FormElement>();
                //Need to ask if the eventregistration contains workshop session requests related
                IQueryable<Entity> workshopSessionRequests = from e in dataAccess.context.CreateQuery("oems_eventworkshopsessionrequest")
                                                             where (Guid)e["oems_eventapplication"] == root.Id
                                                             select e;

                Entity workshopSessionRequest = null;

                //Obtain the associated event
                EntityReference oems_event = (EntityReference)root["oems_event"];

                //Obtain the elements
                var eventSessions = from e in dataAccess.context.CreateQuery("oems_eventsession")
                                    where (Guid)e["oems_event"] == oems_event.Id
                                    select e;


                foreach (Entity eventSession in eventSessions)
                {
                    workshopSessionRequest = FindWorkshopRequestSession(workshopSessionRequests, eventSession);

                    string eventSessionDates = getEventSessionDates(dataAccess, eventSession.Id);

                    DataWork.FormElement sessionLegend = new DataWork.FormElement();
                    sessionLegend.name = eventSession.Id.ToString();
                    sessionLegend.children = new FormElement[0];
                    sessionLegend.type = "legend";
                    sessionLegend.value = eventSession["oems_sessionname"].ToString() + eventSessionDates;

                    children.Add(sessionLegend);

                    //Create the component asking for the workshopsession
                    DataWork.FormElement firstChoiceElement = new FormElement();
                    firstChoiceElement.type = "selectbox";
                    firstChoiceElement.id = "FirstChoice";
                    firstChoiceElement.children = new FormElement[0];
                    firstChoiceElement.options = new ElementOptions();
                    firstChoiceElement.options.label = "First preference";
                    firstChoiceElement.name = eventSession.Id.ToString() + firstChoice;

                    DataWork.FormElement secondChoiceElement = new FormElement();
                    secondChoiceElement.type = "selectbox";
                    secondChoiceElement.children = new FormElement[0];
                    secondChoiceElement.id = "SecondChoice";
                    secondChoiceElement.options = new ElementOptions();
                    secondChoiceElement.options.label = "Second preference";
                    secondChoiceElement.name = eventSession.Id.ToString() + secondChoice;

                    DataWork.FormElement thirdChoiceElement = new FormElement();
                    thirdChoiceElement.type = "selectbox";
                    thirdChoiceElement.children = new FormElement[0];
                    thirdChoiceElement.id = "ThirdChoice";
                    thirdChoiceElement.options = new ElementOptions();
                    thirdChoiceElement.options.label = "Third preference";
                    thirdChoiceElement.name = eventSession.Id.ToString() + thirdChoice;


                    DataWork.FormElement status = null;
                    DataWork.FormElement notes = null;

                    if (root.Attributes.Contains("statuscode") &&
                        ((OptionSetValue)root.Attributes["statuscode"]).Value == EventRegistrationStatusReason.Approved)
                    {
                        if (workshopSessionRequest != null)
                        {
                            status = new FormElement();
                            status.type = "textinput";
                            status.name = workshopSessionRequest.Id.ToString();
                            status.options.label = "Status";
                            status.children = new FormElement[0];
                            status.value = workshopSessionRequest.Attributes.Contains("statuscode") ?
                                workshopSessionRequest.FormattedValues["statuscode"] : "";
                            status.options.disabled = true;

                            notes = new FormElement();
                            notes.type = "textinput";
                            notes.name = workshopSessionRequest.Id.ToString();
                            notes.options.label = "Assigned";
                            notes.children = new FormElement[0];
                            notes.value = workshopSessionRequest.Attributes.Contains("oems_workshopsessionassigned") ?
                                ((EntityReference)workshopSessionRequest.Attributes["oems_workshopsessionassigned"]).Name : "";
                            notes.options.disabled = true;
                        }
                    }

                    if (workshopSessionRequest == null)
                    {
                        LoadWorkshopOptions(firstChoiceElement, dataAccess, eventSession.Id, null);
                        LoadWorkshopOptions(secondChoiceElement, dataAccess, eventSession.Id, null);
                        LoadWorkshopOptions(thirdChoiceElement, dataAccess, eventSession.Id, null);
                    }
                    else
                    {
                        if (workshopSessionRequest.Attributes.Contains("oems_workshopsessionfirstchoice"))
                        {
                            LoadWorkshopOptions(firstChoiceElement, dataAccess, eventSession.Id, (EntityReference)workshopSessionRequest["oems_workshopsessionfirstchoice"]);
                        }
                        else
                        {
                            LoadWorkshopOptions(firstChoiceElement, dataAccess, eventSession.Id, null);
                        }

                        if (workshopSessionRequest.Attributes.Contains("oems_workshopsessionsecondchoice"))
                        {
                            LoadWorkshopOptions(secondChoiceElement, dataAccess, eventSession.Id, (EntityReference)workshopSessionRequest["oems_workshopsessionsecondchoice"]);
                        }
                        else
                        {
                            LoadWorkshopOptions(secondChoiceElement, dataAccess, eventSession.Id, null);
                        }

                        if (workshopSessionRequest.Attributes.Contains("oems_workshopsessionthirdchoice"))
                        {
                            LoadWorkshopOptions(thirdChoiceElement, dataAccess, eventSession.Id, (EntityReference)workshopSessionRequest["oems_workshopsessionthirdchoice"]);
                        }
                        else
                        {
                            LoadWorkshopOptions(thirdChoiceElement, dataAccess, eventSession.Id, null);
                        }

                    }

                    children.Add(firstChoiceElement);
                    children.Add(secondChoiceElement);
                    children.Add(thirdChoiceElement);

                    if (status != null) children.Add(status);
                    if (notes != null) children.Add(notes);
                }

                element.children = children.ToArray();

                return true;
            }
            else
                return false;

        }

        public bool SaveElementData(DataWork.DataAccess dataAccess, DataWork.FormElement element, List<DataWork.NameValuePair> data, Microsoft.Xrm.Sdk.Entity root)
        {
            if (element.component == (int)FORM_COMPONENT.WorkshopSession)
            {

                Entity workshopsessionRequest = null;

                var workshopSessionRequests = from e in dataAccess.context.CreateQuery("oems_eventworkshopsessionrequest")
                                              where (Guid)e["oems_eventapplication"] == root.Id
                                              select e;

                Entity oems_event = (from e in dataAccess.context.CreateQuery("oems_event")
                                             where (Guid)e["oems_eventid"] == ((EntityReference)root["oems_event"]).Id
                                             select e).FirstOrDefault();
                    

                //Obtain the elements
                var eventSessions = from e in dataAccess.context.CreateQuery("oems_eventsession")
                                    where (Guid)e["oems_event"] == oems_event.Id
                                    select e;

                foreach (Entity session in eventSessions)
                {
                    bool isNew = false;
                    workshopsessionRequest = FindWorkshopRequestSession(workshopSessionRequests, session);

                    var firstchoice = data.Find(x => x.name.EndsWith(session.Id.ToString() + firstChoice));
                    var secondchoice = data.Find(x => x.name.EndsWith(session.Id.ToString() + secondChoice));
                    var thirdchoice = data.Find(x => x.name.EndsWith(session.Id.ToString() + thirdChoice));

                    if (CanISave(firstchoice, secondchoice, thirdchoice))
                    {
                        Entity updatedWorkshopSessionRequest = new Entity("oems_eventworkshopsessionrequest");

                        if (workshopsessionRequest == null)
                        {
                            isNew = true;
                            workshopsessionRequest = new Entity("oems_eventworkshopsessionrequest");
                        }

                        if (firstchoice != null && firstchoice.value != "")
                        {
                            workshopsessionRequest["oems_workshopsessionfirstchoice"] = new EntityReference("oems_eventworkshopsession", Guid.Parse(firstchoice.value));
                        }
                        else
                        {
                            workshopsessionRequest["oems_workshopsessionfirstchoice"] = null;
                        }

                        if (secondchoice != null && secondchoice.value != "")
                        {
                            workshopsessionRequest["oems_workshopsessionsecondchoice"] = new EntityReference("oems_eventworkshopsession", Guid.Parse(secondchoice.value));
                        }
                        else
                        {
                            workshopsessionRequest["oems_workshopsessionsecondchoice"] = null;
                        }


                        if (thirdchoice != null && thirdchoice.value != "")
                        {
                            workshopsessionRequest["oems_workshopsessionthirdchoice"] = new EntityReference("oems_eventworkshopsession", Guid.Parse(thirdchoice.value));
                        }
                        else
                        {
                            workshopsessionRequest["oems_workshopsessionthirdchoice"] = null;
                        }
                        

                        if (isNew)
                        {
                            workshopsessionRequest["oems_event"] = oems_event.ToEntityReference();
                            workshopsessionRequest["oems_eventapplication"] = new EntityReference("oems_eventregistration", root.Id);
                            workshopsessionRequest["oems_eventsession"] = new EntityReference("oems_eventsession", session.Id);

                            try
                            {
                                Guid newWorkshopGuid = dataAccess.context.Create(workshopsessionRequest);

                                EntityReference team = EventHelper.GetRelatedTeam(dataAccess.context, oems_event.Id);

                                if (team != null)
                                {
                                    EventHelper.AssignEntityToTeam(team.Id, newWorkshopGuid, workshopsessionRequest.LogicalName, dataAccess.context);
                                }
                            }
                            catch (Exception e)
                            {
                                throw new InvalidPluginExecutionException(e.Message);
                            }
                            


                        }
                        else
                        {
                            updatedWorkshopSessionRequest.Id = workshopsessionRequest.Id;
                            updatedWorkshopSessionRequest["oems_workshopsessionfirstchoice"] = workshopsessionRequest["oems_workshopsessionfirstchoice"];
                            updatedWorkshopSessionRequest["oems_workshopsessionsecondchoice"] = workshopsessionRequest["oems_workshopsessionsecondchoice"];
                            updatedWorkshopSessionRequest["oems_workshopsessionthirdchoice"] = workshopsessionRequest["oems_workshopsessionthirdchoice"];

                            dataAccess.context.Update(updatedWorkshopSessionRequest);
                        }

                    }
                }

                return true;
            }
            else
                return false;
        }

        bool CanISave(DataWork.NameValuePair firschoice, DataWork.NameValuePair secondchoice, DataWork.NameValuePair thirdchoice)
        {
            if (!String.IsNullOrEmpty(firschoice.value) || !String.IsNullOrEmpty(secondchoice.value) || !String.IsNullOrEmpty(thirdchoice.value))
            {
                return true;
            }
            return false;
        }

        Entity FindWorkshopRequestSession(IQueryable<Entity> associatedRequests, Entity eventSession)
        {
            foreach (Entity workshopSessionRequest in associatedRequests)
            {
                if (((EntityReference)workshopSessionRequest.Attributes["oems_eventsession"]).Id == eventSession.Id)
                {
                    return workshopSessionRequest;
                }
            }
            return null;
        }

        void LoadWorkshopOptions(DataWork.FormElement element, DataWork.DataAccess dataAccess, Guid sessionId, EntityReference selectedOption)
        {

            var workshopsessions = from workshopsession in dataAccess.context.CreateQuery("oems_eventworkshopsession")
                                   where (Guid)workshopsession["oems_eventsession"] == sessionId
                                   select workshopsession;

            List<OptionValue> options = new List<OptionValue>();
            OptionValue val = new OptionValue();
            val.value = null;
            val.idValue = null;
            val.reference = null;
            val.selected = "true";

            options.Add(val);
            options.AddRange(dataAccess.GetOptions(workshopsessions, "oems_workshopsessionname", null, null, null));

            element.selectValues = (DataWork.OptionValue[])options.ToArray();

            if (selectedOption != null)
            {
                element.value = selectedOption.Id.ToString();
            }
        }

        string getEventSessionDates(DataWork.DataAccess dataAccess, Guid sessionId)
        {
            Entity eventSessionEntity = dataAccess.context.oems_EventSessionSet
                                      .Where(i => i.Id == sessionId).FirstOrDefault();
            //select session).FirstOrDefault();

            var startDateSession = eventSessionEntity.Attributes.Contains("oems_startdatetime") ?
                ((DateTime)eventSessionEntity.Attributes["oems_startdatetime"]).ToLocalTime().ToString("dddd, MMMM dd, yyyy hh:mm tt") : "";

            var endDateSession = eventSessionEntity.Attributes.Contains("oems_enddatetime") ?
                ((DateTime)eventSessionEntity.Attributes["oems_enddatetime"]).ToLocalTime().ToString("dddd, MMMM dd, yyyy hh:mm tt") : "";

            return (": " + startDateSession + " - " + endDateSession);
        }



    }
}

﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Omniware.ETFO.Plugin.Billing;
using Omniware.ETFO.Plugin.DataWork;
using Omniware.ETFO.Plugin.Shared;
using Xrm;
using log4net;
using EventTeamPlugin;

namespace Omniware.ETFO.Plugin.FormComponents
{

    // single pricing rule for event or room


//    internal static class EntityCommonSenseExtensions
//    {
//        // get value of attributeName converted to TargetType if attribute exists, default otherwise
//        public static TargetType Get<TargetType>(this Entity entity, string attributeName)
//        {
//            return entity.Contains(attributeName) ? (TargetType) entity[attributeName] : default(TargetType);
//        }
//    }


    // common logic for Room and Event Pricing element
    abstract class PricingFormElement : IFormElement
    {
        internal string pricingEntityName; // pricing entity name  
        internal string requestEntityName; // request entity name "oems_accommodationrequest" or "oems_billingrequest;

        internal string formElementId ;

        internal ILog logger = LogHelper.GetLogger(typeof(PricingFormElement));

        // build form element using the existing CRM data 
        public bool LoadDynamicFormData(DataAccess dataAccess, FormElement element, Entity eventRegistration, Guid groupRowId)
        {
            
            if (element.id != formElementId ) return false;

            var eventId = ((EntityReference)eventRegistration["oems_event"]).Id;

            var currentAccountId = eventRegistration.GetAttributeValue<EntityReference>("oems_account").Id;

            List<PricingRule> pricingRules = BillingUtil.GetPricingRules(dataAccess.context, eventId, pricingEntityName);

            //var theEvent = dataAccess.context.CreateQuery("oems_event").Single(ev => (Guid)ev["oems_eventid"] == eventId);
            // todo use plugin context cached values of account and event .. it takes 20 mssec for each time (twice)
            var theEvent = dataAccess.context.oems_EventSet.Single(ev => (Guid)ev["oems_eventid"] == eventId);
            var account = dataAccess.context.AccountSet.Single(a => a.Id == currentAccountId);
            IEnumerable<PricingRule> allowedPricingRules = pricingRules.Where(pr => pr.AppliesToAccount(account, theEvent));
            if (allowedPricingRules.Count() == 0)
            {
                // if not rules match user type default to All Other rules
                allowedPricingRules = pricingRules.Where(pr => pr.ParticipantType.Value == BillingParticipantType.ALL_OTHER);
            }
            if (allowedPricingRules.Count() == 0)
            {
                logger.Warn("Thare are no matching pricing rules of type "+ pricingEntityName+  " define pricing for All Other ");
            }

            element.type = "listofradios";
            element.name = pricingEntityName;
            element.value = CurrentValue(dataAccess,eventRegistration.Id);
            element.radioValues = allowedPricingRules.Select(
                pr => new OptionValue
                {
                    label = String.Format("{0:C2}", pr.Price) + " - " + pr.Name,
                    value = pr.Id.ToString()
                }
                ).ToArray();

            return true;
        }


        // save the form element into CRM Registration entity
        public bool SaveElementData(DataAccess dataAccess, FormElement element, List<NameValuePair> data, Entity eventRegistration)
        {
            if (element.id != formElementId) return false;
            bool isBillingFlag = false;

            var nameValuePair = data.FirstOrDefault(nv => nv.name.EndsWith(pricingEntityName));
            if (nameValuePair != null && !string.IsNullOrEmpty(nameValuePair.value))
            {
                var request = BillingUtil.GetRequest(dataAccess.context, eventRegistration.Id, requestEntityName);
                if (request == null)
                {
                    request = new Entity(requestEntityName);
                    request["oems_event"] = eventRegistration["oems_event"];
                    request["oems_eventregistration"] = eventRegistration.ToEntityReference();
                    if (requestEntityName == "oems_billingrequest") isBillingFlag = true;
                }

                request.Attributes.Remove("oems_singleroommedicalaccommodationrequest");
                request[pricingEntityName] = new EntityReference(pricingEntityName, new Guid(nameValuePair.value));
                dataAccess.context.CreateOrUpdate(request);
                dataAccess.context.SaveChanges(); // todo refactor to do save chanes only once performance
                if (isBillingFlag) ReassignBillingRequest(dataAccess.context, request.Id, request.LogicalName, ((EntityReference)eventRegistration["oems_event"]).Id);
            }
            return true;
        }

        void ReassignBillingRequest(OmniContext context, Guid requestGuid, string requestEntityName, Guid eventGuid)
        {
            try
            {
                EntityReference team = EventHelper.GetRelatedTeam(context, eventGuid);

                if (team != null)
                {
                    EventHelper.AssignEntityToTeam(team.Id, requestGuid, requestEntityName, context);
                }
            }
            catch (Exception e)
            {
                throw new InvalidPluginExecutionException(e.Message);
            }
        }

        string CurrentValue(DataAccess dataAccess, Guid registrationId)
        {
            // find billing or accommodation request
            var request = BillingUtil.GetRequest(dataAccess.context, registrationId, requestEntityName);
            if (request == null || !request.Contains(pricingEntityName)) return null;

            // name of the field on request matches entity name
            return request.GetAttributeValue<EntityReference>(pricingEntityName).Id.ToString();
        }




    }

    // pricing for different room types
    internal class RoomPricingFormElement : PricingFormElement
    {
        public const string ROOM_PRICE = "Room Price";

        public RoomPricingFormElement()
        {
            formElementId = ROOM_PRICE;
            pricingEntityName = "oems_accommodationpricing";
            requestEntityName = "oems_accommodationrequest";
        }



    }

    // pricing for different Event booking options
    internal class EventPricingFormElement : PricingFormElement
    {
        public const string EVENT_PRICE = "Event Price";

        public EventPricingFormElement()
        {
            formElementId = EVENT_PRICE;
            pricingEntityName = "oems_eventpricing";
            requestEntityName = "oems_billingrequest";
        }


    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Metadata;
using Omniware.ETFO.Plugin;
using Xrm;
using Omniware.ETFO.Plugin.DataWork;


namespace Omniware.ETFO.Plugin.FormComponents
{
    public enum ReleaseTimePeriod
    {
        NONE = 0,
        FULLDAY = 1,
        HALFDAY_AM = 2,
        HALFDAY_PM = 3
    }

    public class ReleaseTime: IFormElement
    {
        const string fulltimeTeacher = "LtoFullMember";
        const string dailyOccasionalTeacher = "DailyOccasionalTeacher";
        const string ltoNonLto = "ReleasetimeLtoNonLto";
        const string partTimeTeacher = "PartTimeTeacher";
        const string releaseAdditionalInformation = "ReleaseAdditionalInformation";

//        entertaining but unnecessary
//        public bool LoadComponent(LocalPluginContext context, Entity e, DataWork.FormElement element)
//        {
//            bool result = false;
//            return result;
//        }
        
        public Entity GetExistingReleaseTimeRequest(DataWork.DataAccess dataAccess, Microsoft.Xrm.Sdk.Entity root)
        {
            Microsoft.Xrm.Sdk.Query.QueryByAttribute qba = new Microsoft.Xrm.Sdk.Query.QueryByAttribute("oems_releasetimerequest");
            qba.AddAttributeValue("oems_eventregistration", root.Id);
            qba.AddAttributeValue("statecode", 0);
            qba.ColumnSet = new Microsoft.Xrm.Sdk.Query.ColumnSet(true);

            EntityCollection ec = dataAccess.context.RetrieveMultiple(qba);
            if(ec.Entities.Count > 0) return ec[0];
            else return null;
            
        }
        
        public bool LoadDynamicFormData(DataWork.DataAccess dataAccess, DataWork.FormElement element, Microsoft.Xrm.Sdk.Entity root, Guid groupRowId)
        {
            if (element.component == (int)FORM_COMPONENT.ReleaseTime)
            {
                Entity request = GetExistingReleaseTimeRequest(dataAccess, root);

                element.name = "releasetime_"+element.name;
                DataWork.FormElement schoolBoard = null;
                DataWork.FormElement school = null;
                DataWork.FormElement local = null;
                DataWork.FormElement schoolName = null;
                DataWork.FormElement schoolFax = null;
                DataWork.FormElement schoolEmail = null;
                DataWork.FormElement schoolCity = null;
                DataWork.FormElement schoolPhone = null;
                DataWork.FormElement regularReleaseTimeSection = null;
                DataWork.FormElement additionalReleaseTimeSection = null;
                DataWork.FormElement schoolPrincipal = null;
                DataWork.FormElement status = null;
                DataWork.FormElement notes = null;
                DataWork.FormElement field_fullTimeTeacher = null;
                DataWork.FormElement field_dailyOccasionalTeacher = null;
                DataWork.FormElement field_LTONonLTO = null;
                DataWork.FormElement field_partTimeTeacher = null;
                DataWork.FormElement field_additionalInformation = null;


                List<DataWork.FormElement> children = new List<DataWork.FormElement>(element.children);
                
                foreach (var c in children)
                {
                    if (c.id == "SchoolBoard") schoolBoard = c;
                    else if (c.id == "School") school = c;
                    else if (c.id == "Local") local = c;
                    else if (c.id == "SchoolCity") schoolCity = c;
                    else if (c.id == "SchoolPhone") schoolPhone = c;
                    else if (c.id == "SchoolFax") schoolFax = c;
                    else if (c.id == "SchoolEmail") schoolEmail = c;
                    else if (c.id == "SchoolName") schoolName = c;
                    else if (c.id == "RegularReleaseTimeSection") regularReleaseTimeSection = c;
                    else if (c.id == "AdditionalReleaseTimeSection") additionalReleaseTimeSection = c;
                    else if (c.id == "SchoolPrincipal") schoolPrincipal = c;
                    else if (c.id == "Status") status = c;
                    else if (c.id == "Notes") notes = c;
                    else if (c.id == fulltimeTeacher) field_fullTimeTeacher = c;
                    else if (c.id == dailyOccasionalTeacher) field_dailyOccasionalTeacher = c;
                    else if (c.id == ltoNonLto) field_LTONonLTO = c;
                    else if (c.id == partTimeTeacher) field_partTimeTeacher = c;
                    else if (c.id == releaseAdditionalInformation) field_additionalInformation = c;
                }

                if (schoolEmail != null)
                {
                    schoolEmail.name = "previewRtSchoolEmail";
                    schoolEmail.type = "previewRtSchoolEmail";
                    if (request != null && request.Contains("oems_schoolemail") && request["oems_schoolemail"] != null)
                    {
                        schoolEmail.value = (string)request["oems_schoolemail"];
                    }
                }
                if (schoolFax != null)
                {
                    /*schoolFax.name = "previewRtSchoolFax";
                    schoolFax.type = "previewRtSchoolFax";*/

                    schoolFax.name = "previewRtSchoolFax";
                    schoolFax.type = "previewRtSchoolFax";
                    if (request != null && request.Contains("oems_schoolfax") && request["oems_schoolfax"] != null)
                    {
                        schoolFax.value = (string)request["oems_schoolfax"];
                    }
                }
                if (schoolName != null)
                {
                    schoolName.name = "previewRtSchoolName";
                    schoolName.type = "previewRtSchoolName";
                }
                if (schoolPhone != null)
                {
                    schoolPhone.name = "previewRtSchoolPhone";
                    schoolPhone.type = "previewRtSchoolPhone";
                }
                if (schoolCity != null)
                {
                    schoolCity.name = "previewRtSchoolCity";
                    schoolCity.type = "previewRtSchoolCity";
                }
                if (schoolPrincipal != null)
                {
                    schoolPrincipal.name = "schoolprincipal";
                    if (request != null && request.Contains("oems_schoolprincipalname") && request["oems_schoolprincipalname"] != null)
                    {
                        schoolPrincipal.value = (string)request["oems_schoolprincipalname"];
                    }
                }

                if (school != null)
                {
                    school.name = "rtschool";
                    school.type = "rtschool";
                    if (request != null && request.Contains("oems_school") && request["oems_school"] != null)
                    {
                        school.value = DataWork.FormElement.GuidToString(((EntityReference)request["oems_school"]).Id);
                    }
                }

                if (local != null)
                {
                    local.name = "rtlocal";
                    local.type = "rtlocal";

                    if (request != null && request.Contains("oems_local") && request["oems_local"] != null)
                    {
                        local.value = DataWork.FormElement.GuidToString(((EntityReference)request["oems_local"]).Id);
                    }
                    
                }

                if (schoolBoard != null)
                {
                    schoolBoard.name = "rtschoolboard";
                    schoolBoard.type = "rtschoolboard";

                    if (request != null && request.Contains("oems_schoolboard") && request["oems_schoolboard"] != null)
                    {
                        schoolBoard.value = DataWork.FormElement.GuidToString(((EntityReference)request["oems_schoolboard"]).Id);
                    }
                }

                if (status != null)
                {
                    schoolPrincipal.name = "status";
                    if (request != null && request.Contains("statuscode") && request["statuscode"] != null)
                    {
                        // get metadata for statuscode
                        AttributeMetadata am = dataAccess.metaData.GetAttributeMetadata(request.LogicalName, "statuscode");
                        StatusAttributeMetadata pam = (StatusAttributeMetadata)am;
                        OptionMetadata om = pam.OptionSet.Options.SingleOrDefault(o => o.Value == ((OptionSetValue)request["statuscode"]).Value);
                        status.value = om.Label.UserLocalizedLabel != null ? om.Label.UserLocalizedLabel.Label : "";
                    }
                }

                if (notes != null)
                {
                    notes.name = "notes";
                    if (request != null && request.Contains("oems_publicnotes") && request["oems_publicnotes"] != null)
                    {
                        notes.value = (string)request["oems_publicnotes"];
                    }
                }



                if (field_fullTimeTeacher != null)
                {
                    field_fullTimeTeacher.name = fulltimeTeacher;
                    if (request != null && request.Contains("oems_fulltimeteacher") && request["oems_fulltimeteacher"] != null)
                    {
                        field_fullTimeTeacher.options.selected = (bool)request["oems_fulltimeteacher"];
                    }

                }

                if (field_dailyOccasionalTeacher != null)
                {
                    field_dailyOccasionalTeacher.name = dailyOccasionalTeacher;
                    if (request != null && request.Contains("oems_dailyocassionalteacher") && request["oems_dailyocassionalteacher"] != null)
                    {
                        field_dailyOccasionalTeacher.options.selected = (bool)request["oems_dailyocassionalteacher"];
                    }
                }

                if (field_LTONonLTO != null)
                {

                    var references = dataAccess.context.CreateQuery("oems_ltooption");
                    references.Where(e => (int)e["statecode"] == 0);
                    field_LTONonLTO.name = ltoNonLto;
                    
                    List<OptionValue> options = dataAccess.GetOptions(references, "oems_name", null, null, null);

                    field_LTONonLTO.selectValues = (DataWork.OptionValue[])options.ToArray();

                    if (request != null)
                    {
                        if (request.Contains("oems_releasetimerequestltooption"))
                        {
                            field_LTONonLTO.value = ((EntityReference)request["oems_releasetimerequestltooption"]).Id.ToString();
                        }
                    }

                }

                if (field_partTimeTeacher != null)
                {
                    field_partTimeTeacher.name = partTimeTeacher;
                    if (request != null && request.Contains("oems_parttimeteacher") && request["oems_parttimeteacher"] != null)
                    {
                        field_partTimeTeacher.options.selected = (bool)request["oems_parttimeteacher"];
                    }

                }

                if (field_additionalInformation != null)
                {
                    field_additionalInformation.name = releaseAdditionalInformation;
                    if (request != null && request.Contains("oems_additionalinformation") && request["oems_additionalinformation"] != null)
                    {
                        field_additionalInformation.value = (string)request["oems_additionalinformation"];
                    }

                }

                if(regularReleaseTimeSection != null)
                {
                    DataWork.FormElement optionRow = regularReleaseTimeSection.FindById("RegularReleaseTimeRow");
                    if(optionRow != null)
                    {
                        List<DataWork.FormElement> optionRows = new List<DataWork.FormElement>();

                        var schedules = from s in dataAccess.context.CreateQuery("oems_eventreleasetimeschedule")
                                        where (int)s["statecode"] == 0 && (Guid)s["oems_event"] == ((EntityReference)root["oems_event"]).Id
                                        orderby s["oems_releasedate"] ascending
                                        select s;
                        foreach(var s in schedules)
                        {

                            ReleaseTimePeriod selectedPeriod = ReleaseTimePeriod.NONE;
                            if(request != null)
                            {
                               var scheduleRequest = (from r in dataAccess.context.CreateQuery("oems_releasetimerequestschedule")
                                                where (int)r["statecode"] == 0 && (Guid)r["oems_releasetimeschedule"] == s.Id &&  (Guid)r["oems_releasetimerequest"] == request.Id 
                                                select r).FirstOrDefault();
                           
                           
                               if(scheduleRequest != null && scheduleRequest.Contains("oems_releasetimeperiod") && scheduleRequest["oems_releasetimeperiod"] != null)
                               {
                                  selectedPeriod = (ReleaseTimePeriod)((OptionSetValue)scheduleRequest["oems_releasetimeperiod"]).Value;
                               }
                            }

                           DataWork.FormElement releaseTimeRow = optionRow.Clone();
                           DataWork.FormElement rowDate = releaseTimeRow.FindById("RegularReleaseTimeDate");
                           if (rowDate != null) 
                           {
                             rowDate.value = DataWork.DataAccess.GetDateString((DateTime)s["oems_releasedate"], true);
                             rowDate.name = "regulartime_date_"+s.Id.ToString();
                             optionRows.Add(rowDate);
                           }
                           DataWork.FormElement rowOptions = releaseTimeRow.FindById("RegularReleaseTimeOptions");
                           if (rowOptions != null)
                           {
                               rowOptions.name = "regulartime_period_"+s.Id.ToString();
                               List<DataWork.OptionValue> options = new List<DataWork.OptionValue>();

                               options.Add(new DataWork.OptionValue()
                               {
                                   value = "1",
                                   label = "Full Day"
                               }
                               );
                               options.Add(new DataWork.OptionValue()
                               {
                                   value ="2",
                                   label = "1/2 Day AM"
                                   
                               }
                               );
                               options.Add(new DataWork.OptionValue()
                               {
                                   value = "3",
                                   label = "1/2 Day PM"
                                   
                               }
                               );
                               options.Add(new DataWork.OptionValue()
                               {
                                   value = "0",
                                   label = "None"
                                   
                               }
                               );

                               rowOptions.value = ((int)selectedPeriod).ToString();
                               rowOptions.radioValues = options.ToArray();

                               optionRows.Add(rowOptions);
                           }
                           
                        }

                        regularReleaseTimeSection.children = optionRows.ToArray();
                    }
                }

                if(additionalReleaseTimeSection != null)
                {
                    DataWork.FormElement releaseTimeDate = additionalReleaseTimeSection.FindById("ReleaseTimeDate");
                    if(releaseTimeDate != null)
                    {
                        releaseTimeDate.name = "additionaltime_date";
                    }
                    
                    DataWork.FormElement additionalReleaseTimeOptions = additionalReleaseTimeSection.FindById("AdditionalReleaseTimeOptions");
                    
                    if(additionalReleaseTimeOptions != null)
                    {
                        additionalReleaseTimeOptions.name = "additionaltime_period";
                        List<DataWork.OptionValue> options = new List<DataWork.OptionValue>();

                        options.Add(new DataWork.OptionValue()
                                {
                                    value = "1",
                                    label = "Full Day"
                                }
                        );
                        options.Add(new DataWork.OptionValue()
                                {
                                    value = "2",
                                    label = "1/2 Day AM"
                                }
                        );
                        options.Add(new DataWork.OptionValue()
                                {
                                    value = "3",
                                    label = "1/2 Day PM"
                                }
                        );

                        additionalReleaseTimeOptions.radioValues = options.ToArray();
                    }

                    DataWork.FormElement additionalReleaseReason = additionalReleaseTimeSection.FindById("ReleaseTimeReason");
                    if (additionalReleaseReason != null)
                    {
                        additionalReleaseReason.name = "additionaltime_reason";
                    }

                    if (request != null)
                    {
                        var extraRequests = from r in dataAccess.context.CreateQuery("oems_extrareleasetimerequestschedule")
                                               where (int)r["statecode"] == 0  && (Guid)r["oems_releasetimerequest"] == request.Id
                                               select r;
                        DataWork.FormElement additionalReleaseTimeRow = additionalReleaseTimeSection.children[0];
                        List<DataWork.FormElement> extraRows = new List<DataWork.FormElement>();
                        foreach(var e in extraRequests)
                        {
                             DataWork.FormElement extraRow = additionalReleaseTimeRow.options.newRowTemplate.Clone();
                             DataWork.FormElement releaseDate = extraRow.FindById("ReleaseTimeDate");
                             DataWork.FormElement releaseReason = extraRow.FindById("ReleaseTimeReason");
                             DataWork.FormElement options = extraRow.FindById("AdditionalReleaseTimeOptions");
                             options.name = options.name + "_" + e.Id.ToString();
                             releaseDate.name = releaseDate.name + "_" + e.Id.ToString();
                             releaseReason.name = releaseReason.name + "_" + e.Id.ToString();

                             if(e.Contains("oems_reason") && e["oems_reason"] != null)
                             {
                                 releaseReason.value = (string)e["oems_reason"];
                             }
                             if (e.Contains("oems_extrareleasedate") && e["oems_extrareleasedate"] != null)
                             {
                                 releaseDate.value = DataWork.DataAccess.GetDateString((DateTime)e["oems_extrareleasedate"], true);
                             }
                             if (e.Contains("oems_releasetimeperiod") && e["oems_releasetimeperiod"] != null)
                             {
                                 ReleaseTimePeriod selected = (ReleaseTimePeriod)((OptionSetValue)e["oems_releasetimeperiod"]).Value;
                                 options.value = ((int)selected).ToString();
                             }

                             extraRows.Add(extraRow);

                            //selectedPeriod = (ReleaseTimePeriod)((OptionSetValue)scheduleRequest["oems_releasetimeperiod"]).Value;
                        }

                        additionalReleaseTimeRow.children = extraRows.ToArray();
                    }

                    

                }
                element.children = children.ToArray();
                //element.children = LoadOptions(dataAccess, element, root, groupRowId, null, categoryTemplate, rowTemplate, selection).ToArray();

                return true;
            }
            else return false;

        }

        public bool SaveElementData(DataWork.DataAccess dataAccess, DataWork.FormElement element, List<DataWork.NameValuePair> data, Microsoft.Xrm.Sdk.Entity root)
        {
            if (element.component == (int)FORM_COMPONENT.ReleaseTime)
            {
                
                Entity request = GetExistingReleaseTimeRequest(dataAccess, root);
                var releaseData = data.FindAll(x => x.name.Contains("releasetime_"));
                var regularTimePeriod = releaseData.Find(x => x.name.Contains("regulartime_period_"));
                var additionalDate = releaseData.Find(x => x.name.Contains("additionaltime_date"));


                if (request == null && releaseData.Count > 0)
                {
                    if ((regularTimePeriod != null && regularTimePeriod.value != "0") || additionalDate != null)
                    {
                        request = new Entity("oems_releasetimerequest");
                        request["oems_eventregistration"] = root.ToEntityReference();
                        request["oems_event"] = (EntityReference)root["oems_event"];
                        request.Id = dataAccess.context.Create(request);
                        request = GetExistingReleaseTimeRequest(dataAccess, root);
                    }

                }

                if(request != null)
                {
                   if(releaseData.Count == 0)
                   {
                      dataAccess.context.Delete(request.LogicalName, request.Id);
                   }
                   else
                   {
                      var pair = releaseData.Find(x => x.name.EndsWith("/rtschoolboard"));
                      if(pair != null)
                      {
                         request["oems_schoolboard"] = new EntityReference("oems_schoolboard", Guid.Parse(pair.value));
                      }
                      pair = releaseData.Find(x => x.name.EndsWith("/rtschool"));
                      if(pair != null)
                      {
                         request["oems_school"] = new EntityReference("oems_school", Guid.Parse(pair.value));
                      }
                      pair = releaseData.Find(x => x.name.EndsWith("/rtlocal"));
                      if(pair != null)
                      {
                         request["oems_local"] = new EntityReference("oems_local", Guid.Parse(pair.value));
                      }
                      pair = releaseData.Find(x => x.name.EndsWith("/schoolprincipal"));
                      if(pair != null)
                      {
                         request["oems_schoolprincipalname"] = pair.value;
                      }
                      pair = releaseData.Find(x => x.name.EndsWith("/previewRtSchoolEmail"));
                      if (pair != null)
                      {
                          request["oems_schoolemail"] = pair.value;
                      }
                      pair = releaseData.Find(x => x.name.EndsWith("/previewRtSchoolFax"));
                      if (pair != null)
                      {
                          request["oems_schoolfax"] = pair.value;
                      }

                      var dataToSave = data.Find(x => x.name.EndsWith(fulltimeTeacher));

                      request["oems_fulltimeteacher"] = (dataToSave != null) ? true : false;

                      dataToSave = data.Find(x => x.name.EndsWith(partTimeTeacher));

                      request["oems_parttimeteacher"] = (dataToSave != null) ? true : false;

                      dataToSave = data.Find(x => x.name.EndsWith(releaseAdditionalInformation));

                      request["oems_additionalinformation"] = (dataToSave != null) ? dataToSave.value : "";

                      dataToSave = data.Find(x => x.name.EndsWith(dailyOccasionalTeacher));

                      request["oems_dailyocassionalteacher"] = (dataToSave != null) ? true : false;

                      dataToSave = releaseData.Find(x => x.name.EndsWith(ltoNonLto));

                      if (dataToSave != null)
                      {
                          request["oems_releasetimerequestltooption"] = new EntityReference("oems_ltooption", Guid.Parse(dataToSave.value));
                      }
                      
                        var schedules = from s in dataAccess.context.CreateQuery("oems_eventreleasetimeschedule")
                                        where (int)s["statecode"] == 0 && (Guid)s["oems_event"] == ((EntityReference)root["oems_event"]).Id
                                        orderby s["oems_releasedate"] ascending
                                        select s;
                        foreach(var s in schedules)
                        {
                            var scheduleRequest = (from r in dataAccess.context.CreateQuery("oems_releasetimerequestschedule")
                                                   where (int)r["statecode"] == 0 && (Guid)r["oems_releasetimeschedule"] == s.Id && (Guid)r["oems_releasetimerequest"] == request.Id
                                                   select r).FirstOrDefault();

                            pair = releaseData.Find(x => x.name.EndsWith("/regulartime_period_"+DataWork.FormElement.GuidToString(s.Id)));
                            if (pair != null && int.Parse(pair.value) != 0)
                            {
                                if (scheduleRequest == null)
                                {
                                    scheduleRequest = new Entity("oems_releasetimerequestschedule");
                                    scheduleRequest["oems_releasetimeschedule"] = s.ToEntityReference();
                                    scheduleRequest["oems_releasetimerequest"] = request.ToEntityReference();
                                    scheduleRequest["oems_releasetimeperiod"] = new OptionSetValue(int.Parse(pair.value));
                                    dataAccess.context.Create(scheduleRequest);
                                    
                                }
                                else
                                {
                                    scheduleRequest = dataAccess.context.Retrieve(scheduleRequest.LogicalName, scheduleRequest.Id, new Microsoft.Xrm.Sdk.Query.ColumnSet("oems_releasetimeperiod"));
                                    scheduleRequest["oems_releasetimeperiod"] = new OptionSetValue(int.Parse(pair.value));
                                    dataAccess.context.Update(scheduleRequest);
                                }

                            }
                            else if(scheduleRequest != null)
                            {
                                dataAccess.context.Delete(scheduleRequest.LogicalName, scheduleRequest.Id);
                            }
                        }

                        var extraRequests = from r in dataAccess.context.CreateQuery("oems_extrareleasetimerequestschedule")
                                            where (int)r["statecode"] == 0 && (Guid)r["oems_releasetimerequest"] == request.Id
                                            select r;
                        foreach (var es in extraRequests)
                        {
                            var pairReason = releaseData.Find(x => x.name.EndsWith("/additionaltime_reason_" + es.Id.ToString()));
                            var pairDate = releaseData.Find(x => x.name.EndsWith("/additionaltime_date_" + es.Id.ToString()));
                            var pairPeriod = releaseData.Find(x => x.name.EndsWith("/additionaltime_period_" + es.Id.ToString()));
                            if (pairReason != null || pairDate != null || pairPeriod != null)
                            {
                                Entity updatedExtraRequest = new Entity("oems_extrareleasetimerequestschedule");
                                updatedExtraRequest.Id = es.Id;
                                if (pairPeriod != null) updatedExtraRequest["oems_releasetimeperiod"] = new OptionSetValue(int.Parse(pairPeriod.value));
                                else updatedExtraRequest["oems_releasetimeperiod"] = null;

                                if (pairDate != null) updatedExtraRequest["oems_extrareleasedate"] = DateTime.Parse(pairDate.value);
                                else updatedExtraRequest["oems_extrareleasedate"] = null;

                                if (pairReason != null) updatedExtraRequest["oems_reason"] = pairReason.value;
                                else updatedExtraRequest["oems_reason"] = null;

                                dataAccess.context.Update(updatedExtraRequest);
                                
                            }
                            else 
                            {
                                dataAccess.context.Delete(es.LogicalName, es.Id);
                            }
                        }

                        var newExtraTime = data.FindAll(x => x.name.Contains("releasetime") && x.name.Contains("#") &&
                            (x.name.EndsWith("additionaltime_reason") ||
                             x.name.EndsWith("additionaltime_date") ||
                             x.name.EndsWith("additionaltime_period")
                             )
                          );
                        while(newExtraTime.Count > 0)
                        {
                          var nvp = newExtraTime[0];

                          int i = nvp.name.IndexOf("#");
                          int j = nvp.name.IndexOf("/", i);
                          string rowNumberSuffix = nvp.name.Substring(i, j-i)+"/";

                          var pairReason = newExtraTime.Find(x => x.name.Contains(rowNumberSuffix) && x.name.EndsWith("additionaltime_reason"));
                          var pairDate = newExtraTime.Find(x => x.name.Contains(rowNumberSuffix) && x.name.EndsWith("additionaltime_date"));
                          var pairPeriod = newExtraTime.Find(x => x.name.Contains(rowNumberSuffix) && x.name.EndsWith("additionaltime_period"));

                          Entity newExtraRequest = new Entity("oems_extrareleasetimerequestschedule");

                          if (pairPeriod != null)
                          {
                             newExtraRequest["oems_releasetimeperiod"] = new OptionSetValue(int.Parse(pairPeriod.value));
                             newExtraTime.Remove(pairPeriod);
                          }


                          if (pairDate != null) 
                          {
                             newExtraRequest["oems_extrareleasedate"] = DateTime.Parse(pairDate.value);
                             newExtraTime.Remove(pairDate);
                          }


                          if (pairReason != null)
                          {
                             newExtraRequest["oems_reason"] = pairReason.value;
                             newExtraTime.Remove(pairReason);
                          }
                          newExtraRequest["oems_releasetimerequest"] = request.ToEntityReference();

                          dataAccess.context.Create(newExtraRequest);
                             

                        }

                      dataAccess.context.Update(request);
                   }
                }
                return true;
            }
            else return false;
        }
    }
}

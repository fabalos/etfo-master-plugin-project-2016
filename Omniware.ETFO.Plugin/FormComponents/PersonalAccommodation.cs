﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Metadata;
using Omniware.ETFO.Plugin.Shared;
using Xrm;
using log4net;


namespace Omniware.ETFO.Plugin.FormComponents
{

    public class PersonalAccommodationSelection
    {
       public bool Hotel {get;set;}
       public bool Event { get; set; }
       public string Details { get; set; }
       public string Notes { get; set; }
       public string Status { get; set; }
       public Guid OptionId {get;set;}
    }

    public class PersonalAccommodation : IFormElement
    {
        ILog Log = LogHelper.GetLogger(typeof(PersonalAccommodation));


        static int expiryMinutes = 5;  // todo can be made configurable
        static DateTime lastLoadedEventOptions, lastLoadedHeaderOptions;

        bool personalAccommodationRequired;

        // cache collection of Crm entities by type ie Category etc. remember when it was loaded
        static readonly ConcurrentDictionary<Guid, List<Entity>> eventOptionCache = new ConcurrentDictionary<Guid, List<Entity>>();
        static List<Entity> headerOptionCache; 


        int ONLY_HOTEL = 4;
        int ONLY_EVENT = 3;

//        entertaining but unnecessary
//        public bool LoadComponent(LocalPluginContext context, Entity e, DataWork.FormElement element)
//        {
//            bool  result = false;
//            return result;
//        }

        public void SetPersonalAccommodationRequired(bool required)
        {
            this.personalAccommodationRequired = required;
        }

        List<Entity> GetEventOptions(OmniContext context, Guid eventId)
        {
            var sw = new Stopwatch(); sw.Start();
            if (DateTime.Now > lastLoadedEventOptions.AddMinutes(expiryMinutes))
            {
                eventOptionCache.Clear();
                lastLoadedEventOptions = DateTime.Now;
                Log.Debug("GetEventOptions Cache cleared");
            }
            var options = eventOptionCache.GetOrAdd(eventId, s =>
                                            (from o in context.CreateQuery("oems_personalaccommodationoption")
                                             join rel in context.CreateQuery("oems_event_personal_accommodation_config") on
                                                 (Guid)o["oems_personalaccommodationoptionid"] equals (Guid)rel["oems_personalaccommodationoptionid"]
                                             where (Guid)rel["oems_eventid"] == eventId
                                             orderby o["oems_optionname"] ascending
                                             select o).ToList()
                );
            sw.Stop();
            if (sw.ElapsedMilliseconds > 1) Log.DebugFormat("GetEventOptions {0} {1} msec", eventId, sw.ElapsedMilliseconds);
         
            return options;
         }

        List<Entity> GetHeaderOptions(OmniContext context)
        {
            var sw = new Stopwatch(); sw.Start();
            if (DateTime.Now > lastLoadedHeaderOptions.AddMinutes(expiryMinutes))
            {
                headerOptionCache = (from o in context.CreateQuery("oems_personalaccommodationoption")
                                     where (int)o["oems_optiontype"] == 1
                                     select o).ToList();
                lastLoadedHeaderOptions = DateTime.Now;
                Log.Debug("GetHeaderOptions Cache cleared");
            }

            sw.Stop();
            if (sw.ElapsedMilliseconds > 1) Log.DebugFormat("GetHeaderOptions {0} msec", sw.ElapsedMilliseconds);
         
            return headerOptionCache;
         }



        public List<DataWork.FormElement> LoadOptions(DataWork.DataAccess dataAccess, DataWork.FormElement element, Microsoft.Xrm.Sdk.Entity root, Guid groupRowId, Entity currentOption, DataWork.FormElement categoryTemplate, DataWork.FormElement rowTemplate, List<PersonalAccommodationSelection> selection)
        {

            List<DataWork.FormElement> result = new List<DataWork.FormElement>(); 
            List<DataWork.FormElement> elements = new List<DataWork.FormElement>();

            List<Entity> headerOptions = null;
            List<Entity> linkedOptions = null;

            var sw = new Stopwatch(); sw.Start();

            if(currentOption == null)
            {
//                linkedOptions = (from o in dataAccess.context.CreateQuery("oems_personalaccommodationoption")
//                                    join rel in dataAccess.context.CreateQuery("oems_event_personal_accommodation_config") on (Guid)o["oems_personalaccommodationoptionid"] equals (Guid)rel["oems_personalaccommodationoptionid"]
//                                    where (Guid)rel["oems_eventid"] == ((EntityReference)root["oems_event"]).Id
//                                 where (int)o["oems_optiontype"] != 1 && o["oems_parentoption"] == null
//                                 orderby o["oems_optionname"] ascending
//                                  select o).ToList();
                linkedOptions = (from o in GetEventOptions( dataAccess.context, ((EntityReference)root["oems_event"]).Id)
                                 where o.GetAttributeValue<int>("oems_optiontype") != 1 && !o.Contains("oems_parentoption")
                                 orderby o["oems_optionname"] ascending
                                  select o).ToList();
//                headerOptions = (from o in dataAccess.context.CreateQuery("oems_personalaccommodationoption")
//                                 where (int)o["oems_optiontype"] == 1 && o["oems_parentoption"] == null
//                                 orderby o["oems_optionname"] ascending
//                                 select o).ToList();
                headerOptions = (from o in GetHeaderOptions(dataAccess.context)
                                 where !o.Contains("oems_parentoption")
                                 orderby o["oems_optionname"] ascending
                                 select o).ToList();
            }
            else
            {
//                linkedOptions = (from o in dataAccess.context.CreateQuery("oems_personalaccommodationoption")
//                                  join rel in dataAccess.context.CreateQuery("oems_event_personal_accommodation_config")
//                                    on (Guid)o["oems_personalaccommodationoptionid"] equals (Guid)rel["oems_personalaccommodationoptionid"]
//                                    where  (Guid)rel["oems_eventid"] == ((EntityReference)root["oems_event"]).Id
//                                  where (Guid)o["oems_parentoption"] == (Guid)currentOption.Id && (int)o["oems_optiontype"] != 1
//                                  orderby o["oems_optionname"] ascending
//                                  select o).ToList();
                linkedOptions = (from o in GetEventOptions(dataAccess.context, ((EntityReference)root["oems_event"]).Id)
                                 where o.GetAttributeValue<Guid>("oems_parentoption") == currentOption.Id && o.GetAttributeValue<int>("oems_optiontype") != 1
                                  orderby o["oems_optionname"] ascending
                                  select o).ToList();
//                headerOptions = (from o in dataAccess.context.CreateQuery("oems_personalaccommodationoption")
//                                 where (int)o["oems_optiontype"] == 1 && (Guid)o["oems_parentoption"] == (Guid)currentOption.Id
//                                 orderby o["oems_optionname"] ascending
//                                 select o).ToList();
                headerOptions = (from o in GetHeaderOptions(dataAccess.context)
                                 where o.GetAttributeValue<Guid>("oems_parentoption") == currentOption.Id
                                 orderby o["oems_optionname"] ascending
                                 select o).ToList();
            }

            List<Entity> categoryOptions = new List<Entity>();
            categoryOptions.AddRange(headerOptions);
            categoryOptions.AddRange(linkedOptions);
            foreach (var o in categoryOptions)
            {
                if(((OptionSetValue)o["oems_optiontype"]).Value == 1)
                {
                    DataWork.FormElement optionElement = categoryTemplate.Clone();
                    if (optionElement.id == "PersonalAccommodationRow" && (string)o["oems_optionname"] != "Other")
                    {
                        optionElement.DeleteChildByTitle("Details");
                    }
                    optionElement.name = o.Id.ToString();
                    optionElement.value = (string)o["oems_optionname"];
                    optionElement.options.label = (string)o["oems_optionname"];
                    optionElement.options.title = (string)o["oems_optionname"];
                    //optionElement.options.open = false;

                    DataWork.FormElement optionNameElement = optionElement.FindById("PersonalAccommodationCategoryName");
                    optionNameElement.name = "PersonalAccommodationCategoryName"+o.Id.ToString();
                    optionNameElement.options.selected = false;
                    optionNameElement.options.label = (string)o["oems_optionname"];
                    optionNameElement.options.title = (string)o["oems_optionname"];
                    optionNameElement.options.open = false;

                    
                    DataWork.FormElement sectionElement = optionElement.FindById("PersonalAccommodationCategorySection");
                    sectionElement.name = "PersonalAccommodationCategorySection" + o.Id.ToString();
                    
                    optionNameElement.options.enablesSection = sectionElement.name;
                    sectionElement.options.collapse = false;

                    sectionElement.children = LoadOptions(dataAccess, element, root, groupRowId, o, categoryTemplate, rowTemplate, selection).ToArray();

                    // set parent selected flag if children are selected
                    foreach (var child in sectionElement.children)
                    {
                        if (child.options.selected ?? false)
                        {
                            sectionElement.options.selected = true;
                            optionNameElement.options.selected = true;
                            optionElement.options.selected = true;
                            break;
                        }
                    }

                    if (sectionElement.children.Length > 0) result.Add(optionElement);
                }
                else
                {
                    DataWork.FormElement optionElement = rowTemplate.Clone();

                    if (optionElement.id == "PersonalAccommodationRow" && (string)o["oems_optionname"] != "Other")
                    {
                        optionElement.DeleteChildByTitle("Details");
                    }

                    optionElement.name = o.Id.ToString();
                    optionElement.value = (string)o["oems_optionname"];
                    optionElement.options.label =  (string)o["oems_optionname"];
                    optionElement.options.title = (string)o["oems_optionname"];
                    optionElement.options.open = false;
                    elements.Add(optionElement);
                    PersonalAccommodationSelection pas = selection.Find(s => s.OptionId == o.Id);

                    bool hotelVisible = true;
                    bool eventVisible = true;
                    bool detailsVisible = true;

                    if (((OptionSetValue)o["oems_optiontype"]).Value == ONLY_EVENT)
                    {
                        hotelVisible = false;
                    }
                    if (((OptionSetValue)o["oems_optiontype"]).Value == ONLY_HOTEL)
                    {
                        eventVisible = false;
                    }
                    if ((Boolean)o["oems_userspecifieddetailflag"] != true)
                    {
                        detailsVisible = false;
                    }

                    DataWork.FormElement fe = optionElement.FindById("Selection");
                    if(fe != null)
                    {
                      fe.name = "personalaccommodation_selection_"+o.Id.ToString().Replace("{", "").Replace("}", "") + "_" + fe.name;
                      fe.options.label = "";
                      List<DataWork.OptionValue> options = new List<DataWork.OptionValue>();
                      if(eventVisible)
                      {
                        options.Add(new DataWork.OptionValue()
                          {
                              value = "personalaccommodation_eventflag_" + o.Id.ToString().Replace("{", "").Replace("}", ""),
                              label = "Event",
                              selected = (pas != null && pas.Event == true).ToString().ToLower()
                          }
                        );
                      }

                      if (hotelVisible)
                      {
                          options.Add(new DataWork.OptionValue()
                          {
                              value = "personalaccommodation_hotelflag_" + o.Id.ToString().Replace("{", "").Replace("}", ""),
                              label = "Hotel",
                              selected = (pas != null && pas.Hotel == true).ToString().ToLower()
                               
                          }
                          );
                      }
                     
                      fe.checkboxValues = options.ToArray();
                        /*
                        if(pas != null)
                        {
                          List<DataWork.OptionValue> selectedOptions = new List<DataWork.OptionValue>();
                        
                          if(pas.Hotel)
                          {
                              selectedOptions.Add(new DataWork.OptionValue()
                                  {
                                      value = "personalaccommodation_hotelflag_" + o.Id.ToString().Replace("{", "").Replace("}", ""),
                                      label = "Hotel"
                                  }
                              );
                          }
                          if (pas.Event)
                          {
                              selectedOptions.Add(new DataWork.OptionValue()
                                  {
                                      value = "personalaccommodation_eventflag_" + o.Id.ToString().Replace("{", "").Replace("}", ""),
                                      label = "Event"
                                  }
                              );
                          }
                          fe.selectValues = selectedOptions.ToArray();
                        }
                         * */
                    }

                    if (detailsVisible)
                    {
                        fe = optionElement.FindChildBySourcePath("oems_userspecifieddetails");
                        if (fe != null)
                        {
                            fe.name = "personalaccommodation_details_" + o.Id.ToString().Replace("{", "").Replace("}", "");
                        }
                    }
                    fe = optionElement.FindChildBySourcePath("statuscode");
                    if (fe != null)
                    {
                        fe.name = "personalaccommodation_statuscode_" + o.Id.ToString().Replace("{", "").Replace("}", "");
                        //fe.options.disabled = true;
                    }
                    fe = optionElement.FindChildBySourcePath("oems_publicnotes");
                    if (fe != null)
                    {
                        fe.name = "personalaccommodation_publicnotes_" + o.Id.ToString().Replace("{", "").Replace("}", "");
                    }

                    fe = optionElement.FindById("OptionTitle");
                    if (fe != null)
                    {
                        fe.value = fe.value.Replace("#name#", (string)o["oems_optionname"]);
                    }

                    if(pas != null)
                    {
                       if (pas.Details != null)
                       {
                           fe = optionElement.FindChildBySourcePath("oems_userspecifieddetails");
                           if (fe != null)
                               fe.value = pas.Details;
                       }
                       if (pas.Status != null)
                       {
                           fe = optionElement.FindChildBySourcePath("statuscode");
                           if (fe != null)
                               fe.value = pas.Status;
                       }
                       if (pas.Notes != null)
                       {
                           fe = optionElement.FindChildBySourcePath("oems_publicnotes");
                           if (fe != null)
                               fe.value = pas.Notes;
                       }

                       // set optionElement to selected if the PAS is found, which means event or hotel is selected
                       optionElement.options.selected = true;
                       
                    }



                    
                }
                
            }
            result.AddRange(elements);
            
            sw.Stop();
            if(sw.ElapsedMilliseconds > 1) Log.Debug("Personal Accommodation load data "+element.id +" : " +sw.ElapsedMilliseconds + " msec");
            return result;
        }

        public bool LoadDynamicFormData(DataWork.DataAccess dataAccess, DataWork.FormElement element, Microsoft.Xrm.Sdk.Entity root, Guid groupRowId)
        {
            if(element.component == (int)FORM_COMPONENT.PersonalAccommodation)
            {
               DataWork.FormElement categoryTemplate = null;
               DataWork.FormElement rowTemplate = null;
               foreach(var c in element.children)
               {
                 if(c.id == "PersonalAccommodationRow") rowTemplate = c;
                 if (c.id == "PersonalAccommodationCategory") categoryTemplate = c;
               }

               List<PersonalAccommodationSelection> selection = 
                  (from e in dataAccess.context.CreateQuery("oems_personalaccommodationrequestoption")
                   where (Guid)e["oems_eventregistration"] == root.Id
                   select new PersonalAccommodationSelection()
                     {
                         Hotel = e.Contains("oems_hotelflag") && e["oems_hotelflag"] != null && (bool)e["oems_hotelflag"] == true,
                         Event = e.Contains("oems_eventflag") && e["oems_eventflag"] != null && (bool)e["oems_eventflag"] == true,
                         Details = e.Contains("oems_userspecifieddetails") && e["oems_userspecifieddetails"] != null ? (string)e["oems_userspecifieddetails"] : "",
                         Notes = e.Contains("oems_publicnotes") && e["oems_publicnotes"] != null ? (string)e["oems_publicnotes"] : "",
                         Status = ((PersonalAccomodationRequestOptionStatusReason)((OptionSetValue)e["statuscode"]).Value).ToString(),
                         OptionId = ((EntityReference)e["oems_personalaccommodationoption"]).Id
                     }
                   ).ToList<PersonalAccommodationSelection>();
                 
               element.children = LoadOptions(dataAccess, element, root, groupRowId, null, categoryTemplate, rowTemplate, selection).ToArray();
               
               return true;
            }
            else return false;
            
        }

        public bool SaveElementData(DataWork.DataAccess dataAccess, DataWork.FormElement element, List<DataWork.NameValuePair> data, Microsoft.Xrm.Sdk.Entity root)
        {
            Entity personalAccommodationRequest = null;

            if (element.component == (int)FORM_COMPONENT.PersonalAccommodation)
            {

                if (((bool)root.Attributes["oems_personalaccommodationrequiredflag"] == true) || (this.personalAccommodationRequired == true))
                {
                    List<Entity> updates = (from e in dataAccess.context.CreateQuery("oems_personalaccommodationrequestoption")
                                            where (Guid)e["oems_eventregistration"] == root.Id
                                            select e).ToList();

                    personalAccommodationRequest = (from e in dataAccess.context.CreateQuery("oems_personalaccommodationrequest")
                                                    where (Guid)e["oems_eventregistration"] == root.Id
                                                    select e).FirstOrDefault();


                    if (personalAccommodationRequest == null)
                    {
                        personalAccommodationRequest = new Entity("oems_personalaccommodationrequest");
                        personalAccommodationRequest["oems_eventregistration"] = root.ToEntityReference();
                        personalAccommodationRequest["oems_event"] = root.Attributes["oems_event"];
                        personalAccommodationRequest.Id = dataAccess.context.Create(personalAccommodationRequest);

                    }

                    foreach (var nvp in data)
                    {
                        int i = nvp.name.IndexOf("personalaccommodation_");
                        if (i > 0)
                        {
                            string name = nvp.name.Substring(i);
                            string[] parts = name.Split('_');//0 = .., 1 = name, 2 = optionid
                            Guid optionId = Guid.Parse(parts[2]);

                            if (parts[1] == "statuscode" || parts[1] == "publicnotes") continue;

                            Entity existingRequestOption = updates.Find(x => ((EntityReference)x["oems_personalaccommodationoption"]).Id == optionId);

                            if (existingRequestOption == null)
                            {

                                existingRequestOption = (from e in dataAccess.context.CreateQuery("oems_personalaccommodationrequestoption")
                                                         where (Guid)e["oems_eventregistration"] == root.Id && (Guid)e["oems_personalaccommodationoption"] == optionId
                                                         select e).FirstOrDefault();
                                if (existingRequestOption == null)
                                {
                                    existingRequestOption = new Entity("oems_personalaccommodationrequestoption");
                                    existingRequestOption["oems_personalaccommodationoption"] = new EntityReference("oems_personalaccommodationoption", optionId);
                                    existingRequestOption["oems_eventregistration"] = root.ToEntityReference();
                                    existingRequestOption["oems_personalaccommodationrequest"] = personalAccommodationRequest.ToEntityReference();
                                    existingRequestOption.Id = dataAccess.context.Create(existingRequestOption);
                                    existingRequestOption = dataAccess.context.Retrieve(existingRequestOption.LogicalName, existingRequestOption.Id, new Microsoft.Xrm.Sdk.Query.ColumnSet(true));
                                }
                                updates.Add(existingRequestOption);
                                //existingRequestOption["updated"] = true;
                            }
                            if (!existingRequestOption.Contains("updated"))
                            {
                                existingRequestOption["oems_hotelflag"] = false;
                                existingRequestOption["oems_eventflag"] = false;

                            }

                            if (parts[1] == "selection")
                            {
                                parts = nvp.value.Split('_');
                            }
                            if (parts[1] == "hotelflag")
                            {
                                existingRequestOption["oems_hotelflag"] = true;
                                existingRequestOption["updated"] = true;
                            }
                            if (parts[1] == "eventflag")
                            {
                                existingRequestOption["oems_eventflag"] = true;
                                existingRequestOption["updated"] = true;
                            }
                            if (parts[1] == "details")
                            {
                                existingRequestOption["oems_userspecifieddetails"] = nvp.value;
                            }
                        }
                    }
                    foreach (var e in updates)
                    {
                        if (e.Contains("updated"))
                        {
                            Entity tmpEntity = new Entity("oems_personalaccommodationrequestoption");
                            if (e.Contains("oems_eventflag")) tmpEntity["oems_eventflag"] = e["oems_eventflag"];
                            if (e.Contains("oems_hotelflag")) tmpEntity["oems_hotelflag"] = e["oems_hotelflag"];
                            if (e.Contains("oems_userspecifieddetails")) tmpEntity["oems_userspecifieddetails"] = e["oems_userspecifieddetails"];
                            tmpEntity.Id = e.Id;
                            dataAccess.context.Update(tmpEntity);
                        }
                        else
                        {
                            dataAccess.context.Delete(e.LogicalName, e.Id);
                        }

                    }

                    return true;
                }
                else return false;
            }
            else return false;
        }
    }
}

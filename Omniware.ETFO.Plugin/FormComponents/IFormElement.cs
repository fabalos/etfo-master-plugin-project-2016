﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xrm.Sdk;
using Xrm;
using Omniware.ETFO.Plugin;
using System.Collections;
using Microsoft.Xrm.Sdk.Metadata;
using Xrm;


namespace Omniware.ETFO.Plugin.FormComponents
{

    

    public interface IFormElement
    {
        bool LoadDynamicFormData(DataWork.DataAccess dataAccess, DataWork.FormElement element, Entity root, Guid groupRowId);
        bool SaveElementData(DataWork.DataAccess dataAccess, DataWork.FormElement element, List<DataWork.NameValuePair> data, Entity root);
        //bool LoadComponent(LocalPluginContext context, Entity e, DataWork.FormElement element);
    }
}

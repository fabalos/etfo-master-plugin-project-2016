﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xrm.Sdk;
using Omniware.ETFO.Plugin;
using Xrm;


namespace Omniware.ETFO.Plugin.FormComponents
{
    public class SchoolBoardLocal : IFormElement
    {

//        entertaining but unnecessary
//        public bool LoadComponent(LocalPluginContext context, Entity e, DataWork.FormElement element)
//        {
//            bool result = false;
//            return result;
//        }

        public bool LoadDynamicFormData(DataWork.DataAccess dataAccess, DataWork.FormElement element, Microsoft.Xrm.Sdk.Entity root, Guid groupRowId)
        {
            if (element.component == (int)FORM_COMPONENT.SchoolBoardLocal)
            {
                if (element.id == "CoSchool Board Local")
                {
                    element.name = "coschoolboardlocal_" + element.name;
                }
                else
                {
                    element.name = "schoolboardlocal_" + element.name;
                }

                DataWork.FormElement schoolBoard = null;
                DataWork.FormElement school = null;
                DataWork.FormElement local = null;
                DataWork.FormElement coSchoolBoard = null;
                DataWork.FormElement coSchool = null;
                DataWork.FormElement coLocal = null;

                List<DataWork.FormElement> children = new List<DataWork.FormElement>(element.children);
                
                foreach (var c in children)
                {
                    if (c.id == "SchoolBoard") schoolBoard = c;
                    else if (c.id == "School") school = c;
                    else if (c.id == "Local") local = c;
                    else if (c.id == "CoSchoolBoard") coSchoolBoard = c;
                    else if (c.id == "CoSchool") coSchool = c;
                    else if (c.id == "CoLocal") coLocal = c;
                }

                if(school != null) 
                {
                  school.name = "pi_school";
                  school.type = "school";
                  if (root != null && root.Contains("oems_school") && root["oems_school"] != null)
                  {
                      school.value = DataWork.FormElement.GuidToString(((EntityReference)root["oems_school"]).Id);
                  }
                }

                if (local != null)
                {
                    local.name = "pi_local";
                    local.type = "local";
                    if (root != null && root.Contains("oems_local") && root["oems_local"] != null)
                    {
                        local.value = DataWork.FormElement.GuidToString(((EntityReference)root["oems_local"]).Id);
                    }
                    
                }

                if (schoolBoard != null)
                {
                    schoolBoard.name = "pi_schoolboard";
                    schoolBoard.type = "schoolboard";
                    if (root != null && root.Contains("oems_schoolboard") && root["oems_schoolboard"] != null)
                    {
                        schoolBoard.value = DataWork.FormElement.GuidToString(((EntityReference)root["oems_schoolboard"]).Id);
                    }
                }

                if (coSchool != null)
                {
                    coSchool.name = "pi_coschool";
                    coSchool.type = "coschool";
                    if (root != null && root.Contains("oems_coschool") && root["oems_coschool"] != null)
                    {
                        coSchool.value = DataWork.FormElement.GuidToString(((EntityReference)root["oems_coschool"]).Id);
                    }
                }

                if (coLocal != null)
                {
                    coLocal.name = "pi_colocal";
                    coLocal.type = "colocal";
                    if (root != null && root.Contains("oems_colocal") && root["oems_colocal"] != null)
                    {
                        coLocal.value = DataWork.FormElement.GuidToString(((EntityReference)root["oems_colocal"]).Id);
                    }

                }

                if (coSchoolBoard != null)
                {
                    coSchoolBoard.name = "pi_coschoolboard";
                    coSchoolBoard.type = "coschoolboard";
                    if (root != null && root.Contains("oems_coschoolboard") && root["oems_coschoolboard"] != null)
                    {
                        coSchoolBoard.value = DataWork.FormElement.GuidToString(((EntityReference)root["oems_coschoolboard"]).Id);
                    }
                }
             
                element.children = children.ToArray();

                return true;
            }
            else return false;

        }

        public bool SaveElementData(DataWork.DataAccess dataAccess, DataWork.FormElement element, List<DataWork.NameValuePair> data, Microsoft.Xrm.Sdk.Entity root)
        {
            Entity oems_eventregistration = new Entity(root.LogicalName);
            oems_eventregistration.Id = root.Id;

            if (element.component == (int)FORM_COMPONENT.SchoolBoardLocal)
            {
                if (element.id == "CoSchool Board Local")
                {
                    var coSchoolBoardLocalData = data.FindAll(x => x.name.Contains("coschoolboardlocal_"));

                    var copair = coSchoolBoardLocalData.Find(x => x.name.EndsWith("/pi_coschoolboard"));
                    if (copair != null)
                    {
                        oems_eventregistration["oems_coschoolboard"] = new EntityReference("oems_schoolboard", Guid.Parse(copair.value));
                    }
                    copair = coSchoolBoardLocalData.Find(x => x.name.EndsWith("/pi_coschool"));
                    if (copair != null)
                    {
                        oems_eventregistration["oems_coschool"] = new EntityReference("oems_school", Guid.Parse(copair.value));
                    }
                    copair = coSchoolBoardLocalData.Find(x => x.name.EndsWith("/pi_colocal"));
                    if (copair != null)
                    {
                        oems_eventregistration["oems_colocal"] = new EntityReference("oems_local", Guid.Parse(copair.value));
                    }
                }
                else
                {
                    var schoolBoardLocalData = data.FindAll(x => x.name.Contains("schoolboardlocal_"));

                    var pair = schoolBoardLocalData.Find(x => x.name.EndsWith("/pi_schoolboard"));
                    if (pair != null)
                    {
                        oems_eventregistration["oems_schoolboard"] = new EntityReference("oems_schoolboard", Guid.Parse(pair.value));
                    }
                    pair = schoolBoardLocalData.Find(x => x.name.EndsWith("/pi_school"));
                    if (pair != null)
                    {
                        oems_eventregistration["oems_school"] = new EntityReference("oems_school", Guid.Parse(pair.value));
                    }
                    pair = schoolBoardLocalData.Find(x => x.name.EndsWith("/pi_local"));
                    if (pair != null)
                    {
                        oems_eventregistration["oems_local"] = new EntityReference("oems_local", Guid.Parse(pair.value));
                        oems_eventregistration["oems_event"] = root.Attributes.Contains("oems_event") == true ? root["oems_event"] : null;
                        SetupLocal(dataAccess.context, oems_eventregistration);
                    }
                }

                //root.Attributes.Remove("statuscode");
                /*if (root.Attributes.Contains("oems_submittedflag")) root.Attributes.Remove("oems_submittedflag");
                if (root.Attributes.Contains("oems_approvedflag")) root.Attributes.Remove("oems_approvedflag");
                if (root.Attributes.Contains("oems_rejectedflag")) root.Attributes.Remove("oems_rejectedflag");
                if (root.Attributes.Contains("oems_withdrawnflag")) root.Attributes.Remove("oems_withdrawnflag");
                if (root.Attributes.Contains("oems_cancelledflag")) root.Attributes.Remove("oems_cancelledflag");*/

                dataAccess.context.Update(oems_eventregistration);
                      
                return true;
            }
            else return false;
        }


        void SetupLocal(OmniContext localContext, Entity root)
        {
            if(root.LogicalName == "oems_eventregistration")
            {
            Entity eventRegistration = root;

            if (eventRegistration.Attributes.ContainsKey("oems_local"))
            {
                if (eventRegistration.Attributes.ContainsKey("oems_event"))
                {
                    EntityReference eventReference = (EntityReference)eventRegistration.Attributes["oems_event"];

                    Entity oemsEvent = localContext.Retrieve(eventReference.LogicalName, eventReference.Id, new Microsoft.Xrm.Sdk.Query.ColumnSet(true));

                    if (oemsEvent.Attributes.ContainsKey("oems_localdelegationcomponentflag"))
                    {
                        bool flag = (bool)oemsEvent.Attributes["oems_localdelegationcomponentflag"];

                        if (flag)
                        {
                            EntityReference localRef = (EntityReference)eventRegistration["oems_local"];

                            EntityReference local = FindLocalInEvent(oemsEvent, localContext, localRef.Id, oemsEvent.Id);

                            if (local != null)
                            {
                                if (eventRegistration.Attributes.ContainsKey("oems_eventlocal"))
                                {
                                    eventRegistration["oems_eventlocal"] = local;
                                }
                                else
                                {
                                    eventRegistration.Attributes.Add("oems_eventlocal", local);
                                }

                            }
                        }
                    }
                }
            }
            }
        }

        EntityReference FindLocalInEvent(Entity oemsEvent, OmniContext localContext, Guid oemsLocalId, Guid oemsEventId)
        {
            EntityReference eRef = null;
            var result = (from v in localContext.CreateQuery("oems_eventlocal")
                                      where (Guid)v["oems_event"] == oemsEventId
                                      && (Guid)v["oems_local"] == oemsLocalId

                                      select v).SingleOrDefault();


            
            if(result !=null)
            {
                eRef = result.ToEntityReference();
            }

            return eRef;

        }
    }
}

﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Omniware.ETFO.Plugin.DataWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Omniware.ETFO.Plugin.FormComponents
{
    public class AttachedDocuments : IFormElement
    {
        Dictionary<string, string> properties;

        public AttachedDocuments() 
        {
        }

        public bool LoadDynamicFormData(DataWork.DataAccess dataAccess, DataWork.FormElement element, Microsoft.Xrm.Sdk.Entity root, Guid groupRowId)
        {
            if(element.component == (int)FORM_COMPONENT.Attachments)
            {
                //Panel
                element.type = "panel";
                element.id = "attachments";
                element.name = "attachments";
                element.options = new ElementOptions() { title = "Supporting Documents", open = true };

                //Get all elements
                var propertiesInfo = (from prop in dataAccess.context.oems_FormElementPropertySet
                join propValue in dataAccess.context.oems_FormElementPropertyValueSet on prop.Id equals propValue.oems_FormElementProperty.Id
                select new Tuple<string, string>(prop.oems_PropertyCode, propValue.oems_PropertyValue));

                properties = new Dictionary<string, string>();
                foreach(var prop in propertiesInfo)
                {
                    properties[prop.Item1] = prop.Item2;
                }

                #region Html 1

                DataWork.FormElement legend = new FormElement();
                legend.type = "legend";
                legend.name = "attachmentslegend";
                legend.value = properties["pr_Html1"];
                legend.children = new FormElement[0];

                element.AddChild(legend);
                #endregion

                #region Before approval container

                DataWork.FormElement attachmentsBeforeApprovalContainer = new FormElement();
                attachmentsBeforeApprovalContainer.type = "attachmentsbeforeapprovalcontainer";
                attachmentsBeforeApprovalContainer.name = "attachmentsbeforeapprovalcontainer";
                attachmentsBeforeApprovalContainer.options = new ElementOptions() { required = true };
                
                FormatAttachmentsBeForeApprovalContainer(attachmentsBeforeApprovalContainer, dataAccess, root);
                element.AddChild(attachmentsBeforeApprovalContainer);

                #endregion

                #region Aproval container

                DataWork.FormElement attachmentsapprovedcontainer = new FormElement();
                attachmentsapprovedcontainer.name = "attachmentsapprovedcontainer";
                attachmentsapprovedcontainer.type = "attachmentsapprovedcontainer";
                attachmentsapprovedcontainer.options = new ElementOptions();
                attachmentsapprovedcontainer.children = new FormElement[0];

                FormatAttachmentsApprovedContainer(attachmentsapprovedcontainer, dataAccess, root);
                element.AddChild(attachmentsapprovedcontainer);

                #endregion

                #region Alert

                DataWork.FormElement attachmentsAlert = new FormElement();
                attachmentsAlert.type = "attachmentsalert";
                attachmentsAlert.name = "attachmentsalert";
                attachmentsAlert.children = new FormElement[0];

                element.AddChild(attachmentsAlert);

                #endregion

                return true;
            }

            return false;
        }

        void FormatAttachmentsBeForeApprovalContainer(DataWork.FormElement beforeApprovalElement, DataAccess dataaccess, Entity root) 
        {
            beforeApprovalElement.type = "attachmentsbeforeapprovalcontainer";
            beforeApprovalElement.name = "attachmentsbeforeapprovalcontainer";
            beforeApprovalElement.children = new FormElement[0];

            #region Course Outline

            DataWork.FormElement courseOutline = new FormElement();
            courseOutline.type = "attachmentsbeforeapprovalrequired";
            courseOutline.name = "course-outline";
            courseOutline.options = new ElementOptions() { required = true };
            courseOutline.children = new FormElement[0];
            courseOutline.checkboxValues = new OptionValue[1];

            // Course Outline
            EntityReference courseOutLineRef = (root.Contains("oems_courseoutline1")) ? (EntityReference)root.Attributes["oems_courseoutline1"] : null;
            courseOutline.checkboxValues[0] = CreateAttachmentElement(properties["pr_CourseOutlineLabel"], RetrieveDocument(dataaccess, courseOutLineRef), "course-outline", properties["pr_CourseOutlineRequiredFlag"]);

            beforeApprovalElement.AddChild(courseOutline);

            #endregion

            #region Returning Presenter Radios

            bool? returningPresenterVal = root.Contains("oems_returningpresenter") && root.Attributes["oems_returningpresenter"] != null ? (bool?)root.Attributes["oems_returningpresenter"] : (bool?)null;

            DataWork.FormElement returningPresenter = new FormElement();
            returningPresenter.type = "attachmentsyesnoradios";
            returningPresenter.name = "returning-presenter";
            returningPresenter.value = returningPresenterVal.HasValue ? returningPresenterVal.ToString().ToLower() : string.Empty;
            returningPresenter.options = new ElementOptions() { required = true, label = properties["pr_ReturningPresenterLabel"], selected = returningPresenterVal, disabled = returningPresenterVal };
            returningPresenter.children = new FormElement[0];

            beforeApprovalElement.AddChild(returningPresenter);

            #endregion

            #region Legend

            DataWork.FormElement legendReturningPresenter = new FormElement();
            legendReturningPresenter.type = "legend";
            legendReturningPresenter.name = "returning-presenter";
            legendReturningPresenter.value = properties["pr_HtmlReturningPresenter"];
            legendReturningPresenter.children = new FormElement[0];

            beforeApprovalElement.AddChild(legendReturningPresenter);
           
            #endregion

            #region Attachments list

            FormElement attachmentsbeforeapproval = new DataWork.FormElement();
            attachmentsbeforeapproval.name = "attachmentsbeforeapproval";
            attachmentsbeforeapproval.type = "attachmentsbeforeapproval";
            attachmentsbeforeapproval.options = new ElementOptions() { required = true };
            attachmentsbeforeapproval.children = new FormElement[0];

            attachmentsbeforeapproval.checkboxValues = new OptionValue[3];

            bool hideUpload = returningPresenterVal.HasValue && returningPresenterVal.Value;

            // Resume
            EntityReference resume = (root.Contains("oems_resume1")) ? (EntityReference)root.Attributes["oems_resume1"] : null;
            attachmentsbeforeapproval.checkboxValues[0] = CreateAttachmentElement(properties["pr_ResumeLabel"], RetrieveDocument(dataaccess, resume), "resume", properties["pr_ResumeRequiredFlag"], hideUpload);

            // Letter of Reference 1
            EntityReference letterOfReference1 = (root.Contains("oems_letterofreference1")) ? (EntityReference)root.Attributes["oems_letterofreference1"] : null;
            attachmentsbeforeapproval.checkboxValues[1] = CreateAttachmentElement(properties["pr_LetterOfReference1Label"], RetrieveDocument(dataaccess, letterOfReference1), "letter-reference1", properties["pr_LetterOfReference1RequiredFlag"], hideUpload);

            // Letter of Reference 2
            EntityReference letterOfReference2 = (root.Contains("oems_letterofreference2")) ? (EntityReference)root.Attributes["oems_letterofreference2"] : null;
            attachmentsbeforeapproval.checkboxValues[2] = CreateAttachmentElement(properties["pr_LetterOfReference2Label"], RetrieveDocument(dataaccess, letterOfReference2), "letter-reference2", properties["pr_LetterOfReference2RequiredFlag"], hideUpload);

            beforeApprovalElement.AddChild(attachmentsbeforeapproval);

            #endregion        
        }

        void FormatAttachmentsApprovedContainer(DataWork.FormElement element, DataAccess dataaccess, Entity root, bool hideUpload = false)
        {
            if (root.Contains("oems_approvedflag") && (bool)root.Attributes["oems_approvedflag"])
            {
                //HTML 2
                element.value = properties["pr_Html2"];

                #region Blank forms

                var attachmentsapprovedEL = new FormElement();
                attachmentsapprovedEL.name = "attachmentsapprovedempty";
                attachmentsapprovedEL.type = "attachmentsapprovedempty";
                attachmentsapprovedEL.value = properties["pr_BlankFormsLabel"];
                attachmentsapprovedEL.children = new FormElement[0];

                var rootEvent = dataaccess.context.Retrieve("oems_event", ((EntityReference)root["oems_event"]).Id, new ColumnSet(true));

                attachmentsapprovedEL.checkboxValues = new OptionValue[4];

                // Instructor Course Agreement Blank Form
                var instructorAgreementForm = (rootEvent.Contains("oems_instructorcourseagreementblankform1")) ? (EntityReference)rootEvent.Attributes["oems_instructorcourseagreementblankform1"] : null;
                attachmentsapprovedEL.checkboxValues[0] = CreateAttachmentElement(properties["pr_InstructorCourseAgreementBlankFormLabel"], RetrieveDocument(dataaccess, instructorAgreementForm), "instructor-course-agreement-form", false.ToString());

                // Instructor Payroll Enrollment Blank Form
                var instructorPayrollEnrollmentForm = (rootEvent.Contains("oems_instructorpayrollenrollmentblankform1")) ? (EntityReference)rootEvent.Attributes["oems_instructorpayrollenrollmentblankform1"] : null;
                attachmentsapprovedEL.checkboxValues[1] = CreateAttachmentElement(properties["pr_InstructorPayrollBlankFormLabel"], RetrieveDocument(dataaccess, instructorPayrollEnrollmentForm), "instructor-payroll-enrollment-form", false.ToString());

                // Sample Participation Letter
                var sampleParticipationLetterForm = (rootEvent.Contains("oems_sampleparticipationletter")) ? (EntityReference)rootEvent.Attributes["oems_sampleparticipationletter"] : null;
                attachmentsapprovedEL.checkboxValues[2] = CreateAttachmentElement(properties["pr_SampleParticipationLetterBlankFormLabel"], RetrieveDocument(dataaccess, sampleParticipationLetterForm), "sample-participation-letter-form", false.ToString());

                // Letter Template
                var letterTemplateForm = (rootEvent.Contains("oems_lettertemplate")) ? (EntityReference)rootEvent.Attributes["oems_lettertemplate"] : null;
                attachmentsapprovedEL.checkboxValues[3] = CreateAttachmentElement(properties["pr_InstructorPayrollBlankFormLabel"], RetrieveDocument(dataaccess, letterTemplateForm), "letter-template-form", false.ToString());

                element.AddChild(attachmentsapprovedEL);

                #endregion

                #region Upload documents

                FormElement attachmentsapproved = new DataWork.FormElement();
                attachmentsapproved.name = "attachmentsapproved";
                attachmentsapproved.type = "attachmentsapproved";
                attachmentsapproved.value = properties["pr_UploadFormsLabel"];
                attachmentsapproved.options = new ElementOptions() { required = true };
                attachmentsapproved.children = new FormElement[0];
                attachmentsapproved.checkboxValues = new OptionValue[3];

                // Individual Course Participation Letter
                EntityReference individualParticipationLetter = (root.Contains("oems_individualcourseparticipationletter1")) ? (EntityReference)root.Attributes["oems_individualcourseparticipationletter1"] : null;
                attachmentsapproved.checkboxValues[0] = CreateAttachmentElement(properties["pr_IndividualCourseLabel"], RetrieveDocument(dataaccess, individualParticipationLetter), "individual-course-participation-letter", properties["pr_IndividualCourseRequiredFlag"]);

                // Instructor Course Agreement Form
                EntityReference instructorAgreementCourse = (root.Contains("oems_instructorcourseagreementform1")) ? (EntityReference)root.Attributes["oems_instructorcourseagreementform1"] : null;
                attachmentsapproved.checkboxValues[1] = CreateAttachmentElement(properties["pr_InstructorCourseAgreementFormLabel"], RetrieveDocument(dataaccess, instructorAgreementCourse), "instructor-course-agreement-form", properties["pr_InstructorCourseAgreementFormRequiredFlag"]);

                // Instructor Payroll Enrollment Form
                EntityReference instructorPaymentEnrollment = (root.Contains("oems_instructorpayrollenrollmentform1")) ? (EntityReference)root.Attributes["oems_instructorpayrollenrollmentform1"] : null;
                attachmentsapproved.checkboxValues[2] = CreateAttachmentElement(properties["pr_InstructorPayrollFormLabel"], RetrieveDocument(dataaccess, instructorPaymentEnrollment), "instructor-payroll-enrollment-form", properties["pr_InstructorPayrollFormRequiredFlag"]);

                element.AddChild(attachmentsapproved);

                #endregion

            }
        }

        Guid? RetrieveDocument(DataWork.DataAccess dataAccess,EntityReference fieldReference) 
        {
            if (fieldReference != null)
            {
                Entity annotation = dataAccess.context.AnnotationSet.Where(x => x.ObjectId.Id == fieldReference.Id).FirstOrDefault<Entity>();
                
                if (annotation != null)
                {
                    return annotation.Id;
                }
                else
                {
                    return null;
                }
            }
            return null;
        }

        OptionValue CreateAttachmentElement(string label, Guid? value, string type, string required, bool hideUpload = false) 
        {
            OptionValue checkElement = new OptionValue();
            
            checkElement.label = label;
            checkElement.value = (value.HasValue) ? value.Value.ToString() : null;

            checkElement.required = required.ToLower();
            
            checkElement.group = hideUpload.ToString().ToLower();
            checkElement.type = type;
            checkElement.selected = (value == null) ? false.ToString().ToLower() : true.ToString().ToLower();
            
            return checkElement;
        }

        public bool SaveElementData(DataWork.DataAccess dataAccess, DataWork.FormElement element, List<DataWork.NameValuePair> data, Microsoft.Xrm.Sdk.Entity root)
        {
            if (element.component == (int)FORM_COMPONENT.Attachments) 
            {
                return true;
            }

            return false;
        }
    }
}
